package gr.gsis.rgwspublic.rgwspublic;

import gr.neuropublic.utils.TwoTuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.Holder;
import java.io.IOException;

public class TestWebService {

    private static Logger logger = LoggerFactory.getLogger(TestWebService.class);

    public static void main(String[] args ) throws ParserConfigurationException, IOException, SAXException {

        try {
            //System.out.println(GsisServiceStub.getVersionInfo());
            System.out.println("Sending Request...");
            TwoTuple<Holder<RgWsPublicBasicRtUser>, Holder<RgWsPublicFirmActRtUserArray>> afmInfo =  GsisServiceStub.getAfmInfo("119672410");

            System.out.println("Request received... first ="+afmInfo.getFirst());
            Holder<RgWsPublicBasicRtUser> first = afmInfo.getFirst();
            Holder<RgWsPublicFirmActRtUserArray> second = afmInfo.getSecond();

            if( (first != null) && (first.value != null)){

                System.out.println(first.value.getAfm());
                System.out.println(first.value.getCommerTitle());
                System.out.println(first.value.getDeactivationFlag());
                System.out.println(first.value.getDeactivationFlagDescr());
                System.out.println(first.value.getDoy());
                System.out.println(first.value.getDoyDescr());
                System.out.println(first.value.getFirmFlagDescr());
                System.out.println(first.value.getINiFlagDescr());
                System.out.println(first.value.getLegalStatusDescr());
                System.out.println(first.value.getOnomasia());
                System.out.println(first.value.getPostalAddress());
                System.out.println(first.value.getPostalZipCode());
            }


        } catch (Exception e) {

            System.out.println(e.getMessage());
        }
    }
}
