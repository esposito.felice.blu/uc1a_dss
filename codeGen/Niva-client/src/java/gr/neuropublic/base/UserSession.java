/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.functional.Func;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gmamais
 */
public class UserSession {
    private static final Logger logger = LoggerFactory.getLogger(UserSession.class);
    public final Integer userId;
    public final String usrEmail;
    public final Integer subsId;
    public final String subsCode;
    public final String subsDescription;
    public final Integer fromSubsId; 
    public final String appName;
    public final String userVat;
    public final String clientIp;
    public final HashSet<String> privileges = new HashSet<String>();
    public final ArrayList<Integer> subSecClasses = new ArrayList();
    public final CryptoUtils crypto;
    private Date lastAccess;
    private String sessionId;
    private String usrActiveEmail;
    private String userLoginName;
    
    public UserSession(
            final Integer userId, 
            final String usrEmail,
            final String userVat,
            final Integer subsId,
            final String subsCode,
            final String subsDescription,
            final Integer fromSubsId,
            final String appName,
            final List<String> privileges,
            final List<Integer> subSecClasses,
            final String userSalt,
            final String clientIp) 
    {
        this.userId = userId;
        this.usrEmail = usrEmail;
        this.subsId = subsId;
        this.subsCode = subsCode;
        this.subsDescription = subsDescription;
        this.fromSubsId = fromSubsId;
        this.appName = appName;
        this.userVat = userVat;
        this.crypto = new CryptoUtils(userSalt);
        this.lastAccess = new Date();
        if (privileges != null)
            this.privileges.addAll(privileges);
        if (subSecClasses!=null)
            this.subSecClasses.addAll(subSecClasses);
        this.clientIp = clientIp;
    }
    
    public long secondsSinceLastAccess() {
        Date curTime = new Date();
        long diff = curTime.getTime() - lastAccess.getTime();
        return diff/1000;
    }
    
    public void updateLastAccessTime() {
        lastAccess = new Date();
    }
    
    public final long LOGOUT_TIMEOUT = 3600;
    
    public boolean isOld() {
        return secondsSinceLastAccess() > LOGOUT_TIMEOUT;
    }
    
    
    public String DecryptAnyToString(String encryptedMimeText, String type){
        if (type.equals("String"))
            return DecryptString(encryptedMimeText);
        if (type.equals("BigInteger"))
            return DecryptBigInteger(encryptedMimeText).toString();
        if (type.equals("Integer"))
            return DecryptInteger(encryptedMimeText).toString();
        if (type.equals("Long"))
            return DecryptLong(encryptedMimeText).toString();
        if (type.equals("Short"))
            return DecryptShort(encryptedMimeText).toString();
        if (type.equals("BigDecimal"))
            return DecryptBigDecimal(encryptedMimeText).toString();
        if (type.equals("Double"))
            return DecryptDouble(encryptedMimeText).toString();
        if (type.equals("Float"))
            return DecryptFloat(encryptedMimeText).toString();
        
        logger.error("Unknown type in UserSession.DecryptAny");
        throw new GenericApplicationException("Unknown type in UserSession.DecryptAny");
        
    }
    
    public String EncryptString(final String vl) {
        return this.crypto.EncryptString(vl);
    }

    public String DecryptString(String encryptedMimeText)  {
        return this.crypto.DecryptString(encryptedMimeText);
    }
    
    
    public String EncryptNumber(BigInteger vl) {
        return this.crypto.EncryptNumber(vl);
    }
    
    public String EncryptNumber(Integer vl) {
        return this.crypto.EncryptNumber(vl);
    }
    public String EncryptNumber(Long vl) {
        return this.crypto.EncryptNumber(vl);
    }
    public String EncryptNumber(Short vl) {
        return this.crypto.EncryptNumber(vl);
    }
    
    public String EncryptNumber(BigDecimal vl) {
        return this.crypto.EncryptNumber(vl);
    }
    
    public String EncryptNumber(Double vl) {
        return this.crypto.EncryptNumber(vl);
    }
    public String EncryptNumber(Float vl) {
        return this.crypto.EncryptNumber(vl);
    }
    public BigInteger DecryptBigInteger(String encryptedMimeText) {
        return this.crypto.DecryptBigInteger(encryptedMimeText);
    }
    public Integer DecryptInteger(String encryptedMimeText) {
        return this.crypto.DecryptInteger(encryptedMimeText);
    }
    public Long DecryptLong(String encryptedMimeText) {
        return this.crypto.DecryptLong(encryptedMimeText);
    }
    public Short DecryptShort(String encryptedMimeText) {
        return this.crypto.DecryptShort(encryptedMimeText);
    }
    public BigDecimal DecryptBigDecimal(String encryptedMimeText) {
        return this.crypto.DecryptBigDecimal(encryptedMimeText);
    }
    public Double DecryptDouble(String encryptedMimeText) {
        return this.crypto.DecryptDouble(encryptedMimeText);
    }
    public Float DecryptFloat(String encryptedMimeText) {
        return this.crypto.DecryptFloat(encryptedMimeText);
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsrActiveEmail() {
        return usrActiveEmail;
    }

    public void setUsrActiveEmail(String usrActiveEmail) {
        this.usrActiveEmail = usrActiveEmail;
    }
    
    public String getUserLoginName() {
        return userLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }
    
    public String getClientIp0() {
        return this.clientIp != null ? this.clientIp.split(",")[0] : null;
    }
}
