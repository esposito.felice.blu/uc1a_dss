/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gmamais
 */
public class Property {
    private static Logger logger = LoggerFactory.getLogger(Property.class);

    public static String getPropertyValue(String fileName, String property, String defaultValue) {

        Properties props = new Properties();  
        String serverConfUrl = System.getProperty("jboss.server.config.dir") ;
        String path = new File(serverConfUrl,fileName).toString();  

        if(new File(path).exists()) {  
            try {  
                props.load(new FileInputStream(path));
            } catch (IOException ex) {
                logger.error("File: " + path + " doesn't exists");
            }
        }
        return props.getProperty(property, defaultValue);
    }
    
    
}
