/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import gr.neuropublic.exceptions.GenericApplicationException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author gmamais
 */
public class CryptoUtils {
    Cipher aes;
    SecretKeySpec key;
    
    public CryptoUtils(String password)  {
        try {
			aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
			MessageDigest digest = MessageDigest.getInstance("SHA");
			digest.update(password.getBytes());
			key = new SecretKeySpec(digest.digest(), 0, 16, "AES");
        } catch (Exception e) {
            throw new RuntimeException (e);
        }
    }

    
    public String EncryptString(String vl)  {
        try {
            synchronized(aes) {
                aes.init(Cipher.ENCRYPT_MODE, key);

                byte[] ciphertext = aes.doFinal(vl.getBytes());

                String encryptedMimeText = Base64Utils.encode(ciphertext);
                return encryptedMimeText;
            }
        } catch (Exception e) {
            throw new RuntimeException (e);
        }
    }
    
    public String DecryptString(String encryptedMimeText)  {
        try {
            synchronized(aes) {
                byte[] encryptedBytes = Base64Utils.decode(encryptedMimeText);
                aes.init(Cipher.DECRYPT_MODE, key);

                byte [] decodedBytes = aes.doFinal(encryptedBytes);
                return new String(decodedBytes);
            }
        } catch (Exception e) {
            //throw new RuntimeException (e);
            throw new GenericApplicationException("Unable to connect to application server (error code:0406)");
        }
    }
    
    
    public String EncryptNumber(BigInteger vl)  {
        try {
            synchronized(aes) {
                aes.init(Cipher.ENCRYPT_MODE, key);

                byte[] ciphertext = aes.doFinal(vl.toByteArray());

                String encryptedMimeText = Base64Utils.encode(ciphertext);
                return encryptedMimeText;
            }
        } catch (Exception e) {
            throw new RuntimeException (e);
        }
    }
    public String EncryptNumber(Integer vl)  {
        return EncryptNumber(BigInteger.valueOf(vl));
    }
    public String EncryptNumber(Long vl)  {
        return EncryptNumber(BigInteger.valueOf(vl));
    }
    public String EncryptNumber(Short vl)  {
        return EncryptNumber(BigInteger.valueOf(vl));
    }
    
    public String EncryptNumber(BigDecimal vl)  {
        return EncryptNumber(vl.unscaledValue());
    }
    
    public String EncryptNumber(Double vl)  {
        return EncryptNumber(BigDecimal.valueOf(vl));
    }
    public String EncryptNumber(Float vl)  {
        return EncryptNumber(BigDecimal.valueOf(vl));
    }
    public BigInteger DecryptBigInteger(String encryptedMimeText)  {
        try {
            synchronized(aes) {
                byte[] encryptedBytes = Base64Utils.decode(encryptedMimeText);
                aes.init(Cipher.DECRYPT_MODE, key);

                byte [] decodedBytes = aes.doFinal(encryptedBytes);
                return new BigInteger(decodedBytes);
            }
        } catch (Exception e) {
            //throw new RuntimeException (e);
            throw new GenericApplicationException("Unable to connect to application server (error code:0406)");
        }
    }
    public Integer DecryptInteger(String encryptedMimeText)  {
        BigInteger ret = DecryptBigInteger(encryptedMimeText);
        return ret.intValue();
    }
    public Long DecryptLong(String encryptedMimeText)  {
        BigInteger ret = DecryptBigInteger(encryptedMimeText);
        return ret.longValue();
    }
    public Short DecryptShort(String encryptedMimeText)  {
        BigInteger ret = DecryptBigInteger(encryptedMimeText);
        return ret.shortValue();
    }
    public BigDecimal DecryptBigDecimal(String encryptedMimeText)  {
        BigInteger ret = DecryptBigInteger(encryptedMimeText);
        return new BigDecimal(ret,0);
    }
    public Double DecryptDouble(String encryptedMimeText)  {
        BigDecimal ret = DecryptBigDecimal(encryptedMimeText);
        return ret.doubleValue();
    }
    public Float DecryptFloat(String encryptedMimeText)  {
        BigDecimal ret = DecryptBigDecimal(encryptedMimeText);
        return ret.floatValue();
    }
}
