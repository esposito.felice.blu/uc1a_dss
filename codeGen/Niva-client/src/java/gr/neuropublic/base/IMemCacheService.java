/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

public interface IMemCacheService {

    @Local
    public interface ILocal extends IMemCacheService {}

    @Remote
    public interface IRemote extends IMemCacheService {}

  public void setValue(String key, String value);
  public String getValue(String key);
  public void deleteKey(String key);
  
  public void setListValue(String key, List<String> value);
  public List<String> getListValue(String key);
  
  public String getExternalLogin_reqKey();
}
