/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import java.util.Date;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.ArrayList;
import java.util.List;

/**
/**
 *
 * @author gmamais
 */
public class ChangeToCommit<T>  implements Comparable<ChangeToCommit<T>>{

    private T entity;
    private Date when;
    private ChangeStatus status;

    public ChangeToCommit(T entity, ChangeStatus status)
    {
        if (entity == null)
        {
            throw new IllegalArgumentException("entity is null");
        }
        if (status == null)
        {
            throw new IllegalArgumentException("status is null");
        }

        this.entity = entity;
        this.when = new Date();
        this.status = status;
    }

    public ChangeToCommit(T entity, Date when, ChangeStatus status)
    {
        if (entity == null)
        {
            throw new IllegalArgumentException("entity is null");
        }
        if (when == null)
        {
            throw new IllegalArgumentException("when is null");
        }
        if (status == null)
        {
            throw new IllegalArgumentException("status is null");
        }

        this.entity = entity;
        this.when = when;
        this.status = status;
    }


    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    public Date getWhen() {
        return when;
    }

    public void setWhen(Date when) {
        this.when = when;
    }

    public ChangeStatus getStatus() {
        return status;
    }

    /**
     * @param modificationKind the modificationKind to set
     */
    public void setStatus(ChangeStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChangeToCommit that = (ChangeToCommit) o;

        if (entity != null ? !entity.equals(that.entity) : that.entity != null) return false;
        if (status != that.status) return false;
        if (when != null ? !when.equals(that.when) : that.when != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = entity != null ? entity.hashCode() : 0;
        result = 31 * result + (when != null ? when.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return String.format("Entity = %s When = %d  Status = %s", entity.toString(), when.getTime(), status);
    }

    @Override
    public int compareTo(ChangeToCommit<T> o)
    {

        if (this.when.compareTo(o.when) > 0)
        {
            return 1;
        } else if (this.when.compareTo(o.when) < 0)
        {
            return -1;
        } else
        {
            return 0;
        }
    }


    public enum ChangeStatus {

        NEW,
        UPDATE,
        DELETE
    }
	
    public static <K extends ICheckedEntity> gr.neuropublic.mutil.base.Pair<K,ChangeStatus>  getFirstOrNull(Class<K> type, List<ChangeToCommit<ICheckedEntity>> changes) {
        for(ChangeToCommit<ICheckedEntity> cur:changes) {
            if ( type.isAssignableFrom(cur.getEntity().getClass()) )
                    return new gr.neuropublic.mutil.base.Pair<K,ChangeStatus>((K)cur.getEntity(), cur.getStatus());
        }
        return null;
    }
    
    public static <K extends ICheckedEntity> gr.neuropublic.mutil.base.Pair<K,ChangeStatus>  getFirst(Class<K> type, List<ChangeToCommit<ICheckedEntity>> changes) {
        gr.neuropublic.mutil.base.Pair<K,ChangeStatus> ret = getFirstOrNull(type, changes);
        if (ret == null)
            throw new IllegalStateException();
        
        return ret;
    }
	
    public static <K extends ICheckedEntity> ArrayList<K> getRowsForInsert(Class<K> type, List<ChangeToCommit<ICheckedEntity>> changes) {
        ArrayList<K> ret = new ArrayList<K>();
        for(ChangeToCommit<ICheckedEntity> cur:changes) {
            if ((cur.getStatus() == ChangeStatus.NEW) && (type.isAssignableFrom(cur.getEntity().getClass())) )
                       ret.add((K)cur.getEntity()); 
        }
        return ret;
    }
    
    public static <K extends ICheckedEntity> ArrayList<K> getRowsForUpdate(Class<K> type, List<ChangeToCommit<ICheckedEntity>> changes) {
        ArrayList<K> ret = new ArrayList<K>();
        for(ChangeToCommit<ICheckedEntity> cur:changes) {
            if ((cur.getStatus() == ChangeStatus.UPDATE) && (type.isAssignableFrom(cur.getEntity().getClass())) )
                       ret.add((K)cur.getEntity()); 
        }
        return ret;
    }
    
    public static <K extends ICheckedEntity> ArrayList<K> getRowsForDelete(Class<K> type, List<ChangeToCommit<ICheckedEntity>> changes) {
        ArrayList<K> ret = new ArrayList<K>();
        for(ChangeToCommit<ICheckedEntity> cur:changes) {
            if ((cur.getStatus() == ChangeStatus.DELETE) && (type.isAssignableFrom(cur.getEntity().getClass())) )
                       ret.add((K)cur.getEntity()); 
        }
        return ret;
    }
	
}
