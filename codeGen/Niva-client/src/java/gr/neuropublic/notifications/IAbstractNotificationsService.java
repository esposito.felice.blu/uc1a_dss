package gr.neuropublic.notifications;

import com.google.gson.JsonObject;
import java.util.Date;

/**
 *
 * @author st_krommydas
 */
public abstract interface IAbstractNotificationsService {
    public void setVerboseFlag(boolean verboseFlag);
    public FcmResponse sendNotificationToAllDevices(String title, String body, Date expireDate);
    public FcmResponse sendNotificationMessageToAllDevices(String title, String body, Date expireDate, JsonObject data);
    public FcmResponse sendNotification(String title, String body, AbstractFcmTarget target, Date expireDate);
    public FcmResponse sendNotificationMessage(String title, String body, AbstractFcmTarget target, Date expireDate, JsonObject data);
}
