/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.notifications;

/**
 *
 * @author st_krommydas
 */
public class FcmTopicTarget extends AbstractFcmTarget {
    
    public FcmTopicTarget(String value) {
        super("topic", value);
    }
}
