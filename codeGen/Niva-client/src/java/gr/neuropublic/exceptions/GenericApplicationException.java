
package gr.neuropublic.exceptions;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.ApplicationException;
import org.slf4j.LoggerFactory;

@ApplicationException(rollback = true)
public class GenericApplicationException extends RuntimeException{

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger("GAE");

    public GenericApplicationException(){
        super();
    }

    public GenericApplicationException(String msg){
        super(msg);
    }

    public GenericApplicationException(Throwable e){
        super(e);
    }

    public static String getExceptionStack(String msg, Exception e){
        String errorStack = "" + msg;
        int elemsLength = e.getStackTrace().length;
        for (int i=0 ; i<elemsLength && i < 5 ; i++){
            errorStack = errorStack + "<br>" + e.getStackTrace()[i].toString();
        }
        return errorStack;
    }
    
    public static String searchOracleApplicationError(String errMessage){
        if (errMessage != null) {
            errMessage = errMessage.replaceAll("ORA-20001: Unhandled Exception ", "");
        }
        String res = errMessage;
        String pattern = "^ORA-2\\d{4}: (.*?)ORA-\\d+:";

        Pattern r = Pattern.compile(pattern, Pattern.DOTALL);
        if (errMessage != null){
            Matcher m = r.matcher(errMessage);
            if (m.find()){
                res = m.group(1);
                if (res.trim().equals("")) {
                    res = errMessage;
                }
            } 
        }

        return res;
    }

    public static String parsePostgresqlError(String errMessage){
        if (errMessage == null)
            return null;
        String[] msgParts = errMessage.split("ERROR:");
        if (msgParts == null || msgParts.length == 1)
            return errMessage;
        String msg = msgParts[1].trim(); 
        String[] retParts = msg.split("Where: PL/pgSQL");
        if (retParts == null || retParts.length == 1)
            return msg;
        String ret = retParts[0].trim();
        return ret;
    }

    public static String getMessageFromJasperException(Exception e) {

        logger.info("!!! Exception during jasper invocation ", e);
        for (Throwable t = e.getCause(); t != null; t = t.getCause()) {
            if (t instanceof org.hibernate.exception.GenericJDBCException){
                String errMessage = searchOracleApplicationError(t.getMessage());
                return parsePostgresqlError(errMessage);
            } else if (t instanceof java.sql.SQLException) {
                String errMessage = searchOracleApplicationError(t.getMessage());
                return parsePostgresqlError(errMessage);
            } 
        }

        String errMessage = searchOracleApplicationError(e.getMessage());
        return errMessage;
    }
    
}
