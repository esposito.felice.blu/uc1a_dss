/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.exceptions;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class DatabaseConstraintViolationException extends RuntimeException{
    
    Object mergedEntity = null;
    
    public DatabaseConstraintViolationException(){
        super();
    }
    
    public DatabaseConstraintViolationException(String contraintName, Object entity){
        super(contraintName);
        this.mergedEntity = entity;
    }
    
    public DatabaseConstraintViolationException(String msg){
        super(msg);
    }
    
    public Object getEntity(){
        return mergedEntity;
    }
    
    public void setEntity(Object entity){
        this.mergedEntity = entity;
    }
}
