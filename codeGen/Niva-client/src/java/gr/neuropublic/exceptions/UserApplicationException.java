/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.neuropublic.exceptions;

import javax.ejb.ApplicationException;

/**
 *
 * @author st_krommydas
 */
@ApplicationException(rollback = true)
public class UserApplicationException extends RuntimeException {
    
    public UserApplicationException(){
        super();
    }
    
    public UserApplicationException(String msg){
        super(msg);
    }
}
