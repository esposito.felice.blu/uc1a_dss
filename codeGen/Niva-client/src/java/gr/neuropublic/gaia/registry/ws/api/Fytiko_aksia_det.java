package gr.neuropublic.gaia.registry.ws.api;

import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;

public class Fytiko_aksia_det {

    public String shallowToString() {
        return String.format("(%d, %d, %4.2f, %8.4f, %5.2f)", dnm_kodikos, elp_id, epilektash, estimated_quantity, estimated_unit_price);
    }

    public Fytiko_aksia_det(int dnm_kodikos, int elp_id, BigDecimal epilektash, BigDecimal estimated_quantity, BigDecimal estimated_unit_price) {
        this.dnm_kodikos = dnm_kodikos;
        this.elp_id = elp_id;
        this.epilektash = epilektash;
        this.estimated_quantity = estimated_quantity;
        this.estimated_unit_price = estimated_unit_price;
    }

    public int dnm_kodikos;
    public int elp_id;
    public BigDecimal epilektash;
    public BigDecimal estimated_quantity;
    public BigDecimal estimated_unit_price;
}