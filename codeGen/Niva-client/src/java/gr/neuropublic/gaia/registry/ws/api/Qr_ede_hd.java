package gr.neuropublic.gaia.registry.ws.api;

import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;

public class Qr_ede_hd {

    public String shallowToString() {
        return String.format("%d, %d, %d, %s, %d, %s, %d, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "+
                             "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %s, %s, %s, %s, %d, %s, %d, %s, %s, %d, %s, "+
                             "%s, %s, %s, %s, %s, %s, %s, %10.2f, %d, %10.2f, %d",
                      id, ede_id, ede_year, afm, prosopotype, prosopotype_description,
                      nmr_id, nmr_description, genderflag, gender_description, name, lastname,
                             firstname, fathername, odos_b1, numodos_b1, taxkodikos_b1, phone1_b1,
                             mobilephone_b1 ,ppr_kodikos, ppr_description,
                             dnm_kodikos_b1      , dnm_description_b1  , municipality_id_b1  ,
                              municipality_name_b1,
                              odos_b2             ,
                              numodos_b2          ,
                              taxkodikos_b2       ,
                              phone1_b2           ,
                              ppr_kodikos_b2      ,
                              ppr_description_b2  ,
                              dnm_kodikos_b2      ,
                              dnm_description_b2  ,
                              municipality_id_b2  ,
                              municipality_name_b2,
                              idtype              ,
                              idtype_description  ,
                              idno                ,
                              iddteekd==null?"":new SimpleDateFormat("yyyy.MM.dd").format(iddteekd),
                              idekdarxh           ,
                              doy_id              ,
                              doy_description     ,
                              efo_id              ,
                              efo_description     ,
                              iban                ,
                              bnk_id              ,
                              bnk_description     ,
                              daa_kodikos         ,
                              daa_description     ,
                              ama                 ,
                              amoga               ,
                              bnk_account         ,
                              dtebirth==null?"":new SimpleDateFormat("yyyy.MM.dd").format(dtebirth),
                              amka                ,
                              total_epilektash    ,
                              total_plots         ,
                              total_zoa           ,
                              total_stables);
    }

    public Qr_ede_hd(
                      int           id,
                      long          ede_id,
                      int           ede_year,
                      String        afm,
                      int           prosopotype,
                      String        prosopotype_description,
                      long          nmr_id,
                      String        nmr_description,
                      int           genderflag,
                      String        gender_description,
                      String        name          ,
                      String        lastname      ,
                      String        firstname     ,
                      String        fathername    ,
                      String        odos_b1       ,
                      String        numodos_b1    ,
                      String        taxkodikos_b1 ,
                      String        phone1_b1     ,
                      String        mobilephone_b1      ,
                      String        ppr_kodikos         ,
                      String        ppr_description     ,
                      String        dnm_kodikos_b1      ,
                      String        dnm_description_b1  ,
                      String        municipality_id_b1  ,
                      String        municipality_name_b1,
                      String        odos_b2             ,
                      String        numodos_b2          ,
                      String        taxkodikos_b2       ,
                      String        phone1_b2           ,
                      String        ppr_kodikos_b2      ,
                      String        ppr_description_b2  ,
                      String        dnm_kodikos_b2      ,
                      String        dnm_description_b2  ,
                      String        municipality_id_b2  ,
                      String        municipality_name_b2,
                      int           idtype              ,
                      String        idtype_description  ,
                      String        idno                ,
                      Timestamp     iddteekd            ,
                      String        idekdarxh           ,
                      long          doy_id              ,
                      String        doy_description     ,
                      long          efo_id              ,
                      String        efo_description     ,
                      String        iban                ,
                      long          bnk_id              ,
                      String        bnk_description     ,
                      String        daa_kodikos         ,
                      String        daa_description     ,
                      String        ama                 ,
                      String        amoga               ,
                      String        bnk_account         ,
                      Timestamp     dtebirth            ,
                      String        amka                ,
                      BigDecimal    total_epilektash    ,
                      int           total_plots         ,
                      BigDecimal    total_zoa           ,
                      int           total_stables
                     ) {
        this(
             id,
             ede_id,
             ede_year,
             afm,
             prosopotype,
             prosopotype_description,
             nmr_id,
             nmr_description,
             genderflag,
             gender_description,
             name          ,
             lastname      ,
             firstname     ,
             fathername    ,
             odos_b1       ,
             numodos_b1    ,
             taxkodikos_b1 ,
             phone1_b1     ,
             mobilephone_b1      ,
             ppr_kodikos         ,
             ppr_description     ,
             dnm_kodikos_b1      ,
             dnm_description_b1  ,
             municipality_id_b1  ,
             municipality_name_b1,
             odos_b2             ,
             numodos_b2          ,
             taxkodikos_b2       ,
             phone1_b2           ,
             ppr_kodikos_b2      ,
             ppr_description_b2  ,
             dnm_kodikos_b2      ,
             dnm_description_b2  ,
             municipality_id_b2  ,
             municipality_name_b2,
             idtype              ,
             idtype_description  ,
             idno                ,
             iddteekd            ,
             idekdarxh           ,
             doy_id              ,
             doy_description     ,
             efo_id              ,
             efo_description     ,
             iban                ,
             bnk_id              ,
             bnk_description     ,
             daa_kodikos         ,
             daa_description     ,
             ama                 ,
             amoga               ,
             bnk_account         ,
             dtebirth            ,
             amka                ,
             null                ,
             total_epilektash    ,
             total_plots         ,
             total_zoa           ,
             total_stables
             );
    }

    public Qr_ede_hd(
                      int           id,
                      long          ede_id,
                      int           ede_year,
                      String        afm,
                      int           prosopotype,
                      String        prosopotype_description,
                      long          nmr_id,
                      String        nmr_description,
                      int           genderflag,
                      String        gender_description,
                      String        name          ,
                      String        lastname      ,
                      String        firstname     ,
                      String        fathername    ,
                      String        odos_b1       ,
                      String        numodos_b1    ,
                      String        taxkodikos_b1 ,
                      String        phone1_b1     ,
                      String        mobilephone_b1      ,
                      String        ppr_kodikos         ,
                      String        ppr_description     ,
                      String        dnm_kodikos_b1      ,
                      String        dnm_description_b1  ,
                      String        municipality_id_b1  ,
                      String        municipality_name_b1,
                      String        odos_b2             ,
                      String        numodos_b2          ,
                      String        taxkodikos_b2       ,
                      String        phone1_b2           ,
                      String        ppr_kodikos_b2      ,
                      String        ppr_description_b2  ,
                      String        dnm_kodikos_b2      ,
                      String        dnm_description_b2  ,
                      String        municipality_id_b2  ,
                      String        municipality_name_b2,
                      int           idtype              ,
                      String        idtype_description  ,
                      String        idno                ,
                      Timestamp     iddteekd            ,
                      String        idekdarxh           ,
                      long          doy_id              ,
                      String        doy_description     ,
                      long          efo_id              ,
                      String        efo_description     ,
                      String        iban                ,
                      long          bnk_id              ,
                      String        bnk_description     ,
                      String        daa_kodikos         ,
                      String        daa_description     ,
                      String        ama                 ,
                      String        amoga               ,
                      String        bnk_account         ,
                      Timestamp     dtebirth            ,
                      String        amka                ,
                      Integer       drasthriothtaflag   ,
                      BigDecimal    total_epilektash    ,
                      int           total_plots         ,
                      BigDecimal    total_zoa           ,
                      int           total_stables
                     ) {
    this.         id=            id;
    this.         ede_id=           ede_id;
    this.         ede_year=            ede_year;
    this.         afm=         afm;
    this.         prosopotype=            prosopotype;
    this.         prosopotype_description=         prosopotype_description;
    this.         nmr_id=           nmr_id;
    this.         nmr_description=         nmr_description;
    this.         genderflag=            genderflag;
    this.         gender_description=         gender_description;
    this.         name          =         name          ;
    this.         lastname      =         lastname      ;
    this.         firstname     =         firstname     ;
    this.         fathername    =         fathername    ;
    this.         odos_b1       =         odos_b1       ;
    this.         numodos_b1    =         numodos_b1    ;
    this.         taxkodikos_b1 =         taxkodikos_b1 ;
    this.         phone1_b1     =         phone1_b1     ;
    this.         mobilephone_b1      =         mobilephone_b1      ;
    this.         ppr_kodikos         =         ppr_kodikos         ;
    this.         ppr_description     =         ppr_description     ;
    this.         dnm_kodikos_b1      =         dnm_kodikos_b1      ;
    this.         dnm_description_b1  =         dnm_description_b1  ;
    this.         municipality_id_b1  =         municipality_id_b1  ;
    this.         municipality_name_b1=         municipality_name_b1;
    this.         odos_b2             =         odos_b2             ;
    this.         numodos_b2          =         numodos_b2          ;
    this.         taxkodikos_b2       =         taxkodikos_b2       ;
    this.         phone1_b2           =         phone1_b2           ;
    this.         ppr_kodikos_b2      =         ppr_kodikos_b2      ;
    this.         ppr_description_b2  =         ppr_description_b2  ;
    this.         dnm_kodikos_b2      =         dnm_kodikos_b2      ;
    this.         dnm_description_b2  =         dnm_description_b2  ;
    this.         municipality_id_b2  =         municipality_id_b2  ;
    this.         municipality_name_b2=         municipality_name_b2;
    this.         idtype              =            idtype              ;
    this.         idtype_description  =         idtype_description  ;
    this.         idno                =         idno                ;
    this.         iddteekd            =      iddteekd            ;
    this.         idekdarxh           =         idekdarxh           ;
    this.         doy_id              =           doy_id              ;
    this.         doy_description     =         doy_description     ;
    this.         efo_id              =           efo_id              ;
    this.         efo_description     =         efo_description     ;
    this.         iban                =         iban                ;
    this.         bnk_id              =           bnk_id              ;
    this.         bnk_description     =         bnk_description     ;
    this.         daa_kodikos         =         daa_kodikos         ;
    this.         daa_description     =         daa_description     ;
    this.         ama                 =         ama                 ;
    this.         amoga               =         amoga               ;
    this.         bnk_account         =         bnk_account         ;
    this.         dtebirth            =      dtebirth            ;
    this.         amka                =         amka                ;
    this.         drasthriothtaflag   =      drasthriothtaflag      ;
    this.         total_epilektash    =         total_epilektash   ;
    this.         total_plots         =         total_plots   ;

    this.         total_zoa           =         total_zoa; 
    this.         total_stables       =         total_stables;

    this.qr_ede_agroi   = new ArrayList<Qr_ede_agroi>();
    this.qr_ede_stavloi = new ArrayList<Qr_ede_stavloi>();
    }

    public int           id; //                      | integer                     | not null default nextval('gr_query.ede_id_seq'::regclass) | plain    | 
    public long          ede_id; //                  | numeric(12,0)               | not null                                                  | main     | 
    public int           ede_year; //                | smallint                    | not null                                                  | plain    | 
    public String        afm; //                     | character varying(9)        |                                                           | extended | 
    public int           prosopotype; //             | numeric(2,0)                |                                                           | main     | 
    public String        prosopotype_description; // | character varying           |                                                           | extended | 
    public long          nmr_id; //                  | numeric(12,0)               |                                                           | main     | 
    public String        nmr_description; //         | character varying(40)       |                                                           | extended | 
    public int           genderflag; //              | numeric(1,0)                |                                                           | main     | 
    public String        gender_description; //      | character varying(50)       |                                                           | extended | 
    public String        name          ; //      | character varying(100)      |                                                           | extended | 
    public String        lastname      ; //          | character varying(100)      |                                                           | extended | 
    public String        firstname     ; //          | character varying(40)       |                                                           | extended | 
    public String        fathername    ; //          | character varying(40)       |                                                           | extended | 
    public String        odos_b1       ; //          | character varying(60)       |                                                           | extended | 
    public String        numodos_b1    ; //          | character varying(9)        |                                                           | extended | 
    public String        taxkodikos_b1 ; //          | character varying(5)        |                                                           | extended | 
    public String        phone1_b1     ; //          | character varying(15)       |                                                           | extended | 
    public String        mobilephone_b1      ; //    | character varying(30)       |                                                           | extended | 
    public String        ppr_kodikos         ; //    | character varying(12)       |                                                           | extended | 
    public String        ppr_description     ; //    | character varying(40)       |                                                           | extended | 
    public String        dnm_kodikos_b1      ; //    | character varying(2)        |                                                           | extended | 
    public String        dnm_description_b1  ; //    | character varying(40)       |                                                           | extended | 
    public String        municipality_id_b1  ; //    | character varying(10)       |                                                           | extended | 
    public String        municipality_name_b1; //    | character varying(100)      |                                                           | extended | 
    public String        odos_b2             ; //    | character varying(60)       |                                                           | extended | 
    public String        numodos_b2          ; //    | character varying(9)        |                                                           | extended | 
    public String        taxkodikos_b2       ; //    | character varying(5)        |                                                           | extended | 
    public String        phone1_b2           ; //    | character varying(15)       |                                                           | extended | 
    public String        ppr_kodikos_b2      ; //    | character varying(12)       |                                                           | extended | 
    public String        ppr_description_b2  ; //    | character varying(40)       |                                                           | extended | 
    public String        dnm_kodikos_b2      ; //    | character varying(2)        |                                                           | extended | 
    public String        dnm_description_b2  ; //    | character varying(40)       |                                                           | extended | 
    public String        municipality_id_b2  ; //    | character varying(10)       |                                                           | extended | 
    public String        municipality_name_b2; //    | character varying(100)      |                                                           | extended | 
    public int           idtype              ; //    | numeric(2,0)                |                                                           | main     | 
    public String        idtype_description  ; //    | character varying(50)       |                                                           | extended | 
    public String        idno                ; //    | character varying(10)       |                                                           | extended | 
    public Timestamp     iddteekd            ; //    | timestamp without time zone |                                                           | plain    | 
    public String        idekdarxh           ; //    | character varying(40)       |                                                           | extended | 
    public long          doy_id              ; //    | numeric(12,0)               |                                                           | main     | 
    public String        doy_description     ; //    | character varying(40)       |                                                           | extended | 
    public long          efo_id              ; //    | numeric(12,0)               |                                                           | main     | 
    public String        efo_description     ; //    | character varying(100)      |                                                           | extended | 
    public String        iban                ; //    | character varying(40)       |                                                           | extended | 
    public long          bnk_id              ; //    | numeric(12,0)               |                                                           | main     | 
    public String        bnk_description     ; //    | character varying(100)      |                                                           | extended | 
    public String        daa_kodikos         ; //    | character varying(3)        |                                                           | extended | 
    public String        daa_description     ; //    | character varying(100)      |                                                           | extended | 
    public String        ama                 ; //    | character varying(20)       |                                                           | extended | 
    public String        amoga               ; //    | character varying(20)       |                                                           | extended | 
    public String        bnk_account         ; //    | character varying(20)       |                                                           | extended | 
    public Timestamp     dtebirth            ; //   | timestamp without time zone |                                                           | plain    | 
    public String        amka                ; //    | character varying(16)       |                                                           | extended | 

    public Integer drasthriothtaflag              ;

    BigDecimal    total_epilektash               ;
    int           total_plots                    ;
    BigDecimal    total_zoa                      ;
    int           total_stables                  ;


    public List<Qr_ede_agroi> qr_ede_agroi       ; 
    public List<Qr_ede_stavloi> qr_ede_stavloi   ; 
}