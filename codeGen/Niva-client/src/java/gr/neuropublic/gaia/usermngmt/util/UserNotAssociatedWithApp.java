package gr.neuropublic.gaia.usermngmt.util;

public class UserNotAssociatedWithApp extends UserMngmtException {

    private String email;
    private String app;
    public UserNotAssociatedWithApp(String email, String app) {
        this.email = email;
        this.app = app;
    }

    public String toString() {
        return String.format("%s(%s, %s)", this.getClass().getName(), email, app);
    }

}