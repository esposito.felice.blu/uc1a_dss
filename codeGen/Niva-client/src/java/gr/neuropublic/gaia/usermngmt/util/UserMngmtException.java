package gr.neuropublic.gaia.usermngmt.util;

public abstract class UserMngmtException extends Exception {

    public void throwSpecificType() throws UserStatusNotActive,
                                           SubscriberNotAssociatedWithApp,
                                           UserNotAssociatedWithAppAlthoughSubscriptionOK,
                                           UserNotAssociatedWithApp,
                                           SubscriberStatusNotActive,
                                           SubscriberModuleGroupNotActive,
                                           SubscriberModuleGroupTemporalMismatch,
                                           UnknownApp {
        if (this.getClass().equals(UserStatusNotActive.class))                                 throw (UserStatusNotActive) this;
        else if (this.getClass().equals(SubscriberNotAssociatedWithApp.class))                 throw (SubscriberNotAssociatedWithApp) this;
        else if (this.getClass().equals(UserNotAssociatedWithAppAlthoughSubscriptionOK.class)) throw (UserNotAssociatedWithAppAlthoughSubscriptionOK) this;
        else if (this.getClass().equals(UserNotAssociatedWithApp.class))                       throw (UserNotAssociatedWithApp) this;
        else if (this.getClass().equals(SubscriberStatusNotActive.class))                      throw (SubscriberStatusNotActive) this;
        else if (this.getClass().equals(SubscriberModuleGroupNotActive.class))                 throw (SubscriberModuleGroupNotActive) this;
        else if (this.getClass().equals(SubscriberModuleGroupTemporalMismatch.class))          throw (SubscriberModuleGroupTemporalMismatch) this;
        else if (this.getClass().equals(UnknownApp.class)) throw (UnknownApp) this;
        else throw new RuntimeException("unrecognized class: %s" + this.getClass().getName());
    }

}