package gr.neuropublic.gaia.usermngmt.util;

public class UserStatusNotActive extends StatusException {
    public UserStatusNotActive(int status) {
        super(status);
    }
}