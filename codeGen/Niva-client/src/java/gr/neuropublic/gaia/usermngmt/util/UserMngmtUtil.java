package gr.neuropublic.gaia.usermngmt.util;

import java.sql.Connection; import java.sql.Statement; import java.sql.SQLException; import java.sql.ResultSet;
import java.sql.PreparedStatement; import java.sql.Timestamp; import java.sql.Types;
import java.util.List; import java.util.Map; import java.util.HashMap;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.shiro.crypto.hash.Sha256Hash;
import java.security.SecureRandom;
import java.math.BigInteger;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang3.StringUtils;

import gr.neuropublic.mutil.base.ExceptionAdapter;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.mutil.base.Triad;
import gr.neuropublic.mutil.base.Util;
import static gr.neuropublic.mutil.jdbc.JdbcUtils.ps_setInteger;
import static gr.neuropublic.mutil.jdbc.JdbcUtils.ps_setBoolean;
import org.apache.shiro.crypto.hash.Sha256Hash;



public class UserMngmtUtil {

    private static Logger l = LoggerFactory.getLogger(UserMngmtUtil.class);
    private static SecureRandom random = new SecureRandom();

    public static List<Pair<String, String>> ssoLoggedinApps(Connection conn, String ssoSessionId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet         rs = null;
        try {
            ps = conn.prepareStatement("SELECT usrs_email, usrs_app FROM subscription.ss_user_sessions WHERE usrs_ssosession=? AND usrs_logoutsse IS NULL");
            ps.setString(1, ssoSessionId);
            rs = ps.executeQuery();
            List<Pair<String,String>> retValue = new ArrayList<Pair<String,String>>();
            while (rs.next())
                retValue.add(Pair.create(rs.getString(1), rs.getString(2)));
            return retValue;
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    public static boolean existsConcurrentLoginWtSideEffects(Connection conn, String email, String app) throws SQLException {
        int now             = Util.secondsSinceTheEpoch();
        int thresholdInSecs = concurrentLogoutThresholdSecs(conn);
        PreparedStatement  pstm = null;
        ResultSet            rs = null;
        try {
            String stmtStr = "SELECT usrs_id, usrs_loginsse FROM subscription.ss_user_sessions WHERE "          +
                             "usrs_email=? AND usrs_app=? AND usrs_logoutsse IS NULL";
            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1, email);
            pstm.setString     (2, app);
            rs = pstm.executeQuery();
            int numOfDanglingSessionsEliminated = 0 ;
            int numOfFreshConcurrentSessions    = 0 ;
            while (rs.next()) {
                int usrsId   = rs.getInt(1);
                int loginSSE = rs.getInt(2);
                if (now - loginSSE >= thresholdInSecs) {
                    specialLogoutOfDanglingSessions(conn, usrsId);
                    numOfDanglingSessionsEliminated++;
                }
                else
                    numOfFreshConcurrentSessions++;
            }
            //l.info(String.format("existsConcrrentLoginWtSideEffects(%s, %s): %d threshold, %d dangling eliminated / %d fresh", email, app, thresholdInSecs, numOfDanglingSessionsEliminated, numOfFreshConcurrentSessions));
            return numOfFreshConcurrentSessions>0;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }



    public static boolean existsConcurrentLogin(Connection conn, String email, String app) throws SQLException {
        PreparedStatement  pstm = null;
        ResultSet            rs = null;
        try {
            String stmtStr = "SELECT COUNT(*) FROM subscription.ss_user_sessions WHERE "          +
                             "usrs_email=? AND usrs_app=? AND usrs_logoutsse IS NULL";
            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1, email);
            pstm.setString     (2, app);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            int numOfOtherAccesses = 0 ;
            while (rs.next()) {
                if (beenHereBefore) throw new RuntimeException();
                numOfOtherAccesses = rs.getInt(1);
                beenHereBefore = true;
            }
            return numOfOtherAccesses>0;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    private static Pair<String, String> passwordAndSalt(Connection conn, String email) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet           rs = null;
        try {
            String stmtStr = "SELECT user_password, user_password_salt FROM subscription.ss_users WHERE user_email=?";
            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1, email);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            boolean atLeastOneRow  = false;
            String pwd  = null;
            String salt = null;
            while (rs.next()) {
                if (beenHereBefore) throw new RuntimeException();
                atLeastOneRow = true;
                pwd  = rs.getString(1);
                salt = rs.getString(2);
                beenHereBefore = true;
            }
            return atLeastOneRow?Pair.create(pwd, salt):null;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    public static void main(String args[]) {
        final String salt = "sd;lj;21l34sd09fAS@ds;fsdfjazl;jkqp[weori[xcv.msdaszczx7878723j;kj";
        long total = 0;
        int iterations = 10;
        for (int i = 0 ; i < iterations ; i++) {
            long l1 = System.currentTimeMillis();
            String foo = hash256("secret-password123", salt, 1000*1000);
            long l2 = System.currentTimeMillis();
            total += (l2 -l1);
        }
        System.out.println(String.format("hash created in: %5.2f milliseconds", ((float) total) / iterations));
    }

    private static String hash256(String input, String salt, int iterations) {
        Sha256Hash hash = new Sha256Hash(input,salt, iterations);
        return hash.toHex();
    }


    public static EmailPwdOutcome checkPassword(Connection conn, String email, String pass) throws SQLException {
        Pair<String, String> hashedPwdAndSalt = passwordAndSalt(conn, email);
        if (hashedPwdAndSalt != null) {
            String hashedPwd = hashedPwdAndSalt.a;
            String salt      = hashedPwdAndSalt.b;
            String passHashed = hash256(pass, salt, 1000000);
            // l.info(String.format("pass=%s, db-hashed-pwd=%s, salt=%s, hashed-salted-pass=%s", pass, hashedPwd, salt, passHashed));
            return passHashed.equals(hashedPwd)?EmailPwdOutcome.CORRECT_PWD:EmailPwdOutcome.WRONG_PWD;
        } else return EmailPwdOutcome.ACCOUNT_NOT_EXISTS;

    }

    /*   replaced by makeNoteOfSuccessfulLogin
    public static void createSSOSession(Connection conn, String email, String ip, String session, String ssoSession, String app) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String stmtStr =
              "INSERT INTO ss_user_sessions(usrs_email, usrs_ip, usrs_session, usrs_ssosession, usrs_app, usrs_loginsse) "+
              "VALUES(?, ?, ?, ?, ?, ?)" ;
            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1,                       email);
            pstm.setString     (2,                          ip);
            pstm.setString     (3,                     session);
            pstm.setString     (4,                  ssoSession);
            pstm.setString     (5,                         app);
            pstm.setInt        (6, Util.secondsSinceTheEpoch());
            pstm.executeUpdate ();
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }*/

    public static void makeNoteOfSuccessfulLogin(Connection conn, 
                                                  String     userName, 
                                                  String     userIp, 
                                                  String     userSessionId, 
                                                  String     ssoSessionId,
                                                  String     appName) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String stmtStr = "INSERT INTO subscription.ss_user_sessions "+
                             "(usrs_email, usrs_ip, usrs_session, usrs_ssosession, usrs_app, usrs_loginsse) "+
                             "VALUES(?,?,?,?,?,?)";
            pstm  = conn.prepareStatement(stmtStr);

            pstm.setString     (1, userName);
            pstm.setString     (2, userIp);
            pstm.setString     (3, userSessionId);
            pstm.setString     (4, ssoSessionId);
            pstm.setString     (5, appName);
            pstm.setInt        (6, Util.secondsSinceTheEpoch());
            pstm.executeUpdate();
        } finally {
            DbUtils.closeQuietly(pstm);
        }        
        try {
            String stmtStr = "UPDATE subscription.ss_users SET user_last_login_date = now() WHERE user_email = ?";
            pstm  = conn.prepareStatement(stmtStr);

            pstm.setString     (1, userName);
            pstm.executeUpdate();
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }



    public static int logoutUser   (Connection conn, String email, LogoutCode logoutCode) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String pstmStr = "UPDATE subscription.ss_user_sessions SET usrs_logoutsse=?, uslc_id=? WHERE usrs_email=? AND usrs_logoutsse IS NULL";
            pstm = conn.prepareStatement( pstmStr );
            pstm.setInt   (1, Util.secondsSinceTheEpoch());
            pstm.setInt   (2, logoutCode.getCode());
            pstm.setString(3, email);
            return pstm.executeUpdate();
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static int logoutUser   (Connection conn, String email, String app, LogoutCode logoutCode) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String pstmStr = "UPDATE subscription.ss_user_sessions SET usrs_logoutsse=?, uslc_id=? WHERE usrs_email=? AND usrs_app=? AND usrs_logoutsse IS NULL";
            pstm = conn.prepareStatement( pstmStr );
            pstm.setInt   (1, Util.secondsSinceTheEpoch());
            pstm.setInt   (2, logoutCode.getCode());
            pstm.setString(3, email);
            pstm.setString(4, app);
            return pstm.executeUpdate();
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }


    public static int logoutUserBySessionId   (Connection conn, String sessionId, String app, LogoutCode logoutCode) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String pstmStr = "UPDATE subscription.ss_user_sessions SET usrs_logoutsse=?, uslc_id=? WHERE usrs_session=? AND usrs_app=? AND usrs_logoutsse IS NULL";
            pstm = conn.prepareStatement( pstmStr );
            pstm.setInt   (1, Util.secondsSinceTheEpoch());
            pstm.setInt   (2, logoutCode.getCode());
            pstm.setString(3, sessionId);
            pstm.setString(4, app);
            return pstm.executeUpdate();
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }
	
	
    private static void specialLogoutOfDanglingSessions (Connection conn, int usrsId) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String pstmStr = "UPDATE subscription.ss_user_sessions SET usrs_logoutsse=?, uslc_id="+LogoutCode.CONCURRENT.getCode()+" WHERE usrs_id=? AND usrs_logoutsse IS NULL";
            pstm = conn.prepareStatement( pstmStr );
            pstm.setInt   (1, Util.secondsSinceTheEpoch());
            pstm.setInt   (2, usrsId);
            pstm.executeUpdate();
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }


    public static int concurrentLogoutThresholdSecs(Connection conn) throws SQLException {
        int minutes = Integer.valueOf(getPropertyValue(conn, "CONC_LGOUT_THRSHLD_MINS"));
        return minutes*60;
    }

    public static String getPropertyValue(Connection conn, String propKey) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet           rs = null;
        try {
            String stmtStr = "SELECT prop_value FROM subscription.ss_properties WHERE prop_key=?";
            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1, propKey);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            boolean atLeastOneRow  = false;
            String propValue = null;
            while (rs.next()) {
                if (beenHereBefore) throw new RuntimeException();
                atLeastOneRow = true;
                propValue = rs.getString(1);
                beenHereBefore = true;
            }
            return propValue;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    public static boolean appExists(Connection conn, String app) {
        PreparedStatement pstm = null;
        ResultSet           rs = null;
        try {
            String stmtStr = "SELECT count(*) from subscription.ssv_user_modules "+
                             "WHERE modu_name = ?";
            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1,   app);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            boolean atLeastOneRow  = false;
            int n = -1;
            while (rs.next()) {
                if (beenHereBefore) throw new RuntimeException(app);
                atLeastOneRow  = true;
                n  = rs.getInt(1);
                beenHereBefore = true;
            }
            if (!atLeastOneRow) throw new RuntimeException(app);
            return (n > 0);
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    public static boolean emailExists(Connection conn, String email) {
        PreparedStatement pstm = null;
        ResultSet           rs = null;
        try {
            String stmtStr = "SELECT count(*) from subscription.ssv_user_modules "+
                             "WHERE user_email = ?";
            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1,   email);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            boolean atLeastOneRow  = false;
            int n = -1;
            while (rs.next()) {
                if (beenHereBefore) throw new RuntimeException(email);
                atLeastOneRow  = true;
                n  = rs.getInt(1);
                beenHereBefore = true;
            }
            if (!atLeastOneRow) throw new RuntimeException(email);
            return (n > 0);
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    private static boolean appIsFree(Connection conn, String appName) throws UnknownApp{
        PreparedStatement pstm = null;
        ResultSet           rs = null;
        try {
            String pstmStr = "SELECT a.modu_isFree FROM subscription.ss_modules a WHERE a.modu_name = ?";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, appName);
            rs = pstm.executeQuery();
            Boolean rv = null;
            boolean beenHereBefore = false;
            while (rs.next()) {
                if (beenHereBefore) throw new RuntimeException(appName);
                else rv = rs.getBoolean(1);
            }
            if (rv==null)
                throw new UnknownApp(appName);
            else
                return rv;
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    private static int getUserStatus(Connection conn, String email) {
        PreparedStatement pstm = null;
        ResultSet           rs = null;
        try {
            // String pstmStr = "SELECT a.usst_id FROM subscription.ss_users a WHERE a.user_email=?";
            String pstmStr = "SELECT a.usst_id FROM subscription.ssv_gaiaportal_users a WHERE a.user_email=?";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, email);
            rs = pstm.executeQuery();
            
			if (rs.next())
            	return rs.getInt(1);	
            else
            	return 3;	// Διαγραμμένος
			
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    public static void assertSubscriptionOK(Connection conn, String email, String app) throws
                                                                             UserStatusNotActive,
                                                                             SubscriberNotAssociatedWithApp,
                                                                             UserNotAssociatedWithAppAlthoughSubscriptionOK,
                                                                             UserNotAssociatedWithApp,
                                                                             SubscriberStatusNotActive,
                                                                             SubscriberModuleGroupNotActive,
                                                                             SubscriberModuleGroupTemporalMismatch,
                                                                             UnknownApp {
        {
            int userStatus = getUserStatus(conn, email);
            if (appIsFree(conn, app) && (userStatus == 0))
                return;
            else if (userStatus != 0)
                throw new UserStatusNotActive(userStatus);
        }
        PreparedStatement pstm = null;
        ResultSet           rs = null;
        try {
            Timestamp now = new Timestamp(System.currentTimeMillis());
            String stmtStr = "SELECT sust_id, smgs_id, sumg_valid_from, sumg_valid_until, "   +
                             "       subscriber_has_the_module, user_has_the_module, usst_id, subs_short_name, subs_id,sumg_id, mogr_name "+
                             "FROM subscription.ssv_user_modules "                            +
                             "WHERE user_email = ? AND modu_name = ?";
            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1, email);
            pstm.setString     (2,   app);
            rs = pstm.executeQuery();

            boolean atLeastOneRow  = false;

            int sust_id                         = -1;
            int most_id                         = -1;
            Timestamp sumo_valid_from           = null;
            Timestamp sumo_valid_until          = null;
            Boolean   subs_has_the_module       = null;
            Boolean   user_has_the_module       = null;
            int usst_id                         = -1;
            String subs_short_name              = null;
            int subs_id                         = -1;
            Integer sumg_id                     = null;
            String mogr_name                    = null;

            UserMngmtException exceptionToThrow = null;
            while (rs.next()) {
                atLeastOneRow  = true;
                sust_id                = rs.getInt(1);
                most_id                = rs.getInt(2);
                sumo_valid_from  = rs.getTimestamp(3);
                sumo_valid_until = rs.getTimestamp(4);
                subs_has_the_module    = rs.getBoolean(5);
                user_has_the_module    = rs.getBoolean(6);
                usst_id                = rs.getInt(7);
                subs_short_name        = rs.getString(8);
                subs_id                = rs.getInt(9);
                sumg_id                = rs.getInt(10);
                mogr_name              = rs.getString(11);

                //l.info(String.format("subs_short_name, sust_id, most_id, usst_id, subs_has_the_module, user_has_the_module", subs_short_name, sust_id,most_id, usst_id, subs_has_the_module, user_has_the_module));
                try {
                    StatusException.raiseIfNotZero (SubscriberStatusNotActive     .class, sust_id);
                    StatusException.raiseIfNotZero (SubscriberModuleGroupNotActive.class, most_id);
                    if (!subs_has_the_module) throw new SubscriberNotAssociatedWithApp(email, subs_id, subs_short_name, app);
                    SubscriberModuleGroupTemporalMismatch.raiseIfNecessary(subs_short_name, app, sumo_valid_from, sumo_valid_until, now);
                    if (!user_has_the_module) throw new UserNotAssociatedWithAppAlthoughSubscriptionOK(email, app, subs_id, subs_short_name, sumg_id, mogr_name);
                    StatusException.raiseIfNotZero (UserStatusNotActive           .class, usst_id);
                    exceptionToThrow = null;
                    break;
                } catch (UserMngmtException e) {
                    exceptionToThrow  = e;
                    continue;
                }
            }
            if (!atLeastOneRow) {
                // There are three possibilities: (a) the email doesn't exist, (b) the app doesn't exist, (c) the email / app combination doesn't exist
                //                                    althought the email and the app individually exist
                // The logic checks in the order: (b), (c) and (a) as follows:
                // if the app doesn't exist throw UnknownApp (don't test for email existence so as not to help hackers)
                // , else check that both the email and app individually exist, but not their combo in which case throw
                //    UserNotAssociatedWithApp, else that means that the email doesn't exist but don't do anything in that case
                // as the exception will be caught and thrown by the LogingController::login implementation.
                if      (  !appExists(conn, app  ) ) throw new UnknownApp(app);
                else if ( emailExists(conn, email) ) throw new UserNotAssociatedWithApp(email, app);
                else ; // do nothing
            } else if (exceptionToThrow != null) exceptionToThrow.throwSpecificType();
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }


    public static String getUsernameOfLoggedInSession(Connection conn, String sessionId) {
        String            retValue = null;
        PreparedStatement pstm     = null;
        ResultSet         rs       = null;
        try {
            String stmtStr = "SELECT DISTINCT usrs_email FROM subscription.ss_user_sessions WHERE usrs_logoutsse IS NULL AND usrs_ssosession=?";

            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1, sessionId);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            while (rs.next()) {
                if (beenHereBefore) throw new RuntimeException(sessionId);
                retValue = rs.getString(1);
                beenHereBefore = true;
            }
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);
        } finally {
            try { pstm.close(); } catch (SQLException e) { throw new ExceptionAdapter(e); }
            try {   rs.close(); } catch (SQLException e) { throw new ExceptionAdapter(e); }
            return retValue;
        }
    }

    public static boolean userExists(Connection conn, String email) throws SQLException {
        PreparedStatement  pstm = null;
        ResultSet            rs = null;
        try {
            // String stmtStr = "SELECT COUNT(*) FROM subscription.ss_users WHERE user_email=?";
            String stmtStr = "SELECT COUNT(*) FROM subscription.ssv_gaiaportal_users WHERE user_email=?";

            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1, email);
            rs   = pstm.executeQuery();
            boolean beenHereBefore = false;
            int count = -1 ;
            while (rs.next()) {
                if (beenHereBefore) throw new RuntimeException(email);
                count = rs.getInt(1);
                beenHereBefore = true;
            }
            if ( (count != 0) && (count != 1) ) throw new RuntimeException(email+":"+count);
            //l.info(String.format("found %d users with email: %s", count, email));
            return count>0;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }




    private static String nextRandomSalt(SecureRandom random) {
        return new BigInteger(130, random).toString(20);
    }

    private static Pair<String, String> encodePwd(String pwd) {
        random.setSeed(System.currentTimeMillis());
        String salt = nextRandomSalt(random);
        return Pair.create(hash256(pwd, salt, 1000000), salt);
    }

    public static int deleteUserCategories(Connection conn, String email) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String pstmStr ="DELETE FROM subscription.ss_user_user_categories a WHERE a.user_id IN    "+
                            "(SELECT b.user_id FROM subscription.ss_users b WHERE b.user_email=?)     ";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, email);
            int rv = pstm.executeUpdate();
            return rv;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static int addUserCategory(Connection conn, String email, String category, int usoc_order) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String pstmStr =
                "INSERT INTO subscription.ss_user_user_categories         "+
                "(user_id, usca_id, usuc_order)                           "+
                "( SELECT DISTINCT a.user_id, b.usca_id, ? FROM           "+
                "  subscription.ss_users a               ,                "+
                "  subscription.ss_user_categories b                      "+
                "  WHERE a.user_email=? AND b.usca_name=?                 "+
                ")                                                        ";

            pstm = conn.prepareStatement(pstmStr);
            pstm.setInt    (1, usoc_order);
            pstm.setString (2, email     );
            pstm.setString (3, category  );
            int rv = pstm.executeUpdate();
            return rv;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static int insertUser(Connection conn, int status    , int subs_id      , boolean isAdmin  , String email,
                                              String pwd                     , String fname     , String  lname,
                                 String nickname, String vat, String phone  , String phoneCell , String addr  ,
                                              String addrNmbr, String  pstl ,String city      , String prefecture, 
                                 String country  , String comments, Integer portal_id, Integer user_registered_from_id, Boolean user_agree_for_transmission)
                    throws SQLException {
        PreparedStatement pstm = null;
        try {
            Pair<String, String> hashAndSalt = encodePwd(pwd);
            String hash  = hashAndSalt.a;
            String salt = hashAndSalt.b;
            String pstmStr = "INSERT INTO subscription.ss_users("                                 +
                "usst_id, subs_id, user_is_administrator, user_email, "                           +
                "user_password, user_password_salt, user_firstname, user_surname, "               +
                "user_nickname, user_vat, user_phone, user_phone_cell, user_address, "                      +
                "user_address_number, user_postal_code, user_city, user_prefecture, "             +
                "user_country, user_comments, user_portal_id, user_registered_from_id, user_agree_for_transmission, user_registration_date, user_last_login_date)"             +
                "VALUES(?, ?, ?, ?,"       +
                      " ?, ?, ?, ?, ?,"    +
                      " ?, ?, ?, ?,"       +
                      " ?, ?, ?, ?, ?,"    +
                      " ?, ?, ?, ?, now(), NULL)";

            pstm = conn.prepareStatement( pstmStr, Statement.RETURN_GENERATED_KEYS);
            ps_setInteger   (pstm,  1, status     );
            ps_setInteger   (pstm,  2, subs_id    );
            ps_setBoolean   (pstm,  3, isAdmin    );
            pstm.setString  (       4, email      );
            pstm.setString  (       5, hash       );
            pstm.setString  (       6, salt       );
            pstm.setString  (       7, fname      );
            pstm.setString  (       8, lname      );
            pstm.setString  (       9, nickname   );
            pstm.setString  (      10, vat        );
            pstm.setString  (      11, phone      );
            pstm.setString  (      12, phoneCell  );
            pstm.setString  (      13, addr       );
            pstm.setString  (      14, addrNmbr   );
            pstm.setString  (      15, pstl       );
            pstm.setString  (      16, city       );
            pstm.setString  (      17, prefecture );
            pstm.setString  (      18, country    );
            pstm.setString  (      19, comments   );
            ps_setInteger  (pstm, 20, portal_id  );     
            ps_setInteger  (pstm, 21, user_registered_from_id);
            ps_setBoolean  (pstm, 22, user_agree_for_transmission);
            pstm.executeUpdate();

            Integer key = null;
            {
                ResultSet keySet = pstm.getGeneratedKeys();
                boolean beenHereBefore = false;
                while ( keySet.next() ) {
                    if (beenHereBefore) throw new RuntimeException();
                    else {
                        key = keySet.getInt(1);
                        beenHereBefore = true;
                    }
                }
            }
            return key;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    private static String prepareUpdateStmt(String ...names) {
        List<String> columns = new ArrayList<String>();
        for (String name : names) {
            columns.add(String.format("%s = coalesce(?, %s)", name, name));
        }
        return StringUtils.join(columns, ",");
    }

    public static int updateUser(Connection conn, Integer status, Integer subs_id, Boolean isAdmin, String email, String pwd, String fname, String  lname, String nickname, String vat, String phone, String phoneCell, String addr, String addrNmbr, String  pstl, String city, String prefecture, String country, String comments, Integer portal_id, Integer registered_from_id, Boolean agree_for_transmission)
                    throws SQLException {
        return updateUser(conn, UpdateUserOption.BY_EMAIL, status, subs_id, isAdmin, email, pwd, fname, lname,  nickname, vat, phone, phoneCell, addr, addrNmbr, pstl, city, prefecture, country, comments, portal_id, registered_from_id, agree_for_transmission);
    }


    public static int updateUser(Connection conn, UpdateUserOption updateOption, Integer status, Integer subs_id, Boolean isAdmin, String email, String pwd, String fname, String  lname, String nickname, String vat, String phone, String phoneCell, String addr, String addrNmbr, String  pstl, String city, String prefecture, String country, String comments, Integer portal_id, Integer registered_from_id, Boolean agree_for_transmission)
                    throws SQLException {
         PreparedStatement pstm = null;
         
        try {
            String[] columnNames = {"user_firstname", "user_surname", 
                                    "user_nickname", "user_phone", "user_phone_cell", "user_address", "user_address_number", 
                                    "user_postal_code", "user_city", "user_prefecture", "user_country", "user_comments", 
                                    "user_portal_id"};

            String updateStmt = prepareUpdateStmt(columnNames);
            String pstmStr = String.format("UPDATE subscription.ss_users SET %s WHERE %s=?", updateStmt, updateOption.getColumnName());

            pstm = conn.prepareStatement( pstmStr, Statement.RETURN_GENERATED_KEYS);
            int i = 0;
            pstm.setString                      ( ++i, fname)    ;
            pstm.setString                      ( ++i, lname)    ;
            pstm.setString                      ( ++i, nickname) ;
            pstm.setString                      ( ++i, phone)    ;
            pstm.setString                      ( ++i, phoneCell);
            pstm.setString                      ( ++i, addr)     ;
            pstm.setString                      ( ++i, addrNmbr) ;
            pstm.setString                      ( ++i, pstl)     ;
            pstm.setString                      ( ++i, city)     ;
            pstm.setString                      ( ++i, prefecture);
            pstm.setString                      ( ++i, country)  ;
            pstm.setString                      ( ++i, comments) ;
            ps_setInteger                  (pstm, ++i, portal_id);
            switch (updateOption) {
            case BY_EMAIL:     pstm.setString   ( ++i, email);     break;
            case BY_VAT  :     pstm.setString   ( ++i, vat);       break;
            case BY_PORTAL_ID: pstm.setInt      ( ++i, portal_id); break;
            }
            if (i != columnNames.length+1 ) throw new RuntimeException(String.format("%d!=%d", i, columnNames.length));
            pstm.executeUpdate();
            Integer key = null;
            {
                ResultSet keySet = pstm.getGeneratedKeys();
                boolean beenHereBefore = false;
                while ( keySet.next() ) {
                    if (beenHereBefore) throw new RuntimeException();
                    else {
                        key = keySet.getInt(1);
                        beenHereBefore = true;
                    }
                }
            }
            return key;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static String setUserPassword(Connection conn, Integer userId, String pwd, String vat, String email, Integer portal_id) throws SQLException {
        
        if (userId == null){
            Map<String, Object> user = getUserInfo(conn, vat, email, portal_id);
            if (user == null)
                throw new RuntimeException("no user found for "+String.format("vat=%s, email=%s, portal_id=%d", vat, email, portal_id));

            userId = (Integer)user.get("user_id");
        }
        PreparedStatement pstm = null;
        try {
            String pstmStr = "select * from subscription.password_set_aux(?, ?, ?, ?)" ;
            pstm = conn.prepareStatement(pstmStr);
            pstm.setInt      (1, userId);
            pstm.setString   (2, email);
            pstm.setString   (3, pwd);
            pstm.setString   (4, email);

            ResultSet rs = pstm.executeQuery();
            if (rs.next()){
                if (!"OK".equals(rs.getString(1))){
                    return rs.getString(1);
                }
            } 
            else{
                throw new SQLException(email);
            }
        } finally {
                DbUtils.closeQuietly(pstm);
        }        
        return "OK";
    }

    public static Map<String, Object> getUserInfo(Connection conn, String vat, String email, Integer portal_id) throws SQLException {
        String[] columns = {"user_id", "usst_id", "subs_id", "user_is_administrator", "user_email", "user_password", "user_password_salt", "user_firstname", "user_surname", "user_nickname", "user_vat", "user_phone", "user_phone_cell", "user_address", "user_address_number", "user_postal_code", "user_city", "user_prefecture", "user_country", "user_comments", "user_portal_id", "user_registered_from_id", "user_agree_for_transmission"};
        List<String> whereClauses = null;
        {
            whereClauses = new ArrayList<>();
            if (vat       != null) whereClauses.add("user_vat=? AND subs_id=2");
            if (email     != null) whereClauses.add("user_email=?");
            if (portal_id != null) whereClauses.add("user_portal_id=?");
        }
        String pstmStr = String.format("SELECT %s FROM subscription.ssv_gaiaportal_users a WHERE %s", StringUtils.join(columns, ","), StringUtils.join(whereClauses, " AND "));

        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            pstm = conn.prepareStatement( pstmStr );
            int _idx = 0;
            if (vat       != null) pstm.setString (++_idx, vat);
            if (email     != null) pstm.setString (++_idx, email);
            if (portal_id != null) pstm.setInt    (++_idx, portal_id);
            if (_idx != whereClauses.size()) throw new RuntimeException(String.format("%d!=%d", _idx, whereClauses.size()));
            rs = pstm.executeQuery();
            Map<String, Object> rv = new HashMap<>();
            boolean beenHereBefore = false;
            while (rs.next())        
                if (beenHereBefore)
                    throw new RuntimeException("more than one rows for "+String.format("vat=%s, email=%s, portal_id=%d", vat, email, portal_id));
                else {
                    int _i = 0;
                    for (String column : columns)
                        rv.put(column, rs.getObject(++_i));
                    beenHereBefore = true;
                }
            return rv;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static int addSubs (Connection conn, int status, int suca, String short_name, String vat, String legal_name, String tax_office, 
                                         String occupation, String phone, String fax, String address, String address_number, String postal_code, 
                               String city, String prefecture, String country, String email, String web_site, String database, String app_url)
                throws SQLException {
        PreparedStatement pstm = null;
        try {

            String pstmStr = "INSERT INTO subscription.ss_subscribers("                                                    +
                "sust_id        , suca_id        , subs_short_name, subs_vat    , subs_legal_name     , subs_tax_office,"  +
                "subs_occupation, subs_phone     , subs_fax       , subs_address, subs_address_number , subs_postal_code," +
                "subs_city      , subs_prefecture, subs_country   , subs_email  , subs_web_site       , subs_database, subs_application_url)   " +
                "VALUES(?, ?, ?, ?, ?, ?,"+
                       "?, ?, ?, ?, ?, ?,"+
                       "?, ?, ?, ?, ?, ?,?)";


            pstm = conn.prepareStatement( pstmStr );
            int i = 0;
            pstm.setInt    ( ++i, status) ; pstm.setInt  ( ++i, suca)    ; pstm.setString( ++i, short_name) ; pstm.setString(++i, vat); pstm.setString( ++i, legal_name) ; pstm.setString(++i, tax_office);
            pstm.setString    ( ++i, occupation) ; pstm.setString  ( ++i, phone)    ; pstm.setString( ++i, fax) ; pstm.setString( ++i, address) ; pstm.setString(++i, address_number); pstm.setString( ++i, postal_code);
            pstm.setString    ( ++i, city) ; pstm.setString  ( ++i, prefecture)    ; pstm.setString( ++i, country) ; pstm.setString( ++i, email) ; pstm.setString(++i, web_site); pstm.setString (++i, database); pstm.setString(++i,app_url);
            return pstm.executeUpdate();
        } finally {
            DbUtils.closeQuietly(pstm);
        }        
    }

    public static boolean setUserAdmin(Connection conn, String email, String vat) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String pstmStr = "UPDATE subscription.ss_users SET user_is_administrator=true, subs_id = "              +
                             "(SELECT b.subs_id FROM subscription.ss_subscribers b WHERE b.subs_vat = ?)"           +
                             " WHERE user_email = ? "                                                               +
                            " AND (user_is_administrator IS FALSE OR "                                              +
                            " subs_id = (SELECT b.subs_id FROM subscription.ss_subscribers b WHERE b.subs_vat = ?))";
            pstm = conn.prepareStatement( pstmStr );
            pstm.setString(1,   vat);
            pstm.setString(2, email);
            pstm.setString(3,   vat);
            int rowsUpdated = pstm.executeUpdate();
            if (rowsUpdated > 1) throw new RuntimeException(String.format("%d, %s, %s", rowsUpdated, email, vat));
            return pstm.executeUpdate()==1;
        } finally {
            DbUtils.closeQuietly(pstm);
        }        
    }

    public static Pair<String, String> getSubs_DbAppUrl(Connection conn, String email) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet  rs = null;
        try {
            String pstmStr = 
                "SELECT b.subs_database, b.subs_application_url FROM   "+
                "subscription.ss_users       a INNER JOIN              "+
                "subscription.ss_subscribers b ON b.subs_id=a.subs_id  "+
                "WHERE a.user_email = ?                                ";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, email);
            rs = pstm.executeQuery();
            String subs_database        = null;
            String subs_application_url = null;
            boolean beenHereBefore      = false;
            while (rs.next())
                if (beenHereBefore) throw new RuntimeException(email);
                else {
                    subs_database        = rs.getString(1);
                    subs_application_url = rs.getString(2);
                    beenHereBefore = true;
                }
            return Pair.create(subs_database, subs_application_url);
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static List<Triad<String, String, String>> getFreeApps(Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet         rs   = null;
        try {
            String pstmStr = "SELECT a.modu_name, a.modu_ulr_suffix, a.modu_description FROM subscription.ss_modules a WHERE a.modu_isfree IS TRUE";
            pstm = conn.prepareStatement(pstmStr);
            rs = pstm.executeQuery();
            List<Triad<String, String, String>> freeApps = new ArrayList<>();
            while (rs.next())
                freeApps.add(Triad.create(rs.getString(1), rs.getString(2), rs.getString(3)));
            return freeApps;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    public static Pair<String, List<Triad<String, String, String>>> getAppsForEmail(Connection conn, String email) throws SQLException {
        PreparedStatement pstm     = null;
        ResultSet         rs       = null;
        try {
            String pstmStr = "with user_modules " +
                             "   as ( " +
                             "   SELECT b.subs_database, a.modu_name, a.modu_ulr_suffix, a.modu_description, " +
                             "          a.subscriber_has_the_module, a.user_has_the_module, " +
                             "          a.sumg_valid_from, a.sumg_valid_until " +
                             "   FROM subscription.ssv_user_modules a " + 
                             "        JOIN subscription.ss_subscribers b ON b.subs_id = a.subs_id " +
                             "   WHERE a.usst_id in (0,4) AND a.sust_id=0 AND a.smgs_id=0 " +
                             "   AND a.user_email = ?) " +
                             "                          " +
                             " select distinct um.subs_database, um.modu_name, um.modu_ulr_suffix, um.modu_description " +
                             "   from user_modules um " +
                             "  where um.subscriber_has_the_module IS TRUE AND um.user_has_the_module IS TRUE " +
                             "    AND um.sumg_valid_from <= NOW() AND COALESCE (um.sumg_valid_until, NOW()+ INTERVAL '1 DAY') > NOW() ";
                                 
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, email);
            rs = pstm.executeQuery();
            String database    = null;
            String ulr_suffix  = null;
            String description = null;
            List<Triad<String, String, String>> apps = new ArrayList<>();
            while (rs.next()) {
                if (database != null && !database.equals(rs.getString(1)))
                    throw new RuntimeException(email+":"+database+"/"+rs.getString(1));
                database = rs.getString(1);
                apps.add(Triad.create(rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            return Pair.create(database, apps);
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }
    
    
    public static boolean isUserRegistedInModuleBySpecificSubscriber(Connection conn, String userVat, String moduleName, Integer fromSubscriber) throws SQLException {
        PreparedStatement pstm     = null;
        ResultSet         rs       = null;
        try {
            String pstmStr = 
              " select exists ( "+
              " select 1 "+
              " from subscription.ss_user_modules um "+
              "  JOIN subscription.ss_users u ON um.user_id = u.user_id "+
              "  JOIN subscription.ss_modules m ON um.modu_id = m.modu_id "+
              " where "+
              "    u.subs_id = 2 "+
              "    and u.user_vat= ? "+
              "    and (um.usmo_valid_until is null OR um.usmo_valid_until > NOW()) "+
              "    and um.usmo_from_subscriber = ? "+
              "    and m.modu_name = ? "+
              " LIMIT 1 "+
              " ) ";
                    
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, userVat);
            pstm.setInt(2, fromSubscriber);
            pstm.setString(3, moduleName);
            rs = pstm.executeQuery();
            List<Triad<String, String, String>> apps = new ArrayList<>();
            while (rs.next()) {
                return rs.getBoolean(1);
            }
            throw new RuntimeException("isUserRegistedInModuleBySpecificSubscriber did not returned any result");
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }
    
    public static Integer getFromSubsIdForModule(Connection conn, Integer userId, String moduleName) throws SQLException {
        PreparedStatement pstm     = null;
        ResultSet         rs       = null;
        try {
            String pstmStr = 
                 " select um.usmo_from_subscriber " +
                 " from subscription.ss_user_modules um " +
                 " JOIN subscription.ss_modules m ON um.modu_id = m.modu_id " +
                 " where " +
                 " 	um.user_id=  ? " +
                 " 	and (um.usmo_valid_until is null OR um.usmo_valid_until > NOW()) " +
                 " 	and m.modu_name = ? " ;
                    
            pstm = conn.prepareStatement(pstmStr);
            pstm.setInt(1, userId);
            pstm.setString(2, moduleName);
            rs = pstm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            return null;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }
    
    
    
    private static boolean subscriptionIsCurrent(Connection conn, String subs_vat, String mogr_name) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            String pstmStr = "SELECT COUNT(*) FROM     subscription.ss_subscribers_module_groups a                           "+
                             "INNER JOIN               subscription.ss_subscribers               b ON b.subs_id = a.subs_id  "+
                             "INNER JOIN               subscription.ss_module_groups             c ON c.mogr_id = a.mogr_id  "+
                             "WHERE now() >= a.sumg_valid_from AND now() < a.sumg_valid_until AND                            "+
                             "      b.subs_vat=? AND c.mogr_name=?                                                           ";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, subs_vat);
            pstm.setString(2, mogr_name);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            Integer count = null;
            while (rs.next()) {
                if (beenHereBefore) throw new RuntimeException();
                else {
                    beenHereBefore = true;
                    count = rs.getInt(1);
                }
            }
            return (count>1);
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    public static boolean addModuleGroupToSubscriber(Connection conn, String subs_vat, String mogr_name,
                                                 Timestamp sumg_valid_from,
                                                 Timestamp sumg_valid_to, 
                                                 int max_users, int max_customers) throws SQLException {
        if (subscriptionIsCurrent(conn, subs_vat, mogr_name)) return false;
        PreparedStatement pstm = null;
        try {
            String pstmStr = 
                "INSERT INTO subscription.ss_subscribers_module_groups                                               "+
                "(subs_id, mogr_id, smgs_id, sumg_valid_from, sumg_valid_until, sumg_max_users, sumg_max_customers)  "+
                "VALUES ( (SELECT subs_id FROM subscription.ss_subscribers   WHERE subs_vat=?),                      "+
                "         (SELECT mogr_id FROM subscription.ss_module_groups WHERE mogr_name=?),                     "+
                "         1, ?, ?, ?, ?)                                                                             ";

            pstm = conn.prepareStatement(pstmStr);
            int idx=1;
            pstm.setString   (idx++, subs_vat);
            pstm.setString   (idx++, mogr_name);
            pstm.setTimestamp(idx++, sumg_valid_from);
            pstm.setTimestamp(idx++, sumg_valid_to);
            pstm.setInt      (idx++, max_users);
            pstm.setInt      (idx++, max_customers);
            pstm.executeUpdate();
            return true;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static boolean isAdmin(Connection conn, String email) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet         rs   = null;
        try {
            String pstmStr = "SELECT a.user_is_administrator FROM subscription.ss_users a WHERE a.user_email=?";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, email);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            Boolean retValue = null;
            while (rs.next())
                if (beenHereBefore)
                    throw new RuntimeException(email);
                else {
                    retValue = rs.getBoolean(1);
                    beenHereBefore = true;
                }
            return retValue;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    public static boolean existsSsUserModules(Connection conn, int user_id, int modu_id) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet         rs   = null;
        try {
            String pstmStr = "SELECT COUNT(*) FROM subscription.ss_user_modules a "+
                             "WHERE a.user_id = ? AND a.modu_id =?                ";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setInt(1, user_id);
            pstm.setInt(2, modu_id);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            Boolean retValue = null;
            while (rs.next())
                if (beenHereBefore)
                    throw new RuntimeException(String.format("%d, %d", user_id, modu_id));
                else {
                    int _n = rs.getInt(1);
                    retValue = _n > 0 ? true : false;
                    beenHereBefore = true;
                }
            return retValue;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    public static boolean existsSsUserModulesAtDate(Connection conn, int user_id, int modu_id, java.sql.Date theDate) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet         rs   = null;
        try {
            String pstmStr = "SELECT COUNT(*) FROM subscription.ss_user_modules a "+
                             "WHERE a.user_id = ? AND a.modu_id =? AND (a.usmo_valid_until is null OR a.usmo_valid_until >= ?)   ";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setInt(1, user_id);
            pstm.setInt(2, modu_id);
            pstm.setDate(3, theDate);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            Boolean retValue = null;
            while (rs.next())
                if (beenHereBefore)
                    throw new RuntimeException(String.format("%d, %d", user_id, modu_id));
                else {
                    int _n = rs.getInt(1);
                    retValue = _n > 0 ? true : false;
                    beenHereBefore = true;
                }
            return retValue;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }
    
    public static Map<String, Object> getSubsInfo(Connection conn, int subs_id) throws SQLException {
        String[] columns = {"subs_id", "sust_id", "suca_id", "subs_short_name", "subs_vat", "subs_legal_name",
            "subs_tax_office", "subs_occupation", "subs_phone", "subs_fax", "subs_address", "subs_address_number",
            "subs_postal_code", "subs_city", "subs_prefecture", "subs_country", "subs_email", "subs_web_site",
                            "subs_database", "subs_application_url"};
        List<String> whereClauses = null;
        {
            whereClauses = new ArrayList<>();
            whereClauses.add("subs_id=?");
        }
        String pstmStr = String.format("SELECT %s FROM subscription.ss_subscribers WHERE %s",
                                       StringUtils.join(columns, ","),
                                       StringUtils.join(whereClauses, " AND "));
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            pstm = conn.prepareStatement( pstmStr );
            int _idx = 0;
            pstm.setInt    (++_idx, subs_id);
            if (_idx != whereClauses.size()) throw new RuntimeException(String.format("%d!=%d", _idx, whereClauses.size()));
            rs = pstm.executeQuery();
            Map<String, Object> rv = new HashMap<>();
            boolean beenHereBefore = false;
            while (rs.next())        
                if (beenHereBefore)
                    throw new RuntimeException("more than one rows for "+String.format("subs_id=%d", subs_id));
                else {
                    int _i = 0;
                    for (String column : columns)
                        rv.put(column, rs.getObject(++_i));
                    beenHereBefore = true;
                }
            return rv;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static int numOfUsers(Connection conn) throws SQLException {
        String pstmStr = "SELECT num_of_users FROM subscription.ssv_user_subscriptions";
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            pstm = conn.prepareStatement( pstmStr );
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            Integer rv = null;
            while (rs.next())        
                if (beenHereBefore)
                    throw new RuntimeException();
                else {
                    rv = rs.getInt(1);
                    beenHereBefore = true;
                }
            return rv;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static int numOfUserSubscriptions(Connection conn) throws SQLException {
        String pstmStr = "SELECT num_of_user_subscriptions FROM subscription.ssv_user_subscriptions";
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            pstm = conn.prepareStatement( pstmStr );
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            Integer rv = null;
            while (rs.next())        
                if (beenHereBefore)
                    throw new RuntimeException();
                else {
                    rv = rs.getInt(1);
                    beenHereBefore = true;
                }
            return rv;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static Map<String, Integer> numOfUsers2(Connection conn) throws SQLException {
        String pstmStr = "SELECT num_of_users, num_of_user_subscriptions FROM subscription.ssv_user_subscriptions";
        PreparedStatement pstm = null;
        ResultSet rs = null;
        Map<String, Integer> results = new HashMap<>();
        try {
            pstm = conn.prepareStatement(pstmStr);
            rs = pstm.executeQuery();
            if (rs.next()) {
                results.put("num_of_users", rs.getInt("num_of_users"));
                results.put("num_of_user_subscriptions", rs.getInt("num_of_user_subscriptions"));
                return results;
            } 
            else {
                throw new RuntimeException();
            }       
               
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static List<Map<String, String>> getUniqueUsersInDateRange(Connection conn, String subsId1, String subsId2, String fromDate, String toDate) throws SQLException {
        
        String[] columns = {"user_email", "user_firstname", "user_surname", "user_vat", "user_phone", 
                            "user_phone_cell", "user_address", "user_address_number", "user_postal_code", "user_city",
                            "user_prefecture", "user_country", "user_comments", "user_portal_id", "user_registered_from_id",
                            "user_agree_for_transmission", "user_registration_date", "user_last_login_date", "user_invoice_type",
                            "user_identity_number", "subs_registered_from_id", "user_payment_method", "dteinsert", "usrinsert",
                            "dteupdate", "usrupdate"};
        
        String sql = String.format("SELECT %s " +
                                   "FROM subscription.ss_users usr " + 
                                   "WHERE usr.user_id IN ( " +
                                            "SELECT distinct(mdl.user_id) " +
                                            "FROM subscription.ss_user_modules mdl " + 
                                            "LEFT JOIN subscription.ss_subscribers subs " + 
                                            "ON mdl.usmo_from_subscriber = subs.subs_id " + 
                                        //    "WHERE subs.subs_vat = '%s' " +
                                            "WHERE subs.subs_ID IN (%s,%s) " +
                                            "AND mdl.usmo_registration_date > '%s' " +
                                            "AND mdl.usmo_registration_date < '%s' " +
                                            "AND mdl.user_id NOT IN( " +
                                                    "SELECT distinct(mdl2.user_id) " +
                                                    "FROM subscription.ss_user_modules mdl2 " + 
                                                    "LEFT JOIN subscription.ss_subscribers subs2 " + 
                                                    "ON mdl2.usmo_from_subscriber = subs2.subs_id " + 
                                                //    "WHERE subs2.subs_vat = '%s' " +
                                                    "WHERE subs2.subs_id IN (%s,%s) " + 
                                                    "AND mdl2.usmo_registration_date <= '%s' " +
                                            ") " +
                                    ")", StringUtils.join(columns, ","), subsId1, subsId2, fromDate, toDate, subsId1, subsId2, fromDate);

        // l.info(sql);
        
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            pstm = conn.prepareStatement(sql);
            rs = pstm.executeQuery();

            List<Map<String, String>> results = new ArrayList<>();

            Object columnValue = null;

            while (rs.next()) {
                Map<String, String> user = new HashMap<String, String>();
                for (String column : columns) {
                    columnValue = rs.getObject(column);
                    user.put(column, columnValue == null ? "" : columnValue.toString());
                }
                results.add(user);
            }

            return results;
        } 
        finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static List<Map<String, String>> getUpdatedUsersInDateRange(Connection conn, String fromDate, String toDate) throws SQLException {
        
        String[] columns = {"user_email", "user_firstname", "user_surname", "user_vat", "user_phone", 
                            "user_phone_cell", "user_address", "user_address_number", "user_postal_code", "user_city",
                            "user_prefecture", "user_country", "user_comments", "user_portal_id", "user_registered_from_id",
                            "user_agree_for_transmission", "user_registration_date", "user_last_login_date", "user_invoice_type",
                            "user_identity_number", "subs_registered_from_id", "user_payment_method", "dteinsert", "usrinsert",
                            "dteupdate", "usrupdate"};

        String sql = String.format("SELECT %s " + 
                                    "FROM subscription.ss_users usr " +  
                                    "WHERE usr.dteupdate > '%s' " + 
                                    "AND usr.dteupdate < '%s'", StringUtils.join(columns, ","), fromDate, toDate);


        // l.info(sql);

        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            pstm = conn.prepareStatement(sql);
            rs = pstm.executeQuery();

            List<Map<String, String>> results = new ArrayList<>();
            
            Object columnValue = null;

            while (rs.next()) {
                Map<String, String> user = new HashMap<String, String>();
                for (String column : columns) {
                    columnValue = rs.getObject(column);
                    user.put(column, columnValue == null ? "" : columnValue.toString());
                }
                results.add(user);
            }
            return results;
        } 
        finally {
            DbUtils.closeQuietly(pstm);
        }

    }
    
    public static int numOfUserModules(Connection conn) throws SQLException {
        String pstmStr = "SELECT COUNT(usmo_id) FROM subscription.ss_user_modules";
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            pstm = conn.prepareStatement( pstmStr );
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            Integer rv = null;
            while (rs.next())        
                if (beenHereBefore)
                    throw new RuntimeException();
                else {
                    rv = rs.getInt(1);
                    beenHereBefore = true;
                }
            return rv;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static List<Map<String, String>> subsInfo(Connection conn, String whereClause) throws SQLException {
        String[] columns = {"subs_id", "subs_short_name"};
        String pstmStr = String.format("SELECT %s FROM subscription.ss_subscribers WHERE %s"
                                       ,StringUtils.join(columns, ",")
                                       ,whereClause);
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            pstm = conn.prepareStatement( pstmStr );
            rs = pstm.executeQuery();
            List<Map<String, String>> rv = new ArrayList<>();
            while (rs.next()) {
                Map<String, String> aSubsInfo = new HashMap<>();
                int _i = 0;
                for (String column : columns) {
                    Object o = rs.getObject(++ _i);
                    if (o != null)
                        aSubsInfo.put(column, o.toString());
                }
                rv.add(aSubsInfo);
            }
            return rv;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }

    public static List<Map<String, String>> subsUsersInfo(Connection conn, int subs_id) throws SQLException {
        String[] columns = {"user_id", "user_firstname", "user_surname"};
        String pstmStr = String.format("SELECT %s FROM subscription.ss_users WHERE subs_id=?"
                                       ,StringUtils.join(columns, ","));
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            pstm = conn.prepareStatement( pstmStr );
            pstm.setInt(1, subs_id);
            rs = pstm.executeQuery();
            List<Map<String, String>> rv = new ArrayList<>();
            while (rs.next()) {
                Map<String, String> aUsersInfo = new HashMap<>();
                int _i = 0;
                for (String column : columns) {
                    Object o = rs.getObject(++ _i);
                    if (o != null)
                        aUsersInfo.put(column, o.toString());
                }
                rv.add(aUsersInfo);
            }
            return rv;
        } finally {
            DbUtils.closeQuietly(pstm);
        }
    }
    
    public static boolean securityClassPermitted(Connection conn, String user_email, String modu_name, String sypr_name) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet         rs   = null;
        try {
            String pstmStr = "SELECT 1 FROM subscription.ssv_user_privilleges           "+
                             "WHERE user_email=? AND modu_name=? AND sypr_name=? LIMIT 1";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, user_email);
            pstm.setString(2, modu_name);
            pstm.setString(3, sypr_name);
            rs = pstm.executeQuery();
            Boolean rv = false;
            boolean beenHereBefore = false;        
            while (rs.next())
                if (beenHereBefore) throw new RuntimeException();
                else {
                    rv = true;
                    beenHereBefore = true;
                }
            return rv;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }
    
    public static List<String> securityClassPermissions(Connection conn, String user_email, String modu_name) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet         rs   = null;
        try {
            String pstmStr = "SELECT sypr_name FROM subscription.ssv_user_privilleges   "+
                             "WHERE user_email=? AND modu_name=?                        ";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, user_email);
            pstm.setString(2, modu_name);
            rs = pstm.executeQuery();
            List<String> rv = new ArrayList<>();
            while (rs.next())
                rv.add(rs.getString(1));
            return rv;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
    }

    public static Pair<Integer, String> subsInvitation_invite(Connection conn, String email, int subs_id, int secsGiven) throws SQLException {
        Integer prevSubs_id    = null;
        {
            PreparedStatement pstm = null;
            ResultSet         rs   = null;
            try {
                String pstmStr = "SELECT subs_id FROM subscription.ss_users "+
                                 "WHERE user_email=?                        ";
                pstm = conn.prepareStatement(pstmStr);
                pstm.setString(1, email);
                rs = pstm.executeQuery();
                boolean rowFound       = false;
                boolean beenHereBefore = false;
                while (rs.next())
                    if (beenHereBefore) throw new SQLException(email+" "+subs_id);
                    else {
                        rowFound       = true;
                        beenHereBefore = true;
                        prevSubs_id    = rs.getInt(1);
                    }
                if (!rowFound) return Pair.create(null, "EMAIL_NOT_FOUND");
                if ((prevSubs_id!=null) && (prevSubs_id == subs_id))
                    return Pair.create(null, "ALREADY_BOUND_TO_THAT_SUBS_ID");
            } finally {
                DbUtils.closeQuietly(null, pstm, rs);
            }
        }
        {
            PreparedStatement pstm = null;
            try {
                String guid = Util.secureRandomAlphanumString(120);
                Timestamp valid_until = new Timestamp( (new java.util.Date()).getTime() + secsGiven*1000 );
                String pstmStr = 
                "INSERT INTO subscription.ss_subscriber_invitations                                         "+
                "(subs_id, suin_guid, suin_email, suin_valid_until, suin_consumed_when, suin_accepted_when) "+
                "VALUES (?, ?       , ?         , ?               , NULL              , NULL)               ";
                pstm = conn.prepareStatement(pstmStr);
                pstm.setInt      (1, subs_id);
                pstm.setString   (2, guid);
                pstm.setString   (3, email);
                pstm.setTimestamp(4, valid_until);
                int rowsAffected = pstm.executeUpdate();
                if (rowsAffected!=1) throw new SQLException(email+" "+subs_id);
                return Pair.create(prevSubs_id, null);
            } finally {
                DbUtils.closeQuietly(null, pstm, null);
            }
        }
    }

    public static List<Triad<String, Integer, Timestamp>>  subsInvitations_inquire(Connection conn, String email) throws SQLException {
        List<Triad<String, Integer, Timestamp>> rv = null;
        {
            PreparedStatement pstm = null;
            ResultSet         rs   = null;
            try {
                String pstmStr =
    "SELECT suin_guid, subs_id, suin_valid_until FROM               "+
    "subscription.ss_subscriber_invitations                         "+
    "WHERE suin_consumed_when IS NULL                               "+
    "AND suin_email=?                                               ";
                pstm = conn.prepareStatement(pstmStr);
                pstm.setString(1, email);
                rs = pstm.executeQuery();
                rv = new ArrayList<>();
                while (rs.next())
                    rv.add(Triad.create(rs.getString(1), rs.getInt(2), rs.getTimestamp(3)));
            } finally {
                DbUtils.closeQuietly(null, pstm, rs);
            }
        }
        {
            PreparedStatement pstm = null;
            try {
                String pstmStr =
    "UPDATE subscription.ss_subscriber_invitations         "+
    "SET suin_consumed_when = NOW()                        "+
    "WHERE suin_consumed_when IS NULL AND suin_email=?     ";
                pstm = conn.prepareStatement(pstmStr);
                pstm.setString(1, email);
                pstm.executeUpdate();
            } finally {
                DbUtils.closeQuietly(null, pstm, null);
            }
        }
        return rv;
    }

    private static void setInvitationAccepted(Connection conn, String guid) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String pstmStr =
"UPDATE subscription.ss_subscriber_invitations         "+
"SET suin_accepted_when = NOW()                        "+
"WHERE suin_guid=?                                     ";
            pstm = conn.prepareStatement(pstmStr);
            pstm.setString(1, guid);
            pstm.executeUpdate();
        } finally {
            DbUtils.closeQuietly(null, pstm, null);
        }
    }

    public static String subsInvitations_accept(Connection conn, String guid) throws SQLException {
        final Timestamp NOW  = new Timestamp((new java.util.Date()).getTime());
        boolean foundInvitation= false;
        Integer subs_id              = null;
        String  email                = null;
        Timestamp suin_valid_until   = null;
        Timestamp suin_consumed_when = null;
        Timestamp suin_accepted_when = null;
        {
            PreparedStatement pstm = null;
            ResultSet         rs   = null;
            try {
                String pstmStr = 
                "SELECT subs_id, suin_email, suin_valid_until, suin_consumed_when, suin_accepted_when FROM  "+
                "subscription.ss_subscriber_invitations                                                     "+
                "WHERE suin_guid=?                                                                          ";
                pstm = conn.prepareStatement(pstmStr);
                pstm.setString(1, guid);
                rs = pstm.executeQuery();
                boolean beenHereBefore = false;
                while (rs.next())
                    if (beenHereBefore) throw new SQLException(guid);
                    else {
                        foundInvitation = true;
                        beenHereBefore = true;
                        subs_id = rs.getInt(1);
                        if (rs.wasNull())
                            subs_id = null;
                        email = rs.getString(2);
                        suin_valid_until   = rs.getTimestamp(3);
                        suin_consumed_when = rs.getTimestamp(4);
                        suin_accepted_when = rs.getTimestamp(5);
                    }
            } finally {
                DbUtils.closeQuietly(null, pstm, rs);
            }
        }
        if (!foundInvitation) return "GUID_NOT_FOUND";
        if (subs_id == null) throw new SQLException (guid);
        else if (NOW.after(suin_valid_until)) return "EXPIRED";
        else if (suin_consumed_when == null)  return "NOT_CONSUMED";
        else if (suin_accepted_when != null)  return "ALREADY_ACCEPTED";
        else {
            updateUser(conn, (Integer) null, subs_id, (Boolean) null, email, (String) null, (String) null, (String)  null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Integer) null, (Integer) null, (Boolean) null);
            setInvitationAccepted(conn, guid);
            return null;
        }
    }

    public static String activateUser(Connection conn, String email, String pwd, String ucode) throws SQLException {
        Integer user_id = null;
        int usst_id = -1;
        String usuc_ucode = "0";
        {
            PreparedStatement pstm = null;
            ResultSet         rs   = null;
            try {
                String pstmStr = 
                "select usr.user_id, usr.usst_id, COALESCE(usuc.usuc_ucode, '0') usuc_ucode " +
                "  from subscription.ssv_gaiaportal_users usr " +
                "  	left join user_registration.ur_users_ucodes usuc " +
                "           on usuc.user_id = usr.user_id and usuc.usuc_activeflag is true " +
                " where usr.subs_id = 2 " +
                "   and usr.user_email = ?";
                pstm = conn.prepareStatement(pstmStr);
                pstm.setString(1, email);
                rs = pstm.executeQuery();
                boolean beenHereBefore = false;
                while (rs.next()){
                    if (beenHereBefore) throw new SQLException(email);
                    else {
                        beenHereBefore = true;
                        user_id = rs.getInt(1);
                        usst_id = rs.getInt(2);
                        usuc_ucode = rs.getString(3);
                    }
                }
                if (user_id == null || !ucode.equals(usuc_ucode)){
                    return "INVALID_CREDENTIALS"; // Τα στοιχεία που δώσατε δεν είναι έγκυρα!
                }
                if (usst_id != 0 && usst_id != 4){
                    return "BLOCKED_USER"; // O λογαριασμός είναι μπλοκαρισμένος ή διαγραμμένος!
                }
            } finally {
                DbUtils.closeQuietly(null, pstm, rs);
            }
        }
        {
            PreparedStatement pstm = null;
            try {
                
                String passwordSet = setUserPassword(conn, user_id, pwd, null, email, null);
                
                if (!"OK".equals(passwordSet)){
                    return passwordSet;
                }

                if (usst_id == 4) {
                    String pstmStr = 
                        "update subscription.ss_users usr " +
                        "   set usr.usst_id = 0, " +
                        "       usr.dteupdate = now(), " +
                        "       usr.usrupdate = ? " +
                        " where usr.user_id = ?";
                    pstm = conn.prepareStatement(pstmStr);
                    pstm.setString   (1, email);
                    pstm.setInt      (2, user_id);
                    int rowsAffected = pstm.executeUpdate();
                    if (rowsAffected!=1) throw new SQLException(email);
                }
            } finally {
                    DbUtils.closeQuietly(null, pstm, null);
            }
        }
        return "USER_ACTIVATED"; // Ο κωδικός ενημερώθηκε με επιτυχία.;
    }
    
}