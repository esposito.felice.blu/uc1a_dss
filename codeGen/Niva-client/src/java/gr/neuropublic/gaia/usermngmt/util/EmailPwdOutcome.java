package gr.neuropublic.gaia.usermngmt.util;


public enum EmailPwdOutcome {
    ACCOUNT_NOT_EXISTS, CORRECT_PWD, WRONG_PWD;
}