package gr.neuropublic.gaia.usermngmt.util;

public class UserNotAssociatedWithAppAlthoughSubscriptionOK extends UserMngmtException {

    private String user_email;
    private String app;
    private int subs_id;
    private String subs_short_name;
    private int sumg_id;
    private String mogr_name;

    public UserNotAssociatedWithAppAlthoughSubscriptionOK(String user_email, String app, int subs_id, String subs_short_name, int sumg_id, String mogr_name) {
        this.user_email = user_email;
        this.app = app;
        this.subs_id = subs_id;
        this.subs_short_name = subs_short_name;
        this.sumg_id = sumg_id;
        this.mogr_name = mogr_name;
    }

    public String toString() {
        return String.format("%s(%s,%s,%d,%s,%d,%s)", this.getClass().getName(), user_email, app, subs_id, subs_short_name, sumg_id, mogr_name);
    }

}