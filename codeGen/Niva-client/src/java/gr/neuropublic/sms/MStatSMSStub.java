package gr.neuropublic.sms;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MStatSMSStub implements SMSInterface {

    private final String AUTH_KEY;// = "xQ4jENvTGNifb4ypKzHNHLOavEwH5EDs";
    private static final String SMS_URL = "http://corpsms-api.m-stat.gr/http/sms.php";
    private static final String DLR_URL = "http://corpsms-api.m-stat.gr/http/dlr.php";

    public MStatSMSStub(final String AUTH_KEY) {
        this.AUTH_KEY = AUTH_KEY;
    }
    
    
    public Map<String,SMSResultsContainer> sendSMS_custome_key(String message, String senderText, Map<String, String> idsMap, SMSInterface.ENCODING ENC) {

        if (message == null || senderText == null || idsMap == null || idsMap.size() == 0) {
            throw new IllegalArgumentException("Illegal argument in sendSMS() message =" + message + " senderText =" + senderText + " receiverNumbers[] =" + idsMap);
        } else if(senderText.length() > 11){
            throw new IllegalArgumentException("Illegal argument in sendSMS() senderText must be up to 11 alphanumeric characters");
        } else if (idsMap.size() > 300) {
            throw new IllegalArgumentException("Illegal argument in sendSMS() receiverNumbers must not exceed 300");
        }

        
        HttpURLConnection connection = null;
        Map<String, SMSResultsContainer> resultsMap = new LinkedHashMap<>();

        try {

            //Send post
            connection = sendPostRequest(message, senderText, ENC, idsMap);

            //Get Response
            receiveResponse(connection, resultsMap, idsMap);

        } catch ( Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception in sendSMS() caused by :"+e.getMessage());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return resultsMap;
    }
    

    /**
     * Send SMS to receiverNumbers, id are created by this method.
     *
     * @param message The text message to be sent
     * @param senderText The sender displayed in receivers
     * @param receiverNumbers The phone numbers of receivers
     * @param ENC encoding of SMS message
     * @return Map of Phone Numbers as keys and SMSResultsContainer as values
     */
    public Map<String,SMSResultsContainer> sendSMS(String message, String senderText, String[] receiverNumbers, SMSInterface.ENCODING ENC) {

        argumentChecking(message, senderText, receiverNumbers);

        HttpURLConnection connection = null;
        Map<String, SMSResultsContainer> resultsMap = new LinkedHashMap<>();

        try {
            Map<String, String> idsMap = createIds(receiverNumbers);//Map<Id, ReceiverPhone>

            //Send post
            connection = sendPostRequest(message, senderText, ENC, idsMap);

            //Get Response
            receiveResponse(connection, resultsMap, idsMap);

        } catch ( Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception in sendSMS() caused by :"+e.getMessage());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return resultsMap;
    }

    /**
     * Send SMS to predefined id,Number pairs defined in idsMap
     *
     * @param message The text message to be sent
     * @param senderText The sender displayed in receivers
     * @param idsMap LinkedHashMap<Id,Number>  Id is the key, Phone Number is the value, we use LinkedHashMap to ensure ordering
     * @param ENC  encoding of SMS message
     * @return  Map of Phone Numbers as keys and SMSResultsContainer as values
     */
    public Map<String,SMSResultsContainer> sendSMS(String message, String senderText, LinkedHashMap<String, String> idsMap, SMSInterface.ENCODING ENC) {

        argumentChecking(message, senderText, idsMap);

        HttpURLConnection connection = null;
        Map<String, SMSResultsContainer> resultsMap = new LinkedHashMap<>();

        try {
            //Send post
            connection = sendPostRequest(message, senderText, ENC, idsMap);

            //Get Response
            receiveResponse(connection, resultsMap, idsMap);

        } catch ( Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception in sendSMS() caused by :"+e.getMessage());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return resultsMap;
    }

    private void receiveResponse(HttpURLConnection connection, Map<String, SMSResultsContainer> resultsMap, Map<String, String> idsMap) throws IOException {

        InputStream is = connection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = rd.readLine()) != null) {

            String[] parts = line.split(";");
            resultsMap.put(idsMap.get(parts[1]), new SMSResultsContainer(parts[0], parts[1], Float.parseFloat(parts[2])));
        }
        rd.close();
    }

    private HttpURLConnection sendPostRequest(String message, String senderText, ENCODING ENC, Map<String, String> idsMap) throws IOException {

        HttpURLConnection connection;
        URL url = new URL(SMS_URL);
        connection = (HttpURLConnection) url.openConnection();
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("charset", "utf-8");
        connection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");

        String urlParameters = addAuthKey().
                                concat(addIds(idsMap)).
                                concat(addFrom(senderText)).
                                concat(addTo(idsMap)).
                                concat(addMessage(message)).
                                concat(addCoding(ENC));

        connection.setRequestProperty( "Content-Length", Integer.toString(urlParameters.getBytes(StandardCharsets.UTF_8).length));
        //Send request
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.close();
        return connection;
    }

    /**
     * Request a Delivery Receipt for specific ids
     *
     * @param receiverIds the ids
     * @return list of results
     */
    public List<DLRResultsContainer> sendDLR(String[] receiverIds) {

        dlrArgumentChecking(receiverIds);

        HttpURLConnection connection = null;
        List<DLRResultsContainer> resultsList = new ArrayList<>();

        try {

            URL url = new URL(DLR_URL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");

            String urlParameters = addAuthKey().concat(addIds(receiverIds));

            connection.setRequestProperty( "Content-Length", Integer.toString(urlParameters.getBytes(StandardCharsets.UTF_8).length));
            //Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = rd.readLine()) != null) {

                String[] parts = line.split(";");
                resultsList.add(new DLRResultsContainer(parts[0], parts[1], parts[2], (parts.length > 3 ? parts[3] : null)));
            }
            rd.close();

        } catch ( Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception in sendDLR() caused by :"+e.getMessage());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return resultsList;
    }

    private String addIds(String[] receiverIds) throws UnsupportedEncodingException {

        StringBuilder ids = new StringBuilder("&id=");
        boolean firstFlag = true;

        for (String currentId : receiverIds) {
            if (!firstFlag) {
                ids.append(URLEncoder.encode(",","UTF-8"));
            }
            ids.append(URLEncoder.encode(currentId,"UTF-8"));
            firstFlag = false;
        }

        return ids.toString();
    }

    private void dlrArgumentChecking(String[] receiverIds) {

        if ( receiverIds == null || receiverIds.length == 0) {
            throw new IllegalArgumentException("Illegal argument in sendSMS() receiverIds[] =" + receiverIds);
        }
    }

    private String addCoding(SMSInterface.ENCODING enc) throws UnsupportedEncodingException {
        return  "&coding="+URLEncoder.encode(enc.getCode().toString(), "UTF-8");
    }

    private void argumentChecking(String message, String senderText, String[] receiverNumbers) {

        if (message == null || senderText == null || receiverNumbers == null || receiverNumbers.length == 0) {
            throw new IllegalArgumentException("Illegal argument in sendSMS() message =" + message + " senderText =" + senderText + " receiverNumbers[] =" + receiverNumbers);
        } else if(senderText.length() > 11){
            throw new IllegalArgumentException("Illegal argument in sendSMS() senderText must be up to 11 alphanumeric characters");
        } else if (receiverNumbers.length > 300) {
            throw new IllegalArgumentException("Illegal argument in sendSMS() receiverNumbers must not exceed 300");
        }
    }

    private void argumentChecking(String message, String senderText, Map<String, String> idsMap) {

        if (message == null || senderText == null || idsMap == null || idsMap.size() == 0) {
            throw new IllegalArgumentException("Illegal argument in sendSMS() message =" + message + " senderText =" + senderText + " idsMap[] =" + idsMap);
        } else if(senderText.length() > 11){
            throw new IllegalArgumentException("Illegal argument in sendSMS() senderText must be up to 11 alphanumeric characters");
        } else if (idsMap.size() > 300) {
            throw new IllegalArgumentException("Illegal argument in sendSMS() idsMap must not exceed 300");
        }
    }

    private String addMessage(String message) throws UnsupportedEncodingException {
        return "&text="+ URLEncoder.encode(message, "UTF-8");
    }

    private String addTo(Map<String, String> idsMap) throws UnsupportedEncodingException {

        StringBuilder tos = new StringBuilder("&to=");
        boolean firstFlag = true;

        for (String currentPhoneNumber : idsMap.values()) {
            if (!firstFlag) {
                tos.append(URLEncoder.encode(",", "UTF-8"));
            }
            tos.append(URLEncoder.encode(currentPhoneNumber, "UTF-8"));
            firstFlag = false;
        }

        return tos.toString();

    }

    private String addFrom(String sender) throws UnsupportedEncodingException {
        return "&from=" + URLEncoder.encode(sender, "UTF-8");
    }

    /**
     * Returns for each phone number a unique id
     *
     * @param phoneNumbers
     * @return  Map<Id, ReceiverPhone>
     */
    private Map<String,String> createIds(String[] phoneNumbers) {

        Map<String, String> idsMap = new LinkedHashMap<>();//to ensure ordering
        Calendar rightNow = Calendar.getInstance();
        int day = rightNow.get(Calendar.DAY_OF_MONTH);
        int month = rightNow.get(Calendar.MONTH);
        int year = rightNow.get(Calendar.YEAR);
        int hour = rightNow.get(Calendar.HOUR_OF_DAY);
        int minute = rightNow.get(Calendar.MINUTE);
        int second = rightNow.get(Calendar.SECOND);

        for (String currentPhone : phoneNumbers) {
            idsMap.put( currentPhone.substring(8) + day + month + year + hour + minute + second, currentPhone);
        }

        return idsMap;
    }

    private String addAuthKey() throws UnsupportedEncodingException {

        if (this.AUTH_KEY == null) {
            throw new IllegalArgumentException(" AUTH_KEY is null");
        }

        return "auth_key="+URLEncoder.encode(this.AUTH_KEY, "UTF-8");
    }

    private String addIds(Map<String,String> idsMap) throws UnsupportedEncodingException {

        StringBuilder ids = new StringBuilder("&id=");
        boolean firstFlag = true;

        for (String currentId : idsMap.keySet()) {
            if (!firstFlag) {
                ids.append(URLEncoder.encode(",", "UTF-8"));
            }
            ids.append(URLEncoder.encode(currentId, "UTF-8"));
            firstFlag = false;
        }

        return ids.toString();
    }
}
