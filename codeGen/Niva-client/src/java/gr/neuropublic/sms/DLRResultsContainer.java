package gr.neuropublic.sms;

public class DLRResultsContainer {

    private String acknowledgement;
    private String id;
    private String dlr;
    private String desc;

    public DLRResultsContainer(String acknowledgement, String id, String dlr, String desc) {

        this.acknowledgement = acknowledgement;
        this.id = id;
        this.dlr = dlr;
        this.desc = desc;
    }

    public String getAcknowledgement() {
        return acknowledgement;
    }

    public String getId() {
        return id;
    }

    public String getDlr() {
        return dlr;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return " ACK :"+this.getAcknowledgement()+" id :"+this.getId()+" dlr :"+this.getDlr()+" description :"+getDesc();
    }
}
