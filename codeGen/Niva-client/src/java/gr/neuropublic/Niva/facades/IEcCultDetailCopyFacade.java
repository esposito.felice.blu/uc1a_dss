//5DD6BD2B027FD2FDE1F11778C995EFAE
package gr.neuropublic.Niva.facades;
import gr.neuropublic.Niva.facadesBase.IEcCultDetailCopyBaseFacade;
import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IEcCultDetailCopyFacade extends IEcCultDetailCopyBaseFacade {
    @Local
    public interface ILocal extends IEcCultDetailCopyFacade {}

    //@Remote
    //public interface IRemote extends IEcCultDetailCopyFacade {}


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
