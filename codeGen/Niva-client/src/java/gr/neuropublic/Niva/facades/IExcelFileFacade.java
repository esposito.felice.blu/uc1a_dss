//5A140922C7A1C767F5913A5A211D0377
package gr.neuropublic.Niva.facades;
import gr.neuropublic.Niva.facadesBase.IExcelFileBaseFacade;
import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IExcelFileFacade extends IExcelFileBaseFacade {
    @Local
    public interface ILocal extends IExcelFileFacade {}

    //@Remote
    //public interface IRemote extends IExcelFileFacade {}


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
