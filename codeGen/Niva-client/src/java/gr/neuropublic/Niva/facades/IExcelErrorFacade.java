//B3FA5B3DCC9B79A677804696C2FFE923
package gr.neuropublic.Niva.facades;
import gr.neuropublic.Niva.facadesBase.IExcelErrorBaseFacade;
import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IExcelErrorFacade extends IExcelErrorBaseFacade {
    @Local
    public interface ILocal extends IExcelErrorFacade {}

    //@Remote
    //public interface IRemote extends IExcelErrorFacade {}


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
