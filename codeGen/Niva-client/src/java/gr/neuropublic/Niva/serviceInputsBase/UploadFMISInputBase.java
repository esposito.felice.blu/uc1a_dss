package gr.neuropublic.Niva.serviceInputsBase;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.Date;

public class UploadFMISInputBase {
        
    public String fmisUid;
    public Integer id;
    public String attachment;
    public String metadata;

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
