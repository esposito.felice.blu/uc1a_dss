//64369319526707294C9C407874FD698F
package gr.neuropublic.Niva.services;
import java.util.List;

public class UserSession extends gr.neuropublic.Niva.servicesBase.UserSessionBase {
    public UserSession(
            final Integer userId, 
            final String usrEmail,
            final String userVat,
            final Integer subsId,
            final String subsCode,
            final String subsDescription,
            final Integer fromSubsId,
            final List<String> privileges,
            final List<Integer> subSecClasses,
            final String userSalt,
            final String clientIp) {
        super(userId, usrEmail, userVat, subsId, subsCode, subsDescription, fromSubsId, privileges, subSecClasses, userSalt, clientIp );
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
