//C84E937253D3669C43A04675770C8739
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.DecisionMakingBase;



@Entity
@Table(name = "decision_making", schema="niva" )
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class DecisionMaking  extends DecisionMakingBase {

    public DecisionMaking() {
        super();
    }
    public DecisionMaking(Integer demaId) {
        super(demaId);
    }
    public DecisionMaking(Integer demaId, String description, Date dateTime, Integer rowVersion, Integer recordtype, Boolean hasBeenRun, Boolean hasPopulatedIntegratedDecisionsAndIssues) {
        super(demaId, description, dateTime, rowVersion, recordtype, hasBeenRun, hasPopulatedIntegratedDecisionsAndIssues);
    }
    public DecisionMaking(String description, Date dateTime, Integer rowVersion, Integer recordtype, Boolean hasBeenRun, Boolean hasPopulatedIntegratedDecisionsAndIssues, Classification clasId, EcGroup ecgrId) {
        super(description, dateTime, rowVersion, recordtype, hasBeenRun, hasPopulatedIntegratedDecisionsAndIssues, clasId, ecgrId);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
