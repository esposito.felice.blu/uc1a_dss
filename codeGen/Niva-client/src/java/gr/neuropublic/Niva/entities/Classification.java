//3262A7320371B061FFA951277EC70BCC
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.ClassificationBase;



@Entity
@Table(name = "classification", schema="niva" )
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class Classification  extends ClassificationBase {

    public Classification() {
        super();
    }
    public Classification(Integer clasId) {
        super(clasId);
    }
    public Classification(Integer clasId, String name, String description, Date dateTime, Integer rowVersion, Integer recordtype, String filePath, byte[] attachedFile, Short year, Boolean isImported) {
        super(clasId, name, description, dateTime, rowVersion, recordtype, filePath, attachedFile, year, isImported);
    }
    public Classification(String name, String description, Date dateTime, Integer rowVersion, Integer recordtype, String filePath, byte[] attachedFile, Short year, Boolean isImported, Classifier clfrId, FileTemplate fiteId) {
        super(name, description, dateTime, rowVersion, recordtype, filePath, attachedFile, year, isImported, clfrId, fiteId);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
