//9859BF4AA26D8FC86E7C10B58F363BAF
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.GpRequestsContextsBase;



@Entity
@Table(name = "gp_requests_contexts", schema="niva" ,  
    uniqueConstraints={
        @UniqueConstraint(columnNames={"hash"})
    })
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class GpRequestsContexts  extends GpRequestsContextsBase {

    public GpRequestsContexts() {
        super();
    }
    public GpRequestsContexts(Integer gpRequestsContextsId) {
        super(gpRequestsContextsId);
    }
    public GpRequestsContexts(Integer gpRequestsContextsId, String type, String label, String comment, String geomHexewkb, String referencepoint, Integer rowVersion, String hash) {
        super(gpRequestsContextsId, type, label, comment, geomHexewkb, referencepoint, rowVersion, hash);
    }
    public GpRequestsContexts(String type, String label, String comment, String geomHexewkb, String referencepoint, Integer rowVersion, String hash, GpRequestsProducers gpRequestsProducersId, ParcelsIssues parcelsIssuesId) {
        super(type, label, comment, geomHexewkb, referencepoint, rowVersion, hash, gpRequestsProducersId, parcelsIssuesId);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
