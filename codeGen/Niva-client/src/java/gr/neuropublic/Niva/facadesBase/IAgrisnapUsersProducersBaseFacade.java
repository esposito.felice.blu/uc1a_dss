package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IAgrisnapUsersProducersBaseFacade extends IFacade<AgrisnapUsersProducers> {
/*
    @Local
    public interface ILocal extends IAgrisnapUsersProducersBaseFacade {}

    //@Remote
    //public interface IRemote extends IAgrisnapUsersProducersBaseFacade {}
*/
    public AgrisnapUsersProducers initRow();

    public void getTransientFields(List<AgrisnapUsersProducers> entities);

    public AgrisnapUsersProducers createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IAgrisnapUsersFacade.ILocal agrisnapUsersFacade, gr.neuropublic.Niva.facades.IProducersFacade.ILocal producersFacade, Set<String> allowedFields);

    public List<AgrisnapUsersProducers> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public AgrisnapUsersProducers findByAgrisnapUsersId_AgrisnapUsersProducersId(AgrisnapUsers agrisnapUsersId, Integer agrisnapUsersProducersId);

    public AgrisnapUsersProducers findByAgrisnapUsersId_AgrisnapUsersProducersId(Integer agrisnapUsersId, Integer agrisnapUsersProducersId);

    public int delByAgrisnapUsersId_AgrisnapUsersProducersId(AgrisnapUsers agrisnapUsersId, Integer agrisnapUsersProducersId);

    public int delByAgrisnapUsersId_AgrisnapUsersProducersId(Integer agrisnapUsersId, Integer agrisnapUsersProducersId);

    public AgrisnapUsersProducers findByAgrisnapUsersProducersId(Integer agrisnapUsersProducersId);

    public int delByAgrisnapUsersProducersId(Integer agrisnapUsersProducersId);

    public List<AgrisnapUsersProducers> findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers(Integer agrisnapUsersId_agrisnapUsersId, Date fsch_dteRelCreated, Date fsch_dteRelCanceled, Integer fsch_producersId_producersId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<AgrisnapUsersProducers> findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers(boolean noTransient, Integer agrisnapUsersId_agrisnapUsersId, Date fsch_dteRelCreated, Date fsch_dteRelCanceled, Integer fsch_producersId_producersId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers_count(Integer agrisnapUsersId_agrisnapUsersId, Date fsch_dteRelCreated, Date fsch_dteRelCanceled, Integer fsch_producersId_producersId, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
