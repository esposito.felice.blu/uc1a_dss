package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IEcCultCopyBaseFacade extends IFacade<EcCultCopy> {
/*
    @Local
    public interface ILocal extends IEcCultCopyBaseFacade {}

    //@Remote
    //public interface IRemote extends IEcCultCopyBaseFacade {}
*/
    public EcCultCopy initRow();

    public void getTransientFields(List<EcCultCopy> entities);

    public EcCultCopy createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.ICultivationFacade.ILocal cultivationFacade, gr.neuropublic.Niva.facades.IEcGroupCopyFacade.ILocal ecGroupCopyFacade, gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade, gr.neuropublic.Niva.facades.ISuperClasFacade.ILocal superClasFacade, Set<String> allowedFields);

    public List<EcCultCopy> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public EcCultCopy findByEcccId(Integer ecccId);

    public int delByEcccId(Integer ecccId);

    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingGrpEcCultCopy(Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingGrpEcCultCopy(boolean noTransient, Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DecisionMakingGrpEcCultCopy_count(Integer ecgcId_ecgcId, List<String> excludedEntities);


    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingEcCultCopyCover(Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingEcCultCopyCover(boolean noTransient, Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DecisionMakingEcCultCopyCover_count(Integer ecgcId_ecgcId, List<String> excludedEntities);


    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingEcCultCopySuper(Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingEcCultCopySuper(boolean noTransient, Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DecisionMakingEcCultCopySuper_count(Integer ecgcId_ecgcId, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
