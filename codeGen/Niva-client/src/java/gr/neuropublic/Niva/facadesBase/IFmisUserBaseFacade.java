package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IFmisUserBaseFacade extends IFacade<FmisUser> {
/*
    @Local
    public interface ILocal extends IFmisUserBaseFacade {}

    //@Remote
    //public interface IRemote extends IFmisUserBaseFacade {}
*/
    public FmisUser initRow();

    public void getTransientFields(List<FmisUser> entities);

    public FmisUser createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, Set<String> allowedFields);

    public List<FmisUser> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public FmisUser findByFmisUsersId(Integer fmisUsersId);

    public int delByFmisUsersId(Integer fmisUsersId);

    public List<FmisUser> findLazyFmisUser(String fmisName, String fmisUid, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<FmisUser> findLazyFmisUser(boolean noTransient, String fmisName, String fmisUid, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyFmisUser_count(String fmisName, String fmisUid, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
