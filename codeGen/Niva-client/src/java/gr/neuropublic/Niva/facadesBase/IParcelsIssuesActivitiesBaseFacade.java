package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IParcelsIssuesActivitiesBaseFacade extends IFacade<ParcelsIssuesActivities> {
/*
    @Local
    public interface ILocal extends IParcelsIssuesActivitiesBaseFacade {}

    //@Remote
    //public interface IRemote extends IParcelsIssuesActivitiesBaseFacade {}
*/
    public ParcelsIssuesActivities initRow();

    public void getTransientFields(List<ParcelsIssuesActivities> entities);

    public ParcelsIssuesActivities createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal parcelsIssuesFacade, Set<String> allowedFields);

    public List<ParcelsIssuesActivities> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public ParcelsIssuesActivities findByParcelsIssuesActivitiesId(Integer parcelsIssuesActivitiesId);

    public int delByParcelsIssuesActivitiesId(Integer parcelsIssuesActivitiesId);

    public List<ParcelsIssuesActivities> findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities(Integer parcelsIssuesId_parcelsIssuesId, Short fsch_type, Date fsch_dteTimestamp, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelsIssuesActivities> findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, Short fsch_type, Date fsch_dteTimestamp, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities_count(Integer parcelsIssuesId_parcelsIssuesId, Short fsch_type, Date fsch_dteTimestamp, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
