package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IFmisUploadBaseFacade extends IFacade<FmisUpload> {
/*
    @Local
    public interface ILocal extends IFmisUploadBaseFacade {}

    //@Remote
    //public interface IRemote extends IFmisUploadBaseFacade {}
*/
    public FmisUpload initRow();

    public void getTransientFields(List<FmisUpload> entities);

    public FmisUpload createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal parcelsIssuesFacade, Set<String> allowedFields);

    public List<FmisUpload> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public FmisUpload findByFmisUploadsId(Integer fmisUploadsId);

    public int delByFmisUploadsId(Integer fmisUploadsId);

    public List<FmisUpload> findAllByCriteriaRange_ParcelFMISGrpFmisUpload(Integer parcelsIssuesId_parcelsIssuesId, Date fsch_dteUpload, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<FmisUpload> findAllByCriteriaRange_ParcelFMISGrpFmisUpload(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, Date fsch_dteUpload, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ParcelFMISGrpFmisUpload_count(Integer parcelsIssuesId_parcelsIssuesId, Date fsch_dteUpload, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
