package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IAgencyBaseFacade extends IFacade<Agency> {
/*
    @Local
    public interface ILocal extends IAgencyBaseFacade {}

    //@Remote
    //public interface IRemote extends IAgencyBaseFacade {}
*/
    public Agency initRow();

    public void getTransientFields(List<Agency> entities);

    public Agency createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, Set<String> allowedFields);

    public List<Agency> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public Agency findByName(String name);

    public int delByName(String name);

    public Agency findByAgenId(Integer agenId);

    public int delByAgenId(Integer agenId);

    public List<Agency> findAllByCriteriaRange_AgencyGrpAgency(String fsch_name, String fsch_country, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Agency> findAllByCriteriaRange_AgencyGrpAgency(boolean noTransient, String fsch_name, String fsch_country, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_AgencyGrpAgency_count(String fsch_name, String fsch_country, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
