package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IParcelClasBaseFacade extends IFacade<ParcelClas> {
/*
    @Local
    public interface ILocal extends IParcelClasBaseFacade {}

    //@Remote
    //public interface IRemote extends IParcelClasBaseFacade {}
*/
    public ParcelClas initRow();

    public void getTransientFields(List<ParcelClas> entities);

    public ParcelClas createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IClassificationFacade.ILocal classificationFacade, gr.neuropublic.Niva.facades.ICultivationFacade.ILocal cultivationFacade, gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade, Set<String> allowedFields);

    public List<ParcelClas> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public ParcelClas findByPclaId(Integer pclaId);

    public int delByPclaId(Integer pclaId);

    public List<ParcelClas> findLazyParcelGP(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelClas> findLazyParcelGP(boolean noTransient, Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyParcelGP_count(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, List<String> excludedEntities);


    public List<ParcelClas> findLazyDashboard(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelClas> findLazyDashboard(boolean noTransient, Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyDashboard_count(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, List<String> excludedEntities);


    public List<ParcelClas> findLazyParcelFMIS(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelClas> findLazyParcelFMIS(boolean noTransient, Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyParcelFMIS_count(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, List<String> excludedEntities);


    public List<ParcelClas> findAllByCriteriaRange_ClassificationGrpParcelClas(Integer clasId_clasId, String fsch_parcIdentifier, String fsch_parcCode, Integer fsch_prodCode, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelClas> findAllByCriteriaRange_ClassificationGrpParcelClas(boolean noTransient, Integer clasId_clasId, String fsch_parcIdentifier, String fsch_parcCode, Integer fsch_prodCode, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ClassificationGrpParcelClas_count(Integer clasId_clasId, String fsch_parcIdentifier, String fsch_parcCode, Integer fsch_prodCode, List<String> excludedEntities);


    public List<ParcelClas> findAllByCriteriaRange_forLov(Double fsch_ParcelClasLov_probPred, Double fsch_ParcelClasLov_probPred2, Integer fsch_ParcelClasLov_prodCode, String fsch_ParcelClasLov_parcIdentifier, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelClas> findAllByCriteriaRange_forLov(boolean noTransient, Double fsch_ParcelClasLov_probPred, Double fsch_ParcelClasLov_probPred2, Integer fsch_ParcelClasLov_prodCode, String fsch_ParcelClasLov_parcIdentifier, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_forLov_count(Double fsch_ParcelClasLov_probPred, Double fsch_ParcelClasLov_probPred2, Integer fsch_ParcelClasLov_prodCode, String fsch_ParcelClasLov_parcIdentifier, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
