package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IParcelDecisionBaseFacade extends IFacade<ParcelDecision> {
/*
    @Local
    public interface ILocal extends IParcelDecisionBaseFacade {}

    //@Remote
    //public interface IRemote extends IParcelDecisionBaseFacade {}
*/
    public ParcelDecision initRow();

    public void getTransientFields(List<ParcelDecision> entities);

    public ParcelDecision createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IDecisionMakingFacade.ILocal decisionMakingFacade, gr.neuropublic.Niva.facades.IParcelClasFacade.ILocal parcelClasFacade, Set<String> allowedFields);

    public List<ParcelDecision> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public ParcelDecision findByPadeId(Integer padeId);

    public int delByPadeId(Integer padeId);

    public List<ParcelDecision> findAllByCriteriaRange_DecisionMakingGrpParcelDecision(Integer demaId_demaId, Integer fsch_decisionLight, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelDecision> findAllByCriteriaRange_DecisionMakingGrpParcelDecision(boolean noTransient, Integer demaId_demaId, Integer fsch_decisionLight, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DecisionMakingGrpParcelDecision_count(Integer demaId_demaId, Integer fsch_decisionLight, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, List<String> excludedEntities);


    public List<ParcelDecision> findAllByCriteriaRange_DashboardGrpParcelDecision(Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelDecision> findAllByCriteriaRange_DashboardGrpParcelDecision(boolean noTransient, Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DashboardGrpParcelDecision_count(Integer pclaId_pclaId, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
