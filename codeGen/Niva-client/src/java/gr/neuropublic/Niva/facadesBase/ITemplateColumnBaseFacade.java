package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface ITemplateColumnBaseFacade extends IFacade<TemplateColumn> {
/*
    @Local
    public interface ILocal extends ITemplateColumnBaseFacade {}

    //@Remote
    //public interface IRemote extends ITemplateColumnBaseFacade {}
*/
    public TemplateColumn initRow();

    public void getTransientFields(List<TemplateColumn> entities);

    public TemplateColumn createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IFileTemplateFacade.ILocal fileTemplateFacade, gr.neuropublic.Niva.facades.IPredefColFacade.ILocal predefColFacade, Set<String> allowedFields);

    public List<TemplateColumn> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public TemplateColumn findByFiteId_PrcoId(FileTemplate fiteId, PredefCol prcoId);

    public TemplateColumn findByFiteId_PrcoId(Integer fiteId, Integer prcoId);

    public int delByFiteId_PrcoId(FileTemplate fiteId, PredefCol prcoId);

    public int delByFiteId_PrcoId(Integer fiteId, Integer prcoId);

    public TemplateColumn findByTecoId(Integer tecoId);

    public int delByTecoId(Integer tecoId);

    public List<TemplateColumn> findAllByCriteriaRange_FileTemplateGrpTemplateColumn(Integer fiteId_fiteId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<TemplateColumn> findAllByCriteriaRange_FileTemplateGrpTemplateColumn(boolean noTransient, Integer fiteId_fiteId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_FileTemplateGrpTemplateColumn_count(Integer fiteId_fiteId, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
