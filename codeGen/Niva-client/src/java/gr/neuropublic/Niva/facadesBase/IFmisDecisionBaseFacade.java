package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IFmisDecisionBaseFacade extends IFacade<FmisDecision> {
/*
    @Local
    public interface ILocal extends IFmisDecisionBaseFacade {}

    //@Remote
    //public interface IRemote extends IFmisDecisionBaseFacade {}
*/
    public FmisDecision initRow();

    public void getTransientFields(List<FmisDecision> entities);

    public FmisDecision createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal parcelsIssuesFacade, gr.neuropublic.Niva.facades.ICultivationFacade.ILocal cultivationFacade, gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade, Set<String> allowedFields);

    public List<FmisDecision> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public FmisDecision findByFmisDecisionsId(Integer fmisDecisionsId);

    public int delByFmisDecisionsId(Integer fmisDecisionsId);

    public List<FmisDecision> findAllByCriteriaRange_DashboardGrpFmisDecision(Integer parcelsIssuesId_parcelsIssuesId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<FmisDecision> findAllByCriteriaRange_DashboardGrpFmisDecision(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DashboardGrpFmisDecision_count(Integer parcelsIssuesId_parcelsIssuesId, List<String> excludedEntities);


    public List<FmisDecision> findAllByCriteriaRange_ParcelFMISGrpFmisDecision(Integer parcelsIssuesId_parcelsIssuesId, Integer fsch_cultId_cultId, Integer fsch_cotyId_cotyId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<FmisDecision> findAllByCriteriaRange_ParcelFMISGrpFmisDecision(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, Integer fsch_cultId_cultId, Integer fsch_cotyId_cotyId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ParcelFMISGrpFmisDecision_count(Integer parcelsIssuesId_parcelsIssuesId, Integer fsch_cultId_cultId, Integer fsch_cotyId_cotyId, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
