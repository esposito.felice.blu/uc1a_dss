package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IGpRequestsContextsBaseFacade extends IFacade<GpRequestsContexts> {
/*
    @Local
    public interface ILocal extends IGpRequestsContextsBaseFacade {}

    //@Remote
    //public interface IRemote extends IGpRequestsContextsBaseFacade {}
*/
    public GpRequestsContexts initRow();

    public void getTransientFields(List<GpRequestsContexts> entities);

    public GpRequestsContexts createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IGpRequestsProducersFacade.ILocal gpRequestsProducersFacade, gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal parcelsIssuesFacade, Set<String> allowedFields);

    public List<GpRequestsContexts> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public GpRequestsContexts findByHash(String hash);

    public int delByHash(String hash);

    public GpRequestsContexts findByGpRequestsContextsId(Integer gpRequestsContextsId);

    public int delByGpRequestsContextsId(Integer gpRequestsContextsId);

    public List<GpRequestsContexts> findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts(Integer parcelsIssuesId_parcelsIssuesId, String fsch_hash, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<GpRequestsContexts> findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, String fsch_hash, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts_count(Integer parcelsIssuesId_parcelsIssuesId, String fsch_hash, List<String> excludedEntities);


    public List<GpRequestsContexts> findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts(Integer parcelsIssuesId_parcelsIssuesId, String fsch_type, String fsch_label, String fsch_comment, String fsch_geomHexewkb, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<GpRequestsContexts> findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, String fsch_type, String fsch_label, String fsch_comment, String fsch_geomHexewkb, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts_count(Integer parcelsIssuesId_parcelsIssuesId, String fsch_type, String fsch_label, String fsch_comment, String fsch_geomHexewkb, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
