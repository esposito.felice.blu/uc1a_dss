package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IProducersBaseFacade extends IFacade<Producers> {
/*
    @Local
    public interface ILocal extends IProducersBaseFacade {}

    //@Remote
    //public interface IRemote extends IProducersBaseFacade {}
*/
    public Producers initRow();

    public void getTransientFields(List<Producers> entities);

    public Producers createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, Set<String> allowedFields);

    public List<Producers> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public Producers findByProducersId(Integer producersId);

    public int delByProducersId(Integer producersId);

    public List<Producers> findLazyProducers(String prodName, Integer prodCode, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Producers> findLazyProducers(boolean noTransient, String prodName, Integer prodCode, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyProducers_count(String prodName, Integer prodCode, List<String> excludedEntities);


    public List<Producers> findAllByCriteriaRange_forLov(String fsch_ProducersLov_prodName, Integer fsch_ProducersLov_prodCode, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Producers> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_ProducersLov_prodName, Integer fsch_ProducersLov_prodCode, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_forLov_count(String fsch_ProducersLov_prodName, Integer fsch_ProducersLov_prodCode, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
