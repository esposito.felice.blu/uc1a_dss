/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
classifier


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class ClassifierBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'clfr_id' είναι υποχρεωτικό")
    @Column(name = "clfr_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="classifier_clfr_id")
    //@SequenceGenerator(name="classifier_clfr_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer clfrId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "name")
    protected String name;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'description' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "description")
    protected String description;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "clfrId", orphanRemoval=false)
    @OneToMany(mappedBy = "clfrId")
    protected List<Classification> classificationCollection = new ArrayList<Classification>();



    public ClassifierBase() {
    }

    public ClassifierBase(Integer clfrId) {
        this.clfrId = clfrId;
    }

    public ClassifierBase(Integer clfrId, String name, String description, Integer rowVersion) {
        this.clfrId = clfrId;
        this.name = name;
        this.description = description;
        this.rowVersion = rowVersion;
    }

    public ClassifierBase(String name, String description, Integer rowVersion) {
        this.name = name;
        this.description = description;
        this.rowVersion = rowVersion;
    }

    public Classifier clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "Classifier:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (Classifier)alreadyCloned.get(key);

        Classifier clone = new Classifier();
        alreadyCloned.put(key, clone);

        clone.setClfrId(getClfrId());
        clone.setName(getName());
        clone.setDescription(getDescription());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setClassificationCollection(getClassificationCollection());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "Classifier:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(clfrId) : clfrId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (clfrId != null) {
            sb.append("\"clfrId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(clfrId) : clfrId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"clfrId\":"); sb.append("null");sb.append(",");
        }
        if (name != null) {
            sb.append("\"name\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(name)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"name\":"); sb.append("null");sb.append(",");
        }
        if (description != null) {
            sb.append("\"description\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(description)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"description\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getClfrId() {
        return clfrId;
    }

    public void setClfrId(Integer clfrId) {
        this.clfrId = clfrId;
    }
    /*
    classifier name
    */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
    classifier description
    */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    @XmlTransient
    public List<Classification> getClassificationCollection() {
        return classificationCollection;
    }

    public void setClassificationCollection(List<Classification> classificationCollection) {
        this.classificationCollection = classificationCollection;
    }

    public void removeClassificationCollection(Classification child) {
        if (child != null)
            child.setClfrId(null);
    }

    public void addClassificationCollection(Classification child) {
        if (child != null)
            child.setClfrId((Classifier)this);
    }

    void internalRemoveClassificationCollection(Classification child) {
        if (child != null)
            this.classificationCollection.remove(child);
    }

    void internalAddClassificationCollection(Classification child) {
        if (child != null)
            this.classificationCollection.add(child);
    }



    @Override
    public int hashCode() {
        return clfrId.hashCode();
    }

    public String getRowKey() {
        return clfrId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return clfrId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(clfrId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.Classifier[ clfrId=" + clfrId + " ]";
    }

    @Override
    public String getEntityName() {
        return "Classifier";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the clfrId fields are not set
        if (!(object instanceof ClassifierBase)) {
            return false;
        }
        ClassifierBase other = (ClassifierBase) object;
        if (this.clfrId == null || other.clfrId == null) 
            return false;
        
        return this.clfrId.equals(other.clfrId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
