/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
classification of parcel


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "ParcelClas.findParcelGP", query = "SELECT x FROM ParcelClas x WHERE (x.prodCode = :prodCode) AND (x.parcIdentifier like :parcIdentifier) AND (x.parcCode like :parcCode) AND (x.clasId.clasId = :clasId_clasId) ORDER BY x.parcIdentifier "),
@NamedQuery(name = "ParcelClas.findDashboard", query = "SELECT x FROM ParcelClas x WHERE (x.prodCode = :prodCode) AND (x.parcIdentifier like :parcIdentifier) AND (x.parcCode like :parcCode) AND (x.clasId.clasId = :clasId_clasId) ORDER BY x.parcIdentifier, x.parcCode "),
@NamedQuery(name = "ParcelClas.findParcelFMIS", query = "SELECT x FROM ParcelClas x WHERE (x.prodCode = :prodCode) AND (x.parcIdentifier like :parcIdentifier) AND (x.parcCode like :parcCode) AND (x.clasId.clasId = :clasId_clasId) ORDER BY x.parcIdentifier, x.parcCode ")})
public abstract class ParcelClasBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Field 'pcla_id' is mandatory")
    @Column(name = "pcla_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="parcel_class_pcla_id")
    //@SequenceGenerator(name="parcel_class_pcla_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer pclaId;


    @Basic(optional = true)
    @Column(name = "prob_pred")
    protected Double probPred;


    @Basic(optional = true)
    @Column(name = "prob_pred2")
    protected Double probPred2;


    @Basic(optional = true)
    @Column(name = "prod_code")
    protected Integer prodCode;



    @Basic(optional = false)
    @NotNull(message="Field 'parc_identifier' is mandatory")
    @Size(min = 1, max = 2147483647, message="Field 'parc_identifier' must have a value between 1 and 2147483647.")
    @Column(name = "parc_identifier")
    protected String parcIdentifier;



    @Basic(optional = false)
    @NotNull(message="Field 'parc_code' is mandatory")
    @Size(min = 1, max = 2147483647, message="Field 'parc_code' must have a value between 1 and 2147483647.")
    @Column(name = "parc_code")
    protected String parcCode;


    @Basic(optional = true)
    @Column(name = "geom4326")
    @Type(type = "org.hibernate.spatial.GeometryType")
    protected Geometry geom4326;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "pclaId", orphanRemoval=false)
    @OneToMany(mappedBy = "pclaId")
    protected List<Integrateddecision> integrateddecisionCollection = new ArrayList<Integrateddecision>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "pclaId", orphanRemoval=false)
    @OneToMany(mappedBy = "pclaId")
    protected List<ParcelDecision> parcelDecisionCollection = new ArrayList<ParcelDecision>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "pclaId", orphanRemoval=false)
    @OneToMany(mappedBy = "pclaId")
    protected List<ParcelsIssues> parcelsIssuesCollection = new ArrayList<ParcelsIssues>();


    @JoinColumn(name = "clas_id", referencedColumnName = "clas_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Field 'clas_id' is mandatory")
    protected Classification clasId;

    public boolean isClasIdPresent() {
        if (clasId==null || clasId.clasId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "cult_id_decl", referencedColumnName = "cult_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected Cultivation cultIdDecl;

    public boolean isCultIdDeclPresent() {
        if (cultIdDecl==null || cultIdDecl.cultId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "cult_id_pred", referencedColumnName = "cult_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected Cultivation cultIdPred;

    public boolean isCultIdPredPresent() {
        if (cultIdPred==null || cultIdPred.cultId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "coty_id_decl", referencedColumnName = "coty_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected CoverType cotyIdDecl;

    public boolean isCotyIdDeclPresent() {
        if (cotyIdDecl==null || cotyIdDecl.cotyId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "coty_id_pred", referencedColumnName = "coty_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected CoverType cotyIdPred;

    public boolean isCotyIdPredPresent() {
        if (cotyIdPred==null || cotyIdPred.cotyId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "cult_id_pred2", referencedColumnName = "cult_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected Cultivation cultIdPred2;

    public boolean isCultIdPred2Present() {
        if (cultIdPred2==null || cultIdPred2.cultId == null)
            return false;
        return true;
    }



    public ParcelClasBase() {
    }

    public ParcelClasBase(Integer pclaId) {
        this.pclaId = pclaId;
    }

    public ParcelClasBase(Integer pclaId, Double probPred, Double probPred2, Integer prodCode, String parcIdentifier, String parcCode, Geometry geom4326) {
        this.pclaId = pclaId;
        this.probPred = probPred;
        this.probPred2 = probPred2;
        this.prodCode = prodCode;
        this.parcIdentifier = parcIdentifier;
        this.parcCode = parcCode;
        this.geom4326 = geom4326;
    }

    public ParcelClasBase(Double probPred, Double probPred2, Integer prodCode, String parcIdentifier, String parcCode, Geometry geom4326, Classification clasId, Cultivation cultIdDecl, Cultivation cultIdPred, CoverType cotyIdDecl, CoverType cotyIdPred, Cultivation cultIdPred2) {
        this.probPred = probPred;
        this.probPred2 = probPred2;
        this.prodCode = prodCode;
        this.parcIdentifier = parcIdentifier;
        this.parcCode = parcCode;
        this.geom4326 = geom4326;
        this.clasId = clasId;
        this.cultIdDecl = cultIdDecl;
        this.cultIdPred = cultIdPred;
        this.cotyIdDecl = cotyIdDecl;
        this.cotyIdPred = cotyIdPred;
        this.cultIdPred2 = cultIdPred2;
    }

    public ParcelClas clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "ParcelClas:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (ParcelClas)alreadyCloned.get(key);

        ParcelClas clone = new ParcelClas();
        alreadyCloned.put(key, clone);

        clone.setPclaId(getPclaId());
        clone.setProbPred(getProbPred());
        clone.setProbPred2(getProbPred2());
        clone.setProdCode(getProdCode());
        clone.setParcIdentifier(getParcIdentifier());
        clone.setParcCode(getParcCode());
        clone.setGeom4326(getGeom4326());
        clone.setIntegrateddecisionCollection(getIntegrateddecisionCollection());
        clone.setParcelDecisionCollection(getParcelDecisionCollection());
        clone.setParcelsIssuesCollection(getParcelsIssuesCollection());
        if (clasId == null || clasId.clasId == null) {
            clone.setClasId(clasId);
        } else {
            clone.setClasId(getClasId().clone(alreadyCloned));
        }
        if (cultIdDecl == null || cultIdDecl.cultId == null) {
            clone.setCultIdDecl(cultIdDecl);
        } else {
            clone.setCultIdDecl(getCultIdDecl().clone(alreadyCloned));
        }
        if (cultIdPred == null || cultIdPred.cultId == null) {
            clone.setCultIdPred(cultIdPred);
        } else {
            clone.setCultIdPred(getCultIdPred().clone(alreadyCloned));
        }
        if (cotyIdDecl == null || cotyIdDecl.cotyId == null) {
            clone.setCotyIdDecl(cotyIdDecl);
        } else {
            clone.setCotyIdDecl(getCotyIdDecl().clone(alreadyCloned));
        }
        if (cotyIdPred == null || cotyIdPred.cotyId == null) {
            clone.setCotyIdPred(cotyIdPred);
        } else {
            clone.setCotyIdPred(getCotyIdPred().clone(alreadyCloned));
        }
        if (cultIdPred2 == null || cultIdPred2.cultId == null) {
            clone.setCultIdPred2(cultIdPred2);
        } else {
            clone.setCultIdPred2(getCultIdPred2().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "ParcelClas:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(pclaId) : pclaId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (pclaId != null) {
            sb.append("\"pclaId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(pclaId) : pclaId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"pclaId\":"); sb.append("null");sb.append(",");
        }
        if (probPred != null) {
            sb.append("\"probPred\":"); sb.append(probPred);sb.append(",");
        } else {
            sb.append("\"probPred\":"); sb.append("null");sb.append(",");
        }
        if (probPred2 != null) {
            sb.append("\"probPred2\":"); sb.append(probPred2);sb.append(",");
        } else {
            sb.append("\"probPred2\":"); sb.append("null");sb.append(",");
        }
        if (prodCode != null) {
            sb.append("\"prodCode\":"); sb.append(prodCode);sb.append(",");
        } else {
            sb.append("\"prodCode\":"); sb.append("null");sb.append(",");
        }
        if (parcIdentifier != null) {
            sb.append("\"parcIdentifier\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(parcIdentifier)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"parcIdentifier\":"); sb.append("null");sb.append(",");
        }
        if (parcCode != null) {
            sb.append("\"parcCode\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(parcCode)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"parcCode\":"); sb.append("null");sb.append(",");
        }
        if (geom4326 != null) {
            sb.append("\"geom4326\":"); GeometryJSON gjson = new GeometryJSON(6); sb.append(gjson.toString(geom4326)); sb.append(",");
        } else {
            sb.append("\"geom4326\":"); sb.append("null");sb.append(",");
        }
        if (clasId != null && clasId.clasId != null) {
            sb.append("\"clasId\":"); sb.append(getClasId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (clasId == null) {
            sb.append("\"clasId\":"); sb.append("null");  sb.append(",");
        }
        if (cultIdDecl != null && cultIdDecl.cultId != null) {
            sb.append("\"cultIdDecl\":"); sb.append(getCultIdDecl().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cultIdDecl == null) {
            sb.append("\"cultIdDecl\":"); sb.append("null");  sb.append(",");
        }
        if (cultIdPred != null && cultIdPred.cultId != null) {
            sb.append("\"cultIdPred\":"); sb.append(getCultIdPred().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cultIdPred == null) {
            sb.append("\"cultIdPred\":"); sb.append("null");  sb.append(",");
        }
        if (cotyIdDecl != null && cotyIdDecl.cotyId != null) {
            sb.append("\"cotyIdDecl\":"); sb.append(getCotyIdDecl().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cotyIdDecl == null) {
            sb.append("\"cotyIdDecl\":"); sb.append("null");  sb.append(",");
        }
        if (cotyIdPred != null && cotyIdPred.cotyId != null) {
            sb.append("\"cotyIdPred\":"); sb.append(getCotyIdPred().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cotyIdPred == null) {
            sb.append("\"cotyIdPred\":"); sb.append("null");  sb.append(",");
        }
        if (cultIdPred2 != null && cultIdPred2.cultId != null) {
            sb.append("\"cultIdPred2\":"); sb.append(getCultIdPred2().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cultIdPred2 == null) {
            sb.append("\"cultIdPred2\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getPclaId() {
        return pclaId;
    }

    public void setPclaId(Integer pclaId) {
        this.pclaId = pclaId;
    }
    /*
    probability predicted
    */
    public Double getProbPred() {
        return probPred;
    }

    public void setProbPred(Double probPred) {
        this.probPred = probPred;
    }
    /*
    */
    public Double getProbPred2() {
        return probPred2;
    }

    public void setProbPred2(Double probPred2) {
        this.probPred2 = probPred2;
    }
    /*
    */
    public Integer getProdCode() {
        return prodCode;
    }

    public void setProdCode(Integer prodCode) {
        this.prodCode = prodCode;
    }
    /*
    */
    public String getParcIdentifier() {
        return parcIdentifier;
    }

    public void setParcIdentifier(String parcIdentifier) {
        this.parcIdentifier = parcIdentifier;
    }
    /*
    Parcel''s code as String.
    */
    public String getParcCode() {
        return parcCode;
    }

    public void setParcCode(String parcCode) {
        this.parcCode = parcCode;
    }
    /*
    */
    public Geometry getGeom4326() {
        return geom4326;
    }

    public void setGeom4326(Geometry geom4326) {
        this.geom4326 = geom4326;
    }
    @XmlTransient
    public List<Integrateddecision> getIntegrateddecisionCollection() {
        return integrateddecisionCollection;
    }

    public void setIntegrateddecisionCollection(List<Integrateddecision> integrateddecisionCollection) {
        this.integrateddecisionCollection = integrateddecisionCollection;
    }

    public void removeIntegrateddecisionCollection(Integrateddecision child) {
        if (child != null)
            child.setPclaId(null);
    }

    public void addIntegrateddecisionCollection(Integrateddecision child) {
        if (child != null)
            child.setPclaId((ParcelClas)this);
    }

    void internalRemoveIntegrateddecisionCollection(Integrateddecision child) {
        if (child != null)
            this.integrateddecisionCollection.remove(child);
    }

    void internalAddIntegrateddecisionCollection(Integrateddecision child) {
        if (child != null)
            this.integrateddecisionCollection.add(child);
    }

    @XmlTransient
    public List<ParcelDecision> getParcelDecisionCollection() {
        return parcelDecisionCollection;
    }

    public void setParcelDecisionCollection(List<ParcelDecision> parcelDecisionCollection) {
        this.parcelDecisionCollection = parcelDecisionCollection;
    }

    public void removeParcelDecisionCollection(ParcelDecision child) {
        if (child != null)
            child.setPclaId(null);
    }

    public void addParcelDecisionCollection(ParcelDecision child) {
        if (child != null)
            child.setPclaId((ParcelClas)this);
    }

    void internalRemoveParcelDecisionCollection(ParcelDecision child) {
        if (child != null)
            this.parcelDecisionCollection.remove(child);
    }

    void internalAddParcelDecisionCollection(ParcelDecision child) {
        if (child != null)
            this.parcelDecisionCollection.add(child);
    }

    @XmlTransient
    public List<ParcelsIssues> getParcelsIssuesCollection() {
        return parcelsIssuesCollection;
    }

    public void setParcelsIssuesCollection(List<ParcelsIssues> parcelsIssuesCollection) {
        this.parcelsIssuesCollection = parcelsIssuesCollection;
    }

    public void removeParcelsIssuesCollection(ParcelsIssues child) {
        if (child != null)
            child.setPclaId(null);
    }

    public void addParcelsIssuesCollection(ParcelsIssues child) {
        if (child != null)
            child.setPclaId((ParcelClas)this);
    }

    void internalRemoveParcelsIssuesCollection(ParcelsIssues child) {
        if (child != null)
            this.parcelsIssuesCollection.remove(child);
    }

    void internalAddParcelsIssuesCollection(ParcelsIssues child) {
        if (child != null)
            this.parcelsIssuesCollection.add(child);
    }

    /*
    fk to classification
    */
    public Classification getClasId() {
        return clasId;
    }

    public void setClasId(Classification clasId) {
    //    if (this.clasId != null) 
    //        this.clasId.internalRemoveParcelClasCollection(this);
        this.clasId = clasId;
    //    if (clasId != null)
    //        clasId.internalAddParcelClasCollection(this);
    }

    /*
    cultivation declared
    */
    public Cultivation getCultIdDecl() {
        return cultIdDecl;
    }

    public void setCultIdDecl(Cultivation cultIdDecl) {
    //    if (this.cultIdDecl != null) 
    //        this.cultIdDecl.internalRemoveParcelClascult_id_declCollection(this);
        this.cultIdDecl = cultIdDecl;
    //    if (cultIdDecl != null)
    //        cultIdDecl.internalAddParcelClascult_id_declCollection(this);
    }

    /*
    cultivation predicted
    */
    public Cultivation getCultIdPred() {
        return cultIdPred;
    }

    public void setCultIdPred(Cultivation cultIdPred) {
    //    if (this.cultIdPred != null) 
    //        this.cultIdPred.internalRemoveParcelClascult_id_predCollection(this);
        this.cultIdPred = cultIdPred;
    //    if (cultIdPred != null)
    //        cultIdPred.internalAddParcelClascult_id_predCollection(this);
    }

    /*
    fk to cover_type declared
    */
    public CoverType getCotyIdDecl() {
        return cotyIdDecl;
    }

    public void setCotyIdDecl(CoverType cotyIdDecl) {
    //    if (this.cotyIdDecl != null) 
    //        this.cotyIdDecl.internalRemoveParcelClascoty_id_declCollection(this);
        this.cotyIdDecl = cotyIdDecl;
    //    if (cotyIdDecl != null)
    //        cotyIdDecl.internalAddParcelClascoty_id_declCollection(this);
    }

    /*
    fk to cover_type predicted
    */
    public CoverType getCotyIdPred() {
        return cotyIdPred;
    }

    public void setCotyIdPred(CoverType cotyIdPred) {
    //    if (this.cotyIdPred != null) 
    //        this.cotyIdPred.internalRemoveParcelClascoty_id_predCollection(this);
        this.cotyIdPred = cotyIdPred;
    //    if (cotyIdPred != null)
    //        cotyIdPred.internalAddParcelClascoty_id_predCollection(this);
    }

    /*
    */
    public Cultivation getCultIdPred2() {
        return cultIdPred2;
    }

    public void setCultIdPred2(Cultivation cultIdPred2) {
    //    if (this.cultIdPred2 != null) 
    //        this.cultIdPred2.internalRemoveParcelClascult_id_pred2Collection(this);
        this.cultIdPred2 = cultIdPred2;
    //    if (cultIdPred2 != null)
    //        cultIdPred2.internalAddParcelClascult_id_pred2Collection(this);
    }



    @Override
    public int hashCode() {
        return pclaId.hashCode();
    }

    public String getRowKey() {
        return pclaId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return pclaId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(pclaId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.ParcelClas[ pclaId=" + pclaId + " ]";
    }

    @Override
    public String getEntityName() {
        return "ParcelClas";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the pclaId fields are not set
        if (!(object instanceof ParcelClasBase)) {
            return false;
        }
        ParcelClasBase other = (ParcelClasBase) object;
        if (this.pclaId == null || other.pclaId == null) 
            return false;
        
        return this.pclaId.equals(other.pclaId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
