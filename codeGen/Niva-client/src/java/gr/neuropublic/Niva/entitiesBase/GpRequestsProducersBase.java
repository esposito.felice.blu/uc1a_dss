/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains info per Producer of a geotagged photos request. 


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class GpRequestsProducersBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'gp_requests_producers_id' είναι υποχρεωτικό")
    @Column(name = "gp_requests_producers_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="gp_requests_producers_gp_requests_producers_id")
    //@SequenceGenerator(name="gp_requests_producers_gp_requests_producers_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer gpRequestsProducersId;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'notes' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "notes")
    protected String notes;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "gpRequestsProducersId", orphanRemoval=false)
    @OneToMany(mappedBy = "gpRequestsProducersId")
    protected List<GpRequestsContexts> gpRequestsContextsCollection = new ArrayList<GpRequestsContexts>();


    @JoinColumn(name = "gp_requests_id", referencedColumnName = "gp_requests_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'gp_requests_id' είναι υποχρεωτικό")
    protected GpRequests gpRequestsId;

    public boolean isGpRequestsIdPresent() {
        if (gpRequestsId==null || gpRequestsId.gpRequestsId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "producers_id", referencedColumnName = "producers_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'producers_id' είναι υποχρεωτικό")
    protected Producers producersId;

    public boolean isProducersIdPresent() {
        if (producersId==null || producersId.producersId == null)
            return false;
        return true;
    }



    public GpRequestsProducersBase() {
    }

    public GpRequestsProducersBase(Integer gpRequestsProducersId) {
        this.gpRequestsProducersId = gpRequestsProducersId;
    }

    public GpRequestsProducersBase(Integer gpRequestsProducersId, Integer rowVersion, String notes) {
        this.gpRequestsProducersId = gpRequestsProducersId;
        this.rowVersion = rowVersion;
        this.notes = notes;
    }

    public GpRequestsProducersBase(Integer rowVersion, String notes, GpRequests gpRequestsId, Producers producersId) {
        this.rowVersion = rowVersion;
        this.notes = notes;
        this.gpRequestsId = gpRequestsId;
        this.producersId = producersId;
    }

    public GpRequestsProducers clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "GpRequestsProducers:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (GpRequestsProducers)alreadyCloned.get(key);

        GpRequestsProducers clone = new GpRequestsProducers();
        alreadyCloned.put(key, clone);

        clone.setGpRequestsProducersId(getGpRequestsProducersId());
        clone.setRowVersion(getRowVersion());
        clone.setNotes(getNotes());
        clone.setGpRequestsContextsCollection(getGpRequestsContextsCollection());
        if (gpRequestsId == null || gpRequestsId.gpRequestsId == null) {
            clone.setGpRequestsId(gpRequestsId);
        } else {
            clone.setGpRequestsId(getGpRequestsId().clone(alreadyCloned));
        }
        if (producersId == null || producersId.producersId == null) {
            clone.setProducersId(producersId);
        } else {
            clone.setProducersId(getProducersId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "GpRequestsProducers:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(gpRequestsProducersId) : gpRequestsProducersId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (gpRequestsProducersId != null) {
            sb.append("\"gpRequestsProducersId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(gpRequestsProducersId) : gpRequestsProducersId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"gpRequestsProducersId\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (notes != null) {
            sb.append("\"notes\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(notes)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"notes\":"); sb.append("null");sb.append(",");
        }
        if (gpRequestsId != null && gpRequestsId.gpRequestsId != null) {
            sb.append("\"gpRequestsId\":"); sb.append(getGpRequestsId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (gpRequestsId == null) {
            sb.append("\"gpRequestsId\":"); sb.append("null");  sb.append(",");
        }
        if (producersId != null && producersId.producersId != null) {
            sb.append("\"producersId\":"); sb.append(getProducersId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (producersId == null) {
            sb.append("\"producersId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getGpRequestsProducersId() {
        return gpRequestsProducersId;
    }

    public void setGpRequestsProducersId(Integer gpRequestsProducersId) {
        this.gpRequestsProducersId = gpRequestsProducersId;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    @XmlTransient
    public List<GpRequestsContexts> getGpRequestsContextsCollection() {
        return gpRequestsContextsCollection;
    }

    public void setGpRequestsContextsCollection(List<GpRequestsContexts> gpRequestsContextsCollection) {
        this.gpRequestsContextsCollection = gpRequestsContextsCollection;
    }

    public void removeGpRequestsContextsCollection(GpRequestsContexts child) {
        if (child != null)
            child.setGpRequestsProducersId(null);
    }

    public void addGpRequestsContextsCollection(GpRequestsContexts child) {
        if (child != null)
            child.setGpRequestsProducersId((GpRequestsProducers)this);
    }

    void internalRemoveGpRequestsContextsCollection(GpRequestsContexts child) {
        if (child != null)
            this.gpRequestsContextsCollection.remove(child);
    }

    void internalAddGpRequestsContextsCollection(GpRequestsContexts child) {
        if (child != null)
            this.gpRequestsContextsCollection.add(child);
    }

    /*
    */
    public GpRequests getGpRequestsId() {
        return gpRequestsId;
    }

    public void setGpRequestsId(GpRequests gpRequestsId) {
    //    if (this.gpRequestsId != null) 
    //        this.gpRequestsId.internalRemoveGpRequestsProducersCollection(this);
        this.gpRequestsId = gpRequestsId;
    //    if (gpRequestsId != null)
    //        gpRequestsId.internalAddGpRequestsProducersCollection(this);
    }

    /*
    */
    public Producers getProducersId() {
        return producersId;
    }

    public void setProducersId(Producers producersId) {
    //    if (this.producersId != null) 
    //        this.producersId.internalRemoveGpRequestsProducersCollection(this);
        this.producersId = producersId;
    //    if (producersId != null)
    //        producersId.internalAddGpRequestsProducersCollection(this);
    }



    @Override
    public int hashCode() {
        return gpRequestsProducersId.hashCode();
    }

    public String getRowKey() {
        return gpRequestsProducersId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return gpRequestsProducersId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(gpRequestsProducersId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.GpRequestsProducers[ gpRequestsProducersId=" + gpRequestsProducersId + " ]";
    }

    @Override
    public String getEntityName() {
        return "GpRequestsProducers";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the gpRequestsProducersId fields are not set
        if (!(object instanceof GpRequestsProducersBase)) {
            return false;
        }
        GpRequestsProducersBase other = (GpRequestsProducersBase) object;
        if (this.gpRequestsProducersId == null || other.gpRequestsProducersId == null) 
            return false;
        
        return this.gpRequestsProducersId.equals(other.gpRequestsProducersId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
