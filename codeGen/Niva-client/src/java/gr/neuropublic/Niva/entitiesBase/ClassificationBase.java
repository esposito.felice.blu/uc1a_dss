/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
classification


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "Classification.findClassification", query = "SELECT x FROM Classification x WHERE (x.name like :name) AND (x.dateTime = :dateTime) AND (x.clfrId.clfrId = :clfrId_clfrId) AND (x.year = :year)  ")})
public abstract class ClassificationBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'clas_id' είναι υποχρεωτικό")
    @Column(name = "clas_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="classification_clas_id")
    //@SequenceGenerator(name="classification_clas_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer clasId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "name")
    protected String name;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'description' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "description")
    protected String description;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'date_time' είναι υποχρεωτικό")
    @Column(name = "date_time")
    @Temporal(TemporalType.DATE)
    protected Date dateTime;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'recordtype' είναι υποχρεωτικό")
    @Column(name = "recordtype")
    protected Integer recordtype;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'file_path' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "file_path")
    protected String filePath;


    @Basic(optional = true, fetch=FetchType.LAZY)
    @Column(name = "attached_file")

    protected byte[] attachedFile;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'year' είναι υποχρεωτικό")
    @Column(name = "year")
    protected Short year;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'is_imported' είναι υποχρεωτικό")
    @Column(name = "is_imported")
    protected Boolean isImported;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "clasId", orphanRemoval=false)
    @OneToMany(mappedBy = "clasId")
    protected List<Statistic> statisticCollection = new ArrayList<Statistic>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "clasId", orphanRemoval=false)
    @OneToMany(mappedBy = "clasId")
    protected List<ParcelClas> parcelClasCollection = new ArrayList<ParcelClas>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "clasId", orphanRemoval=false)
    @OneToMany(mappedBy = "clasId")
    protected List<DecisionMaking> decisionMakingCollection = new ArrayList<DecisionMaking>();


    @JoinColumn(name = "clfr_id", referencedColumnName = "clfr_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'clfr_id' είναι υποχρεωτικό")
    protected Classifier clfrId;

    public boolean isClfrIdPresent() {
        if (clfrId==null || clfrId.clfrId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "fite_id", referencedColumnName = "fite_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'fite_id' είναι υποχρεωτικό")
    protected FileTemplate fiteId;

    public boolean isFiteIdPresent() {
        if (fiteId==null || fiteId.fiteId == null)
            return false;
        return true;
    }



    public ClassificationBase() {
    }

    public ClassificationBase(Integer clasId) {
        this.clasId = clasId;
    }

    public ClassificationBase(Integer clasId, String name, String description, Date dateTime, Integer rowVersion, Integer recordtype, String filePath, byte[] attachedFile, Short year, Boolean isImported) {
        this.clasId = clasId;
        this.name = name;
        this.description = description;
        this.dateTime = dateTime;
        this.rowVersion = rowVersion;
        this.recordtype = recordtype;
        this.filePath = filePath;
        this.attachedFile = attachedFile;
        this.year = year;
        this.isImported = isImported;
    }

    public ClassificationBase(String name, String description, Date dateTime, Integer rowVersion, Integer recordtype, String filePath, byte[] attachedFile, Short year, Boolean isImported, Classifier clfrId, FileTemplate fiteId) {
        this.name = name;
        this.description = description;
        this.dateTime = dateTime;
        this.rowVersion = rowVersion;
        this.recordtype = recordtype;
        this.filePath = filePath;
        this.attachedFile = attachedFile;
        this.year = year;
        this.isImported = isImported;
        this.clfrId = clfrId;
        this.fiteId = fiteId;
    }

    public Classification clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "Classification:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (Classification)alreadyCloned.get(key);

        Classification clone = new Classification();
        alreadyCloned.put(key, clone);

        clone.setClasId(getClasId());
        clone.setName(getName());
        clone.setDescription(getDescription());
        clone.setDateTime(getDateTime());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setRecordtype(getRecordtype());
        clone.setFilePath(getFilePath());
        clone.setAttachedFile(getAttachedFile());
        clone.setYear(getYear());
        clone.setIsImported(getIsImported());
        clone.setStatisticCollection(getStatisticCollection());
        clone.setParcelClasCollection(getParcelClasCollection());
        clone.setDecisionMakingCollection(getDecisionMakingCollection());
        if (clfrId == null || clfrId.clfrId == null) {
            clone.setClfrId(clfrId);
        } else {
            clone.setClfrId(getClfrId().clone(alreadyCloned));
        }
        if (fiteId == null || fiteId.fiteId == null) {
            clone.setFiteId(fiteId);
        } else {
            clone.setFiteId(getFiteId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "Classification:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(clasId) : clasId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (clasId != null) {
            sb.append("\"clasId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(clasId) : clasId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"clasId\":"); sb.append("null");sb.append(",");
        }
        if (name != null) {
            sb.append("\"name\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(name)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"name\":"); sb.append("null");sb.append(",");
        }
        if (description != null) {
            sb.append("\"description\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(description)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"description\":"); sb.append("null");sb.append(",");
        }
        if (dateTime != null) {
            sb.append("\"dateTime\":"); sb.append(dateTime.getTime()); sb.append(",");
        } else {
            sb.append("\"dateTime\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (recordtype != null) {
            sb.append("\"recordtype\":"); sb.append(recordtype);sb.append(",");
        } else {
            sb.append("\"recordtype\":"); sb.append("null");sb.append(",");
        }
        if (filePath != null) {
            sb.append("\"filePath\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(filePath)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"filePath\":"); sb.append("null");sb.append(",");
        }
        if (year != null) {
            sb.append("\"year\":"); sb.append(year);sb.append(",");
        } else {
            sb.append("\"year\":"); sb.append("null");sb.append(",");
        }
        if (isImported != null) {
            sb.append("\"isImported\":"); sb.append(isImported);sb.append(",");
        } else {
            sb.append("\"isImported\":"); sb.append("null");sb.append(",");
        }
        if (clfrId != null && clfrId.clfrId != null) {
            sb.append("\"clfrId\":"); sb.append(getClfrId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (clfrId == null) {
            sb.append("\"clfrId\":"); sb.append("null");  sb.append(",");
        }
        if (fiteId != null && fiteId.fiteId != null) {
            sb.append("\"fiteId\":"); sb.append(getFiteId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (fiteId == null) {
            sb.append("\"fiteId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getClasId() {
        return clasId;
    }

    public void setClasId(Integer clasId) {
        this.clasId = clasId;
    }
    /*
    */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
    */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    /*
    */
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Integer getRecordtype() {
        return recordtype;
    }

    public void setRecordtype(Integer recordtype) {
        this.recordtype = recordtype;
    }
    /*
    */
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    /*
    */
    public byte[] getAttachedFile() {
        return attachedFile;
    }

    public void setAttachedFile(byte[] attachedFile) {
        this.attachedFile = attachedFile;
    }
    /*
    The year that the classifications is about.
    */
    public Short getYear() {
        return year;
    }

    public void setYear(Short year) {
        this.year = year;
    }
    /*
    True if the file has been parsed.
    */
    public Boolean getIsImported() {
        return isImported;
    }

    public void setIsImported(Boolean isImported) {
        this.isImported = isImported;
    }
    @XmlTransient
    public List<Statistic> getStatisticCollection() {
        return statisticCollection;
    }

    public void setStatisticCollection(List<Statistic> statisticCollection) {
        this.statisticCollection = statisticCollection;
    }

    public void removeStatisticCollection(Statistic child) {
        if (child != null)
            child.setClasId(null);
    }

    public void addStatisticCollection(Statistic child) {
        if (child != null)
            child.setClasId((Classification)this);
    }

    void internalRemoveStatisticCollection(Statistic child) {
        if (child != null)
            this.statisticCollection.remove(child);
    }

    void internalAddStatisticCollection(Statistic child) {
        if (child != null)
            this.statisticCollection.add(child);
    }

    @XmlTransient
    public List<ParcelClas> getParcelClasCollection() {
        return parcelClasCollection;
    }

    public void setParcelClasCollection(List<ParcelClas> parcelClasCollection) {
        this.parcelClasCollection = parcelClasCollection;
    }

    public void removeParcelClasCollection(ParcelClas child) {
        if (child != null)
            child.setClasId(null);
    }

    public void addParcelClasCollection(ParcelClas child) {
        if (child != null)
            child.setClasId((Classification)this);
    }

    void internalRemoveParcelClasCollection(ParcelClas child) {
        if (child != null)
            this.parcelClasCollection.remove(child);
    }

    void internalAddParcelClasCollection(ParcelClas child) {
        if (child != null)
            this.parcelClasCollection.add(child);
    }

    @XmlTransient
    public List<DecisionMaking> getDecisionMakingCollection() {
        return decisionMakingCollection;
    }

    public void setDecisionMakingCollection(List<DecisionMaking> decisionMakingCollection) {
        this.decisionMakingCollection = decisionMakingCollection;
    }

    public void removeDecisionMakingCollection(DecisionMaking child) {
        if (child != null)
            child.setClasId(null);
    }

    public void addDecisionMakingCollection(DecisionMaking child) {
        if (child != null)
            child.setClasId((Classification)this);
    }

    void internalRemoveDecisionMakingCollection(DecisionMaking child) {
        if (child != null)
            this.decisionMakingCollection.remove(child);
    }

    void internalAddDecisionMakingCollection(DecisionMaking child) {
        if (child != null)
            this.decisionMakingCollection.add(child);
    }

    /*
    fk to classifier
    */
    public Classifier getClfrId() {
        return clfrId;
    }

    public void setClfrId(Classifier clfrId) {
    //    if (this.clfrId != null) 
    //        this.clfrId.internalRemoveClassificationCollection(this);
        this.clfrId = clfrId;
    //    if (clfrId != null)
    //        clfrId.internalAddClassificationCollection(this);
    }

    /*
    fk to file template
    */
    public FileTemplate getFiteId() {
        return fiteId;
    }

    public void setFiteId(FileTemplate fiteId) {
    //    if (this.fiteId != null) 
    //        this.fiteId.internalRemoveClassificationCollection(this);
        this.fiteId = fiteId;
    //    if (fiteId != null)
    //        fiteId.internalAddClassificationCollection(this);
    }



    @Override
    public int hashCode() {
        return clasId.hashCode();
    }

    public String getRowKey() {
        return clasId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return clasId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(clasId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.Classification[ clasId=" + clasId + " ]";
    }

    @Override
    public String getEntityName() {
        return "Classification";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the clasId fields are not set
        if (!(object instanceof ClassificationBase)) {
            return false;
        }
        ClassificationBase other = (ClassificationBase) object;
        if (this.clasId == null || other.clasId == null) 
            return false;
        
        return this.clasId.equals(other.clasId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
