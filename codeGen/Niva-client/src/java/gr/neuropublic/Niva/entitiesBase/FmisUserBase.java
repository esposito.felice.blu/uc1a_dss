/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
FMIS User


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "FmisUser.findFmisUser", query = "SELECT x FROM FmisUser x WHERE (x.fmisName like :fmisName) AND (x.fmisUid like :fmisUid)")})
public abstract class FmisUserBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'fmis_name' είναι υποχρεωτικό")
    @Size(min = 1, max = 40, message="Το πεδίο 'fmis_name' πρέπει νά έχει μέγεθος μεταξύ 1 και 40.")
    @Column(name = "fmis_name")
    protected String fmisName;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'fmis_users_id' είναι υποχρεωτικό")
    @Column(name = "fmis_users_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="fmis_users_fmis_users_id")
    //@SequenceGenerator(name="fmis_users_fmis_users_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer fmisUsersId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'fmis_uid' είναι υποχρεωτικό")
    @Size(min = 1, max = 5, message="Το πεδίο 'fmis_uid' πρέπει νά έχει μέγεθος μεταξύ 1 και 5.")
    @Column(name = "fmis_uid")
    protected String fmisUid;



    public FmisUserBase() {
    }

    public FmisUserBase(Integer fmisUsersId) {
        this.fmisUsersId = fmisUsersId;
    }

    public FmisUserBase(Integer fmisUsersId, String fmisName, Integer rowVersion, String fmisUid) {
        this.fmisUsersId = fmisUsersId;
        this.fmisName = fmisName;
        this.rowVersion = rowVersion;
        this.fmisUid = fmisUid;
    }

    public FmisUserBase(String fmisName, Integer rowVersion, String fmisUid) {
        this.fmisName = fmisName;
        this.rowVersion = rowVersion;
        this.fmisUid = fmisUid;
    }

    public FmisUser clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "FmisUser:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (FmisUser)alreadyCloned.get(key);

        FmisUser clone = new FmisUser();
        alreadyCloned.put(key, clone);

        clone.setFmisName(getFmisName());
        clone.setRowVersion(getRowVersion());
        clone.setFmisUsersId(getFmisUsersId());
        clone.setFmisUid(getFmisUid());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "FmisUser:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(fmisUsersId) : fmisUsersId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (fmisName != null) {
            sb.append("\"fmisName\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(fmisName)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"fmisName\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (fmisUsersId != null) {
            sb.append("\"fmisUsersId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(fmisUsersId) : fmisUsersId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"fmisUsersId\":"); sb.append("null");sb.append(",");
        }
        if (fmisUid != null) {
            sb.append("\"fmisUid\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(fmisUid)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"fmisUid\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public String getFmisName() {
        return fmisName;
    }

    public void setFmisName(String fmisName) {
        this.fmisName = fmisName;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Integer getFmisUsersId() {
        return fmisUsersId;
    }

    public void setFmisUsersId(Integer fmisUsersId) {
        this.fmisUsersId = fmisUsersId;
    }
    /*
    Contains a 5 chars human-readable User Id.
    */
    public String getFmisUid() {
        return fmisUid;
    }

    public void setFmisUid(String fmisUid) {
        this.fmisUid = fmisUid;
    }


    @Override
    public int hashCode() {
        return fmisUsersId.hashCode();
    }

    public String getRowKey() {
        return fmisUsersId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return fmisUsersId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(fmisUsersId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.FmisUser[ fmisUsersId=" + fmisUsersId + " ]";
    }

    @Override
    public String getEntityName() {
        return "FmisUser";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the fmisUsersId fields are not set
        if (!(object instanceof FmisUserBase)) {
            return false;
        }
        FmisUserBase other = (FmisUserBase) object;
        if (this.fmisUsersId == null || other.fmisUsersId == null) 
            return false;
        
        return this.fmisUsersId.equals(other.fmisUsersId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
