/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class DocumentBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'docu_id' είναι υποχρεωτικό")
    @Column(name = "docu_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="documents_docu_id")
    //@SequenceGenerator(name="documents_docu_id", sequenceName="niva.documents_docu_id_seq", allocationSize=1)
    protected Integer docuId;


    @Basic(optional = true, fetch=FetchType.LAZY)
    @Column(name = "attached_file")

    protected byte[] attachedFile;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'file_path' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "file_path")
    protected String filePath;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'description' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "description")
    protected String description;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;



    public DocumentBase() {
    }

    public DocumentBase(Integer docuId) {
        this.docuId = docuId;
    }

    public DocumentBase(Integer docuId, byte[] attachedFile, String filePath, String description, Integer rowVersion) {
        this.docuId = docuId;
        this.attachedFile = attachedFile;
        this.filePath = filePath;
        this.description = description;
        this.rowVersion = rowVersion;
    }

    public DocumentBase(byte[] attachedFile, String filePath, String description, Integer rowVersion) {
        this.attachedFile = attachedFile;
        this.filePath = filePath;
        this.description = description;
        this.rowVersion = rowVersion;
    }

    public Document clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "Document:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (Document)alreadyCloned.get(key);

        Document clone = new Document();
        alreadyCloned.put(key, clone);

        clone.setDocuId(getDocuId());
        clone.setAttachedFile(getAttachedFile());
        clone.setFilePath(getFilePath());
        clone.setDescription(getDescription());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "Document:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(docuId) : docuId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (docuId != null) {
            sb.append("\"docuId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(docuId) : docuId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"docuId\":"); sb.append("null");sb.append(",");
        }
        if (filePath != null) {
            sb.append("\"filePath\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(filePath)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"filePath\":"); sb.append("null");sb.append(",");
        }
        if (description != null) {
            sb.append("\"description\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(description)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"description\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getDocuId() {
        return docuId;
    }

    public void setDocuId(Integer docuId) {
        this.docuId = docuId;
    }
    /*
    */
    public byte[] getAttachedFile() {
        return attachedFile;
    }

    public void setAttachedFile(byte[] attachedFile) {
        this.attachedFile = attachedFile;
    }
    /*
    */
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    /*
    */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }


    @Override
    public int hashCode() {
        return docuId.hashCode();
    }

    public String getRowKey() {
        return docuId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return docuId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(docuId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.Document[ docuId=" + docuId + " ]";
    }

    @Override
    public String getEntityName() {
        return "Document";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the docuId fields are not set
        if (!(object instanceof DocumentBase)) {
            return false;
        }
        DocumentBase other = (DocumentBase) object;
        if (this.docuId == null || other.docuId == null) 
            return false;
        
        return this.docuId.equals(other.docuId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
