/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains info about the farmers.


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "Producers.findProducers", query = "SELECT x FROM Producers x WHERE (x.prodName like :prodName) AND (x.prodCode = :prodCode)")})
public abstract class ProducersBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'prod_name' είναι υποχρεωτικό")
    @Size(min = 1, max = 40, message="Το πεδίο 'prod_name' πρέπει νά έχει μέγεθος μεταξύ 1 και 40.")
    @Column(name = "prod_name")
    protected String prodName;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'prod_code' είναι υποχρεωτικό")
    @Column(name = "prod_code")
    protected Integer prodCode;


    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'producers_id' είναι υποχρεωτικό")
    @Column(name = "producers_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="producers_producers_id")
    //@SequenceGenerator(name="producers_producers_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer producersId;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "producersId", orphanRemoval=false)
    @OneToMany(mappedBy = "producersId")
    protected List<GpRequestsProducers> gpRequestsProducersCollection = new ArrayList<GpRequestsProducers>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "producersId", orphanRemoval=false)
    @OneToMany(mappedBy = "producersId")
    protected List<AgrisnapUsersProducers> agrisnapUsersProducersCollection = new ArrayList<AgrisnapUsersProducers>();



    public ProducersBase() {
    }

    public ProducersBase(Integer producersId) {
        this.producersId = producersId;
    }

    public ProducersBase(Integer producersId, String prodName, Integer rowVersion, Integer prodCode) {
        this.producersId = producersId;
        this.prodName = prodName;
        this.rowVersion = rowVersion;
        this.prodCode = prodCode;
    }

    public ProducersBase(String prodName, Integer rowVersion, Integer prodCode) {
        this.prodName = prodName;
        this.rowVersion = rowVersion;
        this.prodCode = prodCode;
    }

    public Producers clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "Producers:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (Producers)alreadyCloned.get(key);

        Producers clone = new Producers();
        alreadyCloned.put(key, clone);

        clone.setProdName(getProdName());
        clone.setRowVersion(getRowVersion());
        clone.setProdCode(getProdCode());
        clone.setProducersId(getProducersId());
        clone.setGpRequestsProducersCollection(getGpRequestsProducersCollection());
        clone.setAgrisnapUsersProducersCollection(getAgrisnapUsersProducersCollection());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "Producers:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(producersId) : producersId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (prodName != null) {
            sb.append("\"prodName\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(prodName)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"prodName\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (prodCode != null) {
            sb.append("\"prodCode\":"); sb.append(prodCode);sb.append(",");
        } else {
            sb.append("\"prodCode\":"); sb.append("null");sb.append(",");
        }
        if (producersId != null) {
            sb.append("\"producersId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(producersId) : producersId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"producersId\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Integer getProdCode() {
        return prodCode;
    }

    public void setProdCode(Integer prodCode) {
        this.prodCode = prodCode;
    }
    /*
    */
    public Integer getProducersId() {
        return producersId;
    }

    public void setProducersId(Integer producersId) {
        this.producersId = producersId;
    }
    @XmlTransient
    public List<GpRequestsProducers> getGpRequestsProducersCollection() {
        return gpRequestsProducersCollection;
    }

    public void setGpRequestsProducersCollection(List<GpRequestsProducers> gpRequestsProducersCollection) {
        this.gpRequestsProducersCollection = gpRequestsProducersCollection;
    }

    public void removeGpRequestsProducersCollection(GpRequestsProducers child) {
        if (child != null)
            child.setProducersId(null);
    }

    public void addGpRequestsProducersCollection(GpRequestsProducers child) {
        if (child != null)
            child.setProducersId((Producers)this);
    }

    void internalRemoveGpRequestsProducersCollection(GpRequestsProducers child) {
        if (child != null)
            this.gpRequestsProducersCollection.remove(child);
    }

    void internalAddGpRequestsProducersCollection(GpRequestsProducers child) {
        if (child != null)
            this.gpRequestsProducersCollection.add(child);
    }

    @XmlTransient
    public List<AgrisnapUsersProducers> getAgrisnapUsersProducersCollection() {
        return agrisnapUsersProducersCollection;
    }

    public void setAgrisnapUsersProducersCollection(List<AgrisnapUsersProducers> agrisnapUsersProducersCollection) {
        this.agrisnapUsersProducersCollection = agrisnapUsersProducersCollection;
    }

    public void removeAgrisnapUsersProducersCollection(AgrisnapUsersProducers child) {
        if (child != null)
            child.setProducersId(null);
    }

    public void addAgrisnapUsersProducersCollection(AgrisnapUsersProducers child) {
        if (child != null)
            child.setProducersId((Producers)this);
    }

    void internalRemoveAgrisnapUsersProducersCollection(AgrisnapUsersProducers child) {
        if (child != null)
            this.agrisnapUsersProducersCollection.remove(child);
    }

    void internalAddAgrisnapUsersProducersCollection(AgrisnapUsersProducers child) {
        if (child != null)
            this.agrisnapUsersProducersCollection.add(child);
    }



    @Override
    public int hashCode() {
        return producersId.hashCode();
    }

    public String getRowKey() {
        return producersId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return producersId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(producersId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.Producers[ producersId=" + producersId + " ]";
    }

    @Override
    public String getEntityName() {
        return "Producers";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the producersId fields are not set
        if (!(object instanceof ProducersBase)) {
            return false;
        }
        ProducersBase other = (ProducersBase) object;
        if (this.producersId == null || other.producersId == null) 
            return false;
        
        return this.producersId.equals(other.producersId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
