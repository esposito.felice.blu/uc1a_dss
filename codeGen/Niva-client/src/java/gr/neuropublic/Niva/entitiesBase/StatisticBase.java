/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Statistics


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class StatisticBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'stat_id' είναι υποχρεωτικό")
    @Column(name = "stat_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="statistics_stat_id")
    //@SequenceGenerator(name="statistics_stat_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer statId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "name")
    protected String name;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'description' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "description")
    protected String description;


    @Basic(optional = true)
    @Column(name = "value")
    protected Integer value;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @JoinColumn(name = "clas_id", referencedColumnName = "clas_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'clas_id' είναι υποχρεωτικό")
    protected Classification clasId;

    public boolean isClasIdPresent() {
        if (clasId==null || clasId.clasId == null)
            return false;
        return true;
    }



    public StatisticBase() {
    }

    public StatisticBase(Integer statId) {
        this.statId = statId;
    }

    public StatisticBase(Integer statId, String name, String description, Integer value, Integer rowVersion) {
        this.statId = statId;
        this.name = name;
        this.description = description;
        this.value = value;
        this.rowVersion = rowVersion;
    }

    public StatisticBase(String name, String description, Integer value, Integer rowVersion, Classification clasId) {
        this.name = name;
        this.description = description;
        this.value = value;
        this.rowVersion = rowVersion;
        this.clasId = clasId;
    }

    public Statistic clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "Statistic:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (Statistic)alreadyCloned.get(key);

        Statistic clone = new Statistic();
        alreadyCloned.put(key, clone);

        clone.setStatId(getStatId());
        clone.setName(getName());
        clone.setDescription(getDescription());
        clone.setValue(getValue());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        if (clasId == null || clasId.clasId == null) {
            clone.setClasId(clasId);
        } else {
            clone.setClasId(getClasId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "Statistic:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(statId) : statId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (statId != null) {
            sb.append("\"statId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(statId) : statId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"statId\":"); sb.append("null");sb.append(",");
        }
        if (name != null) {
            sb.append("\"name\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(name)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"name\":"); sb.append("null");sb.append(",");
        }
        if (description != null) {
            sb.append("\"description\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(description)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"description\":"); sb.append("null");sb.append(",");
        }
        if (value != null) {
            sb.append("\"value\":"); sb.append(value);sb.append(",");
        } else {
            sb.append("\"value\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (clasId != null && clasId.clasId != null) {
            sb.append("\"clasId\":"); sb.append(getClasId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (clasId == null) {
            sb.append("\"clasId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getStatId() {
        return statId;
    }

    public void setStatId(Integer statId) {
        this.statId = statId;
    }
    /*
    */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
    */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    /*
    value
    */
    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    fk to classification
    */
    public Classification getClasId() {
        return clasId;
    }

    public void setClasId(Classification clasId) {
    //    if (this.clasId != null) 
    //        this.clasId.internalRemoveStatisticCollection(this);
        this.clasId = clasId;
    //    if (clasId != null)
    //        clasId.internalAddStatisticCollection(this);
    }



    @Override
    public int hashCode() {
        return statId.hashCode();
    }

    public String getRowKey() {
        return statId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return statId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(statId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.Statistic[ statId=" + statId + " ]";
    }

    @Override
    public String getEntityName() {
        return "Statistic";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the statId fields are not set
        if (!(object instanceof StatisticBase)) {
            return false;
        }
        StatisticBase other = (StatisticBase) object;
        if (this.statId == null || other.statId == null) 
            return false;
        
        return this.statId.equals(other.statId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
