/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
predefine columns


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class PredefColBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'prco_id' είναι υποχρεωτικό")
    @Column(name = "prco_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="predef_col_prco_id")
    //@SequenceGenerator(name="predef_col_prco_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer prcoId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'column_name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'column_name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "column_name")
    protected String columnName;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'system_column_name' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "system_column_name")
    protected String systemColumnName;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "prcoId", orphanRemoval=false)
    @OneToMany(mappedBy = "prcoId")
    protected List<TemplateColumn> templateColumnCollection = new ArrayList<TemplateColumn>();



    public PredefColBase() {
    }

    public PredefColBase(Integer prcoId) {
        this.prcoId = prcoId;
    }

    public PredefColBase(Integer prcoId, String columnName, Integer rowVersion, String systemColumnName) {
        this.prcoId = prcoId;
        this.columnName = columnName;
        this.rowVersion = rowVersion;
        this.systemColumnName = systemColumnName;
    }

    public PredefColBase(String columnName, Integer rowVersion, String systemColumnName) {
        this.columnName = columnName;
        this.rowVersion = rowVersion;
        this.systemColumnName = systemColumnName;
    }

    public PredefCol clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "PredefCol:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (PredefCol)alreadyCloned.get(key);

        PredefCol clone = new PredefCol();
        alreadyCloned.put(key, clone);

        clone.setPrcoId(getPrcoId());
        clone.setColumnName(getColumnName());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setSystemColumnName(getSystemColumnName());
        clone.setTemplateColumnCollection(getTemplateColumnCollection());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "PredefCol:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(prcoId) : prcoId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (prcoId != null) {
            sb.append("\"prcoId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(prcoId) : prcoId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"prcoId\":"); sb.append("null");sb.append(",");
        }
        if (columnName != null) {
            sb.append("\"columnName\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(columnName)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"columnName\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (systemColumnName != null) {
            sb.append("\"systemColumnName\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(systemColumnName)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"systemColumnName\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getPrcoId() {
        return prcoId;
    }

    public void setPrcoId(Integer prcoId) {
        this.prcoId = prcoId;
    }
    /*
    dss column name - internal system name
    */
    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public String getSystemColumnName() {
        return systemColumnName;
    }

    public void setSystemColumnName(String systemColumnName) {
        this.systemColumnName = systemColumnName;
    }
    @XmlTransient
    public List<TemplateColumn> getTemplateColumnCollection() {
        return templateColumnCollection;
    }

    public void setTemplateColumnCollection(List<TemplateColumn> templateColumnCollection) {
        this.templateColumnCollection = templateColumnCollection;
    }

    public void removeTemplateColumnCollection(TemplateColumn child) {
        if (child != null)
            child.setPrcoId(null);
    }

    public void addTemplateColumnCollection(TemplateColumn child) {
        if (child != null)
            child.setPrcoId((PredefCol)this);
    }

    void internalRemoveTemplateColumnCollection(TemplateColumn child) {
        if (child != null)
            this.templateColumnCollection.remove(child);
    }

    void internalAddTemplateColumnCollection(TemplateColumn child) {
        if (child != null)
            this.templateColumnCollection.add(child);
    }



    @Override
    public int hashCode() {
        return prcoId.hashCode();
    }

    public String getRowKey() {
        return prcoId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return prcoId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(prcoId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.PredefCol[ prcoId=" + prcoId + " ]";
    }

    @Override
    public String getEntityName() {
        return "PredefCol";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the prcoId fields are not set
        if (!(object instanceof PredefColBase)) {
            return false;
        }
        PredefColBase other = (PredefColBase) object;
        if (this.prcoId == null || other.prcoId == null) 
            return false;
        
        return this.prcoId.equals(other.prcoId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
