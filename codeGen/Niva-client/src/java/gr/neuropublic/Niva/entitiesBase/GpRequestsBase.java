/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains the requests (and their responses) of AgriSpan.


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class GpRequestsBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'gp_requests_id' είναι υποχρεωτικό")
    @Column(name = "gp_requests_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="gp_requests_gp_requests_id")
    //@SequenceGenerator(name="gp_requests_gp_requests_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer gpRequestsId;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'dte_received' είναι υποχρεωτικό")
    @Column(name = "dte_received")
    @Temporal(TemporalType.DATE)
    protected Date dteReceived;


    @Basic(optional = true)
    @Column(name = "type_code")
    protected Short typeCode;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'type_description' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "type_description")
    protected String typeDescription;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'scheme' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "scheme")
    protected String scheme;


    @Basic(optional = true)
    @Column(name = "correspondence_id")
    protected Integer correspondenceId;


    @Basic(optional = true)
    @Column(name = "correspondence_doc_nr")
    protected Integer correspondenceDocNr;



    @Basic(optional = true)
    @Size(min = 0, max = 1, message="Το πεδίο 'geotag' πρέπει νά έχει μέγεθος μεταξύ 0 και 1.")
    @Column(name = "geotag")
    protected String geotag;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "gpRequestsId", orphanRemoval=false)
    @OneToMany(mappedBy = "gpRequestsId")
    protected List<GpRequestsProducers> gpRequestsProducersCollection = new ArrayList<GpRequestsProducers>();


    @JoinColumn(name = "agrisnap_users_id", referencedColumnName = "agrisnap_users_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'agrisnap_users_id' είναι υποχρεωτικό")
    protected AgrisnapUsers agrisnapUsersId;

    public boolean isAgrisnapUsersIdPresent() {
        if (agrisnapUsersId==null || agrisnapUsersId.agrisnapUsersId == null)
            return false;
        return true;
    }



    public GpRequestsBase() {
    }

    public GpRequestsBase(Integer gpRequestsId) {
        this.gpRequestsId = gpRequestsId;
    }

    public GpRequestsBase(Integer gpRequestsId, Date dteReceived, Short typeCode, String typeDescription, String scheme, Integer correspondenceId, Integer correspondenceDocNr, String geotag, Integer rowVersion) {
        this.gpRequestsId = gpRequestsId;
        this.dteReceived = dteReceived;
        this.typeCode = typeCode;
        this.typeDescription = typeDescription;
        this.scheme = scheme;
        this.correspondenceId = correspondenceId;
        this.correspondenceDocNr = correspondenceDocNr;
        this.geotag = geotag;
        this.rowVersion = rowVersion;
    }

    public GpRequestsBase(Date dteReceived, Short typeCode, String typeDescription, String scheme, Integer correspondenceId, Integer correspondenceDocNr, String geotag, Integer rowVersion, AgrisnapUsers agrisnapUsersId) {
        this.dteReceived = dteReceived;
        this.typeCode = typeCode;
        this.typeDescription = typeDescription;
        this.scheme = scheme;
        this.correspondenceId = correspondenceId;
        this.correspondenceDocNr = correspondenceDocNr;
        this.geotag = geotag;
        this.rowVersion = rowVersion;
        this.agrisnapUsersId = agrisnapUsersId;
    }

    public GpRequests clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "GpRequests:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (GpRequests)alreadyCloned.get(key);

        GpRequests clone = new GpRequests();
        alreadyCloned.put(key, clone);

        clone.setGpRequestsId(getGpRequestsId());
        clone.setDteReceived(getDteReceived());
        clone.setTypeCode(getTypeCode());
        clone.setTypeDescription(getTypeDescription());
        clone.setScheme(getScheme());
        clone.setCorrespondenceId(getCorrespondenceId());
        clone.setCorrespondenceDocNr(getCorrespondenceDocNr());
        clone.setGeotag(getGeotag());
        clone.setRowVersion(getRowVersion());
        clone.setGpRequestsProducersCollection(getGpRequestsProducersCollection());
        if (agrisnapUsersId == null || agrisnapUsersId.agrisnapUsersId == null) {
            clone.setAgrisnapUsersId(agrisnapUsersId);
        } else {
            clone.setAgrisnapUsersId(getAgrisnapUsersId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "GpRequests:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(gpRequestsId) : gpRequestsId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (gpRequestsId != null) {
            sb.append("\"gpRequestsId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(gpRequestsId) : gpRequestsId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"gpRequestsId\":"); sb.append("null");sb.append(",");
        }
        if (dteReceived != null) {
            sb.append("\"dteReceived\":"); sb.append(dteReceived.getTime()); sb.append(",");
        } else {
            sb.append("\"dteReceived\":"); sb.append("null");sb.append(",");
        }
        if (typeCode != null) {
            sb.append("\"typeCode\":"); sb.append(typeCode);sb.append(",");
        } else {
            sb.append("\"typeCode\":"); sb.append("null");sb.append(",");
        }
        if (typeDescription != null) {
            sb.append("\"typeDescription\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(typeDescription)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"typeDescription\":"); sb.append("null");sb.append(",");
        }
        if (scheme != null) {
            sb.append("\"scheme\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(scheme)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"scheme\":"); sb.append("null");sb.append(",");
        }
        if (correspondenceId != null) {
            sb.append("\"correspondenceId\":"); sb.append(correspondenceId);sb.append(",");
        } else {
            sb.append("\"correspondenceId\":"); sb.append("null");sb.append(",");
        }
        if (correspondenceDocNr != null) {
            sb.append("\"correspondenceDocNr\":"); sb.append(correspondenceDocNr);sb.append(",");
        } else {
            sb.append("\"correspondenceDocNr\":"); sb.append("null");sb.append(",");
        }
        if (geotag != null) {
            sb.append("\"geotag\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(geotag)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"geotag\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (agrisnapUsersId != null && agrisnapUsersId.agrisnapUsersId != null) {
            sb.append("\"agrisnapUsersId\":"); sb.append(getAgrisnapUsersId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (agrisnapUsersId == null) {
            sb.append("\"agrisnapUsersId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getGpRequestsId() {
        return gpRequestsId;
    }

    public void setGpRequestsId(Integer gpRequestsId) {
        this.gpRequestsId = gpRequestsId;
    }
    /*
    */
    public Date getDteReceived() {
        return dteReceived;
    }

    public void setDteReceived(Date dteReceived) {
        this.dteReceived = dteReceived;
    }
    /*
    */
    public Short getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(Short typeCode) {
        this.typeCode = typeCode;
    }
    /*
    */
    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }
    /*
    */
    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }
    /*
    */
    public Integer getCorrespondenceId() {
        return correspondenceId;
    }

    public void setCorrespondenceId(Integer correspondenceId) {
        this.correspondenceId = correspondenceId;
    }
    /*
    */
    public Integer getCorrespondenceDocNr() {
        return correspondenceDocNr;
    }

    public void setCorrespondenceDocNr(Integer correspondenceDocNr) {
        this.correspondenceDocNr = correspondenceDocNr;
    }
    /*
    */
    public String getGeotag() {
        return geotag;
    }

    public void setGeotag(String geotag) {
        this.geotag = geotag;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    @XmlTransient
    public List<GpRequestsProducers> getGpRequestsProducersCollection() {
        return gpRequestsProducersCollection;
    }

    public void setGpRequestsProducersCollection(List<GpRequestsProducers> gpRequestsProducersCollection) {
        this.gpRequestsProducersCollection = gpRequestsProducersCollection;
    }

    public void removeGpRequestsProducersCollection(GpRequestsProducers child) {
        if (child != null)
            child.setGpRequestsId(null);
    }

    public void addGpRequestsProducersCollection(GpRequestsProducers child) {
        if (child != null)
            child.setGpRequestsId((GpRequests)this);
    }

    void internalRemoveGpRequestsProducersCollection(GpRequestsProducers child) {
        if (child != null)
            this.gpRequestsProducersCollection.remove(child);
    }

    void internalAddGpRequestsProducersCollection(GpRequestsProducers child) {
        if (child != null)
            this.gpRequestsProducersCollection.add(child);
    }

    /*
    */
    public AgrisnapUsers getAgrisnapUsersId() {
        return agrisnapUsersId;
    }

    public void setAgrisnapUsersId(AgrisnapUsers agrisnapUsersId) {
    //    if (this.agrisnapUsersId != null) 
    //        this.agrisnapUsersId.internalRemoveGpRequestsCollection(this);
        this.agrisnapUsersId = agrisnapUsersId;
    //    if (agrisnapUsersId != null)
    //        agrisnapUsersId.internalAddGpRequestsCollection(this);
    }



    @Override
    public int hashCode() {
        return gpRequestsId.hashCode();
    }

    public String getRowKey() {
        return gpRequestsId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return gpRequestsId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(gpRequestsId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.GpRequests[ gpRequestsId=" + gpRequestsId + " ]";
    }

    @Override
    public String getEntityName() {
        return "GpRequests";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the gpRequestsId fields are not set
        if (!(object instanceof GpRequestsBase)) {
            return false;
        }
        GpRequestsBase other = (GpRequestsBase) object;
        if (this.gpRequestsId == null || other.gpRequestsId == null) 
            return false;
        
        return this.gpRequestsId.equals(other.gpRequestsId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
