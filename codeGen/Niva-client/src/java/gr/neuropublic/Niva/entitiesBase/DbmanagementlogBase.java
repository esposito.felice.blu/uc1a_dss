/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains info about any updates of the db schema etc.


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class DbmanagementlogBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'dblid' είναι υποχρεωτικό")
    @Column(name = "dblid")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="dbmanagementlog_dblid")
    //@SequenceGenerator(name="dbmanagementlog_dblid", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer dblid;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'dte_done' είναι υποχρεωτικό")
    @Column(name = "dte_done")
    @Temporal(TemporalType.DATE)
    protected Date dteDone;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'version' είναι υποχρεωτικό")
    @Size(min = 1, max = 10, message="Το πεδίο 'version' πρέπει νά έχει μέγεθος μεταξύ 1 και 10.")
    @Column(name = "version")
    protected String version;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'notes' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "notes")
    protected String notes;


    @Basic(optional = true)
    @Column(name = "type")
    protected Integer type;



    public DbmanagementlogBase() {
    }

    public DbmanagementlogBase(Integer dblid) {
        this.dblid = dblid;
    }

    public DbmanagementlogBase(Integer dblid, Date dteDone, String version, String notes, Integer type) {
        this.dblid = dblid;
        this.dteDone = dteDone;
        this.version = version;
        this.notes = notes;
        this.type = type;
    }

    public DbmanagementlogBase(Date dteDone, String version, String notes, Integer type) {
        this.dteDone = dteDone;
        this.version = version;
        this.notes = notes;
        this.type = type;
    }

    public Dbmanagementlog clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "Dbmanagementlog:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (Dbmanagementlog)alreadyCloned.get(key);

        Dbmanagementlog clone = new Dbmanagementlog();
        alreadyCloned.put(key, clone);

        clone.setDblid(getDblid());
        clone.setDteDone(getDteDone());
        clone.setVersion(getVersion());
        clone.setNotes(getNotes());
        clone.setType(getType());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "Dbmanagementlog:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(dblid) : dblid); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (dblid != null) {
            sb.append("\"dblid\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(dblid) : dblid); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"dblid\":"); sb.append("null");sb.append(",");
        }
        if (dteDone != null) {
            sb.append("\"dteDone\":"); sb.append(dteDone.getTime()); sb.append(",");
        } else {
            sb.append("\"dteDone\":"); sb.append("null");sb.append(",");
        }
        if (version != null) {
            sb.append("\"version\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(version)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"version\":"); sb.append("null");sb.append(",");
        }
        if (notes != null) {
            sb.append("\"notes\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(notes)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"notes\":"); sb.append("null");sb.append(",");
        }
        if (type != null) {
            sb.append("\"type\":"); sb.append(type);sb.append(",");
        } else {
            sb.append("\"type\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getDblid() {
        return dblid;
    }

    public void setDblid(Integer dblid) {
        this.dblid = dblid;
    }
    /*
    */
    public Date getDteDone() {
        return dteDone;
    }

    public void setDteDone(Date dteDone) {
        this.dteDone = dteDone;
    }
    /*
    */
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    /*
    */
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    /*
    Contains the log type. 
    1= Started.
    2= Succeeded.
    */
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }


    @Override
    public int hashCode() {
        return dblid.hashCode();
    }

    public String getRowKey() {
        return dblid.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return dblid;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(dblid);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.Dbmanagementlog[ dblid=" + dblid + " ]";
    }

    @Override
    public String getEntityName() {
        return "Dbmanagementlog";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the dblid fields are not set
        if (!(object instanceof DbmanagementlogBase)) {
            return false;
        }
        DbmanagementlogBase other = (DbmanagementlogBase) object;
        if (this.dblid == null || other.dblid == null) 
            return false;
        
        return this.dblid.equals(other.dblid);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
