/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains issues about the not indisputably classified parcels.



*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "ParcelsIssues.findParcelsIssues", query = "SELECT x FROM ParcelsIssues x WHERE (x.dteCreated = :dteCreated) AND (x.status = :status) AND (x.dteStatusUpdate = :dteStatusUpdate) AND (x.pclaId.pclaId = :pclaId_pclaId)")})
public abstract class ParcelsIssuesBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'parcels_issues_id' είναι υποχρεωτικό")
    @Column(name = "parcels_issues_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="parcels_issues_parcels_issues_id")
    //@SequenceGenerator(name="parcels_issues_parcels_issues_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer parcelsIssuesId;


    @Basic(optional = true)
    @Column(name = "dte_created")
    @Temporal(TemporalType.DATE)
    protected Date dteCreated;


    @Basic(optional = true)
    @Column(name = "status")
    protected Short status;


    @Basic(optional = true)
    @Column(name = "dte_status_update")
    @Temporal(TemporalType.DATE)
    protected Date dteStatusUpdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = true)
    @Column(name = "type_of_issue")
    protected Short typeOfIssue;


    @Basic(optional = true)
    @Column(name = "active")
    protected Boolean active;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "parcelsIssuesId", orphanRemoval=false)
    @OneToMany(mappedBy = "parcelsIssuesId")
    protected List<FmisDecision> fmisDecisionCollection = new ArrayList<FmisDecision>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "parcelsIssuesId", orphanRemoval=false)
    @OneToMany(mappedBy = "parcelsIssuesId")
    protected List<GpDecision> gpDecisionCollection = new ArrayList<GpDecision>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "parcelsIssuesId", orphanRemoval=false)
    @OneToMany(mappedBy = "parcelsIssuesId")
    protected List<GpRequestsContexts> gpRequestsContextsCollection = new ArrayList<GpRequestsContexts>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "parcelsIssuesId", orphanRemoval=false)
    @OneToMany(mappedBy = "parcelsIssuesId")
    protected List<FmisUpload> fmisUploadCollection = new ArrayList<FmisUpload>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "parcelsIssuesId", orphanRemoval=false)
    @OneToMany(mappedBy = "parcelsIssuesId")
    protected List<ParcelsIssuesStatusPerDema> parcelsIssuesStatusPerDemaCollection = new ArrayList<ParcelsIssuesStatusPerDema>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "parcelsIssuesId", orphanRemoval=false)
    @OneToMany(mappedBy = "parcelsIssuesId")
    protected List<ParcelsIssuesActivities> parcelsIssuesActivitiesCollection = new ArrayList<ParcelsIssuesActivities>();


    @JoinColumn(name = "pcla_id", referencedColumnName = "pcla_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'pcla_id' είναι υποχρεωτικό")
    protected ParcelClas pclaId;

    public boolean isPclaIdPresent() {
        if (pclaId==null || pclaId.pclaId == null)
            return false;
        return true;
    }



    public ParcelsIssuesBase() {
    }

    public ParcelsIssuesBase(Integer parcelsIssuesId) {
        this.parcelsIssuesId = parcelsIssuesId;
    }

    public ParcelsIssuesBase(Integer parcelsIssuesId, Date dteCreated, Short status, Date dteStatusUpdate, Integer rowVersion, Short typeOfIssue, Boolean active) {
        this.parcelsIssuesId = parcelsIssuesId;
        this.dteCreated = dteCreated;
        this.status = status;
        this.dteStatusUpdate = dteStatusUpdate;
        this.rowVersion = rowVersion;
        this.typeOfIssue = typeOfIssue;
        this.active = active;
    }

    public ParcelsIssuesBase(Date dteCreated, Short status, Date dteStatusUpdate, Integer rowVersion, Short typeOfIssue, Boolean active, ParcelClas pclaId) {
        this.dteCreated = dteCreated;
        this.status = status;
        this.dteStatusUpdate = dteStatusUpdate;
        this.rowVersion = rowVersion;
        this.typeOfIssue = typeOfIssue;
        this.active = active;
        this.pclaId = pclaId;
    }

    public ParcelsIssues clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "ParcelsIssues:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (ParcelsIssues)alreadyCloned.get(key);

        ParcelsIssues clone = new ParcelsIssues();
        alreadyCloned.put(key, clone);

        clone.setParcelsIssuesId(getParcelsIssuesId());
        clone.setDteCreated(getDteCreated());
        clone.setStatus(getStatus());
        clone.setDteStatusUpdate(getDteStatusUpdate());
        clone.setRowVersion(getRowVersion());
        clone.setTypeOfIssue(getTypeOfIssue());
        clone.setActive(getActive());
        clone.setFmisDecisionCollection(getFmisDecisionCollection());
        clone.setGpDecisionCollection(getGpDecisionCollection());
        clone.setGpRequestsContextsCollection(getGpRequestsContextsCollection());
        clone.setFmisUploadCollection(getFmisUploadCollection());
        clone.setParcelsIssuesStatusPerDemaCollection(getParcelsIssuesStatusPerDemaCollection());
        clone.setParcelsIssuesActivitiesCollection(getParcelsIssuesActivitiesCollection());
        if (pclaId == null || pclaId.pclaId == null) {
            clone.setPclaId(pclaId);
        } else {
            clone.setPclaId(getPclaId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "ParcelsIssues:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(parcelsIssuesId) : parcelsIssuesId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (parcelsIssuesId != null) {
            sb.append("\"parcelsIssuesId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(parcelsIssuesId) : parcelsIssuesId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"parcelsIssuesId\":"); sb.append("null");sb.append(",");
        }
        if (dteCreated != null) {
            sb.append("\"dteCreated\":"); sb.append(dteCreated.getTime()); sb.append(",");
        } else {
            sb.append("\"dteCreated\":"); sb.append("null");sb.append(",");
        }
        if (status != null) {
            sb.append("\"status\":"); sb.append(status);sb.append(",");
        } else {
            sb.append("\"status\":"); sb.append("null");sb.append(",");
        }
        if (dteStatusUpdate != null) {
            sb.append("\"dteStatusUpdate\":"); sb.append(dteStatusUpdate.getTime()); sb.append(",");
        } else {
            sb.append("\"dteStatusUpdate\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (typeOfIssue != null) {
            sb.append("\"typeOfIssue\":"); sb.append(typeOfIssue);sb.append(",");
        } else {
            sb.append("\"typeOfIssue\":"); sb.append("null");sb.append(",");
        }
        if (active != null) {
            sb.append("\"active\":"); sb.append(active);sb.append(",");
        } else {
            sb.append("\"active\":"); sb.append("null");sb.append(",");
        }
        if (pclaId != null && pclaId.pclaId != null) {
            sb.append("\"pclaId\":"); sb.append(getPclaId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (pclaId == null) {
            sb.append("\"pclaId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getParcelsIssuesId() {
        return parcelsIssuesId;
    }

    public void setParcelsIssuesId(Integer parcelsIssuesId) {
        this.parcelsIssuesId = parcelsIssuesId;
    }
    /*
    */
    public Date getDteCreated() {
        return dteCreated;
    }

    public void setDteCreated(Date dteCreated) {
        this.dteCreated = dteCreated;
    }
    /*
    value of 900 (or greater) means inactive.

    value 1 means was created.
    value 2 means at least one photo request has occured.
    value 4 means at least one photo has been uploaded. 
    value 8 means at least a FMIS has been uploaded.
    value 16 means both at least one photo and a FMIS have been uploaded.
    value 32 means crop level photo check has been performed.
    value 64 means crop level FMIS check has been performed.
    value 128 means land cover level photo check has been performed.
    value 256 means cover level FMIS check has been performed.
    value 900 means issue has been solved.
    */
    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }
    /*
    */
    public Date getDteStatusUpdate() {
        return dteStatusUpdate;
    }

    public void setDteStatusUpdate(Date dteStatusUpdate) {
        this.dteStatusUpdate = dteStatusUpdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    Type of Issue
    1=Declared Crop doesn't match with Decision Making
    2= Declated Land Cover doesn't match with Decision Making
    4=Declared Crop doesn't match with Geotagged Photos
    8=Declared Land Cover doesn't match with Geotagged Photos
    16=Declared Crop doesn't match with FMIS
    32=Declared Land Cover doesn't match with FMIS
    */
    public Short getTypeOfIssue() {
        return typeOfIssue;
    }

    public void setTypeOfIssue(Short typeOfIssue) {
        this.typeOfIssue = typeOfIssue;
    }
    /*
    True if the issue is considered totally solved.
    */
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    @XmlTransient
    public List<FmisDecision> getFmisDecisionCollection() {
        return fmisDecisionCollection;
    }

    public void setFmisDecisionCollection(List<FmisDecision> fmisDecisionCollection) {
        this.fmisDecisionCollection = fmisDecisionCollection;
    }

    public void removeFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            child.setParcelsIssuesId(null);
    }

    public void addFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            child.setParcelsIssuesId((ParcelsIssues)this);
    }

    void internalRemoveFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            this.fmisDecisionCollection.remove(child);
    }

    void internalAddFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            this.fmisDecisionCollection.add(child);
    }

    @XmlTransient
    public List<GpDecision> getGpDecisionCollection() {
        return gpDecisionCollection;
    }

    public void setGpDecisionCollection(List<GpDecision> gpDecisionCollection) {
        this.gpDecisionCollection = gpDecisionCollection;
    }

    public void removeGpDecisionCollection(GpDecision child) {
        if (child != null)
            child.setParcelsIssuesId(null);
    }

    public void addGpDecisionCollection(GpDecision child) {
        if (child != null)
            child.setParcelsIssuesId((ParcelsIssues)this);
    }

    void internalRemoveGpDecisionCollection(GpDecision child) {
        if (child != null)
            this.gpDecisionCollection.remove(child);
    }

    void internalAddGpDecisionCollection(GpDecision child) {
        if (child != null)
            this.gpDecisionCollection.add(child);
    }

    @XmlTransient
    public List<GpRequestsContexts> getGpRequestsContextsCollection() {
        return gpRequestsContextsCollection;
    }

    public void setGpRequestsContextsCollection(List<GpRequestsContexts> gpRequestsContextsCollection) {
        this.gpRequestsContextsCollection = gpRequestsContextsCollection;
    }

    public void removeGpRequestsContextsCollection(GpRequestsContexts child) {
        if (child != null)
            child.setParcelsIssuesId(null);
    }

    public void addGpRequestsContextsCollection(GpRequestsContexts child) {
        if (child != null)
            child.setParcelsIssuesId((ParcelsIssues)this);
    }

    void internalRemoveGpRequestsContextsCollection(GpRequestsContexts child) {
        if (child != null)
            this.gpRequestsContextsCollection.remove(child);
    }

    void internalAddGpRequestsContextsCollection(GpRequestsContexts child) {
        if (child != null)
            this.gpRequestsContextsCollection.add(child);
    }

    @XmlTransient
    public List<FmisUpload> getFmisUploadCollection() {
        return fmisUploadCollection;
    }

    public void setFmisUploadCollection(List<FmisUpload> fmisUploadCollection) {
        this.fmisUploadCollection = fmisUploadCollection;
    }

    public void removeFmisUploadCollection(FmisUpload child) {
        if (child != null)
            child.setParcelsIssuesId(null);
    }

    public void addFmisUploadCollection(FmisUpload child) {
        if (child != null)
            child.setParcelsIssuesId((ParcelsIssues)this);
    }

    void internalRemoveFmisUploadCollection(FmisUpload child) {
        if (child != null)
            this.fmisUploadCollection.remove(child);
    }

    void internalAddFmisUploadCollection(FmisUpload child) {
        if (child != null)
            this.fmisUploadCollection.add(child);
    }

    @XmlTransient
    public List<ParcelsIssuesStatusPerDema> getParcelsIssuesStatusPerDemaCollection() {
        return parcelsIssuesStatusPerDemaCollection;
    }

    public void setParcelsIssuesStatusPerDemaCollection(List<ParcelsIssuesStatusPerDema> parcelsIssuesStatusPerDemaCollection) {
        this.parcelsIssuesStatusPerDemaCollection = parcelsIssuesStatusPerDemaCollection;
    }

    public void removeParcelsIssuesStatusPerDemaCollection(ParcelsIssuesStatusPerDema child) {
        if (child != null)
            child.setParcelsIssuesId(null);
    }

    public void addParcelsIssuesStatusPerDemaCollection(ParcelsIssuesStatusPerDema child) {
        if (child != null)
            child.setParcelsIssuesId((ParcelsIssues)this);
    }

    void internalRemoveParcelsIssuesStatusPerDemaCollection(ParcelsIssuesStatusPerDema child) {
        if (child != null)
            this.parcelsIssuesStatusPerDemaCollection.remove(child);
    }

    void internalAddParcelsIssuesStatusPerDemaCollection(ParcelsIssuesStatusPerDema child) {
        if (child != null)
            this.parcelsIssuesStatusPerDemaCollection.add(child);
    }

    @XmlTransient
    public List<ParcelsIssuesActivities> getParcelsIssuesActivitiesCollection() {
        return parcelsIssuesActivitiesCollection;
    }

    public void setParcelsIssuesActivitiesCollection(List<ParcelsIssuesActivities> parcelsIssuesActivitiesCollection) {
        this.parcelsIssuesActivitiesCollection = parcelsIssuesActivitiesCollection;
    }

    public void removeParcelsIssuesActivitiesCollection(ParcelsIssuesActivities child) {
        if (child != null)
            child.setParcelsIssuesId(null);
    }

    public void addParcelsIssuesActivitiesCollection(ParcelsIssuesActivities child) {
        if (child != null)
            child.setParcelsIssuesId((ParcelsIssues)this);
    }

    void internalRemoveParcelsIssuesActivitiesCollection(ParcelsIssuesActivities child) {
        if (child != null)
            this.parcelsIssuesActivitiesCollection.remove(child);
    }

    void internalAddParcelsIssuesActivitiesCollection(ParcelsIssuesActivities child) {
        if (child != null)
            this.parcelsIssuesActivitiesCollection.add(child);
    }

    /*
    */
    public ParcelClas getPclaId() {
        return pclaId;
    }

    public void setPclaId(ParcelClas pclaId) {
    //    if (this.pclaId != null) 
    //        this.pclaId.internalRemoveParcelsIssuesCollection(this);
        this.pclaId = pclaId;
    //    if (pclaId != null)
    //        pclaId.internalAddParcelsIssuesCollection(this);
    }



    @Override
    public int hashCode() {
        return parcelsIssuesId.hashCode();
    }

    public String getRowKey() {
        return parcelsIssuesId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return parcelsIssuesId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(parcelsIssuesId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.ParcelsIssues[ parcelsIssuesId=" + parcelsIssuesId + " ]";
    }

    @Override
    public String getEntityName() {
        return "ParcelsIssues";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the parcelsIssuesId fields are not set
        if (!(object instanceof ParcelsIssuesBase)) {
            return false;
        }
        ParcelsIssuesBase other = (ParcelsIssuesBase) object;
        if (this.parcelsIssuesId == null || other.parcelsIssuesId == null) 
            return false;
        
        return this.parcelsIssuesId.equals(other.parcelsIssuesId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
