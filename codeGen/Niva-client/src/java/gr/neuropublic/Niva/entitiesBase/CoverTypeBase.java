/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
cover_type


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class CoverTypeBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'coty_id' είναι υποχρεωτικό")
    @Column(name = "coty_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cover_type_coty_id")
    //@SequenceGenerator(name="cover_type_coty_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer cotyId;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'code' είναι υποχρεωτικό")
    @Column(name = "code")
    protected Integer code;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "name")
    protected String name;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cotyId", orphanRemoval=false)
    @OneToMany(mappedBy = "cotyId")
    protected List<Cultivation> cultivationCollection = new ArrayList<Cultivation>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cotyId", orphanRemoval=false)
    @OneToMany(mappedBy = "cotyId")
    protected List<EcCult> ecCultCollection = new ArrayList<EcCult>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cotyId", orphanRemoval=false)
    @OneToMany(mappedBy = "cotyId")
    protected List<FmisDecision> fmisDecisionCollection = new ArrayList<FmisDecision>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cotyId", orphanRemoval=false)
    @OneToMany(mappedBy = "cotyId")
    protected List<GpDecision> gpDecisionCollection = new ArrayList<GpDecision>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cotyId", orphanRemoval=false)
    @OneToMany(mappedBy = "cotyId")
    protected List<EcCultCopy> ecCultCopyCollection = new ArrayList<EcCultCopy>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cotyIdDecl", orphanRemoval=false)
    @OneToMany(mappedBy = "cotyIdDecl")
    protected List<ParcelClas> parcelClascoty_id_declCollection = new ArrayList<ParcelClas>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cotyIdPred", orphanRemoval=false)
    @OneToMany(mappedBy = "cotyIdPred")
    protected List<ParcelClas> parcelClascoty_id_predCollection = new ArrayList<ParcelClas>();


    @JoinColumn(name = "exfi_id", referencedColumnName = "id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected ExcelFile exfiId;

    public boolean isExfiIdPresent() {
        if (exfiId==null || exfiId.id == null)
            return false;
        return true;
    }



    public CoverTypeBase() {
    }

    public CoverTypeBase(Integer cotyId) {
        this.cotyId = cotyId;
    }

    public CoverTypeBase(Integer cotyId, Integer code, String name, Integer rowVersion) {
        this.cotyId = cotyId;
        this.code = code;
        this.name = name;
        this.rowVersion = rowVersion;
    }

    public CoverTypeBase(Integer code, String name, Integer rowVersion, ExcelFile exfiId) {
        this.code = code;
        this.name = name;
        this.rowVersion = rowVersion;
        this.exfiId = exfiId;
    }

    public CoverType clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "CoverType:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (CoverType)alreadyCloned.get(key);

        CoverType clone = new CoverType();
        alreadyCloned.put(key, clone);

        clone.setCotyId(getCotyId());
        clone.setCode(getCode());
        clone.setName(getName());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setCultivationCollection(getCultivationCollection());
        clone.setEcCultCollection(getEcCultCollection());
        clone.setFmisDecisionCollection(getFmisDecisionCollection());
        clone.setGpDecisionCollection(getGpDecisionCollection());
        clone.setEcCultCopyCollection(getEcCultCopyCollection());
        clone.setParcelClascoty_id_declCollection(getParcelClascoty_id_declCollection());
        clone.setParcelClascoty_id_predCollection(getParcelClascoty_id_predCollection());
        if (exfiId == null || exfiId.id == null) {
            clone.setExfiId(exfiId);
        } else {
            clone.setExfiId(getExfiId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "CoverType:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(cotyId) : cotyId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (cotyId != null) {
            sb.append("\"cotyId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(cotyId) : cotyId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"cotyId\":"); sb.append("null");sb.append(",");
        }
        if (code != null) {
            sb.append("\"code\":"); sb.append(code);sb.append(",");
        } else {
            sb.append("\"code\":"); sb.append("null");sb.append(",");
        }
        if (name != null) {
            sb.append("\"name\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(name)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"name\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (exfiId != null && exfiId.id != null) {
            sb.append("\"exfiId\":"); sb.append(getExfiId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (exfiId == null) {
            sb.append("\"exfiId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getCotyId() {
        return cotyId;
    }

    public void setCotyId(Integer cotyId) {
        this.cotyId = cotyId;
    }
    /*
    code of crop
    */
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
    /*
    crop name
    */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    @XmlTransient
    public List<Cultivation> getCultivationCollection() {
        return cultivationCollection;
    }

    public void setCultivationCollection(List<Cultivation> cultivationCollection) {
        this.cultivationCollection = cultivationCollection;
    }

    public void removeCultivationCollection(Cultivation child) {
        if (child != null)
            child.setCotyId(null);
    }

    public void addCultivationCollection(Cultivation child) {
        if (child != null)
            child.setCotyId((CoverType)this);
    }

    void internalRemoveCultivationCollection(Cultivation child) {
        if (child != null)
            this.cultivationCollection.remove(child);
    }

    void internalAddCultivationCollection(Cultivation child) {
        if (child != null)
            this.cultivationCollection.add(child);
    }

    @XmlTransient
    public List<EcCult> getEcCultCollection() {
        return ecCultCollection;
    }

    public void setEcCultCollection(List<EcCult> ecCultCollection) {
        this.ecCultCollection = ecCultCollection;
    }

    public void removeEcCultCollection(EcCult child) {
        if (child != null)
            child.setCotyId(null);
    }

    public void addEcCultCollection(EcCult child) {
        if (child != null)
            child.setCotyId((CoverType)this);
    }

    void internalRemoveEcCultCollection(EcCult child) {
        if (child != null)
            this.ecCultCollection.remove(child);
    }

    void internalAddEcCultCollection(EcCult child) {
        if (child != null)
            this.ecCultCollection.add(child);
    }

    @XmlTransient
    public List<FmisDecision> getFmisDecisionCollection() {
        return fmisDecisionCollection;
    }

    public void setFmisDecisionCollection(List<FmisDecision> fmisDecisionCollection) {
        this.fmisDecisionCollection = fmisDecisionCollection;
    }

    public void removeFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            child.setCotyId(null);
    }

    public void addFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            child.setCotyId((CoverType)this);
    }

    void internalRemoveFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            this.fmisDecisionCollection.remove(child);
    }

    void internalAddFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            this.fmisDecisionCollection.add(child);
    }

    @XmlTransient
    public List<GpDecision> getGpDecisionCollection() {
        return gpDecisionCollection;
    }

    public void setGpDecisionCollection(List<GpDecision> gpDecisionCollection) {
        this.gpDecisionCollection = gpDecisionCollection;
    }

    public void removeGpDecisionCollection(GpDecision child) {
        if (child != null)
            child.setCotyId(null);
    }

    public void addGpDecisionCollection(GpDecision child) {
        if (child != null)
            child.setCotyId((CoverType)this);
    }

    void internalRemoveGpDecisionCollection(GpDecision child) {
        if (child != null)
            this.gpDecisionCollection.remove(child);
    }

    void internalAddGpDecisionCollection(GpDecision child) {
        if (child != null)
            this.gpDecisionCollection.add(child);
    }

    @XmlTransient
    public List<EcCultCopy> getEcCultCopyCollection() {
        return ecCultCopyCollection;
    }

    public void setEcCultCopyCollection(List<EcCultCopy> ecCultCopyCollection) {
        this.ecCultCopyCollection = ecCultCopyCollection;
    }

    public void removeEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            child.setCotyId(null);
    }

    public void addEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            child.setCotyId((CoverType)this);
    }

    void internalRemoveEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            this.ecCultCopyCollection.remove(child);
    }

    void internalAddEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            this.ecCultCopyCollection.add(child);
    }

    @XmlTransient
    public List<ParcelClas> getParcelClascoty_id_declCollection() {
        return parcelClascoty_id_declCollection;
    }

    public void setParcelClascoty_id_declCollection(List<ParcelClas> parcelClascoty_id_declCollection) {
        this.parcelClascoty_id_declCollection = parcelClascoty_id_declCollection;
    }

    public void removeParcelClascoty_id_declCollection(ParcelClas child) {
        if (child != null)
            child.setCotyIdDecl(null);
    }

    public void addParcelClascoty_id_declCollection(ParcelClas child) {
        if (child != null)
            child.setCotyIdDecl((CoverType)this);
    }

    void internalRemoveParcelClascoty_id_declCollection(ParcelClas child) {
        if (child != null)
            this.parcelClascoty_id_declCollection.remove(child);
    }

    void internalAddParcelClascoty_id_declCollection(ParcelClas child) {
        if (child != null)
            this.parcelClascoty_id_declCollection.add(child);
    }

    @XmlTransient
    public List<ParcelClas> getParcelClascoty_id_predCollection() {
        return parcelClascoty_id_predCollection;
    }

    public void setParcelClascoty_id_predCollection(List<ParcelClas> parcelClascoty_id_predCollection) {
        this.parcelClascoty_id_predCollection = parcelClascoty_id_predCollection;
    }

    public void removeParcelClascoty_id_predCollection(ParcelClas child) {
        if (child != null)
            child.setCotyIdPred(null);
    }

    public void addParcelClascoty_id_predCollection(ParcelClas child) {
        if (child != null)
            child.setCotyIdPred((CoverType)this);
    }

    void internalRemoveParcelClascoty_id_predCollection(ParcelClas child) {
        if (child != null)
            this.parcelClascoty_id_predCollection.remove(child);
    }

    void internalAddParcelClascoty_id_predCollection(ParcelClas child) {
        if (child != null)
            this.parcelClascoty_id_predCollection.add(child);
    }

    /*
    */
    public ExcelFile getExfiId() {
        return exfiId;
    }

    public void setExfiId(ExcelFile exfiId) {
    //    if (this.exfiId != null) 
    //        this.exfiId.internalRemoveCoverTypeCollection(this);
        this.exfiId = exfiId;
    //    if (exfiId != null)
    //        exfiId.internalAddCoverTypeCollection(this);
    }



    @Override
    public int hashCode() {
        return cotyId.hashCode();
    }

    public String getRowKey() {
        return cotyId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return cotyId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(cotyId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.CoverType[ cotyId=" + cotyId + " ]";
    }

    @Override
    public String getEntityName() {
        return "CoverType";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the cotyId fields are not set
        if (!(object instanceof CoverTypeBase)) {
            return false;
        }
        CoverTypeBase other = (CoverTypeBase) object;
        if (this.cotyId == null || other.cotyId == null) 
            return false;
        
        return this.cotyId.equals(other.cotyId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
