/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains the decisions per parcel, based on all modules.


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "Integrateddecision.findIntegrateddecision", query = "SELECT x FROM Integrateddecision x WHERE (x.decisionCode = :decisionCode) AND (x.dteUpdate = :dteUpdate) AND (x.usrUpdate like :usrUpdate) AND (x.pclaId.pclaId = :pclaId_pclaId) AND (x.demaId.demaId = :demaId_demaId)")})
public abstract class IntegrateddecisionBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'integrateddecisions_id' είναι υποχρεωτικό")
    @Column(name = "integrateddecisions_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="integrateddecisions_integrateddecisions_id")
    //@SequenceGenerator(name="integrateddecisions_integrateddecisions_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer integrateddecisionsId;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'decision_code' είναι υποχρεωτικό")
    @Column(name = "decision_code")
    protected Short decisionCode;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'dte_update' είναι υποχρεωτικό")
    @Column(name = "dte_update")
    @Temporal(TemporalType.DATE)
    protected Date dteUpdate;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usr_update' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usr_update")
    protected String usrUpdate;


    @JoinColumn(name = "pcla_id", referencedColumnName = "pcla_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'pcla_id' είναι υποχρεωτικό")
    protected ParcelClas pclaId;

    public boolean isPclaIdPresent() {
        if (pclaId==null || pclaId.pclaId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "dema_id", referencedColumnName = "dema_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'dema_id' είναι υποχρεωτικό")
    protected DecisionMaking demaId;

    public boolean isDemaIdPresent() {
        if (demaId==null || demaId.demaId == null)
            return false;
        return true;
    }



    public IntegrateddecisionBase() {
    }

    public IntegrateddecisionBase(Integer integrateddecisionsId) {
        this.integrateddecisionsId = integrateddecisionsId;
    }

    public IntegrateddecisionBase(Integer integrateddecisionsId, Short decisionCode, Date dteUpdate, String usrUpdate) {
        this.integrateddecisionsId = integrateddecisionsId;
        this.decisionCode = decisionCode;
        this.dteUpdate = dteUpdate;
        this.usrUpdate = usrUpdate;
    }

    public IntegrateddecisionBase(Short decisionCode, Date dteUpdate, String usrUpdate, ParcelClas pclaId, DecisionMaking demaId) {
        this.decisionCode = decisionCode;
        this.dteUpdate = dteUpdate;
        this.usrUpdate = usrUpdate;
        this.pclaId = pclaId;
        this.demaId = demaId;
    }

    public Integrateddecision clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "Integrateddecision:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (Integrateddecision)alreadyCloned.get(key);

        Integrateddecision clone = new Integrateddecision();
        alreadyCloned.put(key, clone);

        clone.setIntegrateddecisionsId(getIntegrateddecisionsId());
        clone.setDecisionCode(getDecisionCode());
        clone.setDteUpdate(getDteUpdate());
        clone.setUsrUpdate(getUsrUpdate());
        if (pclaId == null || pclaId.pclaId == null) {
            clone.setPclaId(pclaId);
        } else {
            clone.setPclaId(getPclaId().clone(alreadyCloned));
        }
        if (demaId == null || demaId.demaId == null) {
            clone.setDemaId(demaId);
        } else {
            clone.setDemaId(getDemaId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "Integrateddecision:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(integrateddecisionsId) : integrateddecisionsId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (integrateddecisionsId != null) {
            sb.append("\"integrateddecisionsId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(integrateddecisionsId) : integrateddecisionsId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"integrateddecisionsId\":"); sb.append("null");sb.append(",");
        }
        if (decisionCode != null) {
            sb.append("\"decisionCode\":"); sb.append(decisionCode);sb.append(",");
        } else {
            sb.append("\"decisionCode\":"); sb.append("null");sb.append(",");
        }
        if (dteUpdate != null) {
            sb.append("\"dteUpdate\":"); sb.append(dteUpdate.getTime()); sb.append(",");
        } else {
            sb.append("\"dteUpdate\":"); sb.append("null");sb.append(",");
        }
        if (usrUpdate != null) {
            sb.append("\"usrUpdate\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(usrUpdate)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"usrUpdate\":"); sb.append("null");sb.append(",");
        }
        if (pclaId != null && pclaId.pclaId != null) {
            sb.append("\"pclaId\":"); sb.append(getPclaId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (pclaId == null) {
            sb.append("\"pclaId\":"); sb.append("null");  sb.append(",");
        }
        if (demaId != null && demaId.demaId != null) {
            sb.append("\"demaId\":"); sb.append(getDemaId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (demaId == null) {
            sb.append("\"demaId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getIntegrateddecisionsId() {
        return integrateddecisionsId;
    }

    public void setIntegrateddecisionsId(Integer integrateddecisionsId) {
        this.integrateddecisionsId = integrateddecisionsId;
    }
    /*
    0=Unevaluated
    1=Green
    2=Yellow
    3=Red
    4=Undefined
    */
    public Short getDecisionCode() {
        return decisionCode;
    }

    public void setDecisionCode(Short decisionCode) {
        this.decisionCode = decisionCode;
    }
    /*
    */
    public Date getDteUpdate() {
        return dteUpdate;
    }

    public void setDteUpdate(Date dteUpdate) {
        this.dteUpdate = dteUpdate;
    }
    /*
    */
    public String getUsrUpdate() {
        return usrUpdate;
    }

    public void setUsrUpdate(String usrUpdate) {
        this.usrUpdate = usrUpdate;
    }
    /*
    */
    public ParcelClas getPclaId() {
        return pclaId;
    }

    public void setPclaId(ParcelClas pclaId) {
    //    if (this.pclaId != null) 
    //        this.pclaId.internalRemoveIntegrateddecisionCollection(this);
        this.pclaId = pclaId;
    //    if (pclaId != null)
    //        pclaId.internalAddIntegrateddecisionCollection(this);
    }

    /*
    */
    public DecisionMaking getDemaId() {
        return demaId;
    }

    public void setDemaId(DecisionMaking demaId) {
    //    if (this.demaId != null) 
    //        this.demaId.internalRemoveIntegrateddecisionCollection(this);
        this.demaId = demaId;
    //    if (demaId != null)
    //        demaId.internalAddIntegrateddecisionCollection(this);
    }



    @Override
    public int hashCode() {
        return integrateddecisionsId.hashCode();
    }

    public String getRowKey() {
        return integrateddecisionsId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return integrateddecisionsId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(integrateddecisionsId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.Integrateddecision[ integrateddecisionsId=" + integrateddecisionsId + " ]";
    }

    @Override
    public String getEntityName() {
        return "Integrateddecision";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the integrateddecisionsId fields are not set
        if (!(object instanceof IntegrateddecisionBase)) {
            return false;
        }
        IntegrateddecisionBase other = (IntegrateddecisionBase) object;
        if (this.integrateddecisionsId == null || other.integrateddecisionsId == null) 
            return false;
        
        return this.integrateddecisionsId.equals(other.integrateddecisionsId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
