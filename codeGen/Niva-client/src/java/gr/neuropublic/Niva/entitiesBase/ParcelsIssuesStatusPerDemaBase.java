/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Status of a parcels issue related to a decision making.


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class ParcelsIssuesStatusPerDemaBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'status' είναι υποχρεωτικό")
    @Column(name = "status")
    protected Short status;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'dte_status_update' είναι υποχρεωτικό")
    @Column(name = "dte_status_update")
    @Temporal(TemporalType.DATE)
    protected Date dteStatusUpdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'parcels_issues_status_per_dema_id' είναι υποχρεωτικό")
    @Column(name = "parcels_issues_status_per_dema_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="parcels_issues_status_per_dema_parcels_issues_status_per_dema_id")
    //@SequenceGenerator(name="parcels_issues_status_per_dema_parcels_issues_status_per_dema_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer parcelsIssuesStatusPerDemaId;


    @JoinColumn(name = "parcels_issues_id", referencedColumnName = "parcels_issues_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'parcels_issues_id' είναι υποχρεωτικό")
    protected ParcelsIssues parcelsIssuesId;

    public boolean isParcelsIssuesIdPresent() {
        if (parcelsIssuesId==null || parcelsIssuesId.parcelsIssuesId == null)
            return false;
        return true;
    }



    public ParcelsIssuesStatusPerDemaBase() {
    }

    public ParcelsIssuesStatusPerDemaBase(Integer parcelsIssuesStatusPerDemaId) {
        this.parcelsIssuesStatusPerDemaId = parcelsIssuesStatusPerDemaId;
    }

    public ParcelsIssuesStatusPerDemaBase(Integer parcelsIssuesStatusPerDemaId, Short status, Date dteStatusUpdate, Integer rowVersion) {
        this.parcelsIssuesStatusPerDemaId = parcelsIssuesStatusPerDemaId;
        this.status = status;
        this.dteStatusUpdate = dteStatusUpdate;
        this.rowVersion = rowVersion;
    }

    public ParcelsIssuesStatusPerDemaBase(Short status, Date dteStatusUpdate, Integer rowVersion, ParcelsIssues parcelsIssuesId) {
        this.status = status;
        this.dteStatusUpdate = dteStatusUpdate;
        this.rowVersion = rowVersion;
        this.parcelsIssuesId = parcelsIssuesId;
    }

    public ParcelsIssuesStatusPerDema clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "ParcelsIssuesStatusPerDema:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (ParcelsIssuesStatusPerDema)alreadyCloned.get(key);

        ParcelsIssuesStatusPerDema clone = new ParcelsIssuesStatusPerDema();
        alreadyCloned.put(key, clone);

        clone.setStatus(getStatus());
        clone.setDteStatusUpdate(getDteStatusUpdate());
        clone.setRowVersion(getRowVersion());
        clone.setParcelsIssuesStatusPerDemaId(getParcelsIssuesStatusPerDemaId());
        if (parcelsIssuesId == null || parcelsIssuesId.parcelsIssuesId == null) {
            clone.setParcelsIssuesId(parcelsIssuesId);
        } else {
            clone.setParcelsIssuesId(getParcelsIssuesId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "ParcelsIssuesStatusPerDema:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(parcelsIssuesStatusPerDemaId) : parcelsIssuesStatusPerDemaId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (status != null) {
            sb.append("\"status\":"); sb.append(status);sb.append(",");
        } else {
            sb.append("\"status\":"); sb.append("null");sb.append(",");
        }
        if (dteStatusUpdate != null) {
            sb.append("\"dteStatusUpdate\":"); sb.append(dteStatusUpdate.getTime()); sb.append(",");
        } else {
            sb.append("\"dteStatusUpdate\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (parcelsIssuesStatusPerDemaId != null) {
            sb.append("\"parcelsIssuesStatusPerDemaId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(parcelsIssuesStatusPerDemaId) : parcelsIssuesStatusPerDemaId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"parcelsIssuesStatusPerDemaId\":"); sb.append("null");sb.append(",");
        }
        if (parcelsIssuesId != null && parcelsIssuesId.parcelsIssuesId != null) {
            sb.append("\"parcelsIssuesId\":"); sb.append(getParcelsIssuesId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (parcelsIssuesId == null) {
            sb.append("\"parcelsIssuesId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }
    /*
    */
    public Date getDteStatusUpdate() {
        return dteStatusUpdate;
    }

    public void setDteStatusUpdate(Date dteStatusUpdate) {
        this.dteStatusUpdate = dteStatusUpdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Integer getParcelsIssuesStatusPerDemaId() {
        return parcelsIssuesStatusPerDemaId;
    }

    public void setParcelsIssuesStatusPerDemaId(Integer parcelsIssuesStatusPerDemaId) {
        this.parcelsIssuesStatusPerDemaId = parcelsIssuesStatusPerDemaId;
    }
    /*
    */
    public ParcelsIssues getParcelsIssuesId() {
        return parcelsIssuesId;
    }

    public void setParcelsIssuesId(ParcelsIssues parcelsIssuesId) {
    //    if (this.parcelsIssuesId != null) 
    //        this.parcelsIssuesId.internalRemoveParcelsIssuesStatusPerDemaCollection(this);
        this.parcelsIssuesId = parcelsIssuesId;
    //    if (parcelsIssuesId != null)
    //        parcelsIssuesId.internalAddParcelsIssuesStatusPerDemaCollection(this);
    }



    @Override
    public int hashCode() {
        return parcelsIssuesStatusPerDemaId.hashCode();
    }

    public String getRowKey() {
        return parcelsIssuesStatusPerDemaId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return parcelsIssuesStatusPerDemaId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(parcelsIssuesStatusPerDemaId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.ParcelsIssuesStatusPerDema[ parcelsIssuesStatusPerDemaId=" + parcelsIssuesStatusPerDemaId + " ]";
    }

    @Override
    public String getEntityName() {
        return "ParcelsIssuesStatusPerDema";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the parcelsIssuesStatusPerDemaId fields are not set
        if (!(object instanceof ParcelsIssuesStatusPerDemaBase)) {
            return false;
        }
        ParcelsIssuesStatusPerDemaBase other = (ParcelsIssuesStatusPerDemaBase) object;
        if (this.parcelsIssuesStatusPerDemaId == null || other.parcelsIssuesStatusPerDemaId == null) 
            return false;
        
        return this.parcelsIssuesStatusPerDemaId.equals(other.parcelsIssuesStatusPerDemaId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
