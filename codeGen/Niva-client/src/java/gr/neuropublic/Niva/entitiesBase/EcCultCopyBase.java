/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class EcCultCopyBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'eccc_id' είναι υποχρεωτικό")
    @Column(name = "eccc_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ec_cult_copy_eccc_id")
    //@SequenceGenerator(name="ec_cult_copy_eccc_id", sequenceName="niva.ec_cult_copy_eccc_id_seq", allocationSize=1)
    protected Integer ecccId;


    @Basic(optional = true)
    @Column(name = "none_match_decision")
    protected Integer noneMatchDecision;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "ecccId", orphanRemoval=false)
    @OneToMany(mappedBy = "ecccId")
    protected List<EcCultDetailCopy> ecCultDetailCopyCollection = new ArrayList<EcCultDetailCopy>();


    @JoinColumn(name = "cult_id", referencedColumnName = "cult_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected Cultivation cultId;

    public boolean isCultIdPresent() {
        if (cultId==null || cultId.cultId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "ecgc_id", referencedColumnName = "ecgc_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'ecgc_id' είναι υποχρεωτικό")
    protected EcGroupCopy ecgcId;

    public boolean isEcgcIdPresent() {
        if (ecgcId==null || ecgcId.ecgcId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "coty_id", referencedColumnName = "coty_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected CoverType cotyId;

    public boolean isCotyIdPresent() {
        if (cotyId==null || cotyId.cotyId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "suca_id", referencedColumnName = "suca_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected SuperClas sucaId;

    public boolean isSucaIdPresent() {
        if (sucaId==null || sucaId.sucaId == null)
            return false;
        return true;
    }



    public EcCultCopyBase() {
    }

    public EcCultCopyBase(Integer ecccId) {
        this.ecccId = ecccId;
    }

    public EcCultCopyBase(Integer ecccId, Integer noneMatchDecision, Integer rowVersion) {
        this.ecccId = ecccId;
        this.noneMatchDecision = noneMatchDecision;
        this.rowVersion = rowVersion;
    }

    public EcCultCopyBase(Integer noneMatchDecision, Integer rowVersion, Cultivation cultId, EcGroupCopy ecgcId, CoverType cotyId, SuperClas sucaId) {
        this.noneMatchDecision = noneMatchDecision;
        this.rowVersion = rowVersion;
        this.cultId = cultId;
        this.ecgcId = ecgcId;
        this.cotyId = cotyId;
        this.sucaId = sucaId;
    }

    public EcCultCopy clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "EcCultCopy:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (EcCultCopy)alreadyCloned.get(key);

        EcCultCopy clone = new EcCultCopy();
        alreadyCloned.put(key, clone);

        clone.setEcccId(getEcccId());
        clone.setNoneMatchDecision(getNoneMatchDecision());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setEcCultDetailCopyCollection(getEcCultDetailCopyCollection());
        if (cultId == null || cultId.cultId == null) {
            clone.setCultId(cultId);
        } else {
            clone.setCultId(getCultId().clone(alreadyCloned));
        }
        if (ecgcId == null || ecgcId.ecgcId == null) {
            clone.setEcgcId(ecgcId);
        } else {
            clone.setEcgcId(getEcgcId().clone(alreadyCloned));
        }
        if (cotyId == null || cotyId.cotyId == null) {
            clone.setCotyId(cotyId);
        } else {
            clone.setCotyId(getCotyId().clone(alreadyCloned));
        }
        if (sucaId == null || sucaId.sucaId == null) {
            clone.setSucaId(sucaId);
        } else {
            clone.setSucaId(getSucaId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "EcCultCopy:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(ecccId) : ecccId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (ecccId != null) {
            sb.append("\"ecccId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(ecccId) : ecccId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"ecccId\":"); sb.append("null");sb.append(",");
        }
        if (noneMatchDecision != null) {
            sb.append("\"noneMatchDecision\":"); sb.append(noneMatchDecision);sb.append(",");
        } else {
            sb.append("\"noneMatchDecision\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (cultId != null && cultId.cultId != null) {
            sb.append("\"cultId\":"); sb.append(getCultId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cultId == null) {
            sb.append("\"cultId\":"); sb.append("null");  sb.append(",");
        }
        if (ecgcId != null && ecgcId.ecgcId != null) {
            sb.append("\"ecgcId\":"); sb.append(getEcgcId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (ecgcId == null) {
            sb.append("\"ecgcId\":"); sb.append("null");  sb.append(",");
        }
        if (cotyId != null && cotyId.cotyId != null) {
            sb.append("\"cotyId\":"); sb.append(getCotyId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cotyId == null) {
            sb.append("\"cotyId\":"); sb.append("null");  sb.append(",");
        }
        if (sucaId != null && sucaId.sucaId != null) {
            sb.append("\"sucaId\":"); sb.append(getSucaId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (sucaId == null) {
            sb.append("\"sucaId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getEcccId() {
        return ecccId;
    }

    public void setEcccId(Integer ecccId) {
        this.ecccId = ecccId;
    }
    /*
    */
    public Integer getNoneMatchDecision() {
        return noneMatchDecision;
    }

    public void setNoneMatchDecision(Integer noneMatchDecision) {
        this.noneMatchDecision = noneMatchDecision;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    @XmlTransient
    public List<EcCultDetailCopy> getEcCultDetailCopyCollection() {
        return ecCultDetailCopyCollection;
    }

    public void setEcCultDetailCopyCollection(List<EcCultDetailCopy> ecCultDetailCopyCollection) {
        this.ecCultDetailCopyCollection = ecCultDetailCopyCollection;
    }

    public void removeEcCultDetailCopyCollection(EcCultDetailCopy child) {
        if (child != null)
            child.setEcccId(null);
    }

    public void addEcCultDetailCopyCollection(EcCultDetailCopy child) {
        if (child != null)
            child.setEcccId((EcCultCopy)this);
    }

    void internalRemoveEcCultDetailCopyCollection(EcCultDetailCopy child) {
        if (child != null)
            this.ecCultDetailCopyCollection.remove(child);
    }

    void internalAddEcCultDetailCopyCollection(EcCultDetailCopy child) {
        if (child != null)
            this.ecCultDetailCopyCollection.add(child);
    }

    /*
    */
    public Cultivation getCultId() {
        return cultId;
    }

    public void setCultId(Cultivation cultId) {
    //    if (this.cultId != null) 
    //        this.cultId.internalRemoveEcCultCopyCollection(this);
        this.cultId = cultId;
    //    if (cultId != null)
    //        cultId.internalAddEcCultCopyCollection(this);
    }

    /*
    */
    public EcGroupCopy getEcgcId() {
        return ecgcId;
    }

    public void setEcgcId(EcGroupCopy ecgcId) {
    //    if (this.ecgcId != null) 
    //        this.ecgcId.internalRemoveEcCultCopyCollection(this);
        this.ecgcId = ecgcId;
    //    if (ecgcId != null)
    //        ecgcId.internalAddEcCultCopyCollection(this);
    }

    /*
    */
    public CoverType getCotyId() {
        return cotyId;
    }

    public void setCotyId(CoverType cotyId) {
    //    if (this.cotyId != null) 
    //        this.cotyId.internalRemoveEcCultCopyCollection(this);
        this.cotyId = cotyId;
    //    if (cotyId != null)
    //        cotyId.internalAddEcCultCopyCollection(this);
    }

    /*
    */
    public SuperClas getSucaId() {
        return sucaId;
    }

    public void setSucaId(SuperClas sucaId) {
    //    if (this.sucaId != null) 
    //        this.sucaId.internalRemoveEcCultCopyCollection(this);
        this.sucaId = sucaId;
    //    if (sucaId != null)
    //        sucaId.internalAddEcCultCopyCollection(this);
    }



    @Override
    public int hashCode() {
        return ecccId.hashCode();
    }

    public String getRowKey() {
        return ecccId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return ecccId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(ecccId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.EcCultCopy[ ecccId=" + ecccId + " ]";
    }

    @Override
    public String getEntityName() {
        return "EcCultCopy";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the ecccId fields are not set
        if (!(object instanceof EcCultCopyBase)) {
            return false;
        }
        EcCultCopyBase other = (EcCultCopyBase) object;
        if (this.ecccId == null || other.ecccId == null) 
            return false;
        
        return this.ecccId.equals(other.ecccId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
