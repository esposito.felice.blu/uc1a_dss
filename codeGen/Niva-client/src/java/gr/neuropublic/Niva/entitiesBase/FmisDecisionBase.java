/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains the decisions related to FMIS for each parcel issue (set of photos).


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class FmisDecisionBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'fmis_decisions_id' είναι υποχρεωτικό")
    @Column(name = "fmis_decisions_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="fmis_decisions_fmis_decisions_id")
    //@SequenceGenerator(name="fmis_decisions_fmis_decisions_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer fmisDecisionsId;


    @Basic(optional = true)
    @Column(name = "crop_ok")
    protected Short cropOk;


    @Basic(optional = true)
    @Column(name = "landcover_ok")
    protected Short landcoverOk;


    @Basic(optional = true)
    @Column(name = "dte_insert")
    @Temporal(TemporalType.DATE)
    protected Date dteInsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usr_insert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usr_insert")
    protected String usrInsert;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @JoinColumn(name = "parcels_issues_id", referencedColumnName = "parcels_issues_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'parcels_issues_id' είναι υποχρεωτικό")
    protected ParcelsIssues parcelsIssuesId;

    public boolean isParcelsIssuesIdPresent() {
        if (parcelsIssuesId==null || parcelsIssuesId.parcelsIssuesId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "cult_id", referencedColumnName = "cult_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'cult_id' είναι υποχρεωτικό")
    protected Cultivation cultId;

    public boolean isCultIdPresent() {
        if (cultId==null || cultId.cultId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "coty_id", referencedColumnName = "coty_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'coty_id' είναι υποχρεωτικό")
    protected CoverType cotyId;

    public boolean isCotyIdPresent() {
        if (cotyId==null || cotyId.cotyId == null)
            return false;
        return true;
    }



    public FmisDecisionBase() {
    }

    public FmisDecisionBase(Integer fmisDecisionsId) {
        this.fmisDecisionsId = fmisDecisionsId;
    }

    public FmisDecisionBase(Integer fmisDecisionsId, Short cropOk, Short landcoverOk, Date dteInsert, String usrInsert, Integer rowVersion) {
        this.fmisDecisionsId = fmisDecisionsId;
        this.cropOk = cropOk;
        this.landcoverOk = landcoverOk;
        this.dteInsert = dteInsert;
        this.usrInsert = usrInsert;
        this.rowVersion = rowVersion;
    }

    public FmisDecisionBase(Short cropOk, Short landcoverOk, Date dteInsert, String usrInsert, Integer rowVersion, ParcelsIssues parcelsIssuesId, Cultivation cultId, CoverType cotyId) {
        this.cropOk = cropOk;
        this.landcoverOk = landcoverOk;
        this.dteInsert = dteInsert;
        this.usrInsert = usrInsert;
        this.rowVersion = rowVersion;
        this.parcelsIssuesId = parcelsIssuesId;
        this.cultId = cultId;
        this.cotyId = cotyId;
    }

    public FmisDecision clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "FmisDecision:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (FmisDecision)alreadyCloned.get(key);

        FmisDecision clone = new FmisDecision();
        alreadyCloned.put(key, clone);

        clone.setFmisDecisionsId(getFmisDecisionsId());
        clone.setCropOk(getCropOk());
        clone.setLandcoverOk(getLandcoverOk());
        clone.setDteInsert(getDteInsert());
        clone.setUsrInsert(getUsrInsert());
        clone.setRowVersion(getRowVersion());
        if (parcelsIssuesId == null || parcelsIssuesId.parcelsIssuesId == null) {
            clone.setParcelsIssuesId(parcelsIssuesId);
        } else {
            clone.setParcelsIssuesId(getParcelsIssuesId().clone(alreadyCloned));
        }
        if (cultId == null || cultId.cultId == null) {
            clone.setCultId(cultId);
        } else {
            clone.setCultId(getCultId().clone(alreadyCloned));
        }
        if (cotyId == null || cotyId.cotyId == null) {
            clone.setCotyId(cotyId);
        } else {
            clone.setCotyId(getCotyId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "FmisDecision:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(fmisDecisionsId) : fmisDecisionsId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (fmisDecisionsId != null) {
            sb.append("\"fmisDecisionsId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(fmisDecisionsId) : fmisDecisionsId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"fmisDecisionsId\":"); sb.append("null");sb.append(",");
        }
        if (cropOk != null) {
            sb.append("\"cropOk\":"); sb.append(cropOk);sb.append(",");
        } else {
            sb.append("\"cropOk\":"); sb.append("null");sb.append(",");
        }
        if (landcoverOk != null) {
            sb.append("\"landcoverOk\":"); sb.append(landcoverOk);sb.append(",");
        } else {
            sb.append("\"landcoverOk\":"); sb.append("null");sb.append(",");
        }
        if (dteInsert != null) {
            sb.append("\"dteInsert\":"); sb.append(dteInsert.getTime()); sb.append(",");
        } else {
            sb.append("\"dteInsert\":"); sb.append("null");sb.append(",");
        }
        if (usrInsert != null) {
            sb.append("\"usrInsert\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(usrInsert)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"usrInsert\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (parcelsIssuesId != null && parcelsIssuesId.parcelsIssuesId != null) {
            sb.append("\"parcelsIssuesId\":"); sb.append(getParcelsIssuesId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (parcelsIssuesId == null) {
            sb.append("\"parcelsIssuesId\":"); sb.append("null");  sb.append(",");
        }
        if (cultId != null && cultId.cultId != null) {
            sb.append("\"cultId\":"); sb.append(getCultId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cultId == null) {
            sb.append("\"cultId\":"); sb.append("null");  sb.append(",");
        }
        if (cotyId != null && cotyId.cotyId != null) {
            sb.append("\"cotyId\":"); sb.append(getCotyId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cotyId == null) {
            sb.append("\"cotyId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getFmisDecisionsId() {
        return fmisDecisionsId;
    }

    public void setFmisDecisionsId(Integer fmisDecisionsId) {
        this.fmisDecisionsId = fmisDecisionsId;
    }
    /*
    */
    public Short getCropOk() {
        return cropOk;
    }

    public void setCropOk(Short cropOk) {
        this.cropOk = cropOk;
    }
    /*
    Compatibility with the declared landcover
    0=Not Evaluated
    1=Yes
    2=No
    3=Unclear
    */
    public Short getLandcoverOk() {
        return landcoverOk;
    }

    public void setLandcoverOk(Short landcoverOk) {
        this.landcoverOk = landcoverOk;
    }
    /*
    */
    public Date getDteInsert() {
        return dteInsert;
    }

    public void setDteInsert(Date dteInsert) {
        this.dteInsert = dteInsert;
    }
    /*
    */
    public String getUsrInsert() {
        return usrInsert;
    }

    public void setUsrInsert(String usrInsert) {
        this.usrInsert = usrInsert;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public ParcelsIssues getParcelsIssuesId() {
        return parcelsIssuesId;
    }

    public void setParcelsIssuesId(ParcelsIssues parcelsIssuesId) {
    //    if (this.parcelsIssuesId != null) 
    //        this.parcelsIssuesId.internalRemoveFmisDecisionCollection(this);
        this.parcelsIssuesId = parcelsIssuesId;
    //    if (parcelsIssuesId != null)
    //        parcelsIssuesId.internalAddFmisDecisionCollection(this);
    }

    /*
    */
    public Cultivation getCultId() {
        return cultId;
    }

    public void setCultId(Cultivation cultId) {
    //    if (this.cultId != null) 
    //        this.cultId.internalRemoveFmisDecisionCollection(this);
        this.cultId = cultId;
    //    if (cultId != null)
    //        cultId.internalAddFmisDecisionCollection(this);
    }

    /*
    */
    public CoverType getCotyId() {
        return cotyId;
    }

    public void setCotyId(CoverType cotyId) {
    //    if (this.cotyId != null) 
    //        this.cotyId.internalRemoveFmisDecisionCollection(this);
        this.cotyId = cotyId;
    //    if (cotyId != null)
    //        cotyId.internalAddFmisDecisionCollection(this);
    }



    @Override
    public int hashCode() {
        return fmisDecisionsId.hashCode();
    }

    public String getRowKey() {
        return fmisDecisionsId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return fmisDecisionsId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(fmisDecisionsId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.FmisDecision[ fmisDecisionsId=" + fmisDecisionsId + " ]";
    }

    @Override
    public String getEntityName() {
        return "FmisDecision";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the fmisDecisionsId fields are not set
        if (!(object instanceof FmisDecisionBase)) {
            return false;
        }
        FmisDecisionBase other = (FmisDecisionBase) object;
        if (this.fmisDecisionsId == null || other.fmisDecisionsId == null) 
            return false;
        
        return this.fmisDecisionsId.equals(other.fmisDecisionsId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
