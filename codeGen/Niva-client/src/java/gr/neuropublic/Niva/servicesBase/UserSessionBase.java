package gr.neuropublic.Niva.servicesBase;

import java.math.BigInteger;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;

public class UserSessionBase extends gr.neuropublic.base.UserSession {
    public UserSessionBase(
            final Integer userId, 
            final String usrEmail,
            final String userVat,
            final Integer subsId,
            final String subsCode,
            final String subsDescription,
            final Integer fromSubsId,
            final List<String> privileges,
            final List<Integer> subSecClasses,
            final String userSalt,
            final String clientIp) {
        super(userId, usrEmail, userVat, subsId, subsCode, subsDescription, fromSubsId, "Niva", privileges, subSecClasses, userSalt, clientIp );
    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
