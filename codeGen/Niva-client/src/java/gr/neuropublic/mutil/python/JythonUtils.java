package gr.neuropublic.mutil.python;
import org.slf4j.Logger;
import gr.neuropublic.mutil.base.ExceptionAdapter;


public class JythonUtils {

    private Logger l;

    public JythonUtils(Logger l) {
        this.l = l;
    }

    public void println(String foo) {
        System.out.println(foo);
    }

    public void print(String foo) {
        System.out.print(foo);
    }

    public void info(String foo) {
        l.info(foo);
    }

    public void printf(String format, Object ... args) {
        l.info(String.format(format,  args));
    }


}