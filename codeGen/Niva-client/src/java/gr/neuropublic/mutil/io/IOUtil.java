// Copyright (c) 2001 Hursh Jain (http://www.mollypages.org) 
// The Molly framework is freely distributable under the terms of an
// MIT-style license. For details, see the molly pages web site at:
// http://www.mollypages.org/. Use, modify, have fun !

package gr.neuropublic.mutil.io;	

import gr.neuropublic.mutil.*;
import gr.neuropublic.mutil.base.Argcheck;
import gr.neuropublic.mutil.base.Args;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;
import java.util.*;
import java.text.*;
import java.security.*;




/** Misc. IO utilities 

@author 	hursh jain
@version 	1.1 
**/
public class IOUtil
{
//for internal class debugging at development time
private static final boolean dbg = false;

/** System.getProperty("file.separator") for convenience **/
public static final String FILE_SEP = System.getProperty("file.separator");

/** System.getProperty("path.separator") for convenience **/
public static final String PATH_SEP = System.getProperty("path.separator");

/** System.getProperty("line.separator") for convenience **/
public static final String LINE_SEP = System.getProperty("line.separator");

public static final int FILECOPY_OVERWRITE = 0;
public static final int FILECOPY_NO_OVERWRITE = 1;

/** 
Ignores a directory copy command if the destination directory already
exists.
**/
public static final int DIRCOPY_NO_OVERWRITE = 2;

/** 
Copies the existing directory and overwrites any files with
the same name in the destination directory. Files/directories that 
exist in the destination but not in the source directory are left
untouched.
**/
public static final int DIRCOPY_ADD_OR_OVERWRITE = 3;

/** 
Copies the existing directory. If the destination directory already
exists, the entire target directory is first deleted and then
the specified directory is copied to the destination
**/
public static final int DIRCOPY_DELETE_AND_WRITE = 4;


/** number of bytes contained in a kilobyte */
public static final int ONE_KB = 1024;

/** number of bytes contained in a megabyte. */
public static final int ONE_MB = ONE_KB * ONE_KB;

/** number of bytes contained in a gigabyte. */
public static final int ONE_GB = ONE_KB * ONE_MB;

/** number of bytes equal to 2^32 **/
public static final long FOUR_GB = ONE_GB * 4L;

/**
Beeps by writing the beep control code to System.out
*/
public static void beep() 
	{
	System.out.print("\007");
	System.out.flush();
	}

/**
Copies source file (not directory) to destination. If the destination
already exists, then no copy is made (that is, the destination is
<b>not</b> overwritten and this method returns silently (an Exception
is not thrown).

@param file 	the source file to be copied, a java.io.File
@param dest		the destination file or directory.
@see copyFile(File, File, int)
*/
public static boolean copyFile(File source, File dest) throws FileNotFoundException, IOException
	{
	return copyFile(source, dest, IOUtil.FILECOPY_NO_OVERWRITE);
	}

/**
Copies the source file (not directory) to the specified
destination file or directory. If a directory is specified as
the destination, then the source file is copied into that
directory. To specify the action when a file with the same name
already exists in the specified directory, use the appropriate
{@link #FILECOPY_OVERWRITE} or {@link #FILECOPY_NO_OVERWRITE}
flags.
If a file is specified as the destination, then the source file
is copied to that file. If the specified file exists already
then specify the appropriate overwrite flag.  
<p>
Try to use absolute path names for files and directories. Relative
path names can be relative either to user.dir or where the jvm was invoked
or some platform/jvm dependent place, so don't rely on relative paths.  
Copying, moving or working with files is tricky in java. Similar 
behavior is not defined for all platforms. For example, in WindowsNT/java1.2, 
aliases cannot be opened/resolved and hence cannot be copied via 
FileInput/Output streams. This sort of thing is best left to
JNI calls native code. 
<p>
This method returns <tt>true</tt> if the directory was copied
successfully, <tt>false</tt> otherwise. (for example, <tt>false</tt>
will be returned when the copy mode is not to overwrite but the
target file already exists).
 
@param source 	the source file (<b>not</b> directory) to be copied, a java.io.File
@param dest		the destination file or directory.
@param copyflag	the file copy mode. {@link #FILECOPY_OVERWRITE}, {@link #FILECOPY_NO_OVERWRITE} 
*/	
public static boolean copyFile(File source, File dest, int copyflag) throws FileNotFoundException, IOException
	{
	Argcheck.notnull(source, "copyFile(): source file argument is null"); 
	Argcheck.notnull(dest, "copyFile(): destination file argument is null"); 
	Argcheck.istrue((copyflag == FILECOPY_OVERWRITE || copyflag == FILECOPY_NO_OVERWRITE),"copyflag not valid"); 
	Argcheck.isfalse(source.isDirectory(), "A directory [" + source + "] was specified for the source. This method cannot only copy normal files");
	
	if (dest.isDirectory()) {
		dest = new File(dest, source.getName());
		}
		
	if (dest.exists()) {
		if (copyflag == IOUtil.FILECOPY_NO_OVERWRITE) {
			return false;
			}
		}

  	final FileInputStream fin = new FileInputStream(source);
	final FileOutputStream fout = new FileOutputStream(dest);
	final FileChannel fcin = fin.getChannel();
	final FileChannel fcout = fout.getChannel();
    
    final MappedByteBuffer mbb = fcin.map(
    					FileChannel.MapMode.READ_ONLY, 0, fcin.size());
    
    fcout.write(mbb);
    	
    fcin.close();
	fcout.close();
	fin.close();
	fout.close();
	return true;
	}

/**
Calls {@link #copyFile(File, File, int)} with the 
{@link #DIRCOPY_ADD_OR_OVERWRITE} flag.

@param file 	the source directory to be copied
@param dest		the destination directory.
@see copyFile(File, File, int)
*/
public static boolean copyDirectory(File source, File dest) throws FileNotFoundException, IOException
	{
	return copyDirectory(source, dest, IOUtil.DIRCOPY_ADD_OR_OVERWRITE);
	}

/**
Copies the source directory and all it's contents to the specified 
destination directory. A directory must be specified both for the source 
and the destination. 
<p>
To handle cases where the destination directory already exists
use the appropriate {@link #DIRCOPY_NO_OVERWRITE}
{@link #DIRCOPY_DELETE_AND_WRITE} and
{@link #DIRCOPY_ADD_OR_OVERWRITE} flags.
<p>
Try to use absolute path names for files and directories. Relative
path names can be relative either to user.dir or where the jvm was invoked
or some platform/jvm dependent place, so don't rely on relative paths.  
<u>Copying, moving or working with files is tricky in java</u>. Similar 
behavior is not defined for all platforms. For example, in WindowsNT/java1.2, 
aliases cannot be opened/resolved and hence cannot be copied via 
FileInput/Output streams. This sort of thing is best left to
JNI calls to POSIX or to platform specific code. 
<p>
This method returns <tt>true</tt> if the directory was copied
successfully, <tt>false</tt> otherwise. (for example, <tt>false</tt>
will be returned when the copy mode is not to overwrite but the
target directory already exists).

@param source 	the source directory (<b>not</b> file) to be copied, a java.io.File
@param dest		the destination file or directory.
@param copyflag	the dir copy mode. {@link #DIRCOPY_NO_OVERWRITE}, {@link #DIRCOPY_ADD_OR_OVERWRITE} 
*/	
public static boolean copyDirectory(File source, File dest, int copyflag) 
throws IOException
    {
	Argcheck.notnull(source, "copyDirectory(): source file argument is null"); 
	Argcheck.notnull(dest, "copyDirectory(): destination file argument is null"); 
	Argcheck.istrue((copyflag == DIRCOPY_NO_OVERWRITE || copyflag == DIRCOPY_ADD_OR_OVERWRITE || copyflag == DIRCOPY_DELETE_AND_WRITE), "copyflag not valid"); 

	if (source.exists()) {
		Argcheck.istrue(source.isDirectory(), "IOUtil.copyDirectory(): A file was specified for the source, need a directory not a file");
		}
		
	if (dest.exists()) 
		{
		if (dbg) System.out.println("IOUtil.copyDirectory(): destination '" + dest + "' exists");
		
		if ( ! dest.isDirectory() ) {
			if (dbg) System.out.println("IOUtil.copyDirectory('" + source + "','" + dest + "'): A file was specified for the destination, need a directory not a file");
			return false;
			}
		
		if (copyflag == IOUtil.DIRCOPY_NO_OVERWRITE) {
			System.out.println("IOUtil.copyDirectory(): Incompatible flag DIRCOPY_NO_OVERWRITE specified, returning false");
			return false;
			}
		if (copyflag == IOUtil.DIRCOPY_DELETE_AND_WRITE) 
			{
			boolean good = deepDelete(dest);
			if (! good) 
				throw new IOException("IOUtil.copyDirectory, flag=DIRCOPY_DELETE_AND_WRITE, cannot delete the destination directory:" + dest);
			}
		}	
	else { //dest dir does not exist
		if (dbg) System.out.println("IOUtil.copyDirectory(): destination dir '" + dest + "' does not exist. Creating..");		
		boolean good = dest.mkdirs();
		if (! good) {
			if (dbg) System.out.println("IOUtil.copyDirectory(): could not make directory '" + dest + "' ; returning false");
			return false;
			}
		}
	
	String files[] = source.list(); //does not return "." or ".."
	
	boolean copiedOne = false;
	boolean copiedAll = true;
	for(int i = 0; i < files.length; i++)
		{
		File source_f = new File(source, files[i]);
		File dest_f = new File(dest, files[i]);
		if(source_f.isDirectory()) {
			if (dbg) System.out.println("IOUtil.copyDirectory(): recursive copy directory call: '" + source_f + "' to '" + dest_f + "'");
			copiedOne = copyDirectory(source_f, dest_f, copyflag);
			}
		else {
			if (dbg) System.out.println("IOUtil.copyDirectory(): copying file: '" + source_f + "' to '" + dest_f + "'");
			copiedOne = copyFile(source_f, dest_f, IOUtil.FILECOPY_OVERWRITE);
			}
        if (! copiedOne)
		 	copiedAll = false;
		}
	if (dbg) System.out.println("IOUtil.copyDirectory: returning: " + copiedAll);
	return copiedAll;
    }	//~copyDirectory

/** 
Copies all data from the specific input stream to the specified output stream.
Closes both streams after it is finished.

@param 	in	the InputStream
@param	out the OutputStream
**/
public static void copyStream(InputStream in, OutputStream out) 
													throws IOException
	{
	final BufferedInputStream bin = bufferStream(in);
	final BufferedOutputStream bout = bufferStream(out);
	int i = 0;
	while ( (i = bin.read()) > -1) {
		bout.write(i);	
		}
	//FilterStream (like bufferedinputstream etc) close all internal
	//streams when they are closed too !
	bin.close();
	bout.close();		
	} 

/** 
Alphabetizes the specified list by the filename. Only the
filename is considered and not the path name (if any). The 
collection should contain one of: 
<ul>
	<li> <tt>java.io.File</tt>
	<li> <tt>String[]</tt>
	<li> <tt>Object</tt>
</ul>
Any/all of these can be contained in the specified list at the
same time. If a <tt>String[]</tt> is found, the <b>0</b>th element
(i.e., (String[] foo)[0]) is used for comparison purposes. The
list is sorted by the default {@link String#compareTo(String)} implementation 
of <tt>String</tt>.

@param	list	the list to be sorted
**/
public static void sortByFileName(List c) {
	Collections.sort(c, new Comparator() {
		public int compare(Object o1, Object o2) {
			return getStr(o1).compareTo(getStr(o2));		
			}
		private String getStr(Object o) {
			String str = null;
			if ( o instanceof File )
				str = ((File)o).getName();
			else if (o instanceof String) 
				str = (String) o;
			else 
				str = (o!=null)? o.toString() : null;
			return str;
			}		
		});
	}

/**
This method recursively removes a specified file or recursively 
removes a specified directory. It is needed (as of JDK 1.4) because
{@link File#delete} lacks the ability to delete a directory if the 
directory is not empty. 
<p>
Internally, this method delegates to {@link File#delete}, so if
{@link File#delete} follows sym links, then so will this method.
Be careful !

@param	file	the file or directory to be removed
@return			<tt>true</tt> on success, <tt>false</tt> otherwise. Also returns false if
				the specified file or directory does not exist.
**/
public static boolean deepDelete(File f)	
	{
	Argcheck.notnull(f, "File was null");
	if (dbg) System.out.println("deepDelete(): deleting: " + f);
	
	boolean ok = true;

	if (! f.exists()) 
		return false;

	if (f.isFile()) {
		ok = f.delete();
		return ok;
		}

	//f is a directory
	File[] files = f.listFiles();  //does not return "." or ".."
	boolean subok = false;

	//1. delete sub directories
	for (int n = 0; n < files.length; n++) {
		subok = deepDelete(files[n]);
		if (! subok) ok = false;
		}	
	
	//2. delete current directory
	subok = f.delete();
	if (! subok) ok = false;
	
	return ok;	
	}

/**
Gets the total size for a directory and all of it's contents. If
the specified argument is a regular file, returns the size of that
file itself.

@param	dir		the target dir
@return 		the directory or file size in bytes
**/
public static long dirSize(File dir) 
	{
	Argcheck.notnull(dir, "File was null");
	long size = 0;

	if (! dir.exists()) 
		return 0;

	if (dir.isFile()) {
		return dir.length();		
		}

	File[] files = dir.listFiles();  //does not return "." or ".."

	for (int n = 0; n < files.length; n++) 
		size += dirSize(files[n]);
		
	return size;
	}

/** 
Buffers and returns the specified InputStream, if it is not already buffered.
Does not buffer an already buffered stream but returns it as is.

@param in	the input stream to be buffered
@return	the buffered stream
*/
public static BufferedInputStream bufferStream(InputStream in)
	{
	Argcheck.notnull(in, "InputStream was null");
	BufferedInputStream bin;
	if (! (in instanceof BufferedInputStream)) {
		bin = new BufferedInputStream(in);		
		}
	else {
		bin = (BufferedInputStream) in;
		}
	return bin;
	}

/** 
Buffers and returns the specified OutputStream, if it is not already buffered.
Does not buffer an already buffered stream but returns it as is.

@param out	the output stream to be buffered
@return	the buffered stream

**/
public static BufferedOutputStream bufferStream(OutputStream out)
	{
	Argcheck.notnull(out, "OutputStream was null");
	BufferedOutputStream bout;
	if (! (out instanceof BufferedOutputStream)) {
		bout =  new BufferedOutputStream(out);		
		}
	else {
		bout = (BufferedOutputStream) out;
		}
	return bout;
	}
	
public static BufferedReader bufferReader(Reader in) 
	{
	Argcheck.notnull(in, "Reader was null");
	BufferedReader bin;
	if ( ! (in instanceof BufferedReader)) {
		bin = new BufferedReader(in);
		}
	else {
		bin = (BufferedReader) in;
		}
	return bin;
	}
		
public static PrintStream toPrintStream(OutputStream out) 
	{
	Argcheck.notnull(out, "OutputStream was null");
	if ( ! (out instanceof PrintStream)) {
		out = new PrintStream(out);
		}
	return (PrintStream) out;
	}
	
public static PrintWriter toPrintWriter(Writer out) 
	{
	Argcheck.notnull(out, "Writer was null");
	if ( ! (out instanceof PrintWriter)) {
		out = new PrintWriter(out);
		}
	return (PrintWriter) out;
	}


public static BufferedWriter bufferWriter(Writer out) 
	{
	Argcheck.notnull(out, "Writer was null");
	BufferedWriter bout;
	if ( ! (out instanceof BufferedWriter)) {
		bout = new BufferedWriter(out);
		}
	else {
		bout = (BufferedWriter) out;
		}
	return bout;
	}

/**
Convenience method to print the contents of a java.util.Property object
to a String (using the default platform encoding).

**/
public static String propertiesToString(Properties props)
	{
	String temp = null;
	final ByteArrayOutputStream bout = new ByteArrayOutputStream();
	final PrintStream pout = new PrintStream(bout);
	props.list(pout);
	pout.flush();
	temp = bout.toString();
	pout.close();
	return temp;
	}

private static String defaultEncoding = null;

/**
Returns the default encoding used by the current platform. Returns
<tt>null</tt> is the default encoding cannot be determined.
**/
public static String getDefaultEncoding() 
	{
	if (defaultEncoding != null) 
		return defaultEncoding;
	String de = null;
	try {
		final InputStream in = ClassLoader.getSystemResourceAsStream("fc/io/IOUtil.class");
		final InputStreamReader defaultReader = new InputStreamReader(in);
		de = defaultReader.getEncoding();
		defaultEncoding = de;
		if (dbg) System.out.println("IOUtil.getDefaultEncoding() = " + de);
		}
	catch (Exception e) {
		e.printStackTrace();	
		}
	return de;
	}
	
/** 
Returns the contents of an entire file as a String. If the specified
file does not exist returns <tt>null</tt>. Files that exist but have no
content return an empty String.
<p>
<b>Note 1:</b> Due to jdk1.4 brain damage, this method is limited to files 
less than 2^32 bytes. If the specified file is greater than 2^32 bytes,
an <tt>IOException</tt> will be thrown.
<br>
<b>Note 2:</b> The files is converted into a String using an encoding
that is determined programmatically (from the filesystem). This may
not be totally reliable but there is no way around this because JDK 1.4
provides <b>no</b> way to easily get the default platform encoding.
Uses the <tt>ISO_8859_1</tt> encoding as a fallaback measure, if the
default encoding cannot be determined.

@param	filename	the absolute path to the file name to be read
@param	trim		if <tt>true</tt>, any trailing whitespace is trimmed from the file's end.
**/
public static String fileToString(String filename, boolean trim) throws IOException
    {
	byte[] buf = fileToByteArray(filename);
	if (buf == null)
		return null;
	
	//there is no way to convert a byte buffer to a String
	//because we cannot get a Charset that uses the default platform
	//encoding in JDK 1.4.0. So we are going to IOUtil.getDefaultEncoding 
	//method (which is a workaround) to get the default platform encoding.

	// resultstr will be "" if buf contains 0 chars (it can't be null 
	// if we have reached this point)
	String resultstr = arrayToCharBuffer(buf).toString(); 
	if (trim) {
		//might trigger g.c if string size is large
		resultstr = resultstr.trim();
		}
	return resultstr;
    }

/** 
Calls {@link #fileToString(String, boolean)} with trim being
<tt>false</tt> (that is, files are not trimmed at their trailing end).

@param	filename	the absolute path to the file name to be read
**/
public static String fileToString(String filename) throws IOException
    {
    return fileToString(filename, false);
	}

/**  
Returns the contents of an entire file as a <tt>byte[]</tt>. If the specified
file does not exist returns <tt>null</tt>. 
<p>
<b>Note 1:</b> Since java arrays cannot be greater than 2^32 elements,
this method is limited to files less than or equal to 2^32 bytes. If 
the specified file is greater than 2^32 bytes, an <tt>IOException</tt> 
will be thrown.

@param		filename	the absolute path to the file name to be read
@return	ByteBuffer	contains the bytes for that file
**/
public static byte[] fileToByteArray(String filename) throws IOException
	{
	return fileToByteArray(new File(filename));
	}

/**  
Returns the contents of an entire file as a <tt>byte[]</tt>. If the specified
file does not exist returns <tt>null</tt>. 
<p>
<b>Note 1:</b> Since java arrays cannot be greater than 2^32 elements,
this method is limited to files less than or equal to 2^32 bytes. If 
the specified file is greater than 2^32 bytes, an <tt>IOException</tt> 
will be thrown.

@param		file		the file to be read
@return	byte[]		contains the bytes for that file
**/
public static byte[] fileToByteArray(File file) throws IOException
	{
	if (dbg) System.out.println("ENTER fileToByteBuffer(" + file + ")");

	Argcheck.notnull(file);
	
	if ( ! file.exists() )
		return null;

	if (dbg) System.out.println("file '" + file + "' exists");
	
	long longfsize = file.length();
	if (dbg) System.out.println("'" + file + "' size = " + longfsize);
	
	if ( longfsize > FOUR_GB )
		throw new IOException("File size of " + longfsize + " too large for this method"); 	

	FileInputStream fin = new FileInputStream(file);
	int fsize = (int) longfsize;
	byte[]	buf = new byte[fsize];

	try {
		int read, pos = 0;
		while (pos < fsize) 
			{
			/* Usually, this will read everything the first time */
			read = fin.read(buf, pos, fsize - pos);
			pos += read;
			if (read < 0)
				break;
			}

		if (dbg) System.out.println("Read file byte[] = " + buf.length + " bytes");
		
		if (pos != fsize)
      		throw new IOException( "Can't read entire file, filesize = " + fsize + ", read = " + pos);

		if (dbg) System.out.println("EXIT fileToByteBuffer(" + file + ")");
		}
	finally {
	 	fin.close(); 
		}

	return buf;
	}
	
/**  
Returns the contents of an entire file as a ByteBuffer backed by mapping the
file to memory. If the specified file does not exist returns <tt>null</tt>.
Mapped files do <b>not</b> have have a accesible backing array and the
<tt>ByteBuffer.hasArray()</tt> will be <tt>false</tt>. See the {@link
java.nio.MappedByteBuffer} documentation about concurrent modification or
deletion of files that are mapped into memory.
<p>
The ByteBuffer returned by this method will have <tt>{@link
ByteBuffer#rewind()} </tt> called on it before it is returned.
<p>
<b>Note 1:</b> This method is limited to files less than 2^32 bytes, since
ByteBuffers cannot be greater than this size. If the specified file is greater 
than 2^32 bytes, an <tt>IOException</tt> will be thrown.

@param		file		the file to be read
@return	ByteBuffer	contains the bytes for that file
**/
public static ByteBuffer fileToByteBuffer(File file) throws IOException
	{
	if (dbg) System.out.println("ENTER fileAsByteBuffer(" + file + ")");
	Argcheck.notnull(file);
	long fsize = 0;

	if ( ! file.exists() )
		return null;

	if (dbg) System.out.println("file '" + file + "' exists");
	
	fsize = file.length();

	if (dbg) System.out.println("'" + file + "' size = " + fsize);
	
	if ( fsize > FOUR_GB)
		throw new IOException("File size of " + file.length() + " too large for this method"); 

	FileChannel fcin = new FileInputStream(file).getChannel();
	
	BufferedReader reader = null;
	final ByteBuffer bufin = fcin.map(FileChannel.MapMode.READ_ONLY, 0, fsize);
	
	if (dbg) System.out.println("File ByteBuffer = " + bufin);

	//This is very important and easy to forget -- rewind the buffer !!!
	
	bufin.rewind();

	if (dbg) System.out.println("EXIT fileAsByteBuffer(" + file + ")");
	return bufin;
	}

/**  
Returns the contents of an entire file as a <tt>char[]</tt>. If the specified
file does not exist returns <tt>null</tt>. 
<p>
<b>Note 1:</b> Since java arrays cannot be greater than 2^32 elements,
this method is limited to files less than or equal to 2^32 bytes. If 
the specified file is greater than 2^32 bytes, an <tt>IOException</tt> 
will be thrown.

@param		file		the file to be read
@param		encoding	the name of the character encoding
						to use. Specify <tt>null</tt> to use the 
						default encoding.
@return	char[]			contains the chars for that file
**/
public static char[] fileToCharArray(File file, String encoding) 
throws IOException
	{
	Argcheck.notnull(file);
	
	if ( ! file.exists() )
		return null;

	if (dbg) System.out.println("file '" + file + "' exists");
	
	long longfsize = file.length();
	if (dbg) System.out.println("'" + file + "' size = " + longfsize);
	
	if ( longfsize > FOUR_GB )
		throw new IOException("File size of " + longfsize + " too large for this method"); 	

	FileInputStream fin = new FileInputStream(file);
	
	final Reader reader = (encoding != null) ?
			new InputStreamReader(fin, encoding) :
			new InputStreamReader(fin);
	
	int fsize = (int) longfsize;
	char[]	buf = new char[fsize];

	try {
		int read, pos = 0;
		while (pos < fsize) 
			{
			/* Usually, this will read everything the first time */
			read = reader.read(buf, pos, fsize - pos);
			pos += read;
			if (read < 0)
				break;
			}

		if (dbg) System.out.println("Read file char[] = " + buf.length + " bytes");
		
		if (pos != fsize)
      		throw new IOException( "Can't read entire file, filesize = " + fsize + ", read = " + pos);

		}
	finally {
	 	fin.close(); 
		}

	return buf;
	}


/** 
Converts the specified byte array into a CharBuffer using the specified
encoding. The returned CharBuffer can be directly used in statements such
as <tt>System.out.println</tt> to print it's contents,
<p>
This method returns <tt>null</tt> if the specified array is <tt>null</tt>
or if the specified encoding is <tt>null</tt>.

@param	array		the array to convert
@param	encoding	the encoding to use to convert bytes into chars
**/
public static CharBuffer arrayToCharBuffer(byte[] array, String encoding) 
	{
	if ( (array == null) || (encoding == null))
		return null;	
		
	Charset cset = Charset.forName(encoding);
	CharBuffer cbuf = cset.decode(ByteBuffer.wrap(array));
	return cbuf;
	}

/** 
Convenience method that delegates to {@link #arrayToCharBuffer(byte[],
String)} using the platform's default encoding.
**/	
public static CharBuffer arrayToCharBuffer(byte[] array) 
	{
	String enc = IOUtil.getDefaultEncoding();
	enc = (enc != null) ? enc : "ISO-8859-1";
	return arrayToCharBuffer(array, enc);
	}	

/**
Reads the entire InputStream and returns all read data as a 
<tt>String</tt> (using the default platform encoding). If
no data is available, returns <tt>null</tt>. The specified input
stream is <u>not</u> closed.

@param	in		the InputStream to read
@param	block 	if <tt>true</tt>, this method will block until all 
				available data from the specified input stream
				has been read. If <tt>false</tt>, this method will
				read and return as much data as currently is available 
				is read without blocking. The available amount is 
				that returned by the specified input stream.

@throws NegativeArraySizeException	if the specified input stream returns
									a negative number for available()
**/
public static String inputStreamToString(InputStream in, boolean block) 
throws IOException
	{
	final byte[] buf = inputStreamToByteArray(in, block);
	if (buf == null) 	
		return null;
	return new String(buf);		
	}		//~inputStreamToString

/**
Reads the entire InputStream and returns all read data as a 
<tt>byte[]</tt>. If no data is available, returns <tt>null</tt>.

@param	in		the InputStream to read
@param	block 	if <tt>true</tt>, this method will block until all 
				available data from the specified input stream
				has been read. If <tt>false</tt>, this method will
				read and return as much data as currently is available 
				is read without blocking. The available amount is 
				that returned by the available() method of the specified 
				input stream.

@throws NegativeArraySizeException	if the specified input stream returns
									a negative number for available()				
**/
public static byte[] inputStreamToByteArray(InputStream in, boolean block) 
throws IOException
	{
	Argcheck.notnull(in, "InputStream was null");

	final BufferedInputStream bin = bufferStream(in);
	
	if (! block) {
		int buffer_size = bin.available();
		if (dbg) System.out.println("inputStreamToByteArray(), block=no, buffersize=" + buffer_size);
		byte[] buf = new byte[buffer_size];
		int read = 0;
		int pos = 0;
		while (read < buffer_size) {
			read += bin.read(buf, pos, buffer_size - read);
			pos = read + 1;
			}
		if (dbg) System.out.println("inputStreamToByteArray(), returning buf=" + buf.length + " bytes");
		return buf;
		}
	
	//block
	int buffer_size = 512;
	byte[] buf = new byte[buffer_size];
	if (dbg) System.out.println("inputStreamToByteArray(), block=yes");

	final ByteArrayOutputStream bout = new ByteArrayOutputStream(buffer_size);
	int read = 0;
	while (true) {
		read = bin.read(buf, 0, buffer_size);	
		if (read == -1)
			break;	
		bout.write(buf, 0, read);
		}
	//if size() is 0, toByteArray returns an array of size 0. we
	//return null instead.
	if (bout.size() == 0) {
		return null;
		}
	return bout.toByteArray();		
	}

/**
Calls inputStreamToByteArray(in, <tt>true</tt>)
*/
public static byte[] inputStreamToByteArray(InputStream in) throws IOException
	{
	return inputStreamToByteArray(in, true);	
	}

/**
Calls inputStreamToString(in, <tt>true</tt>)
*/
public static String inputStreamToString(InputStream in) throws IOException
	{
	return inputStreamToString(in, true);
	}


/**
Convenience method to print the stack trace of an Exception (or Throwable)
to a String (using the default platform encoding). (The <tt>getMessage()</tt>
method of a Throwable does not print the entire stack trace).
**/
public static String throwableToString(final Throwable e)
	{
	Argcheck.notnull(e, "The specified exception object was null");
	String temp = null;
 	final ByteArrayOutputStream bout = new ByteArrayOutputStream(768);	
	final PrintStream pout = new PrintStream(bout);
	e.printStackTrace(pout);
	pout.flush();
	temp = bout.toString();
	pout.close();
	return temp;
	}

/**
Convenience method that returns the current execution stack trace as a String.
(using the default platform encoding).
**/
public static String stackTrace()
	{
	String temp = null;
 	final ByteArrayOutputStream bout = new ByteArrayOutputStream(768);	
	final PrintStream pout = new PrintStream(bout);
	pout.println("==================== Debug Stack Trace ======================");
	new Exception().printStackTrace(pout);
	pout.println("=============================================================");
	pout.flush();
	temp = bout.toString();
	pout.close();
	return temp;
	}

/** 
Calls {@link #fileSizeToString(long)} and truncates the 
size to fit in the specified number of digits. For example,
a file size of <tt>4.455 KB</tt> and a length of 2 will 
return <tt>4.45 KB</tt>

@param	filesize	the size of the file in bytes
@param  length		the max number of digits <b>after</b>
					the decimal point
*/
public static String fileSizeToString(long filesize, int length)
	{
	NumberFormat nf = NumberFormat.getInstance();	
	nf.setMaximumFractionDigits(length);
	return fileSizeToStringImpl(nf, filesize);
	}


/** 
Converts the specified file size into a human readable description. 
Similar to the <tt>"--human-readable"</tt> flag found in various 
GNU programs.

@param	filesize	the size of the file in bytes
*/
public static String fileSizeToString(long filesize)
	{
	NumberFormat nf = NumberFormat.getInstance();	
	return fileSizeToStringImpl(nf, filesize);		
	}
	
private static final String fileSizeToStringImpl(
					NumberFormat nf, long filesize)
	{
	StringBuffer buf = new StringBuffer(32);
	if (filesize > ONE_GB) 
        buf.append(nf.format(filesize / ONE_GB)).append(" GB");
	else if (filesize > ONE_MB) 
		buf.append(nf.format( filesize / ONE_MB)).append(" MB");
	else if (filesize > ONE_KB)
		buf.append(nf.format(filesize / ONE_KB)).append(" KB");
	else		
		buf.append(nf.format(filesize)).append(" bytes");
	
	return buf.toString();
	}

/**
Converts a string file size into a number denoting the equivalent bytes. 
For example:
<pre>
34K, 34KB --> 34 bytes
34M, 34megabytes --> 34 * 1024 bytes
</pre>
Allows numbers to end with <tt>k..., m..., g...., b....</tt> or no suffix 
at all. Suffixes are case insensitive. 
*/
public static long stringToFileSize(String str)
	{
	Argcheck.notnull(str, "the specified string was null");
	
	str = str.replace(" ",""); //remove all leading, trailing, embedded spaces
	
	int pos = 0;  //this is slighty easier than str.indexOf
	for (int n = 0; n < str.length(); n++) 
		{
		char c = str.charAt(n);
		switch (c) {
			case 'g': case 'G':
				return Long.parseLong(str.substring(0,n)) * ONE_GB; 
			case 'm': case 'M':
				return Long.parseLong(str.substring(0,n)) * ONE_MB; 
			case 'k': case 'K':
				return Long.parseLong(str.substring(0,n)) * ONE_KB; 
			case 'b': case 'B':
				return Long.parseLong(str.substring(0,n)); 
			default: 
				//nothing to do
			}
		}
	return Long.parseLong(str);
	}

// the previous version by Hursh Jain had a bug which I fixed on 07-IX-2012.
public static final String sha1hash(byte[] buf) throws NoSuchAlgorithmException {
    return gr.neuropublic.mutil.hashing.HashAlgorithms.hexString(buf);
}

/**
Usage:
java IOUtil args where args are:
	-fileToString	full-path-to-file 
					[to convert to String (for fileToString() method)]
	
**/
public static void main(String[] args) throws Exception
	{
	Args myargs = new Args(args);
	String fts = myargs.getRequired("fileToString");
	new Test(fts);
	}


/**
Unit Test History 	
<pre>
Class Version	Tester	Status			Notes
1.1				hj		still-testing	limited testing only, more needs to be done
</pre>
*/
static private class Test {
Test(String fileToString) throws Exception
	{
	java.io.File sourcefile = new java.io.File("testsourcefile");
	java.io.File sourcedir = new java.io.File("testsourcedir");	
	java.io.File destfile = new java.io.File("testdestfile");	
	java.io.File destdir = new java.io.File("testdestdir");	

	String dir1 = "foo";
	String dir2 = "foo" + FILE_SEP;   	
	String f = "f1";
	//String r1 = IOUtil.makeFilePath(dir1,f);
	//String r2 = IOUtil.makeFilePath(dir2,f);
	for(long n = 2; n < 4000000001L; n *= 1000) {
		System.out.println("file size " + n + " = " 
							+ IOUtil.fileSizeToString(n));
		}
	
	System.out.println("Platform encoding, via file.encoding: " + System.getProperty("file.encoding"));
	System.out.println("Platform encoding: " + IOUtil.getDefaultEncoding());
	System.out.println("--- fileToString('"+ fileToString + "') -----");
	String filestr = fileToString(fileToString);
	System.out.println("file '" + fileToString + "' read into String");	
	System.out.println("String length = " + filestr.length());
	System.out.println("String data = ");
	System.out.println(filestr);

	FileInputStream fin = new FileInputStream(fileToString);
	System.out.println("--- inputStreamToString('"+ fileToString + "') -----");
	String str = inputStreamToString(fin, false);
	System.out.println(str);
	System.out.println(fileToString + ", size=" + str.length());

	Socket sock = new Socket("www.yahoo.com", 80);
	System.out.println("--- inputStreamToString('"+ sock + "') -----");
	InputStream sin = sock.getInputStream();
	OutputStream sout = sock.getOutputStream();
	sout.write("GET /index.html\n\n".getBytes());	
	sout.flush();
	System.out.println(inputStreamToString(sin, true));
	
	copyFileTest();		
	} //~constructor
	
	void copyFileTest() throws IOException {
		File source = new File("/tmp/foo");
		File dest = new File("/tmp/bar");			
		System.out.println("Copy test: " + source + " to " + dest);
		copyDirectory(source, dest);
		}
}	//~Test
	
}           //~ IOUtil	


	
	
	