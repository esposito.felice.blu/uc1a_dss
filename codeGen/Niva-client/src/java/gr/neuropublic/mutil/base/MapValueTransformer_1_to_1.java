package gr.neuropublic.mutil.base;

import java.util.HashMap;
import java.util.Map;

public class MapValueTransformer_1_to_1<K, V, L> {
	
	public Map<K, L> dispatch(Map<K, V> mapOfKsToVs, ITransformer_1_to_1<V, L> transformer) {
		HashMap<K, L> retValue = new HashMap<K, L>(mapOfKsToVs.size());
		for (K k : mapOfKsToVs.keySet())
			retValue.put(k, transformer.transform(mapOfKsToVs.get(k)));
		return retValue;
	}

}
