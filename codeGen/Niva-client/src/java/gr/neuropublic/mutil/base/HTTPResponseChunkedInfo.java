package gr.neuropublic.mutil.base;

public class HTTPResponseChunkedInfo {
	public boolean lastChunk;

	public HTTPResponseChunkedInfo(boolean lastChunk) {
		this.lastChunk = lastChunk;
	}

	@Override
	public String toString() {
		return "HTTPResponseChunkedInfo [lastChunk=" + lastChunk + "]";
	}
	
}
