package gr.neuropublic.mutil.base;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RingBuffer<Item> implements Iterable<Item> {
    private Item[] a;            // queue elements
    private int N = 0;           // number of elements on queue
    private int first = 0;       // index of first element of queue
    private int last  = 0;       // index of next available slot

    // cast needed since no generic array creation in Java
    public RingBuffer(int capacity) {
        a = (Item[]) new Object[capacity];
    }

    public boolean isEmpty() { return N == 0; 		}
    public boolean isFull()	 { return N == a.length;}
    public int size()        { return N;      		}
    
    public RingBuffer<Item> copyWithCapacity(int capacity) {
    	RingBuffer<Item> retValue = new RingBuffer<Item>(capacity);
    	for (Item item : this) {
    		retValue.enqueueSoftly(item);
    	}
    	return retValue;
    }


    public void enqueue(Item item) {
        if (N == a.length) { throw new RuntimeException("Ring buffer overflow"); }
        a[last] = item;
        last = (last + 1) % a.length;     // wrap-around
        N++;
    }
    
    public void enqueue(Item items[]) {
        for (Item item : items)
        	enqueue(item);
    }
    
    public void enqueueSoftly(Item item) {
    	while (isFull())
    		dequeue();
    	enqueue(item);
    }
    
    public void enqueueSoftly(Item items[]) {
    	for (Item item: items)
    		enqueueSoftly(item);
    }

    // remove the least recently added item - doesn't check for underflow
    public Item dequeue() {
        if (isEmpty()) { throw new RuntimeException("Ring buffer underflow"); }
        Item item = a[first];
        a[first] = null;                  // to help with garbage collection
        N--;
        first = (first + 1) % a.length;   // wrap-around
        return item;
    }

    public Iterator<Item> iterator() { return new RingBufferIterator(); }

    // an iterator, doesn't implement remove() since it's optional
    private class RingBufferIterator implements Iterator<Item> {
        private int i = 0;
        public boolean hasNext()  { return i < N;                               }
        public void remove()      { throw new UnsupportedOperationException();  }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = a[(i + first) % a.length];
            i++;
            return item;
        }
    }



    // a test client
    public static void main(String[] args) {
        RingBuffer<String> ring = new RingBuffer<String>(20);
        ring.enqueue("Delete");
        ring.enqueue("This");
        ring.enqueue("is");
        ring.enqueue("a");
        ring.enqueue("test.");
        ring.dequeue();

        for (String s : ring) {
            System.out.println(s);
        }

        System.out.println();

        while (!ring.isEmpty())  {
            System.out.println(ring.dequeue());
        }
        
        RingBuffer<Byte> test = new RingBuffer<Byte>(1);
        test.enqueueSoftly((byte) 3);
        test.enqueueSoftly((byte) 4);
        for (byte b : test) {
        	System.out.println(b);
        }
        test.enqueueSoftly((byte) 5);
        for (byte b : test) {
        	System.out.println(b);
        }
        test = test.copyWithCapacity(2);
        test.enqueue((byte) 6);
        System.out.println("new array");
        for (byte b : test) {
        	System.out.println(b);
        }
        test.enqueueSoftly((byte) 7);
        test.enqueueSoftly((byte) 8);
        test.enqueueSoftly((byte) 9);
        System.out.println("new array");
        for (byte b : test) {
        	System.out.println(b);
        }
        test = test.copyWithCapacity(1);
        System.out.println("new array");
        for (byte b : test) {
        	System.out.println(b);
        }
    }
    


}
