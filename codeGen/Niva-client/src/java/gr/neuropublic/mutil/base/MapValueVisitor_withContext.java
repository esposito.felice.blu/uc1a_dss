package gr.neuropublic.mutil.base;

import java.util.Map;

public class MapValueVisitor_withContext<K, V, L> {
	
	private L l;
	public MapValueVisitor_withContext(L l) {
		this.l = l;
	}
	public void dispatch(Map<K, V> mapOfKsToVs, IVisitorWithContext<V, L> transformer) {
		for (K k : mapOfKsToVs.keySet())
			transformer.visit(mapOfKsToVs.get(k), l);
	}

}
