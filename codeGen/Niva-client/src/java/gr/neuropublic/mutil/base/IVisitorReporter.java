package gr.neuropublic.mutil.base;

public interface IVisitorReporter<K, V> {
    public void visit(K k);
    public V report();
}