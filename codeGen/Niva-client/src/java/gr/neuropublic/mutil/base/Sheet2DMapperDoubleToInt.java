package gr.neuropublic.mutil.base;

import java.awt.Dimension;
import java.awt.Point;

public class Sheet2DMapperDoubleToInt {
	
	Pair<Double, Double> dimensionsFrom;
	Pair<Double, Double> dimensionsTo;
	
	public Sheet2DMapperDoubleToInt(Pair<Double, Double> dimensionsFrom, Dimension dimensionsTo) {
		Util.panicIf(dimensionsFrom.a < 0);
		Util.panicIf(dimensionsFrom.b < 0);
		Util.panicIf(dimensionsTo.width  <= 0);
		Util.panicIf(dimensionsTo.height <= 0);
		this.dimensionsFrom = dimensionsFrom;
		this.dimensionsTo = Pair.create(new Double(dimensionsTo.width), new Double(dimensionsTo.height));
	}
	
	public Pair<Float, Float> metaCompressionRatios() {
		float multiplier_width 	= (float) (dimensionsTo.a / dimensionsFrom.a);
		float multiplier_height = (float) (dimensionsTo.b / dimensionsFrom.b);
		if (multiplier_width > multiplier_height)
			return Pair.create( multiplier_width/multiplier_height, 1.0f );
		else
			return Pair.create( 1.0f, multiplier_height/multiplier_width);
	}
	
	public Point map(Pair<Double, Double> point) {
		Util.panicIf(point.a < 0);
		Util.panicIf(point.b < 0);
		Util.panicIf(point.a+">"+dimensionsFrom.a, point.a > dimensionsFrom.a);
		Util.panicIf(point.b > dimensionsFrom.b);
		return new Point((int) (point.a * dimensionsTo.a / dimensionsFrom.a), (int) (point.b * dimensionsTo.b / dimensionsFrom.b) );
	}
	
	public static void main(String ... args) {
		Pair<Double, Double> from = Pair.create(1.0, 1.0);
		Dimension to = new Dimension(100, 100);
		Sheet2DMapperDoubleToInt a = new Sheet2DMapperDoubleToInt(from, to);
		System.out.println(a.metaCompressionRatios());
		System.out.println("map = "+a.map(Pair.create(0.5, 0.5)));
		
		from = Pair.create(1.0, 2.0);
		to = new Dimension(100, 100);
		a = new Sheet2DMapperDoubleToInt(from, to);
		System.out.println(a.metaCompressionRatios());
		System.out.println("map = "+a.map(Pair.create(0.5, 0.5)));
		
		from = Pair.create(2.0, 1.0);
		to = new Dimension(100, 100);
		a = new Sheet2DMapperDoubleToInt(from, to);
		System.out.println(a.metaCompressionRatios());
		System.out.println("map = "+a.map(Pair.create(0.5, 0.5)));
		
		from = Pair.create(0.2, 0.1);
		to = new Dimension(200, 200);
		a = new Sheet2DMapperDoubleToInt(from, to);
		System.out.println(a.metaCompressionRatios());
		System.out.println("map = "+a.map(Pair.create(0.2, 0.1)));
		System.out.println("map = "+a.map(Pair.create(0.0, 0.0)));

	}
	

}
