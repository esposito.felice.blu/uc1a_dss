package gr.neuropublic.mutil.base;

import java.net.InetSocketAddress;
import java.util.StringTokenizer;

public class InetSocketAddressStringProvider implements IProviderFromString<InetSocketAddress> {

	@Override
	public InetSocketAddress fromString(String s) {
		// we expect "hostname:port"
		StringTokenizer st = new StringTokenizer(s, ":", false);
		return new InetSocketAddress(st.nextToken(), new Integer(st.nextToken()).intValue());
	}



}
