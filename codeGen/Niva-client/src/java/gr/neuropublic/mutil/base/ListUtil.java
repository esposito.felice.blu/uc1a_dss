package gr.neuropublic.mutil.base;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ListUtil {

    public static<K> void dispatch(List<K> ks, IVisitor<K> visitor) {
        for (K k : ks)
            visitor.visit(k);
    }

    public static<K, V> V dispatch(List<K> ks, IVisitorReporter<K, V> visitor) {
        for (K k : ks)
            visitor.visit(k);
        return visitor.report();
    }

    public static<K, V> List<V> transform(List<K> ks, ITransformer_1_to_1<K, V> transformer) {
        List<V> vs = new ArrayList<V>();
        for (K k : ks) {
            V v = transformer.transform(k);
            vs.add(v);
        }
        return vs;
    }
	
	public static <T> T getOneAndOnly(List<T> aListOfTsWhichShouldHaveExactlyOneElement) {
		if (aListOfTsWhichShouldHaveExactlyOneElement==null)
			throw new RuntimeException("list is null");
		else if (aListOfTsWhichShouldHaveExactlyOneElement.size()!=1)
			throw new RuntimeException("list has "+aListOfTsWhichShouldHaveExactlyOneElement.size()+" elements");
		else return aListOfTsWhichShouldHaveExactlyOneElement.get(0);
	}
	
	public static <T> T getLast(List<T> listOfTs) {
		if (listOfTs.size()==0) throw new RuntimeException();
		else {
			return listOfTs.get(listOfTs.size()-1);
		}
	}

        public static <T> void remove(List<T> ts, T t) {
            Iterator<T> it = ts.iterator();
            while (it.hasNext()) {
                T val = it.next();
                if (val.equals(t))
                    it.remove();
            }
        }
	
	  /* List order not maintained */
	  public static <T> void removeDuplicate(List<T> arlList)
	  {
	   HashSet<T> h = new HashSet<T> (arlList);
	   arlList.clear();
	   arlList.addAll(h);
	  }
	  
	  /* List order not maintained */
	  public static <T> List<T> removeDuplicateAndReturn(List<T> arlList)
	  {
	   removeDuplicate(arlList);
	   return arlList;
	  }
	
	/* List order maintained */
	public static <T> void removeDuplicateWithOrder(List<T> arlList)
	 {
	 Set<T> set = new HashSet<T>();
	 List<T> newList = new ArrayList<T>();
	 for (Iterator<T> iter = arlList.iterator();    iter.hasNext(); ) {
	 T element = iter.next();
	   if (set.add(element))
	      newList.add(element);
	    }
	    arlList.clear();
	    arlList.addAll(newList);
	}
	
	public static<T> List<T> removeDuplicateWithOrderAndReturn(List<T> arList) {
		removeDuplicateWithOrder(arList);
		return arList;
	}
	


}


