package gr.neuropublic.mutil.base;

public interface ITransformer_1_to_1_withContext<K, V, L> {
	public V transform(K k, L l);
}
