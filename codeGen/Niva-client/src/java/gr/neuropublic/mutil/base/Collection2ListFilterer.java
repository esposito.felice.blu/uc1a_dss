package gr.neuropublic.mutil.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class Collection2ListFilterer  {
	
	public static <V> List<V> dispatch(Collection<V> collectionOfV, ISimpleAcceptor<V> acceptor) {
		List<V> retValue = new ArrayList<V>();
		for (V v : collectionOfV) {
			if (acceptor.accept(v))
				retValue.add(v);
		}
		return retValue;
	}
	
}
