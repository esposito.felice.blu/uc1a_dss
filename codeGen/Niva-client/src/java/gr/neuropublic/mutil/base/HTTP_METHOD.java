package gr.neuropublic.mutil.base;

public enum HTTP_METHOD {GET, POST, CONNECT, HEAD, UNKNOWN};
