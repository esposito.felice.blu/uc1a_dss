package gr.neuropublic.mutil.base;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

// TODO: check whether it behaves exactly like java.util.Arrays::asList, and if so, remove.

public class Array2List {
	
	public static <T> List<T> trsnfrm(T[] ts) {
		List<T> retValue = new ArrayList<T>();
		if (ts==null) return retValue; // short-circuitry added 2010-03-05 2146 to
		                               // counter the trace that appears after end of class
									   // apparently this bug was produced at the first time
									   // a program with a single phase (and hence, no delays array)
									   // was encountered.
		for (T t : ts)
			retValue.add(t);
		return retValue;
	}

	
}
