package gr.neuropublic.mutil.base;

import static gr.neuropublic.mutil.base.Util.panicIfNotNull;

import java.io.Serializable;

public class Pair<A, B> implements Serializable {
	 
	private static final long serialVersionUID = 1L;
	
	public A a;
    public B b;
 
    @Override
	public String toString() {
		return "Pair [a=" + a + ", b=" + b + "]";
	}
	// no args constructor needed by Json 
    public Pair() {
    	super();
    }
    public Pair(final A a, final B b) {
        this.a = a;
        this.b = b;
    }
    
    public static <A, B> Pair<A, B> create(A a, B b) {
        return new Pair<A, B>(a, b);
    }
 
    @SuppressWarnings("unchecked")
	public final boolean equals(Object o) {
        if (!(o instanceof Pair))
            return false;
 
        final Pair<?, ?> other = (Pair) o;
        return equal(a, other.a) && equal(b, other.b);
    }
    
    private static final boolean equal(Object o1, Object o2) {
        if (o1 == null) {
            return o2 == null;
        }
        return o1.equals(o2);
    }
 
    public int hashCode() {
        int ha = a == null ? 0 : a.hashCode();
        int hb = b == null ? 0 : b.hashCode();
 
        return ha + (57 * hb);
    }
    
    public static <A, B> ITransformer_1_to_1<Pair<A,B>, A> accept_a(A a, B b) {
    	// arguments a and b are passed just for type information. If the user
    	// passes non-null values, maybe he has misunderstood something
    	panicIfNotNull(a, b);
    	return new ITransformer_1_to_1<Pair<A,B>, A>() {

			@Override
			public A transform(Pair<A, B> v) {
				return v.a;
			}
    	};
    }
    
    public static <A, B> ITransformer_1_to_1<Pair<A,B>, B> accept_b(A a, B b) {
    	// arguments a and b are passed just for type information. If the user
    	// passes non-null values, maybe he has misunderstood something
    	panicIfNotNull(a, b);
    	return new ITransformer_1_to_1<Pair<A,B>, B>() {

			@Override
			public B transform(Pair<A, B> v) {
				return v.b;
			}
    	};
    }
}