// Copyright (c) 2001 Hursh Jain (http://www.mollypages.org) 
// The Molly framework is freely distributable under the terms of an
// MIT-style license. For details, see the molly pages web site at:
// http://www.mollypages.org/. Use, modify, have fun !

package gr.neuropublic.mutil.base;

import gr.neuropublic.mutil.io.IOUtil;
import java.io.*;
import java.util.*;


/** 
Manages arguments passed to an application.  Arguments can be optional or 
required. Arguments received by this class have to be either of the
following formats: 
<blockquote>
<tt>
-flagname1 value 
-flagname2=value2
</tt>. 
</blockquote>
Arguments consist of flag name and flag value pairs. All flags must
begin with a <tt>'-'</tt> character and any new word beginning
with a <tt>'-'</tt> character is seen as the start of a new flag name
(the <tt>'-'</tt> character is always permissible <b>inside</b> a
token, i.e., after the first character). Additionaly, a flag name is
delimited from it's corresponding value by either a space or the
<tt>'='</tt> character. 
<p>Note, in the above example, <tt>- flagname1</tt> is separated from
it's value by a space whereas <tt>- flagname2</tt> is seperated from
it's value by a <tt>'='</tt> character. Both delimiters are valid and
can be intermixed on the same line.
<p>
Note also that if the delimiter is a space, then the value part cannot
begin with the <tt>-</tt> character, but if the delimiter is <tt>=</tt>,
then the value part can begin with the <tt>-</tt> character. That is, the
following input is <b>valid</b>:
<blockquote><tt>-flagname1=-value-for-flag1</tt></blockquote>
<p>whereas this is <b>invalid</b>:
<blockquote><tt>-flagname1 -value-for-flag1</tt></blockquote>
Here, <tt>-value-for-flag1</tt> is seen as a new flag itself.
<p>
Following a flag name, all following values are assigned to that flagname,
up until another flagname is encountered or the end of input is reached.
Therefore, spaces do not have to be quoted. The following example,
<blockquote>
<tt>-path \some file\some where else -port 80</tt>
</blockquote>
is parsed as:
<dl>
<dt><tt><b>path</b></tt></dt>
	<dd><tt>\some file\some where else</tt></dd>
<dt><tt><b>port</b></tt></dt>
	<dd><tt>80</tt></dd>
</dl>
<p>
Different operating systems (and their command interpreters) also
transform/modify command lines in various ways. This class (and
Java/JVM) can only parse arguments that it recieves from the operating
system and those may be different than what you may have specified on
the command line, due to the aforementioned transformation. The
following example,
<blockquote>
<tt>-path "/foo/bar /baz" -anotherpath \"a/b c/\"</tt>
</blockquote>
is parsed as:
<dl>
<dt><tt><b>path</b></tt></dt>
	<dd><tt>/foo/bar /baz</tt></dd>
<dt><tt><b>anotherpath</b></tt></dt>
	<dd><tt>&quot;a/b c/&quot;</tt></dd>
</dl>
Note, that the <tt>path</tt> value has lost it's double quotes whereas
the <tt>anotherpath</tt> value has not. This is because the O.S., in this case,
stripped the quotes for <tt>path</tt> since they were not escaped on the
command line. Note also that various unix'en convert command line args into 
space delimited tokens. <code>bash</code> for example tokenizes the lines 
using the IFS variable, and then feeds the tokens (separated by space) to the
invoked program, in this case, the java interpreter.
<p>
Sometimes flags are specified <b>without</b> any corresponding value, with the
existence of the flag itself implying some condition. However for clarity,
it's sometimes better to explicitly set an option to yes or no. Therefore
instead of saying <tt>-flag</tt>, always say <tt>-flag=[yes|no]</tt>, with 
the value part being "yes" or "no". However, the existence of the flag by 
itself <b>can</b> be simply checked by using the <code>flagExists()</code> method. 
<p>
Typical usage of this class may look like:
<xmp>
//inside the main method
Args myargs = new Args(args);
args.setUsage("java myApp -port 80 -foo=moo -debugport=5432"); //to inform user
int port	= args.getRequired("port");	//get required "-port" flag
String abc	= args.get("foo");	//get optional "-foo" flag
String bar	= args.get("bar","5000"); //get optional "bar" flag, default to "5000"
</xmp>
</b>

Thread Safety: This class <b>is</b> thread safe and can be used by multiple
threads after it's instantiated.

@author hursh jain	
@version 1.0 7/15/2001
**/
public class Args
{
protected 	String 		usage;
protected 	String[] 	input;
protected	Map			flagMap;

/** 
If a flag name is not well formed, then the corresponding value (if any)
is stored under this name. For example: <blockquote><tt>- abc</tt></blockquote>
is missing a flagname (<tt>'-'</tt> followed by whitespace), therefore the 
value <tt>abc</tt> is stored under this default name.
**/
public static String DEFAULT_FLAGNAME = "default";

/** 
If a flag name is repeated, then all corresponding values for that name are 
concatenated with each other and the concatenated values are <b>delimited</b> 
by this value. It's value in this class is <tt>,</tt>. For example: 
<blockquote><tt>-foo abc -foo xyz</tt></blockquote> has the same flag repeated 
twice (<tt>foo</tt>) and the resulting value for this flag will be <blockquote>
<tt>abc<b>,</b>xyz</tt></blockquote>
**/
public static String FLAG_REPEAT_VALUE_DELIM = ",";

/** 
Creates a new instance, with the specified String[] to read arguments from. 
Typically, the <tt>static void main(String[] args)</tt> method gets a String[]
array from the JVM, containing the arguments passed to the application. This 
array will typically be be passed to this constructor.
**/
public Args(String args[]) 
	{ 
	input = args;
	parseFlags();
	}

/** 
Creates a new instance, with the specified InputStream to read arguments from.
Reads as much as possible from the specified stream, so as to not block and
delay the construction of this object. Any data not available via the specified
InputStream at the time of construction, is not used later on and is essentially
lost. The stream encoding is assumed to be ISO-8859-1, i.e., the flags names
and values read from the stream are treated as normal ascii characters. Be aware
of differences between parsing command line args and parsing args from a file, 
because typically command line args are modified by the shell before being passed
to the application.
**/
public Args(InputStream in) 
	{ 
	toStringArray(in);	
	parseFlags();
	}


/** 
Checks if the specified flag exits, regardless of whether a corresponding
value exist for that flag. For example, suppose the invoked arguments looked
like: 
<blockquote><tt>-foo -bar=baz</tt></blockquote>. Then the flag <tt>foo</tt> 
would exist but have no corresponding value.
@param	flagname the flagname to check
@return true if flag exists, false otherwise
**/
public boolean flagExists(String flagname) 
	{
	boolean exists = false;
	if (this.flagMap.containsKey(flagname)) 
		{
		exists = true;
		}
	return exists;
	}

/** Returns the total number of flags available**/
public int getFlagCount() {
	return flagMap.size();
	}

/** 
Returns the fully qualified name of the class that contained
the <tt>main()</tt> method used to invoke the application.
<b>Note: </b> this method must be called from the same thread
(the "main" thread) that started the application. Newly started
threads that call this method will instead obtain the class name 
containing that thread's <tt>run()</tt> method.
**/
public String getMainClassName() 
	{
   	StackTraceElement[] st = new Throwable().getStackTrace();
	//System.out.println("stack trace=" + Arrays.asList(st));
	StackTraceElement mainElement = st[st.length-1];
	String className = mainElement.getClassName();
	return className;
	}


/** 
Returns an argument by positional value. <tt>get(n)</tt>
is the same as <tt>static void main(String[] args) --> args[n]
</tt> and is provided here for convenience.

@param	n 	the 0-based index of the String[] passed to main()
@throws NullPointerException 
			if the specified index is greater than the number
			of arguments
**/
public String get(int n) {
	return input[n];
	}

/** 
Returns a required argument by positional value. <tt>get(n)</tt>
is the same as <tt>static void main(String[] args) --> args[n]
</tt> and is provided here for convenience. Ff the specified 
index is greater than the number of arguments, then the error
is handled in the same way as {@link #getRequired(String)}

@param	n 	the 0-based index of the String[] passed to main()
**/
public String getRequired(int n) {
	if (n < 0 || n > (input.length - 1)) {
		handleError("Missing required argument at position [" + n + "]");
		}
	return input[n];
	}


/** 
Returns the value corresponding to the specified flag. 

@param	flagname	the name of the flag <b>without</b> the leading <tt>-</tt> character
@return	<tt>null</tt> either if the flag itself was not found, or if the 
		flag was found but no associated value was found. 
**/
public String get(String flagname)
	{
	return (String) flagMap.get(flagname);	
	}

/** 
Returns the value corresponding to the specified flag. If
the flag and/or it's value is not found, this method returns
the specified backup value instead.

@param 	flagname	the name of the flag <b>without</b> the leading <tt>-</tt> character
@param 	backup		the value to return if flag not found	
@return	value of specified flag or backup string
**/
public String get(String flagname, String backup)
	{
	String val = get(flagname);
	if (val == null) 
		{
		return backup;	
		}
	else return val;
	}


/**
Returns the value corresponding to the specified flag. If
the flag and/or it's value is not found, this method returns
the specified backup value instead.

If the property <b>is</b> present, then the corresponding
value is <tt>true</tt> if the property value is any of the
following (case-insensitive):
<blockquote>
<code>
yes, 1, true
</code>
<blockquote>
else <tt>false</tt> is returned. Also see {@link #flagExists}.

@param 	flagname	the name of the flag <b>without</b>
					the leading <tt>-</tt> character
@param 	backup		value to return if the property for the 
					specified property is <b>not</b> present	

@return	value of specified key or backup string
*/
public boolean getBoolean(String flagname, boolean backup) 
	{
	String val = get(flagname);

    if (val == null)
    	 return backup;
    else
  		 return toBoolean(val);
    }

/**
Returns the value corresponding to the specified flag. If
the flag and/or it's value is not found or is found but
cannot be converted into an integer (via a {@link
Integer.parseInt(String)} call), the backup value will be
returned.

@param 	flagname	the name of the flag <b>without</b>
					the leading <tt>-</tt> character
@param 	backup		value to return if the property for the 
					specified property is <b>not</b> present	

@return	value of specified key or backup string
*/
public int getInt(String flagname, int backup) 
	{
    String val = get(flagname);
    if (val != null)
    	{
   		try {
	    	return Integer.parseInt(val);
	    	}
	    catch (NumberFormatException e) {
	    	System.err.println("Cannot convert flag '" + flagname + "', into a number, returning backup value " + backup);
	    	}
	    }
	return backup;
    }


  public Integer getInteger(String flagname) {
    String val = get(flagname);
    if (val != null)
    	{
   		try {
	    	return Integer.parseInt(val);
	    	}
	    catch (NumberFormatException e) {
	    	System.err.println("Cannot convert flag '" + flagname + "', into a number, returning null");
	    	}
	    }
      return null;
    }


public long getLong(String flagname, long backup) 
	{
    String val = get(flagname);
    if (val != null)
    	{
   		try {
	    	return Long.parseLong(val);
	    	}
	    catch (NumberFormatException e) {
	    	System.err.println("Cannot convert flag '" + flagname + "', into a number, returning backup value " + backup);
	    	}
	    }
	return backup;
    }

    public Long getLong(String flagname)
	{
    String val = get(flagname);
    if (val != null)
    	{
   		try {
	    	return Long.parseLong(val);
	    	}
	    catch (NumberFormatException e) {
	    	System.err.println("Cannot convert flag '" + flagname + "', into a number, returning null");
	    	}
	    }
	return null;
    }


/** 
Returns the value corresponding to the specified flag. If
the flag's value is not found (even if the flag name
exists), this method calls the <code>error method</code>,
which by default prints a stack trace and exits the
application.

<p>If the handleError method is overriden to <b>not</b> exit
the application, then this method will return <tt>null</tt>
if the specified flag is not found.

@param 	flagname	the name of the flag <b>without</b> the leading <tt>'-'</tt> character
@return	value of specified flag
@see	#handleError
**/
public String getRequired(String flagname)
	{
	if (flagname.equals("")) {
		handleError("This program expects invocation flag(s).");
		return null;
		}
		
	String val = get(flagname);
	if (val == null) {
		handleError("Missing required flag: " + flagname);
		}
	
	return val;
	}

/*
Returns the value obtained via {@link getRequired(String)}
as as a boolean. The boolean value returned is
<code>true</code> if the property value is any of the
following (case-insensitive):
<blockquote>
<code>
yes, 1, true
</code>
<blockquote>
else <tt>false</tt> is returned.

<p>If the handleError method is overriden to <b>not</b> exit
the application, then this method will return <tt>false</tt>
if the specified flag is not found.

@param 	flagname	the name of the flag <b>without</b> the leading <tt>'-'</tt> character
@return	value of specified flag as a boolean
@see	#handleError
*/
public boolean getRequiredBoolean(String flagname) 
	{
	String val = getRequired(flagname);
	return toBoolean(val);	
	}

/*
Returns the value obtained via {@link getRequired(String)}
as as an integer. If the property cannot be converted into
an integer (via a {@link Integer.parseInt(String)} call),
the resulting action would be the same as if the required
flag was not present.

<p>If the handleError method is overriden to <b>not</b> exit
the application, then this method will return <tt>0</tt>
if the specified flag is not found.

@param 	flagname	the name of the flag <b>without</b> the leading <tt>'-'</tt> character
@return	value of specified flag as a boolean
@see	#handleError
*/
public int getRequiredInt(String flagname)
	{
	String val = getRequired(flagname);
   	
   	int result = 0;
   	
   	try {
	    result = Integer.parseInt(val);
	    }
	catch (NumberFormatException ne) {
	    System.err.println("Cannot convert flag '" + flagname + "', into a number");
		handleError(flagname);
		}
	
	return result;
	}
	

/**
Returns the raw String[] passed to this object during construction. If
an InputStream was passed in via construction, returns a String[] created
from that InputStream (tokenized by whitespace). The returned String[]
is modifiable but it's modification will not affect any other method in
this class.
@return	String[] of unparsed values
**/
public String[] rawValues() 
	{
	return this.input;
	}


/**
Returns a Map containing all flagname/flagvalue pairs. The returned
map is an <b>unmodifiable</b> view of the map contained in this class.
@return	Map of flagnames/values
**/
public Map values()
	{
	return Collections.unmodifiableMap(this.flagMap);
	}


/** 
Specify program usage information to be output when an error occurs. This 
information should contain a short description of expected and/or optional
flags and information that the application expects.
@param str	Usage information
**/
public void setUsage(String str)
	{
	this.usage = str;		
	}

/** 
Convenience method to display the usage information in case
of program error. Reads and sets the usage file found in the
same directory/resource location as the calling object's
package. For example, if clazz <tt>a.b.foo</tt> calls this
method, then the usage file should exist in the <tt>a/b</tt>
directory (typically starting from the classpath or a
jar/zip file).
<p>
The name of the usage file is the same as the class name of
the specified object (without the ending ".java") appended
with "<tt>_usage.txt</tt>". Therefore the usage file for
code residing in <tt>a.b.foo.java</tt> should be called
<tt>foo_usage.txt</tt> and reside in the <tt>a/b</tt>
directory.

@param	caller	the calling object <i>or</i> {@link java.lang.Class} Class 
				corresponding to that object
**/
public void setDefaultUsage(Object caller)
	{
	try {
		Class clazz = caller.getClass();
		
		String usageFile = StringUtil.fileName(
				clazz.getName().replace('.', '/')) + "_usage.txt";
		
		InputStream in = clazz.getResourceAsStream(usageFile);
     	if (in != null) 
     		setUsage( IOUtil.inputStreamToString(in, false) );
     	else 
     		setUsage( "Cannot display usage instructions, " + usageFile + " not found");
     	}
	catch (IOException e) {
		e.printStackTrace();
		}
	}

/** 
Convenience method to display the usage information in case of
program error. Reads and sets the usage file found specified by
the <tt>pathToFile</tt> argument of this method. This path should
be a path to a file reachable from any directory in the classpath
(i.e., relative to the classpath) and should not start with a "/"
**/
public void setDefaultUsage(String pathToFile)
	{
	try {
		InputStream in = ClassLoader.getSystemResourceAsStream(pathToFile);
		if (in != null) 
			setUsage( IOUtil.inputStreamToString(in, false) );
		else 
			setUsage( "Cannot display usage instructions, " + pathToFile + " not found");
		}
	catch (IOException e) {
		e.printStackTrace();
		}
	}

/** 
Invoking this method gives the same results as invoking
{@link #getRequired(String) getRequired} with a non-existent flag name.
**/
public void showError() {
	getRequired("");
	}

/** 
Returns the PrintStream where error messages will be sent. This
method can be overriden in sublasses to return a different PrintStream. 
**/
protected PrintStream getOutputStream()
	{
	return System.err;
	}

final boolean toBoolean(String val)
	{
	val = val.trim().toLowerCase().intern();
	if ( val == "true" || val == "yes" || val == "1" )
		return true;
	return false;
	}

/** 
Handles an error situation such as when a required flag is
not found. By default, calls the <tt>getOutputStream</tt> method,
prints an error message and exits the JVM by calling <tt>System.exit(1)</tt>
This method can be overriden in subclasses to handle errors differently.
@param 	str		the error message
@see #getOutputStream
**/
protected void handleError(String str)
	{
	PrintStream out = getOutputStream();
	out.println(str);
	if (usage != null) {
		out.println(usage);
		}
	out.println();
	out.println("-------- Stack Trace --------");
	new Exception().printStackTrace(out);
	System.exit(1);
	}

public static void main(String[] args) throws Exception
	{
	new Test(args);		
	}

static private class Test
{
Test(String[] args) throws Exception
	{
	Args myarg = new Args(args);
	System.out.println("Main Class Name: " + myarg.getMainClassName());
	System.out.println("Command line parsed as: "+myarg.values());	
	
	String testbytes =  
		"-port 80 -port2=5000 -flow c:\\program files -foo " +  
		"-moo \"moo moo\" -x aaa -x bbb - default";
	
	Args myarg2 = new Args(new ByteArrayInputStream(testbytes.getBytes("ISO-8859-1")));
	Map values = myarg2.values();
	System.out.println("testbyte string parsed as: " + values); 
	try { 
		values.put("foo","bar"); 
		} 
	catch (Exception e) { e.printStackTrace(); }
	String[] rawvals = myarg2.rawValues();
	System.out.println("raw values="+Arrays.asList(rawvals));
	System.out.println("get(port2)="+myarg2.get("port2"));
	System.out.println("get(default)="+myarg2.get(DEFAULT_FLAGNAME));
	System.out.println("get(x)="+myarg2.get("x"));
	System.out.println("flagExists(moo)="+myarg2.flagExists("flow")); 
	System.out.println("flagExists(moo)="+myarg2.flagExists("foo")); 
	System.out.println("flagExists(doesnotexits)="+myarg2.flagExists("doesnotexist")); 
	myarg2.setUsage("Usage: java Args.java your arguments here");
	myarg2.getRequired("doesnotexist");
	}
}	//~class Test	
	
//-------------------- implementation helper methods ----------------------

//converts the inputstream into a whitespace delimited array of tokens
//and then saves those tokens as the input (a String[]) instance variable
private void toStringArray(InputStream in)
	{
	try {
		int len = in.available();
		byte[] arr = new byte[len]; 
		//will read upto len bytes into arr, shouldn't block
		new BufferedInputStream(in).read(arr, 0, len);	
     	String temp = new String(arr, "ISO-8859-1");
		//default tokenizer delimits by all whitespace and does 
		//not return delimiters themselves.
     	StringTokenizer st = new StringTokenizer(temp); 
     	int toknum = st.countTokens();
     	this.input = new String[toknum];
     	int n = 0;
     	while (st.hasMoreTokens()) {
     		this.input[n++] = st.nextToken().trim();
     		}
     	}
	catch (IOException e) {
		this.input = null;
		}
	}

//reads this.input(String[]), parses and stores the arguments in this.flagMap (as
//key=value pairs). A state machine approach to this is *really* painful, surprisingly 
//so. (i tried it and it was 5 times the lines of code over this method and it was still
//kinda buggy), while this method, although apparently hacky, works like a charm.
private void parseFlags()
	{
	if (input == null) {
		return;
		}
	flagMap = new HashMap();
	String flagname = null, flagvalue = null;
	int fp = findNextFlag(0);
	int nextfp = 0;
	int endvalp = 0; //current flagvalue to be read until this position
	while (fp != -1) 
		{
		flagname = input[fp];
		flagvalue = null;
		int eqindex = flagname.indexOf('=');
		if (eqindex != -1)  {
			flagvalue = flagname.substring(eqindex+1, flagname.length());
			flagname = flagname.substring(0, eqindex);
			}	
		nextfp = findNextFlag(fp+1); 
		endvalp = (nextfp == -1) ? input.length : nextfp;
		for (int n = fp + 1; n < endvalp; n++) {
			if (flagvalue == null ) { flagvalue = input[n]; }
			else 					{ flagvalue += " " + input[n]; }
			}
		if (flagname.length() == 1) { 
			flagname = DEFAULT_FLAGNAME;	
			} 
		else { 
			flagname = flagname.substring(1,flagname.length()); 
			}
		if (flagMap.containsKey(flagname)) { //append if flagvalue already exists
			flagvalue = flagMap.get(flagname) + FLAG_REPEAT_VALUE_DELIM + flagvalue;
			}
		flagMap.put(flagname, flagvalue);
		fp = nextfp;
// 		for debugging
//		System.out.println("flagname="+flagname);
//		System.out.println(eqindex);
//		System.out.println("fp, nextfp, endvalp = "+fp+","+nextfp+","+endvalp);
//		System.out.println("flagname, flagvalue="+flagname +"," + flagvalue);
		} 	//~while		 
	}		//~parseFlags

//finds the position of the next -flag starting from pos or -1 if no flag found
private int findNextFlag(int pos) 
	{
	for (int n = pos; n < input.length; n++) 
		{
//		System.out.println("n, input[n]="+n+","+input[n]);
		if (input[n].startsWith("-")) 
			{
			return n;
			}
		}
	return -1;
	}

}		//~class Args