package gr.neuropublic.mutil.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class Collection2ListTransformer_withContext<K, V, L> {
	
	private L context;
	public Collection2ListTransformer_withContext(L context) {
		this.context = context;
	}
	
	public List<V> dispatch(Collection<K> collectionOfK, ITransformer_1_to_1_withContext<K, V, L> transformer) {
		List<V> listOfVs = new ArrayList<V>();
		for (K k: collectionOfK) {
			listOfVs.add(transformer.transform(k, context));
		}
		return listOfVs;
	}
	
}
