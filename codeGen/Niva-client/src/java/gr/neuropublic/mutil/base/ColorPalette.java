package gr.neuropublic.mutil.base;

import java.awt.Color;

public class ColorPalette {
	public static class Whites {
	public static final Color Snow = new Color(255,250,250);
	public static final Color Snow_b = new Color(238,233,233);
	public static final Color Snow_c = new Color(205,201,201);
	public static final Color Snow_d = new Color(139,137,137);
	public static final Color Ghost_White = new Color(248,248,255);
	public static final Color White_Smoke = new Color(245,245,245);
	public static final Color Gainsboro = new Color(220,220,220);
	public static final Color Floral_White = new Color(255,250,240);
	public static final Color Old_Lace = new Color(253,245,230);
	public static final Color Linen = new Color(240,240,230);
	public static final Color Antique_White = new Color(250,235,215);
	public static final Color Antique_White_b = new Color(238,223,204);
	public static final Color Antique_White_c = new Color(205,192,176);
	public static final Color Antique_White_d = new Color(139,131,120);
	public static final Color Papaya_Whip = new Color(255,239,213);
	public static final Color Blanched_Almond = new Color(255,235,205);
	public static final Color Bisque = new Color(255,228,196);
	public static final Color Bisque_b = new Color(238,213,183);
	public static final Color Bisque_c = new Color(205,183,158);
	public static final Color Bisque_d = new Color(139,125,107);
	public static final Color Peach_Puff = new Color(255,218,185);
	public static final Color Peach_Puff_b = new Color(238,203,173);
	public static final Color Peach_Puff_c = new Color(205,175,149);
	public static final Color Peach_Puff_d = new Color(139,119,101);
	public static final Color Navajo_White = new Color(255,222,173);
	public static final Color Moccasin = new Color(255,228,181);
	public static final Color Cornsilk = new Color(255,248,220);
	public static final Color Cornsilk_b = new Color(238,232,205);
	public static final Color Cornsilk_c = new Color(205,200,177);
	public static final Color Cornsilk_d = new Color(139,136,120);
	public static final Color Ivory = new Color(255,255,240);
	public static final Color Ivory_b = new Color(238,238,224);
	public static final Color Ivory_c = new Color(205,205,193);
	public static final Color Ivory_d = new Color(139,139,131);
	public static final Color Lemon_Chiffon = new Color(255,250,205);
	public static final Color Seashell = new Color(255,245,238);
	public static final Color Seashell_b = new Color(238,229,222);
	public static final Color Seashell_c = new Color(205,197,191);
	public static final Color Seashell_d = new Color(139,134,130);
	public static final Color Honeydew = new Color(240,255,240);
	public static final Color Honeydew_b = new Color(244,238,224);
	public static final Color Honeydew_c = new Color(193,205,193);
	public static final Color Honeydew_d = new Color(131,139,131);
	public static final Color Mint_Cream = new Color(245,255,250);
	public static final Color Azure = new Color(240,255,255);
	public static final Color Alice_Blue = new Color(240,248,255);
	public static final Color Lavender = new Color(230,230,250);
	public static final Color Lavender_Blush = new Color(255,240,245);
	public static final Color Misty_Rose = new Color(255,228,225);
	public static final Color White = new Color(255,255,255);
	}
	public static class Grays {
	public static final Color Black = new Color(0,0,0);
	public static final Color Dark_Slate_Gray = new Color(49,79,79);
	public static final Color Dim_Gray = new Color(105,105,105);
	public static final Color Slate_Gray = new Color(112,138,144);
	public static final Color Light_Slate_Gray = new Color(119,136,153);
	public static final Color Gray = new Color(190,190,190);
	public static final Color Light_Gray = new Color(211,211,211);
	}
	public static class Blues {
	public static final Color Midnight_Blue = new Color(25,25,112);
	public static final Color Navy = new Color(0,0,128);
	public static final Color Cornflower_Blue = new Color(100,149,237);
	public static final Color Dark_Slate_Blue = new Color(72,61,139);
	public static final Color Slate_Blue = new Color(106,90,205);
	public static final Color Medium_Slate_Blue = new Color(123,104,238);
	public static final Color Light_Slate_Blue = new Color(132,112,255);
	public static final Color Medium_Blue = new Color(0,0,205);
	public static final Color Royal_Blue = new Color(65,105,225);
	public static final Color Blue = new Color(0,0,255);
	public static final Color Dodger_Blue = new Color(30,144,255);
	public static final Color Deep_Sky_Blue = new Color(0,191,255);
	public static final Color Sky_Blue = new Color(135,206,250);
	public static final Color Light_Sky_Blue = new Color(135,206,250);
	public static final Color Steel_Blue = new Color(70,130,180);
	public static final Color Light_Steel_Blue = new Color(176,196,222);
	public static final Color Light_Blue = new Color(173,216,230);
	public static final Color Powder_Blue = new Color(176,224,230);
	public static final Color Pale_Turquoise = new Color(175,238,238);
	public static final Color Dark_Turquoise = new Color(0,206,209);
	public static final Color Medium_Turquoise = new Color(72,209,204);
	public static final Color Turquoise = new Color(64,224,208);
	public static final Color Cyan = new Color(0,255,255);
	public static final Color Light_Cyan = new Color(224,255,255);
	public static final Color Cadet_Blue = new Color(95,158,160);
	}
	public static class Greens {
	public static final Color Medium_Aquamarine = new Color(102,205,170);
	public static final Color Aquamarine = new Color(127,255,212);
	public static final Color Dark_Green = new Color(0,100,0);
	public static final Color Dark_Olive_Green = new Color(85,107,47);
	public static final Color Dark_Sea_Green = new Color(143,188,143);
	public static final Color Sea_Green = new Color(46,139,87);
	public static final Color Medium_Sea_Green = new Color(60,179,113);
	public static final Color Light_Sea_Green = new Color(32,178,170);
	public static final Color Pale_Green = new Color(152,251,152);
	public static final Color Spring_Green = new Color(0,255,127);
	public static final Color Lawn_Green = new Color(124,252,0);
	public static final Color Chartreuse = new Color(127,255,0);
	public static final Color Medium_Spring_Green = new Color(0,250,154);
	public static final Color Green_Yellow = new Color(173,255,47);
	public static final Color Lime_Green = new Color(50,205,50);
	public static final Color Yellow_Green = new Color(154,205,50);
	public static final Color Forest_Green = new Color(34,139,34);
	public static final Color Olive_Drab = new Color(107,142,35);
	public static final Color Dark_Khaki = new Color(189,183,107);
	public static final Color Khaki = new Color(240,230,140);
	} public static class Yellows {
	public static final Color Pale_Goldenrod = new Color(238,232,170);
	public static final Color Light_Goldenrod_Yellow = new Color(250,250,210);
	public static final Color Light_Yellow = new Color(255,255,224);
	public static final Color Yellow = new Color(255,255,0);
	public static final Color Gold = new Color(255,215,0);
	public static final Color Light_Goldenrod = new Color(238,221,130);
	public static final Color Goldenrod = new Color(218,165,32);
	public static final Color Dark_Goldenrod = new Color(184,134,11);
	} public static class Browns {
	public static final Color Rosy_Brown = new Color(188,143,143);
	public static final Color Indian_Red = new Color(205,92,92);
	public static final Color Saddle_Brown = new Color(139,69,19);
	public static final Color Sienna = new Color(160,82,45);
	public static final Color Peru = new Color(205,133,63);
	public static final Color Burlywood = new Color(222,184,135);
	public static final Color Beige = new Color(245,245,220);
	public static final Color Wheat = new Color(245,222,179);
	public static final Color Sandy_Brown = new Color(244,164,96);
	public static final Color Tan = new Color(210,180,140);
	public static final Color Chocolate = new Color(210,105,30);
	public static final Color Firebrick = new Color(178,34,34);
	public static final Color Brown = new Color(165,42,42);
	} public static class Oranges {
	public static final Color Dark_Salmon = new Color(233,150,122);
	public static final Color Salmon = new Color(250,128,114);
	public static final Color Light_Salmon = new Color(255,160,122);
	public static final Color Orange = new Color(255,165,0);
	public static final Color Dark_Orange = new Color(255,140,0);
	public static final Color Coral = new Color(255,127,80);
	public static final Color Light_Coral = new Color(240,128,128);
	public static final Color Tomato = new Color(255,99,71);
	public static final Color Orange_Red = new Color(255,69,0);
	public static final Color Red = new Color(255,0,0);
	} public static class Pinks {
	public static final Color Hot_Pink = new Color(255,105,180);
	public static final Color Deep_Pink = new Color(255,20,147);
	public static final Color Pink = new Color(255,192,203);
	public static final Color Light_Pink = new Color(255,182,193);
	public static final Color Pale_Violet_Red = new Color(219,112,147);
	public static final Color Maroon = new Color(176,48,96);
	public static final Color Medium_Violet_Red = new Color(199,21,133);
	public static final Color Violet_Red = new Color(208,32,144);
	public static final Color Violet = new Color(238,130,238);
	public static final Color Plum = new Color(221,160,221);
	public static final Color Orchid = new Color(218,112,214);
	public static final Color Medium_Orchid = new Color(186,85,211);
	public static final Color Dark_Orchid = new Color(153,50,204);
	public static final Color Dark_Violet = new Color(148,0,211);
	public static final Color Blue_Violet = new Color(138,43,226);
	public static final Color Purple = new Color(160,32,240);
	public static final Color Medium_Purple = new Color(147,112,219);
	public static final Color Thistle = new Color(216,191,216);
	}
}


