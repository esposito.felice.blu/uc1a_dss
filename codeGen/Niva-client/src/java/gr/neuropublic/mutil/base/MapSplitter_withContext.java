package gr.neuropublic.mutil.base;

import java.util.HashMap;
import java.util.Map;

public class MapSplitter_withContext<K, V, L> {
	
	private L context;
	public MapSplitter_withContext(L context) {
		this.context = context;
	}

	public Pair<Map<K, V>, Map<K, V>> split(Map<K, V> theMapToSplit, IAcceptor<K, V, L> acceptor) {
		Map<K, V> a = new HashMap<K, V>();
		Map<K, V> b = new HashMap<K, V>();
		for (K k : theMapToSplit.keySet()) {
			if (acceptor.accept(k, theMapToSplit.get(k), context))
				a.put(k, theMapToSplit.get(k));
			else
				b.put(k, theMapToSplit.get(k));
		}
		return Pair.create(a, b);
	}
}
