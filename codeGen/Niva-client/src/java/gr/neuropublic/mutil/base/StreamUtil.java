package gr.neuropublic.mutil.base;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;


public class StreamUtil {

	public static void synchEatUp(InputStream is) {
		byte[] buffer = new byte[2048];
		try {while (is.read(buffer)!=-1);} catch (Exception e) {throw new ExceptionAdapter(e);}
	}
	
	/**
	 * @Deprecated See Java bug 4254082 - InputStream::skip doesn't work!! 
	 */
	public static long skipTillTheEnd(InputStream is) {
		long bytesSkipped = 0;
		final long SKIP_SIZE = 2048;
		try {
			while ( true ) {
				long skipped = is.skip(SKIP_SIZE);
				System.out.println("skip returned:"+skipped);
				bytesSkipped += skipped;
				if (skipped != SKIP_SIZE) break;
			}
			return bytesSkipped;
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
	}
	
	public static void main(String ...args) throws IOException {
		StringBuffer sb = new StringBuffer();
		for (int i = 0 ; i < 20000 ; i++) {
			sb.append(i);
		}
		InputStream is = new ByteArrayInputStream(sb.toString().getBytes());
		skipTillTheEnd(is);
		System.out.println("skip doesn't hung on a ByteArrayInputStream");


		File tempFile = File.createTempFile("skip", "fails");
		FileWriter fw = new FileWriter(tempFile);
		fw.write(sb.toString());
		fw.close();
		
		is = new FileInputStream(tempFile);
		System.out.println("about to skip on a FileInputStream");
		skipTillTheEnd(is);
		System.out.println("you should never see this message / skip hungs");
	}
}
