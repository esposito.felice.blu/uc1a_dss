package gr.neuropublic.mutil.base;

import static gr.neuropublic.mutil.base.Util.*;
import Jama.Matrix;
public class Interpolations {
	
	public static int linear(int a, int b, int distance, int x) {
		panicIfLessThanOrEqualToZero(distance);
		panicIfLessThanZero(x);
		panicIfLessThanZero(distance-x);
		int l = castToIntWithChecks(Math.round( ((double) ((b-a)*x))/((double) distance)
				                              )
		                            );
		int retValue = a+l;
		panicIfNotWithin(a, b, retValue);
		return retValue;
	}
	
	public static int[] linearAll(int a, int b, int distance) {
		int[] retValue = new int[distance+1];
		for (int x = 0 ; x < distance+1 ; x++)
			retValue[x] = linear(a, b, distance, x); 
		return retValue;
	}
	
	public static int polynomial(int a, int b, int distance, int bulgePercentage, int x) {
		// if the distance is 1 or less, ignore the bulge percentage and use linear interpolation, else use polynomial (quadratic with a mid-length bulge)
		if (distance<=1)
			return linear(a, b, distance, x);
		else {
			// if the distance is 2 or more, plot a binomial interpolation with a midway bulge which is given
			// as a percentage of the difference between the two end values
			/*
			 * the points are: (0, a), (distance/2, a+(a-b)*(percentage/100)), (distance, b)
			 */
			double x1 = 0         ;  double y1 = a;
			double x2 = distance/2;  double y2 = a+ ((b-a)/2) + Math.round((abs(a-b)*bulgePercentage)/100.);
			double x3 = distance  ;  double y3 = b;
	        double[][] aarr     = {{x1*x1,x1,1},
	        		               {x2*x2,x2,1},
	        		               {x3*x3,x3,1}}; // this is the Vandermonde matrix for a 3-point polynomial extrapolation
	        Matrix     A = new Matrix(aarr);
	        double[][]    barr = {{y1}, {y2}, {y3}};
	        Matrix        bM  = new Matrix(barr);
	        //System.out.println("trying to solve Ax=B where:");
	        //System.out.println("A is:");
	        //A.print(20, 2);
	        //System.out.println("B is:");
	        //bM.print(20,2);
	        Matrix        xM = A.solve(bM);
	        
	        double a2 = xM.get(0, 0);
	        double a1 = xM.get(1, 0);
	        double a0 = xM.get(2, 0);
	        
	        return castToIntWithChecks(a2*x*x + a1*x + a0);
		}
	}
	
	public static int[] polynomialAll(int a, int b, int distance, int percentage) {
		int[] retValue = new int[distance+1];
		for (int x = 0 ; x < retValue.length ; x++)
			retValue[x] = polynomial(a, b, distance, percentage, x);
		return retValue;
	}
	
	public static int skewedLinear(int a, int b, int percentage, int distance, int x) {
		int supraComponent = (abs(a-b)*percentage) / 100;
		int supra_a;
		int supra_b;
		int supra_distance = -1;
		int supra_distance_a = distance / 2;
		int supra_distance_b = distance - supra_distance_a;
		int supra_x ;
		if (x<supra_distance_a) {
			supra_a = 0;
			supra_b = supraComponent;
			supra_distance = supra_distance_a;
			supra_x = x;
		} else {
			supra_a = supraComponent;
			supra_b = 0;
			supra_distance = supra_distance_b;
			supra_x = x - supra_distance_a;
		}
		return linear(a, b				, distance      , x) +
			   linear(supra_a, supra_b  , supra_distance, supra_x);
	}
	
	public static int[] skewedLinearAll(int a, int b, int percentage, int distance) {
		int[] retValue = new int[distance+1];
		for (int x = 0 ; x < distance+1 ; x++)
			retValue[x] = skewedLinear(a, b, percentage, distance, x); 
		return retValue;		
	}

}
