package gr.neuropublic.mutil.base;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



public class HTTPRequest {
	public HTTP_METHOD 					method;
	public String 						path;
	public Map<String, List<String>>	qparams;
	public String						userAgent;
	public String 						host;
	public Integer						port;
	public Integer 						contentLength;
	public boolean 						expect_100_continuePresent;	
	public String						content;
	public boolean						encounteredEndOfHeader;
	public Integer						contentBytesRemaining;
	@Override
	public String toString() {
		return "HTTPRequest [method=" + method + ", path=" + path
				+ ", qparams=" + qparams + ", userAgent=" + userAgent
				+ ", host=" + host + ", port=" + port + ", contentLength="
				+ contentLength + ", expect_100_continuePresent="
				+ expect_100_continuePresent + ", content=" + content
				+ ", encounteredEndOfHeader=" + encounteredEndOfHeader
				+ ", contentBytesRemaining=" + contentBytesRemaining + "]";
	}
	public HTTPRequest(HTTP_METHOD method, String path,
			Map<String, List<String>> qparams, String userAgent, String host,
			Integer port, Integer contentLength,
			boolean expect_100_continuePresent, String content,
			boolean encounteredEndOfHeader, Integer contentBytesRemaining) {
		super();
		this.method = method;
		this.path = path;
		this.qparams = qparams;
		this.userAgent = userAgent;
		this.host = host;
		this.port = port;
		this.contentLength = contentLength;
		this.expect_100_continuePresent = expect_100_continuePresent;
		this.content = content;
		this.encounteredEndOfHeader = encounteredEndOfHeader;
		this.contentBytesRemaining = contentBytesRemaining;
	}
}
