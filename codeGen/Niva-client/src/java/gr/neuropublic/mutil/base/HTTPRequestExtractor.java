package gr.neuropublic.mutil.base;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.util.logging.Logger;

public class HTTPRequestExtractor {
	

	private static final Pattern FULL_PATH_PATTERN      = Pattern.compile("^((POST)?(GET)?(CONNECT)?(HEAD)?)(\\s+)(\\S*)(\\s+)HTTP/.*", Pattern.DOTALL);
	private static final Pattern USER_AGENT_PATTERN  	= Pattern.compile("^User-Agent:\\s*(.*)\\s*?"			, Pattern.DOTALL);
	private static final String  IP_ADDRESS_PATTERN		= "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."+
														  "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])";
	private static final Pattern HOST_PATTERN   		= Pattern.compile("^Host:\\s*(\\S*?)\\s*(:(\\d{1,5}))?\\s*?", Pattern.DOTALL);
	private static final Pattern CONTENT_LENGTH_PATTERN = Pattern.compile("^Content-Length:\\s*(\\S*)\\s*?"		, Pattern.DOTALL);
	private static final Pattern EXPECT_100_CONTINUE     =Pattern.compile("Expect: 100-continue");
	
	private static String extractMethod         (String line)	{return RegExpUtil.extract(FULL_PATH_PATTERN		, line, 1);}
	private static String extractFullPath		(String line)	{return RegExpUtil.extract(FULL_PATH_PATTERN		, line, 7);}
	private static String extractAgent			(String line) 	{return RegExpUtil.extract(USER_AGENT_PATTERN		, line, 1);}
	private static String extractHost   		(String line) 	{return RegExpUtil.extract(HOST_PATTERN			, line, 1);}
	private static String extractPort   		(String line) 	{return RegExpUtil.extract(HOST_PATTERN			, line, 3);}	
	private static String extractContentLength	(String line) 	{return RegExpUtil.extract(CONTENT_LENGTH_PATTERN	, line, 1);}
	private static Boolean extract100Continue   (String line)   {return EXPECT_100_CONTINUE.matcher(line).matches();}
	private static final String NEW_LINE 		= "\\r?\\n";
	private static final byte[] DOUBLE_LINE_BYTE_PATTERN = {'\r','\n', '\r', '\n'};
	
	

	
	public static void main (String args[]) {
		String linetest ="POST /sd HTTP/1.1\r\nUser-Agent: sdf\r\nAccept: dfs\r\nContent-Length: 234\r\naaa\r\n\r\n";
		String hostTest = "Host: www.thetest.com:234";
		String host = extractHost(hostTest);
		String port = extractPort(hostTest);
		System.out.println(host+" port is: "+port);
		if (false) {
			Pattern EMPTY_LINE = Pattern.compile(".*?(\r\n\r\n).*", Pattern.MULTILINE|Pattern.DOTALL); // (^\\s*?$).*");
			Matcher matcher = EMPTY_LINE.matcher(linetest);
			System.out.println(matcher.matches());
			System.out.println("EMPTY LINE MATCH = "+linetest.substring(matcher.end(1)));
			
		
			String lines[] = {
					 "POST /fdsdf HTTP/1.1",
					 "User-Agent: curl/7.24.0 (i686-pc-cygwin) libcurl/7.24.0 OpenSSL/0.9.8t zlib/1.2.5 libidn/1.22 libssh2/1.3.0",
					 "Host: 192.168.0.191:1821",
					 "Accept: */*",
					 "Content-Length: 26",
					 "Content-Type: application/x-www-form-urlencoded",
					 "",
					 "abcd"
			};
			for (String line : lines) {
				String result = StringUtils.join(new String[]{
						extractMethod(line),
						extractFullPath(line),
						extractAgent(line),
						extractHost(line),
						extractPort(line),
						extractContentLength(line)}, " @ ");
				System.out.println("for line: "+line);
				System.out.println("..result:"+result);
			}
		}
		String testLines[] = {linetest, linetest+"a", linetest+"aa"
				, "POST /sd HTTP/1.1\r\nUser-Agent: sdf\r\nAccept: dfs\r\nContent-Length: 0\r\naaa\r\n\r\n"		
				, "POST /sd HTTP/1.1\r\nUser-Agent: sdf\r\nAccept: dfs\r\nContent-Length: 0\r\naaa\r\n\r\nXXX"
				, "POST /sd HTTP/1.1\r\nUser-Agent: sdf\r\nAccept: dfs\r\nContent-Length: 1\r\naaa\r\n\r\nXX"				
				, "GET /sd HTTP/1.1\r\nUser-Agent: sdf\r\nAccept: dfs\r\n"
				, "GET /sd HTTP/1.1\r\nUser-Agent: sdf\r\nAccept: dfs\r\n\r\n"
				, "GET http://www.thegeekstuff.com/2010/03/netstat-command-examples/ HTTP/1.1\r\nHost: www.thegeekstuff.com\r\nProxy-Connection: keep-alive\r\nCache-Control: max-age=0\r\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nReferer: http://www.google.gr/url?sa=t&rct=j&q=netstat%20examples&source=web&cd=1&ved=0CB4QFjAA&url=http%3A%2F%2Fwww.thegeekstuff.com%2F2010%2F03%2Fnetstat-command-examples%2F&ei=lrs7T9ewCKSl4gSjm9WkBg&usg=AFQjCNEZMY0aOvkz9FFxRPVXpoi0RLnn6g\r\nAccept-Encoding: gzip,deflate,sdch\r\nAccept-Language: en-GB,en;q=0.8,en-US;q=0.6,de-AT;q=0.4,de;q=0.2\r\nAccept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3\r\nCookie: __utma=52867053.749965098.1325676687.1328786699.1329314714.4; __utmc=52867053; __utmz=52867053.1329314714.4.4.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=netstat%20examples\r\nIf-Modified-Since: Wed, 15 Feb 2012 13:22:01 GMT\r\n\r\n"
				, "CONNECT plusone.google.com:443 HTTP/1.1\r\nHost: plusone.google.com\r\nProxy-Connection: keep-alive\r\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7\r\n\r\n"
				, "HEAD http://oesemoybzc/ HTTP/1.1\r\nHost: oesemoybzc\r\nProxy-Connection: keep-alive\r\nContent-Length: 0\r\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7\r\nAccept-Encoding: gzip,deflate,sdch\r\n\r\n"
		};

		for (String testLine : testLines)
			System.out.println("total = "+extractHeaderInfo(Logger.getGlobal(),testLine.getBytes()));
	}
	
	private static String[] splitOnNewLinesButDenoteConsecutiveNewLinesWithNull(String in) {
		String [] linesNotQuiteRight = in.split(NEW_LINE, Integer.MAX_VALUE);
		String [] retValue = linesNotQuiteRight;
		if (linesNotQuiteRight[linesNotQuiteRight.length-1].equals(""))
			retValue = (String []) ArrayUtils.subarray(linesNotQuiteRight, 0, linesNotQuiteRight.length-1);
		return retValue;
	}

	private static int positionOfDoubleNewLine(byte bytes[]) {
		int positionOfDoubleNewLine = Util.findFirst(bytes, DOUBLE_LINE_BYTE_PATTERN);
		return positionOfDoubleNewLine;
	}

	public static HTTPRequest extractHeaderInfo(Logger l, ByteBuffer buffer) {
		byte[] contents = ByteBufferUtil.getBytes(buffer);
		return extractHeaderInfo(l, contents);
	}
		
	public static HTTPRequest extractHeaderInfo(Logger l, byte bytes[])
	{
		HTTP_METHOD	method 			= HTTP_METHOD.UNKNOWN;
		String		path			= null;
		Map<String, List<String>>   qparams = null;
		String		userAgent		= null;
		String		host			= null;
		Integer		port			= null;
		Integer 	contentLength	= null;
		Boolean     expect100Continue = false;
		boolean		encounteredEmptyLine = false;
		Integer		bytesOfHeaderContentMissing = null;
		try {
			String incoming = new String(bytes, "US-ASCII"); 
			// String lines[] = incoming.split(NEW_LINE);
			String lines[] = splitOnNewLinesButDenoteConsecutiveNewLinesWithNull(incoming);
			for (String line : lines) {
				if (line.equals(""))
					encounteredEmptyLine = true;
				else {
					String _method    	= extractMethod(line);			if (_method   		!=null) method   		= HTTP_METHOD.valueOf(_method);
					String _fullPath  	= extractFullPath(line);		if (_fullPath 		!=null) {
						Pair<String, Map<String, List<String>>> pathParams = URLUtil.getPathAndUrlParameters(_fullPath);
						path = pathParams.a;
						qparams = pathParams.b;
					}
					String _userAgent 	= extractAgent(line);			if (_userAgent		!=null) userAgent 		= _userAgent;
					String _host		= extractHost(line);			if (_host			!=null) host			= _host;
					String _port		= extractPort(line);			if (_port			!=null) port			= Integer.valueOf(_port);
					String _contentLength= extractContentLength(line);	if (_contentLength 	!=null)	contentLength	= Integer.valueOf(_contentLength);
					expect100Continue = extract100Continue(line);
				}
			}
			if (contentLength != null)
				bytesOfHeaderContentMissing = contentLength - (bytes.length - positionOfDoubleNewLine(bytes)-DOUBLE_LINE_BYTE_PATTERN.length);
			else
				bytesOfHeaderContentMissing = encounteredEmptyLine?0:null;
			switch (method) {
			case GET:
			case CONNECT:
				Util.panicIf(contentLength != null);
				Util.panicIf(expect100Continue!=false);
				return new HTTPRequest(method, path, qparams, userAgent, host, port, contentLength, expect100Continue, null, encounteredEmptyLine, bytesOfHeaderContentMissing);
			case HEAD:
				// in the case of a HEAD header I've collected an example with a ContentLength field
				Util.panicIf(expect100Continue!=false);
				return new HTTPRequest(method, path, qparams, userAgent, host, port, contentLength, expect100Continue, null, encounteredEmptyLine, bytesOfHeaderContentMissing);
			case POST:
				Util.panicIf(contentLength==null);
				String content = null;
				if (!expect100Continue)
					content = incoming.substring(incoming.indexOf("\r\n\r\n")+4);
				Util.panicIf(expect100Continue && (content!=null));
				return new HTTPRequest(method, path, qparams, userAgent, host, port, contentLength, expect100Continue, content, encounteredEmptyLine, bytesOfHeaderContentMissing);
			default:
				throw new IllegalStateException(incoming);
			}
		} catch (UnsupportedEncodingException uee) {
			throw new ExceptionAdapter(uee);
		}
	}
}
