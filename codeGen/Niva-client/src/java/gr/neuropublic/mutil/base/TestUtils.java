package gr.neuropublic.mutil.base;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class TestUtils {
	public static <T> Method getMethod(Class<T> klass, String methodName) {
		final Method methods[] = klass.getDeclaredMethods();
		Method found = null;
		for (Method method : methods) {
			if (method.getName().equals(methodName)) {
				if (found != null)
					fail("method "+methodName+" found twice - the problem is in the test file not the tested class");
				found = method;
			}
		}
		if (found==null)
			throw new RuntimeException("method "+methodName+" not found");
		else {
			found.setAccessible(true);
			return found;
		}
	}
	
	public static <T> String getValueOfStaticStringField(Class<T> klass, String fieldName) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		final Field field = klass.getDeclaredField(fieldName);
		field.setAccessible(true);
		return (String) field.get(null);
	}
}
