package gr.neuropublic.mutil.base;

public interface IStringFilter {
	public boolean accept(String s);
	
}
