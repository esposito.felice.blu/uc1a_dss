package gr.neuropublic.mutil.base;
import java.util.Random;
import static gr.neuropublic.mutil.base.Util.abs;

public class RandomProcession {

    private long start;
    private long end;
    private int numOfTakes;
    private float at;
    private int takesSoFar;
    private Random random;

    public RandomProcession(long start, long end, int numOfTakes) {
        if (end <= start) throw new RuntimeException(String.format("%d<=%d", end, start));
        if (numOfTakes<1) throw new RuntimeException(String.format("numOfTakes = %d which is < 1", numOfTakes));
        this.start      = start;
        this.end        = end;
        this.numOfTakes = numOfTakes;
        this.at         = start;
        this.takesSoFar = 0;
        this.random = new Random(System.currentTimeMillis());
    }
    
    private float averageSpanForRemaining() {
        return ((float) (end-at))/((float) (numOfTakes-takesSoFar));
    }

    private float nextStep() {
        return random.nextFloat()*2*averageSpanForRemaining();
    }

    private long atMost(long atMost, long l) { return l > atMost ? atMost : l ; }

    public long take() {
        if (takesSoFar==numOfTakes) throw new RuntimeException(String.format("Has already been called %d times", takesSoFar));
        at += nextStep();
        takesSoFar++;
        long atL = (long) at;
        return atMost(end-1, atL);
    }
}