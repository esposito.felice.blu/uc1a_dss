package gr.neuropublic.mutil.base;

import java.util.HashMap;
import java.util.Map;

import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.FileSystem;
import org.hyperic.sigar.FileSystemUsage;
import org.hyperic.sigar.NetInterfaceConfig;
import org.hyperic.sigar.NetInterfaceStat;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;


public class SigarUtil {
	private static Sigar sigar = new Sigar();
	public static double cpuPercToOne() {
		CpuPerc cpuPerc;
		try {
			cpuPerc = sigar.getCpuPerc();
		} catch (SigarException e) {
			return 50;
		}
		return cpuPerc.getSys()+cpuPerc.getUser();
	}
	public static double memPercToOne() {
		try {
			return (100 - sigar.getMem().getFreePercent())/100.0;
		} catch (SigarException e) {
			return 20;
		}
	}
	
	private static long lastRxBytes = rxBytesFromAllNICs(); // not thread-safe
	public static double rxBytesPercToOne() {
		long newValue = rxBytesFromAllNICs();
		long diff = newValue - lastRxBytes;
		lastRxBytes = newValue;
		return percOfSpeed(diff);
	}

	private static long lastTxBytes = txBytesFromAllNICs(); // not thread-safe
	public static double txBytesPercToOne() {
		long newValue = txBytesFromAllNICs();
		long diff = newValue - lastTxBytes;
		lastTxBytes = newValue;
		return percOfSpeed(diff);
	}
	
	private static double percOfSpeed (long value) {
		return (double) value / speedOfAllNICs();
	}
	
	public static long speedOfAllNICs() {
		try {
			Map<String, Long> macToSpeed = new HashMap<>();
			String nics[] = sigar.getNetInterfaceList();
			for (String nic : nics) {
				NetInterfaceConfig config = sigar.getNetInterfaceConfig(nic);
				if (!(config.getType().equals("Local Loopback"))) {
					String mac = config.getHwaddr();
					NetInterfaceStat stat = sigar.getNetInterfaceStat(nic);
					long speed = stat.getSpeed();
					macToSpeed.put(mac, speed);
				}
			}
			//System.out.println(JsonProvider.toJson(macToSpeed));
			return MapUtil.sumValues(macToSpeed) / 2; // full-duplex
		} catch (SigarException e) {
			return -1;
		}
	}
	
	public static long rxBytesFromAllNICs() {
		try {
			Map<String, Long> macToRx = new HashMap<>();
			String nics[] = sigar.getNetInterfaceList();
			for (String nic : nics) {
				NetInterfaceConfig config = sigar.getNetInterfaceConfig(nic);
				String mac = config.getHwaddr();
				NetInterfaceStat stat = sigar.getNetInterfaceStat(nic);
				long bytesRx = stat.getRxBytes();
				macToRx.put(mac, bytesRx);
			}
			return MapUtil.sumValues(macToRx);
		} catch (SigarException e) {
			return -1;
		}
	}

	public static long txBytesFromAllNICs() {
		try {
			Map<String, Long> macToTx = new HashMap<>();
			String nics[] = sigar.getNetInterfaceList();
			for (String nic : nics) {
				NetInterfaceConfig config = sigar.getNetInterfaceConfig(nic);
				String mac = config.getHwaddr();
				NetInterfaceStat stat = sigar.getNetInterfaceStat(nic);
				long bytesTx = stat.getTxBytes();
				macToTx.put(mac, bytesTx);
			}
			return MapUtil.sumValues(macToTx);
		} catch (SigarException e) {
			return -1;
		}
	}
	
	public static double maxDiskPercent() {
		try {
			Map<String, Double> diskUsages = new HashMap<String, Double>();
			FileSystem systemInfos[] = sigar.getFileSystemList();
			for (FileSystem systemInfo : systemInfos) {
				try {
					FileSystemUsage fsUsage = sigar.getFileSystemUsage(systemInfo.getDevName());
					System.out.println(systemInfo+ " : " +fsUsage);
					diskUsages.put(systemInfo.getDevName(), fsUsage.getUsePercent());
				} catch (Exception e) {}
			}
			return MapUtil.maxValue(diskUsages);
		} catch (Exception e) {return -1;}
	}
		
	public static void main (String ...args) throws InterruptedException {
		long initial = rxBytesFromAllNICs();
		while (true) {
			Thread.sleep (1000);
			// System.out.format ("%8.4f %8.4f %n", cpuPercToOne(), memPercToOne());
			System.out.format("(diskperone, nu, cumul, speed, recvd, trsmt) = (%10.8f %10.8f %10d %10d %10d %10d) %n", maxDiskPercent(), rxBytesPercToOne(), rxBytesFromAllNICs()-initial, speedOfAllNICs(), rxBytesFromAllNICs(), txBytesFromAllNICs());
		}
	}
}
