package gr.neuropublic.mutil.base;

import java.util.Arrays;

public class ArrayMonotonicityUtils {
	
	// the terms STRICTLY_INCREASING and STRICTLY_DECREASING have the normal mathematical meaning.
	// the terms OCCASIONALY_INCREASING and OCCASIONALY_DECREASING do not have a mathematical counter-part and should not be confused with
	// the mathematical terms "weakly incresing / decreasing". Also, the term "NONE" means none of these values. However a matrix
	// whose monotonicity is pronounced to be Array2Utils.NONE may be weakly incresing or weakly decreasing in the mathematical sense.
	// note finally, that in the mathematical sense, a function can be both weakly increasing and weakly decreasing. Not so, in this
	// enumeration. A matrix cannot be both ArrayMonotonicityUtils.OCCASIONALY_DECREASING and ArrayMonotonicityUtils.OCCASIONALY_INCREASING.
	public enum Monotonicity {NONE, STRICTLY_DECREASING, OCCASIONALY_DECREASING, OCCASIONALY_INCREASING, STRICTLY_INCREASING};
	
	public static Monotonicity pronounceMonotonicity(int matrix[]) {
		boolean strictlyIncreasing 	    = true;
		Boolean occasionalyIncreasing 	= null;
		boolean strictlyDecreasing		= true;
		Boolean occasionalyDecreasing	= null;
		for (int i = 0 ; i < matrix.length ; i++) {
			if ( (i>0) && (matrix[i] >  matrix[i-1] )) {strictlyDecreasing = false; occasionalyDecreasing = false; occasionalyIncreasing = (occasionalyIncreasing==null?true:occasionalyIncreasing);}
			if ( (i>0) && (matrix[i] == matrix[i-1] )) {strictlyDecreasing = false; strictlyIncreasing    = false; }
			if ( (i>0) && (matrix[i] <  matrix[i-1] )) {strictlyIncreasing = false; occasionalyIncreasing = false; occasionalyDecreasing = (occasionalyDecreasing==null?true:occasionalyDecreasing);}
		}
		if (strictlyDecreasing) 		return Monotonicity.STRICTLY_DECREASING;
		else if (occasionalyDecreasing)	return Monotonicity.OCCASIONALY_DECREASING;
		else if (strictlyIncreasing)	return Monotonicity.STRICTLY_INCREASING;
		else if (occasionalyIncreasing) return Monotonicity.OCCASIONALY_INCREASING;
		else							return Monotonicity.NONE;
	}
	
	public static Pair<Monotonicity, Monotonicity> pronounceMonotonicity(int matrix[][]) {
		boolean iStrictlyIncreasing 	= true;
		Boolean iOccasionalyIncreasing 	= null;
		boolean iStrictlyDecreasing		= true;
		Boolean iOccasionalyDecreasing	= null;

		boolean jStrictlyIncreasing 	= true;
		Boolean jOccasionalyIncreasing 	= null;
		boolean jStrictlyDecreasing 	= true;
		Boolean jOccasionalyDecreasing 	= null;

		for (int i = 0 ; i < matrix.length ; i++) {
			for (int j = 0; j < matrix[i].length ; j++) {
				if ((i>0) && (matrix[i][j] > matrix[i-1][j  ])) {iStrictlyDecreasing = false; iOccasionalyDecreasing 	= false; iOccasionalyIncreasing = (iOccasionalyIncreasing==null?true:iOccasionalyIncreasing);}
				if ((i>0) && (matrix[i][j]== matrix[i-1][j  ])) {iStrictlyDecreasing = false; iStrictlyIncreasing 		= false; 																					 }
				if ((i>0) && (matrix[i][j] < matrix[i-1][j  ])) {iStrictlyIncreasing = false; iOccasionalyIncreasing 	= false; iOccasionalyDecreasing = (iOccasionalyDecreasing==null?true:iOccasionalyDecreasing);}
				
				if ((j>0) && (matrix[i][j] > matrix[i  ][j-1])) {jStrictlyDecreasing = false; jOccasionalyDecreasing 	= false; jOccasionalyIncreasing = (jOccasionalyIncreasing==null?true:jOccasionalyIncreasing);}
				if ((j>0) && (matrix[i][j]== matrix[i  ][j-1])) {jStrictlyDecreasing = false; jStrictlyIncreasing 		= false; 																				     }
				if ((j>0) && (matrix[i][j] < matrix[i  ][j-1])) {jStrictlyIncreasing = false; jOccasionalyIncreasing 	= false; jOccasionalyDecreasing = (jOccasionalyDecreasing==null?true:jOccasionalyDecreasing);}
			}
		}
		Monotonicity iMonotonicity = Monotonicity.NONE;
		Monotonicity jMonotonicity = Monotonicity.NONE;
		if 		 (iStrictlyIncreasing) 											iMonotonicity = Monotonicity.STRICTLY_INCREASING;
		else if ((iOccasionalyIncreasing!=null) && (iOccasionalyIncreasing))	iMonotonicity = Monotonicity.OCCASIONALY_INCREASING;
		else if (iStrictlyDecreasing)											iMonotonicity = Monotonicity.STRICTLY_DECREASING;
		else if ((iOccasionalyDecreasing!=null) && (iOccasionalyDecreasing))	iMonotonicity = Monotonicity.OCCASIONALY_DECREASING;

		if 		(jStrictlyIncreasing) 											jMonotonicity = Monotonicity.STRICTLY_INCREASING;
		else if ((jOccasionalyIncreasing!=null) && (jOccasionalyIncreasing))	jMonotonicity = Monotonicity.OCCASIONALY_INCREASING;
		else if (jStrictlyDecreasing)											jMonotonicity = Monotonicity.STRICTLY_DECREASING;
		else if ((jOccasionalyDecreasing!=null) && (jOccasionalyDecreasing))	jMonotonicity = Monotonicity.OCCASIONALY_DECREASING;
		
		return Pair.create(iMonotonicity, jMonotonicity);
	}
}


