package gr.neuropublic.mutil.base;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



public class HTTPResponseOld {
	public Integer 						contentLength;
	public boolean						encounteredEndOfHeader;
	public Integer						contentBytesRemaining;
	@Override
	public String toString() {
		return "HTTPResponse [contentLength=" + contentLength
				+ ", encounteredEndOfHeader=" + encounteredEndOfHeader
				+ ", contentBytesRemaining=" + contentBytesRemaining + "]";
	}
	public HTTPResponseOld(Integer contentLength, boolean encounteredEndOfHeader,
			Integer contentBytesRemaining) {
		super();
		this.contentLength = contentLength;
		this.encounteredEndOfHeader = encounteredEndOfHeader;
		this.contentBytesRemaining = contentBytesRemaining;
	}	

	
}
