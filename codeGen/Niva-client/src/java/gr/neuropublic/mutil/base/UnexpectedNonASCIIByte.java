package gr.neuropublic.mutil.base;

import org.apache.commons.codec.binary.Hex;

public class UnexpectedNonASCIIByte extends Exception {

	private static final long serialVersionUID = 1L;
	
	private byte offendingByte;
	public UnexpectedNonASCIIByte(byte offendingByte) {
		this.offendingByte = offendingByte;
	}
	
	public static String exceptionMessage(byte offendingByte) {
		return "non-ASCII byte encountered, value = "+offendingByte +", hex = "+String.valueOf(Hex.encodeHex(new byte[]{offendingByte}));
	}
	
	public String toString() {
		return exceptionMessage(offendingByte);
	}

}
