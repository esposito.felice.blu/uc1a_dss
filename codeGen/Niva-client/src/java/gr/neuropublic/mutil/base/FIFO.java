package gr.neuropublic.mutil.base;

import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class FIFO<T> implements Iterable<T>{

	LinkedBlockingDeque<T> queue;
	
	public FIFO(int size) {
		queue = new LinkedBlockingDeque<>(size);
	}
	
	public void add(T t) {
		if (queue.remainingCapacity()==0)
			queue.poll();
		queue.add(t);
	}
	
	public Iterator<T> descendingIterator() {
		return queue.descendingIterator();
	}
	
	
	@Override
	public Iterator<T> iterator() {
		return queue.iterator();
	}
	
	public static void main(String args[]) {
		{
			FIFO<String> oneString = new FIFO<>(1);
			oneString.add("alpha");
			Iterator<String> strings = oneString.iterator();
			while (strings.hasNext())
				System.out.println(strings.next());
			System.out.println("****");
			oneString.add("beta");
			strings = oneString.iterator();
			while (strings.hasNext())
				System.out.println(strings.next());
			System.out.println("****");
		}
		{
			FIFO<String> twoStrings = new FIFO<>(2);
			twoStrings.add("alpha");
			Iterator<String> strings = twoStrings.iterator();
			while (strings.hasNext())
				System.out.println(strings.next());
			System.out.println("****");
			twoStrings.add("beta");
			strings = twoStrings.iterator();
			while (strings.hasNext())
				System.out.println(strings.next());
			System.out.println("****");
			twoStrings.add("gamma");
			strings = twoStrings.iterator();
			while (strings.hasNext())
				System.out.println(strings.next());
			System.out.println("****");

		}		
		

	}
}
