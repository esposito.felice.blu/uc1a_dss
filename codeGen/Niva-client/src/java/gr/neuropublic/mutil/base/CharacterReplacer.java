package gr.neuropublic.mutil.base;

public class CharacterReplacer implements IStringModifier {
	
	char a;
	char b;
	
	public CharacterReplacer(char a, char b) {
		this.a = a ;
		this.b = b;
	}

	@Override
	public String modify(String aString) {
		return aString.replace(a, b);
	}
	
	public static IStringModifier getTabReplacer() {
		return new CharacterReplacer('\t', ' ');
	}
	
	public static void main(String args[]) {
		System.out.println( CharacterReplacer.getTabReplacer().modify("\t\ta\tb\t\t\tc"));
	}

}
