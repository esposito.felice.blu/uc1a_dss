package gr.neuropublic.mutil.base;

import static gr.neuropublic.mutil.base.BlankRemover.*;

public class WhiteSpaceTrimmer implements IStringModifier {

	@Override
	public String modify(String aString) {
		return trim(aString);
	}

	public static void main(String args[]) {
		System.out.println((new WhiteSpaceTrimmer()).modify("    a  a    aa                       b "));
	}
}
