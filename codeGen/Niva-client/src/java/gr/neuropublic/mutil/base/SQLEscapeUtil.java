package gr.neuropublic.mutil.base;
import java.io.IOException;
import java.io.File;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import java.util.List; import java.util.Map; import java.util.HashMap;
import java.util.ArrayList;

public class SQLEscapeUtil {

    private static String RARE_STRING="use a rare string, @#$645k";

    private static String replacePerc(String s) {
        return s.replace("%", "n"+RARE_STRING);
    }

    public static boolean unescapedPercentageExists(String s) {
        return StringEscapeUtils.unescapeJava(replacePerc(s)).indexOf("n"+RARE_STRING)!=-1;
    }

    public static String sqlOPEQ(Object o) {
        String rv = null;
        if (o==null)
            rv = "IS NULL";
        else
            rv = "= ?";
        //System.out.println("sqlOPEQ returning: "+rv);
        return rv;
    }

    public static String sqlOP(String s) {
        String rv = null;
        if (s==null)
            rv = "IS NULL";
        else
            rv = unescapedPercentageExists(s)?"LIKE ?":"= ?";
        // System.out.println("sqlOP returning: "+rv);
        return rv;
    }

    private static String sqlOPForPrintout(String s) {
        String rv = null;
        if (s==null)
            rv = "IS";
        else
            rv = unescapedPercentageExists(s)?"LIKE":"=";
        // System.out.println("sqlOPForPrintout returning: "+rv);
        return rv;
    }

    public static String sqlESC(String s) {
        if (s==null) {
            // System.out.println("sqlESC: null to null");
            return s;
        }
        else {
            String rv = StringEscapeUtils.escapeJava(s).replace("%", "\\%");
            // System.out.println("sqlESC: "+s +" to: " +rv);
            return rv;
        }
    }

    public static String sqlUNESC(String s) {
        if (s==null) return s;
        String s1 = StringEscapeUtils.unescapeJava(s);
        //old buggy implementation:
        //return s1.replace(RARE_STRING, "%").replace("\n", "");
        String rv = s1.replace("\n"+RARE_STRING, "%");
        // System.out.println("sqlUNESC:\n"+s+"\nto:\n"+rv);
        return rv;
    }

    private static String escapeBackSlashes(String s) {
        return s.replace("\\", "\\\\");
    }

    // TODO: this is wild shot, if it works I'll have to document it
    public static String sqlUNESC2(String s) {
        String rv = null;
        if (s==null) rv = null;
        else if (unescapedPercentageExists(s))
            rv = escapeBackSlashes(sqlUNESC(s));
        else
            rv = sqlUNESC(s);
        // System.out.println("sqlUNESC2 changed:\n"+s+"\ninto:\n"+rv);
        return rv;
    }

    public static List<String> whereClauses(Map<String, Object> columnValuesMap) {
        List<String> rv = new ArrayList<>();
        if (columnValuesMap.size()==0)
            rv.add("1=1");
        else
        for (String name : columnValuesMap.keySet()) {
            Object value = columnValuesMap.get(name);
            if (value==null)
                rv.add(String.format("%s IS NULL", name));
            else if (! (value instanceof String))
                rv.add(String.format("%s = ?", name));
            else if (unescapedPercentageExists ( (String) value))
                rv.add(String.format("%s LIKE ?", name));
            else
                rv.add(String.format("%s = ?", name));
        }
        return rv;
    }


    public static void main(String args[]) throws IOException {
        boolean tests[] = {false, true};
        if (tests[0]) {
            String [] testStrings = new String[]{"a", "a\\nb", "a\\b"};
            for (String testString : testStrings)
                System.out.println(String.format("%s --> %s", testString, escapeBackSlashes(testString)));
    
            List<String> testStrs = FileUtil.readFileAsListOfStrings(new File("./data"));
            // now add a multi-line Greek String
            testStrs.add("Πιστεύω\\nεις\\nέναν\\nΘεό");
            testStrs.add("Πιστεύω\nεις\nέναν\nΘεό");
            testStrs.add("Πιστεύω\nεις\nέναν%\nΘεό");
            testStrs.add("Πιστεύω\nεις\nέναν%%\nΘεό");
            testStrs.add("Πιστεύω\nεις\nέναν\\%%\nΘεό");
            testStrs.add("Πιστεύω\nεις\nέναν\\\\%%\nΘεό");
            testStrs.add("Πιστεύω\nεις\nέναν\\\\%\\%\nΘεό");
            System.out.println("PART I - showing if unescaped percentage exists and how it should be interpeted");
            for (String testStr : testStrs) {
                String testStrUnescaped = sqlUNESC(testStr);
                System.out.println(String.format("%10s --> should be interpreted as %7s '%s'", testStr, sqlOPForPrintout(testStr), testStrUnescaped));
            }
            System.out.println("PART II - showing how values meant to be taken literally should be escaped");
            for (String testStr : testStrs) {
                System.out.println(String.format("%s -> %s", testStr, sqlESC(testStr)));
            }
            System.out.println("PART III - putting it all together (should all be interpreted as '=')");
            for (String testStr : testStrs) {
                String escaped = sqlESC(testStr);
                String interpretedAs = sqlOPForPrintout(escaped);
                String valueForEquals = sqlUNESC(escaped);
                System.out.println(String.format("%10s --> %20s ==> %10s '%s'", testStr, escaped, interpretedAs, valueForEquals));
                if (!valueForEquals.equals(testStr)) throw new RuntimeException(testStr);
            }
        }
        if (tests[1]) {
            Map<String, Object> columnValues = new HashMap<>();
            columnValues.put("name", "Menelaos");
            columnValues.put("surname", "Perd%eas");
            columnValues.put("job_description", null);
            columnValues.put("age", 35);
            columnValues.put("address", "Serron\\%7");
            List<String> whereClauses = whereClauses(columnValues);
            System.out.println("whereClauses below:\n");
            System.out.println("1st expression: "+StringUtils.join(whereClauses, " AND "));
            System.out.println("2nd expression: "+StringUtils.join(SQLEscapeUtil.whereClauses(new HashMap<String, Object>()), " AND "));
        }
    }
}