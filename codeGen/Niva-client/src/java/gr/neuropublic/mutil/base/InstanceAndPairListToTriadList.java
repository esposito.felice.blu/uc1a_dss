package gr.neuropublic.mutil.base;

import java.util.ArrayList;
import java.util.List;

public class InstanceAndPairListToTriadList<K, V, T> {
	
	public List<Triad<K, V, T>> merge (List<K> listOfK, List<Pair<V, T>> listOfPairVT) {
		// I know I don't check for null - let it break - I'll fix it then since it
		// might reveal a deeper problem in the calling context
		// but I will check for size :
		List<Triad<K, V, T>> retValue = new ArrayList<Triad<K, V, T>>();
		int i,j;
		if ((i=listOfK.size()) != (j=listOfPairVT.size())) throw new RuntimeException(i+" != "+j);
		for (int _i = 0 ; _i < i ; _i++)
			retValue.add(new Triad<K, V, T>(
							listOfK.get(_i),
							listOfPairVT.get(_i).a,
							listOfPairVT.get(_i).b));
		return retValue;
	}
}
