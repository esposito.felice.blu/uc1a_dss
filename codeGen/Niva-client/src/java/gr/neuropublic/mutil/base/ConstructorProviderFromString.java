package gr.neuropublic.mutil.base;

import java.lang.reflect.Constructor;


public class ConstructorProviderFromString<T> implements IProviderFromString<T>
{

	private Constructor<T> 	konstructor		= null;
	
	public ConstructorProviderFromString (Class<T> klass) {
		try {
			this.konstructor = klass.getConstructor(new Class[] {String.class});
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
	}

	@Override
	public T fromString(String s) {
		try {
			return konstructor.newInstance((Object[]) new String[]{s});
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
	}

}
