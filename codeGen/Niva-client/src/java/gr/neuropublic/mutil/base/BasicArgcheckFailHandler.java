// Copyright (c) 2001 Hursh Jain (http://www.mollypages.org) 
// The Molly framework is freely distributable under the terms of an
// MIT-style license. For details, see the molly pages web site at:
// http://www.mollypages.org/. Use, modify, have fun !

package gr.neuropublic.mutil.base;

import java.util.*;

/**
Handles argument {@link Checker} errors by printing the thread, time and stack trace
to <tt>System.err</tt>. <tt>System.err</tt> is chosen because the
default assertion mechanism also writes a message to that location.
After writing the message to <tt>System.err</tt>, a new
<tt>IllegalArgumentException</tt> is thrown.

@author		hursh jain
@version	1.0
@date		5/30/2002
**/
public class BasicArgcheckFailHandler implements IArgcheckFailHandler
{
long id = 0;
	
public void handle(String msg) {
		long myid = 0;
		synchronized (this){
			myid = id++;
			}
		String argcheckmsg = "Argcheck error [#" + myid + "] " + ((msg!=null)?msg:"");	
		System.err.println(argcheckmsg + ": " + new Date() + " " + Thread.currentThread());
		new Exception("=== stack trace, " + argcheckmsg + "===").printStackTrace(System.err);
		throw new IllegalArgumentException(argcheckmsg);
		}
}
