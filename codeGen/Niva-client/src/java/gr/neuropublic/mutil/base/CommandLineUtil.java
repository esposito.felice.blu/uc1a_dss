package gr.neuropublic.mutil.base;

import java.util.*;
import java.io.*;

import org.apache.commons.lang.StringUtils;

import java.util.logging.Logger;

public class CommandLineUtil {
	public static ExecFuture exec(Logger l, String[] cmd) throws IOException, InterruptedException {
		return exec(l, cmd, false);
	}
	public static ExecFuture exec(Logger l, String[] cmd, boolean outputToNull) throws IOException, InterruptedException {
	        Runtime rt = Runtime.getRuntime();
	        l.finest("about to execute: (see next line)\n"+StringUtils.join(cmd, " "));
	        Process proc = rt.exec(cmd);
	        // any error message?
	        StreamGobbler errorGobbler = new 
	            StreamGobbler(outputToNull?null:l, proc.getErrorStream(), "ERROR");            
	        
	        // any output?
	        StreamGobbler outputGobbler = new 
	            StreamGobbler(outputToNull?null:l, proc.getInputStream(), "OUTPUT");
	            
	        // kick them off
	        errorGobbler.start();
	        outputGobbler.start();
	        return new ExecFuture(proc);
	}
}
