package gr.neuropublic.mutil.base;

public interface ICopyStreamTracker {
	
	public void track(long numOfBytesCopied);

}
