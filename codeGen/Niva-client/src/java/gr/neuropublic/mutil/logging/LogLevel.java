// Copyright (c) 2001 Hursh Jain (http://www.mollypages.org) 
// The Molly framework is freely distributable under the terms of an
// MIT-style license. For details, see the molly pages web site at:
// http://www.mollypages.org/. Use, modify, have fun !

package gr.neuropublic.mutil.logging;

import java.util.*;

public class LogLevel 
{ 
public String 	desc;
public int 		intval;

protected LogLevel(String desc, int intval) 
	{
	this.desc = desc;
	this.intval = intval;
	}
		
//remember, the static enums are instances too, so this method is possible.
public int compareTo(Object obj) 
	{
	if ( (obj == null) || (!(obj instanceof LogLevel)) )  
		return -1; 
		
	else return intval - ((LogLevel)obj).intval;
	}

public String toString() 
	{
	return "LogLevel [" + intval + "," + desc + "]";
	}
			
} //~LogLevel
	

