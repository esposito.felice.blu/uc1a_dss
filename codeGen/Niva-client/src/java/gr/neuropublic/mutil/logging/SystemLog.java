// Copyright (c) 2001 Hursh Jain (http://www.mollypages.org) 
// The Molly framework is freely distributable under the terms of an
// MIT-style license. For details, see the molly pages web site at:
// http://www.mollypages.org/. Use, modify, have fun !

package gr.neuropublic.mutil.logging;

import java.io.*;
import java.util.*;


import gr.neuropublic.mutil.*;

/**
A default implementation of {@link Log} that provides logging to a
PrintStream destination and is intended for sending normal application
logs to <code>System.{out, error}</code>
<p>
Messages written to output destinations depend upon the flushing policy of
the output destination. Destinations that use <tt>System.{err,out}</tt>
will flush messages after every log method invocation.
<p>
This class should be sufficient for all logging tasks. However custom
loggers that subclass {@link Log} can also be written if really desired.

@author hursh jain
*/
public class SystemLog extends Log
{
final PrintStream out;

//or level names
private final static String level_name_sep = " \t";

/** 
Creates a new SystemLog with a default destination of <tt>System.err</tt>
and a default level of {@link Log#DEFAULT_LEVEL}

@param	name		name of this log (any arbitrary string)
*/
public SystemLog(String name)
	{
	this(name, System.err, DEFAULT_LEVEL);
	}

/** 
Creates a new SystemLog with the specified destination and a 
default level of {@link Log#DEFAULT_LEVEL}

@param	name		name of this log (any arbitrary string)
@param	out			the output destination
*/
public SystemLog(String name, PrintStream out)
	{
	this(name, out, DEFAULT_LEVEL);
	}

/** 
Creates a new SystemLog. The newly created log will automatically add
itself to the list of all logs maintained by {@link Log} and a subsequent
call to {@link Log#get(String)} with the same name will return this log.

@param	name		name of this log (any arbitrary string)
@param	out			the output destination
@param	loglevel	the logging level
*/
public SystemLog(String name, PrintStream out, LogLevel loglevel)
	{
	super(name, loglevel);	
	assert out != null;
	this.out = out;
	addLog(this);
	}

/**
Closes the log by flushing the destination PrintStream.
<code>close()</code> is <b>not</b> called on the PrintStream
since it would make PrintStreams like <code>System.out</code> 
unusable by other code.
*/
public void close()
	{
	out.flush();	
	}

public void log(LogLevel level, Object str1) 
	{	
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		
			
		out.println(str1);
		}
	}
		
		
public void log(LogLevel level, Object str1, Object str2) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.println(str2);
		}
	}
	
public void log(LogLevel level, Object str1, Object str2, Object str3) 
	{	
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.println(str3);
		}
	}
		
public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.println(str4);	
		}
	}

public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4,
						 Object str5) 
	{	
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.print(str4);	
		out.println(str5);	
		}
	}

public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4,
						 Object str5, Object str6) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.print(str4);	
		out.print(str5);	
		out.println(str6);	
		}
	}

public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4,
						 Object str5, Object str6,
						 Object str7) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.print(str4);	
		out.print(str5);	
		out.print(str6);	
		out.println(str7);	
		}
	}

public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4,
						 Object str5, Object str6,
						 Object str7, Object str8) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.print(str4);	
		out.print(str5);	
		out.print(str6);	
		out.print(str7);	
		out.println(str8);	
		}
	}

public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4,
						 Object str5, Object str6,
						 Object str7, Object str8, 
						 Object... args) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.print(str4);	
		out.print(str5);	
		out.print(str6);	
		out.print(str7);	
		out.print(str8);
		
		final int len = args.length;
		for (int i = 0; i < len; i++) {
			out.print(args[i]);
			}
		
		out.println();
		}
	}


public static void main(String args[]) throws Exception
	{
	SystemLog l = new SystemLog("stdout2");
	l.printTimestamp(true);
	l.printRelativeTimestamp(true);
	System.out.println("printing using new log with timestamps=true");
	l.log(INFO, "abc");
	l.bug("some debug message -- should not print");	
	l.error("error message");
	l.warn("warn message ", "hah ", "34324 ", "foo ", "bar ", "baz");
	l.info("info message: ", "hello", "world");
	l.setLevel(DEBUG);
	l.bug("some debug message -- should print");	
	
	l.printRelativeTimestamp(false);
	System.out.println("sleeping for 2 seconds.....");
	Thread.currentThread().sleep(2000);
	System.out.println("setting level to 'inFO'");
	l.setLevel("inFO");
	System.out.println("level set to: " + l.currentLevel);
	
	l.logSystemInfo();
	System.out.println("setting level to 'inFOO'");
	l.setLevel("inFOO");
	System.out.println("level set to: " + l.currentLevel);

	SystemLog stdout = Log.getDefault();
	System.out.println("printing using Default Log");
	stdout.log(SystemLog.WARN, "some warning");
	
	SystemLog l2 = new SystemLog("stdout3");
	Log.setLevelForAll("std", Log.ERROR);
	System.out.println("Setting all logs to level: error");
	System.out.println("l1=" + l);
	System.out.println("l2=" + l2);
	}
}