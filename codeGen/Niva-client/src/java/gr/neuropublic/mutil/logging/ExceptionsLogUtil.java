package gr.neuropublic.mutil.logging;

import gr.neuropublic.mutil.base.ExceptionAdapter;

import com.google.gson.JsonParseException;

public class ExceptionsLogUtil {
	// this class provides a utility method to log exceptions that may have been adapted
	public static void log(String context, Log l, Exception e) {
		l.error(" /---  "+context+" exception start ---\\");
		l.error("exception runtime type:"+e.getClass().getName());
		l.error(e.getMessage());
		for (StackTraceElement stackTraceElement : e.getStackTrace())
			l.error(stackTraceElement);
		// STEP 1 - generic throwable cause drilling
		{
			l.error("cause found below:");
			l.error(" -  -  -  start of generic cause -  -  -  ");
			Throwable t = e.getCause();
			if (t!=null)
				for (StackTraceElement stackTraceElement : t.getStackTrace())
					l.error(stackTraceElement);
			l.error(" -  -  -  end of generic cause -  -  -  ");
		}
		  // STEP 2 - hack-based throwable-cause drilling (maybe redundant)
		{ // start of various hacks for more descriptive output
			if (e.getClass().equals(ExceptionAdapter.class)) {
				l.error("Exception has been adapted by class "+ExceptionsLogUtil.class.getName()+". Original stack trace is found below:");
				l.error( ((ExceptionAdapter) e).getOriginalStackTrace());
			}
			else if (e.getClass().equals(JsonParseException.class)) {
				l.error("Exception has been adapted by class "+JsonParseException.class.getName()+". Original stack trace is found below:");
				l.error( ((JsonParseException) e).getCause());
			}
		} // end of various hacks for more descriptive output
		l.error(" \\---  "+context+" exception end ---/");
		

		
	}

}
