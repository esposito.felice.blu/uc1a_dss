package gr.neuropublic.mutil.logging;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import gr.neuropublic.mutil.base.ExceptionAdapter;
import gr.neuropublic.mutil.base.Holder;
import gr.neuropublic.mutil.base.Util;


public class FileLogDispatcher implements IFileLogDispatcherCompressorSynch{

	private final File directory;
	private final String namePrefix;
	private final String nameSuffix;
	private final String nameExtension;
	private final boolean echoToStandardOut;
	private final FileLogCompressor logCompressor;
	private final Integer EVERY;
	private int i;

	
	private DateFormat dateFormat;
	private boolean compressorRunning = false;
	public void setCompressorRunning(boolean value) {
		synchronized (this) {
			this.compressorRunning = value;			
		}
	}
	public boolean  getCompressorRunning() {
		return this.compressorRunning;
	}
	
	public String toString() {
		return prefix()+":"+suffix()+":"+nameExtension+"/"+previousFileName;
	}
	
	public FileLogDispatcher(File directory, String namePrefix, String nameSuffix, String nameExtension, boolean echoToStandardOut, Integer everyThatManyOperationsCompress) {
		Util.throwRuntimeExceptionIfAnyIsNull("name components shouldn't be null; use empty string instead", namePrefix, nameSuffix, nameExtension);
		this.directory 			= directory;
		this.namePrefix 		= namePrefix;
		this.nameSuffix 		= nameSuffix;
		this.nameExtension 		= nameExtension;
		this.echoToStandardOut 	= echoToStandardOut;
		this.dateFormat 		= new SimpleDateFormat("yyyy.MM.dd (EEE) HH");
		this.logCompressor      = new FileLogCompressor(this, directory, prefix());
		this.EVERY				= everyThatManyOperationsCompress;
		this.i                  = 0;
	}
	
	private static final String EMPTY_STRING = "";
	private static final String NAME_COMPONENTS_SEPARATOR = "_";
	
	private String prefix() {
		if (namePrefix.equals(EMPTY_STRING)) return namePrefix;
		else return namePrefix+NAME_COMPONENTS_SEPARATOR;
	}
	
	private String suffix() {
		if (nameSuffix.equals(EMPTY_STRING)) return nameSuffix;
		else return nameSuffix+NAME_COMPONENTS_SEPARATOR;
	}
	
	private String extension() {
		if (nameExtension.equals(EMPTY_STRING)) return nameExtension;
		else return "." + nameExtension;
	}

	
	private String generateFileName() {
		return prefix()+dateFormat.format(new Date(System.currentTimeMillis()))+suffix()+extension(); 
	}
	
	
	private String 				previousFileName 	= null;
	private PrintStream 		previousPrintStream = null;
	

	public void runCompressorSynchronously() {
		if (this.EVERY != null) throw new RuntimeException("wasn't conceived with this scenario in mind. The conception was to have either synchronous invocations or %EVERY invocations in their own thread, but not both");
		this.logCompressor.run();
	}
	
	private PrintStream getPrintStream() {
		try {
			if (EVERY!=null) {
				synchronized (this) {
					if ((i++%EVERY==0) && (!getCompressorRunning())) {
						(new Thread(logCompressor)).start();
					}
				}
			}
			String fileName = generateFileName();
			if (fileName.equals(previousFileName))
				return previousPrintStream;
			else {
				PrintStream ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(directory, fileName), true)));
				previousFileName = fileName;
				if (previousPrintStream!=null)
					previousPrintStream.close();
				previousPrintStream = ps;
				return ps;
			}
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
		
	}
	
	private static String escapeObjects(Object msg) {
		if (msg==null) return "(null)";
		else return msg.toString();
	}
	public void print(Object msg) {
		print(escapeObjects(msg));
	}
	
	public void print(String msg) {
		try {
			PrintStream ps = getPrintStream();
			ps.print(msg);
			if (ps.checkError()) throw new Exception(toString()+"/print");
			if (echoToStandardOut) System.out.print(msg);
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
	}
	
	public void println(Object msg) {
		println(escapeObjects(msg));
	}
	
	public void println(String msg) {
		try {
			PrintStream ps = getPrintStream();
			ps.println(msg);
			if (ps.checkError()) throw new Exception(toString()+"/println");
			if (echoToStandardOut) System.out.println(msg);
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
	}
	
	public void println() {
		try {
			PrintStream ps = getPrintStream();
			ps.println();
			if (ps.checkError()) throw new Exception(toString()+"/println(void)");
			if (echoToStandardOut) System.out.println();
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}		
	}
	
	public void close() {
		if (previousPrintStream!=null) {
			previousPrintStream.flush();
//			previousPrintStream.close();
		}
	}
	
	public static void main(String args[]) {
		long start = System.currentTimeMillis();
		FileLogDispatcher fileLogDispatcher = new FileLogDispatcher(new File(System.getProperty("java.io.tmpdir")), "FileLogDispatcherTest", "", "log", true, 10000);
		final int NUMBER_OF_MSGS = 1000*1000;
		for (int i = 0; i < NUMBER_OF_MSGS ; i++) {
			fileLogDispatcher.print("iteration: "+i);
			fileLogDispatcher.println(" "+i+" menelaos");
		}
		long end = System.currentTimeMillis();
		System.out.println("took "+(end-start)+" milliseconds to write "+NUMBER_OF_MSGS+" messages");
	}
	

	
}
