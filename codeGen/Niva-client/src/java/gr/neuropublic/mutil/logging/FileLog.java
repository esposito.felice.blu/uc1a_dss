package gr.neuropublic.mutil.logging;

import java.io.File;
import java.io.PrintStream;


public class FileLog extends Log {

	private FileLogDispatcher out; 
	private final static String level_name_sep = " \t";
	
	public FileLog(String name, LogLevel defaultLevel, File loggingDir, boolean printLevelName, boolean printTimestamp, boolean echoToStandardOut, Integer everyThatManyCompress) {
		super(name, defaultLevel);
		this.out = new FileLogDispatcher(loggingDir, name, "", "log", echoToStandardOut, everyThatManyCompress);
		this.printLevelName(printLevelName);
		this.printTimestamp(printTimestamp);
	}
	
	public void compress() {
		out.runCompressorSynchronously();
	}

	@Override
	public void close() {
		out.close();
	}

	@Override
	public void log(LogLevel level, Object str1) {
		synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		
			
		out.println(str1);
		}
	}

	public void log(LogLevel level, Object str1, Object str2) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.println(str2);
		}
	}
	
public void log(LogLevel level, Object str1, Object str2, Object str3) 
	{	
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.println(str3);
		}
	}
		
public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.println(str4);	
		}
	}

public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4,
						 Object str5) 
	{	
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.print(str4);	
		out.println(str5);	
		}
	}

public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4,
						 Object str5, Object str6) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.print(str4);	
		out.print(str5);	
		out.println(str6);	
		}
	}

public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4,
						 Object str5, Object str6,
						 Object str7) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.print(str4);	
		out.print(str5);	
		out.print(str6);	
		out.println(str7);	
		}
	}

public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4,
						 Object str5, Object str6,
						 Object str7, Object str8) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.print(str4);	
		out.print(str5);	
		out.print(str6);	
		out.print(str7);	
		out.println(str8);	
		}
	}

public void log(LogLevel level, Object str1, Object str2, 
						 Object str3, Object str4,
						 Object str5, Object str6,
						 Object str7, Object str8, 
						 Object... args) 
	{
	synchronized(out)
		{
		if (printLevelName) {
			out.print(level.desc);
			out.print(level_name_sep);
			}

		if (printTimestamp) {
			out.print(getTS());
			out.print(" ");
			}		

		out.print(str1);
		out.print(" ");
		out.print(str2);
		out.print(str3);
		out.print(str4);	
		out.print(str5);	
		out.print(str6);	
		out.print(str7);	
		out.print(str8);
		
		final int len = args.length;
		for (int i = 0; i < len; i++) {
			out.print(args[i]);
			}
		
		out.println();
		}
	}

}
