package gr.neuropublic.validators.annotations;


import gr.neuropublic.validators.impl.TinValidator;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;


@Documented
@Constraint(validatedBy = TinValidator.class)
@Target({ METHOD, FIELD })
@Retention(RUNTIME)
public @interface ValidTin {
    	String message() default "$$Tin Number is not Valid";
    	Class<?>[] groups() default {};
        Class<? extends Payload>[] payload() default {};
}
