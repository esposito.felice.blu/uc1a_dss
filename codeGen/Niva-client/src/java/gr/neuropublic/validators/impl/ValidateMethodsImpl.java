package gr.neuropublic.validators.impl;

import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;
import gr.neuropublic.validators.annotations.ValidateMethods;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ValidateMethodsImpl implements ConstraintValidator<ValidateMethods, Serializable> {

    ValidateMethods annotation;
    private static Connection connection = null;
    private static final Logger logger = LoggerFactory.getLogger(ValidateMethodsImpl.class);

    @Override
    public void initialize(final ValidateMethods a) {

        annotation = a;

        logger.info("ValidateUniqueKeysImpl initialize()!!! ");

        try {
            //Create new connection in case we don't have one 
            if (connection == null) {
                connection = this.connectToDatabase();
            }

        } catch (Exception ex) {
            logger.error("Exception in initialize() of ValidateMethodsImpl exception =" + ex.getMessage());
            throw new RuntimeException("Exception in initialize() of ValidateMethodsImpl exception =" + ex.getMessage());
        }
    }

    @Override
    public boolean isValid(final Serializable target, ConstraintValidatorContext context) {

        final Class<?> entityClass = target.getClass();//entity class type
        EntityValidationError entityValidationError = new EntityValidationError();

        try {

            //Call all method validators in entity
            callAllMethodValidators(entityClass, target, entityValidationError);

            //Perform the same for superclass 
            Class superClass = entityClass.getSuperclass();
            if (superClass != null) {
                callAllMethodValidators(superClass, target, entityValidationError);
            }

            //If result has errors
            if (entityValidationError.hasErrors()) {

                //Set column names to message
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(entityValidationError.toString()).addConstraintViolation();

                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception in isValid() of ValidateMethodsImpl exception =" + e.getMessage());
            throw new RuntimeException(e.getMessage());
        }

        return true;
    }

    /**
     * Calls all methods of entity that have the
     *
     * @MethodValidator annotation
     * @param entityClass
     * @param target
     * @param entityValidationError
     * @throws IllegalAccessException
     * @throws SecurityException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     */
    private void callAllMethodValidators(final Class<?> entityClass, final Serializable target, EntityValidationError entityValidationError) throws IllegalAccessException, SecurityException, InvocationTargetException, IllegalArgumentException, SQLException, ClassNotFoundException, Exception {

        //For each method in entity that has the annotation @MethodValidator
        for (Method method : entityClass.getMethods()) {

            if (method.isAnnotationPresent(MethodValidator.class)) {

                EntityValidationSubError result = null;

                //if annotation needs DBConnection
                MethodValidator methodValidatorAnnotation = method.getAnnotation(MethodValidator.class);
                if ((methodValidatorAnnotation.needDBConnection() != null)
                        && (methodValidatorAnnotation.needDBConnection().equals("true"))) {

                    //check connection status
                    if ((connection == null) || (connection.isClosed())) {
                        connection = this.connectToDatabase();
                    }

                    //call method passing db connection
                    result = (EntityValidationSubError) method.invoke(target, connection);
                } else {
                    result = (EntityValidationSubError) method.invoke(target);
                }

                //Check validation result
                if (result != null) {
                    entityValidationError.getSubErrors().add(result);
                }
            }
        }
    }

    /**
     * This method creates a new JDBC connection with database specified in
     * properties file.
     *
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    protected Connection connectToDatabase() throws ClassNotFoundException,
            SQLException,
            Exception {
        //Επιλέγουμε να πάρουμε connection από το pool του weblogic
        return getJNDIConnection();
    }

    /**
     * Uses JNDI and Datasource (preferred style).
     *
     */
    protected Connection getJNDIConnection() {

        try {

            if ((connection == null) || (connection.isClosed())) {

                Context initialContext = new InitialContext();
                if (initialContext == null) {
                    logger.error("JNDI problem. Cannot get InitialContext.");
                }
//                DataSource datasource = (DataSource) initialContext.lookup(ApplicationConfig.getProperty("dataSourceName"));
                DataSource datasource = (DataSource) initialContext.lookup(AppDataSource.getDataSource());
                if (datasource != null) {
                    connection = datasource.getConnection();
                } else {
                    logger.error("Failed to lookup datasource.");
                }
            }

        } catch (NamingException ex) {
            logger.error("Cannot get connection: " + ex);
        } catch (SQLException ex) {
            logger.error("Cannot get connection: " + ex);
        }
        return connection;
    }
}
