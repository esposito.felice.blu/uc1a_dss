package gr.neuropublic.validators.impl;

import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.ICheckedEntity;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidateUniqueKeysImpl implements ConstraintValidator<ValidateUniqueKeys, Serializable> {

    private static final Logger logger = LoggerFactory.getLogger(ValidateUniqueKeysImpl.class);
    private static Connection connection = null;
    private ValidateUniqueKeys constraintAnnotation;

    public ValidateUniqueKeysImpl() {
    }

    @Override
    public void initialize(final ValidateUniqueKeys constraintAnnotation) {

        logger.info("ValidateUniqueKeysImpl initialize()!!! ");

        //Read annotation metadata
        this.constraintAnnotation = constraintAnnotation;

        try {
            //Create new connection in case we don't have one 
            if (connection == null) {
                connection = this.connectToDatabase();
            }

        } catch (Exception ex) {
            logger.error("Exception in initialize() of UniqueKeysValidator exception =" + ex.getMessage());
            throw new RuntimeException("Exception in initialize() of UniqueKeysValidator exception =" + ex.getMessage());
        }
    }

    @Override
    public boolean isValid(final Serializable target, final ConstraintValidatorContext context) {
        try {

            //check connection status
            if ((connection == null) || (connection.isClosed())) {
                connection = this.connectToDatabase();
            }

            //Call entity method to check database unique constraints
            EntityValidationError entityValidationError = ((ICheckedEntity) target).checkUniqueConstraints(connection);

            //If result has errors
            if (entityValidationError.hasErrors()) {

                //Set column names to message
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(entityValidationError.toString()).addConstraintViolation();

                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception in isValid() of ValidateUniqueKeysImpl exception =" + e.getMessage());
            throw new RuntimeException(e.getMessage());
        }

        return true;
    }

    /**
     * This method creates a new JDBC connection with database specified in
     * properties file.
     *
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    protected Connection connectToDatabase() throws ClassNotFoundException,
            SQLException,
            Exception {
        //Επιλέγουμε να πάρουμε connection από το pool του weblogic
        return getJNDIConnection();
    }

    /**
     * Uses JNDI and Datasource (preferred style).
     *
     */
    protected Connection getJNDIConnection() {

        try {

            if ((connection == null) || (connection.isClosed())) {

                Context initialContext = new InitialContext();
                if (initialContext == null) {
                    logger.error("JNDI problem. Cannot get InitialContext.");
                }
//                DataSource datasource = (DataSource) initialContext.lookup(ApplicationConfig.getProperty("dataSourceName"));
                DataSource datasource = (DataSource) initialContext.lookup(AppDataSource.getDataSource());
                if (datasource != null) {
                    connection = datasource.getConnection();
                } else {
                    logger.error("Failed to lookup datasource.");
                }
            }

        } catch (NamingException ex) {
            logger.error("Cannot get connection: " + ex);
        } catch (SQLException ex) {
            logger.error("Cannot get connection: " + ex);
        }
        return connection;
    }
}