package gr.neuropublic.utils;

/**
 * 
 * @author v_asloglou
 * @param <A>
 * @param <B> 
 */
public class TwoTuple<A, B> implements java.io.Serializable
{

    private final A first;
    private final B second;

    public TwoTuple(A first, B second)
    {
        this.first = first;
        this.second = second;
    }

    public A getFirst()
    {
        return this.first;
    }

    public B getSecond()
    {
        return this.second;
    }

    protected boolean equalObjects(Object o1, Object o2)
    {
        return ((o1 == null && o2 == null) || (o1 != null && o2 != null && o1.equals(o2)));
    }

    protected boolean equalFirst(TwoTuple other)
    {
        return equalObjects(this.getFirst(), other.getFirst());
    }

    protected boolean equalSecond(TwoTuple other)
    {
        return equalObjects(this.getSecond(), other.getSecond());
    }

    @Override
    public int hashCode()
    {
        int result = 17;
        result = 37 * result + (this.getFirst() == null ? 0 : this.getFirst().hashCode());
        result = 37 * result + (this.getSecond() == null ? 0 : this.getSecond().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof TwoTuple)) {
            return false;
        }
        TwoTuple other = (TwoTuple) o;
        return equalFirst(other) && equalSecond(other);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("{");
        sb.append(this.getFirst() == null ? "null" : this.getFirst().toString());
        sb.append(", ");
        sb.append(this.getSecond() == null ? "null" : this.getSecond().toString());
        sb.append("}");
        return sb.toString();
    }
}
