package gr.neuropublic.utils;

/**
 * 
 * @author v_asloglou
 * @param <A>
 * @param <B>
 * @param <C>
 * @param <D>
 * @param <E>
 * @param <F>
 * @param <G>
 * @param <H> 
 */
public class EightTuple<A, B, C, D, E, F, G, H>
        extends SevenTuple<A, B, C, D, E, F, G>
{

    private final H eighth;

    /**
     * Creates a new instance of EightTuple
     */
    public EightTuple(A first, B second, C third, D fourth, E fifth, F sixth, G seventh, H eighth)
    {
        super(first, second, third, fourth, fifth, sixth, seventh);
        this.eighth = eighth;
    }

    public H getEighth()
    {
        return this.eighth;
    }

    protected boolean equalEighth(EightTuple other)
    {
        return equalObjects(this.getEighth(), other.getEighth());
    }

    @Override
    public int hashCode()
    {
        return 37 * super.hashCode() + (this.getEighth() == null ? 0 : this.getEighth().hashCode());
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof EightTuple)) {
            return false;
        }
        EightTuple other = (EightTuple) o;
        return equalFirst(other) && equalSecond(other) && equalThird(other) && equalFourth(other) && equalFifth(other) && equalSixth(other) && equalSeventh(other) && equalEighth(other);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("{");
        sb.append(this.getFirst() == null ? "null" : this.getFirst().toString());
        sb.append(", ");
        sb.append(this.getSecond() == null ? "null" : this.getSecond().toString());
        sb.append(", ");
        sb.append(this.getThird() == null ? "null" : this.getThird().toString());
        sb.append(", ");
        sb.append(this.getFourth() == null ? "null" : this.getFourth().toString());
        sb.append(", ");
        sb.append(this.getFifth() == null ? "null" : this.getFifth().toString());
        sb.append(", ");
        sb.append(this.getSixth() == null ? "null" : this.getSixth().toString());
        sb.append(", ");
        sb.append(this.getSeventh() == null ? "null" : this.getSeventh().toString());
        sb.append(", ");
        sb.append(this.getEighth() == null ? "null" : this.getEighth().toString());
        sb.append("}");
        return sb.toString();
    }
}
