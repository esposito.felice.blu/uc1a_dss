package gr.neuropublic.utils;

/**
 * 
 * @author v_asloglou
 * @param <A>
 * @param <B>
 * @param <C>
 * @param <D>
 * @param <E>
 * @param <F>
 * @param <G> 
 */
public class SevenTuple<A, B, C, D, E, F, G>
        extends SixTuple<A, B, C, D, E, F>
{

    private final G seventh;

    /**
     * Creates a new instance of SevenTuple
     */
    public SevenTuple(A first, B second, C third, D fourth, E fifth, F sixth, G seventh)
    {
        super(first, second, third, fourth, fifth, sixth);
        this.seventh = seventh;
    }

    public G getSeventh()
    {
        return this.seventh;
    }

    protected boolean equalSeventh(SevenTuple other)
    {
        return equalObjects(this.getSeventh(), other.getSeventh());
    }

    @Override
    public int hashCode()
    {
        return 37 * super.hashCode() + (this.getSeventh() == null ? 0 : this.getSeventh().hashCode());
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SevenTuple)) {
            return false;
        }
        SevenTuple other = (SevenTuple) o;
        return equalFirst(other) && equalSecond(other) && equalThird(other) && equalFourth(other) && equalFifth(other) && equalSixth(other) && equalSeventh(other);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("{");
        sb.append(this.getFirst() == null ? "null" : this.getFirst().toString());
        sb.append(", ");
        sb.append(this.getSecond() == null ? "null" : this.getSecond().toString());
        sb.append(", ");
        sb.append(this.getThird() == null ? "null" : this.getThird().toString());
        sb.append(", ");
        sb.append(this.getFourth() == null ? "null" : this.getFourth().toString());
        sb.append(", ");
        sb.append(this.getFifth() == null ? "null" : this.getFifth().toString());
        sb.append(", ");
        sb.append(this.getSixth() == null ? "null" : this.getSixth().toString());
        sb.append(", ");
        sb.append(this.getSeventh() == null ? "null" : this.getSeventh().toString());
        sb.append("}");
        return sb.toString();
    }
}
