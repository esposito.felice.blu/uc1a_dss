package gr.neuropublic.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

public class DateUtil
{

    public final static int DAY_MILLISECONDS = 24 * 60 * 60 * 1000;
    public final static int HOUR_MILLISECONDS = 60 * 60 * 1000;
    public final static int MINUTE_MILLISECONDS = 60 * 1000;
    public final static int DAYS_OF_WEEK = 7;
    public final static int DAYS_OF_MONTH = 30;
    public final static int MONTHS_OF_YEAR = 12;

    /**
     * returns current datetime (seconds precision - milliseconds are set zero)
     */
    public static Date now()
    {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /**
     * returns current date (hours, minutes and seconds are 0)
     */
    public static Date today()
    {
        return trunc(now());
    }

    /**
     * truncates a date<BR> for example, trunc(11/11/1975 08:10:30) retunrs 11/11/1975 00:00:00<BR> If input parameter is null, returns null
     */
    public static Date trunc(Date dt)
    {
        if (dt == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /**
     * truncates a date, setting Milliseconds = 0<BR> for example, trunc(11/11/1975 08:10:30.385) retunrs 11/11/1975 08:10:30.000<BR> If input parameter is
     * null, returns null
     */
    public static Date truncMillis(Date dt)
    {
        if (dt == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /**
     * truncates a date, setting seconds = 0<BR> for example, trunc(11/11/1975 08:10:30.385) retunrs 11/11/1975 08:10:00:00<BR> If input parameter is null,
     * returns null
     */
    public static Date truncSeconds(Date dt)
    {
        if (dt == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        return c.getTime();
    }

    /**
     * returns the last second of a day<BR> for example, lastSecOfDay(11/11/1975 08:10:30.385) returns 11/11/1975 23:59:59.000)<BR> If input parameter is null,
     * returns null
     */
    public static Date lastSecOfDay(Date dt)
    {
        if (dt == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /**
     * Returns the date which occurs n days after (or before) a specified date
     *
     * @param dt The initial date
     * @param n days after (if negative, it means days before)
     * @return If input parameter is null, returns null
     */
    public static Date relativeDate(Date dt, int n)
    {
        if (dt == null) {
            return null;
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dt);
            cal.add(Calendar.DATE, n);
            return cal.getTime();
        }
    }

    /**
     * Returns the next day of the specified date
     *
     * @param dt the date
     * @return the next day or null if dt is null
     */
    public static Date nextDay(Date dt)
    {
        return dt == null ? null : relativeDate(dt, 1);
    }

    /**
     * Returns the previous day of the specified date
     *
     * @param dt the date
     * @return the previous date or null if dt is null
     */
    public static Date previousDay(Date dt)
    {
        return dt == null ? null : relativeDate(dt, -1);
    }

    /**
     * Returns the next minute of the specified date
     *
     * @param dt the date
     * @return the next minute or null if dt is null
     */
    public static Date nextMinute(Date dt)
    {
        return dt == null ? null : relativeDate(dt, 1, Calendar.MINUTE);
    }

    /**
     * Returns the date which occurs n periods after (or before) a specified date
     *
     * @param dt The initial date.
     * @param n periods after (if negative, it means periods before).
     * @param periodIndicator The period indicator (calendar.MONTH or YEAR).
     * @return The relative date. If input parameter is null, returns null.
     */
    public static Date relativeDate(Date dt, int n, int periodIndicator)
    {
        if (dt == null) {
            return null;
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dt);
            cal.add(periodIndicator, n);
            return cal.getTime();
        }
    }

    /**
     * Determines the number of days one date occurs after another
     *
     * @param dt1 a date value that is the start date of the interval being measured
     * @param dt2 a date value that is the end date of the interval
     * @return an integer whose value is the number of days dt2 occurs after dt1. If dt2 occurs before dt1, then returns a negative number.
     */
    public static int daysAfter(Date dt1, Date dt2)
    {
        long msec = dt2.getTime() - dt1.getTime();
        if (msec >= 0) {
            msec += (DAY_MILLISECONDS / 2);
        } else {
            msec -= (DAY_MILLISECONDS / 2);
        }
        return (int) (msec / DAY_MILLISECONDS);
    }

    /**
     * Determines the days duration in the specified period
     *
     * @param dt1 a date value that is the start date of the period
     * @param dt2 a date value that is the end date of the period
     * @return an integer whose value is the number of days of the period.
     */
    public static int periodDuration(Date dt1, Date dt2)
    {
        Date periodStart = dt1;
        Date periodEnd = dt2;
        if (dt1.after(dt2)) {
            periodStart = dt2;
            periodEnd = dt1;
        }
        return daysAfter(periodStart, relativeDate(periodEnd, 1));
    }

    /**
     * Returns the number of months (rounded) one date occurs after another
     *
     * @param dt1 a date value that is the start date of the interval being measured
     * @param dt2 a date value that is the end date of the interval
     * @return an integer whose value is the number of months (rounded) dt2 occurs after dt1. If dt2 occurs before dt1, then returns a negative number. If dt1
     * is null or dt2 is null then returns null
     */
    public static Integer monthsAfter(Date dt1, Date dt2)
    {
        if (dt1 == null || dt2 == null) {
            return null;
        }
        int days = daysAfter(dt1, dt2);
        Double d = RoundUtil.round(days / 30, 0); // check this out!
        return d.intValue();
    }

    /**
     * Returns the number of years (rounded) one date occurs after another
     *
     * @param dt1 a date value that is the start date of the interval being measured
     * @param dt2 a date value that is the end date of the interval
     * @return an integer whose value is the number of years (rounded) dt2 occurs after dt1. If dt2 occurs before dt1, then returns a negative number. If dt1 is
     * null or dt2 is null then returns null
     */
    public static Integer yearsAfter(Date dt1, Date dt2)
    {
        if (dt1 == null || dt2 == null) {
            return null;
        }
        int days = daysAfter(dt1, dt2);
        Double d = RoundUtil.round(days / 365.25, 0);
        return d.intValue();
    }

    /**
     * Returns the number of years one date occurs after another
     *
     * @param dt1 a date value that is the start date of the interval being measured
     * @param dt2 a date value that is the end date of the interval
     * @return an integer whose value is the number of years dt2 occurs after dt1. Calculation is only based in year value for example if dt1 is 1/1/2004 and
     * dt2 is 31/12/2008 it will return 4 and not 5 (witch is the rounded year)
     */
    public static Integer yearsAfterByYear(Date dt1, Date dt2)
    {
        Calendar day1 = Calendar.getInstance();
        day1.setTime(dt1);
        Calendar day2 = Calendar.getInstance();
        day2.setTime(dt2);
        int year1 = day1.get(day1.YEAR);
        int year2 = day2.get(day1.YEAR);
        return year2 - year1;
    }

    /**
     * Returns true if and only if the specified date has time (time != 00:00:00)
     */
    public static boolean hasTime(Date dt)
    {
        if (dt == null) {
            return false;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        return !((c.get(c.HOUR_OF_DAY) == 0)
                && (c.get(c.MINUTE) == 0)
                && (c.get(c.SECOND) == 0));
    }

    /**
     * returns a list with dates divided in slot (slot in minutes) for example if date from is 11/11/2005 18:00 and date to is 11/11/2005 19:00 and slot = 15 it
     * will return a list with dates 11/11/2005 18:00 11/11/2005 18:15 11/11/2005 18:30 11/11/2005 18:45 11/11/2005 19:00 if slot <= 0 it will return an empty list!
     */
    public static final List getDateSlots(Date dateFrom, Date dateTo, int slot)
    {
        ArrayList dates = new ArrayList();
        Date newDate = new Date(dateFrom.getTime());

        if (slot <= 0) {
            return dates;
        }
        //System.out.println("*************************");
        //System.out.println("************************* newDate " + newDate.toString());
        //System.out.println("************************* dateFrom " + dateFrom.toString());
        //System.out.println("************************* dateTo " + dateTo.toString());
        dates.add(new Date(dateFrom.getTime()));
        while (newDate.getTime() < (dateTo.getTime() - slot * 60 * 1000)) {
            //System.out.println("************************* newDate In " + newDate.toString());
            newDate.setTime(newDate.getTime() + slot * 60 * 1000);

            Date sDate = new Date(newDate.getTime());
            //System.out.println("**********!!!! Date to Add " + sDate.toString());
            // @@@@@@@ KRA @@@@@@@ πρέπει να είναι με = για να μη χάνει το τελευταίο σλοτ και πρέπει
            // να βάζουμε 14:01
            if (sDate.getTime() + slot * 60 * 1000 <= dateTo.getTime()) {
                dates.add(sDate);
                //System.out.println("*********** Date added " + sDate.toString());
            }

        }
        return dates;
    }

    /**
     *
     * @return a list of all the dates of week in which date belongs Weeks Start AT SUNDAY!! for example if date is TUE 29/11/2005 it will return an array with
     * [0] = SUN 27/11/2005 [1] = MON 28/11/2005 [2] = TUE 29/11/2005 [3] = WED 30/11/2005 ... [6] = SUN (whatever is next Suturday...)
     * @param date
     */
    public static final Date[] getDatesOfWeekDay(Date date)
    {
        Date dates[] = new Date[7];


        Calendar cDate = Calendar.getInstance();
        cDate.setTime(date);


        cDate.add(cDate.DATE, cDate.SUNDAY - cDate.get(cDate.DAY_OF_WEEK)); // roll to first day of week
        dates[0] = new Date(cDate.getTime().getTime());
        for (int i = 1; i <= 6; i++) {
            cDate.add(cDate.DATE, 1);
            Date newDate = cDate.getTime();
            dates[i] = newDate;
        }

        return dates;
    }

    /**
     *
     * @return the first day of week in which date belongs. Weeks Start AT SUNDAY!! for example if date is TUE 29/11/2005 it will return SUN 27/11/2005
     * @param date
     */
    public static final Date getFirstDateOfWeekDay(Date date)
    {
        Date firstDateOfWeek = null;

        Calendar cDate = Calendar.getInstance();
        cDate.setTime(date);

        cDate.add(cDate.DATE, cDate.SUNDAY - cDate.get(cDate.DAY_OF_WEEK)); // roll to first day of week
        firstDateOfWeek = new Date(cDate.getTime().getTime());

        return firstDateOfWeek;
    }

    /**
     * The first day of a month.
     *
     * @param month The month indicator (january is month 0)
     * @param year The year.
     * @return The date of the first day of a month's year.
     */
    public static final Date getFirstDateOfMonth(int month, int year)
    {
        Date firstDateOfMonth = null;
        Calendar c = Calendar.getInstance();
        c.set(c.MONTH, month);
        c.set(c.YEAR, year);
        c.set(c.DAY_OF_MONTH, 1);
        firstDateOfMonth = new Date(c.getTime().getTime());
        return firstDateOfMonth;
    }

    /**
     * The first day of a month.
     *
     * @param date The date whose month's first day we search for.
     * @return The date of the first day of the month.
     */
    public static final Date getFirstDateOfMonth(Date dt)
    {
        Date firstDateOfMonth = null;
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.set(c.DAY_OF_MONTH, 1);
        firstDateOfMonth = new Date(c.getTime().getTime());
        return firstDateOfMonth;
    }

    /**
     * The last day of a month.
     *
     * @param date The date whose month's last day we search for.
     * @return The date of the last day of the month.
     */
    public static final Date getLastDateOfMonth(Date dt)
    {
        Date lastDateOfMonth = null;
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.set(c.DAY_OF_MONTH, getDaysOfMonth(dt));
        lastDateOfMonth = new Date(c.getTime().getTime());
        return lastDateOfMonth;
    }

    /**
     * The last day of a month
     *
     * @param month The month whose last day we are searching
     * @param year The year to which this month belongs to
     */
    public static final Date getLastDateOfMonth(int month, int year)
    {

        Date lastDateOfMonth = null;
        Calendar c = new GregorianCalendar(year, month, 1);
        c.set(c.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        lastDateOfMonth = new Date(c.getTime().getTime());
        return lastDateOfMonth;


    }

    /**
     *
     * @return a date with the time given in date dateWithTime
     * @param dateWithTime
     * @param dateOrigin
     *
     * for example if dateOrigin is 28/11/2005 13:00 and dateWithTime id 1/1/3000 8:40 it will return 28/11/2005 8:40
     *
     */
    public static final Date replaceTime(Date dateOrigin, Date dateWithTime)
    {
        Calendar dateTime = Calendar.getInstance();
        dateTime.setTime(dateWithTime);
        int hourOfDay = dateTime.get(dateTime.HOUR_OF_DAY);
        int min = dateTime.get(dateTime.MINUTE);
        int mili = dateTime.get(dateTime.MILLISECOND);

        Calendar dateTo = Calendar.getInstance();
        dateTo.setTime(dateOrigin);
        dateTo.set(dateTo.HOUR_OF_DAY, hourOfDay);
        dateTo.set(dateTo.MINUTE, min);
        dateTo.set(dateTo.MILLISECOND, mili);

        return dateTo.getTime();
    }

    /**
     *
     * @return @param min
     * @param date
     */
    public static final Date addMinutes(Date date, int min)
    {
        Calendar dateOld = Calendar.getInstance();
        dateOld.setTime(date);
        dateOld.add(Calendar.MINUTE, min);

        return dateOld.getTime();
    }

    /**
     * returns a date of the same day as "date" that is in week of date "dateTo" For example, if date is Mo 28/11/2005 and dateTo is Tue 22/12/2005 it will
     * return Mo 21/12/2005 It is used in booking for perm availabilities... - long story...
     *
     * @return
     * @param dateTo
     * @param date
     */
    public static final Date getSameDayInWeekOfDate(Date dateFrom, Date dateTo)
    {
        Date weekToDays[] = getDatesOfWeekDay(dateTo);
        Date rollDate = null;
        Calendar dateFromC = Calendar.getInstance();
        dateFromC.setTime(dateFrom);

        for (int i = 0; i <= 6; i++) {
            Date thisDay = weekToDays[i];
            Calendar thisDayC = Calendar.getInstance();
            thisDayC.setTime(thisDay);
            if (thisDayC.get(Calendar.DAY_OF_WEEK) == dateFromC.get(Calendar.DAY_OF_WEEK)) {
                dateFromC.set(thisDayC.get(Calendar.YEAR), thisDayC.get(Calendar.MONTH), thisDayC.get(Calendar.DAY_OF_MONTH));
                rollDate = dateFromC.getTime();
            }
        }
        return rollDate;
    }

    /**
     * Checks if two date/time periods intersect
     *
     * @param from1 Start of period1 (null means eternity)
     * @param to1 End of period1 (null means eternity)
     * @param from2 Start of period2 (null means eternity)
     * @param to2 End of period2 (null means eternity)
     * @return true if and only if the periods intersect<BR> Example:<BR>period1=[24/01/05-28/01/05], period2=[27/01/05-30/01/05] returns true (intersection is
     * 27/01 & 28/01)
     */
    public static boolean periodsIntersect(Date from1, Date to1, Date from2, Date to2)
    {
        if (from1 == null && from2 == null) {
            return true;
        }
        if (to1 == null && to2 == null) {
            return true;
        }
        if ((from2 != null) && (from1 == null || from2.after(from1))) {
            return (to1 == null || !from2.after(to1));
        } else {
            return (to2 == null || !from1.after(to2));
        }
    }

//    /**
//     * Counts the intersection days of two periods (period dates cant be null).
//     * 
//     * @param periodAFrom The start date of the first period.
//     * @param periodATo The end date of the first period.
//     * @param periodBFrom The start date of the second period.
//     * @param periodBTo The end date of the second period.
//     * Example:<BR>period1=[24/01/05-28/01/05], period2=[27/01/05-30/01/05] returns 2 (intersection is 27/01 & 28/01).
//     */
//    public static int countIntersectionDaysOfPeriods(Date periodAFrom, Date periodATo, Date periodBFrom, Date periodBTo){
////        if(periodATo.before(periodBFrom) || periodBTo.before(periodAFrom)){
////            return 0;
////        }
////        if(!periodAFrom.after(periodBFrom)){
////            return periodDuration(periodBFrom, min(periodATo, periodBTo));
////        }
////        else{
////            return periodDuration(periodAFrom, min(periodBTo, periodATo));
////        }
//        TwoTuple<Date, Date> intersectionPeriod = getIntersectionPeriodOfPeriods(periodAFrom, periodATo, periodBFrom, periodBTo); 
//        if(intersectionPeriod == null){
//            return 0;
//        }
//        else{
//            return periodDuration(intersectionPeriod.getFirst(), intersectionPeriod.getSecond());
//        }
//    }
    /**
     * Gets the intersection period of two periods (period dates cant be null).
     *
     * @param periodAFrom The start date of the first period.
     * @param periodATo The end date of the first period.
     * @param periodBFrom The start date of the second period.
     * @param periodBTo The end date of the second period.
     * @return The intersection period Example:<BR>period1=[24/01/05-28/01/05], period2=[27/01/05-30/01/05] the intersection is 27/01 & 28/01. Returns null if
     * periods dont intersect.
     */
    public static TwoTuple<Date, Date> getIntersectionPeriodOfPeriods(Date periodAFrom, Date periodATo, Date periodBFrom, Date periodBTo)
    {
        if (periodATo.before(periodBFrom) || periodBTo.before(periodAFrom)) {
            return null;
        }
        if (!periodAFrom.after(periodBFrom)) {
            return new TwoTuple<Date, Date>(periodBFrom, min(periodATo, periodBTo));
        } else {
            return new TwoTuple<Date, Date>(periodAFrom, min(periodBTo, periodATo));
        }
    }

    /**
     * Gets two Dates, and returns the minimum
     *
     * @param dt1
     * @param dt2
     * @return if dt1 before dt2, returns dt1, else returns dt2
     */
    public static Date min(Date dt1, Date dt2)
    {
        if (dt1 == null && dt2 != null) {
            return dt2;
        } else {
            if (dt1 != null && dt2 == null) {
                return dt1;
            } else {
                if (dt1 == null && dt2 == null) {
                    return null;
                } else {
                    return (dt1.before(dt2) ? dt1 : dt2);
                }
            }
        }
    }

    /**
     * Gets two Dates, and returns the maximum
     *
     * @param dt1
     * @param dt2
     * @return if dt1 before dt2, returns dt2, else returns dt1
     */
    public static Date max(Date dt1, Date dt2)
    {
        if (dt1 == null && dt2 != null) {
            return dt1;
        } else {
            if (dt1 != null && dt2 == null) {
                return dt2;
            } else {
                return (dt1.before(dt2) ? dt2 : dt1);
            }
        }
    }

    /**
     * Returns a string with format yyyymmddhh24miss for the date passed
     */
    public static String toStringDateTime(Date dt)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        return getDate(cal) + getTime(cal);
    }

    /**
     * Returns a string with format yyyymmdd for the date passed
     */
    public static String toStringDate(Date dt)
    {
        if (dt == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        return getDate(cal);
    }

    /**
     * Returns a string with format ddmmyyyy for the date passed
     */
    public static String toStringDDMMYYYY(Date dt)
    {
        if (dt == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        return getDateDDMMYYYY(cal);
    }

    public static String curDateTime()
    {
        Calendar cal = Calendar.getInstance();

        return getDate(cal) + getTime(cal);
    }

    public static String curDate()
    {
        Calendar cal = Calendar.getInstance();

        return getDate(cal);
    }

    /**
     * Returns a string with format mm (the month) for the date passed
     */
    public static String getMonth(Date dt)
    {
        if (dt == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        return getMonth(cal);
    }

    /**
     * Returns a string with format mm (the next month) for the date passed
     */
    public static String getNextMonth(Date dt)
    {
        if (dt == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.add(Calendar.MONTH, 1);
        return getMonth(cal);
    }

    /**
     * Returns the calendar specific number for the month of the specified date. January is month 0 and so forth.
     */
    public static Integer getMonthNum(Date dt)
    {
        return Integer.valueOf(getMonth(dt)) - 1;
    }

    /**
     * Returns a string with format yyyy (the year) for the date passed
     */
    public static String getYear(Date dt)
    {
        if (dt == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        return getYear(cal);
    }

    /**
     * Returns the calendar specific number for the year of the specified date.
     */
    public static Integer getYearNum(Date dt)
    {
        return Integer.valueOf(getYear(dt));
    }

    /**
     * Gets a calendar as a parameter. Returns a string with format yyyymmdd.
     */
    private static String getDate(Calendar cal)
    {
        String sYear, sMonth, sDay;
        sYear = new Integer(cal.get(Calendar.YEAR)).toString();
        sMonth = "00" + (cal.get(Calendar.MONTH) + 1);
        sDay = "00" + cal.get(Calendar.DAY_OF_MONTH);
        String sDate;
        sDate = sYear + sMonth.substring(sMonth.length() - 2, sMonth.length()) + sDay.substring(sDay.length() - 2, sDay.length());

        return sDate;
    }

    /**
     * Gets a calendar as a parameter. Returns a string with format ddMMYYYY.
     */
    private static String getDateDDMMYYYY(Calendar cal)
    {
        String sYear, sMonth, sDay;
        sYear = new Integer(cal.get(Calendar.YEAR)).toString();
        sMonth = "00" + (cal.get(Calendar.MONTH) + 1);
        sDay = "00" + cal.get(Calendar.DAY_OF_MONTH);
        String sDate;
        sDate = sDay.substring(sDay.length() - 2, sDay.length()) + sMonth.substring(sMonth.length() - 2, sMonth.length()) + sYear;
        return sDate;
    }

    private static String getYear(Calendar cal)
    {
        String sYear;
        sYear = new Integer(cal.get(Calendar.YEAR)).toString();
        return sYear;
    }

    private static String getMonth(Calendar cal)
    {
        String sMonth;
        sMonth = new Integer(cal.get(Calendar.MONTH) + 1).toString();
        return sMonth;
    }

    /**
     * Gets a calendar as a parameter. Returns a string with format hh24miss.
     */
    public static String getTime(Calendar cal)
    {
        String sHours, sMinutes, sSeconds;
        sHours = "00" + cal.get(Calendar.HOUR_OF_DAY);
        sMinutes = "00" + cal.get(Calendar.MINUTE);
        sSeconds = "00" + cal.get(Calendar.SECOND);
        String sTime;
        sTime = sHours.substring(sHours.length() - 2, sHours.length())
                + sMinutes.substring(sMinutes.length() - 2, sMinutes.length())
                + sSeconds.substring(sSeconds.length() - 2, sSeconds.length());

        return sTime;
    }

    /**
     * Returns the week day id of the specified date. Returned day ids correspond to {@link com.atkosoft.cm.entitybeans.WeekDay} object data.
     */
    public static Integer getWeekDay(Date dt)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        return dayOfWeek;

    }

    /**
     * Return the months of the given days period.
     */
    public static double getMonthsDurationOfDays(long days)
    {
        Double daysD = Double.valueOf(days);
        Double daysOfMonthD = Double.valueOf(DAYS_OF_MONTH);
        return RoundUtil.round((daysD.doubleValue() / daysOfMonthD.doubleValue()), 2);
    }

    /**
     * Converts a string to a Date of the specified format (for example dd/MM/yyyy). If string and date format are incompatible, or string is null return null.
     */
    public static Date toDate(String date, String format)
    {
        if (date == null) {
            return null;
        }
        try {
            return new SimpleDateFormat(format).parse(date);
        } catch (ParseException ex) {
            return null;
        }
    }

    /**
     * Returns the days of the specified date's month. Return 0 id date is null.
     */
    public static int getDaysOfMonth(Date dt)
    {
        if (dt == null) {
            return 0;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        return cal.getActualMaximum(cal.DAY_OF_MONTH);
    }

    /**
     * Returns the number of whole months between two dates.
     */
    public static int monthsBetween(Date dt1, Date dt2)
    {
        Date firstDate = min(dt1, dt2);
        Date secondDate = max(dt1, dt2);
        Calendar c1 = Calendar.getInstance();
        c1.setTime(firstDate);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(secondDate);
        int day1 = c1.get(c1.DAY_OF_MONTH);
        int day2 = c2.get(c2.DAY_OF_MONTH);
        int daysOfMonth1 = DateUtil.getDaysOfMonth(dt1);
        int daysOfMonth2 = DateUtil.getDaysOfMonth(dt2);
        int year1 = c1.get(c1.YEAR);
        int year2 = c2.get(c2.YEAR);
        // (january is month 0 for java so add 1 in months)
        int month1 = c1.get(c1.MONTH) + 1;
        int month2 = c2.get(c2.MONTH) + 1;

        int num1 = year1 * DateUtil.MONTHS_OF_YEAR + month1;
        int num2 = year2 * DateUtil.MONTHS_OF_YEAR + month2;

        double monthCoverage1 = (Double.valueOf(day1)).doubleValue() / (Double.valueOf(daysOfMonth1)).doubleValue();
        double monthCoverage2 = (Double.valueOf(day2)).doubleValue() / (Double.valueOf(daysOfMonth2)).doubleValue();

        int monthDuration = (num2 - num1);
        if (monthCoverage2 < monthCoverage1) {
            monthDuration -= 1;
        }
        return monthDuration;
    }

    /**
     * Return an array with the periods (years, months, days) indicated by the specified days. For example 392 days is 1 year, 1 month and 2 days, supposing
     * than one month is 30 days.
     *
     * @param days The number of days.
     * @return An array with 3 elements : <li>The years of days <li>The months of the rest days <li>The rest days.
     */
    public static int[] getPeriodComponentsOfDays(int totalDays)
    {
        int daysOfYear = DAYS_OF_MONTH * MONTHS_OF_YEAR;
        int daysOfMonth = DAYS_OF_MONTH;
        int years = 0;
        int months = 0;
        int days = 0;
        // construct days compoments
        years = totalDays / daysOfYear; // years
        totalDays = totalDays % daysOfYear; // rest days of year
        if (totalDays >= daysOfMonth) {
            months = totalDays / daysOfMonth; // months
            totalDays = totalDays % daysOfMonth; // rest days of month
        }
        if (totalDays > 0) {
            days = totalDays; // days
        }
        return new int[]{years, months, days};
    }

    public static boolean equals(Date dt1, Date dt2)
    {
        if (((dt1 == null) && (dt2 != null))
                || ((dt1 != null) && (dt2 == null))
                || ((dt1 != null) && (dt2 != null) && (!dt1.equals(dt2)))) {
            return false;
        }
        return true;
    }

    /**
     * Compares dates. Comararison handles null dates as greater than any other date
     *
     * @param dt1 date 1
     * @param dt2 date 2
     * @return <LI>1 if dt1 after dt2 <LI>0 if dt1 equals to dt2 <LI>-1 if dt1 before dt2
     */
    public static int compareDates(Date dt1, Date dt2)
    {
        if (dt1 == null && dt2 == null) {
            return 0;
        } else {
            if (dt1 == null && dt2 != null) {
                return 1;
            } else {
                if (dt1 != null && dt2 == null) {
                    return -1;
                } else {
                    if (dt1.before(dt2)) {
                        return -1;
                    } else {
                        if (dt1.after(dt2)) {
                            return 1;
                        } else {
                            return 0;
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns the month day of a date.
     */
    public static int getMonthDay(Date dt)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        return c.get(c.DAY_OF_MONTH);
    }
    
    
    
    
    

    
    
    
    
    
    
    
    
    
// 2004-06-14T19:GMT20:30Z
    // 2004-06-20T06:GMT22:01Z

    // http://www.cl.cam.ac.uk/~mgk25/iso-time.html
    //    
    // http://www.intertwingly.net/wiki/pie/DateTime
    //
    // http://www.w3.org/TR/NOTE-datetime
    //
    // Different standards may need different levels of granularity in the date and
    // time, so this profile defines six levels. Standards that reference this
    // profile should specify one or more of these granularities. If a given
    // standard allows more than one granularity, it should specify the meaning of
    // the dates and times with reduced precision, for example, the result of
    // comparing two dates with different precisions.

    // The formats are as follows. Exactly the components shown here must be
    // present, with exactly this punctuation. Note that the "T" appears literally
    // in the string, to indicate the beginning of the time element, as specified in
    // ISO 8601.

    //    Year:
    //       YYYY (eg 1997)
    //    Year and month:
    //       YYYY-MM (eg 1997-07)
    //    Complete date:
    //       YYYY-MM-DD (eg 1997-07-16)
    //    Complete date plus hours and minutes:
    //       YYYY-MM-DDThh:mmTZD (eg 1997-07-16T19:20+01:00)
    //    Complete date plus hours, minutes and seconds:
    //       YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)
    //    Complete date plus hours, minutes, seconds and a decimal fraction of a
    // second
    //       YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)

    // where:

    //      YYYY = four-digit year
    //      MM   = two-digit month (01=January, etc.)
    //      DD   = two-digit day of month (01 through 31)
    //      hh   = two digits of hour (00 through 23) (am/pm NOT allowed)
    //      mm   = two digits of minute (00 through 59)
    //      ss   = two digits of second (00 through 59)
    //      s    = one or more digits representing a decimal fraction of a second
    //      TZD  = time zone designator (Z or +hh:mm or -hh:mm)
    public static Date parseISO8601Date ( String input ) throws java.text.ParseException {
        
        if (input == null)
            return null;

        //NOTE: SimpleDateFormat uses GMT[-+]hh:mm for the TZ which breaks
        //things a bit.  Before we go on we have to repair this.
        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSSz" );
        
        //this is zero time so we need to add that TZ indicator for 
        if ( input.endsWith( "Z" ) ) {
            input = input.substring( 0, input.length() - 1) + "GMT-00:00";
        } else {
            int inset = 6;
        
            String s0 = input.substring( 0, input.length() - inset );
            String s1 = input.substring( input.length() - inset, input.length() );

            input = s0 + "GMT" + s1;
        }
        
        return df.parse( input );
        
    }

    public static String toString( Date date ) {
        
        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssz" );
        
        TimeZone tz = TimeZone.getTimeZone( "UTC" );
        
        df.setTimeZone( tz );

        String output = df.format( date );

        int inset0 = 9;
        int inset1 = 6;
        
        String s0 = output.substring( 0, output.length() - inset0 );
        String s1 = output.substring( output.length() - inset1, output.length() );

        String result = s0 + s1;

        result = result.replaceAll( "UTC", "+00:00" );
        
        return result;
        
    }
    
    
    
    
}
