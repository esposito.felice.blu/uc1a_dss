/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.neuropublic.functional;

/**
 *
 * @author st_krommydas
 */
public interface Func1Ex<T1, TRes> {
    public TRes lambda(T1 x1) throws Exception;
}
