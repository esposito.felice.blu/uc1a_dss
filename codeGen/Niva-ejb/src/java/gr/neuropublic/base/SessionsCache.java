package gr.neuropublic.base;


import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class SessionsCache implements ISessionsCache {

    private final Logger logger = LoggerFactory.getLogger(SessionsCache.class);

    ConcurrentMap<String,UserSession> sessionsCacheMap = new ConcurrentHashMap<>();
    
    
    @EJB
    private IMemCacheService.ILocal memCacheService;
    public IMemCacheService.ILocal getMemCacheService() {
        return memCacheService;
    }
    public void setMemCacheService(IMemCacheService.ILocal memCacheService) {
        this.memCacheService = memCacheService;
    }
    
    

    public abstract IUserManagementService getUserManagementService();

    @Override
    public UserSession getEntry(String sessionId ) {
        return getEntry(sessionId, true);
    }
    /**
     * returns null if the sessionId is not present in the dictionary or in the 
     * database. Otherwise returns the UserSession object
     */
    @Override
    public UserSession getEntry(String sessionId, boolean resetMemCacheTimer ) {
        if (sessionId == null)
            return null;

        if (getMemCacheService().getValue(sessionId) == null) {
            sessionsCacheMap.remove(sessionId);
            getUserManagementService().userSessionExpired(sessionId);            
            return null;
        }
        else {
            if (resetMemCacheTimer) {
                getMemCacheService().setValue(sessionId, "1");
            }
        }
        
        
        UserSession entryData = sessionsCacheMap.get(sessionId);
        if (entryData != null) {
            entryData.updateLastAccessTime();
            return entryData;
        }
        else {

            //Fetch data from DB
            UserSession ret = getUserManagementService().getUserSession(sessionId);
            if (ret == null)
                return null; //no user is logged in with this sessionid
            
            //Add data to Map
            UserSession existing = sessionsCacheMap.putIfAbsent(sessionId, ret);
            if (existing != null) {
                ret = existing;
            }
            
            ret.updateLastAccessTime();
            return ret;
        }
    }

    @Override
    public UserSession getEntry(String sessionId, String ssoSessionId, boolean resetMemCacheTimer) {
        if (sessionId == null)
            return null;
        
        UserSession ret = getEntry(sessionId, resetMemCacheTimer); 
        
        // if sessionId is active and exists sso add sessionId to sso sessions
        if (resetMemCacheTimer && ret != null && ssoSessionId != null)
            refreshSsoSessions(sessionId, ssoSessionId);
            
        return ret;

    }

    private void refreshSsoSessions(String sessionId, String ssoSessionId) {
            List<String> ssoSessions = getMemCacheService().getListValue(ssoSessionId);
            List<String> refreshedSsoSessions = new ArrayList<>();
            
            if (ssoSessions == null) {
                refreshedSsoSessions.add(sessionId);
            } else {
                for (String sId:ssoSessions) {
                    if (getMemCacheService().getValue(sId) != null) {
                        refreshedSsoSessions.add(sId);
                        getMemCacheService().setValue(sId, "1");
                    }
                }
                if (!refreshedSsoSessions.contains(sessionId))
                    refreshedSsoSessions.add(sessionId);
                
            }
            getMemCacheService().setListValue(ssoSessionId, refreshedSsoSessions);
    }
    
    
    private void removeEntryAndLogoutFromDataBase(String sessionId) {
        if (sessionId == null)
            return;
        
        sessionsCacheMap.remove(sessionId);
        getUserManagementService().logoutUserBySessionId(sessionId);
        getMemCacheService().deleteKey(sessionId);

    }
    @Override
    public void removeEntryAndLogoutFromDataBase(String sessionId, String ssoSessionId, Boolean removeSsoKey) {
        if (sessionId == null)
            return;
        
        removeEntryAndLogoutFromDataBase(sessionId);
        if (ssoSessionId != null) {
            List<String> currentSsoSessions = getMemCacheService().getListValue(ssoSessionId);
            
            if (removeSsoKey) {
                if (currentSsoSessions != null) {
                    for(String sId:currentSsoSessions) {
                        getMemCacheService().deleteKey(sId);
                    }
                }
                getMemCacheService().deleteKey(ssoSessionId);
            } else {
                if (currentSsoSessions != null) {
                    //ssoSessions.remove(sessionId);
                    List<String> ssoSessions = new ArrayList<>();
                    for (String sId:currentSsoSessions) {
                        if (!sId.equals(sessionId))
                            ssoSessions.add(sId);
                    }
                    getMemCacheService().setListValue(ssoSessionId, ssoSessions);
                }
                
            }
            
        }
    }
    
    @Override
    public boolean sessionBelongToSso(String sessionId, String ssoSessionId) {
        if (sessionId == null || ssoSessionId == null)
            return false;
        
        List<String> ssoSessions = getMemCacheService().getListValue(ssoSessionId);
        if (ssoSessions == null || ssoSessions.isEmpty())
            return false;
        
        if (ssoSessions.contains(sessionId))
            return true;
        
        return false;
    }
}
