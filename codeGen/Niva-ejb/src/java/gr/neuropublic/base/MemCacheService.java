/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Remote;
import net.spy.memcached.MemcachedClient;

/**
 *
 * @author gmamais
 */
@javax.ejb.Singleton
@javax.ejb.Startup
@Local(IMemCacheService.ILocal.class)
@Remote(IMemCacheService.IRemote.class)
public class MemCacheService implements IMemCacheService {
  
  
  MemcachedClient _memCache = null;
  Integer memCacheTimeout = 1800;
    
  String externalLogin_reqKey = UUID.randomUUID().toString();
  @PostConstruct 
  public void init()  {
        try {
            String memCacheIpAddress = Property.getPropertyValue("memCache.properties", "IP", "172.17.6.103");
            Integer memCachePort = Integer.parseInt(Property.getPropertyValue("memCache.properties", "port", "11211"));
            memCacheTimeout = Integer.parseInt(Property.getPropertyValue("memCache.properties", "timeout", "1800"));
            _memCache = new MemcachedClient(new InetSocketAddress(memCacheIpAddress, memCachePort));
            
            externalLogin_reqKey = Property.getPropertyValue("external_login.properties", "reqKey", externalLogin_reqKey);
        } catch (IOException ex) {
            Logger.getLogger(MemCacheService.class.getName()).log(Level.SEVERE, null, ex);
        }
  } 
  
  
  public void setValue(String key, String value) {
      _memCache.set(key, memCacheTimeout, value);
  }
  
  public String getValue(String key) {
      return (String)_memCache.get(key);
  }
  
  public void deleteKey(String key) {
      _memCache.delete(key);
  }

  public void setListValue(String key, List<String> value) {
      String stringValue = "";
      if (value == null || value.isEmpty()) {
        stringValue = "";
      } else{
        Integer length = value.size();
        for(int i=0; i<length-1;i++) {
              stringValue += value.get(i) + ", ";
          }
          stringValue += value.get(length-1);
      }
      
      _memCache.set(key, memCacheTimeout, stringValue);
  }
  public List<String> getListValue(String key) {
      String val = (String)_memCache.get(key);
      List<String> list = new ArrayList<>();
      if (val == null || "".equals(val))
          return list;
      
      list = Arrays.asList(val.split(", "));
      return list;
  }

  
    public String getExternalLogin_reqKey() {
        return externalLogin_reqKey;
    }
  
}
