package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class EcCultDetailCopyBaseFacade extends AbstractFacade<EcCultDetailCopy> implements  IEcCultDetailCopyBaseFacade, /*.ILocal, IEcCultDetailCopyBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(EcCultDetailCopyBaseFacade.class);

    public EcCultDetailCopyBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public EcCultDetailCopy initRow() {
        return new EcCultDetailCopy(getNextSequenceValue());
    }

    public void getTransientFields(List<EcCultDetailCopy> entities){
    }

    @Override
    public EcCultDetailCopy createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IEcCultCopyFacade.ILocal ecCultCopyFacade, Set<String> allowedFields) {
        EcCultDetailCopy ret = null;
        if (!json.containsKey("ecdcId") || json.get("ecdcId") == null)
            throw new GenericApplicationException("Error in EcCultDetailCopyBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("ecdcId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("ecdcId"));
                //ret = this.findByEcdcId(id);
                ret = getEntityManager().find(EcCultDetailCopy.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:EcCultDetailCopy:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("orderingNumber") && allowedFields.contains("orderingNumber")) {
            Long orderingNumber = (Long)json.get("orderingNumber");
            ret.setOrderingNumber(orderingNumber != null ? orderingNumber.intValue() : null);
        }
        if (json.containsKey("agreesDeclar") && allowedFields.contains("agreesDeclar")) {
            Long agreesDeclar = (Long)json.get("agreesDeclar");
            ret.setAgreesDeclar(agreesDeclar != null ? agreesDeclar.intValue() : null);
        }
        if (json.containsKey("comparisonOper") && allowedFields.contains("comparisonOper")) {
            Long comparisonOper = (Long)json.get("comparisonOper");
            ret.setComparisonOper(comparisonOper != null ? comparisonOper.intValue() : null);
        }
        if (json.containsKey("probabThres") && allowedFields.contains("probabThres")) {
            Object  obj = json.get("probabThres");
            Double probabThres = null;
            if (obj != null) {
                if (obj instanceof Double) {
                    probabThres = (Double)obj;
                } else if (obj instanceof Long) {
                    probabThres = ((Long)obj).doubleValue();
                } else {
                    throw new GenericApplicationException("Error in  CreateFromJson. probabThres with value '"+obj.toString()+"' cannot be converted to Double");
                }
            }
            ret.setProbabThres(probabThres != null ? new BigDecimal(probabThres.toString()) : null);
        }
        if (json.containsKey("decisionLight") && allowedFields.contains("decisionLight")) {
            Long decisionLight = (Long)json.get("decisionLight");
            ret.setDecisionLight(decisionLight != null ? decisionLight.intValue() : null);
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : EcCultDetailCopy");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("agreesDeclar2") && allowedFields.contains("agreesDeclar2")) {
            Long agreesDeclar2 = (Long)json.get("agreesDeclar2");
            ret.setAgreesDeclar2(agreesDeclar2 != null ? agreesDeclar2.intValue() : null);
        }
        if (json.containsKey("comparisonOper2") && allowedFields.contains("comparisonOper2")) {
            Long comparisonOper2 = (Long)json.get("comparisonOper2");
            ret.setComparisonOper2(comparisonOper2 != null ? comparisonOper2.intValue() : null);
        }
        if (json.containsKey("probabThres2") && allowedFields.contains("probabThres2")) {
            Object  obj = json.get("probabThres2");
            Double probabThres2 = null;
            if (obj != null) {
                if (obj instanceof Double) {
                    probabThres2 = (Double)obj;
                } else if (obj instanceof Long) {
                    probabThres2 = ((Long)obj).doubleValue();
                } else {
                    throw new GenericApplicationException("Error in  CreateFromJson. probabThres2 with value '"+obj.toString()+"' cannot be converted to Double");
                }
            }
            ret.setProbabThres2(probabThres2 != null ? new BigDecimal(probabThres2.toString()) : null);
        }
        if (json.containsKey("probabThresSum") && allowedFields.contains("probabThresSum")) {
            Object  obj = json.get("probabThresSum");
            Double probabThresSum = null;
            if (obj != null) {
                if (obj instanceof Double) {
                    probabThresSum = (Double)obj;
                } else if (obj instanceof Long) {
                    probabThresSum = ((Long)obj).doubleValue();
                } else {
                    throw new GenericApplicationException("Error in  CreateFromJson. probabThresSum with value '"+obj.toString()+"' cannot be converted to Double");
                }
            }
            ret.setProbabThresSum(probabThresSum != null ? new BigDecimal(probabThresSum.toString()) : null);
        }
        if (json.containsKey("comparisonOper3") && allowedFields.contains("comparisonOper3")) {
            Long comparisonOper3 = (Long)json.get("comparisonOper3");
            ret.setComparisonOper3(comparisonOper3 != null ? comparisonOper3.intValue() : null);
        }
        if (json.containsKey("ecccId") && allowedFields.contains("ecccId")) {
            JSONObject ecccId = (JSONObject)json.get("ecccId");
            if (ecccId == null) {
                ret.setEcccId(null);
            } else {
                if (!ecccId.containsKey("ecccId") || ecccId.get("ecccId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. ecccId is not null but PK is missing or null");
                } 
                EcCultCopy ecccId_db = null;
                String temp_id = (String)ecccId.get("ecccId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. ecccId.ecccId is negative but not in the keysMap dictionary");
                    }
                    ecccId_db = (EcCultCopy)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)ecccId.get("ecccId"));
                    ecccId_db = ecCultCopyFacade.findByEcccId(id);
                    if (ecccId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity EcCultCopy with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setEcccId(ecccId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.ec_cult_detail_copy_ecdc_id_seq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetailCopy> findAllByIds(List<Integer> ids) {

        List<EcCultDetailCopy> ret = (List<EcCultDetailCopy>) em.createQuery("SELECT x FROM EcCultDetailCopy x left join fetch x.ecccId x_ecccId left join fetch x_ecccId.ecgcId x_ecccId_ecgcId left join fetch x_ecccId_ecgcId.demaId x_ecccId_ecgcId_demaId  WHERE x.ecdcId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcCultDetailCopy x WHERE x.ecdcId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EcCultDetailCopy findByEcdcId(Integer ecdcId) {
        List<EcCultDetailCopy> results = (List<EcCultDetailCopy>) em.createQuery("SELECT x FROM EcCultDetailCopy x left join fetch x.ecccId x_ecccId left join fetch x_ecccId.ecgcId x_ecccId_ecgcId left join fetch x_ecccId_ecgcId.demaId x_ecccId_ecgcId_demaId  WHERE x.ecdcId = :ecdcId").
            setParameter("ecdcId", ecdcId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByEcdcId(Integer ecdcId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcCultDetailCopy x WHERE x.ecdcId = :ecdcId")
    .setParameter("ecdcId", ecdcId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy(Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy(false, ecccId_ecccId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy(boolean noTransient, Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCultDetailCopy x  left join fetch x.ecccId x_ecccId left join fetch x_ecccId.ecgcId x_ecccId_ecgcId left join fetch x_ecccId_ecgcId.demaId x_ecccId_ecgcId_demaId WHERE ((1=1) AND (x.ecccId.ecccId = :ecccId_ecccId)) ORDER BY x.orderingNumber";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecccId_ecccId", ecccId_ecccId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.ecdcId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.ecdcId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCultDetailCopy> ret = (List<EcCultDetailCopy>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy_count(Integer ecccId_ecccId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCultDetailCopy x WHERE ((1=1) AND (x.ecccId.ecccId = :ecccId_ecccId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecccId_ecccId", ecccId_ecccId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover(Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover(false, ecccId_ecccId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover(boolean noTransient, Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCultDetailCopy x  left join fetch x.ecccId x_ecccId left join fetch x_ecccId.ecgcId x_ecccId_ecgcId left join fetch x_ecccId_ecgcId.demaId x_ecccId_ecgcId_demaId WHERE ((1=1) AND (x.ecccId.ecccId = :ecccId_ecccId)) ORDER BY x.orderingNumber";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecccId_ecccId", ecccId_ecccId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.ecdcId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.ecdcId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCultDetailCopy> ret = (List<EcCultDetailCopy>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover_count(Integer ecccId_ecccId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCultDetailCopy x WHERE ((1=1) AND (x.ecccId.ecccId = :ecccId_ecccId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecccId_ecccId", ecccId_ecccId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper(Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper(false, ecccId_ecccId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper(boolean noTransient, Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCultDetailCopy x  left join fetch x.ecccId x_ecccId left join fetch x_ecccId.ecgcId x_ecccId_ecgcId left join fetch x_ecccId_ecgcId.demaId x_ecccId_ecgcId_demaId WHERE ((1=1) AND (x.ecccId.ecccId = :ecccId_ecccId)) ORDER BY x.orderingNumber";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecccId_ecccId", ecccId_ecccId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.ecdcId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.ecdcId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCultDetailCopy> ret = (List<EcCultDetailCopy>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper_count(Integer ecccId_ecccId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCultDetailCopy x WHERE ((1=1) AND (x.ecccId.ecccId = :ecccId_ecccId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecccId_ecccId", ecccId_ecccId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
