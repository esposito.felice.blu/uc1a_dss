package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class FmisDecisionBaseFacade extends AbstractFacade<FmisDecision> implements  IFmisDecisionBaseFacade, /*.ILocal, IFmisDecisionBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(FmisDecisionBaseFacade.class);

    public FmisDecisionBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public FmisDecision initRow() {
        return new FmisDecision(getNextSequenceValue());
    }

    public void getTransientFields(List<FmisDecision> entities){
    }

    @Override
    public FmisDecision createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal parcelsIssuesFacade, gr.neuropublic.Niva.facades.ICultivationFacade.ILocal cultivationFacade, gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade, Set<String> allowedFields) {
        FmisDecision ret = null;
        if (!json.containsKey("fmisDecisionsId") || json.get("fmisDecisionsId") == null)
            throw new GenericApplicationException("Error in FmisDecisionBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("fmisDecisionsId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("fmisDecisionsId"));
                //ret = this.findByFmisDecisionsId(id);
                ret = getEntityManager().find(FmisDecision.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:FmisDecision:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("cropOk") && allowedFields.contains("cropOk")) {
            Long cropOk = (Long)json.get("cropOk");
            ret.setCropOk(cropOk != null ? cropOk.shortValue() : null);
        }
        if (json.containsKey("landcoverOk") && allowedFields.contains("landcoverOk")) {
            Long landcoverOk = (Long)json.get("landcoverOk");
            ret.setLandcoverOk(landcoverOk != null ? landcoverOk.shortValue() : null);
        }
        if (json.containsKey("dteInsert") && allowedFields.contains("dteInsert")) {
            String dteInsert = (String)json.get("dteInsert");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteInsert);
                Date entityDate = ret.getDteInsert();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteInsert(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteInsert(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("usrInsert") && allowedFields.contains("usrInsert")) {
            ret.setUsrInsert((String)json.get("usrInsert"));
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : FmisDecision");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("parcelsIssuesId") && allowedFields.contains("parcelsIssuesId")) {
            JSONObject parcelsIssuesId = (JSONObject)json.get("parcelsIssuesId");
            if (parcelsIssuesId == null) {
                ret.setParcelsIssuesId(null);
            } else {
                if (!parcelsIssuesId.containsKey("parcelsIssuesId") || parcelsIssuesId.get("parcelsIssuesId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. parcelsIssuesId is not null but PK is missing or null");
                } 
                ParcelsIssues parcelsIssuesId_db = null;
                String temp_id = (String)parcelsIssuesId.get("parcelsIssuesId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. parcelsIssuesId.parcelsIssuesId is negative but not in the keysMap dictionary");
                    }
                    parcelsIssuesId_db = (ParcelsIssues)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)parcelsIssuesId.get("parcelsIssuesId"));
                    parcelsIssuesId_db = parcelsIssuesFacade.findByParcelsIssuesId(id);
                    if (parcelsIssuesId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity ParcelsIssues with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setParcelsIssuesId(parcelsIssuesId_db);
            }
        }
        if (json.containsKey("cultId") && allowedFields.contains("cultId")) {
            JSONObject cultId = (JSONObject)json.get("cultId");
            if (cultId == null) {
                ret.setCultId(null);
            } else {
                if (!cultId.containsKey("cultId") || cultId.get("cultId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cultId is not null but PK is missing or null");
                } 
                Cultivation cultId_db = null;
                String temp_id = (String)cultId.get("cultId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cultId.cultId is negative but not in the keysMap dictionary");
                    }
                    cultId_db = (Cultivation)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cultId.get("cultId"));
                    cultId_db = cultivationFacade.findByCultId(id);
                    if (cultId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Cultivation with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCultId(cultId_db);
            }
        }
        if (json.containsKey("cotyId") && allowedFields.contains("cotyId")) {
            JSONObject cotyId = (JSONObject)json.get("cotyId");
            if (cotyId == null) {
                ret.setCotyId(null);
            } else {
                if (!cotyId.containsKey("cotyId") || cotyId.get("cotyId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cotyId is not null but PK is missing or null");
                } 
                CoverType cotyId_db = null;
                String temp_id = (String)cotyId.get("cotyId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cotyId.cotyId is negative but not in the keysMap dictionary");
                    }
                    cotyId_db = (CoverType)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cotyId.get("cotyId"));
                    cotyId_db = coverTypeFacade.findByCotyId(id);
                    if (cotyId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity CoverType with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCotyId(cotyId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FmisDecision> findAllByIds(List<Integer> ids) {

        List<FmisDecision> ret = (List<FmisDecision>) em.createQuery("SELECT x FROM FmisDecision x left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId  WHERE x.fmisDecisionsId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM FmisDecision x WHERE x.fmisDecisionsId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public FmisDecision findByFmisDecisionsId(Integer fmisDecisionsId) {
        List<FmisDecision> results = (List<FmisDecision>) em.createQuery("SELECT x FROM FmisDecision x left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId  WHERE x.fmisDecisionsId = :fmisDecisionsId").
            setParameter("fmisDecisionsId", fmisDecisionsId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByFmisDecisionsId(Integer fmisDecisionsId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM FmisDecision x WHERE x.fmisDecisionsId = :fmisDecisionsId")
    .setParameter("fmisDecisionsId", fmisDecisionsId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FmisDecision> findAllByCriteriaRange_DashboardGrpFmisDecision(Integer parcelsIssuesId_parcelsIssuesId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DashboardGrpFmisDecision(false, parcelsIssuesId_parcelsIssuesId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FmisDecision> findAllByCriteriaRange_DashboardGrpFmisDecision(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM FmisDecision x  left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) ORDER BY x.dteInsert DESC ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.fmisDecisionsId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.fmisDecisionsId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<FmisDecision> ret = (List<FmisDecision>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DashboardGrpFmisDecision", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DashboardGrpFmisDecision_count(Integer parcelsIssuesId_parcelsIssuesId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM FmisDecision x WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DashboardGrpFmisDecision_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FmisDecision> findAllByCriteriaRange_ParcelFMISGrpFmisDecision(Integer parcelsIssuesId_parcelsIssuesId, Integer fsch_cultId_cultId, Integer fsch_cotyId_cotyId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_ParcelFMISGrpFmisDecision(false, parcelsIssuesId_parcelsIssuesId, fsch_cultId_cultId, fsch_cotyId_cotyId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FmisDecision> findAllByCriteriaRange_ParcelFMISGrpFmisDecision(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, Integer fsch_cultId_cultId, Integer fsch_cotyId_cotyId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM FmisDecision x  left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) AND (x.cultId = :fsch_cultId_cultId) AND (x.cotyId = :fsch_cotyId_cotyId) ORDER BY x.dteInsert DESC ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            if (fsch_cultId_cultId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_cultId_cultId");
            } else {
                paramSets.put("fsch_cultId_cultId", fsch_cultId_cultId);
            }

            if (fsch_cotyId_cotyId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_cotyId_cotyId");
            } else {
                paramSets.put("fsch_cotyId_cotyId", fsch_cotyId_cotyId);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.fmisDecisionsId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.fmisDecisionsId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<FmisDecision> ret = (List<FmisDecision>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelFMISGrpFmisDecision", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_ParcelFMISGrpFmisDecision_count(Integer parcelsIssuesId_parcelsIssuesId, Integer fsch_cultId_cultId, Integer fsch_cotyId_cotyId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM FmisDecision x WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) AND (x.cultId = :fsch_cultId_cultId) AND (x.cotyId = :fsch_cotyId_cotyId) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            if (fsch_cultId_cultId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_cultId_cultId");
            } else {
                paramSets.put("fsch_cultId_cultId", fsch_cultId_cultId);
            }

            if (fsch_cotyId_cotyId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_cotyId_cotyId");
            } else {
                paramSets.put("fsch_cotyId_cotyId", fsch_cotyId_cotyId);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelFMISGrpFmisDecision_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
