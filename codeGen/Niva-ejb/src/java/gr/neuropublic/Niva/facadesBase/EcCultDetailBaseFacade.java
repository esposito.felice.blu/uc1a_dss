package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class EcCultDetailBaseFacade extends AbstractFacade<EcCultDetail> implements  IEcCultDetailBaseFacade, /*.ILocal, IEcCultDetailBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(EcCultDetailBaseFacade.class);

    public EcCultDetailBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public EcCultDetail initRow() {
        return new EcCultDetail(getNextSequenceValue());
    }

    public void getTransientFields(List<EcCultDetail> entities){
    }

    @Override
    public EcCultDetail createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IEcCultFacade.ILocal ecCultFacade, Set<String> allowedFields) {
        EcCultDetail ret = null;
        if (!json.containsKey("eccdId") || json.get("eccdId") == null)
            throw new GenericApplicationException("Error in EcCultDetailBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("eccdId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("eccdId"));
                //ret = this.findByEccdId(id);
                ret = getEntityManager().find(EcCultDetail.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:EcCultDetail:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("orderingNumber") && allowedFields.contains("orderingNumber")) {
            Long orderingNumber = (Long)json.get("orderingNumber");
            ret.setOrderingNumber(orderingNumber != null ? orderingNumber.intValue() : null);
        }
        if (json.containsKey("agreesDeclar") && allowedFields.contains("agreesDeclar")) {
            Long agreesDeclar = (Long)json.get("agreesDeclar");
            ret.setAgreesDeclar(agreesDeclar != null ? agreesDeclar.intValue() : null);
        }
        if (json.containsKey("comparisonOper") && allowedFields.contains("comparisonOper")) {
            Long comparisonOper = (Long)json.get("comparisonOper");
            ret.setComparisonOper(comparisonOper != null ? comparisonOper.intValue() : null);
        }
        if (json.containsKey("probabThres") && allowedFields.contains("probabThres")) {
            Object  obj = json.get("probabThres");
            Double probabThres = null;
            if (obj != null) {
                if (obj instanceof Double) {
                    probabThres = (Double)obj;
                } else if (obj instanceof Long) {
                    probabThres = ((Long)obj).doubleValue();
                } else {
                    throw new GenericApplicationException("Error in  CreateFromJson. probabThres with value '"+obj.toString()+"' cannot be converted to Double");
                }
            }
            ret.setProbabThres(probabThres != null ? new BigDecimal(probabThres.toString()) : null);
        }
        if (json.containsKey("decisionLight") && allowedFields.contains("decisionLight")) {
            Long decisionLight = (Long)json.get("decisionLight");
            ret.setDecisionLight(decisionLight != null ? decisionLight.intValue() : null);
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : EcCultDetail");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("agreesDeclar2") && allowedFields.contains("agreesDeclar2")) {
            Long agreesDeclar2 = (Long)json.get("agreesDeclar2");
            ret.setAgreesDeclar2(agreesDeclar2 != null ? agreesDeclar2.intValue() : null);
        }
        if (json.containsKey("comparisonOper2") && allowedFields.contains("comparisonOper2")) {
            Long comparisonOper2 = (Long)json.get("comparisonOper2");
            ret.setComparisonOper2(comparisonOper2 != null ? comparisonOper2.intValue() : null);
        }
        if (json.containsKey("probabThres2") && allowedFields.contains("probabThres2")) {
            Object  obj = json.get("probabThres2");
            Double probabThres2 = null;
            if (obj != null) {
                if (obj instanceof Double) {
                    probabThres2 = (Double)obj;
                } else if (obj instanceof Long) {
                    probabThres2 = ((Long)obj).doubleValue();
                } else {
                    throw new GenericApplicationException("Error in  CreateFromJson. probabThres2 with value '"+obj.toString()+"' cannot be converted to Double");
                }
            }
            ret.setProbabThres2(probabThres2 != null ? new BigDecimal(probabThres2.toString()) : null);
        }
        if (json.containsKey("probabThresSum") && allowedFields.contains("probabThresSum")) {
            Object  obj = json.get("probabThresSum");
            Double probabThresSum = null;
            if (obj != null) {
                if (obj instanceof Double) {
                    probabThresSum = (Double)obj;
                } else if (obj instanceof Long) {
                    probabThresSum = ((Long)obj).doubleValue();
                } else {
                    throw new GenericApplicationException("Error in  CreateFromJson. probabThresSum with value '"+obj.toString()+"' cannot be converted to Double");
                }
            }
            ret.setProbabThresSum(probabThresSum != null ? new BigDecimal(probabThresSum.toString()) : null);
        }
        if (json.containsKey("comparisonOper3") && allowedFields.contains("comparisonOper3")) {
            Long comparisonOper3 = (Long)json.get("comparisonOper3");
            ret.setComparisonOper3(comparisonOper3 != null ? comparisonOper3.intValue() : null);
        }
        if (json.containsKey("eccuId") && allowedFields.contains("eccuId")) {
            JSONObject eccuId = (JSONObject)json.get("eccuId");
            if (eccuId == null) {
                ret.setEccuId(null);
            } else {
                if (!eccuId.containsKey("eccuId") || eccuId.get("eccuId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. eccuId is not null but PK is missing or null");
                } 
                EcCult eccuId_db = null;
                String temp_id = (String)eccuId.get("eccuId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. eccuId.eccuId is negative but not in the keysMap dictionary");
                    }
                    eccuId_db = (EcCult)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)eccuId.get("eccuId"));
                    eccuId_db = ecCultFacade.findByEccuId(id);
                    if (eccuId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity EcCult with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setEccuId(eccuId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetail> findAllByIds(List<Integer> ids) {

        List<EcCultDetail> ret = (List<EcCultDetail>) em.createQuery("SELECT x FROM EcCultDetail x left join fetch x.eccuId x_eccuId left join fetch x_eccuId.ecgrId x_eccuId_ecgrId  WHERE x.eccdId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcCultDetail x WHERE x.eccdId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EcCultDetail findByEccuId_OrderingNumber(EcCult eccuId, Integer orderingNumber) {
        List<EcCultDetail> results = (List<EcCultDetail>) em.createQuery("SELECT x FROM EcCultDetail x left join fetch x.eccuId x_eccuId left join fetch x_eccuId.ecgrId x_eccuId_ecgrId  WHERE x.eccuId.eccuId = :eccuId AND x.orderingNumber = :orderingNumber").
            setParameter("eccuId", eccuId!=null?eccuId.getEccuId():null).
            setParameter("orderingNumber", orderingNumber).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EcCultDetail findByEccuId_OrderingNumber(Integer eccuId, Integer orderingNumber) {
        List<EcCultDetail> results = (List<EcCultDetail>) em.createQuery("SELECT x FROM EcCultDetail x left join fetch x.eccuId x_eccuId left join fetch x_eccuId.ecgrId x_eccuId_ecgrId  WHERE x.eccuId.eccuId = :eccuId AND x.orderingNumber = :orderingNumber").
            setParameter("eccuId", eccuId).
            setParameter("orderingNumber", orderingNumber).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByEccuId_OrderingNumber(EcCult eccuId, Integer orderingNumber) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcCultDetail x WHERE x.eccuId.eccuId = :eccuId AND x.orderingNumber = :orderingNumber")
    .setParameter("eccuId", eccuId!=null?eccuId.getEccuId():null).
            setParameter("orderingNumber", orderingNumber);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    public int delByEccuId_OrderingNumber(Integer eccuId, Integer orderingNumber) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcCultDetail x WHERE x.eccuId.eccuId = :eccuId AND x.orderingNumber = :orderingNumber")
    .setParameter("eccuId", eccuId).
            setParameter("orderingNumber", orderingNumber);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EcCultDetail findByEccdId(Integer eccdId) {
        List<EcCultDetail> results = (List<EcCultDetail>) em.createQuery("SELECT x FROM EcCultDetail x left join fetch x.eccuId x_eccuId left join fetch x_eccuId.ecgrId x_eccuId_ecgrId  WHERE x.eccdId = :eccdId").
            setParameter("eccdId", eccdId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByEccdId(Integer eccdId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcCultDetail x WHERE x.eccdId = :eccdId")
    .setParameter("eccdId", eccdId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetail> findAllByCriteriaRange_EcGroupGrpEcCultDetail(Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_EcGroupGrpEcCultDetail(false, eccuId_eccuId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetail> findAllByCriteriaRange_EcGroupGrpEcCultDetail(boolean noTransient, Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCultDetail x  left join fetch x.eccuId x_eccuId left join fetch x_eccuId.ecgrId x_eccuId_ecgrId WHERE ((1=1) AND (x.eccuId.eccuId = :eccuId_eccuId)) ORDER BY x.orderingNumber";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("eccuId_eccuId", eccuId_eccuId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.eccdId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.eccdId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCultDetail> ret = (List<EcCultDetail>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupGrpEcCultDetail", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_EcGroupGrpEcCultDetail_count(Integer eccuId_eccuId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCultDetail x WHERE ((1=1) AND (x.eccuId.eccuId = :eccuId_eccuId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("eccuId_eccuId", eccuId_eccuId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupGrpEcCultDetail_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetail> findAllByCriteriaRange_EcGroupEcCultDetailCover(Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_EcGroupEcCultDetailCover(false, eccuId_eccuId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetail> findAllByCriteriaRange_EcGroupEcCultDetailCover(boolean noTransient, Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCultDetail x  left join fetch x.eccuId x_eccuId left join fetch x_eccuId.ecgrId x_eccuId_ecgrId WHERE ((1=1) AND (x.eccuId.eccuId = :eccuId_eccuId)) ORDER BY x.orderingNumber";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("eccuId_eccuId", eccuId_eccuId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.eccdId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.eccdId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCultDetail> ret = (List<EcCultDetail>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupEcCultDetailCover", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_EcGroupEcCultDetailCover_count(Integer eccuId_eccuId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCultDetail x WHERE ((1=1) AND (x.eccuId.eccuId = :eccuId_eccuId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("eccuId_eccuId", eccuId_eccuId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupEcCultDetailCover_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetail> findAllByCriteriaRange_EcGroupEcCultDetailSuper(Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_EcGroupEcCultDetailSuper(false, eccuId_eccuId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultDetail> findAllByCriteriaRange_EcGroupEcCultDetailSuper(boolean noTransient, Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCultDetail x  left join fetch x.eccuId x_eccuId left join fetch x_eccuId.ecgrId x_eccuId_ecgrId WHERE ((1=1) AND (x.eccuId.eccuId = :eccuId_eccuId)) ORDER BY x.orderingNumber";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("eccuId_eccuId", eccuId_eccuId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.eccdId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.eccdId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCultDetail> ret = (List<EcCultDetail>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupEcCultDetailSuper", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_EcGroupEcCultDetailSuper_count(Integer eccuId_eccuId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCultDetail x WHERE ((1=1) AND (x.eccuId.eccuId = :eccuId_eccuId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("eccuId_eccuId", eccuId_eccuId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupEcCultDetailSuper_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
