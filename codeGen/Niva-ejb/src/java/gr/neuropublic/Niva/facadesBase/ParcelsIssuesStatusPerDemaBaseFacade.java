package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class ParcelsIssuesStatusPerDemaBaseFacade extends AbstractFacade<ParcelsIssuesStatusPerDema> implements  IParcelsIssuesStatusPerDemaBaseFacade, /*.ILocal, IParcelsIssuesStatusPerDemaBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(ParcelsIssuesStatusPerDemaBaseFacade.class);

    public ParcelsIssuesStatusPerDemaBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public ParcelsIssuesStatusPerDema initRow() {
        return new ParcelsIssuesStatusPerDema(getNextSequenceValue());
    }

    public void getTransientFields(List<ParcelsIssuesStatusPerDema> entities){
    }

    @Override
    public ParcelsIssuesStatusPerDema createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal parcelsIssuesFacade, Set<String> allowedFields) {
        ParcelsIssuesStatusPerDema ret = null;
        if (!json.containsKey("parcelsIssuesStatusPerDemaId") || json.get("parcelsIssuesStatusPerDemaId") == null)
            throw new GenericApplicationException("Error in ParcelsIssuesStatusPerDemaBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("parcelsIssuesStatusPerDemaId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("parcelsIssuesStatusPerDemaId"));
                //ret = this.findByParcelsIssuesStatusPerDemaId(id);
                ret = getEntityManager().find(ParcelsIssuesStatusPerDema.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:ParcelsIssuesStatusPerDema:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("status") && allowedFields.contains("status")) {
            Long status = (Long)json.get("status");
            ret.setStatus(status != null ? status.shortValue() : null);
        }
        if (json.containsKey("dteStatusUpdate") && allowedFields.contains("dteStatusUpdate")) {
            String dteStatusUpdate = (String)json.get("dteStatusUpdate");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteStatusUpdate);
                Date entityDate = ret.getDteStatusUpdate();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteStatusUpdate(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteStatusUpdate(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : ParcelsIssuesStatusPerDema");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("parcelsIssuesId") && allowedFields.contains("parcelsIssuesId")) {
            JSONObject parcelsIssuesId = (JSONObject)json.get("parcelsIssuesId");
            if (parcelsIssuesId == null) {
                ret.setParcelsIssuesId(null);
            } else {
                if (!parcelsIssuesId.containsKey("parcelsIssuesId") || parcelsIssuesId.get("parcelsIssuesId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. parcelsIssuesId is not null but PK is missing or null");
                } 
                ParcelsIssues parcelsIssuesId_db = null;
                String temp_id = (String)parcelsIssuesId.get("parcelsIssuesId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. parcelsIssuesId.parcelsIssuesId is negative but not in the keysMap dictionary");
                    }
                    parcelsIssuesId_db = (ParcelsIssues)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)parcelsIssuesId.get("parcelsIssuesId"));
                    parcelsIssuesId_db = parcelsIssuesFacade.findByParcelsIssuesId(id);
                    if (parcelsIssuesId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity ParcelsIssues with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setParcelsIssuesId(parcelsIssuesId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssuesStatusPerDema> findAllByIds(List<Integer> ids) {

        List<ParcelsIssuesStatusPerDema> ret = (List<ParcelsIssuesStatusPerDema>) em.createQuery("SELECT x FROM ParcelsIssuesStatusPerDema x   WHERE x.parcelsIssuesStatusPerDemaId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM ParcelsIssuesStatusPerDema x WHERE x.parcelsIssuesStatusPerDemaId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ParcelsIssuesStatusPerDema findByParcelsIssuesStatusPerDemaId(Integer parcelsIssuesStatusPerDemaId) {
        List<ParcelsIssuesStatusPerDema> results = (List<ParcelsIssuesStatusPerDema>) em.createQuery("SELECT x FROM ParcelsIssuesStatusPerDema x   WHERE x.parcelsIssuesStatusPerDemaId = :parcelsIssuesStatusPerDemaId").
            setParameter("parcelsIssuesStatusPerDemaId", parcelsIssuesStatusPerDemaId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByParcelsIssuesStatusPerDemaId(Integer parcelsIssuesStatusPerDemaId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM ParcelsIssuesStatusPerDema x WHERE x.parcelsIssuesStatusPerDemaId = :parcelsIssuesStatusPerDemaId")
    .setParameter("parcelsIssuesStatusPerDemaId", parcelsIssuesStatusPerDemaId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
