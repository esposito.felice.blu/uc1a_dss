package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class GpRequestsContextsBaseFacade extends AbstractFacade<GpRequestsContexts> implements  IGpRequestsContextsBaseFacade, /*.ILocal, IGpRequestsContextsBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(GpRequestsContextsBaseFacade.class);

    public GpRequestsContextsBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public GpRequestsContexts initRow() {
        return new GpRequestsContexts(getNextSequenceValue());
    }

    public void getTransientFields(List<GpRequestsContexts> entities){
    }

    @Override
    public GpRequestsContexts createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IGpRequestsProducersFacade.ILocal gpRequestsProducersFacade, gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal parcelsIssuesFacade, Set<String> allowedFields) {
        GpRequestsContexts ret = null;
        if (!json.containsKey("gpRequestsContextsId") || json.get("gpRequestsContextsId") == null)
            throw new GenericApplicationException("Error in GpRequestsContextsBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("gpRequestsContextsId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("gpRequestsContextsId"));
                //ret = this.findByGpRequestsContextsId(id);
                ret = getEntityManager().find(GpRequestsContexts.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:GpRequestsContexts:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("type") && allowedFields.contains("type")) {
            ret.setType((String)json.get("type"));
        }
        if (json.containsKey("label") && allowedFields.contains("label")) {
            ret.setLabel((String)json.get("label"));
        }
        if (json.containsKey("comment") && allowedFields.contains("comment")) {
            ret.setComment((String)json.get("comment"));
        }
        if (json.containsKey("geomHexewkb") && allowedFields.contains("geomHexewkb")) {
            ret.setGeomHexewkb((String)json.get("geomHexewkb"));
        }
        if (json.containsKey("referencepoint") && allowedFields.contains("referencepoint")) {
            ret.setReferencepoint((String)json.get("referencepoint"));
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : GpRequestsContexts");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("hash") && allowedFields.contains("hash")) {
            ret.setHash((String)json.get("hash"));
        }
        if (json.containsKey("gpRequestsProducersId") && allowedFields.contains("gpRequestsProducersId")) {
            JSONObject gpRequestsProducersId = (JSONObject)json.get("gpRequestsProducersId");
            if (gpRequestsProducersId == null) {
                ret.setGpRequestsProducersId(null);
            } else {
                if (!gpRequestsProducersId.containsKey("gpRequestsProducersId") || gpRequestsProducersId.get("gpRequestsProducersId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. gpRequestsProducersId is not null but PK is missing or null");
                } 
                GpRequestsProducers gpRequestsProducersId_db = null;
                String temp_id = (String)gpRequestsProducersId.get("gpRequestsProducersId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. gpRequestsProducersId.gpRequestsProducersId is negative but not in the keysMap dictionary");
                    }
                    gpRequestsProducersId_db = (GpRequestsProducers)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)gpRequestsProducersId.get("gpRequestsProducersId"));
                    gpRequestsProducersId_db = gpRequestsProducersFacade.findByGpRequestsProducersId(id);
                    if (gpRequestsProducersId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity GpRequestsProducers with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setGpRequestsProducersId(gpRequestsProducersId_db);
            }
        }
        if (json.containsKey("parcelsIssuesId") && allowedFields.contains("parcelsIssuesId")) {
            JSONObject parcelsIssuesId = (JSONObject)json.get("parcelsIssuesId");
            if (parcelsIssuesId == null) {
                ret.setParcelsIssuesId(null);
            } else {
                if (!parcelsIssuesId.containsKey("parcelsIssuesId") || parcelsIssuesId.get("parcelsIssuesId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. parcelsIssuesId is not null but PK is missing or null");
                } 
                ParcelsIssues parcelsIssuesId_db = null;
                String temp_id = (String)parcelsIssuesId.get("parcelsIssuesId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. parcelsIssuesId.parcelsIssuesId is negative but not in the keysMap dictionary");
                    }
                    parcelsIssuesId_db = (ParcelsIssues)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)parcelsIssuesId.get("parcelsIssuesId"));
                    parcelsIssuesId_db = parcelsIssuesFacade.findByParcelsIssuesId(id);
                    if (parcelsIssuesId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity ParcelsIssues with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setParcelsIssuesId(parcelsIssuesId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpRequestsContexts> findAllByIds(List<Integer> ids) {

        List<GpRequestsContexts> ret = (List<GpRequestsContexts>) em.createQuery("SELECT x FROM GpRequestsContexts x left join fetch x.gpRequestsProducersId x_gpRequestsProducersId left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId  WHERE x.gpRequestsContextsId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpRequestsContexts x WHERE x.gpRequestsContextsId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public GpRequestsContexts findByHash(String hash) {
        List<GpRequestsContexts> results = (List<GpRequestsContexts>) em.createQuery("SELECT x FROM GpRequestsContexts x left join fetch x.gpRequestsProducersId x_gpRequestsProducersId left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId  WHERE x.hash = :hash").
            setParameter("hash", hash).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByHash(String hash) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpRequestsContexts x WHERE x.hash = :hash")
    .setParameter("hash", hash);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public GpRequestsContexts findByGpRequestsContextsId(Integer gpRequestsContextsId) {
        List<GpRequestsContexts> results = (List<GpRequestsContexts>) em.createQuery("SELECT x FROM GpRequestsContexts x left join fetch x.gpRequestsProducersId x_gpRequestsProducersId left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId  WHERE x.gpRequestsContextsId = :gpRequestsContextsId").
            setParameter("gpRequestsContextsId", gpRequestsContextsId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByGpRequestsContextsId(Integer gpRequestsContextsId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpRequestsContexts x WHERE x.gpRequestsContextsId = :gpRequestsContextsId")
    .setParameter("gpRequestsContextsId", gpRequestsContextsId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpRequestsContexts> findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts(Integer parcelsIssuesId_parcelsIssuesId, String fsch_hash, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts(false, parcelsIssuesId_parcelsIssuesId, fsch_hash, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpRequestsContexts> findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, String fsch_hash, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM GpRequestsContexts x  left join fetch x.gpRequestsProducersId x_gpRequestsProducersId left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) AND (x.hash like :fsch_hash) ORDER BY x.type ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            if (fsch_hash == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_hash");
            } else {
                paramSets.put("fsch_hash", fsch_hash+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.gpRequestsContextsId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.gpRequestsContextsId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<GpRequestsContexts> ret = (List<GpRequestsContexts>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts_count(Integer parcelsIssuesId_parcelsIssuesId, String fsch_hash, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM GpRequestsContexts x WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) AND (x.hash like :fsch_hash) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            if (fsch_hash == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_hash");
            } else {
                paramSets.put("fsch_hash", fsch_hash+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpRequestsContexts> findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts(Integer parcelsIssuesId_parcelsIssuesId, String fsch_type, String fsch_label, String fsch_comment, String fsch_geomHexewkb, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts(false, parcelsIssuesId_parcelsIssuesId, fsch_type, fsch_label, fsch_comment, fsch_geomHexewkb, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpRequestsContexts> findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, String fsch_type, String fsch_label, String fsch_comment, String fsch_geomHexewkb, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM GpRequestsContexts x  left join fetch x.gpRequestsProducersId x_gpRequestsProducersId left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) AND (x.type like :fsch_type) AND (x.label like :fsch_label) AND (x.comment like :fsch_comment) AND (x.geomHexewkb like :fsch_geomHexewkb) ORDER BY x.type, x.gpRequestsContextsId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            if (fsch_type == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_type");
            } else {
                paramSets.put("fsch_type", fsch_type+"%");
            }

            if (fsch_label == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_label");
            } else {
                paramSets.put("fsch_label", fsch_label+"%");
            }

            if (fsch_comment == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_comment");
            } else {
                paramSets.put("fsch_comment", fsch_comment+"%");
            }

            if (fsch_geomHexewkb == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_geomHexewkb");
            } else {
                paramSets.put("fsch_geomHexewkb", fsch_geomHexewkb+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.gpRequestsContextsId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.gpRequestsContextsId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<GpRequestsContexts> ret = (List<GpRequestsContexts>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts_count(Integer parcelsIssuesId_parcelsIssuesId, String fsch_type, String fsch_label, String fsch_comment, String fsch_geomHexewkb, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM GpRequestsContexts x WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) AND (x.type like :fsch_type) AND (x.label like :fsch_label) AND (x.comment like :fsch_comment) AND (x.geomHexewkb like :fsch_geomHexewkb) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            if (fsch_type == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_type");
            } else {
                paramSets.put("fsch_type", fsch_type+"%");
            }

            if (fsch_label == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_label");
            } else {
                paramSets.put("fsch_label", fsch_label+"%");
            }

            if (fsch_comment == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_comment");
            } else {
                paramSets.put("fsch_comment", fsch_comment+"%");
            }

            if (fsch_geomHexewkb == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_geomHexewkb");
            } else {
                paramSets.put("fsch_geomHexewkb", fsch_geomHexewkb+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
