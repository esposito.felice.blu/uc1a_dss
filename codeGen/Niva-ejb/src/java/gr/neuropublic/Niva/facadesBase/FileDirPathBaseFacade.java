package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class FileDirPathBaseFacade extends AbstractFacade<FileDirPath> implements  IFileDirPathBaseFacade, /*.ILocal, IFileDirPathBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(FileDirPathBaseFacade.class);

    public FileDirPathBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public FileDirPath initRow() {
        return new FileDirPath(getNextSequenceValue());
    }

    public void getTransientFields(List<FileDirPath> entities){
    }

    @Override
    public FileDirPath createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, Set<String> allowedFields) {
        FileDirPath ret = null;
        if (!json.containsKey("fidrId") || json.get("fidrId") == null)
            throw new GenericApplicationException("Error in FileDirPathBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("fidrId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("fidrId"));
                //ret = this.findByFidrId(id);
                ret = getEntityManager().find(FileDirPath.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:FileDirPath:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("directory") && allowedFields.contains("directory")) {
            ret.setDirectory((String)json.get("directory"));
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : FileDirPath");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("fileType") && allowedFields.contains("fileType")) {
            Long fileType = (Long)json.get("fileType");
            ret.setFileType(fileType != null ? fileType.intValue() : null);
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.file_dir_path_fidr_id_seq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FileDirPath> findAllByIds(List<Integer> ids) {

        List<FileDirPath> ret = (List<FileDirPath>) em.createQuery("SELECT x FROM FileDirPath x   WHERE x.fidrId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM FileDirPath x WHERE x.fidrId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public FileDirPath findByFileType(Integer fileType) {
        List<FileDirPath> results = (List<FileDirPath>) em.createQuery("SELECT x FROM FileDirPath x   WHERE x.fileType = :fileType").
            setParameter("fileType", fileType).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByFileType(Integer fileType) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM FileDirPath x WHERE x.fileType = :fileType")
    .setParameter("fileType", fileType);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public FileDirPath findByFidrId(Integer fidrId) {
        List<FileDirPath> results = (List<FileDirPath>) em.createQuery("SELECT x FROM FileDirPath x   WHERE x.fidrId = :fidrId").
            setParameter("fidrId", fidrId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByFidrId(Integer fidrId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM FileDirPath x WHERE x.fidrId = :fidrId")
    .setParameter("fidrId", fidrId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FileDirPath> findAllByCriteriaRange_FileDirPathGrpFileDirPath(int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_FileDirPathGrpFileDirPath(false, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FileDirPath> findAllByCriteriaRange_FileDirPathGrpFileDirPath(boolean noTransient, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM FileDirPath x   WHERE (1=1) ORDER BY x.directory";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.fidrId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.fidrId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<FileDirPath> ret = (List<FileDirPath>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_FileDirPathGrpFileDirPath", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_FileDirPathGrpFileDirPath_count(List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM FileDirPath x WHERE (1=1) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();

            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_FileDirPathGrpFileDirPath_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
