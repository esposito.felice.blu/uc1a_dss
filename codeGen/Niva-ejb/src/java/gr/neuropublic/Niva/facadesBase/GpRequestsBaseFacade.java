package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class GpRequestsBaseFacade extends AbstractFacade<GpRequests> implements  IGpRequestsBaseFacade, /*.ILocal, IGpRequestsBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(GpRequestsBaseFacade.class);

    public GpRequestsBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public GpRequests initRow() {
        return new GpRequests(getNextSequenceValue());
    }

    public void getTransientFields(List<GpRequests> entities){
    }

    @Override
    public GpRequests createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IAgrisnapUsersFacade.ILocal agrisnapUsersFacade, Set<String> allowedFields) {
        GpRequests ret = null;
        if (!json.containsKey("gpRequestsId") || json.get("gpRequestsId") == null)
            throw new GenericApplicationException("Error in GpRequestsBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("gpRequestsId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("gpRequestsId"));
                //ret = this.findByGpRequestsId(id);
                ret = getEntityManager().find(GpRequests.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:GpRequests:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("dteReceived") && allowedFields.contains("dteReceived")) {
            String dteReceived = (String)json.get("dteReceived");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteReceived);
                Date entityDate = ret.getDteReceived();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteReceived(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteReceived(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("typeCode") && allowedFields.contains("typeCode")) {
            Long typeCode = (Long)json.get("typeCode");
            ret.setTypeCode(typeCode != null ? typeCode.shortValue() : null);
        }
        if (json.containsKey("typeDescription") && allowedFields.contains("typeDescription")) {
            ret.setTypeDescription((String)json.get("typeDescription"));
        }
        if (json.containsKey("scheme") && allowedFields.contains("scheme")) {
            ret.setScheme((String)json.get("scheme"));
        }
        if (json.containsKey("correspondenceId") && allowedFields.contains("correspondenceId")) {
            Long correspondenceId = (Long)json.get("correspondenceId");
            ret.setCorrespondenceId(correspondenceId != null ? correspondenceId.intValue() : null);
        }
        if (json.containsKey("correspondenceDocNr") && allowedFields.contains("correspondenceDocNr")) {
            Long correspondenceDocNr = (Long)json.get("correspondenceDocNr");
            ret.setCorrespondenceDocNr(correspondenceDocNr != null ? correspondenceDocNr.intValue() : null);
        }
        if (json.containsKey("geotag") && allowedFields.contains("geotag")) {
            ret.setGeotag((String)json.get("geotag"));
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : GpRequests");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("agrisnapUsersId") && allowedFields.contains("agrisnapUsersId")) {
            JSONObject agrisnapUsersId = (JSONObject)json.get("agrisnapUsersId");
            if (agrisnapUsersId == null) {
                ret.setAgrisnapUsersId(null);
            } else {
                if (!agrisnapUsersId.containsKey("agrisnapUsersId") || agrisnapUsersId.get("agrisnapUsersId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. agrisnapUsersId is not null but PK is missing or null");
                } 
                AgrisnapUsers agrisnapUsersId_db = null;
                String temp_id = (String)agrisnapUsersId.get("agrisnapUsersId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. agrisnapUsersId.agrisnapUsersId is negative but not in the keysMap dictionary");
                    }
                    agrisnapUsersId_db = (AgrisnapUsers)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)agrisnapUsersId.get("agrisnapUsersId"));
                    agrisnapUsersId_db = agrisnapUsersFacade.findByAgrisnapUsersId(id);
                    if (agrisnapUsersId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity AgrisnapUsers with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setAgrisnapUsersId(agrisnapUsersId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpRequests> findAllByIds(List<Integer> ids) {

        List<GpRequests> ret = (List<GpRequests>) em.createQuery("SELECT x FROM GpRequests x   WHERE x.gpRequestsId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpRequests x WHERE x.gpRequestsId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public GpRequests findByGpRequestsId(Integer gpRequestsId) {
        List<GpRequests> results = (List<GpRequests>) em.createQuery("SELECT x FROM GpRequests x   WHERE x.gpRequestsId = :gpRequestsId").
            setParameter("gpRequestsId", gpRequestsId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByGpRequestsId(Integer gpRequestsId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpRequests x WHERE x.gpRequestsId = :gpRequestsId")
    .setParameter("gpRequestsId", gpRequestsId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
