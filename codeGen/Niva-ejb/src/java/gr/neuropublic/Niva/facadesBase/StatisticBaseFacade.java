package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class StatisticBaseFacade extends AbstractFacade<Statistic> implements  IStatisticBaseFacade, /*.ILocal, IStatisticBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(StatisticBaseFacade.class);

    public StatisticBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Statistic initRow() {
        return new Statistic(getNextSequenceValue());
    }

    public void getTransientFields(List<Statistic> entities){
    }

    @Override
    public Statistic createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IClassificationFacade.ILocal classificationFacade, Set<String> allowedFields) {
        Statistic ret = null;
        if (!json.containsKey("statId") || json.get("statId") == null)
            throw new GenericApplicationException("Error in StatisticBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("statId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("statId"));
                //ret = this.findByStatId(id);
                ret = getEntityManager().find(Statistic.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:Statistic:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("name") && allowedFields.contains("name")) {
            ret.setName((String)json.get("name"));
        }
        if (json.containsKey("description") && allowedFields.contains("description")) {
            ret.setDescription((String)json.get("description"));
        }
        if (json.containsKey("value") && allowedFields.contains("value")) {
            Long value = (Long)json.get("value");
            ret.setValue(value != null ? value.intValue() : null);
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : Statistic");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("clasId") && allowedFields.contains("clasId")) {
            JSONObject clasId = (JSONObject)json.get("clasId");
            if (clasId == null) {
                ret.setClasId(null);
            } else {
                if (!clasId.containsKey("clasId") || clasId.get("clasId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. clasId is not null but PK is missing or null");
                } 
                Classification clasId_db = null;
                String temp_id = (String)clasId.get("clasId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. clasId.clasId is negative but not in the keysMap dictionary");
                    }
                    clasId_db = (Classification)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)clasId.get("clasId"));
                    clasId_db = classificationFacade.findByClasId(id);
                    if (clasId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Classification with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setClasId(clasId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Statistic> findAllByIds(List<Integer> ids) {

        List<Statistic> ret = (List<Statistic>) em.createQuery("SELECT x FROM Statistic x   WHERE x.statId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Statistic x WHERE x.statId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Statistic findByStatId(Integer statId) {
        List<Statistic> results = (List<Statistic>) em.createQuery("SELECT x FROM Statistic x   WHERE x.statId = :statId").
            setParameter("statId", statId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByStatId(Integer statId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Statistic x WHERE x.statId = :statId")
    .setParameter("statId", statId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
