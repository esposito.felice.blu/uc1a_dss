package gr.neuropublic.Niva.servicesBase;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.functional.Action;

import gr.neuropublic.rtlEntities.RTL_Subscriber;

public abstract class UserManagementServiceBase  extends gr.neuropublic.base.UserManagementService implements  gr.neuropublic.Niva.servicesBase.IUserManagementServiceBase {

    @Override
    protected gr.neuropublic.Niva.services.UserSession createUserSession(final Integer userId, String usrEmail, String userVat, Integer subsId, String subsCode, String subsDescription, Integer fromSubsId, String appName, List<String> privileges, List<Integer> subSecClasses, String userSalt, String clientIp) {
        final gr.neuropublic.Niva.services.UserSession userSession = new gr.neuropublic.Niva.services.UserSession(userId, usrEmail, userVat, subsId, subsCode, subsDescription, fromSubsId, privileges, subSecClasses, userSalt, clientIp);

        if (userId != null) {
            saveToDBGeneric(new Action() {@Override public void lambda() {
                String sqlQuery =   "SELECT s.user_active_email " +
                                    "  FROM subscription.ss_users s" +
                                    " WHERE s.user_id = :userId ";
                Query query = getEntityManager().createNativeQuery(sqlQuery);
                query.setParameter("userId", userId);
                List<String> users = query.getResultList();
                if (users.isEmpty()){
                    throw new GenericApplicationException("User not found");            
                }
                if (users.size() > 1) {
                    throw new GenericApplicationException("Two many users found");
                }
                userSession.setUsrActiveEmail(users.get(0));
            }});

            gr.neuropublic.base.User user = getUserById(userId);
            userSession.setUserLoginName(user != null ? user.publicUserName : usrEmail);
        }
        return userSession;
    }

    @Override
    public String getAppName() {
        return "Niva";
    }


/************* RTL_Subscriber services START **********************************/
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<RTL_Subscriber> findByCode_Login_subscriber(String shortName) 
    {
        try {
            String jpql = "SELECT x FROM RTL_Subscriber x WHERE (x.shortName = :shortName) ORDER BY x.shortName";

            Map<String,Object> paramSets = new HashMap<>();


            jpql = jpql.replace('{', '(').replace('}', ')');

            Query q = getEntityManager().createQuery(jpql);
            q.setParameter("shortName", shortName); 

            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }

            q.setMaxResults(2);
            List<RTL_Subscriber> ret = (List<RTL_Subscriber>)q.getResultList();

            return ret;
        } catch (Exception e) {
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<RTL_Subscriber> findAllByCriteriaRange_Login_subscriber(String srch_subs_short_name, String srch_subs_vat, String srch_subs_legal_name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM RTL_Subscriber x WHERE (1=1) AND (x.shortName like :srch_subs_short_name) AND (x.vat like :srch_subs_vat) AND (x.legalName like :srch_subs_legal_name) AND (x.id > 10 or x.id = 0) ORDER BY x.shortName, x.id ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (srch_subs_short_name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":srch_subs_short_name");
            } else {
                paramSets.put("srch_subs_short_name", srch_subs_short_name+"%");
            }

            if (srch_subs_vat == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":srch_subs_vat");
            } else {
                paramSets.put("srch_subs_vat", srch_subs_vat+"%");
            }

            if (srch_subs_legal_name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":srch_subs_legal_name");
            } else {
                paramSets.put("srch_subs_legal_name", srch_subs_legal_name+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.id");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.id" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = getEntityManager().createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<RTL_Subscriber> ret = (List<RTL_Subscriber>)q.getResultList();
            return ret;
        } catch (Exception e) {
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_Login_subscriber_count(String srch_subs_short_name, String srch_subs_vat, String srch_subs_legal_name, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM RTL_Subscriber x WHERE (1=1) AND (x.shortName like :srch_subs_short_name) AND (x.vat like :srch_subs_vat) AND (x.legalName like :srch_subs_legal_name) AND (x.id > 10 or x.id = 0) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (srch_subs_short_name == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":srch_subs_short_name");
            } else {
                paramSets.put("srch_subs_short_name", srch_subs_short_name+"%");
            }

            if (srch_subs_vat == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":srch_subs_vat");
            } else {
                paramSets.put("srch_subs_vat", srch_subs_vat+"%");
            }

            if (srch_subs_legal_name == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":srch_subs_legal_name");
            } else {
                paramSets.put("srch_subs_legal_name", srch_subs_legal_name+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }
/************* RTL_Subscriber services END **********************************/         
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
