package gr.neuropublic.Niva.servicesBase;

public abstract  class SessionsCacheServiceBase  extends gr.neuropublic.base.SessionsCache implements  gr.neuropublic.Niva.servicesBase.ISessionsCacheServiceBase {
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
