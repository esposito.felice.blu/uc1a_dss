//5A0F85CC92411D0AB151CE07E5DE01E9

package gr.neuropublic.Niva.services;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.JSONArray;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.util.GeometryTransformer;
import java.util.logging.Level;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.sql.Timestamp;
import java.util.Collection;
import java.io.File;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FeatureSource;
import org.geotools.feature.FeatureCollection;
import org.opengis.feature.Feature;
import org.opengis.feature.GeometryAttribute;
import org.geotools.feature.FeatureIterator;

import org.opengis.feature.Property;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.swing.plaf.basic.BasicToggleButtonUI;

import java.sql.Connection;
import java.sql.SQLException;
import gr.neuropublic.base.NamedParameterPreparedStatement;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.FileUtils;
import org.geotools.data.DataAccessFactory;

import org.geotools.referencing.CRS;
//import org.geotools.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.geotools.geometry.jts.JTS;
//import org.opengis.feature.simple.SimpleFeatureType;
//import org.opengis.feature.simple.SimpleFeatureCollection;
import org.opengis.feature.type.FeatureType;

// imports for geom
import com.vividsolutions.jts.geom.GeometryCollection;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.data.store.ReprojectingFeatureCollection;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

import java.nio.charset.StandardCharsets;
import org.apache.commons.codec.binary.Base64;

import org.opengis.referencing.ReferenceIdentifier;

@Stateless
@Local(IMainService.ILocal.class)
@Remote(IMainService.IRemote.class)
public class MainService extends gr.neuropublic.Niva.servicesBase.MainServiceBase
        implements IMainService.ILocal, IMainService.IRemote {

    // NEUROPUBLIC / MT 202109

    private final Logger logger = LoggerFactory.getLogger(MainService.class);

    public List<String> finalizationEcGroup(Integer ecgrId, UserSession usrSession) {

        EcGroup ecg = getEcGroupFacade().findByEcgrId(ecgrId);

        ecg.setDteupdate(new Timestamp(new Date().getTime()));
        ecg.setUsrupdate(usrSession.usrEmail);
        ecg.setRecordtype(1);

        SaveResponse ret = new SaveResponse();
        List<String> returnWarnings = new ArrayList<>();
        returnWarnings.add(ret.warningMessages);

        return returnWarnings;

    }

    /*
     * Performs the importing for the selected classification entity.
     */
    public List<String> importingClassification(Integer classId, UserSession usrSession) {

        Classification clas = getClassificationFacade().findByClasId(classId);

        // FINALIZE LOCKING THE UI
        // clas.setRecordtype(1);
        clas.setDteupdate(new Timestamp(new Date().getTime()));
        clas.setUsrupdate(usrSession.usrEmail);

        List<TemplateColumn> templateCollection = clas.getFiteId().getTemplateColumnCollection();

        EntityManager em = getEntityManager();
        List<Cultivation> results = (List<Cultivation>) em.createQuery("SELECT x FROM Cultivation x ").getResultList();
        HashMap<Integer, Integer> cultMap = new HashMap<>();

        for (Cultivation row : results) {
            cultMap.put(row.getCode(), row.getCultId());

        }

        // Map to store the system(DB) column name with value the imported file column
        // name
        HashMap<String, String> column_mapping = new HashMap<>();
        for (TemplateColumn templateCol : templateCollection) {
            column_mapping.put(templateCol.getPrcoId().getSystemColumnName(), templateCol.getClfierName());
        }

        // add
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(clas.getDateTime());
        int year = calendar.get(Calendar.YEAR);

        String directoryName = ""; // (String) query.getSingleResult();
        try {
            Path tempDirWithPrefix = Files.createTempDirectory("temp");
            directoryName = tempDirWithPrefix.toString();
            // System.out.println("Temp dir is :" + directoryName);
        } catch (IOException ex) {
            throw new GenericApplicationException(ex);
        }

        File rootFile = new File(directoryName);

        byte[] zippedShapeFile = clas.getAttachedFile();
        String shapeFileName = "";
        try {
            shapeFileName = unzipFile(zippedShapeFile, rootFile);
        } catch (IOException ex) {
            throw new GenericApplicationException(ex);
        }

        // just check if is shape file - ends with .shp
        if (shapeFileName.toLowerCase().endsWith(".shp")) {

            // check if filename (clas.getFilePath()) that user selects at ui form exists at
            // folder files
            boolean result = listFileTree(rootFile).toString().contains(shapeFileName);

            if (result) {
                for (int i = 0; i < listFileTree(rootFile).size(); i++) {
                    // the name of file (shapefile) that will parced: shapeFileName
                    String uploadedFileLocation = listFileTree(rootFile).toArray()[i].toString();

                    File file = new File(uploadedFileLocation);

                    if (uploadedFileLocation.endsWith(shapeFileName)) {
                        // System.out.println("File to be imported is = " + file);
                        // when we find the file then excecute the import (readInsertShp)
                        try {
                            readInsertShp(file, column_mapping, year, clas.getClasId(), cultMap);
                            clas.setIsImported(true);
                        } catch (IOException ex) {
                            clas.setIsImported(false);
                            java.util.logging.Logger.getLogger(MainService.class.getName()).log(Level.SEVERE, null, ex);
                            throw new GenericApplicationException(ex);
                        } catch (NamingException | SQLException ex) {
                            clas.setIsImported(false);
                            java.util.logging.Logger.getLogger(MainService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                }
            } else {
                throw new GenericApplicationException("Shp File Not Found.");
            }
        } else {
            throw new GenericApplicationException(
                    "Type of file not supported, only Shp files contained by zip ones are supported.");
        }

        try {
            FileUtils.deleteDirectory(new File(directoryName));
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(MainService.class.getName()).log(Level.SEVERE, null, ex);
        }

        SaveResponse ret = new SaveResponse();
        List<String> returnWarnings = new ArrayList<>();
        returnWarnings.add(ret.warningMessages);
        return returnWarnings;

    }

    private String unzipFile(byte[] zippedShapeFile, File destDir) throws FileNotFoundException, IOException {

        String shapeFileName = "";
        try (ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(zippedShapeFile))) {
            ZipEntry zipEntry = zis.getNextEntry();
            byte[] buffer = new byte[1024];

            while (zipEntry != null) {
                if (zipEntry.getName().toLowerCase().endsWith(".shp"))

                    shapeFileName = zipEntry.getName();

                File newFile = newFilef(destDir, zipEntry);

                if (zipEntry.isDirectory()) {
                    if (!newFile.isDirectory() && !newFile.mkdirs()) {
                        throw new IOException("Failed to create directory " + newFile);
                    }
                } else {
                    // fix for Windows-created archives
                    File parent = newFile.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("Failed to create directory " + parent);
                    }

                    // write file content
                    FileOutputStream fos = new FileOutputStream(newFile);
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                    fos.close();
                }
                zipEntry = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();
        }

        return shapeFileName;
    }

    private File newFilef(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    /*
     * Set the value of the named parameter given in the prepared statement using
     * the corresponding prop of the shapefile. This is for the parcel table
     * insertion.
     */

    private long convertToInteger(Property prop, String namedParam) {

        if (prop.getType().getBinding() == Integer.class) {
            return (Integer) prop.getValue();
        } else if (prop.getType().getBinding() == String.class) {
            String vl = (String) prop.getValue();
            return Long.parseLong(vl);
        } else if (prop.getType().getBinding() == Double.class) {
            return ((Double) prop.getValue()).longValue();
        } else if (prop.getType().getBinding() == Float.class) {
            return ((Float) prop.getValue()).longValue();
        } else if (prop.getType().getBinding() == Long.class) {
            return (Long) prop.getValue();
        } else {
            throw new GenericApplicationException("Cannot convert parameter " + namedParam + " from "
                    + prop.getType().getBinding().toString() + " to integer");
        }
    }

    private double convertToDouble(Property prop, String namedParam) {
        if (prop.getType().getBinding() == Double.class) {
            return (Double) prop.getValue();
        } else if (prop.getType().getBinding() == String.class) {
            String vl = (String) prop.getValue();
            return Double.parseDouble(vl);
        } else if (prop.getType().getBinding() == Float.class) {
            return (Float) prop.getValue();
        } else if (prop.getType().getBinding() == Integer.class) {
            return (Integer) prop.getValue();
        } else if (prop.getType().getBinding() == Long.class) {
            return (Long) prop.getValue();
        } else {
            throw new GenericApplicationException("Cannot convert parameter " + namedParam + " from "
                    + prop.getType().getBinding().toString() + " to double");
        }
    }

    private void setPrepStmValue(NamedParameterPreparedStatement prepStatement, Property prop, String namedParam,
            Class typeClass) throws SQLException {

        if (prop == null) {
            throw new NullPointerException("Property not found for parameter: '" + namedParam + "'");
        }

        if (prop.getName() == null) {
            throw new NullPointerException("Property getName not found for parameter: '" + namedParam + "'");
        }

        if (prop.getValue() == null) {
            throw new NullPointerException("Property value not found for getName: '" + prop.getName() + "'");

        } else if (typeClass == Integer.class) {
            prepStatement.setInt(namedParam, (int) convertToInteger(prop, namedParam));
        } else if (typeClass == Long.class) {
            prepStatement.setLong(namedParam, convertToInteger(prop, namedParam));
        } else if (typeClass == Double.class) {
            prepStatement.setDouble(namedParam, (float) convertToDouble(prop, namedParam));
        } else if (typeClass == Float.class) {
            prepStatement.setFloat(namedParam, (float) convertToDouble(prop, namedParam));
        } else if (typeClass == String.class) {
            prepStatement.setString(namedParam, prop.getValue().toString());
        } else {
            throw new GenericApplicationException("Unknown type class " + typeClass);
        }

    }

    /*
     * Set the value of the named parameter given in the specific prepared statement
     * using the corresponding prop of the Shapefile. This is for the parcel
     * classification table insertion.
     */
    private void setPrepStmValueCultiv(NamedParameterPreparedStatement prepStatement, Property prop, String namedParam,
            HashMap<Integer, Integer> cultMap, Class typeClass) throws SQLException {

        if (prop == null) {
            throw new NullPointerException("Property not found for parameter: '" + namedParam + "'");
        }

        if (prop.getName() == null) {
            throw new NullPointerException("Property getName not found for parameter: '" + namedParam + "'");
        }

        if (prop.getValue() == null) {
            prepStatement.setNull(namedParam, java.sql.Types.NULL);

        } else if (prop.getType().getBinding() == Integer.class) {
            Integer cultCode = (Integer) prop.getValue();
            if (!cultMap.containsKey(cultCode)) {
                throw new GenericApplicationException("No cultivation found with code :" + cultCode);
            }
            Integer cultId = cultMap.get(cultCode);
            prepStatement.setInt(namedParam, cultId);

        } else if (prop.getType().getBinding() == Double.class) {
            Integer cultCode = ((Double) prop.getValue()).intValue();
            if (!cultMap.containsKey(cultCode)) {
                throw new GenericApplicationException("No cultivation found with code :" + cultCode);
            }
            Integer cultId = cultMap.get(cultCode);
            prepStatement.setInt(namedParam, cultId);

        } else if (prop.getType().getBinding() == Float.class) {
            Integer cultCode = ((Float) prop.getValue()).intValue();
            if (!cultMap.containsKey(cultCode)) {
                throw new GenericApplicationException("No cultivation found with code :" + cultCode);
            }
            Integer cultId = cultMap.get(cultCode);
            prepStatement.setInt(namedParam, cultId);
        } else if (prop.getType().getBinding() == Long.class) {
            Integer cultCode = ((Long) prop.getValue()).intValue();
            if (!cultMap.containsKey(cultCode)) {
                throw new GenericApplicationException("No cultivation found with code :" + cultCode);
            }
            Integer cultId = cultMap.get(cultCode);
            if (cultId == null) {
                // System.out.println(" cultId == null ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                // System.out.println(" Cult Code ----------------> " + cultCode);

            }
            prepStatement.setInt(namedParam, cultId);
        } else if (prop.getType().getBinding() == String.class) {

            throw new NullPointerException(
                    "Property cultivation code is String, expected Integer or numeric: '" + namedParam + "'");
        }
    }

    /*
     * Read each line of the passed filename shapefile. Then reproject if selected.
     * Then insert into the parcel database table if not already inserted. Then
     * insert also all lines (parcels) into the table. Create and run the DB
     * prepared statements for both tables’ insertions.
     */
    public void readInsertShp(File file, HashMap<String, String> mapping, int year, int clasId,
            HashMap<Integer, Integer> cultMap) throws IOException, NamingException, SQLException {

        Set<DataAccessFactory> all = new HashSet<DataAccessFactory>();

        Map<String, String> connect = new HashMap();
        connect.put("url", file.toURI().toString());

        DataStore dataStore = DataStoreFinder.getDataStore(connect);

        String[] typeNames = dataStore.getTypeNames();
        String typeName = typeNames[0];

        // System.out.println("Reading content " + typeName);
        // System.out.println("Data store is " + dataStore.toString());

        FeatureSource featureSource = dataStore.getFeatureSource(typeName);

        FeatureCollection collection = featureSource.getFeatures();
        // FeatureIterator iterator = collection.features();

        // get shp file coord system
        FeatureType schema = featureSource.getSchema();

        CoordinateReferenceSystem sourceCRS = schema.getCoordinateReferenceSystem();

        // System.out.println("Source Files' CRS= " + sourceCRS.toString());

        DataSource dsTo = null;
        Connection conTo = null;

        InitialContext cxt = new InitialContext();
        dsTo = (DataSource) cxt.lookup("java:/Niva");
        conTo = dsTo.getConnection();

        final String sql = "insert into /*+ APPEND_VALUES */ niva.parcel_class"
                + " (clas_id, cult_id_decl, cult_id_pred, prob_pred, cult_id_pred2, prob_pred2, geom4326, prod_code, parc_code, parc_identifier)"
                + " values"
                + " (:clas_id, :cult_id_decl, :cult_id_pred, :prob_pred, :cult_id_pred2, :prob_pred2, ST_SetSRID(:geom4326, 4326), :prod_code, :parc_code, :parc_identifier)";

        NamedParameterPreparedStatement prepStatement = NamedParameterPreparedStatement
                .createNamedParameterPreparedStatement(conTo, sql);

        int batchSize = 4000;
        int[] rowsInserted = null;
        int i = 0;

        // MT - Reproject the geometries to 4326 if needed.
        // collection=transformTo4326(collection, sourceCRS);

        CoordinateReferenceSystem CRS4326 = org.geotools.referencing.crs.DefaultGeographicCRS.WGS84;

        // CRS2100 needs to be redefined as it not OK by default.
        String CRS2100WKT = "PROJCS[\"GGRS87 / Greek Grid\",GEOGCS[\"GGRS87\",DATUM[\"Greek_Geodetic_Reference_System_1987\",SPHEROID[\"GRS 1980\",6378137,298.257222101, AUTHORITY[\"EPSG\",\"7019\"]], TOWGS84[-199.87,74.79,246.62,0,0,0,0], AUTHORITY[\"EPSG\",\"6121\"]], PRIMEM[\"Greenwich\",0, AUTHORITY[\"EPSG\",\"8901\"]],    UNIT[\"degree\",0.0174532925199433, AUTHORITY[\"EPSG\",\"9122\"]], AUTHORITY[\"EPSG\",\"4121\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",24],PARAMETER[\"scale_factor\",0.9996],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",0],UNIT[\"metre\",1, AUTHORITY[\"EPSG\",\"9001\"]],AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH],AUTHORITY[\"EPSG\",\"2100\"]]";

        try {
            CoordinateReferenceSystem CRS2100N = CRS.parseWKT(CRS2100WKT);

            if (sourceCRS.getName().toString().equals("Greek_Grid")
                    || sourceCRS.getName().toString().equals("GGRS87_Greek_Grid")) {
                sourceCRS = CRS2100N;
            }
        } catch (Exception ex) {
            throw new GenericApplicationException("GRS2100 cannot be used!");
        }

        // Check if CRS is not 4326 and reproject
        if (!sourceCRS.getName().equals(CRS4326.getName())) {

            try {
                ReprojectingFeatureCollection rfc = new ReprojectingFeatureCollection(collection, sourceCRS, CRS4326);
                collection = rfc;
            } catch (Exception e) {
                throw new RuntimeException("Failed to reproject the collection");
            }

        }

        FeatureIterator iterator = collection.features();

        String mapping_parc_code = mapping.get("parc_code");
        String mapping_prod_code = mapping.get("prod_code");
        String mapping_identifier = mapping.get("identifier");
        String mapping_cult_id_decl = mapping.get("cult_id_decl");
        String mapping_cult_id_pred = mapping.get("cult_id_pred");
        String mapping_cult_id_pred2 = mapping.get("cult_id_pred2");
        String mapping_prob_pred = mapping.get("prob_pred");
        String mapping_prob_pred2 = mapping.get("prob_pred2");

        try {
            while (iterator.hasNext()) {

                Feature feature = iterator.next();

                GeometryAttribute sourceGeometry = feature.getDefaultGeometryProperty();
                Geometry actualPolygon = (Geometry) sourceGeometry.getValue();

                if (actualPolygon == null) {
                    throw new NullPointerException("Geometry is null at row: " + (i + 1));
                }
                Property prop;

                // Now create prep statement to insert into class_parcel

                setPrepStmValueCultiv(prepStatement, feature.getProperty(mapping_cult_id_decl), "cult_id_decl", cultMap,
                        Integer.class);
                setPrepStmValueCultiv(prepStatement, feature.getProperty(mapping_cult_id_pred), "cult_id_pred", cultMap,
                        Integer.class);
                setPrepStmValueCultiv(prepStatement, feature.getProperty(mapping_cult_id_pred2), "cult_id_pred2",
                        cultMap, Integer.class);
                setPrepStmValue(prepStatement, feature.getProperty(mapping_prob_pred), "prob_pred", Double.class);
                setPrepStmValue(prepStatement, feature.getProperty(mapping_prob_pred2), "prob_pred2", Double.class);
                prepStatement.setInt("clas_id", clasId);

                // After parcel moved into parcel_class
                setPrepStmValue(prepStatement, feature.getProperty(mapping_prod_code), "prod_code", Integer.class);
                setPrepStmValue(prepStatement, feature.getProperty(mapping_parc_code), "parc_code", String.class);

                setPrepStmValue(prepStatement, feature.getProperty(mapping_identifier), "parc_identifier",
                        String.class);
                prepStatement.setGeometry("geom4326", actualPolygon);

                //
                prepStatement.addBatch();

                if (i % batchSize == batchSize - 1) {
                    rowsInserted = prepStatement.executeBatch();
                    // System.out.println("Executed Batch -- Rows: " + i);
                }

                i++;

            }
        } finally {

            rowsInserted = prepStatement.executeBatch();

            // System.out.println("Executed Classification Batch !! " + (rowsInserted.length
            // > 0 ? rowsInserted[0] : 0));

            iterator.close();
            dataStore.dispose();

        }

    }

    /*
     * 
     * protected static FeatureCollection transformTo4326(FeatureCollection
     * collection, CoordinateReferenceSystem sourceCRS){
     * 
     * CoordinateReferenceSystem CRS4326 =
     * org.geotools.referencing.crs.DefaultGeographicCRS.WGS84;
     * 
     * ReferenceIdentifier sourceCRSrefid=sourceCRS.getName(); ReferenceIdentifier
     * CRS4326refid=CRS4326.getName();
     * 
     * String CRS2100WKT =
     * "PROJCS[\"GGRS87 / Greek Grid\",GEOGCS[\"GGRS87\",DATUM[\"Greek_Geodetic_Reference_System_1987\",SPHEROID[\"GRS 1980\",6378137,298.257222101, AUTHORITY[\"EPSG\",\"7019\"]], TOWGS84[-199.87,74.79,246.62,0,0,0,0], AUTHORITY[\"EPSG\",\"6121\"]], PRIMEM[\"Greenwich\",0, AUTHORITY[\"EPSG\",\"8901\"]],    UNIT[\"degree\",0.0174532925199433, AUTHORITY[\"EPSG\",\"9122\"]], AUTHORITY[\"EPSG\",\"4121\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",24],PARAMETER[\"scale_factor\",0.9996],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",0],UNIT[\"metre\",1, AUTHORITY[\"EPSG\",\"9001\"]],AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH],AUTHORITY[\"EPSG\",\"2100\"]]"
     * ;
     * 
     * try { sourceCRS = CRS.parseWKT(CRS2100WKT); } catch (Exception ex) { throw
     * new GenericApplicationException("GRS2100 cannot be created!"); }
     * 
     * if (!sourceCRSrefid.equals(CRS4326refid)){
     * 
     * try { ReprojectingFeatureCollection rfc = new
     * ReprojectingFeatureCollection(collection, sourceCRS, CRS4326); return rfc; }
     * catch (Exception e) { throw new
     * RuntimeException("Failed to reproject the collection"); }
     * 
     * } else {
     * 
     * return collection; }
     * 
     * 
     * 
     * }
     * 
     */

    public static Collection<File> listFileTree(File dir) {
        Set<File> fileTree = new HashSet<File>();
        if (dir == null || dir.listFiles() == null) {
            return fileTree;
        }
        for (File entry : dir.listFiles()) {
            if (entry.isFile()) {
                fileTree.add(entry);
            } else {
                fileTree.addAll(listFileTree(entry));
            }
        }
        return fileTree;
    }

    /*
     * Decision Making
     */

    public List<String> runningDecisionMaking(Integer demaId, UserSession usrSession) {

        // System.out.println("Entered into runingDecisionMaking");

        DecisionMaking dema = getDecisionMakingFacade().findByDemaId(demaId);

        // System.out.println("dema was initilialized");

        // criteria_validation(dema);

        decisionProcess(dema, usrSession);

        // TODO: remove the record type comment
        // dema.setRecordtype(1);
        dema.setDteupdate(new Timestamp(new Date().getTime()));
        dema.setUsrupdate(usrSession.usrEmail);

        SaveResponse ret = new SaveResponse();
        List<String> returnWarnings = new ArrayList<>();
        returnWarnings.add(ret.warningMessages);

        // List<ParcelClas> parcelclass = dema.getClasId().getParcelClasCollection();

        return returnWarnings;
    }

    /*
     * 
     * Based on the selected BRE run the type of rule engine Level
     * 
     */

    public void decisionProcess(DecisionMaking dema, UserSession usrSession) {

        // System.out.println("Entered into decisionProcess");

        // Call the Preferred Crop Level Procedure
        // 0 crop, 1 land cover, 2 superclass case
        if (dema.getEcgrId().getCropLevel() == 0) {
            try {
                cropLevelEngine(dema, usrSession.usrEmail);
            } catch (NamingException | SQLException ex) {
                throw new GenericApplicationException(ex);
            }
        }

        if (dema.getEcgrId().getCropLevel() == 1) {
            try {
                landCoverLevelEngine(dema, usrSession.usrEmail);
            } catch (NamingException | SQLException ex) {
                throw new GenericApplicationException(ex);
            }
        }

        if (dema.getEcgrId().getCropLevel() == 2) {

            try {
                cropToLandCoverEngine(dema, usrSession.usrEmail);
            } catch (NamingException | SQLException ex) {
                throw new GenericApplicationException(ex);
            }

        }
        dema.setHasBeenRun(true);

    }

    // Run a crop level decision making process
    // add a new decision parcel_decision

    protected void cropLevelEngine(DecisionMaking dema, String usrinsert) throws NamingException, SQLException {

        DataSource dsTo = null;
        Connection conTo = null;

        InitialContext cxt = new InitialContext();
        dsTo = (DataSource) cxt.lookup("java:/Niva");
        conTo = dsTo.getConnection();

        final String sql = "insert into /*+ APPEND_VALUES */ niva.parcel_decision"
                + " (pcla_id, dema_id, decision_light, usrinsert)" + " values" + " (:pclaId, :demaId, :decisionLight, :usrinsert)";

        NamedParameterPreparedStatement prepStatement = NamedParameterPreparedStatement
                .createNamedParameterPreparedStatement(conTo, sql);

        int batchSize = 4000;
        int[] rowsInserted = null;
        int i = 0;

        //
        HashSet<Integer> ecCultivationSet = new HashSet<>();
        for (EcCult eccult : dema.getEcgrId().getEcCultCollection()) {
            ecCultivationSet.add(eccult.getCultId().getCode());
        }

        // for each cultivation add the cultivation as key and its' criteria list as
        // value
        HashMap<Integer, List<EcCultDetail>> perCultCriteria = new HashMap<>();

        // now for each cultivation retrieve its criteria
        for (EcCult ecCult : dema.getEcgrId().getEcCultCollection()) {

            Query queryCriteria = getEntityManager().createQuery("select ecd from EcCultDetail ecd "
                    + " where ecd.eccuId.eccuId = :eccu_id " + "ORDER BY ecd.orderingNumber ");

            queryCriteria.setParameter("eccu_id", ecCult.getEccuId());

            List<EcCultDetail> ecCultDetailResults = (List<EcCultDetail>) queryCriteria.getResultList();

            perCultCriteria.put(ecCult.getCultId().getCode(), ecCultDetailResults);
        }

        // Define variables for each parcel
        Integer ecCultDetailCriterioAgreesDeclar;
        Integer parcelClassCultIdDeclCultId;
        Integer parcelClassCultIdPredCultId;
        Integer ecCultDetailCriterioComparisonOper;
        BigDecimal ecCultDetailCriterioProbabThres;
        Double parcelClassProbPred;

        Integer ecCultDetailCriterioAgreesDeclar2;
        Integer parcelClassCultIdPredCultId2;
        Integer ecCultDetailCriterioComparisonOper2;
        BigDecimal ecCultDetailCriterioProbabThres2;
        Double parcelClassProbPred2;

        Integer ecCultDetailCriterioComparisonOper3;
        BigDecimal ecCultDetailCriterioProbabThresSum;

        Double parcelClassProbPred100;
        Double ecCultDetailCriterioProbabThresDoubleValue;

        try {
            // now for each Parcel -> decide
            for (ParcelClas parcelClass : dema.getClasId().getParcelClasCollection()) {

                prepStatement.setInt("pclaId", parcelClass.getPclaId());
                prepStatement.setInt("demaId", dema.getDemaId());
                prepStatement.setString("usrinsert", usrinsert);

                if (parcelClass.getCultIdDecl() == null || parcelClass.getCultIdDecl().getCode() == null
                        || parcelClass.getCultIdPred() == null || parcelClass.getCultIdPred().getCode() == null
                        || parcelClass.getCultIdPred2() == null || parcelClass.getCultIdPred2().getCode() == null) {

                    // STOP if missing any prediction in this parcel
                    // then create new with 4 for decision(undefined)

                    prepStatement.setInt("decisionLight", 4);
                    continue;
                }

                // proceed in decision making for this crop
                // get the criteria of the declared crop

                boolean undefined = true;

                List<EcCultDetail> currentCultDetailList = perCultCriteria.get(parcelClass.getCultIdDecl().getCode());

                if (currentCultDetailList != null) {
                    for (EcCultDetail ecCultDetailCriterio : currentCultDetailList) {

                        boolean criteriaResult = true;

                        ecCultDetailCriterioAgreesDeclar = ecCultDetailCriterio.getAgreesDeclar();
                        parcelClassCultIdDeclCultId = parcelClass.getCultIdDecl().getCultId();
                        parcelClassCultIdPredCultId = parcelClass.getCultIdPred().getCultId();
                        ecCultDetailCriterioComparisonOper = ecCultDetailCriterio.getComparisonOper();
                        ecCultDetailCriterioProbabThres = ecCultDetailCriterio.getProbabThres();
                        parcelClassProbPred = parcelClass.getProbPred();

                        ecCultDetailCriterioAgreesDeclar2 = ecCultDetailCriterio.getAgreesDeclar2();
                        parcelClassCultIdPredCultId2 = parcelClass.getCultIdPred2().getCultId();
                        ecCultDetailCriterioComparisonOper2 = ecCultDetailCriterio.getComparisonOper2();
                        ecCultDetailCriterioProbabThres2 = ecCultDetailCriterio.getProbabThres2();
                        parcelClassProbPred2 = parcelClass.getProbPred2();

                        ecCultDetailCriterioComparisonOper3 = ecCultDetailCriterio.getComparisonOper3();
                        ecCultDetailCriterioProbabThresSum = ecCultDetailCriterio.getProbabThresSum();

                        /** For the 1st prediction **/

                        // for disagree & agree

                        if (ecCultDetailCriterioAgreesDeclar != null) {
                            if (ecCultDetailCriterioAgreesDeclar == 0) {
                                if (parcelClassCultIdDeclCultId == parcelClassCultIdPredCultId) {
                                    criteriaResult = false;
                                }
                            } else {
                                if (!(parcelClassCultIdDeclCultId == parcelClassCultIdPredCultId)) {
                                    criteriaResult = false;
                                }
                            }
                        }

                        // For Operator

                        if (ecCultDetailCriterioComparisonOper != null && ecCultDetailCriterioProbabThres != null) {

                            parcelClassProbPred100 = parcelClassProbPred * 100;
                            ecCultDetailCriterioProbabThresDoubleValue = ecCultDetailCriterioProbabThres.doubleValue();

                            switch (ecCultDetailCriterioComparisonOper) {

                                case 1:
                                    // >
                                    if (parcelClassProbPred100 <= ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }
                                    break;

                                case 2:
                                    // >=
                                    if (parcelClassProbPred100 < ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }
                                    break;

                                case 3:
                                    // <
                                    if (parcelClassProbPred100 >= ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }
                                    break;

                                case 4:
                                    // <=
                                    if (parcelClassProbPred100 > ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }
                                    break;

                                default:

                            }

                        }

                        /** For the 2nd prediction **/

                        // for disagree & agree

                        if (ecCultDetailCriterioAgreesDeclar2 != null) {
                            if (ecCultDetailCriterioAgreesDeclar2 == 0) {
                                if (parcelClassCultIdDeclCultId == parcelClassCultIdPredCultId2) {
                                    criteriaResult = false;
                                }
                            } else {
                                if (!(parcelClassCultIdDeclCultId == parcelClassCultIdPredCultId2)) {
                                    criteriaResult = false;
                                }
                            }
                        }

                        // For Operator

                        if (ecCultDetailCriterioComparisonOper2 != null && ecCultDetailCriterioProbabThres2 != null) {

                            parcelClassProbPred100 = parcelClassProbPred2 * 100;
                            ecCultDetailCriterioProbabThresDoubleValue = ecCultDetailCriterioProbabThres2.doubleValue();

                            switch (ecCultDetailCriterioComparisonOper2) {

                                case 1:
                                    // >
                                    if (parcelClassProbPred100 <= ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }
                                    break;

                                case 2:
                                    // >=
                                    if (parcelClassProbPred100 < ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }
                                    break;

                                case 3:
                                    // <
                                    if (parcelClassProbPred100 >= ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }
                                    break;

                                case 4:
                                    // <=
                                    if (parcelClassProbPred100 > ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }
                                    break;

                                default:

                            }

                        }

                        /** For the 1st and 2nd SUM (combined) predictions **/

                        // >

                        if (ecCultDetailCriterioComparisonOper3 != null && ecCultDetailCriterioProbabThresSum != null) {

                            parcelClassProbPred100 = (parcelClassProbPred + parcelClassProbPred2) * 100;
                            ecCultDetailCriterioProbabThresDoubleValue = ecCultDetailCriterioProbabThresSum
                                    .doubleValue();

                            switch (ecCultDetailCriterioComparisonOper3) {

                                case 1:
                                    // >
                                    if (parcelClassProbPred100 <= ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }
                                    break;

                                case 2:
                                    // >=
                                    if (parcelClassProbPred100 < ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }

                                    break;

                                case 3:
                                    // <
                                    if (parcelClassProbPred100 >= ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }

                                    break;

                                case 4:
                                    // <=
                                    if (parcelClassProbPred100 < ecCultDetailCriterioProbabThresDoubleValue) {
                                        criteriaResult = false;
                                    }
                                    break;

                                default:

                                    break;

                            }
                        }

                        // check if criteria met, if yes stop and mark this parcel decision
                        if (criteriaResult == true) {

                            undefined = false;

                            prepStatement.setInt("decisionLight", ecCultDetailCriterio.getDecisionLight());

                            // stop iterating criteria, as decision has been taken
                            break;
                        }

                    }
                }

                // if iterating all criteria of this parcel it is still undefined
                // create new record as undefined
                if (undefined == true) {

                    prepStatement.setInt("decisionLight", 4);

                }

                prepStatement.addBatch();

                if (i > 0 && i % batchSize == 0) {
                    rowsInserted = prepStatement.executeBatch();
                    // System.out.println("Executed Batch -- Rows: " + i);
                }

                i++;

            } // Each Parcel
        } finally {

            rowsInserted = prepStatement.executeBatch();

            // System.out.println("Executed Classification Batch !! " + (rowsInserted.length

        }

    }

    protected void landCoverLevelEngine(DecisionMaking dema, String usrinsert) throws NamingException, SQLException {

        // Initialize database connection

        DataSource dsTo = null;
        Connection conTo = null;

        InitialContext cxt = new InitialContext();
        dsTo = (DataSource) cxt.lookup("java:/Niva");
        conTo = dsTo.getConnection();

        final String sql = "insert into /*+ APPEND_VALUES */ niva.parcel_decision"
                + " (pcla_id, dema_id, decision_light, usrinsert)" + " values" + " (:pclaId, :demaId, :decisionLight, :usrinsert)";

        NamedParameterPreparedStatement prepStatement = NamedParameterPreparedStatement
                .createNamedParameterPreparedStatement(conTo, sql);

        int batchSize = 4000;
        int[] rowsInserted = null;

        // get cultivation ( which are land cover codes in this case)
        HashSet<Integer> ecCultivationSet = new HashSet<>();
        for (EcCult eccult : dema.getEcgrId().getEcCultCollection()) {
            ecCultivationSet.add(eccult.getCotyId().getCode());
        }

        HashMap<Integer, List<EcCultDetail>> perCultCriteria = new HashMap<>();

        // now for each cultivation retrieve its criteria
        for (EcCult ecCult : dema.getEcgrId().getEcCultCollection()) {

            Query queryCriteria = getEntityManager().createQuery("select ecd from EcCultDetail ecd "
                    + " where ecd.eccuId.eccuId = :eccu_id " + "ORDER BY ecd.orderingNumber ");

            queryCriteria.setParameter("eccu_id", ecCult.getEccuId());

            List<EcCultDetail> ecCultDetailResults = (List<EcCultDetail>) queryCriteria.getResultList();

            // for each cover-type, add the cover code as key and its' criteria list as
            // value
            perCultCriteria.put(ecCult.getCotyId().getCode(), ecCultDetailResults);
        }

        int parcelSum = 0;

        try {
            // now for each Parcel

            for (ParcelClas parcelClass : dema.getClasId().getParcelClasCollection()) {

                prepStatement.setInt("pclaId", parcelClass.getPclaId());
                prepStatement.setInt("demaId", dema.getDemaId());
                prepStatement.setString("usrinsert", usrinsert);

                // get the cover type code from the cult
                if (parcelClass.getCultIdDecl() == null || parcelClass.getCultIdDecl().getCotyId().getCode() == null
                        || parcelClass.getCultIdPred() == null
                        || parcelClass.getCultIdPred().getCotyId().getCode() == null
                        || parcelClass.getCultIdPred2() == null
                        || parcelClass.getCultIdPred2().getCotyId().getCode() == null) {

                    // STOP if missing any prediction in this parcel
                    // then create new with 4 for decision(undefined)

                    prepStatement.setInt("decisionLight", 4);
                    continue;

                }

                // proceed in decision making for this crop
                // get the criteria of the declared crop

                boolean undefined = true;

                // perCultCriteria has the the cover-type code as key and its' criteria list as
                // value

                List<EcCultDetail> currentCultDetailList = perCultCriteria
                        .get(parcelClass.getCultIdDecl().getCotyId().getCode());

                if (currentCultDetailList != null) {

                    for (EcCultDetail ecCultDetailCriterio : currentCultDetailList) {

                        boolean criteriaResult = true;

                        /** For the 1st prediction **/

                        // for disagree

                        if (ecCultDetailCriterio.getAgreesDeclar() != null
                                && ecCultDetailCriterio.getAgreesDeclar() == 0) {
                            if (!(parcelClass.getCultIdDecl().getCotyId().getCode() != parcelClass.getCultIdPred()
                                    .getCotyId().getCode())) {
                                criteriaResult = false;
                            }
                        }
                        // for agree
                        if (ecCultDetailCriterio.getAgreesDeclar() != null
                                && ecCultDetailCriterio.getAgreesDeclar() == 1) {
                            if (!(parcelClass.getCultIdDecl().getCotyId().getCode() == parcelClass.getCultIdPred()
                                    .getCotyId().getCode())) {
                                criteriaResult = false;
                            }
                        }

                        // >
                        if (ecCultDetailCriterio.getComparisonOper() != null
                                && ecCultDetailCriterio.getProbabThres() != null
                                && ecCultDetailCriterio.getComparisonOper() == 1) {
                            if (!((parcelClass.getProbPred() * 100) > ecCultDetailCriterio.getProbabThres()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }
                        }
                        // >=
                        if (ecCultDetailCriterio.getComparisonOper() != null
                                && ecCultDetailCriterio.getProbabThres() != null
                                && ecCultDetailCriterio.getComparisonOper() == 2) {
                            if (!((parcelClass.getProbPred() * 100) >= ecCultDetailCriterio.getProbabThres()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }

                        }
                        // <
                        if (ecCultDetailCriterio.getComparisonOper() != null
                                && ecCultDetailCriterio.getProbabThres() != null
                                && ecCultDetailCriterio.getComparisonOper() == 3) {
                            if (!((parcelClass.getProbPred() * 100) < ecCultDetailCriterio.getProbabThres()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }
                        }
                        // <=
                        if (ecCultDetailCriterio.getComparisonOper() != null
                                && ecCultDetailCriterio.getProbabThres() != null
                                && ecCultDetailCriterio.getComparisonOper() == 4) {
                            if (!((parcelClass.getProbPred() * 100) <= ecCultDetailCriterio.getProbabThres()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }
                        }

                        /** For the 2nd prediction **/

                        // for disagree

                        if (ecCultDetailCriterio.getAgreesDeclar2() != null
                                && ecCultDetailCriterio.getAgreesDeclar2() == 0) {
                            if (!(parcelClass.getCultIdDecl().getCotyId().getCode() != parcelClass.getCultIdPred2()
                                    .getCotyId().getCode())) {
                                criteriaResult = false;
                            }
                        }
                        // for agree
                        if (ecCultDetailCriterio.getAgreesDeclar2() != null
                                && ecCultDetailCriterio.getAgreesDeclar2() == 1) {
                            if (!(parcelClass.getCultIdDecl().getCotyId().getCode() == parcelClass.getCultIdPred2()
                                    .getCotyId().getCode())) {
                                criteriaResult = false;
                            }
                        }

                        // >
                        if (ecCultDetailCriterio.getComparisonOper2() != null
                                && ecCultDetailCriterio.getProbabThres2() != null
                                && ecCultDetailCriterio.getComparisonOper2() == 1) {
                            if (!((parcelClass.getProbPred2() * 100) > ecCultDetailCriterio.getProbabThres2()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }
                        }
                        // >=
                        if (ecCultDetailCriterio.getComparisonOper2() != null
                                && ecCultDetailCriterio.getProbabThres2() != null
                                && ecCultDetailCriterio.getComparisonOper2() == 2) {
                            if (!((parcelClass.getProbPred2() * 100) >= ecCultDetailCriterio.getProbabThres2()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }

                        }
                        // <
                        if (ecCultDetailCriterio.getComparisonOper2() != null
                                && ecCultDetailCriterio.getProbabThres2() != null
                                && ecCultDetailCriterio.getComparisonOper2() == 3) {
                            if (!((parcelClass.getProbPred2() * 100) < ecCultDetailCriterio.getProbabThres2()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }

                        }
                        // <=
                        if (ecCultDetailCriterio.getComparisonOper2() != null
                                && ecCultDetailCriterio.getProbabThres2() != null
                                && ecCultDetailCriterio.getComparisonOper2() == 4) {
                            if (!((parcelClass.getProbPred2() * 100) <= ecCultDetailCriterio.getProbabThres2()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }
                        }

                        /**
                         * For the 1st and 2nd SUM (combined) predictions
                         **/

                        // >

                        if (ecCultDetailCriterio.getComparisonOper3() != null
                                && ecCultDetailCriterio.getProbabThresSum() != null
                                && ecCultDetailCriterio.getComparisonOper3() == 1) {
                            if (!((parcelClass.getProbPred() * 100
                                    + parcelClass.getProbPred2() * 100) > ecCultDetailCriterio.getProbabThresSum()
                                            .doubleValue())) {
                                criteriaResult = false;
                            }
                        }
                        // >=
                        if (ecCultDetailCriterio.getComparisonOper3() != null
                                && ecCultDetailCriterio.getProbabThresSum() != null
                                && ecCultDetailCriterio.getComparisonOper3() == 2) {
                            if (!((parcelClass.getProbPred() * 100
                                    + parcelClass.getProbPred2() * 100) >= ecCultDetailCriterio.getProbabThresSum()
                                            .doubleValue())) {
                                criteriaResult = false;
                            }

                        }
                        // <
                        if (ecCultDetailCriterio.getComparisonOper3() != null
                                && ecCultDetailCriterio.getProbabThresSum() != null
                                && ecCultDetailCriterio.getComparisonOper3() == 3) {
                            if (!((parcelClass.getProbPred() * 100
                                    + parcelClass.getProbPred2() * 100) < ecCultDetailCriterio.getProbabThresSum()
                                            .doubleValue())) {
                                criteriaResult = false;
                            }

                        }
                        // <=
                        if (ecCultDetailCriterio.getComparisonOper3() != null
                                && ecCultDetailCriterio.getProbabThresSum() != null
                                && ecCultDetailCriterio.getComparisonOper3() == 4) {
                            if (!((parcelClass.getProbPred() * 100
                                    + parcelClass.getProbPred2() * 100) <= ecCultDetailCriterio.getProbabThresSum()
                                            .doubleValue())) {
                                criteriaResult = false;
                            }
                        }

                        // check if criteria met, if yes stop and mark this parcel decision
                        if (criteriaResult == true) {

                            undefined = false;

                            // create the new

                            prepStatement.setInt("decisionLight", ecCultDetailCriterio.getDecisionLight());

                            // stop iterating criteria, as decision has been taken
                            break;
                        }

                    }

                }

                // if iterating all criteria of this parcel it is still undefined
                // create new record as undefined
                if (undefined == true) {

                    // create the new with 4 for decision(undefined)
                    prepStatement.setInt("decisionLight", 4);
                }

                prepStatement.addBatch();

                if (parcelSum > 0 && parcelSum % batchSize == 0) {
                    rowsInserted = prepStatement.executeBatch();
                    // System.out.println("Executed Batch -- Rows: " + i);
                }

                parcelSum++;

            } // Each Parcel
        } finally {

            rowsInserted = prepStatement.executeBatch();

            // System.out.println("Executed Classification Batch !! " + (rowsInserted.length

        }

    }

    /*
     * A hybrid approach which takes the criteria using the crop level of the parcel
     * but it compares the parcel and the cultivation using their cover type.
     */

    protected void cropToLandCoverEngine(DecisionMaking dema, String usrinsert) throws NamingException, SQLException {

        // Initialize Database connection

        DataSource dsTo = null;
        Connection conTo = null;

        InitialContext cxt = new InitialContext();
        dsTo = (DataSource) cxt.lookup("java:/Niva");
        conTo = dsTo.getConnection();

        final String sql = "insert into /*+ APPEND_VALUES */ niva.parcel_decision"
                + " (pcla_id, dema_id, decision_light, usrinsert)" + " values" + " (:pclaId, :demaId, :decisionLight, :usrinsert)";

        NamedParameterPreparedStatement prepStatement = NamedParameterPreparedStatement
                .createNamedParameterPreparedStatement(conTo, sql);

        int batchSize = 4000;
        int[] rowsInserted = null;

        HashSet<Integer> ecCultivationSet = new HashSet<>();
        for (EcCult eccult : dema.getEcgrId().getEcCultCollection()) {
            ecCultivationSet.add(eccult.getCultId().getCode());
        }

        // for each cultivation add the cultivation as key and its' criteria list as
        // value
        HashMap<Integer, List<EcCultDetail>> perCultCriteria = new HashMap<>();

        // now for each cultivation retrieve its criteria
        for (EcCult ecCult : dema.getEcgrId().getEcCultCollection()) {

            Query queryCriteria = getEntityManager().createQuery("select ecd from EcCultDetail ecd "
                    + " where ecd.eccuId.eccuId = :eccu_id " + "ORDER BY ecd.orderingNumber ");

            queryCriteria.setParameter("eccu_id", ecCult.getEccuId());

            List<EcCultDetail> ecCultDetailResults = (List<EcCultDetail>) queryCriteria.getResultList();

            perCultCriteria.put(ecCult.getCultId().getCode(), ecCultDetailResults);
        }

        // Prefetch the Ids from the Database
        final String sqlGetSq = "SELECT nextval('niva.niva_sq') FROM generate_series(0,:lengthOfParcels)";
        Query queryGetSq = getEntityManager().createNativeQuery(sqlGetSq);

        queryGetSq.setParameter("lengthOfParcels", dema.getClasId().getParcelClasCollection().size());

        List<BigInteger> sq = (List<BigInteger>) queryGetSq.getResultList();

        int parcelSum = 0;

        try {
            // now for each Parcel -> decide
            for (ParcelClas parcelClass : dema.getClasId().getParcelClasCollection()) {

                prepStatement.setInt("pclaId", parcelClass.getPclaId());
                prepStatement.setInt("demaId", dema.getDemaId());
                prepStatement.setString("usrinsert", usrinsert);

                if (parcelClass.getCultIdDecl() == null || parcelClass.getCultIdDecl().getCotyId().getCode() == null
                        || parcelClass.getCultIdPred() == null
                        || parcelClass.getCultIdPred().getCotyId().getCode() == null
                        || parcelClass.getCultIdPred2() == null
                        || parcelClass.getCultIdPred2().getCotyId().getCode() == null) {

                    // STOP if missing any prediction in this parcel
                    // then create new with 4 for decision(undefined)
                    prepStatement.setInt("decisionLight", (int) 4);
                    continue;

                }

                // proceed in decision making for this crop
                // get the criteria of the declared crop

                boolean undefined = true;
                // get the criteria using crop level
                List<EcCultDetail> currentCultDetailList = perCultCriteria.get(parcelClass.getCultIdDecl().getCode());

                if (currentCultDetailList != null) {

                    for (EcCultDetail ecCultDetailCriterio : currentCultDetailList) {

                        boolean criteriaResult = true;

                        /** For the 1st prediction **/

                        // for disagree

                        if (ecCultDetailCriterio.getAgreesDeclar() != null
                                && ecCultDetailCriterio.getAgreesDeclar() == 0) {
                            if (!(parcelClass.getCultIdDecl().getCotyId().getCode() != parcelClass.getCultIdPred()
                                    .getCotyId().getCode())) {
                                criteriaResult = false;
                            }
                        }
                        // for agree
                        if (ecCultDetailCriterio.getAgreesDeclar() != null
                                && ecCultDetailCriterio.getAgreesDeclar() == 1) {
                            if (!(parcelClass.getCultIdDecl().getCotyId().getCode() == parcelClass.getCultIdPred()
                                    .getCotyId().getCode())) {
                                criteriaResult = false;
                            }
                        }

                        // >
                        if (ecCultDetailCriterio.getComparisonOper() != null
                                && ecCultDetailCriterio.getProbabThres() != null
                                && ecCultDetailCriterio.getComparisonOper() == 1) {
                            if (!((parcelClass.getProbPred() * 100) > ecCultDetailCriterio.getProbabThres()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }
                        }
                        // >=
                        if (ecCultDetailCriterio.getComparisonOper() != null
                                && ecCultDetailCriterio.getProbabThres() != null
                                && ecCultDetailCriterio.getComparisonOper() == 2) {
                            if (!((parcelClass.getProbPred() * 100) >= ecCultDetailCriterio.getProbabThres()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }

                        }
                        // <
                        if (ecCultDetailCriterio.getComparisonOper() != null
                                && ecCultDetailCriterio.getProbabThres() != null
                                && ecCultDetailCriterio.getComparisonOper() == 3) {
                            if (!((parcelClass.getProbPred() * 100) < ecCultDetailCriterio.getProbabThres()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }
                        }
                        // <=
                        if (ecCultDetailCriterio.getComparisonOper() != null
                                && ecCultDetailCriterio.getProbabThres() != null
                                && ecCultDetailCriterio.getComparisonOper() == 4) {
                            if (!((parcelClass.getProbPred() * 100) <= ecCultDetailCriterio.getProbabThres()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }
                        }

                        /** For the 2nd prediction **/

                        // for disagree

                        if (ecCultDetailCriterio.getAgreesDeclar2() != null
                                && ecCultDetailCriterio.getAgreesDeclar2() == 0) {
                            if (!(parcelClass.getCultIdDecl().getCotyId().getCode() != parcelClass.getCultIdPred2()
                                    .getCotyId().getCode())) {
                                criteriaResult = false;
                            }
                        }
                        // for agree
                        if (ecCultDetailCriterio.getAgreesDeclar2() != null
                                && ecCultDetailCriterio.getAgreesDeclar2() == 1) {
                            if (!(parcelClass.getCultIdDecl().getCotyId().getCode() == parcelClass.getCultIdPred2()
                                    .getCotyId().getCode())) {
                                criteriaResult = false;
                            }
                        }

                        // >
                        if (ecCultDetailCriterio.getComparisonOper2() != null
                                && ecCultDetailCriterio.getProbabThres2() != null
                                && ecCultDetailCriterio.getComparisonOper2() == 1) {
                            if (!((parcelClass.getProbPred2() * 100) > ecCultDetailCriterio.getProbabThres2()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }
                        }
                        // >=
                        if (ecCultDetailCriterio.getComparisonOper2() != null
                                && ecCultDetailCriterio.getProbabThres2() != null
                                && ecCultDetailCriterio.getComparisonOper2() == 2) {
                            if (!((parcelClass.getProbPred2() * 100) >= ecCultDetailCriterio.getProbabThres2()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }

                        }
                        // <
                        if (ecCultDetailCriterio.getComparisonOper2() != null
                                && ecCultDetailCriterio.getProbabThres2() != null
                                && ecCultDetailCriterio.getComparisonOper2() == 3) {
                            if (!((parcelClass.getProbPred2() * 100) < ecCultDetailCriterio.getProbabThres2()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }

                        }
                        // <=
                        if (ecCultDetailCriterio.getComparisonOper2() != null
                                && ecCultDetailCriterio.getProbabThres2() != null
                                && ecCultDetailCriterio.getComparisonOper2() == 4) {
                            if (!((parcelClass.getProbPred2() * 100) <= ecCultDetailCriterio.getProbabThres2()
                                    .doubleValue())) {
                                criteriaResult = false;
                            }
                        }

                        /** For the 1st and 2nd SUM (combined) predictions **/

                        // >

                        if (ecCultDetailCriterio.getComparisonOper3() != null
                                && ecCultDetailCriterio.getProbabThresSum() != null
                                && ecCultDetailCriterio.getComparisonOper3() == 1) {
                            if (!((parcelClass.getProbPred() * 100
                                    + parcelClass.getProbPred2() * 100) > ecCultDetailCriterio.getProbabThresSum()
                                            .doubleValue())) {
                                criteriaResult = false;
                            }
                        }
                        // >=
                        if (ecCultDetailCriterio.getComparisonOper3() != null
                                && ecCultDetailCriterio.getProbabThresSum() != null
                                && ecCultDetailCriterio.getComparisonOper3() == 2) {
                            if (!((parcelClass.getProbPred() * 100
                                    + parcelClass.getProbPred2() * 100) >= ecCultDetailCriterio.getProbabThresSum()
                                            .doubleValue())) {
                                criteriaResult = false;
                            }

                        }
                        // <
                        if (ecCultDetailCriterio.getComparisonOper3() != null
                                && ecCultDetailCriterio.getProbabThresSum() != null
                                && ecCultDetailCriterio.getComparisonOper3() == 3) {
                            if (!((parcelClass.getProbPred() * 100
                                    + parcelClass.getProbPred2() * 100) < ecCultDetailCriterio.getProbabThresSum()
                                            .doubleValue())) {
                                criteriaResult = false;
                            }

                        }
                        // <=
                        if (ecCultDetailCriterio.getComparisonOper3() != null
                                && ecCultDetailCriterio.getProbabThresSum() != null
                                && ecCultDetailCriterio.getComparisonOper3() == 4) {
                            if (!((parcelClass.getProbPred() * 100
                                    + parcelClass.getProbPred2() * 100) <= ecCultDetailCriterio.getProbabThresSum()
                                            .doubleValue())) {
                                criteriaResult = false;
                            }
                        }

                        // check if criteria met, if yes stop and mark this parcel decision
                        if (criteriaResult == true) {

                            undefined = false;

                            // create the new
                            prepStatement.setInt("decisionLight", ecCultDetailCriterio.getDecisionLight());

                            // stop iterating criteria, as decision has been taken
                            break;
                        }

                    }
                }

                // if iterating all criteria of this parcel it is still undefined
                // create new record as undefined
                if (undefined == true) {

                    // create the new with 4 for decision(undefined)
                    prepStatement.setInt("decisionLight", 4);

                }
                prepStatement.addBatch();

                if (parcelSum > 0 && parcelSum % batchSize == 0) {
                    rowsInserted = prepStatement.executeBatch();
                    // System.out.println("Executed Batch -- Rows: " + i);
                }

                parcelSum++;

            } // Each Parcel
        } finally {

            rowsInserted = prepStatement.executeBatch();

            // System.out.println("Executed Classification Batch !! " + (rowsInserted.length

        }

    }

    /**
     * MT - Manages Agrispan requests.
     *
     * 
     * @param agrispanUid
     * @return
     */

    @Override
    public String getPhotoRequests(String agrisnapUid) {
        // agrisnapUid: a registred Agrisnap User Id (5 Chars).

        final String ErrorProvideParametersRequest = "Error: Please, provide the needed parameters.";
        // Response in case needed parameters are not provided, or agrisnapUid is not
        // registered.
        final String ErrorFetchUserId = "Error: User Not Found.";
        final String ErrorCreateARequest = "Error: Request entry couldn't be created.";
        final String ErrorCreateProducersList = "Error: Related Producers entries couldn't be created.";
        final String ErrorCreateContexts = "Error: Related Contexts entries couldn't be created.";
        final String ErrorFetchRequestData = "Error: Response couldn't be created.";

        final String sqlQueryFetchUserId = "Select agrisnap_users_id From niva.agrisnap_users Where agrisnap_uid = :agrisnap_uid";
        // Query to fetch the User Id related to the agrisnapUid.
        final String sqlQueryCreateARequest = "INSERT INTO niva.gp_requests(type_code, geotag, agrisnap_users_id) VALUES (10, 'Y', :agrisnap_users_id ) RETURNING gp_requests_id";
        // Creates a new request in the DB.
        final String sqlQueryCreateProducersList = "INSERT INTO niva.gp_requests_producers(gp_requests_id, producers_id)"
                + " SELECT DISTINCT :gp_requests_id, producers.producers_id FROM niva.producers producers"
                + " JOIN niva.agrisnap_users_producers agrisnap_users_producers ON producers.producers_id=agrisnap_users_producers.producers_id"
                + " JOIN niva.agrisnap_users agrisnap_users ON agrisnap_users_producers.agrisnap_users_id=agrisnap_users.agrisnap_users_id"
                + " WHERE agrisnap_users.agrisnap_users_id=:agrisnap_users_id"
                + " RETURNING gp_requests_producers_id, producers_id";

        // Creates records relating user & producers involved in the request.

        final String sqlQueryCreateContexts = "INSERT INTO niva.gp_requests_contexts(gp_requests_producers_id, hash, parcels_issues_id, label, geom_hexewkb)"
                + " SELECT gp_requests_producers.gp_requests_producers_id, md5(CONCAT(CAST(parcels_issues.parcels_issues_id AS text),CAST(gp_requests_producers.gp_requests_producers_id AS text) )), parcels_issues.parcels_issues_id, parcel_class.parc_identifier, ST_AsHEXEWKB(parcel_class.geom4326)"
                + " FROM niva.gp_requests_producers gp_requests_producers"
                + " JOIN niva.producers producers ON gp_requests_producers.producers_id=producers.producers_id"
                + " JOIN niva.parcel_class parcel_class ON producers.prod_code=parcel_class.prod_code"
                + " JOIN niva.parcels_issues as parcels_issues on parcel_class.pcla_id=parcels_issues.pcla_id"
                + " WHERE parcels_issues.status<900 AND gp_requests_producers.gp_requests_id=:requestId"
                + " RETURNING gp_requests_contexts.gp_requests_contexts_id";
        // Creates Contexts related to the producers and the request.

        final String sqlQueryInsertIntoParcelsIssuesActivities = "INSERT INTO niva.parcels_issues_activities(parcels_issues_id, type, type_extrainfo) VALUES (:parcels_issues_id, :type, :type_extrainfo)";
        // Insert a record for the request activity into the DB for logging.

        final String sqlQueryFetchRequestData = "SELECT type_code, type_description, scheme, correspondence_id, correspondence_doc_nr, geotag FROM niva.gp_requests WHERE gp_requests_id=:gp_requests_id";
        // Fetches data related to a request.
        final String sqlQueryFetchRequestProducersContexts = "SELECT gp_requests_contexts_id, hash, type, label, comment, ST_AsGeoJSON(ST_Transform(CAST(geom_hexewkb AS geometry),4326)), referencepoint FROM niva.gp_requests_contexts WHERE gp_requests_producers_id=:gp_requests_producers_id";
        // Fetches data about the contexts related to the request.

        /**
         * MT- Create Producers List & Contexts queries could be implemented as a
         * forward and a grouped backward one.
         */

        Query queryFetchUserId;
        List<Integer> resultFetchUserId;
        Integer agrisnapId;

        Query queryCreateARequest;
        List<Integer> resultCreateARequest;
        Integer requestId;

        Query queryCreateProducersList;
        List<Object[]> resultCreateProducersList;

        Query queryCreateContexts;
        List<Integer> resultCreateContexts;

        Query queryFetchRequestData;
        List<Object[]> resultFetchRequestData;
        Object[] responseRequestData;

        Short responseReqTypeCode;
        String responseReqTypeDescription;
        String responseReqScheme;
        Integer responseReqCorrespondenceId;
        Integer responseReqCorrespondenceDocNr;
        String responseReqGeotag;

        String responseJsonText;

        JSONObject obj;
        JSONObject objType;
        JSONArray objRequestProducers;

        Integer i;

        JSONObject objRequestProducersItem;

        Object[] gpRequestProducer;
        Integer gpRequestProducerId;
        Integer gpRequestProducerCode;

        Query queryFetchRequestProducersContexts;
        List<Object[]> resultFetchRequestProducersContexts;

        JSONArray objProducerContexts;

        Integer j;

        Object[] resultQueryProducersContext;

        JSONObject objProducerContext;

        Integer objProducerContextId;
        String objProducerContextHash;
        String objProducerContextType;
        String objProducerContextLabel;
        String objProducerContextComment;
        String objProducerContextGeoJson;
        String objProducerContextReferencepoint;

        /** Find the User */
        if (agrisnapUid == null) {
            return ErrorProvideParametersRequest;
        }

        queryFetchUserId = getEntityManager().createNativeQuery(sqlQueryFetchUserId);
        queryFetchUserId.setParameter("agrisnap_uid", agrisnapUid);
        resultFetchUserId = (List<Integer>) queryFetchUserId.getResultList();

        if (resultFetchUserId.isEmpty()) {
            return ErrorFetchUserId;
        } else {
            agrisnapId = resultFetchUserId.get(0);

        }

        /**
         * Now create a new Request into the Database, either an issue does exist or not
         */

        queryCreateARequest = getEntityManager().createNativeQuery(sqlQueryCreateARequest);
        queryCreateARequest.setParameter("agrisnap_users_id", agrisnapId);

        resultCreateARequest = (List<Integer>) queryCreateARequest.getResultList();

        if (resultCreateARequest.isEmpty()) {
            return ErrorCreateARequest;
        } else {
            requestId = resultCreateARequest.get(0);

        }

        /**
         * Now create the list of Producers related to this request.
         */

        queryCreateProducersList = getEntityManager().createNativeQuery(sqlQueryCreateProducersList);
        queryCreateProducersList.setParameter("gp_requests_id", requestId);
        queryCreateProducersList.setParameter("agrisnap_users_id", agrisnapId);
        resultCreateProducersList = (List<Object[]>) queryCreateProducersList.getResultList();

        if (resultCreateProducersList.isEmpty()) {
            return ErrorCreateProducersList;
        } else {

        }

        /**
         * Now create the Contexts related to this request.
         */

        queryCreateContexts = getEntityManager().createNativeQuery(sqlQueryCreateContexts);
        queryCreateContexts.setParameter("requestId", requestId);

        resultCreateContexts = (List<Integer>) queryCreateContexts.getResultList();

        /** Now create the JSON Object of the response. */

        queryFetchRequestData = getEntityManager().createNativeQuery(sqlQueryFetchRequestData);
        queryFetchRequestData.setParameter("gp_requests_id", requestId);

        resultFetchRequestData = queryFetchRequestData.getResultList();

        if (resultFetchRequestData.isEmpty()) {
            return ErrorFetchRequestData;
        } else {

        }

        responseRequestData = resultFetchRequestData.get(0);

        responseReqTypeCode = (Short) responseRequestData[0];
        responseReqTypeDescription = (String) responseRequestData[1];
        responseReqScheme = (String) responseRequestData[2];
        responseReqCorrespondenceId = (Integer) responseRequestData[3];
        responseReqCorrespondenceDocNr = (Integer) responseRequestData[4];
        responseReqGeotag = Character.toString( (Character) responseRequestData[5]);

        obj = new JSONObject();

        obj.put("id", agrisnapId);
        obj.put("hash", agrisnapId);

        objType = new JSONObject();
        objType.put("code", responseReqTypeCode);
        objType.put("description", responseReqTypeDescription);
        obj.put("type", objType);

        obj.put("scheme", responseReqScheme);
        obj.put("correspondenceId", responseReqCorrespondenceId);
        obj.put("correspondenceDocNo", responseReqCorrespondenceDocNr);
        obj.put("geotag", responseReqGeotag);

        /** Inject the producers & contexts. */

        objRequestProducers = new JSONArray();

        i = 0;

        while (i < resultCreateProducersList.size()) {

            objRequestProducersItem = new JSONObject();

            gpRequestProducer = resultCreateProducersList.get(i);
            gpRequestProducerId = (Integer) gpRequestProducer[0];
            gpRequestProducerCode = (Integer) gpRequestProducer[1];

            objRequestProducersItem.put("code", gpRequestProducerCode);

            queryFetchRequestProducersContexts = getEntityManager()
                    .createNativeQuery(sqlQueryFetchRequestProducersContexts);
            queryFetchRequestProducersContexts.setParameter("gp_requests_producers_id", gpRequestProducerId);

            resultFetchRequestProducersContexts = queryFetchRequestProducersContexts.getResultList();

            objProducerContexts = new JSONArray();

            j = 0;
            while (j < resultFetchRequestProducersContexts.size()) {

                resultQueryProducersContext = resultFetchRequestProducersContexts.get(j);

                objProducerContext = new JSONObject();

                objProducerContextId = (Integer) resultQueryProducersContext[0];
                objProducerContextHash = (String) resultQueryProducersContext[1];
                objProducerContextType = (String) resultQueryProducersContext[2];
                objProducerContextLabel = (String) resultQueryProducersContext[3];
                objProducerContextComment = (String) resultQueryProducersContext[4];
                objProducerContextGeoJson = (String) resultQueryProducersContext[5];
                objProducerContextReferencepoint = (String) resultQueryProducersContext[6];

                objProducerContext.put("id", objProducerContextId);
                objProducerContext.put("hash", objProducerContextHash);
                objProducerContext.put("type", objProducerContextType);
                objProducerContext.put("label", objProducerContextLabel);
                objProducerContext.put("comment", objProducerContextComment);
                objProducerContext.put("geoCoordinates", objProducerContextGeoJson);
                objProducerContext.put("referencePoint", objProducerContextReferencepoint);

                objProducerContexts.add(objProducerContext);

                j++;

            }
            objRequestProducersItem.put("contexts", objProducerContexts);

            i++;

            objRequestProducers.add(objRequestProducersItem);

        }

        obj.put("producers", objRequestProducers);

        responseJsonText = obj.toJSONString();

        /**
         * NOT YET IMPLEMENTED ! MT - Insert info about the request activity into the
         * parcels_issues_activities table
         */
        /**
         * INSERT INTO niva.parcels_issues_activities( parcels_issues_activities_id,
         * row_version, parcels_issues_id, type, type_extrainfo, dte_timestamp) VALUES
         * (?, ?, ?, ?, ?, ?);
         * 
         * query = getEntityManager().createNativeQuery(sqlQueryFetchRequestContexts);
         * query.setParameter("gp_requests_producers_id", gpRequestProducerId);
         * 
         * resultQueryContexts = query.getResultList();
         * 
         * if (resultQueryContexts.isEmpty()) { return null; } else {
         * 
         * }
         */

        return responseJsonText;

    }

    /**
     * MT - Manages Agrispan uploads.
     *
     * 
     * @param hash
     * @param attachment
     * @param data
     * @param environment
     * @param id
     * @return
     */
    @Override
    public String uploadPhoto(String hash, String attachment, String data, String environment, Integer id) {
        // hash: 32 chars about the related Context of the Request.
        // attachment: The Image in base64 encoded format.
        // data: The Metadata of the Image.
        // environment:
        // id: The id of the Request Context related to the response. (Additional to the
        // hash.)

        Integer photoId;

        final String ErrorPhotoNotUploaded = "Error: Photo couldn't be uploaded.";
        final String sqlQueryStoreUploadedData = "INSERT INTO niva.gp_uploads(image, data, environment, gp_requests_contexts_id, hash) VALUES (decode(:imageBase64, 'base64'), :data, :environment,:gp_requests_contexts_id, :hash) RETURNING gp_uploads_id";
        // Stores the Photo and Its metadata in the DB.
        JSONObject objResponse;

        Query query = getEntityManager().createNativeQuery(sqlQueryStoreUploadedData);
        query.setParameter("imageBase64", attachment);
        query.setParameter("data", data);
        query.setParameter("environment", environment);
        query.setParameter("gp_requests_contexts_id", id);
        query.setParameter("hash", hash);

        List<Integer> resultQuery = (List<Integer>) query.getResultList();

        if (resultQuery.isEmpty()) {
            return ErrorPhotoNotUploaded;
        } else {
            objResponse = new JSONObject();
            objResponse.put("photoId", resultQuery.get(0));
            return objResponse.toJSONString();
        }

    }

    /**
     * MT - Returns the list of BRE Runs Results Sets.
     *
     * 
     * @param info
     * @return
     */

    @Override
    public String getBRERunsResultsList(Boolean info, UserSession usrSession) {
        // info: if yes, return credits & help.

        final String sqlQueryGetDemaList = "SELECT dema_id, description FROM niva.decision_making;";
        Integer resultQuerySize;
        Integer i;
        Object[] objBRERunItem;
        JSONObject objResponseBRERunResultItem;
        JSONObject objResponse;
        Query query = getEntityManager().createNativeQuery(sqlQueryGetDemaList);
        JSONArray objResponseBRERunsResultsSets;

        List<Object[]> resultQuery = query.getResultList();

        resultQuerySize = resultQuery.size();
        objResponse = new JSONObject();

        objResponseBRERunsResultsSets = new JSONArray();

        i = 0;

        while (i < resultQuerySize) {

            objBRERunItem = resultQuery.get(i);

            objResponseBRERunResultItem = new JSONObject();

            objResponseBRERunResultItem.put("id", objBRERunItem[0]);
            objResponseBRERunResultItem.put("description", objBRERunItem[1]);

            objResponseBRERunsResultsSets.add(objResponseBRERunResultItem);

            i++;
        }

        objResponse.put("BRERunsResultsSets", objResponseBRERunsResultsSets);

        return objResponse.toJSONString();

    }

    /**
     * MT - Returns the results of a BRE Run.
     *
     * 
     * @param info
     * @param id
     * @return
     */

    @Override
    public String getBRERunResults(Boolean info, Integer id, UserSession usrSession) {
        // info: if yes, return credits & help.
        // id: the Id of the BRE Run.

        /*
         * 
         * final String sqlGetBRERunResults =
         * "SELECT parcel_decision.decision_light,parcel_class.prod_code as producerCode, parcel_class.code as parcelCode, parcel_class.identifier, cultivationdecl.code as cultivationDeclaredCode, cultivationdecl.name as cultivationDeclaredName, cultivation1.code as cultivation1Code, cultivation1.name as cultivation1Name, parcel_class.prob_pred as probabilityPrediction1,cultivation2.code as cultivation2Code, cultivation2.name as cultivation2name, parcel_class.prob_pred2 as probabilityPrediction2"
         * + " FROM niva.parcel_decision parcel_decision" +
         * " JOIN niva.parcel_class parcel_class ON parcel_decision.pcla_id=parcel_class.pcla_id"
         * + " JOIN niva.parcel parcel ON parcel_class.parc_id=parcel_class.parc_id" +
         * " JOIN niva.cultivation cultivationdecl ON parcel_class.cult_id_decl=cultivationdecl.cult_id"
         * +
         * " JOIN niva.cultivation cultivation1 ON parcel_class.cult_id_pred=cultivation1.cult_id"
         * +
         * " JOIN niva.cultivation cultivation2 ON parcel_class.cult_id_pred2=cultivation2.cult_id"
         * + " WHERE parcel_decision.dema_id=:demaId";
         * 
         */

        final String sqlGetBRERunResults = "SELECT parcel_decision.decision_light,parcel_class.prod_code as producerCode, parcel_class.parc_code as parcelCode, parcel_class.parc_identifier as identifier, cultivationdecl.code as cultivationDeclaredCode, cultivationdecl.name as cultivationDeclaredName, cultivation1.code as cultivation1Code, cultivation1.name as cultivation1Name, parcel_class.prob_pred as probabilityPrediction1,cultivation2.code as cultivation2Code, cultivation2.name as cultivation2name, parcel_class.prob_pred2 as probabilityPrediction2"
                + " FROM niva.parcel_decision parcel_decision"
                + " JOIN niva.parcel_class parcel_class ON parcel_decision.pcla_id=parcel_class.pcla_id"
                + " JOIN niva.cultivation cultivationdecl ON parcel_class.cult_id_decl=cultivationdecl.cult_id"
                + " JOIN niva.cultivation cultivation1 ON parcel_class.cult_id_pred=cultivation1.cult_id"
                + " JOIN niva.cultivation cultivation2 ON parcel_class.cult_id_pred2=cultivation2.cult_id"
                + " WHERE parcel_decision.dema_id=:demaId";

        Integer demaId;
        Integer resultQuerySize;
        Integer i;
        Object[] objBRERunResultsItem;
        JSONObject objResponseBRERunResultsItem;
        JSONObject objResponse;
        JSONArray objResponseBRERunResults;
        JSONObject objResponseBRERunResultsItemCultivationDeclared;
        JSONObject objResponseBRERunResultsItemCultivation1;
        JSONObject objResponseBRERunResultsItemCultivation2;

        Query query = getEntityManager().createNativeQuery(sqlGetBRERunResults);

        demaId = id;

        query.setParameter("demaId", demaId);
        List<Object[]> resultQuery = query.getResultList();

        resultQuerySize = resultQuery.size();
        objResponse = new JSONObject();

        objResponseBRERunResults = new JSONArray();

        i = 0;

        while (i < resultQuerySize) {

            objBRERunResultsItem = resultQuery.get(i);

            objResponseBRERunResultsItem = new JSONObject();

            objResponseBRERunResultsItem.put("decisionLight", (Integer) objBRERunResultsItem[0]);
            objResponseBRERunResultsItem.put("producerCode", (Integer) objBRERunResultsItem[1]);
            objResponseBRERunResultsItem.put("parcelCode", (String) objBRERunResultsItem[2]);
            objResponseBRERunResultsItem.put("identifier", (String) objBRERunResultsItem[3]);

            objResponseBRERunResultsItemCultivationDeclared = new JSONObject();
            objResponseBRERunResultsItemCultivationDeclared.put("code", (Integer) objBRERunResultsItem[4]);
            objResponseBRERunResultsItemCultivationDeclared.put("name", (String) objBRERunResultsItem[5]);
            objResponseBRERunResultsItem.put("cultivationDeclared", objResponseBRERunResultsItemCultivationDeclared);

            objResponseBRERunResultsItemCultivation1 = new JSONObject();
            objResponseBRERunResultsItemCultivation1.put("code", (Integer) objBRERunResultsItem[6]);
            objResponseBRERunResultsItemCultivation1.put("name", (String) objBRERunResultsItem[7]);
            objResponseBRERunResultsItemCultivation1.put("probability", (Double) objBRERunResultsItem[8]);
            objResponseBRERunResultsItem.put("cultivationPredicted1", objResponseBRERunResultsItemCultivation1);

            objResponseBRERunResultsItemCultivation2 = new JSONObject();
            objResponseBRERunResultsItemCultivation2.put("code", (Integer) objBRERunResultsItem[9]);
            objResponseBRERunResultsItemCultivation2.put("name", (String) objBRERunResultsItem[10]);
            objResponseBRERunResultsItemCultivation2.put("probability", (Double) objBRERunResultsItem[11]);
            objResponseBRERunResultsItem.put("cultivationPredicted2", objResponseBRERunResultsItemCultivation2);

            objResponseBRERunResults.add(objResponseBRERunResultsItem);

            i++;
        }

        objResponse.put("BRERunResults", objResponseBRERunResults);

        return objResponse.toJSONString();

    }

    /**
     * MT - Manages FMIS requests.
     *
     * 
     * @param fmisUid
     * @return
     */

    @Override
    public String getFMISRequests(String fmisUid) {
        // fmisUid: a registred FMIS User Id (5 Chars).

        final String ErrorProvideParametersRequest = "Error: Please, provide the needed parameters.";
        // Response in case needed parameters are not provided, or agrisnapUid is not
        // registered.
        final String ErrorUidNotRegistered = "Error: User Not Found.";

        final String sqlQueryFetchUserId = "Select fmis_users_id From niva.fmis_users Where fmis_uid = :fmis_uid";
        // Query to fetch the User Id related to the fmisUid.

        // final String sqlQueryInsertIntoParcelsIssuesActivities = "INSERT INTO
        // niva.parcels_issues_activities(parcels_issues_id, type, type_extrainfo)
        // VALUES (:parcels_issues_id, :type, :type_extrainfo)";
        // Insert a record for the request activity into the DB for logging.

        final String sqlQueryFetchRequestData = "SELECT pi.parcels_issues_id, pi.type_of_issue, pc.clas_id, pc.parc_identifier, pc.parc_code, pc.prod_code FROM niva.parcels_issues AS pi JOIN niva.parcel_class AS pc ON pi.pcla_id=pc.pcla_id WHERE pi.status<900";
        // Fetches data related to a request.

        Integer fmisId;
        Integer i;

        JSONArray jResponseArray;
        JSONObject jResponseItem;

        Integer resultQuerySize;

        Integer reqParcelsIssuesId;
        Short reqTypeOfIssue;
        Integer reqClasId;
        String reqParcIdentifier;
        String reqParcCode;
        Integer reqProdCode;

        /** Find the User */
        if (fmisUid == null) {
            return ErrorProvideParametersRequest;
        }

        Query query = getEntityManager().createNativeQuery(sqlQueryFetchUserId);
        query.setParameter("fmis_uid", fmisUid);
        List<Integer> resultFetchUserId = (List<Integer>) query.getResultList();

        if (resultFetchUserId.isEmpty()) {
            return ErrorUidNotRegistered;
        } else {
            fmisId = resultFetchUserId.get(0);

        }

        /** Now create the JSON Object of the response. */

        String jsonText = "";

        query = getEntityManager().createNativeQuery(sqlQueryFetchRequestData);

        List<Object[]> resultQueryRequest = query.getResultList();

        resultQuerySize = resultQueryRequest.size();

        jResponseArray = new JSONArray();

        i = 0;

        while (i < resultQuerySize) {

            Object[] resultQueryRequestItem = resultQueryRequest.get(i);

            reqParcelsIssuesId = (Integer) resultQueryRequestItem[0];
            reqTypeOfIssue = (Short) resultQueryRequestItem[1];
            reqClasId = (Integer) resultQueryRequestItem[2];
            reqParcIdentifier = (String) resultQueryRequestItem[3];
            reqParcCode = (String) resultQueryRequestItem[4];
            reqProdCode = (Integer) resultQueryRequestItem[5];

            jResponseItem = new JSONObject();
            jResponseItem.put("id", reqParcelsIssuesId);
            jResponseItem.put("type", reqTypeOfIssue);
            jResponseItem.put("clasId", reqClasId);
            jResponseItem.put("parcelIdentifier", reqParcIdentifier);
            jResponseItem.put("parcelCode", reqParcCode);
            jResponseItem.put("producerCode", reqProdCode);

            jResponseArray.add(jResponseItem);

            i++;
        }

        return jResponseArray.toJSONString();

    }

    /**
     * MT - Manages the FMIS Uploads.
     *
     * @param fmisUid
     * @param attachment
     * @param metadata
     * @param id
     * @return
     */
    @Override
    public String uploadFMIS(String fmisUid, Integer id, String attachment, String metadata) {

        // fmsUid: THe 5 chars-long Uid.
        // id: The id of the parcels_issues.
        // attachment: The File in base64 encoded format.
        // metadata: The Metadata of the File.

        Integer photoId;

        final String ErrorFMISNotUploaded = "Error: FMIS document couldn't be uploaded.";
        final String sqlQueryStoreUploadedData = "INSERT INTO niva.fmis_uploads(metadata, docfile, parcels_issues_id ) VALUES (:metadata, decode(:docBase64, 'base64'), :parcels_issues_id) RETURNING fmis_uploads_id";
        // Stores the Doc and Its metadata in the DB.
        JSONObject objResponse;

        Query query = getEntityManager().createNativeQuery(sqlQueryStoreUploadedData);
        query.setParameter("docBase64", attachment);
        query.setParameter("metadata", metadata);
        query.setParameter("parcels_issues_id", id);

        List<Integer> resultQuery = (List<Integer>) query.getResultList();

        if (resultQuery.isEmpty()) {
            return ErrorFMISNotUploaded;
        } else {
            objResponse = new JSONObject();
            objResponse.put("docId", resultQuery.get(0));
            return objResponse.toJSONString();
        }

    }

    /**
     * Updates Integrated Decisions for a Parcel taking into account the Geotagged
     * Photos & FMIS Calendars. MT202105
     *
     * 
     * @param pclaId
     * @param usrSession
     * 
     * @return
     */

    @Override
    public List<String> updateIntegratedDecisionsNIssues(Integer pclaId, UserSession usrSession) {

        // System.out.println("Update Decisions Started");

        final String sqlUpdateIntDec = "UPDATE niva.integrateddecisions AS id SET dte_update=now(), usr_update=:usrUpdate,"
                + " decision_code=(SELECT CASE WHEN pd.decision_light!=1 THEN"
                + " (CASE WHEN ecg.crop_level=0 AND fmisd.cult_id=pc.cult_id_decl THEN 1"
                + " WHEN ecg.crop_level=0 AND gpd.cult_id=pc.cult_id_decl THEN 1"
                + " WHEN ecg.crop_level=1 AND fmisd.coty_id=pc.coty_id_decl THEN 1"
                + " WHEN ecg.crop_level=1 AND gpd.coty_id=pc.coty_id_decl THEN 1"
                + " WHEN ecg.crop_level=0 AND fmisd.cult_id!=pc.cult_id_decl THEN 3"
                + " WHEN ecg.crop_level=0 AND gpd.cult_id!=pc.cult_id_decl THEN 3"
                + " WHEN ecg.crop_level=1 AND fmisd.coty_id!=pc.coty_id_decl THEN 3"
                + " WHEN ecg.crop_level=1 AND gpd.coty_id!=pc.coty_id_decl THEN 3"
                + " WHEN ecg.crop_level=2 AND (fmisd.cult_id=pc.cult_id_decl OR fmisd.coty_id=pc.coty_id_decl OR gpd.cult_id=pc.cult_id_decl OR gpd.coty_id=pc.coty_id_decl) THEN 1"
                + " WHEN ecg.crop_level=2 AND NOT (fmisd.cult_id=pc.cult_id_decl OR fmisd.coty_id=pc.coty_id_decl OR gpd.cult_id=pc.cult_id_decl OR gpd.coty_id=pc.coty_id_decl) THEN 3"
                + " WHEN ecg.crop_level=0 AND ((gpd.cult_id=pc.cult_id_decl AND fmisd.cult_id!=pc.cult_id_decl) OR (gpd.cult_id!=pc.cult_id_decl AND fmisd.cult_id=pc.cult_id_decl)) THEN 4"
                + " WHEN ecg.crop_level=1 AND ((gpd.coty_id=pc.coty_id_decl AND fmisd.coty_id!=pc.coty_id_decl) OR (gpd.coty_id!=pc.coty_id_decl AND fmisd.coty_id=pc.coty_id_decl)) THEN 4"
                + " WHEN ecg.crop_level=2 AND ((gpd.cult_id=pc.cult_id_decl AND fmisd.cult_id!=pc.cult_id_decl) OR (gpd.cult_id!=pc.cult_id_decl AND fmisd.cult_id=pc.cult_id_decl)) AND ((gpd.coty_id=pc.coty_id_decl AND fmisd.coty_id!=pc.coty_id_decl) OR (gpd.coty_id!=pc.coty_id_decl AND fmisd.coty_id=pc.coty_id_decl)) THEN 4"
                + " ELSE pd.decision_light END) ELSE 1 END AS decision_code"
                + " FROM niva.parcel_decision AS pd JOIN niva.decision_making AS dm ON pd.dema_id=dm.dema_id"
                + " JOIN niva.ec_group AS ecg ON dm.ecgr_id=ecg.ecgr_id"
                + " JOIN niva.parcel_class AS pc ON pd.pcla_id=pc.pcla_id"
                + " LEFT JOIN niva.parcels_issues AS pi ON pc.pcla_id=pi.pcla_id"
                + " LEFT JOIN (SELECT parcels_issues_id, t1.cult_id, t1.coty_id FROM niva.gp_decisions t1 WHERE t1.gp_decisions_id IN (SELECT gp_decisions_id FROM niva.gp_decisions t2 WHERE t1.parcels_issues_id=t2.parcels_issues_id ORDER BY dte_insert DESC LIMIT 1)) AS gpd ON pi.parcels_issues_id=gpd.parcels_issues_id"
                + " LEFT JOIN (SELECT parcels_issues_id, t1.cult_id, t1.coty_id FROM niva.fmis_decisions t1 WHERE t1.fmis_decisions_id IN (SELECT fmis_decisions_id FROM niva.fmis_decisions t2 WHERE t1.parcels_issues_id=t2.parcels_issues_id ORDER BY dte_insert DESC LIMIT 1)) AS  fmisd ON pi.parcels_issues_id=fmisd.parcels_issues_id"
                + " WHERE pd.pcla_id=:pclaId) WHERE id.pcla_id=:pclaId";

        Query query = getEntityManager().createNativeQuery(sqlUpdateIntDec);
        query.setParameter("pclaId", pclaId);
        query.setParameter("usrUpdate", usrSession.usrEmail);

        Integer rowsAffected = query.executeUpdate();

        SaveResponse ret = new SaveResponse();
        List<String> returnWarnings = new ArrayList<>();
        returnWarnings.add(ret.warningMessages);
        return returnWarnings;
    }

    /**
     * Populates Integrated Decisions. MT202106
     *
     * 
     * @param demaId
     * @param usrSession
     * 
     * @return
     */
    @Override
    public List<String> updateIntegratedDecisionsNIssuesFromDema(Integer demaId, UserSession usrSession) {

        // System.out.println("Update Decisions Started. demaId="+demaId);

        final String sqlCreateIntDec = "INSERT INTO niva.integrateddecisions(pcla_id, decision_code, usr_update, dema_id)"
                // Create new records in integrateddecisions, with calculated decision_light for
                // each parcel and decision making.
                + " SELECT pd.pcla_id, CASE" + " WHEN pd.decision_light!=1 THEN"
                + " (CASE WHEN ecg.crop_level=0 AND fmisd.cult_id=pc.cult_id_decl THEN 1"
                + " WHEN ecg.crop_level=0 AND gpd.cult_id=pc.cult_id_decl THEN 1"
                + " WHEN ecg.crop_level=1 AND fmisd.coty_id=pc.coty_id_decl THEN 1"
                + " WHEN ecg.crop_level=1 AND gpd.coty_id=pc.coty_id_decl THEN 1"
                + " WHEN ecg.crop_level=0 AND fmisd.cult_id!=pc.cult_id_decl THEN 3"
                + " WHEN ecg.crop_level=0 AND gpd.cult_id!=pc.cult_id_decl THEN 3"
                + " WHEN ecg.crop_level=1 AND fmisd.coty_id!=pc.coty_id_decl THEN 3"
                + " WHEN ecg.crop_level=1 AND gpd.coty_id!=pc.coty_id_decl THEN 3"
                + " WHEN ecg.crop_level=2 AND (fmisd.cult_id=pc.cult_id_decl OR fmisd.coty_id=pc.coty_id_decl OR gpd.cult_id=pc.cult_id_decl OR gpd.coty_id=pc.coty_id_decl) THEN 1"
                + " WHEN ecg.crop_level=2 AND NOT (fmisd.cult_id=pc.cult_id_decl OR fmisd.coty_id=pc.coty_id_decl OR gpd.cult_id=pc.cult_id_decl OR gpd.coty_id=pc.coty_id_decl) THEN 3"
                + " WHEN ecg.crop_level=0 AND ((gpd.cult_id=pc.cult_id_decl AND fmisd.cult_id!=pc.cult_id_decl) OR (gpd.cult_id!=pc.cult_id_decl AND fmisd.cult_id=pc.cult_id_decl)) THEN 4"
                + " WHEN ecg.crop_level=1 AND ((gpd.coty_id=pc.coty_id_decl AND fmisd.coty_id!=pc.coty_id_decl) OR (gpd.coty_id!=pc.coty_id_decl AND fmisd.coty_id=pc.coty_id_decl)) THEN 4"
                + " WHEN ecg.crop_level=2 AND ((gpd.cult_id=pc.cult_id_decl AND fmisd.cult_id!=pc.cult_id_decl) OR (gpd.cult_id!=pc.cult_id_decl AND fmisd.cult_id=pc.cult_id_decl)) AND ((gpd.coty_id=pc.coty_id_decl AND fmisd.coty_id!=pc.coty_id_decl) OR (gpd.coty_id!=pc.coty_id_decl AND fmisd.coty_id=pc.coty_id_decl)) THEN 4"
                + " ELSE pd.decision_light END)" + " ELSE pd.decision_light END AS decision_code,"
                + " :usrUpdate AS usr_update, pd.dema_id" + " FROM" + " niva.parcel_decision AS pd"
                + " JOIN niva.decision_making AS dm ON pd.dema_id=dm.dema_id"
                + " JOIN niva.ec_group AS ecg ON dm.ecgr_id=ecg.ecgr_id"
                + " JOIN niva.parcel_class AS pc ON pd.pcla_id=pc.pcla_id"
                + " LEFT JOIN niva.parcels_issues AS pi ON pc.pcla_id=pi.pcla_id"
                + " LEFT JOIN (SELECT parcels_issues_id, t1.cult_id, t1.coty_id FROM niva.gp_decisions t1 WHERE t1.gp_decisions_id IN (SELECT gp_decisions_id FROM niva.gp_decisions t2 WHERE t1.parcels_issues_id=t2.parcels_issues_id ORDER BY dte_insert DESC LIMIT 1)) AS gpd ON pi.parcels_issues_id=gpd.parcels_issues_id"
                + " LEFT JOIN (SELECT parcels_issues_id, t1.cult_id, t1.coty_id FROM niva.fmis_decisions t1 WHERE t1.fmis_decisions_id IN (SELECT fmis_decisions_id FROM niva.fmis_decisions t2 WHERE t1.parcels_issues_id=t2.parcels_issues_id ORDER BY dte_insert DESC LIMIT 1)) AS  fmisd ON pi.parcels_issues_id=fmisd.parcels_issues_id"
                + " WHERE pd.dema_id= :demaId";

        Query query1 = getEntityManager().createNativeQuery(sqlCreateIntDec);
        query1.setParameter("demaId", demaId);
        query1.setParameter("usrUpdate", usrSession.usrEmail);

        Integer rowsAffected1 = query1.executeUpdate();

        // System.out.println("Update Decisions - Row affected: " + rowsAffected);

        final String sqlCreateNewIssues = "INSERT INTO niva.parcels_issues (pcla_id, status, type_of_issue, active)"
                // Create new rows for issues in parcels_issues
                + " (SELECT id.pcla_id, 1,0,true FROM niva.integrateddecisions AS id WHERE id.pcla_id NOT IN (SELECT sub2pi.pcla_id FROM niva.parcels_issues AS sub2pi) AND (id.decision_code=2 OR id.decision_code=3) AND id.dema_id=:demaId )";

        Query query2 = getEntityManager().createNativeQuery(sqlCreateNewIssues);
        query2.setParameter("demaId", demaId);

        Integer rowsAffected2 = query2.executeUpdate();

        final String sqlUpdateNewIssues = "UPDATE niva.parcels_issues AS pi SET active=true, dte_status_update=now(), type_of_issue=type_of_issue|"
                // Update new issues
                + "(SELECT" + " CASE WHEN sub1ecg.crop_level=0 THEN 1" + " WHEN sub1ecg.crop_level=1 THEN 2"
                + " WHEN sub1ecg.crop_level=2 THEN 3 END AS type_of_issue" + " FROM niva.decision_making AS sub1dm"
                + " JOIN niva.ec_group AS sub1ecg ON sub1dm.ecgr_id=sub1ecg.ecgr_id" + " WHERE sub1dm.dema_id=:demaId )"
                + " WHERE pi.pcla_id IN (SELECT sub2id.pcla_id FROM niva.integrateddecisions AS sub2id WHERE sub2id.dema_id=:demaId )";

        Query query3 = getEntityManager().createNativeQuery(sqlUpdateNewIssues);
        query3.setParameter("demaId", demaId);

        Integer rowsAffected3 = query3.executeUpdate();

        DecisionMaking dema = getDecisionMakingFacade().findByDemaId(demaId);

        dema.setHasPopulatedIntegratedDecisionsAndIssues(true);

        SaveResponse ret = new SaveResponse();
        List<String> returnWarnings = new ArrayList<>();
        returnWarnings.add(ret.warningMessages);

        return returnWarnings;

    }

    /*
     * Push To Commons API - MT202106
     */

    public List<String> actionPushToCommonsAPI(Integer demaId, UserSession usrSession) {

        // System.out.println("Entered into actionPushToCommonsAPI");

        DecisionMaking dema = getDecisionMakingFacade().findByDemaId(demaId);
        Integer ecgrId = (Integer) dema.getEcgrId().getEcgrId();
        EcGroup ecGroup = getEcGroupFacade().findByEcgrId(ecgrId);

        // System.out.println("dema was initilialized");

        // StaticLists
        java.util.HashMap<Integer, String> decisLightDomain = new java.util.HashMap<>();
        decisLightDomain.put(1, "Green");
        decisLightDomain.put(2, "Yellow");
        decisLightDomain.put(3, "Red");
        decisLightDomain.put(4, "Undefined");

        final String putpathPrefix = "http://np-niva.neuropublic.gr/importer/layers/crete/parcels/";

        String putpathfull;
        ClientRequest putrequest;
        ClientResponse response;

        JSONObject obj0Main = new JSONObject();
        JSONObject obj1Timestamp = new JSONObject();
        obj1Timestamp.put("name", ecGroup.getName());
        obj1Timestamp.put("desc", dema.getDescription());
        JSONObject obj2Output = new JSONObject();

        String auth = "demo:123456";
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));
        String authHeader = "Basic " + new String(encodedAuth);

        // http://np-niva.neuropublic.gr/importer/layers/komotini/parcels/1111111111/metadata

        // now for each parcel retrieve its integrated decision

        for (Integrateddecision idparcel : dema.getIntegrateddecisionCollection()) {

            obj2Output.put("decisionlight", decisLightDomain.get((int) idparcel.getDecisionCode()));
            obj1Timestamp.put("output", obj2Output);
            obj0Main.put(dema.getDateTime().getTime(), obj1Timestamp);

            putpathfull = putpathPrefix + idparcel.getPclaId().getParcIdentifier() + "/metadata";

            putrequest = new ClientRequest(putpathfull);
            // Set the accept header to tell the accepted response format
            putrequest.body("application/json", obj0Main);
            // HttpHeaders = putrequest.getHttpHeaders();
            putrequest.header("Authorization", authHeader);

            // Send the request
            try {
                response = putrequest.put();

                // First validate the api status code
                int apiResponseCode = response.getResponseStatus().getStatusCode();
                if (response.getResponseStatus().getStatusCode() != 200) {
                    throw new RuntimeException("Failed with HTTP error code : " + apiResponseCode);
                }

            } catch (Exception e) {
                throw new RuntimeException("Push Decision Lights failed!");
            }
            ;

        }

        SaveResponse ret = new SaveResponse();
        List<String> returnWarnings = new ArrayList<>();
        returnWarnings.add(ret.warningMessages);

        return returnWarnings;
    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
