/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ProducersBase.ts" />
/// <reference path="../Controllers/LovProducers.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovProducersBase = (function (_super) {
        __extends(ModelLovProducersBase, _super);
        function ModelLovProducersBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._fsch_ProducersLov_prodName = new NpTypes.UIStringModel(undefined);
            this._fsch_ProducersLov_prodCode = new NpTypes.UINumberModel(undefined);
        }
        Object.defineProperty(ModelLovProducersBase.prototype, "fsch_ProducersLov_prodName", {
            get: function () {
                return this._fsch_ProducersLov_prodName.value;
            },
            set: function (vl) {
                this._fsch_ProducersLov_prodName.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "fsch_ProducersLov_prodCode", {
            get: function () {
                return this._fsch_ProducersLov_prodCode.value;
            },
            set: function (vl) {
                this._fsch_ProducersLov_prodCode.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovProducersBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelLovProducersBase;
    })(Controllers.AbstractLovModel);
    Controllers.ModelLovProducersBase = ModelLovProducersBase;
    var ControllerLovProducersBase = (function (_super) {
        __extends(ControllerLovProducersBase, _super);
        function ControllerLovProducersBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 5, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            $scope.clearBtnAction = function () {
                self.model.fsch_ProducersLov_prodName = undefined;
                self.model.fsch_ProducersLov_prodCode = undefined;
                self.updateGrid();
            };
            $scope._disabled =
                function () {
                    return self._disabled();
                };
            $scope._invisible =
                function () {
                    return self._invisible();
                };
            $scope.LovProducers_fsch_ProducersLov_prodName_disabled =
                function () {
                    return self.LovProducers_fsch_ProducersLov_prodName_disabled();
                };
            $scope.LovProducers_fsch_ProducersLov_prodCode_disabled =
                function () {
                    return self.LovProducers_fsch_ProducersLov_prodCode_disabled();
                };
            $scope.modelLovProducers = model;
            self.updateUI();
        }
        ControllerLovProducersBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerLovProducersBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerLovProducers";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerLovProducersBase.prototype, "HtmlDivId", {
            get: function () {
                return "Producers_Id";
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovProducersBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName);
            }
            var ret = [
                { cellClass: 'cellToolTip', field: 'prodName', displayName: 'getALString("Full Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '76%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.prodName' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'prodCode', displayName: 'getALString("Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.prodCode' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter((function (cl) { return cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true; }));
            if (self.isMultiSelect()) {
                ret.splice(0, 0, { width: '22', cellTemplate: "<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined });
            }
            return ret;
        };
        ControllerLovProducersBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.Producers.fromJSONComplete(webResponse.data);
        };
        Object.defineProperty(ControllerLovProducersBase.prototype, "modelLovProducers", {
            get: function () {
                var self = this;
                return self.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovProducersBase.prototype.initializeModelWithPreviousValues = function () {
            var self = this;
            var prevModel = self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovProducers.fsch_ProducersLov_prodName = prevModel.fsch_ProducersLov_prodName;
                self.modelLovProducers.fsch_ProducersLov_prodCode = prevModel.fsch_ProducersLov_prodCode;
            }
        };
        ControllerLovProducersBase.prototype._disabled = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovProducersBase.prototype._invisible = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovProducersBase.prototype.LovProducers_fsch_ProducersLov_prodName_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovProducersBase.prototype.LovProducers_fsch_ProducersLov_prodCode_disabled = function () {
            var self = this;
            return false;
        };
        return ControllerLovProducersBase;
    })(Controllers.LovController);
    Controllers.ControllerLovProducersBase = ControllerLovProducersBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovProducersBase.js.map