/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="../Controllers/LovParcelClas.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovParcelClasBase = (function (_super) {
        __extends(ModelLovParcelClasBase, _super);
        function ModelLovParcelClasBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._fsch_ParcelClasLov_probPred = new NpTypes.UINumberModel(undefined);
            this._fsch_ParcelClasLov_probPred2 = new NpTypes.UINumberModel(undefined);
            this._fsch_ParcelClasLov_prodCode = new NpTypes.UINumberModel(undefined);
            this._fsch_ParcelClasLov_parcIdentifier = new NpTypes.UIStringModel(undefined);
        }
        Object.defineProperty(ModelLovParcelClasBase.prototype, "fsch_ParcelClasLov_probPred", {
            get: function () {
                return this._fsch_ParcelClasLov_probPred.value;
            },
            set: function (vl) {
                this._fsch_ParcelClasLov_probPred.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "fsch_ParcelClasLov_probPred2", {
            get: function () {
                return this._fsch_ParcelClasLov_probPred2.value;
            },
            set: function (vl) {
                this._fsch_ParcelClasLov_probPred2.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "fsch_ParcelClasLov_prodCode", {
            get: function () {
                return this._fsch_ParcelClasLov_prodCode.value;
            },
            set: function (vl) {
                this._fsch_ParcelClasLov_prodCode.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "fsch_ParcelClasLov_parcIdentifier", {
            get: function () {
                return this._fsch_ParcelClasLov_parcIdentifier.value;
            },
            set: function (vl) {
                this._fsch_ParcelClasLov_parcIdentifier.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelClasBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelLovParcelClasBase;
    })(Controllers.AbstractLovModel);
    Controllers.ModelLovParcelClasBase = ModelLovParcelClasBase;
    var ControllerLovParcelClasBase = (function (_super) {
        __extends(ControllerLovParcelClasBase, _super);
        function ControllerLovParcelClasBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 5, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            $scope.clearBtnAction = function () {
                self.model.fsch_ParcelClasLov_probPred = undefined;
                self.model.fsch_ParcelClasLov_probPred2 = undefined;
                self.model.fsch_ParcelClasLov_prodCode = undefined;
                self.model.fsch_ParcelClasLov_parcIdentifier = undefined;
                self.updateGrid();
            };
            $scope._disabled =
                function () {
                    return self._disabled();
                };
            $scope._invisible =
                function () {
                    return self._invisible();
                };
            $scope.LovParcelClas_fsch_ParcelClasLov_probPred_disabled =
                function () {
                    return self.LovParcelClas_fsch_ParcelClasLov_probPred_disabled();
                };
            $scope.LovParcelClas_fsch_ParcelClasLov_probPred2_disabled =
                function () {
                    return self.LovParcelClas_fsch_ParcelClasLov_probPred2_disabled();
                };
            $scope.LovParcelClas_fsch_ParcelClasLov_prodCode_disabled =
                function () {
                    return self.LovParcelClas_fsch_ParcelClasLov_prodCode_disabled();
                };
            $scope.LovParcelClas_fsch_ParcelClasLov_parcIdentifier_disabled =
                function () {
                    return self.LovParcelClas_fsch_ParcelClasLov_parcIdentifier_disabled();
                };
            $scope.modelLovParcelClas = model;
            self.updateUI();
        }
        ControllerLovParcelClasBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerLovParcelClasBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerLovParcelClas";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerLovParcelClasBase.prototype, "HtmlDivId", {
            get: function () {
                return "ParcelClas_Id";
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovParcelClasBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName);
            }
            var ret = [
                { cellClass: 'cellToolTip', field: 'prodCode', displayName: 'getALString("Κωδικός", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.prodCode' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'parcIdentifier', displayName: 'getALString("parcIdentifier", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '41%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.parcIdentifier' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'parcCode', displayName: 'getALString("Κωδικός", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '41%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.parcCode' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter((function (cl) { return cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true; }));
            if (self.isMultiSelect()) {
                ret.splice(0, 0, { width: '22', cellTemplate: "<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined });
            }
            return ret;
        };
        ControllerLovParcelClasBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.ParcelClas.fromJSONComplete(webResponse.data);
        };
        Object.defineProperty(ControllerLovParcelClasBase.prototype, "modelLovParcelClas", {
            get: function () {
                var self = this;
                return self.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovParcelClasBase.prototype.initializeModelWithPreviousValues = function () {
            var self = this;
            var prevModel = self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovParcelClas.fsch_ParcelClasLov_probPred = prevModel.fsch_ParcelClasLov_probPred;
                self.modelLovParcelClas.fsch_ParcelClasLov_probPred2 = prevModel.fsch_ParcelClasLov_probPred2;
                self.modelLovParcelClas.fsch_ParcelClasLov_prodCode = prevModel.fsch_ParcelClasLov_prodCode;
                self.modelLovParcelClas.fsch_ParcelClasLov_parcIdentifier = prevModel.fsch_ParcelClasLov_parcIdentifier;
            }
        };
        ControllerLovParcelClasBase.prototype._disabled = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovParcelClasBase.prototype._invisible = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovParcelClasBase.prototype.LovParcelClas_fsch_ParcelClasLov_probPred_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovParcelClasBase.prototype.LovParcelClas_fsch_ParcelClasLov_probPred2_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovParcelClasBase.prototype.LovParcelClas_fsch_ParcelClasLov_prodCode_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovParcelClasBase.prototype.LovParcelClas_fsch_ParcelClasLov_parcIdentifier_disabled = function () {
            var self = this;
            return false;
        };
        return ControllerLovParcelClasBase;
    })(Controllers.LovController);
    Controllers.ControllerLovParcelClasBase = ControllerLovParcelClasBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovParcelClasBase.js.map