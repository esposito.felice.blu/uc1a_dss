/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
/// <reference path="LovDecisionMakingBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
/// <reference path="LovEcGroupBase.ts" />
/// <reference path="../Controllers/DecisionMakingROSearch.ts" />
var Controllers;
(function (Controllers) {
    var ModelDecisionMakingROSearchBase = (function (_super) {
        __extends(ModelDecisionMakingROSearchBase, _super);
        function ModelDecisionMakingROSearchBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._demaId = new NpTypes.UIManyToOneModel(undefined);
            this._clasId = new NpTypes.UIManyToOneModel(undefined);
            this._ecgrId = new NpTypes.UIManyToOneModel(undefined);
            this._dateTime = new NpTypes.UIDateModel(undefined);
        }
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "demaId", {
            get: function () {
                return this._demaId.value;
            },
            set: function (vl) {
                this._demaId.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "clasId", {
            get: function () {
                return this._clasId.value;
            },
            set: function (vl) {
                this._clasId.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "ecgrId", {
            get: function () {
                return this._ecgrId.value;
            },
            set: function (vl) {
                this._ecgrId.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "dateTime", {
            get: function () {
                return this._dateTime.value;
            },
            set: function (vl) {
                this._dateTime.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingROSearchBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelDecisionMakingROSearchBase;
    })(Controllers.AbstractSearchPageModel);
    Controllers.ModelDecisionMakingROSearchBase = ModelDecisionMakingROSearchBase;
    var ModelDecisionMakingROSearchPersistedModel = (function () {
        function ModelDecisionMakingROSearchPersistedModel() {
        }
        return ModelDecisionMakingROSearchPersistedModel;
    })();
    Controllers.ModelDecisionMakingROSearchPersistedModel = ModelDecisionMakingROSearchPersistedModel;
    var ControllerDecisionMakingROSearchBase = (function (_super) {
        __extends(ControllerDecisionMakingROSearchBase, _super);
        function ControllerDecisionMakingROSearchBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'BRE Runs';
            var lastSearchPageModel = $scope.getPageModelByURL('/DecisionMakingROSearch');
            var rowIndexToSelect = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.demaId = lastSearchPageModel.demaId;
                self.pageModel.clasId = lastSearchPageModel.clasId;
                self.pageModel.ecgrId = lastSearchPageModel.ecgrId;
                self.pageModel.dateTime = lastSearchPageModel.dateTime;
            }
            $scope._newIsDisabled =
                function () {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled =
                function () {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled =
                function (decisionMaking) {
                    return self._deleteIsDisabled(decisionMaking);
                };
            $scope._editBtnIsDisabled =
                function (decisionMaking) {
                    return self._editBtnIsDisabled(decisionMaking);
                };
            $scope._cancelIsDisabled =
                function () {
                    return self._cancelIsDisabled();
                };
            $scope.d__demaId_disabled =
                function () {
                    return self.d__demaId_disabled();
                };
            $scope.showLov_d__demaId =
                function () {
                    $timeout(function () {
                        self.showLov_d__demaId();
                    }, 0);
                };
            $scope.d__clasId_disabled =
                function () {
                    return self.d__clasId_disabled();
                };
            $scope.showLov_d__clasId =
                function () {
                    $timeout(function () {
                        self.showLov_d__clasId();
                    }, 0);
                };
            $scope.d__ecgrId_disabled =
                function () {
                    return self.d__ecgrId_disabled();
                };
            $scope.showLov_d__ecgrId =
                function () {
                    $timeout(function () {
                        self.showLov_d__ecgrId();
                    }, 0);
                };
            $scope.d__dateTime_disabled =
                function () {
                    return self.d__dateTime_disabled();
                };
            $scope.clearBtnAction = function () {
                self.pageModel.demaId = undefined;
                self.pageModel.clasId = undefined;
                self.pageModel.ecgrId = undefined;
                self.pageModel.dateTime = undefined;
                self.updateGrid(0, false, true);
            };
            $scope.newBtnAction = function () {
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };
            $scope.onEdit = function (x) {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };
            $timeout(function () {
                $('#Search_DecisionMakingRO').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);
            self.updateUI(rowIndexToSelect);
        }
        ControllerDecisionMakingROSearchBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerDecisionMakingROSearchBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerDecisionMakingROSearch";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerDecisionMakingROSearchBase.prototype, "HtmlDivId", {
            get: function () {
                return "Search_DecisionMakingRO";
            },
            enumerable: true,
            configurable: true
        });
        ControllerDecisionMakingROSearchBase.prototype._getPageTitle = function () {
            return "Field Map";
        };
        ControllerDecisionMakingROSearchBase.prototype.gridColumnFilter = function (field) {
            return true;
        };
        ControllerDecisionMakingROSearchBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            return [
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { cellClass: 'cellToolTip', field: 'description', displayName: 'getALString("Decision Making", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.description' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'clasId.name', displayName: 'getALString("Classification", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.clasId.name' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'ecgrId.name', displayName: 'getALString("BRE Rules Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.ecgrId.name' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'dateTime', displayName: 'getALString("Date", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_dateTime' data-ng-model='row.entity.dateTime' data-np-ui-model='row.entity._dateTime' data-ng-change='markEntityAsUpdated(row.entity,&quot;dateTime&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
        };
        ControllerDecisionMakingROSearchBase.prototype.getPersistedModel = function () {
            var self = this;
            var ret = new ModelDecisionMakingROSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.demaId = self.pageModel.demaId;
            ret.clasId = self.pageModel.clasId;
            ret.ecgrId = self.pageModel.ecgrId;
            ret.dateTime = self.pageModel.dateTime;
            return ret;
        };
        Object.defineProperty(ControllerDecisionMakingROSearchBase.prototype, "pageModel", {
            get: function () {
                return this.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerDecisionMakingROSearchBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.DecisionMaking.fromJSONComplete(webResponse.data);
        };
        ControllerDecisionMakingROSearchBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "DecisionMaking/findLazyDecisionMakingRO";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.demaId) && !isVoid(self.pageModel.demaId.description)) {
                paramData['demaId_description'] = self.pageModel.demaId.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.ecgrId) && !isVoid(self.pageModel.ecgrId.ecgrId)) {
                paramData['ecgrId_ecgrId'] = self.pageModel.ecgrId.ecgrId;
            }
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerDecisionMakingROSearchBase.prototype.makeWebRequest_count = function (excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "DecisionMaking/findLazyDecisionMakingRO_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.demaId) && !isVoid(self.pageModel.demaId.description)) {
                paramData['demaId_description'] = self.pageModel.demaId.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.ecgrId) && !isVoid(self.pageModel.ecgrId.ecgrId)) {
                paramData['ecgrId_ecgrId'] = self.pageModel.ecgrId.ecgrId;
            }
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerDecisionMakingROSearchBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var paramData = {};
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.demaId) && !isVoid(self.pageModel.demaId.description)) {
                paramData['demaId_description'] = self.pageModel.demaId.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.ecgrId) && !isVoid(self.pageModel.ecgrId.ecgrId)) {
                paramData['ecgrId_ecgrId'] = self.pageModel.ecgrId.ecgrId;
            }
            var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
            return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        };
        Object.defineProperty(ControllerDecisionMakingROSearchBase.prototype, "Current", {
            get: function () {
                var self = this;
                return self.$scope.pageModel.selectedEntities[0];
            },
            enumerable: true,
            configurable: true
        });
        ControllerDecisionMakingROSearchBase.prototype.onNew = function () {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/DecisionMakingRO', self._getPageTitle(), {});
        };
        ControllerDecisionMakingROSearchBase.prototype.onEdit = function (x) {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel = { "selectedEntity": x };
            self.$scope.navigateForward('/DecisionMakingRO', self._getPageTitle(), visitedPageModel);
        };
        ControllerDecisionMakingROSearchBase.prototype.getSynchronizeChangesWithDbUrl = function () {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_DecisionMakingRO";
        };
        ControllerDecisionMakingROSearchBase.prototype.getSynchronizeChangesWithDbData = function (x) {
            var self = this;
            var paramData = {
                data: []
            };
            var changeToCommit = new Controllers.ChangeToCommit(Controllers.ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);
            paramData.data.push(changeToCommit);
            return paramData;
        };
        ControllerDecisionMakingROSearchBase.prototype.deleteRecord = function (x) {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
            console.log("deleting DecisionMaking with PK field:" + x.demaId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                self.updateGrid();
                if (!isVoid(self.model.globalDema) && x.demaId === self.model.globalDema.demaId) {
                    self.model.globalDema = null;
                }
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        };
        ControllerDecisionMakingROSearchBase.prototype._newIsDisabled = function () {
            return true;
        };
        ControllerDecisionMakingROSearchBase.prototype._searchIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerDecisionMakingROSearchBase.prototype._deleteIsDisabled = function (decisionMaking) {
            return true;
        };
        ControllerDecisionMakingROSearchBase.prototype._editBtnIsDisabled = function (decisionMaking) {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMakingRO_R"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerDecisionMakingROSearchBase.prototype._cancelIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerDecisionMakingROSearchBase.prototype.d__demaId_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerDecisionMakingROSearchBase.prototype.showLov_d__demaId = function () {
            var self = this;
            var uimodel = self.pageModel._demaId;
            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'DecisionMaking';
            dialogOptions.previousModel = self.cachedLovModel_d__demaId;
            dialogOptions.className = "ControllerLovDecisionMaking";
            dialogOptions.onSelect = function (selectedEntity) {
                var decisionMaking1 = selectedEntity;
                uimodel.clearAllErrors();
                if (false && isVoid(decisionMaking1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                if (!isVoid(decisionMaking1) && decisionMaking1.isEqual(self.pageModel.demaId))
                    return;
                self.pageModel.demaId = decisionMaking1;
            };
            dialogOptions.openNewEntityDialog = function () {
            };
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                self.cachedLovModel_d__demaId = lovModel;
                var wsPath = "DecisionMaking/findAllByCriteriaRange_forLov";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_description)) {
                    paramData['fsch_DecisionMakingLov_description'] = lovModel.fsch_DecisionMakingLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_dateTime)) {
                    paramData['fsch_DecisionMakingLov_dateTime'] = lovModel.fsch_DecisionMakingLov_dateTime;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var wsPath = "DecisionMaking/findAllByCriteriaRange_forLov_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_description)) {
                    paramData['fsch_DecisionMakingLov_description'] = lovModel.fsch_DecisionMakingLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_dateTime)) {
                    paramData['fsch_DecisionMakingLov_dateTime'] = lovModel.fsch_DecisionMakingLov_dateTime;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                var paramData = {};
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_description)) {
                    paramData['fsch_DecisionMakingLov_description'] = lovModel.fsch_DecisionMakingLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_dateTime)) {
                    paramData['fsch_DecisionMakingLov_dateTime'] = lovModel.fsch_DecisionMakingLov_dateTime;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            dialogOptions.shownCols['description'] = true;
            self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/DecisionMaking.html?rev=' + self.$scope.globals.version, dialogOptions);
        };
        ControllerDecisionMakingROSearchBase.prototype.d__clasId_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerDecisionMakingROSearchBase.prototype.showLov_d__clasId = function () {
            var self = this;
            var uimodel = self.pageModel._clasId;
            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Data Import Name';
            dialogOptions.previousModel = self.cachedLovModel_d__clasId;
            dialogOptions.className = "ControllerLovClassification";
            dialogOptions.onSelect = function (selectedEntity) {
                var classification1 = selectedEntity;
                uimodel.clearAllErrors();
                if (false && isVoid(classification1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                if (!isVoid(classification1) && classification1.isEqual(self.pageModel.clasId))
                    return;
                self.pageModel.clasId = classification1;
            };
            dialogOptions.openNewEntityDialog = function () {
            };
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                self.cachedLovModel_d__clasId = lovModel;
                var wsPath = "Classification/findAllByCriteriaRange_forLov";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                    paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                    paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                    paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var wsPath = "Classification/findAllByCriteriaRange_forLov_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                    paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                    paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                    paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                var paramData = {};
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                    paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                    paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                    paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;
            dialogOptions.shownCols['dateTime'] = true;
            self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Classification.html?rev=' + self.$scope.globals.version, dialogOptions);
        };
        ControllerDecisionMakingROSearchBase.prototype.d__ecgrId_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerDecisionMakingROSearchBase.prototype.showLov_d__ecgrId = function () {
            var self = this;
            var uimodel = self.pageModel._ecgrId;
            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'BRE Rules';
            dialogOptions.previousModel = self.cachedLovModel_d__ecgrId;
            dialogOptions.className = "ControllerLovEcGroup";
            dialogOptions.onSelect = function (selectedEntity) {
                var ecGroup1 = selectedEntity;
                uimodel.clearAllErrors();
                if (false && isVoid(ecGroup1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                if (!isVoid(ecGroup1) && ecGroup1.isEqual(self.pageModel.ecgrId))
                    return;
                self.pageModel.ecgrId = ecGroup1;
            };
            dialogOptions.openNewEntityDialog = function () {
            };
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                self.cachedLovModel_d__ecgrId = lovModel;
                var wsPath = "EcGroup/findAllByCriteriaRange_forLov";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                    paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                    paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var wsPath = "EcGroup/findAllByCriteriaRange_forLov_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                    paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                    paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                var paramData = {};
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                    paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                    paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;
            self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/EcGroup.html?rev=' + self.$scope.globals.version, dialogOptions);
        };
        ControllerDecisionMakingROSearchBase.prototype.d__dateTime_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        return ControllerDecisionMakingROSearchBase;
    })(Controllers.AbstractSearchPageController);
    Controllers.ControllerDecisionMakingROSearchBase = ControllerDecisionMakingROSearchBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DecisionMakingROSearchBase.js.map