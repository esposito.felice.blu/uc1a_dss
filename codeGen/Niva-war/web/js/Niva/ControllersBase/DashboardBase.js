var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="../EntitiesBase/ParcelDecisionBase.ts" />
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />
/// <reference path="../EntitiesBase/GpDecisionBase.ts" />
/// <reference path="../EntitiesBase/FmisDecisionBase.ts" />
/// <reference path="../EntitiesBase/IntegrateddecisionBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="LovCoverTypeBase.ts" />
/// <reference path="../Controllers/Dashboard.ts" />
var Controllers;
(function (Controllers) {
    var Dashboard;
    (function (Dashboard) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        Dashboard.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $scope.onNewBtnAction = function () {
                    self.onNewBtnAction();
                };
                $scope.onDeleteBtnAction = function () {
                    self.onDeleteBtnAction();
                };
                $timeout(function () {
                    $('#Dashboard').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Dashboard";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Parcels";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpParcelClas.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpParcelClas.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelsIssues.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpFmisDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpIntegrateddecision.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpParcelClas.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelsIssues.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpFmisDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpIntegrateddecision.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpParcelClas.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpParcelClas.controller,
                            self.model.modelGrpParcelDecision.controller,
                            self.model.modelGrpParcelsIssues.controller,
                            self.model.modelGrpGpDecision.controller,
                            self.model.modelGrpFmisDecision.controller,
                            self.model.modelGrpIntegrateddecision.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getChangesToCommitForDeletion());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelDecision.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpFmisDecision.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getDeleteChangesToCommit());
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelDecision.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpFmisDecision.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getChangesToCommit());
                }
                var hasParcelClas = pageChanges.some(function (change) { return change.entityName === "ParcelClas"; });
                if (!hasParcelClas) {
                    var validateEntity = new Controllers.ChangeToCommit(Controllers.ChangeStatus.UPDATE, 0, "ParcelClas", self.model.modelGrpParcelClas.controller.Current);
                    pageChanges = pageChanges.concat(validateEntity);
                }
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_Dashboard";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpParcelClas.controller.cleanUpAfterSave();
                self.model.modelGrpParcelDecision.controller.cleanUpAfterSave();
                self.model.modelGrpParcelsIssues.controller.cleanUpAfterSave();
                self.model.modelGrpGpDecision.controller.cleanUpAfterSave();
                self.model.modelGrpFmisDecision.controller.cleanUpAfterSave();
                self.model.modelGrpIntegrateddecision.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = self.$scope.getCurrentPageModel();
                        var newParcelClasId = response.newEntitiesIds.firstOrNull(function (x) { return x.entityName === 'ParcelClas'; });
                        if (!isVoid(newParcelClasId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.ParcelClas.CreateById(newParcelClasId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.getInitialEntity = function () {
                if (this.shownAsDialog()) {
                    return this.dialogSelectedEntity();
                }
                else {
                    var breadCrumbStepModel = this.$scope.getPageModelByURL('/Dashboard');
                    if (isVoid(breadCrumbStepModel)) {
                        return null;
                    }
                    else {
                        return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                    }
                }
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            PageControllerBase.prototype.onNewBtnAction = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                self.addNewRecord();
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () { self.addNewRecord(); }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    self.addNewRecord();
                }
            };
            PageControllerBase.prototype.addNewRecord = function () {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                self.model.modelGrpParcelClas.controller.cleanUpAfterSave();
                self.model.modelGrpParcelDecision.controller.cleanUpAfterSave();
                self.model.modelGrpParcelsIssues.controller.cleanUpAfterSave();
                self.model.modelGrpGpDecision.controller.cleanUpAfterSave();
                self.model.modelGrpFmisDecision.controller.cleanUpAfterSave();
                self.model.modelGrpIntegrateddecision.controller.cleanUpAfterSave();
                self.model.modelGrpParcelClas.controller.createNewEntityAndAddToUI();
            };
            PageControllerBase.prototype.onDeleteBtnAction = function () {
                var self = this;
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                    new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(); }),
                    new Tuple2("MessageBox_Button_No", function () { })
                ], 1, 1);
            };
            PageControllerBase.prototype.deleteRecord = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW) {
                    console.log("Delete pressed in a form while being in new mode!");
                    return;
                }
                NpTypes.AlertMessage.clearAlerts(self.model);
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData(true);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "Mode", {
                get: function () {
                    var self = this;
                    if (isVoid(self.model.modelGrpParcelClas) || isVoid(self.model.modelGrpParcelClas.controller))
                        return Controllers.EditMode.NEW;
                    return self.model.modelGrpParcelClas.controller.Mode;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._newIsDisabled = function () {
                var self = this;
                if (isVoid(self.model.modelGrpParcelClas) || isVoid(self.model.modelGrpParcelClas.controller))
                    return true;
                return self.model.modelGrpParcelClas.controller._newIsDisabled();
            };
            PageControllerBase.prototype._deleteIsDisabled = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW)
                    return true;
                if (isVoid(self.model.modelGrpParcelClas) || isVoid(self.model.modelGrpParcelClas.controller))
                    return true;
                return self.model.modelGrpParcelClas.controller._deleteIsDisabled(self.model.modelGrpParcelClas.controller.Current);
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        Dashboard.PageControllerBase = PageControllerBase;
        // GROUP GrpParcelClas
        var ModelGrpParcelClasBase = (function (_super) {
            __extends(ModelGrpParcelClasBase, _super);
            function ModelGrpParcelClasBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].pclaId;
                },
                set: function (pclaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].pclaId = pclaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "probPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probPred;
                },
                set: function (probPred_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probPred = probPred_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_probPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probPred;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "probPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probPred2;
                },
                set: function (probPred2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probPred2 = probPred2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_probPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probPred2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "prodCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].prodCode;
                },
                set: function (prodCode_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].prodCode = prodCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_prodCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._prodCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "parcIdentifier", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcIdentifier;
                },
                set: function (parcIdentifier_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcIdentifier = parcIdentifier_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_parcIdentifier", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcIdentifier;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "parcCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcCode;
                },
                set: function (parcCode_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcCode = parcCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_parcCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "geom4326", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].geom4326;
                },
                set: function (geom4326_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].geom4326 = geom4326_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_geom4326", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._geom4326;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].clasId;
                },
                set: function (clasId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].clasId = clasId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._clasId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cultIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultIdDecl;
                },
                set: function (cultIdDecl_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultIdDecl = cultIdDecl_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cultIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultIdDecl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cultIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultIdPred;
                },
                set: function (cultIdPred_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultIdPred = cultIdPred_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cultIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultIdPred;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cotyIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyIdDecl;
                },
                set: function (cotyIdDecl_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyIdDecl = cotyIdDecl_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cotyIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyIdDecl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cotyIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyIdPred;
                },
                set: function (cotyIdPred_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyIdPred = cotyIdPred_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cotyIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyIdPred;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cultIdPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultIdPred2;
                },
                set: function (cultIdPred2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultIdPred2 = cultIdPred2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cultIdPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultIdPred2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpParcelClasBase;
        })(Controllers.AbstractGroupFormModel);
        Dashboard.ModelGrpParcelClasBase = ModelGrpParcelClasBase;
        var ControllerGrpParcelClasBase = (function (_super) {
            __extends(ControllerGrpParcelClasBase, _super);
            function ControllerGrpParcelClasBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                $scope.modelGrpParcelClas = self.model;
                var selectedEntity = this.$scope.pageModel.controller.getInitialEntity();
                if (isVoid(selectedEntity)) {
                    self.createNewEntityAndAddToUI();
                }
                else {
                    var clonedEntity = Entities.ParcelClas.Create();
                    clonedEntity.updateInstance(selectedEntity);
                    $scope.modelGrpParcelClas.visibleEntities[0] = clonedEntity;
                    $scope.modelGrpParcelClas.selectedEntities[0] = clonedEntity;
                }
                $scope.GrpParcelClas_0_disabled =
                    function () {
                        return self.GrpParcelClas_0_disabled();
                    };
                $scope.GrpParcelClas_0_invisible =
                    function () {
                        return self.GrpParcelClas_0_invisible();
                    };
                $scope.GrpParcelClas_0_0_disabled =
                    function () {
                        return self.GrpParcelClas_0_0_disabled();
                    };
                $scope.GrpParcelClas_0_0_invisible =
                    function () {
                        return self.GrpParcelClas_0_0_invisible();
                    };
                $scope.GrpParcelClas_0_1_disabled =
                    function () {
                        return self.GrpParcelClas_0_1_disabled();
                    };
                $scope.GrpParcelClas_0_1_invisible =
                    function () {
                        return self.GrpParcelClas_0_1_invisible();
                    };
                $scope.XartoReg_disabled =
                    function () {
                        return self.XartoReg_disabled();
                    };
                $scope.XartoReg_invisible =
                    function () {
                        return self.XartoReg_invisible();
                    };
                $scope.INPMPID_disabled =
                    function (parcelClas) {
                        return self.INPMPID_disabled(parcelClas);
                    };
                $scope.GrpParcelClas_0_2_disabled =
                    function () {
                        return self.GrpParcelClas_0_2_disabled();
                    };
                $scope.GrpParcelClas_0_2_invisible =
                    function () {
                        return self.GrpParcelClas_0_2_invisible();
                    };
                $scope.child_group_GrpParcelDecision_isInvisible =
                    function () {
                        return self.child_group_GrpParcelDecision_isInvisible();
                    };
                $scope.child_group_GrpParcelsIssues_isInvisible =
                    function () {
                        return self.child_group_GrpParcelsIssues_isInvisible();
                    };
                $scope.child_group_GrpIntegrateddecision_isInvisible =
                    function () {
                        return self.child_group_GrpIntegrateddecision_isInvisible();
                    };
                $scope.pageModel.modelGrpParcelClas = $scope.modelGrpParcelClas;
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelClas[] , oldVisible:Entities.ParcelClas[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                //bind entity child collection with child controller merged items
                Entities.ParcelClas.parcelDecisionCollection = function (parcelClas, func) {
                    _this.model.modelGrpParcelDecision.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelClas);
                };
                //bind entity child collection with child controller merged items
                Entities.ParcelClas.parcelsIssuesCollection = function (parcelClas, func) {
                    _this.model.modelGrpParcelsIssues.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelClas);
                };
                //bind entity child collection with child controller merged items
                Entities.ParcelClas.integrateddecisionCollection = function (parcelClas, func) {
                    _this.model.modelGrpIntegrateddecision.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelClas);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelClas.parcelDecisionCollection = null; //(parcelClas, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelClas.parcelsIssuesCollection = null; //(parcelClas, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelClas.integrateddecisionCollection = null; //(parcelClas, func) => { }
                });
                this.createGeom4326Map({
                    grp: this,
                    Plato: Plato,
                    divId: "INPMPID",
                    getDataFromEntity: function (x) { return isVoid(x) ? undefined : x.geom4326; },
                    saveDataToEntity: function (x, f) {
                        if (!isVoid(x)) {
                            x.geom4326 = f;
                            _this.markEntityAsUpdated(x, "geom4326");
                        }
                    },
                    isDisabled: function (x) { return _this.INPMPID_disabled(x); },
                    layers: [
                        new NpGeoLayers.OpenStreetTileLayer(true),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMPID1',
                            label: 'Search1',
                            isVisible: true,
                            isSearch: true
                        }),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMPID2',
                            label: 'Search2',
                            isVisible: true,
                            isSearch: true
                        }),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMPID3',
                            label: 'Search3',
                            isVisible: true,
                            isSearch: true
                        })
                    ],
                    coordinateSystem: "EPSG:2100"
                });
            }
            ControllerGrpParcelClasBase.prototype.createGeom4326Map = function (defaultOptions) {
                this.geom4326Map = new NpGeo.NpMap(defaultOptions);
            };
            ControllerGrpParcelClasBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpParcelClas";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Dashboard_ControllerGrpParcelClas";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Parcel Identifier'; }, false, function (ent) { return ent.parcIdentifier; }, function (ent) { return ent._parcIdentifier; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'Parcel Code'; }, false, function (ent) { return ent.parcCode; }, function (ent) { return ent._parcCode; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.NumberItem(function (ent) { return 'Producer Code'; }, false, function (ent) { return ent.prodCode; }, function (ent) { return ent._prodCode; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0),
                            new Controllers.MapItem(function (ent) { return ''; }, false, function (ent) { return ent.geom4326; }, function (ent) { return ent._geom4326; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "modelGrpParcelClas", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.getEntityName = function () {
                return "ParcelClas";
            };
            ControllerGrpParcelClasBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.geom4326Map.cleanUndoBuffers();
            };
            ControllerGrpParcelClasBase.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
                var self = this;
                if (self.$scope.pageModel.controller.shownAsDialog())
                    return;
                var breadCrumbStepModel = { selectedEntity: null };
                if (!isVoid(self.Current)) {
                    var selectedEntity = Entities.ParcelClas.Create();
                    selectedEntity.updateInstance(self.Current);
                    breadCrumbStepModel.selectedEntity = selectedEntity;
                }
                else {
                    breadCrumbStepModel.selectedEntity = null;
                }
                self.$scope.setCurrentPageModel(breadCrumbStepModel);
            };
            ControllerGrpParcelClasBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.ParcelClas.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpParcelClasBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.ParcelClas(
                /*pclaId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*probPred:number*/ null, 
                /*probPred2:number*/ null, 
                /*prodCode:number*/ null, 
                /*parcIdentifier:string*/ null, 
                /*parcCode:string*/ null, 
                /*geom4326:ol.Feature*/ null, 
                /*clasId:Entities.Classification*/ null, 
                /*cultIdDecl:Entities.Cultivation*/ null, 
                /*cultIdPred:Entities.Cultivation*/ null, 
                /*cotyIdDecl:Entities.CoverType*/ null, 
                /*cotyIdPred:Entities.CoverType*/ null, 
                /*cultIdPred2:Entities.Cultivation*/ null);
                return ret;
            };
            ControllerGrpParcelClasBase.prototype.createNewEntityAndAddToUI = function () {
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.$scope.modelGrpParcelClas.visibleEntities[0] = newEnt;
                self.$scope.modelGrpParcelClas.selectedEntities[0] = newEnt;
                return newEnt;
            };
            ControllerGrpParcelClasBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpParcelClasBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.ParcelClas.Create();
                ret.updateInstance(src);
                ret.pclaId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.visibleEntities[0] = ret;
                this.model.selectedEntities[0] = ret;
                this.$timeout(function () {
                    _this.modelGrpParcelClas.modelGrpParcelDecision.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpParcelClas.modelGrpParcelsIssues.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpParcelClas.modelGrpIntegrateddecision.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpParcelClas.modelGrpParcelDecision.controller,
                            self.modelGrpParcelClas.modelGrpParcelsIssues.controller,
                            self.modelGrpParcelClas.modelGrpIntegrateddecision.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpParcelClas.modelGrpParcelDecision.controller.deleteAllEntitiesUnderParent(ent, function () {
                        self.modelGrpParcelClas.modelGrpParcelsIssues.controller.deleteAllEntitiesUnderParent(ent, function () {
                            self.modelGrpParcelClas.modelGrpIntegrateddecision.controller.deleteAllEntitiesUnderParent(ent, function () {
                                _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                            });
                        });
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpParcelClas.modelGrpParcelDecision.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpParcelClas.modelGrpParcelsIssues.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpParcelClas.modelGrpIntegrateddecision.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_1_disabled = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_1_invisible = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.XartoReg_disabled = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_1_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.XartoReg_invisible = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_1_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.INPMPID_disabled = function (parcelClas) {
                var self = this;
                if (parcelClas === undefined || parcelClas === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelClas, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self.XartoReg_disabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_2_disabled = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_2_invisible = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpParcelClasBase.prototype._deleteIsDisabled = function (parcelClas) {
                return true;
            };
            ControllerGrpParcelClasBase.prototype.child_group_GrpParcelDecision_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpParcelDecision === undefined) || (self.model.modelGrpParcelDecision.controller === undefined))
                    return false;
                return self.model.modelGrpParcelDecision.controller._isInvisible();
            };
            ControllerGrpParcelClasBase.prototype.child_group_GrpParcelsIssues_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpParcelsIssues === undefined) || (self.model.modelGrpParcelsIssues.controller === undefined))
                    return false;
                return self.model.modelGrpParcelsIssues.controller._isInvisible();
            };
            ControllerGrpParcelClasBase.prototype.child_group_GrpIntegrateddecision_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpIntegrateddecision === undefined) || (self.model.modelGrpIntegrateddecision.controller === undefined))
                    return false;
                return self.model.modelGrpIntegrateddecision.controller._isInvisible();
            };
            return ControllerGrpParcelClasBase;
        })(Controllers.AbstractGroupFormController);
        Dashboard.ControllerGrpParcelClasBase = ControllerGrpParcelClasBase;
        // GROUP GrpParcelDecision
        var ModelGrpParcelDecisionBase = (function (_super) {
            __extends(ModelGrpParcelDecisionBase, _super);
            function ModelGrpParcelDecisionBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "padeId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].padeId;
                },
                set: function (padeId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].padeId = padeId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_padeId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._padeId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].decisionLight;
                },
                set: function (decisionLight_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].decisionLight = decisionLight_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._decisionLight;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].demaId;
                },
                set: function (demaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].demaId = demaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._demaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].pclaId;
                },
                set: function (pclaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].pclaId = pclaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpParcelDecisionBase;
        })(Controllers.AbstractGroupTableModel);
        Dashboard.ModelGrpParcelDecisionBase = ModelGrpParcelDecisionBase;
        var ControllerGrpParcelDecisionBase = (function (_super) {
            __extends(ControllerGrpParcelDecisionBase, _super);
            function ControllerGrpParcelDecisionBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpParcelDecision = self.model;
                $scope.pageModel.modelGrpParcelDecision = $scope.modelGrpParcelDecision;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelClas.modelGrpParcelDecision = $scope.modelGrpParcelDecision;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelClas.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelDecision[] , oldVisible:Entities.ParcelDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpParcelDecisionBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpParcelDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Dashboard_ControllerGrpParcelDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelDecisionBase.prototype.gridTitle = function () {
                return "Parcel Decision";
            };
            ControllerGrpParcelDecisionBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpParcelDecisionBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'decisionLight', displayName: 'getALString("Decision Light", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpParcelDecision_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'demaId.description', displayName: 'getALString("Decision Making Run", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '14%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__demaId_description' data-ng-model='row.entity.demaId.description' data-np-ui-model='row.entity.demaId._description' data-ng-change='markEntityAsUpdated(row.entity,&quot;demaId.description&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'demaId.ecgrId.name', displayName: 'getALString("BRE Rules Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '14%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__demaId_ecgrId_name' data-ng-model='row.entity.demaId.ecgrId.name' data-np-ui-model='row.entity.demaId.ecgrId._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;demaId.ecgrId.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'demaId.dateTime', displayName: 'getALString("Date", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '7%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__demaId_dateTime' data-ng-model='row.entity.demaId.dateTime' data-np-ui-model='row.entity.demaId._dateTime' data-ng-change='markEntityAsUpdated(row.entity,&quot;demaId.dateTime&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "modelGrpParcelDecision", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelDecisionBase.prototype.getEntityName = function () {
                return "ParcelDecision";
            };
            ControllerGrpParcelDecisionBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpParcelDecisionBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.ParcelDecision.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.pclaId = _this.Parent;
                    }
                    else {
                        x.pclaId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelDecision.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelDecisionBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelClas.controller.isEntityLocked(cur.pclaId, lockKind);
            };
            ControllerGrpParcelDecisionBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpParcelDecisionBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DashboardGrpParcelDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelDecisionBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DashboardGrpParcelDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelDecisionBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DashboardGrpParcelDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelDecisionBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.pclaId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "ParcelDecision/findAllByCriteriaRange_DashboardGrpParcelDecision";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['pclaId_pclaId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpParcelDecisionBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.pclaId === undefined)
                    return false;
                return x.pclaId.getKey() === this.Parent.getKey();
            };
            ControllerGrpParcelDecisionBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "pclaId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelDecisionBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.ParcelDecision(
                /*padeId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*decisionLight:number*/ null, 
                /*rowVersion:number*/ null, 
                /*demaId:Entities.DecisionMaking*/ null, 
                /*pclaId:Entities.ParcelClas*/ null);
                return ret;
            };
            ControllerGrpParcelDecisionBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.pclaId = self.pclaId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpParcelDecisionBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpParcelDecisionBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.ParcelDecision.Create();
                ret.updateInstance(src);
                ret.padeId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpParcelDecisionBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (parcelDecisionList) {
                    parcelDecisionList.forEach(function (parcelDecision) {
                        var parcelDecisionCloned = _this.cloneEntity_internal(parcelDecision, true);
                        parcelDecisionCloned.pclaId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpParcelDecisionBase.prototype.deleteNewEntitiesUnderParent = function (parcelClas) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var parcelDecision = change.a;
                    if (parcelClas.getKey() === parcelDecision.pclaId.getKey()) {
                        toBeDeleted.push(parcelDecision);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpParcelDecisionBase.prototype.deleteAllEntitiesUnderParent = function (parcelClas, afterDeleteAction) {
                var self = this;
                function deleteParcelDecisionList(parcelDecisionList) {
                    if (parcelDecisionList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = parcelDecisionList[0];
                        var tail = parcelDecisionList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteParcelDecisionList(tail);
                        });
                    }
                }
                this.getMergedItems(function (parcelDecisionList) {
                    deleteParcelDecisionList(parcelDecisionList);
                }, false, parcelClas);
            };
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelDecisionBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpParcelDecisionBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelClas_0_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelDecisionBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelClas_0_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelDecisionBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpParcelDecisionBase.prototype._deleteIsDisabled = function (parcelDecision) {
                return true;
            };
            return ControllerGrpParcelDecisionBase;
        })(Controllers.AbstractGroupTableController);
        Dashboard.ControllerGrpParcelDecisionBase = ControllerGrpParcelDecisionBase;
        // GROUP GrpParcelsIssues
        var ModelGrpParcelsIssuesBase = (function (_super) {
            __extends(ModelGrpParcelsIssuesBase, _super);
            function ModelGrpParcelsIssuesBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesId;
                },
                set: function (parcelsIssuesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesId = parcelsIssuesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "dteCreated", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteCreated;
                },
                set: function (dteCreated_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteCreated = dteCreated_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_dteCreated", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteCreated;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "status", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].status;
                },
                set: function (status_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].status = status_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_status", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._status;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "dteStatusUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteStatusUpdate;
                },
                set: function (dteStatusUpdate_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteStatusUpdate = dteStatusUpdate_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_dteStatusUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteStatusUpdate;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "typeOfIssue", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].typeOfIssue;
                },
                set: function (typeOfIssue_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].typeOfIssue = typeOfIssue_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_typeOfIssue", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._typeOfIssue;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "active", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].active;
                },
                set: function (active_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].active = active_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_active", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._active;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].pclaId;
                },
                set: function (pclaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].pclaId = pclaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpParcelsIssuesBase;
        })(Controllers.AbstractGroupTableModel);
        Dashboard.ModelGrpParcelsIssuesBase = ModelGrpParcelsIssuesBase;
        var ControllerGrpParcelsIssuesBase = (function (_super) {
            __extends(ControllerGrpParcelsIssuesBase, _super);
            function ControllerGrpParcelsIssuesBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpParcelsIssues = self.model;
                $scope.GrpParcelsIssues_itm__status_disabled =
                    function (parcelsIssues) {
                        return self.GrpParcelsIssues_itm__status_disabled(parcelsIssues);
                    };
                $scope.GrpParcelsIssues_itm__dteStatusUpdate_disabled =
                    function (parcelsIssues) {
                        return self.GrpParcelsIssues_itm__dteStatusUpdate_disabled(parcelsIssues);
                    };
                $scope.GrpParcelsIssues_itm__dteCreated_disabled =
                    function (parcelsIssues) {
                        return self.GrpParcelsIssues_itm__dteCreated_disabled(parcelsIssues);
                    };
                $scope.child_group_GrpGpDecision_isInvisible =
                    function () {
                        return self.child_group_GrpGpDecision_isInvisible();
                    };
                $scope.child_group_GrpFmisDecision_isInvisible =
                    function () {
                        return self.child_group_GrpFmisDecision_isInvisible();
                    };
                $scope.pageModel.modelGrpParcelsIssues = $scope.modelGrpParcelsIssues;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelClas.modelGrpParcelsIssues = $scope.modelGrpParcelsIssues;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelClas.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelsIssues[] , oldVisible:Entities.ParcelsIssues[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.ParcelsIssues.gpDecisionCollection = function (parcelsIssues, func) {
                    _this.model.modelGrpGpDecision.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelsIssues);
                };
                //bind entity child collection with child controller merged items
                Entities.ParcelsIssues.fmisDecisionCollection = function (parcelsIssues, func) {
                    _this.model.modelGrpFmisDecision.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelsIssues);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelsIssues.gpDecisionCollection = null; //(parcelsIssues, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelsIssues.fmisDecisionCollection = null; //(parcelsIssues, func) => { }
                });
            }
            ControllerGrpParcelsIssuesBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpParcelsIssues";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Dashboard_ControllerGrpParcelsIssues";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.NumberItem(function (ent) { return 'Current Status'; }, false, function (ent) { return ent.status; }, function (ent) { return ent._status; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, function () { return 0; }, function () { return 999; }, 0),
                            new Controllers.DateItem(function (ent) { return 'Date Updated'; }, false, function (ent) { return ent.dteStatusUpdate; }, function (ent) { return ent._dteStatusUpdate; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.DateItem(function (ent) { return 'Date Created'; }, false, function (ent) { return ent.dteCreated; }, function (ent) { return ent._dteCreated; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.gridTitle = function () {
                return "Parcel\'s Issues";
            };
            ControllerGrpParcelsIssuesBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpParcelsIssuesBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'status', displayName: 'getALString("Current Status", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssues_itm__status' data-ng-model='row.entity.status' data-np-ui-model='row.entity._status' data-ng-change='markEntityAsUpdated(row.entity,&quot;status&quot;)' data-ng-readonly='GrpParcelsIssues_itm__status_disabled(row.entity)' data-np-number='dummy' data-np-min='0' data-np-max='999' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteStatusUpdate', displayName: 'getALString("Date Updated", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssues_itm__dteStatusUpdate' data-ng-model='row.entity.dteStatusUpdate' data-np-ui-model='row.entity._dteStatusUpdate' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteStatusUpdate&quot;)' data-ng-readonly='GrpParcelsIssues_itm__dteStatusUpdate_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteCreated', displayName: 'getALString("Date Created", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssues_itm__dteCreated' data-ng-model='row.entity.dteCreated' data-np-ui-model='row.entity._dteCreated' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteCreated&quot;)' data-ng-readonly='GrpParcelsIssues_itm__dteCreated_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "modelGrpParcelsIssues", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.getEntityName = function () {
                return "ParcelsIssues";
            };
            ControllerGrpParcelsIssuesBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpParcelsIssuesBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.ParcelsIssues.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.pclaId = _this.Parent;
                    }
                    else {
                        x.pclaId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelClas.controller.isEntityLocked(cur.pclaId, lockKind);
            };
            ControllerGrpParcelsIssuesBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpParcelsIssuesBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelsIssues/findAllByCriteriaRange_DashboardGrpParcelsIssues_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelsIssuesBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelsIssues/findAllByCriteriaRange_DashboardGrpParcelsIssues";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelsIssuesBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelsIssues/findAllByCriteriaRange_DashboardGrpParcelsIssues_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelsIssuesBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.pclaId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "ParcelsIssues/findAllByCriteriaRange_DashboardGrpParcelsIssues";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['pclaId_pclaId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpParcelsIssuesBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.pclaId === undefined)
                    return false;
                return x.pclaId.getKey() === this.Parent.getKey();
            };
            ControllerGrpParcelsIssuesBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "pclaId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.ParcelsIssues(
                /*parcelsIssuesId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*dteCreated:Date*/ null, 
                /*status:number*/ null, 
                /*dteStatusUpdate:Date*/ null, 
                /*rowVersion:number*/ null, 
                /*typeOfIssue:number*/ null, 
                /*active:boolean*/ null, 
                /*pclaId:Entities.ParcelClas*/ null);
                return ret;
            };
            ControllerGrpParcelsIssuesBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.pclaId = self.pclaId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpParcelsIssuesBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpParcelsIssuesBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.ParcelsIssues.Create();
                ret.updateInstance(src);
                ret.parcelsIssuesId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelGrpParcelsIssues.modelGrpGpDecision.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpParcelsIssues.modelGrpFmisDecision.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerGrpParcelsIssuesBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (parcelsIssuesList) {
                    parcelsIssuesList.forEach(function (parcelsIssues) {
                        var parcelsIssuesCloned = _this.cloneEntity_internal(parcelsIssues, true);
                        parcelsIssuesCloned.pclaId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpParcelsIssuesBase.prototype.deleteNewEntitiesUnderParent = function (parcelClas) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var parcelsIssues = change.a;
                    if (parcelClas.getKey() === parcelsIssues.pclaId.getKey()) {
                        toBeDeleted.push(parcelsIssues);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteNewEntitiesUnderParent(x);
                    self.modelGrpParcelsIssues.modelGrpFmisDecision.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerGrpParcelsIssuesBase.prototype.deleteAllEntitiesUnderParent = function (parcelClas, afterDeleteAction) {
                var self = this;
                function deleteParcelsIssuesList(parcelsIssuesList) {
                    if (parcelsIssuesList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = parcelsIssuesList[0];
                        var tail = parcelsIssuesList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteParcelsIssuesList(tail);
                        });
                    }
                }
                this.getMergedItems(function (parcelsIssuesList) {
                    deleteParcelsIssuesList(parcelsIssuesList);
                }, false, parcelClas);
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpParcelsIssues.modelGrpGpDecision.controller,
                            self.modelGrpParcelsIssues.modelGrpFmisDecision.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteAllEntitiesUnderParent(ent, function () {
                        self.modelGrpParcelsIssues.modelGrpFmisDecision.controller.deleteAllEntitiesUnderParent(ent, function () {
                            _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                        });
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpParcelsIssues.modelGrpFmisDecision.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_itm__status_disabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined || parcelsIssues === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_itm__dteStatusUpdate_disabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined || parcelsIssues === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_itm__dteCreated_disabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined || parcelsIssues === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesBase.prototype._isDisabled = function () {
                if (true)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelClas_0_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpParcelsIssuesBase.prototype._deleteIsDisabled = function (parcelsIssues) {
                return true;
            };
            ControllerGrpParcelsIssuesBase.prototype.child_group_GrpGpDecision_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpGpDecision === undefined) || (self.model.modelGrpGpDecision.controller === undefined))
                    return false;
                return self.model.modelGrpGpDecision.controller._isInvisible();
            };
            ControllerGrpParcelsIssuesBase.prototype.child_group_GrpFmisDecision_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpFmisDecision === undefined) || (self.model.modelGrpFmisDecision.controller === undefined))
                    return false;
                return self.model.modelGrpFmisDecision.controller._isInvisible();
            };
            return ControllerGrpParcelsIssuesBase;
        })(Controllers.AbstractGroupTableController);
        Dashboard.ControllerGrpParcelsIssuesBase = ControllerGrpParcelsIssuesBase;
        // GROUP GrpGpDecision
        var ModelGrpGpDecisionBase = (function (_super) {
            __extends(ModelGrpGpDecisionBase, _super);
            function ModelGrpGpDecisionBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "gpDecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpDecisionsId;
                },
                set: function (gpDecisionsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpDecisionsId = gpDecisionsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_gpDecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpDecisionsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "cropOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cropOk;
                },
                set: function (cropOk_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cropOk = cropOk_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_cropOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cropOk;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "landcoverOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].landcoverOk;
                },
                set: function (landcoverOk_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].landcoverOk = landcoverOk_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_landcoverOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._landcoverOk;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "dteInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteInsert;
                },
                set: function (dteInsert_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteInsert = dteInsert_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_dteInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteInsert;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "usrInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].usrInsert;
                },
                set: function (usrInsert_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].usrInsert = usrInsert_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_usrInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._usrInsert;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesId;
                },
                set: function (parcelsIssuesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesId = parcelsIssuesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpGpDecisionBase;
        })(Controllers.AbstractGroupTableModel);
        Dashboard.ModelGrpGpDecisionBase = ModelGrpGpDecisionBase;
        var ControllerGrpGpDecisionBase = (function (_super) {
            __extends(ControllerGrpGpDecisionBase, _super);
            function ControllerGrpGpDecisionBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 2, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpGpDecision = self.model;
                $scope.GrpGpDecision_itm__cultId_disabled =
                    function (gpDecision) {
                        return self.GrpGpDecision_itm__cultId_disabled(gpDecision);
                    };
                $scope.showLov_GrpGpDecision_itm__cultId =
                    function (gpDecision) {
                        $timeout(function () {
                            self.showLov_GrpGpDecision_itm__cultId(gpDecision);
                        }, 0);
                    };
                $scope.GrpGpDecision_itm__cotyId_disabled =
                    function (gpDecision) {
                        return self.GrpGpDecision_itm__cotyId_disabled(gpDecision);
                    };
                $scope.showLov_GrpGpDecision_itm__cotyId =
                    function (gpDecision) {
                        $timeout(function () {
                            self.showLov_GrpGpDecision_itm__cotyId(gpDecision);
                        }, 0);
                    };
                $scope.pageModel.modelGrpGpDecision = $scope.modelGrpGpDecision;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelsIssues.modelGrpGpDecision = $scope.modelGrpGpDecision;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpDecision[] , oldVisible:Entities.GpDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpGpDecisionBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpGpDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Dashboard_ControllerGrpGpDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.LovItem(function (ent) { return 'Presumed Crop'; }, false, function (ent) { return ent.cultId; }, function (ent) { return ent._cultId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.LovItem(function (ent) { return 'Presumed Land Cover'; }, false, function (ent) { return ent.cotyId; }, function (ent) { return ent._cotyId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.DateItem(function (ent) { return 'Date'; }, false, function (ent) { return ent.dteInsert; }, function (ent) { return ent._dteInsert; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.TextItem(function (ent) { return 'User'; }, false, function (ent) { return ent.usrInsert; }, function (ent) { return ent._usrInsert; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.gridTitle = function () {
                return "Geotagged Photos Decisions";
            };
            ControllerGrpGpDecisionBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpGpDecisionBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'cultId.name', displayName: 'getALString("Presumed Crop", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpGpDecision_itm__cultId_disabled(row.entity) && showLov_GrpGpDecision_itm__cultId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpGpDecision_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpGpDecision_itm__cultId(row.entity)'   data-ng-disabled=\"GrpGpDecision_itm__cultId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cotyId.name', displayName: 'getALString("Presumed Land Cover", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpGpDecision_itm__cotyId_disabled(row.entity) && showLov_GrpGpDecision_itm__cotyId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cotyId.name' data-np-ui-model='row.entity._cotyId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpGpDecision_itm__cotyId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpGpDecision_itm__cotyId(row.entity)'   data-ng-disabled=\"GrpGpDecision_itm__cotyId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteInsert', displayName: 'getALString("Date", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpDecision_itm__dteInsert' data-ng-model='row.entity.dteInsert' data-np-ui-model='row.entity._dteInsert' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteInsert&quot;)' data-np-required='true' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy hh:mm' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'usrInsert', displayName: 'getALString("User", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpDecision_itm__usrInsert' data-ng-model='row.entity.usrInsert' data-np-ui-model='row.entity._usrInsert' data-ng-change='markEntityAsUpdated(row.entity,&quot;usrInsert&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "modelGrpGpDecision", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.getEntityName = function () {
                return "GpDecision";
            };
            ControllerGrpGpDecisionBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpGpDecisionBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.GpDecision.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.parcelsIssuesId = _this.Parent;
                    }
                    else {
                        x.parcelsIssuesId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpDecision.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(cur.parcelsIssuesId, lockKind);
            };
            ControllerGrpGpDecisionBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpGpDecisionBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_DashboardGrpGpDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpDecisionBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_DashboardGrpGpDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpDecisionBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_DashboardGrpGpDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpDecisionBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.parcelsIssuesId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "GpDecision/findAllByCriteriaRange_DashboardGrpGpDecision";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpGpDecisionBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                    return false;
                return x.parcelsIssuesId.getKey() === this.Parent.getKey();
            };
            ControllerGrpGpDecisionBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "parcelsIssuesId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.GpDecision(
                /*gpDecisionsId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*cropOk:number*/ null, 
                /*landcoverOk:number*/ null, 
                /*dteInsert:Date*/ null, 
                /*usrInsert:string*/ null, 
                /*rowVersion:number*/ null, 
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null, 
                /*cultId:Entities.Cultivation*/ null, 
                /*cotyId:Entities.CoverType*/ null);
                return ret;
            };
            ControllerGrpGpDecisionBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.parcelsIssuesId = self.parcelsIssuesId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpGpDecisionBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpGpDecisionBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.GpDecision.Create();
                ret.updateInstance(src);
                ret.gpDecisionsId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpGpDecisionBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (gpDecisionList) {
                    gpDecisionList.forEach(function (gpDecision) {
                        var gpDecisionCloned = _this.cloneEntity_internal(gpDecision, true);
                        gpDecisionCloned.parcelsIssuesId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpGpDecisionBase.prototype.deleteNewEntitiesUnderParent = function (parcelsIssues) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var gpDecision = change.a;
                    if (parcelsIssues.getKey() === gpDecision.parcelsIssuesId.getKey()) {
                        toBeDeleted.push(gpDecision);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpGpDecisionBase.prototype.deleteAllEntitiesUnderParent = function (parcelsIssues, afterDeleteAction) {
                var self = this;
                function deleteGpDecisionList(gpDecisionList) {
                    if (gpDecisionList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = gpDecisionList[0];
                        var tail = gpDecisionList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteGpDecisionList(tail);
                        });
                    }
                }
                this.getMergedItems(function (gpDecisionList) {
                    deleteGpDecisionList(gpDecisionList);
                }, false, parcelsIssues);
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpGpDecisionBase.prototype.isCheckMade = function (gpDecision) {
                console.warn("Unimplemented function isCheckMade()");
                return false;
            };
            ControllerGrpGpDecisionBase.prototype.GrpGpDecision_itm__cultId_disabled = function (gpDecision) {
                var self = this;
                if (gpDecision === undefined || gpDecision === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpDecision, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isCheckMade(gpDecision);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpDecisionBase.prototype.showLov_GrpGpDecision_itm__cultId = function (gpDecision) {
                var self = this;
                if (gpDecision === undefined)
                    return;
                var uimodel = gpDecision._cultId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Cultivation';
                dialogOptions.previousModel = self.cachedLovModel_GrpGpDecision_itm__cultId;
                dialogOptions.className = "ControllerLovCultivation";
                dialogOptions.onSelect = function (selectedEntity) {
                    var cultivation1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(cultivation1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(cultivation1) && cultivation1.isEqual(gpDecision.cultId))
                        return;
                    gpDecision.cultId = cultivation1;
                    self.markEntityAsUpdated(gpDecision, 'GrpGpDecision_itm__cultId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpGpDecision_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpGpDecisionBase.prototype.GrpGpDecision_itm__cotyId_disabled = function (gpDecision) {
                var self = this;
                if (gpDecision === undefined || gpDecision === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpDecision, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isCheckMade(gpDecision);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpDecisionBase.prototype.showLov_GrpGpDecision_itm__cotyId = function (gpDecision) {
                var self = this;
                if (gpDecision === undefined)
                    return;
                var uimodel = gpDecision._cotyId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Land Cover';
                dialogOptions.previousModel = self.cachedLovModel_GrpGpDecision_itm__cotyId;
                dialogOptions.className = "ControllerLovCoverType";
                dialogOptions.onSelect = function (selectedEntity) {
                    var coverType1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(coverType1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(coverType1) && coverType1.isEqual(gpDecision.cotyId))
                        return;
                    gpDecision.cotyId = coverType1;
                    self.markEntityAsUpdated(gpDecision, 'GrpGpDecision_itm__cotyId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpGpDecision_itm__cotyId = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpGpDecisionBase.prototype._isDisabled = function () {
                if (true)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpDecisionBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpDecisionBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpGpDecisionBase.prototype._deleteIsDisabled = function (gpDecision) {
                return true;
            };
            return ControllerGrpGpDecisionBase;
        })(Controllers.AbstractGroupTableController);
        Dashboard.ControllerGrpGpDecisionBase = ControllerGrpGpDecisionBase;
        // GROUP GrpFmisDecision
        var ModelGrpFmisDecisionBase = (function (_super) {
            __extends(ModelGrpFmisDecisionBase, _super);
            function ModelGrpFmisDecisionBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "fmisDecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].fmisDecisionsId;
                },
                set: function (fmisDecisionsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].fmisDecisionsId = fmisDecisionsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_fmisDecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._fmisDecisionsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "cropOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cropOk;
                },
                set: function (cropOk_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cropOk = cropOk_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_cropOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cropOk;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "landcoverOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].landcoverOk;
                },
                set: function (landcoverOk_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].landcoverOk = landcoverOk_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_landcoverOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._landcoverOk;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "dteInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteInsert;
                },
                set: function (dteInsert_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteInsert = dteInsert_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_dteInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteInsert;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "usrInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].usrInsert;
                },
                set: function (usrInsert_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].usrInsert = usrInsert_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_usrInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._usrInsert;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesId;
                },
                set: function (parcelsIssuesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesId = parcelsIssuesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFmisDecisionBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpFmisDecisionBase;
        })(Controllers.AbstractGroupTableModel);
        Dashboard.ModelGrpFmisDecisionBase = ModelGrpFmisDecisionBase;
        var ControllerGrpFmisDecisionBase = (function (_super) {
            __extends(ControllerGrpFmisDecisionBase, _super);
            function ControllerGrpFmisDecisionBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 2, maxLinesInHeader: 1, maxPageVisibleRows: 2 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpFmisDecision = self.model;
                $scope.GrpFmisDecision_itm__cultId_disabled =
                    function (fmisDecision) {
                        return self.GrpFmisDecision_itm__cultId_disabled(fmisDecision);
                    };
                $scope.showLov_GrpFmisDecision_itm__cultId =
                    function (fmisDecision) {
                        $timeout(function () {
                            self.showLov_GrpFmisDecision_itm__cultId(fmisDecision);
                        }, 0);
                    };
                $scope.GrpFmisDecision_itm__cotyId_disabled =
                    function (fmisDecision) {
                        return self.GrpFmisDecision_itm__cotyId_disabled(fmisDecision);
                    };
                $scope.showLov_GrpFmisDecision_itm__cotyId =
                    function (fmisDecision) {
                        $timeout(function () {
                            self.showLov_GrpFmisDecision_itm__cotyId(fmisDecision);
                        }, 0);
                    };
                $scope.pageModel.modelGrpFmisDecision = $scope.modelGrpFmisDecision;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelsIssues.modelGrpFmisDecision = $scope.modelGrpFmisDecision;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.FmisDecision[] , oldVisible:Entities.FmisDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpFmisDecisionBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpFmisDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Dashboard_ControllerGrpFmisDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.LovItem(function (ent) { return 'Presumed Crop'; }, false, function (ent) { return ent.cultId; }, function (ent) { return ent._cultId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.LovItem(function (ent) { return 'Presumed Land Cover'; }, false, function (ent) { return ent.cotyId; }, function (ent) { return ent._cotyId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.DateItem(function (ent) { return 'Date'; }, false, function (ent) { return ent.dteInsert; }, function (ent) { return ent._dteInsert; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.TextItem(function (ent) { return 'User'; }, false, function (ent) { return ent.usrInsert; }, function (ent) { return ent._usrInsert; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpFmisDecisionBase.prototype.gridTitle = function () {
                return "FMIS Calendars Decisions";
            };
            ControllerGrpFmisDecisionBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpFmisDecisionBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'cultId.name', displayName: 'getALString("Presumed Crop", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpFmisDecision_itm__cultId_disabled(row.entity) && showLov_GrpFmisDecision_itm__cultId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpFmisDecision_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpFmisDecision_itm__cultId(row.entity)'   data-ng-disabled=\"GrpFmisDecision_itm__cultId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cotyId.name', displayName: 'getALString("Presumed Land Cover", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpFmisDecision_itm__cotyId_disabled(row.entity) && showLov_GrpFmisDecision_itm__cotyId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cotyId.name' data-np-ui-model='row.entity._cotyId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpFmisDecision_itm__cotyId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpFmisDecision_itm__cotyId(row.entity)'   data-ng-disabled=\"GrpFmisDecision_itm__cotyId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteInsert', displayName: 'getALString("Date", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpFmisDecision_itm__dteInsert' data-ng-model='row.entity.dteInsert' data-np-ui-model='row.entity._dteInsert' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteInsert&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy hh:mm' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'usrInsert', displayName: 'getALString("User", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpFmisDecision_itm__usrInsert' data-ng-model='row.entity.usrInsert' data-np-ui-model='row.entity._usrInsert' data-ng-change='markEntityAsUpdated(row.entity,&quot;usrInsert&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "modelGrpFmisDecision", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpFmisDecisionBase.prototype.getEntityName = function () {
                return "FmisDecision";
            };
            ControllerGrpFmisDecisionBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpFmisDecisionBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.FmisDecision.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.parcelsIssuesId = _this.Parent;
                    }
                    else {
                        x.parcelsIssuesId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpFmisDecision.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpFmisDecisionBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(cur.parcelsIssuesId, lockKind);
            };
            ControllerGrpFmisDecisionBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpFmisDecisionBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "FmisDecision/findAllByCriteriaRange_DashboardGrpFmisDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpFmisDecisionBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "FmisDecision/findAllByCriteriaRange_DashboardGrpFmisDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpFmisDecisionBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "FmisDecision/findAllByCriteriaRange_DashboardGrpFmisDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpFmisDecisionBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.parcelsIssuesId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "FmisDecision/findAllByCriteriaRange_DashboardGrpFmisDecision";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpFmisDecisionBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                    return false;
                return x.parcelsIssuesId.getKey() === this.Parent.getKey();
            };
            ControllerGrpFmisDecisionBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "parcelsIssuesId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpFmisDecisionBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.FmisDecision(
                /*fmisDecisionsId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*cropOk:number*/ null, 
                /*landcoverOk:number*/ null, 
                /*dteInsert:Date*/ null, 
                /*usrInsert:string*/ null, 
                /*rowVersion:number*/ null, 
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null, 
                /*cultId:Entities.Cultivation*/ null, 
                /*cotyId:Entities.CoverType*/ null);
                return ret;
            };
            ControllerGrpFmisDecisionBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.parcelsIssuesId = self.parcelsIssuesId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpFmisDecisionBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpFmisDecisionBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.FmisDecision.Create();
                ret.updateInstance(src);
                ret.fmisDecisionsId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpFmisDecisionBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (fmisDecisionList) {
                    fmisDecisionList.forEach(function (fmisDecision) {
                        var fmisDecisionCloned = _this.cloneEntity_internal(fmisDecision, true);
                        fmisDecisionCloned.parcelsIssuesId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpFmisDecisionBase.prototype.deleteNewEntitiesUnderParent = function (parcelsIssues) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var fmisDecision = change.a;
                    if (parcelsIssues.getKey() === fmisDecision.parcelsIssuesId.getKey()) {
                        toBeDeleted.push(fmisDecision);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpFmisDecisionBase.prototype.deleteAllEntitiesUnderParent = function (parcelsIssues, afterDeleteAction) {
                var self = this;
                function deleteFmisDecisionList(fmisDecisionList) {
                    if (fmisDecisionList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = fmisDecisionList[0];
                        var tail = fmisDecisionList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteFmisDecisionList(tail);
                        });
                    }
                }
                this.getMergedItems(function (fmisDecisionList) {
                    deleteFmisDecisionList(fmisDecisionList);
                }, false, parcelsIssues);
            };
            Object.defineProperty(ControllerGrpFmisDecisionBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpFmisDecisionBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpFmisDecisionBase.prototype.isCheckMade = function (fmisDecision) {
                console.warn("Unimplemented function isCheckMade()");
                return false;
            };
            ControllerGrpFmisDecisionBase.prototype.GrpFmisDecision_itm__cultId_disabled = function (fmisDecision) {
                var self = this;
                if (fmisDecision === undefined || fmisDecision === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(fmisDecision, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isCheckMade(fmisDecision);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpFmisDecisionBase.prototype.showLov_GrpFmisDecision_itm__cultId = function (fmisDecision) {
                var self = this;
                if (fmisDecision === undefined)
                    return;
                var uimodel = fmisDecision._cultId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Cultivation';
                dialogOptions.previousModel = self.cachedLovModel_GrpFmisDecision_itm__cultId;
                dialogOptions.className = "ControllerLovCultivation";
                dialogOptions.onSelect = function (selectedEntity) {
                    var cultivation1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(cultivation1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(cultivation1) && cultivation1.isEqual(fmisDecision.cultId))
                        return;
                    fmisDecision.cultId = cultivation1;
                    self.markEntityAsUpdated(fmisDecision, 'GrpFmisDecision_itm__cultId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpFmisDecision_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpFmisDecisionBase.prototype.GrpFmisDecision_itm__cotyId_disabled = function (fmisDecision) {
                var self = this;
                if (fmisDecision === undefined || fmisDecision === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(fmisDecision, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isCheckMade(fmisDecision);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpFmisDecisionBase.prototype.showLov_GrpFmisDecision_itm__cotyId = function (fmisDecision) {
                var self = this;
                if (fmisDecision === undefined)
                    return;
                var uimodel = fmisDecision._cotyId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Land Cover';
                dialogOptions.previousModel = self.cachedLovModel_GrpFmisDecision_itm__cotyId;
                dialogOptions.className = "ControllerLovCoverType";
                dialogOptions.onSelect = function (selectedEntity) {
                    var coverType1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(coverType1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(coverType1) && coverType1.isEqual(fmisDecision.cotyId))
                        return;
                    fmisDecision.cotyId = coverType1;
                    self.markEntityAsUpdated(fmisDecision, 'GrpFmisDecision_itm__cotyId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpFmisDecision_itm__cotyId = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpFmisDecisionBase.prototype._isDisabled = function () {
                if (true)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpFmisDecisionBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpFmisDecisionBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpFmisDecisionBase.prototype._deleteIsDisabled = function (fmisDecision) {
                return true;
            };
            return ControllerGrpFmisDecisionBase;
        })(Controllers.AbstractGroupTableController);
        Dashboard.ControllerGrpFmisDecisionBase = ControllerGrpFmisDecisionBase;
        // GROUP GrpIntegrateddecision
        var ModelGrpIntegrateddecisionBase = (function (_super) {
            __extends(ModelGrpIntegrateddecisionBase, _super);
            function ModelGrpIntegrateddecisionBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "integrateddecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].integrateddecisionsId;
                },
                set: function (integrateddecisionsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].integrateddecisionsId = integrateddecisionsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_integrateddecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._integrateddecisionsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "decisionCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].decisionCode;
                },
                set: function (decisionCode_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].decisionCode = decisionCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_decisionCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._decisionCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "dteUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteUpdate;
                },
                set: function (dteUpdate_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteUpdate = dteUpdate_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_dteUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteUpdate;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "usrUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].usrUpdate;
                },
                set: function (usrUpdate_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].usrUpdate = usrUpdate_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_usrUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._usrUpdate;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].pclaId;
                },
                set: function (pclaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].pclaId = pclaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].demaId;
                },
                set: function (demaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].demaId = demaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._demaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpIntegrateddecisionBase;
        })(Controllers.AbstractGroupTableModel);
        Dashboard.ModelGrpIntegrateddecisionBase = ModelGrpIntegrateddecisionBase;
        var ControllerGrpIntegrateddecisionBase = (function (_super) {
            __extends(ControllerGrpIntegrateddecisionBase, _super);
            function ControllerGrpIntegrateddecisionBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 5, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpIntegrateddecision = self.model;
                $scope.GrpIntegrateddecision_itm__dteUpdate_disabled =
                    function (integrateddecision) {
                        return self.GrpIntegrateddecision_itm__dteUpdate_disabled(integrateddecision);
                    };
                $scope.GrpIntegrateddecision_itm__usrUpdate_disabled =
                    function (integrateddecision) {
                        return self.GrpIntegrateddecision_itm__usrUpdate_disabled(integrateddecision);
                    };
                $scope.pageModel.modelGrpIntegrateddecision = $scope.modelGrpIntegrateddecision;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelClas.modelGrpIntegrateddecision = $scope.modelGrpIntegrateddecision;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelClas.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Integrateddecision[] , oldVisible:Entities.Integrateddecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpIntegrateddecisionBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpIntegrateddecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Dashboard_ControllerGrpIntegrateddecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.DateItem(function (ent) { return 'Date'; }, false, function (ent) { return ent.dteUpdate; }, function (ent) { return ent._dteUpdate; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.TextItem(function (ent) { return 'User'; }, false, function (ent) { return ent.usrUpdate; }, function (ent) { return ent._usrUpdate; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpIntegrateddecisionBase.prototype.gridTitle = function () {
                return "Parcels";
            };
            ControllerGrpIntegrateddecisionBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpIntegrateddecisionBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'decisionCode', displayName: 'getALString("Decision Light", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpIntegrateddecision_itm__decisionCode' data-ng-model='row.entity.decisionCode' data-np-ui-model='row.entity._decisionCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionCode&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteUpdate', displayName: 'getALString("Date", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_Dashboard_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__dteUpdate' data-ng-model='row.entity.dteUpdate' data-np-ui-model='row.entity._dteUpdate' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteUpdate&quot;)' data-np-required='true' data-ng-readonly='GrpIntegrateddecision_itm__dteUpdate_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy hh:mm' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'usrUpdate', displayName: 'getALString("User", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__usrUpdate' data-ng-model='row.entity.usrUpdate' data-np-ui-model='row.entity._usrUpdate' data-ng-change='markEntityAsUpdated(row.entity,&quot;usrUpdate&quot;)' data-ng-readonly='GrpIntegrateddecision_itm__usrUpdate_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'demaId.description', displayName: 'getALString("Decision Making Run", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__demaId_description' data-ng-model='row.entity.demaId.description' data-np-ui-model='row.entity.demaId._description' data-ng-change='markEntityAsUpdated(row.entity,&quot;demaId.description&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "modelGrpIntegrateddecision", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpIntegrateddecisionBase.prototype.getEntityName = function () {
                return "Integrateddecision";
            };
            ControllerGrpIntegrateddecisionBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpIntegrateddecisionBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.Integrateddecision.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.pclaId = _this.Parent;
                    }
                    else {
                        x.pclaId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpIntegrateddecision.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpIntegrateddecisionBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelClas.controller.isEntityLocked(cur.pclaId, lockKind);
            };
            ControllerGrpIntegrateddecisionBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpIntegrateddecisionBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DashboardGrpIntegrateddecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpIntegrateddecisionBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DashboardGrpIntegrateddecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpIntegrateddecisionBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DashboardGrpIntegrateddecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpIntegrateddecisionBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.pclaId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "Integrateddecision/findAllByCriteriaRange_DashboardGrpIntegrateddecision";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['pclaId_pclaId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpIntegrateddecisionBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.pclaId === undefined)
                    return false;
                return x.pclaId.getKey() === this.Parent.getKey();
            };
            ControllerGrpIntegrateddecisionBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "pclaId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpIntegrateddecisionBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.Integrateddecision(
                /*integrateddecisionsId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*decisionCode:number*/ null, 
                /*dteUpdate:Date*/ null, 
                /*usrUpdate:string*/ null, 
                /*pclaId:Entities.ParcelClas*/ null, 
                /*demaId:Entities.DecisionMaking*/ null);
                return ret;
            };
            ControllerGrpIntegrateddecisionBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.pclaId = self.pclaId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpIntegrateddecisionBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpIntegrateddecisionBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.Integrateddecision.Create();
                ret.updateInstance(src);
                ret.integrateddecisionsId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpIntegrateddecisionBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (integrateddecisionList) {
                    integrateddecisionList.forEach(function (integrateddecision) {
                        var integrateddecisionCloned = _this.cloneEntity_internal(integrateddecision, true);
                        integrateddecisionCloned.pclaId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpIntegrateddecisionBase.prototype.deleteNewEntitiesUnderParent = function (parcelClas) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var integrateddecision = change.a;
                    if (parcelClas.getKey() === integrateddecision.pclaId.getKey()) {
                        toBeDeleted.push(integrateddecision);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpIntegrateddecisionBase.prototype.deleteAllEntitiesUnderParent = function (parcelClas, afterDeleteAction) {
                var self = this;
                function deleteIntegrateddecisionList(integrateddecisionList) {
                    if (integrateddecisionList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = integrateddecisionList[0];
                        var tail = integrateddecisionList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteIntegrateddecisionList(tail);
                        });
                    }
                }
                this.getMergedItems(function (integrateddecisionList) {
                    deleteIntegrateddecisionList(integrateddecisionList);
                }, false, parcelClas);
            };
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpIntegrateddecisionBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_itm__dteUpdate_disabled = function (integrateddecision) {
                var self = this;
                if (integrateddecision === undefined || integrateddecision === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(integrateddecision, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_itm__usrUpdate_disabled = function (integrateddecision) {
                var self = this;
                if (integrateddecision === undefined || integrateddecision === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(integrateddecision, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpIntegrateddecisionBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelClas_0_2_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelClas_0_2_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpIntegrateddecisionBase.prototype._deleteIsDisabled = function (integrateddecision) {
                return true;
            };
            return ControllerGrpIntegrateddecisionBase;
        })(Controllers.AbstractGroupTableController);
        Dashboard.ControllerGrpIntegrateddecisionBase = ControllerGrpIntegrateddecisionBase;
    })(Dashboard = Controllers.Dashboard || (Controllers.Dashboard = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DashboardBase.js.map