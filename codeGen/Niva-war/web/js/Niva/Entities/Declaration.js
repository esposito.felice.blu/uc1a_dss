//71D4D3DB364E6E7F74A3B6F76D41203C
//Εxtended classes
/// <reference path="../EntitiesBase/DeclarationBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Declaration = (function (_super) {
        __extends(Declaration, _super);
        function Declaration() {
            _super.apply(this, arguments);
        }
        Declaration.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.DeclarationBase.fromJSONComplete(data, deserializedEntities);
        };
        return Declaration;
    })(Entities.DeclarationBase);
    Entities.Declaration = Declaration;
})(Entities || (Entities = {}));
//# sourceMappingURL=Declaration.js.map