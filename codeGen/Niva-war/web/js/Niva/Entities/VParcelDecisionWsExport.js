//C7923C73D527A81B00AF4F6AC385E499
//Εxtended classes
/// <reference path="../EntitiesBase/VParcelDecisionWsExportBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var VParcelDecisionWsExport = (function (_super) {
        __extends(VParcelDecisionWsExport, _super);
        function VParcelDecisionWsExport() {
            _super.apply(this, arguments);
        }
        VParcelDecisionWsExport.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.VParcelDecisionWsExportBase.fromJSONComplete(data, deserializedEntities);
        };
        return VParcelDecisionWsExport;
    })(Entities.VParcelDecisionWsExportBase);
    Entities.VParcelDecisionWsExport = VParcelDecisionWsExport;
})(Entities || (Entities = {}));
//# sourceMappingURL=VParcelDecisionWsExport.js.map