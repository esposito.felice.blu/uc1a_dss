//7F6E780EF7A934B868DE19CDE0B9A795
//Εxtended classes
/// <reference path="../EntitiesBase/EcCultDetailCopyBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcCultDetailCopy = (function (_super) {
        __extends(EcCultDetailCopy, _super);
        function EcCultDetailCopy() {
            _super.apply(this, arguments);
        }
        EcCultDetailCopy.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.EcCultDetailCopyBase.fromJSONComplete(data, deserializedEntities);
        };
        return EcCultDetailCopy;
    })(Entities.EcCultDetailCopyBase);
    Entities.EcCultDetailCopy = EcCultDetailCopy;
})(Entities || (Entities = {}));
//# sourceMappingURL=EcCultDetailCopy.js.map