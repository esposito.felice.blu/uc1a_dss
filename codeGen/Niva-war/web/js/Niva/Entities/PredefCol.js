//84E9096A7EF038441E4BE92AFB07481E
//Εxtended classes
/// <reference path="../EntitiesBase/PredefColBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var PredefCol = (function (_super) {
        __extends(PredefCol, _super);
        function PredefCol() {
            _super.apply(this, arguments);
        }
        PredefCol.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.PredefColBase.fromJSONComplete(data, deserializedEntities);
        };
        return PredefCol;
    })(Entities.PredefColBase);
    Entities.PredefCol = PredefCol;
})(Entities || (Entities = {}));
//# sourceMappingURL=PredefCol.js.map