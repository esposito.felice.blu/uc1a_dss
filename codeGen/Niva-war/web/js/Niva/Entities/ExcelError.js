//CB3B97821C9756A90CFD9F0AACF5E1F2
//Εxtended classes
/// <reference path="../EntitiesBase/ExcelErrorBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ExcelError = (function (_super) {
        __extends(ExcelError, _super);
        function ExcelError() {
            _super.apply(this, arguments);
        }
        ExcelError.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ExcelErrorBase.fromJSONComplete(data, deserializedEntities);
        };
        return ExcelError;
    })(Entities.ExcelErrorBase);
    Entities.ExcelError = ExcelError;
})(Entities || (Entities = {}));
//# sourceMappingURL=ExcelError.js.map