//C717CA776607FCBAA78BAB0FC26317ED
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/FileTemplateBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var FileTemplate;
    (function (FileTemplate) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(FileTemplate.PageModelBase);
        FileTemplate.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(FileTemplate.PageControllerBase);
        FileTemplate.PageController = PageController;
        g_controllers['FileTemplate.PageController'] = Controllers.FileTemplate.PageController;
        var ModelGrpFileTemplate = (function (_super) {
            __extends(ModelGrpFileTemplate, _super);
            function ModelGrpFileTemplate() {
                _super.apply(this, arguments);
            }
            return ModelGrpFileTemplate;
        })(FileTemplate.ModelGrpFileTemplateBase);
        FileTemplate.ModelGrpFileTemplate = ModelGrpFileTemplate;
        var ControllerGrpFileTemplate = (function (_super) {
            __extends(ControllerGrpFileTemplate, _super);
            function ControllerGrpFileTemplate($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpFileTemplate($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpFileTemplate;
        })(FileTemplate.ControllerGrpFileTemplateBase);
        FileTemplate.ControllerGrpFileTemplate = ControllerGrpFileTemplate;
        g_controllers['FileTemplate.ControllerGrpFileTemplate'] = Controllers.FileTemplate.ControllerGrpFileTemplate;
        var ModelGrpTemplateColumn = (function (_super) {
            __extends(ModelGrpTemplateColumn, _super);
            function ModelGrpTemplateColumn() {
                _super.apply(this, arguments);
            }
            return ModelGrpTemplateColumn;
        })(FileTemplate.ModelGrpTemplateColumnBase);
        FileTemplate.ModelGrpTemplateColumn = ModelGrpTemplateColumn;
        var ControllerGrpTemplateColumn = (function (_super) {
            __extends(ControllerGrpTemplateColumn, _super);
            function ControllerGrpTemplateColumn($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpTemplateColumn($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpTemplateColumn;
        })(FileTemplate.ControllerGrpTemplateColumnBase);
        FileTemplate.ControllerGrpTemplateColumn = ControllerGrpTemplateColumn;
        g_controllers['FileTemplate.ControllerGrpTemplateColumn'] = Controllers.FileTemplate.ControllerGrpTemplateColumn;
    })(FileTemplate = Controllers.FileTemplate || (Controllers.FileTemplate = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=FileTemplate.js.map