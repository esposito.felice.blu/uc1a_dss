//964E1CBE9A9D042A3B7FA58C0AD341BB
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/SuperClasSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelSuperClasSearch = (function (_super) {
        __extends(ModelSuperClasSearch, _super);
        function ModelSuperClasSearch() {
            _super.apply(this, arguments);
        }
        return ModelSuperClasSearch;
    })(Controllers.ModelSuperClasSearchBase);
    Controllers.ModelSuperClasSearch = ModelSuperClasSearch;
    var ControllerSuperClasSearch = (function (_super) {
        __extends(ControllerSuperClasSearch, _super);
        function ControllerSuperClasSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelSuperClasSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerSuperClasSearch;
    })(Controllers.ControllerSuperClasSearchBase);
    Controllers.ControllerSuperClasSearch = ControllerSuperClasSearch;
    g_controllers['ControllerSuperClasSearch'] = Controllers.ControllerSuperClasSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=SuperClasSearch.js.map