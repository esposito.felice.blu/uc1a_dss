//9FF3987C0D6B69A3402563B74BD68DCA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovClassifierBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovClassifier = (function (_super) {
        __extends(ModelLovClassifier, _super);
        function ModelLovClassifier() {
            _super.apply(this, arguments);
        }
        return ModelLovClassifier;
    })(Controllers.ModelLovClassifierBase);
    Controllers.ModelLovClassifier = ModelLovClassifier;
    var ControllerLovClassifier = (function (_super) {
        __extends(ControllerLovClassifier, _super);
        function ControllerLovClassifier($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovClassifier($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovClassifier;
    })(Controllers.ControllerLovClassifierBase);
    Controllers.ControllerLovClassifier = ControllerLovClassifier;
    g_controllers['ControllerLovClassifier'] = Controllers.ControllerLovClassifier;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovClassifier.js.map