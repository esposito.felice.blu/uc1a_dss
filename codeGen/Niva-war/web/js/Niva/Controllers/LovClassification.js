//9E281A60648BD200853B0B40F977A1CB
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovClassificationBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovClassification = (function (_super) {
        __extends(ModelLovClassification, _super);
        function ModelLovClassification() {
            _super.apply(this, arguments);
        }
        return ModelLovClassification;
    })(Controllers.ModelLovClassificationBase);
    Controllers.ModelLovClassification = ModelLovClassification;
    var ControllerLovClassification = (function (_super) {
        __extends(ControllerLovClassification, _super);
        function ControllerLovClassification($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovClassification($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovClassification;
    })(Controllers.ControllerLovClassificationBase);
    Controllers.ControllerLovClassification = ControllerLovClassification;
    g_controllers['ControllerLovClassification'] = Controllers.ControllerLovClassification;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovClassification.js.map