//43983D79AE1BD14417C8F223FE320530
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovAgrisnapUsersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovAgrisnapUsers = (function (_super) {
        __extends(ModelLovAgrisnapUsers, _super);
        function ModelLovAgrisnapUsers() {
            _super.apply(this, arguments);
        }
        return ModelLovAgrisnapUsers;
    })(Controllers.ModelLovAgrisnapUsersBase);
    Controllers.ModelLovAgrisnapUsers = ModelLovAgrisnapUsers;
    var ControllerLovAgrisnapUsers = (function (_super) {
        __extends(ControllerLovAgrisnapUsers, _super);
        function ControllerLovAgrisnapUsers($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovAgrisnapUsers($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovAgrisnapUsers;
    })(Controllers.ControllerLovAgrisnapUsersBase);
    Controllers.ControllerLovAgrisnapUsers = ControllerLovAgrisnapUsers;
    g_controllers['ControllerLovAgrisnapUsers'] = Controllers.ControllerLovAgrisnapUsers;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovAgrisnapUsers.js.map