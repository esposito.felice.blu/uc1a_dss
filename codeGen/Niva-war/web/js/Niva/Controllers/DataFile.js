//2C500C55C7D4BC26700693E98863FA73
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DataFileBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var DataFile;
    (function (DataFile) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(DataFile.PageModelBase);
        DataFile.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(DataFile.PageControllerBase);
        DataFile.PageController = PageController;
        g_controllers['DataFile.PageController'] = Controllers.DataFile.PageController;
        var ModelGrpDataFile = (function (_super) {
            __extends(ModelGrpDataFile, _super);
            function ModelGrpDataFile() {
                _super.apply(this, arguments);
            }
            return ModelGrpDataFile;
        })(DataFile.ModelGrpDataFileBase);
        DataFile.ModelGrpDataFile = ModelGrpDataFile;
        var ControllerGrpDataFile = (function (_super) {
            __extends(ControllerGrpDataFile, _super);
            function ControllerGrpDataFile($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpDataFile($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpDataFile;
        })(DataFile.ControllerGrpDataFileBase);
        DataFile.ControllerGrpDataFile = ControllerGrpDataFile;
        g_controllers['DataFile.ControllerGrpDataFile'] = Controllers.DataFile.ControllerGrpDataFile;
    })(DataFile = Controllers.DataFile || (Controllers.DataFile = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DataFile.js.map