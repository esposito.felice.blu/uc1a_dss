//E6F2120D6F702545374017526B7F32C2
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelFmisCalendarBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ParcelFmisCalendar;
    (function (ParcelFmisCalendar) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(ParcelFmisCalendar.PageModelBase);
        ParcelFmisCalendar.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(ParcelFmisCalendar.PageControllerBase);
        ParcelFmisCalendar.PageController = PageController;
        g_controllers['ParcelFmisCalendar.PageController'] = Controllers.ParcelFmisCalendar.PageController;
        var ModelGrpParcel = (function (_super) {
            __extends(ModelGrpParcel, _super);
            function ModelGrpParcel() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcel;
        })(ParcelFmisCalendar.ModelGrpParcelBase);
        ParcelFmisCalendar.ModelGrpParcel = ModelGrpParcel;
        var ControllerGrpParcel = (function (_super) {
            __extends(ControllerGrpParcel, _super);
            function ControllerGrpParcel($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcel;
        })(ParcelFmisCalendar.ControllerGrpParcelBase);
        ParcelFmisCalendar.ControllerGrpParcel = ControllerGrpParcel;
        g_controllers['ParcelFmisCalendar.ControllerGrpParcel'] = Controllers.ParcelFmisCalendar.ControllerGrpParcel;
        var ModelGrpFmisCalendar = (function (_super) {
            __extends(ModelGrpFmisCalendar, _super);
            function ModelGrpFmisCalendar() {
                _super.apply(this, arguments);
            }
            return ModelGrpFmisCalendar;
        })(ParcelFmisCalendar.ModelGrpFmisCalendarBase);
        ParcelFmisCalendar.ModelGrpFmisCalendar = ModelGrpFmisCalendar;
        var ControllerGrpFmisCalendar = (function (_super) {
            __extends(ControllerGrpFmisCalendar, _super);
            function ControllerGrpFmisCalendar($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpFmisCalendar($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpFmisCalendar;
        })(ParcelFmisCalendar.ControllerGrpFmisCalendarBase);
        ParcelFmisCalendar.ControllerGrpFmisCalendar = ControllerGrpFmisCalendar;
        g_controllers['ParcelFmisCalendar.ControllerGrpFmisCalendar'] = Controllers.ParcelFmisCalendar.ControllerGrpFmisCalendar;
    })(ParcelFmisCalendar = Controllers.ParcelFmisCalendar || (Controllers.ParcelFmisCalendar = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelFmisCalendar.js.map