//4B3BEA8915520A985F435EA028933116
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelsIssuesActivitiesBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ParcelsIssuesActivities;
    (function (ParcelsIssuesActivities) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(ParcelsIssuesActivities.PageModelBase);
        ParcelsIssuesActivities.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(ParcelsIssuesActivities.PageControllerBase);
        ParcelsIssuesActivities.PageController = PageController;
        g_controllers['ParcelsIssuesActivities.PageController'] = Controllers.ParcelsIssuesActivities.PageController;
        var ModelGrpParcelsIssuesActivities = (function (_super) {
            __extends(ModelGrpParcelsIssuesActivities, _super);
            function ModelGrpParcelsIssuesActivities() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelsIssuesActivities;
        })(ParcelsIssuesActivities.ModelGrpParcelsIssuesActivitiesBase);
        ParcelsIssuesActivities.ModelGrpParcelsIssuesActivities = ModelGrpParcelsIssuesActivities;
        var ControllerGrpParcelsIssuesActivities = (function (_super) {
            __extends(ControllerGrpParcelsIssuesActivities, _super);
            function ControllerGrpParcelsIssuesActivities($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelsIssuesActivities($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelsIssuesActivities;
        })(ParcelsIssuesActivities.ControllerGrpParcelsIssuesActivitiesBase);
        ParcelsIssuesActivities.ControllerGrpParcelsIssuesActivities = ControllerGrpParcelsIssuesActivities;
        g_controllers['ParcelsIssuesActivities.ControllerGrpParcelsIssuesActivities'] = Controllers.ParcelsIssuesActivities.ControllerGrpParcelsIssuesActivities;
    })(ParcelsIssuesActivities = Controllers.ParcelsIssuesActivities || (Controllers.ParcelsIssuesActivities = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelsIssuesActivities.js.map