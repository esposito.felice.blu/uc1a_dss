//45AC6330659930FBF3F2BED31440634C
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/GpUploadBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var GpUpload;
    (function (GpUpload) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(GpUpload.PageModelBase);
        GpUpload.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(GpUpload.PageControllerBase);
        GpUpload.PageController = PageController;
        g_controllers['GpUpload.PageController'] = Controllers.GpUpload.PageController;
        var ModelGrpGpUpload = (function (_super) {
            __extends(ModelGrpGpUpload, _super);
            function ModelGrpGpUpload() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpUpload;
        })(GpUpload.ModelGrpGpUploadBase);
        GpUpload.ModelGrpGpUpload = ModelGrpGpUpload;
        var ControllerGrpGpUpload = (function (_super) {
            __extends(ControllerGrpGpUpload, _super);
            function ControllerGrpGpUpload($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpUpload($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpUpload;
        })(GpUpload.ControllerGrpGpUploadBase);
        GpUpload.ControllerGrpGpUpload = ControllerGrpGpUpload;
        g_controllers['GpUpload.ControllerGrpGpUpload'] = Controllers.GpUpload.ControllerGrpGpUpload;
    })(GpUpload = Controllers.GpUpload || (Controllers.GpUpload = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=GpUpload.js.map