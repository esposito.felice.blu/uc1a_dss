//B1CA32A3DA7DC612DECF2FA90DF2453D
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovCultivationBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovCultivation = (function (_super) {
        __extends(ModelLovCultivation, _super);
        function ModelLovCultivation() {
            _super.apply(this, arguments);
        }
        return ModelLovCultivation;
    })(Controllers.ModelLovCultivationBase);
    Controllers.ModelLovCultivation = ModelLovCultivation;
    var ControllerLovCultivation = (function (_super) {
        __extends(ControllerLovCultivation, _super);
        function ControllerLovCultivation($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovCultivation($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovCultivation;
    })(Controllers.ControllerLovCultivationBase);
    Controllers.ControllerLovCultivation = ControllerLovCultivation;
    g_controllers['ControllerLovCultivation'] = Controllers.ControllerLovCultivation;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovCultivation.js.map