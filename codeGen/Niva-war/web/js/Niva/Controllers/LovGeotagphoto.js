//68E66069DA0F8EF566865D2CF16F0235
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovGeotagphotoBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovGeotagphoto = (function (_super) {
        __extends(ModelLovGeotagphoto, _super);
        function ModelLovGeotagphoto() {
            _super.apply(this, arguments);
        }
        return ModelLovGeotagphoto;
    })(Controllers.ModelLovGeotagphotoBase);
    Controllers.ModelLovGeotagphoto = ModelLovGeotagphoto;
    var ControllerLovGeotagphoto = (function (_super) {
        __extends(ControllerLovGeotagphoto, _super);
        function ControllerLovGeotagphoto($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovGeotagphoto($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovGeotagphoto;
    })(Controllers.ControllerLovGeotagphotoBase);
    Controllers.ControllerLovGeotagphoto = ControllerLovGeotagphoto;
    g_controllers['ControllerLovGeotagphoto'] = Controllers.ControllerLovGeotagphoto;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovGeotagphoto.js.map