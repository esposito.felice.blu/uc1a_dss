//ED7E7013463D45922C09F3C66B9E3F18
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelClasBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ParcelClas;
    (function (ParcelClas) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(ParcelClas.PageModelBase);
        ParcelClas.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(ParcelClas.PageControllerBase);
        ParcelClas.PageController = PageController;
        g_controllers['ParcelClas.PageController'] = Controllers.ParcelClas.PageController;
        var ModelGrpParcelClas = (function (_super) {
            __extends(ModelGrpParcelClas, _super);
            function ModelGrpParcelClas() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelClas;
        })(ParcelClas.ModelGrpParcelClasBase);
        ParcelClas.ModelGrpParcelClas = ModelGrpParcelClas;
        var ControllerGrpParcelClas = (function (_super) {
            __extends(ControllerGrpParcelClas, _super);
            function ControllerGrpParcelClas($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelClas($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelClas;
        })(ParcelClas.ControllerGrpParcelClasBase);
        ParcelClas.ControllerGrpParcelClas = ControllerGrpParcelClas;
        g_controllers['ParcelClas.ControllerGrpParcelClas'] = Controllers.ParcelClas.ControllerGrpParcelClas;
        var ModelGrpParcActivity = (function (_super) {
            __extends(ModelGrpParcActivity, _super);
            function ModelGrpParcActivity() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcActivity;
        })(ParcelClas.ModelGrpParcActivityBase);
        ParcelClas.ModelGrpParcActivity = ModelGrpParcActivity;
        var ControllerGrpParcActivity = (function (_super) {
            __extends(ControllerGrpParcActivity, _super);
            function ControllerGrpParcActivity($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcActivity($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcActivity;
        })(ParcelClas.ControllerGrpParcActivityBase);
        ParcelClas.ControllerGrpParcActivity = ControllerGrpParcActivity;
        g_controllers['ParcelClas.ControllerGrpParcActivity'] = Controllers.ParcelClas.ControllerGrpParcActivity;
        var ModelGrpIntegrateddecision = (function (_super) {
            __extends(ModelGrpIntegrateddecision, _super);
            function ModelGrpIntegrateddecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpIntegrateddecision;
        })(ParcelClas.ModelGrpIntegrateddecisionBase);
        ParcelClas.ModelGrpIntegrateddecision = ModelGrpIntegrateddecision;
        var ControllerGrpIntegrateddecision = (function (_super) {
            __extends(ControllerGrpIntegrateddecision, _super);
            function ControllerGrpIntegrateddecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpIntegrateddecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpIntegrateddecision;
        })(ParcelClas.ControllerGrpIntegrateddecisionBase);
        ParcelClas.ControllerGrpIntegrateddecision = ControllerGrpIntegrateddecision;
        g_controllers['ParcelClas.ControllerGrpIntegrateddecision'] = Controllers.ParcelClas.ControllerGrpIntegrateddecision;
        var ModelGrpParcelsIssues = (function (_super) {
            __extends(ModelGrpParcelsIssues, _super);
            function ModelGrpParcelsIssues() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelsIssues;
        })(ParcelClas.ModelGrpParcelsIssuesBase);
        ParcelClas.ModelGrpParcelsIssues = ModelGrpParcelsIssues;
        var ControllerGrpParcelsIssues = (function (_super) {
            __extends(ControllerGrpParcelsIssues, _super);
            function ControllerGrpParcelsIssues($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelsIssues($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelsIssues;
        })(ParcelClas.ControllerGrpParcelsIssuesBase);
        ParcelClas.ControllerGrpParcelsIssues = ControllerGrpParcelsIssues;
        g_controllers['ParcelClas.ControllerGrpParcelsIssues'] = Controllers.ParcelClas.ControllerGrpParcelsIssues;
        var ModelGrpGpDecision = (function (_super) {
            __extends(ModelGrpGpDecision, _super);
            function ModelGrpGpDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpDecision;
        })(ParcelClas.ModelGrpGpDecisionBase);
        ParcelClas.ModelGrpGpDecision = ModelGrpGpDecision;
        var ControllerGrpGpDecision = (function (_super) {
            __extends(ControllerGrpGpDecision, _super);
            function ControllerGrpGpDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpDecision;
        })(ParcelClas.ControllerGrpGpDecisionBase);
        ParcelClas.ControllerGrpGpDecision = ControllerGrpGpDecision;
        g_controllers['ParcelClas.ControllerGrpGpDecision'] = Controllers.ParcelClas.ControllerGrpGpDecision;
        var ModelGrpGpRequestsContexts = (function (_super) {
            __extends(ModelGrpGpRequestsContexts, _super);
            function ModelGrpGpRequestsContexts() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpRequestsContexts;
        })(ParcelClas.ModelGrpGpRequestsContextsBase);
        ParcelClas.ModelGrpGpRequestsContexts = ModelGrpGpRequestsContexts;
        var ControllerGrpGpRequestsContexts = (function (_super) {
            __extends(ControllerGrpGpRequestsContexts, _super);
            function ControllerGrpGpRequestsContexts($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpRequestsContexts($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpRequestsContexts;
        })(ParcelClas.ControllerGrpGpRequestsContextsBase);
        ParcelClas.ControllerGrpGpRequestsContexts = ControllerGrpGpRequestsContexts;
        g_controllers['ParcelClas.ControllerGrpGpRequestsContexts'] = Controllers.ParcelClas.ControllerGrpGpRequestsContexts;
        var ModelGrpGpUpload = (function (_super) {
            __extends(ModelGrpGpUpload, _super);
            function ModelGrpGpUpload() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpUpload;
        })(ParcelClas.ModelGrpGpUploadBase);
        ParcelClas.ModelGrpGpUpload = ModelGrpGpUpload;
        var ControllerGrpGpUpload = (function (_super) {
            __extends(ControllerGrpGpUpload, _super);
            function ControllerGrpGpUpload($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpUpload($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpUpload;
        })(ParcelClas.ControllerGrpGpUploadBase);
        ParcelClas.ControllerGrpGpUpload = ControllerGrpGpUpload;
        g_controllers['ParcelClas.ControllerGrpGpUpload'] = Controllers.ParcelClas.ControllerGrpGpUpload;
        var ModelGrpFmisDecision = (function (_super) {
            __extends(ModelGrpFmisDecision, _super);
            function ModelGrpFmisDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpFmisDecision;
        })(ParcelClas.ModelGrpFmisDecisionBase);
        ParcelClas.ModelGrpFmisDecision = ModelGrpFmisDecision;
        var ControllerGrpFmisDecision = (function (_super) {
            __extends(ControllerGrpFmisDecision, _super);
            function ControllerGrpFmisDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpFmisDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpFmisDecision;
        })(ParcelClas.ControllerGrpFmisDecisionBase);
        ParcelClas.ControllerGrpFmisDecision = ControllerGrpFmisDecision;
        g_controllers['ParcelClas.ControllerGrpFmisDecision'] = Controllers.ParcelClas.ControllerGrpFmisDecision;
        var ModelGrpFmisUpload = (function (_super) {
            __extends(ModelGrpFmisUpload, _super);
            function ModelGrpFmisUpload() {
                _super.apply(this, arguments);
            }
            return ModelGrpFmisUpload;
        })(ParcelClas.ModelGrpFmisUploadBase);
        ParcelClas.ModelGrpFmisUpload = ModelGrpFmisUpload;
        var ControllerGrpFmisUpload = (function (_super) {
            __extends(ControllerGrpFmisUpload, _super);
            function ControllerGrpFmisUpload($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpFmisUpload($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpFmisUpload;
        })(ParcelClas.ControllerGrpFmisUploadBase);
        ParcelClas.ControllerGrpFmisUpload = ControllerGrpFmisUpload;
        g_controllers['ParcelClas.ControllerGrpFmisUpload'] = Controllers.ParcelClas.ControllerGrpFmisUpload;
        var ModelGrpParcelsIssuesActivities = (function (_super) {
            __extends(ModelGrpParcelsIssuesActivities, _super);
            function ModelGrpParcelsIssuesActivities() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelsIssuesActivities;
        })(ParcelClas.ModelGrpParcelsIssuesActivitiesBase);
        ParcelClas.ModelGrpParcelsIssuesActivities = ModelGrpParcelsIssuesActivities;
        var ControllerGrpParcelsIssuesActivities = (function (_super) {
            __extends(ControllerGrpParcelsIssuesActivities, _super);
            function ControllerGrpParcelsIssuesActivities($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelsIssuesActivities($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelsIssuesActivities;
        })(ParcelClas.ControllerGrpParcelsIssuesActivitiesBase);
        ParcelClas.ControllerGrpParcelsIssuesActivities = ControllerGrpParcelsIssuesActivities;
        g_controllers['ParcelClas.ControllerGrpParcelsIssuesActivities'] = Controllers.ParcelClas.ControllerGrpParcelsIssuesActivities;
        var ModelGrpParcelDecision = (function (_super) {
            __extends(ModelGrpParcelDecision, _super);
            function ModelGrpParcelDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelDecision;
        })(ParcelClas.ModelGrpParcelDecisionBase);
        ParcelClas.ModelGrpParcelDecision = ModelGrpParcelDecision;
        var ControllerGrpParcelDecision = (function (_super) {
            __extends(ControllerGrpParcelDecision, _super);
            function ControllerGrpParcelDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelDecision;
        })(ParcelClas.ControllerGrpParcelDecisionBase);
        ParcelClas.ControllerGrpParcelDecision = ControllerGrpParcelDecision;
        g_controllers['ParcelClas.ControllerGrpParcelDecision'] = Controllers.ParcelClas.ControllerGrpParcelDecision;
    })(ParcelClas = Controllers.ParcelClas || (Controllers.ParcelClas = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelClas.js.map