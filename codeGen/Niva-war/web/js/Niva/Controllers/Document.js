//1865ADC11B521158221BD2E9F71ACF6F
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DocumentBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var Document;
    (function (Document) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(Document.PageModelBase);
        Document.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(Document.PageControllerBase);
        Document.PageController = PageController;
        g_controllers['Document.PageController'] = Controllers.Document.PageController;
        var ModelGrpDocument = (function (_super) {
            __extends(ModelGrpDocument, _super);
            function ModelGrpDocument() {
                _super.apply(this, arguments);
            }
            return ModelGrpDocument;
        })(Document.ModelGrpDocumentBase);
        Document.ModelGrpDocument = ModelGrpDocument;
        var ControllerGrpDocument = (function (_super) {
            __extends(ControllerGrpDocument, _super);
            function ControllerGrpDocument($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpDocument($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                // document.getElementById('vgp').innerHTML = "<button class="btnDownload"><i class="fa fa-download"></i> Download</button>";
            }
            return ControllerGrpDocument;
        })(Document.ControllerGrpDocumentBase);
        Document.ControllerGrpDocument = ControllerGrpDocument;
        g_controllers['Document.ControllerGrpDocument'] = Controllers.Document.ControllerGrpDocument;
    })(Document = Controllers.Document || (Controllers.Document = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=Document.js.map