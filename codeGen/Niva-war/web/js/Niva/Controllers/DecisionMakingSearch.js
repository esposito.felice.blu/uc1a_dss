//5D7BDBF32C05A2C467C3B671DDD8DE96
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DecisionMakingSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelDecisionMakingSearch = (function (_super) {
        __extends(ModelDecisionMakingSearch, _super);
        function ModelDecisionMakingSearch() {
            _super.apply(this, arguments);
        }
        return ModelDecisionMakingSearch;
    })(Controllers.ModelDecisionMakingSearchBase);
    Controllers.ModelDecisionMakingSearch = ModelDecisionMakingSearch;
    var ControllerDecisionMakingSearch = (function (_super) {
        __extends(ControllerDecisionMakingSearch, _super);
        function ControllerDecisionMakingSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelDecisionMakingSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        ControllerDecisionMakingSearch.prototype.getLabelById = function (itmId) {
            switch (itmId) {
                case 'saveBtn_id':
                    return 'Save';
                case 'searchBtn_id':
                    return 'Search';
                case 'clearBtn_id':
                    return 'Clear';
                case 'newBtn_id':
                    return 'Add New Decision Making';
                case 'newPageBtn_id':
                    return 'Add New Decision Making';
                case 'cancelBtn_id':
                    return 'Back';
                case 'deleteBtn_id':
                    return 'Delete';
                case 'auditBtn_id':
                    return '?';
            }
            return itmId;
        };
        return ControllerDecisionMakingSearch;
    })(Controllers.ControllerDecisionMakingSearchBase);
    Controllers.ControllerDecisionMakingSearch = ControllerDecisionMakingSearch;
    g_controllers['ControllerDecisionMakingSearch'] = Controllers.ControllerDecisionMakingSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DecisionMakingSearch.js.map