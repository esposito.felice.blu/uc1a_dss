//44548F4D5A9CA35EFE398A1A2684FF58
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/AgrisnapUsersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var AgrisnapUsers;
    (function (AgrisnapUsers) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(AgrisnapUsers.PageModelBase);
        AgrisnapUsers.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(AgrisnapUsers.PageControllerBase);
        AgrisnapUsers.PageController = PageController;
        g_controllers['AgrisnapUsers.PageController'] = Controllers.AgrisnapUsers.PageController;
        var ModelGrpAgrisnapUsers = (function (_super) {
            __extends(ModelGrpAgrisnapUsers, _super);
            function ModelGrpAgrisnapUsers() {
                _super.apply(this, arguments);
            }
            return ModelGrpAgrisnapUsers;
        })(AgrisnapUsers.ModelGrpAgrisnapUsersBase);
        AgrisnapUsers.ModelGrpAgrisnapUsers = ModelGrpAgrisnapUsers;
        var ControllerGrpAgrisnapUsers = (function (_super) {
            __extends(ControllerGrpAgrisnapUsers, _super);
            function ControllerGrpAgrisnapUsers($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpAgrisnapUsers($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpAgrisnapUsers;
        })(AgrisnapUsers.ControllerGrpAgrisnapUsersBase);
        AgrisnapUsers.ControllerGrpAgrisnapUsers = ControllerGrpAgrisnapUsers;
        g_controllers['AgrisnapUsers.ControllerGrpAgrisnapUsers'] = Controllers.AgrisnapUsers.ControllerGrpAgrisnapUsers;
        var ModelGrpAgrisnapUsersProducers = (function (_super) {
            __extends(ModelGrpAgrisnapUsersProducers, _super);
            function ModelGrpAgrisnapUsersProducers() {
                _super.apply(this, arguments);
            }
            return ModelGrpAgrisnapUsersProducers;
        })(AgrisnapUsers.ModelGrpAgrisnapUsersProducersBase);
        AgrisnapUsers.ModelGrpAgrisnapUsersProducers = ModelGrpAgrisnapUsersProducers;
        var ControllerGrpAgrisnapUsersProducers = (function (_super) {
            __extends(ControllerGrpAgrisnapUsersProducers, _super);
            function ControllerGrpAgrisnapUsersProducers($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpAgrisnapUsersProducers($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpAgrisnapUsersProducers;
        })(AgrisnapUsers.ControllerGrpAgrisnapUsersProducersBase);
        AgrisnapUsers.ControllerGrpAgrisnapUsersProducers = ControllerGrpAgrisnapUsersProducers;
        g_controllers['AgrisnapUsers.ControllerGrpAgrisnapUsersProducers'] = Controllers.AgrisnapUsers.ControllerGrpAgrisnapUsersProducers;
    })(AgrisnapUsers = Controllers.AgrisnapUsers || (Controllers.AgrisnapUsers = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=AgrisnapUsers.js.map