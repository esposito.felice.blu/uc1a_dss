/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/FmisDecision.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FmisDecisionBase = (function (_super) {
        __extends(FmisDecisionBase, _super);
        function FmisDecisionBase(fmisDecisionsId, cropOk, landcoverOk, dteInsert, usrInsert, rowVersion, parcelsIssuesId, cultId, cotyId) {
            _super.call(this);
            var self = this;
            this._fmisDecisionsId = new NpTypes.UIStringModel(fmisDecisionsId, this);
            this._cropOk = new NpTypes.UINumberModel(cropOk, this);
            this._landcoverOk = new NpTypes.UINumberModel(landcoverOk, this);
            this._dteInsert = new NpTypes.UIDateModel(dteInsert, this);
            this._usrInsert = new NpTypes.UIStringModel(usrInsert, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._parcelsIssuesId = new NpTypes.UIManyToOneModel(parcelsIssuesId, this);
            this._cultId = new NpTypes.UIManyToOneModel(cultId, this);
            this._cotyId = new NpTypes.UIManyToOneModel(cotyId, this);
            self.postConstruct();
        }
        FmisDecisionBase.prototype.getKey = function () {
            return this.fmisDecisionsId;
        };
        FmisDecisionBase.prototype.getKeyName = function () {
            return "fmisDecisionsId";
        };
        FmisDecisionBase.prototype.getEntityName = function () {
            return 'FmisDecision';
        };
        FmisDecisionBase.prototype.fromJSON = function (data) {
            return Entities.FmisDecision.fromJSONComplete(data);
        };
        Object.defineProperty(FmisDecisionBase.prototype, "fmisDecisionsId", {
            get: function () {
                return this._fmisDecisionsId.value;
            },
            set: function (fmisDecisionsId) {
                var self = this;
                self._fmisDecisionsId.value = fmisDecisionsId;
                //    self.fmisDecisionsId_listeners.forEach(cb => { cb(<FmisDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisDecisionBase.prototype, "cropOk", {
            get: function () {
                return this._cropOk.value;
            },
            set: function (cropOk) {
                var self = this;
                self._cropOk.value = cropOk;
                //    self.cropOk_listeners.forEach(cb => { cb(<FmisDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisDecisionBase.prototype, "landcoverOk", {
            get: function () {
                return this._landcoverOk.value;
            },
            set: function (landcoverOk) {
                var self = this;
                self._landcoverOk.value = landcoverOk;
                //    self.landcoverOk_listeners.forEach(cb => { cb(<FmisDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisDecisionBase.prototype, "dteInsert", {
            get: function () {
                return this._dteInsert.value;
            },
            set: function (dteInsert) {
                var self = this;
                self._dteInsert.value = dteInsert;
                //    self.dteInsert_listeners.forEach(cb => { cb(<FmisDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisDecisionBase.prototype, "usrInsert", {
            get: function () {
                return this._usrInsert.value;
            },
            set: function (usrInsert) {
                var self = this;
                self._usrInsert.value = usrInsert;
                //    self.usrInsert_listeners.forEach(cb => { cb(<FmisDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisDecisionBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<FmisDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisDecisionBase.prototype, "parcelsIssuesId", {
            get: function () {
                return this._parcelsIssuesId.value;
            },
            set: function (parcelsIssuesId) {
                var self = this;
                self._parcelsIssuesId.value = parcelsIssuesId;
                //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<FmisDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisDecisionBase.prototype, "cultId", {
            get: function () {
                return this._cultId.value;
            },
            set: function (cultId) {
                var self = this;
                self._cultId.value = cultId;
                //    self.cultId_listeners.forEach(cb => { cb(<FmisDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisDecisionBase.prototype, "cotyId", {
            get: function () {
                return this._cotyId.value;
            },
            set: function (cotyId) {
                var self = this;
                self._cotyId.value = cotyId;
                //    self.cotyId_listeners.forEach(cb => { cb(<FmisDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public cotyId_listeners: Array<(c:FmisDecision) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.fmisDecisionsId_listeners.splice(0);
            self.cropOk_listeners.splice(0);
            self.landcoverOk_listeners.splice(0);
            self.dteInsert_listeners.splice(0);
            self.usrInsert_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.parcelsIssuesId_listeners.splice(0);
            self.cultId_listeners.splice(0);
            self.cotyId_listeners.splice(0);
        }
        */
        FmisDecisionBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "FmisDecision:" + x.fmisDecisionsId;
            var ret = new Entities.FmisDecision(x.fmisDecisionsId, x.cropOk, x.landcoverOk, isVoid(x.dteInsert) ? null : new Date(x.dteInsert), x.usrInsert, x.rowVersion, x.parcelsIssuesId, x.cultId, x.cotyId);
            deserializedEntities[key] = ret;
            ret.parcelsIssuesId = (x.parcelsIssuesId !== undefined && x.parcelsIssuesId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.parcelsIssuesId.$entityName].fromJSONPartial(x.parcelsIssuesId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.cultId = (x.cultId !== undefined && x.cultId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cultId.$entityName].fromJSONPartial(x.cultId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.cotyId = (x.cotyId !== undefined && x.cotyId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cotyId.$entityName].fromJSONPartial(x.cotyId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        FmisDecisionBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        FmisDecisionBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "FmisDecision:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "FmisDecisionBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "FmisDecision:"+x.fmisDecisionsId;
                    var cachedCopy = <FmisDecision>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = FmisDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = FmisDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.FmisDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        FmisDecisionBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.FmisDecision.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        FmisDecisionBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/FmisDecision/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.FmisDecision.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        FmisDecisionBase.prototype.toJSON = function () {
            var ret = {};
            ret.fmisDecisionsId = this.fmisDecisionsId;
            ret.cropOk = this.cropOk;
            ret.landcoverOk = this.landcoverOk;
            ret.dteInsert = this.dteInsert;
            ret.usrInsert = this.usrInsert;
            ret.rowVersion = this.rowVersion;
            ret.parcelsIssuesId =
                (this.parcelsIssuesId !== undefined && this.parcelsIssuesId !== null) ?
                    { parcelsIssuesId: this.parcelsIssuesId.parcelsIssuesId } :
                    (this.parcelsIssuesId === undefined ? undefined : null);
            ret.cultId =
                (this.cultId !== undefined && this.cultId !== null) ?
                    { cultId: this.cultId.cultId } :
                    (this.cultId === undefined ? undefined : null);
            ret.cotyId =
                (this.cotyId !== undefined && this.cotyId !== null) ?
                    { cotyId: this.cotyId.cotyId } :
                    (this.cotyId === undefined ? undefined : null);
            return ret;
        };
        FmisDecisionBase.prototype.updateInstance = function (other) {
            var self = this;
            self.fmisDecisionsId = other.fmisDecisionsId;
            self.cropOk = other.cropOk;
            self.landcoverOk = other.landcoverOk;
            self.dteInsert = other.dteInsert;
            self.usrInsert = other.usrInsert;
            self.rowVersion = other.rowVersion;
            self.parcelsIssuesId = other.parcelsIssuesId;
            self.cultId = other.cultId;
            self.cotyId = other.cotyId;
        };
        FmisDecisionBase.Create = function () {
            return new Entities.FmisDecision(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        FmisDecisionBase.CreateById = function (fmisDecisionsId) {
            var ret = Entities.FmisDecision.Create();
            ret.fmisDecisionsId = fmisDecisionsId;
            return ret;
        };
        FmisDecisionBase.findById_unecrypted = function (fmisDecisionsId, $scope, $http, errFunc) {
            if (Entities.FmisDecision.cashedEntities[fmisDecisionsId.toString()] !== undefined)
                return Entities.FmisDecision.cashedEntities[fmisDecisionsId.toString()];
            var wsPath = "FmisDecision/findByFmisDecisionsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fmisDecisionsId'] = fmisDecisionsId;
            var ret = Entities.FmisDecision.Create();
            Entities.FmisDecision.cashedEntities[fmisDecisionsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.FmisDecision.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + fmisDecisionsId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        FmisDecisionBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        FmisDecisionBase.cashedEntities = {};
        return FmisDecisionBase;
    })(NpTypes.BaseEntity);
    Entities.FmisDecisionBase = FmisDecisionBase;
    NpTypes.BaseEntity.entitiesFactory['FmisDecision'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.FmisDecision.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.FmisDecision.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=FmisDecisionBase.js.map