/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/Activity.ts" />
/// <reference path="../Entities/ParcActivity.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcActivityBase = (function (_super) {
        __extends(ParcActivityBase, _super);
        function ParcActivityBase(paacId, code, dateAct, rowVersion, decisionLight, pclaId, actiId) {
            _super.call(this);
            var self = this;
            this._paacId = new NpTypes.UIStringModel(paacId, this);
            this._code = new NpTypes.UIStringModel(code, this);
            this._dateAct = new NpTypes.UIDateModel(dateAct, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._decisionLight = new NpTypes.UINumberModel(decisionLight, this);
            this._pclaId = new NpTypes.UIManyToOneModel(pclaId, this);
            this._actiId = new NpTypes.UIManyToOneModel(actiId, this);
            self.postConstruct();
        }
        ParcActivityBase.prototype.getKey = function () {
            return this.paacId;
        };
        ParcActivityBase.prototype.getKeyName = function () {
            return "paacId";
        };
        ParcActivityBase.prototype.getEntityName = function () {
            return 'ParcActivity';
        };
        ParcActivityBase.prototype.fromJSON = function (data) {
            return Entities.ParcActivity.fromJSONComplete(data);
        };
        Object.defineProperty(ParcActivityBase.prototype, "paacId", {
            get: function () {
                return this._paacId.value;
            },
            set: function (paacId) {
                var self = this;
                self._paacId.value = paacId;
                //    self.paacId_listeners.forEach(cb => { cb(<ParcActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcActivityBase.prototype, "code", {
            get: function () {
                return this._code.value;
            },
            set: function (code) {
                var self = this;
                self._code.value = code;
                //    self.code_listeners.forEach(cb => { cb(<ParcActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcActivityBase.prototype, "dateAct", {
            get: function () {
                return this._dateAct.value;
            },
            set: function (dateAct) {
                var self = this;
                self._dateAct.value = dateAct;
                //    self.dateAct_listeners.forEach(cb => { cb(<ParcActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcActivityBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<ParcActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcActivityBase.prototype, "decisionLight", {
            get: function () {
                return this._decisionLight.value;
            },
            set: function (decisionLight) {
                var self = this;
                self._decisionLight.value = decisionLight;
                //    self.decisionLight_listeners.forEach(cb => { cb(<ParcActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcActivityBase.prototype, "pclaId", {
            get: function () {
                return this._pclaId.value;
            },
            set: function (pclaId) {
                var self = this;
                self._pclaId.value = pclaId;
                //    self.pclaId_listeners.forEach(cb => { cb(<ParcActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcActivityBase.prototype, "actiId", {
            get: function () {
                return this._actiId.value;
            },
            set: function (actiId) {
                var self = this;
                self._actiId.value = actiId;
                //    self.actiId_listeners.forEach(cb => { cb(<ParcActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public actiId_listeners: Array<(c:ParcActivity) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.paacId_listeners.splice(0);
            self.code_listeners.splice(0);
            self.dateAct_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.decisionLight_listeners.splice(0);
            self.pclaId_listeners.splice(0);
            self.actiId_listeners.splice(0);
        }
        */
        ParcActivityBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "ParcActivity:" + x.paacId;
            var ret = new Entities.ParcActivity(x.paacId, x.code, isVoid(x.dateAct) ? null : new Date(x.dateAct), x.rowVersion, x.decisionLight, x.pclaId, x.actiId);
            deserializedEntities[key] = ret;
            ret.pclaId = (x.pclaId !== undefined && x.pclaId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.pclaId.$entityName].fromJSONPartial(x.pclaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.actiId = (x.actiId !== undefined && x.actiId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.actiId.$entityName].fromJSONPartial(x.actiId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        ParcActivityBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ParcActivityBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcActivity:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcActivityBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcActivity:"+x.paacId;
                    var cachedCopy = <ParcActivity>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcActivity.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcActivity.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.ParcActivity.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ParcActivityBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.ParcActivity.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ParcActivityBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/ParcActivity/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.ParcActivity.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ParcActivityBase.prototype.toJSON = function () {
            var ret = {};
            ret.paacId = this.paacId;
            ret.code = this.code;
            ret.dateAct = this.dateAct;
            ret.rowVersion = this.rowVersion;
            ret.decisionLight = this.decisionLight;
            ret.pclaId =
                (this.pclaId !== undefined && this.pclaId !== null) ?
                    { pclaId: this.pclaId.pclaId } :
                    (this.pclaId === undefined ? undefined : null);
            ret.actiId =
                (this.actiId !== undefined && this.actiId !== null) ?
                    { actiId: this.actiId.actiId } :
                    (this.actiId === undefined ? undefined : null);
            return ret;
        };
        ParcActivityBase.prototype.updateInstance = function (other) {
            var self = this;
            self.paacId = other.paacId;
            self.code = other.code;
            self.dateAct = other.dateAct;
            self.rowVersion = other.rowVersion;
            self.decisionLight = other.decisionLight;
            self.pclaId = other.pclaId;
            self.actiId = other.actiId;
        };
        ParcActivityBase.Create = function () {
            return new Entities.ParcActivity(undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        ParcActivityBase.CreateById = function (paacId) {
            var ret = Entities.ParcActivity.Create();
            ret.paacId = paacId;
            return ret;
        };
        ParcActivityBase.findById_unecrypted = function (paacId, $scope, $http, errFunc) {
            if (Entities.ParcActivity.cashedEntities[paacId.toString()] !== undefined)
                return Entities.ParcActivity.cashedEntities[paacId.toString()];
            var wsPath = "ParcActivity/findByPaacId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['paacId'] = paacId;
            var ret = Entities.ParcActivity.Create();
            Entities.ParcActivity.cashedEntities[paacId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.ParcActivity.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + paacId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ParcActivityBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        ParcActivityBase.cashedEntities = {};
        return ParcActivityBase;
    })(NpTypes.BaseEntity);
    Entities.ParcActivityBase = ParcActivityBase;
    NpTypes.BaseEntity.entitiesFactory['ParcActivity'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.ParcActivity.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.ParcActivity.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcActivityBase.js.map