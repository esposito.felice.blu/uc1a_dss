/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/EcCult.ts" />
/// <reference path="../Entities/FmisDecision.ts" />
/// <reference path="../Entities/GpDecision.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ExcelFile.ts" />
/// <reference path="../Entities/CoverType.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var CoverTypeBase = (function (_super) {
        __extends(CoverTypeBase, _super);
        function CoverTypeBase(cotyId, code, name, rowVersion, exfiId) {
            _super.call(this);
            var self = this;
            this._cotyId = new NpTypes.UIStringModel(cotyId, this);
            this._code = new NpTypes.UINumberModel(code, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._exfiId = new NpTypes.UIManyToOneModel(exfiId, this);
            self.postConstruct();
        }
        CoverTypeBase.prototype.getKey = function () {
            return this.cotyId;
        };
        CoverTypeBase.prototype.getKeyName = function () {
            return "cotyId";
        };
        CoverTypeBase.prototype.getEntityName = function () {
            return 'CoverType';
        };
        CoverTypeBase.prototype.fromJSON = function (data) {
            return Entities.CoverType.fromJSONComplete(data);
        };
        Object.defineProperty(CoverTypeBase.prototype, "cotyId", {
            get: function () {
                return this._cotyId.value;
            },
            set: function (cotyId) {
                var self = this;
                self._cotyId.value = cotyId;
                //    self.cotyId_listeners.forEach(cb => { cb(<CoverType>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoverTypeBase.prototype, "code", {
            get: function () {
                return this._code.value;
            },
            set: function (code) {
                var self = this;
                self._code.value = code;
                //    self.code_listeners.forEach(cb => { cb(<CoverType>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoverTypeBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (name) {
                var self = this;
                self._name.value = name;
                //    self.name_listeners.forEach(cb => { cb(<CoverType>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoverTypeBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<CoverType>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoverTypeBase.prototype, "exfiId", {
            get: function () {
                return this._exfiId.value;
            },
            set: function (exfiId) {
                var self = this;
                self._exfiId.value = exfiId;
                //    self.exfiId_listeners.forEach(cb => { cb(<CoverType>self); });
            },
            enumerable: true,
            configurable: true
        });
        CoverTypeBase.prototype.cultivationCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.CoverType.cultivationCollection)) {
                if (self instanceof Entities.CoverType) {
                    Entities.CoverType.cultivationCollection(self, func);
                }
            }
        };
        CoverTypeBase.prototype.ecCultCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.CoverType.ecCultCollection)) {
                if (self instanceof Entities.CoverType) {
                    Entities.CoverType.ecCultCollection(self, func);
                }
            }
        };
        CoverTypeBase.prototype.fmisDecisionCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.CoverType.fmisDecisionCollection)) {
                if (self instanceof Entities.CoverType) {
                    Entities.CoverType.fmisDecisionCollection(self, func);
                }
            }
        };
        CoverTypeBase.prototype.gpDecisionCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.CoverType.gpDecisionCollection)) {
                if (self instanceof Entities.CoverType) {
                    Entities.CoverType.gpDecisionCollection(self, func);
                }
            }
        };
        CoverTypeBase.prototype.ecCultCopyCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.CoverType.ecCultCopyCollection)) {
                if (self instanceof Entities.CoverType) {
                    Entities.CoverType.ecCultCopyCollection(self, func);
                }
            }
        };
        CoverTypeBase.prototype.parcelClascoty_id_declCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.CoverType.parcelClascoty_id_declCollection)) {
                if (self instanceof Entities.CoverType) {
                    Entities.CoverType.parcelClascoty_id_declCollection(self, func);
                }
            }
        };
        CoverTypeBase.prototype.parcelClascoty_id_predCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.CoverType.parcelClascoty_id_predCollection)) {
                if (self instanceof Entities.CoverType) {
                    Entities.CoverType.parcelClascoty_id_predCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.cotyId_listeners.splice(0);
            self.code_listeners.splice(0);
            self.name_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.exfiId_listeners.splice(0);
        }
        */
        CoverTypeBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "CoverType:" + x.cotyId;
            var ret = new Entities.CoverType(x.cotyId, x.code, x.name, x.rowVersion, x.exfiId);
            deserializedEntities[key] = ret;
            ret.exfiId = (x.exfiId !== undefined && x.exfiId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.exfiId.$entityName].fromJSONPartial(x.exfiId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        CoverTypeBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        CoverTypeBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "CoverType:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "CoverTypeBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "CoverType:"+x.cotyId;
                    var cachedCopy = <CoverType>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = CoverType.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = CoverType.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.CoverType.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        CoverTypeBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.CoverType.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        CoverTypeBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/CoverType/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.CoverType.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        CoverTypeBase.prototype.toJSON = function () {
            var ret = {};
            ret.cotyId = this.cotyId;
            ret.code = this.code;
            ret.name = this.name;
            ret.rowVersion = this.rowVersion;
            ret.exfiId =
                (this.exfiId !== undefined && this.exfiId !== null) ?
                    { id: this.exfiId.id } :
                    (this.exfiId === undefined ? undefined : null);
            return ret;
        };
        CoverTypeBase.prototype.updateInstance = function (other) {
            var self = this;
            self.cotyId = other.cotyId;
            self.code = other.code;
            self.name = other.name;
            self.rowVersion = other.rowVersion;
            self.exfiId = other.exfiId;
        };
        CoverTypeBase.Create = function () {
            return new Entities.CoverType(undefined, undefined, undefined, undefined, undefined);
        };
        CoverTypeBase.CreateById = function (cotyId) {
            var ret = Entities.CoverType.Create();
            ret.cotyId = cotyId;
            return ret;
        };
        CoverTypeBase.findById_unecrypted = function (cotyId, $scope, $http, errFunc) {
            if (Entities.CoverType.cashedEntities[cotyId.toString()] !== undefined)
                return Entities.CoverType.cashedEntities[cotyId.toString()];
            var wsPath = "CoverType/findByCotyId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['cotyId'] = cotyId;
            var ret = Entities.CoverType.Create();
            Entities.CoverType.cashedEntities[cotyId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.CoverType.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + cotyId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        CoverTypeBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public exfiId_listeners: Array<(c:CoverType) => void> = [];
        CoverTypeBase.cultivationCollection = null; //(coverType, list) => { };
        CoverTypeBase.ecCultCollection = null; //(coverType, list) => { };
        CoverTypeBase.fmisDecisionCollection = null; //(coverType, list) => { };
        CoverTypeBase.gpDecisionCollection = null; //(coverType, list) => { };
        CoverTypeBase.ecCultCopyCollection = null; //(coverType, list) => { };
        CoverTypeBase.parcelClascoty_id_declCollection = null; //(coverType, list) => { };
        CoverTypeBase.parcelClascoty_id_predCollection = null; //(coverType, list) => { };
        CoverTypeBase.cashedEntities = {};
        return CoverTypeBase;
    })(NpTypes.BaseEntity);
    Entities.CoverTypeBase = CoverTypeBase;
    NpTypes.BaseEntity.entitiesFactory['CoverType'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.CoverType.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.CoverType.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=CoverTypeBase.js.map