/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/AgrisnapUsersProducers.ts" />
/// <reference path="../Entities/GpRequests.ts" />
/// <reference path="../Entities/AgrisnapUsers.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var AgrisnapUsersBase = (function (_super) {
        __extends(AgrisnapUsersBase, _super);
        function AgrisnapUsersBase(agrisnapName, rowVersion, agrisnapUsersId, agrisnapUid) {
            _super.call(this);
            var self = this;
            this._agrisnapName = new NpTypes.UIStringModel(agrisnapName, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._agrisnapUsersId = new NpTypes.UIStringModel(agrisnapUsersId, this);
            this._agrisnapUid = new NpTypes.UIStringModel(agrisnapUid, this);
            self.postConstruct();
        }
        AgrisnapUsersBase.prototype.getKey = function () {
            return this.agrisnapUsersId;
        };
        AgrisnapUsersBase.prototype.getKeyName = function () {
            return "agrisnapUsersId";
        };
        AgrisnapUsersBase.prototype.getEntityName = function () {
            return 'AgrisnapUsers';
        };
        AgrisnapUsersBase.prototype.fromJSON = function (data) {
            return Entities.AgrisnapUsers.fromJSONComplete(data);
        };
        Object.defineProperty(AgrisnapUsersBase.prototype, "agrisnapName", {
            get: function () {
                return this._agrisnapName.value;
            },
            set: function (agrisnapName) {
                var self = this;
                self._agrisnapName.value = agrisnapName;
                //    self.agrisnapName_listeners.forEach(cb => { cb(<AgrisnapUsers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgrisnapUsersBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<AgrisnapUsers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgrisnapUsersBase.prototype, "agrisnapUsersId", {
            get: function () {
                return this._agrisnapUsersId.value;
            },
            set: function (agrisnapUsersId) {
                var self = this;
                self._agrisnapUsersId.value = agrisnapUsersId;
                //    self.agrisnapUsersId_listeners.forEach(cb => { cb(<AgrisnapUsers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgrisnapUsersBase.prototype, "agrisnapUid", {
            get: function () {
                return this._agrisnapUid.value;
            },
            set: function (agrisnapUid) {
                var self = this;
                self._agrisnapUid.value = agrisnapUid;
                //    self.agrisnapUid_listeners.forEach(cb => { cb(<AgrisnapUsers>self); });
            },
            enumerable: true,
            configurable: true
        });
        AgrisnapUsersBase.prototype.agrisnapUsersProducersCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.AgrisnapUsers.agrisnapUsersProducersCollection)) {
                if (self instanceof Entities.AgrisnapUsers) {
                    Entities.AgrisnapUsers.agrisnapUsersProducersCollection(self, func);
                }
            }
        };
        AgrisnapUsersBase.prototype.gpRequestsCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.AgrisnapUsers.gpRequestsCollection)) {
                if (self instanceof Entities.AgrisnapUsers) {
                    Entities.AgrisnapUsers.gpRequestsCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.agrisnapName_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.agrisnapUsersId_listeners.splice(0);
            self.agrisnapUid_listeners.splice(0);
        }
        */
        AgrisnapUsersBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "AgrisnapUsers:" + x.agrisnapUsersId;
            var ret = new Entities.AgrisnapUsers(x.agrisnapName, x.rowVersion, x.agrisnapUsersId, x.agrisnapUid);
            deserializedEntities[key] = ret;
            return ret;
        };
        AgrisnapUsersBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        AgrisnapUsersBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "AgrisnapUsers:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "AgrisnapUsersBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "AgrisnapUsers:"+x.agrisnapUsersId;
                    var cachedCopy = <AgrisnapUsers>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = AgrisnapUsers.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = AgrisnapUsers.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.AgrisnapUsers.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        AgrisnapUsersBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.AgrisnapUsers.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        AgrisnapUsersBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/AgrisnapUsers/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.AgrisnapUsers.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        AgrisnapUsersBase.prototype.toJSON = function () {
            var ret = {};
            ret.agrisnapName = this.agrisnapName;
            ret.rowVersion = this.rowVersion;
            ret.agrisnapUsersId = this.agrisnapUsersId;
            ret.agrisnapUid = this.agrisnapUid;
            return ret;
        };
        AgrisnapUsersBase.prototype.updateInstance = function (other) {
            var self = this;
            self.agrisnapName = other.agrisnapName;
            self.rowVersion = other.rowVersion;
            self.agrisnapUsersId = other.agrisnapUsersId;
            self.agrisnapUid = other.agrisnapUid;
        };
        AgrisnapUsersBase.Create = function () {
            return new Entities.AgrisnapUsers(undefined, undefined, undefined, undefined);
        };
        AgrisnapUsersBase.CreateById = function (agrisnapUsersId) {
            var ret = Entities.AgrisnapUsers.Create();
            ret.agrisnapUsersId = agrisnapUsersId;
            return ret;
        };
        AgrisnapUsersBase.findById_unecrypted = function (agrisnapUsersId, $scope, $http, errFunc) {
            if (Entities.AgrisnapUsers.cashedEntities[agrisnapUsersId.toString()] !== undefined)
                return Entities.AgrisnapUsers.cashedEntities[agrisnapUsersId.toString()];
            var wsPath = "AgrisnapUsers/findByAgrisnapUsersId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['agrisnapUsersId'] = agrisnapUsersId;
            var ret = Entities.AgrisnapUsers.Create();
            Entities.AgrisnapUsers.cashedEntities[agrisnapUsersId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.AgrisnapUsers.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + agrisnapUsersId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        AgrisnapUsersBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public agrisnapUid_listeners: Array<(c:AgrisnapUsers) => void> = [];
        AgrisnapUsersBase.agrisnapUsersProducersCollection = null; //(agrisnapUsers, list) => { };
        AgrisnapUsersBase.gpRequestsCollection = null; //(agrisnapUsers, list) => { };
        AgrisnapUsersBase.cashedEntities = {};
        return AgrisnapUsersBase;
    })(NpTypes.BaseEntity);
    Entities.AgrisnapUsersBase = AgrisnapUsersBase;
    NpTypes.BaseEntity.entitiesFactory['AgrisnapUsers'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.AgrisnapUsers.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.AgrisnapUsers.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=AgrisnapUsersBase.js.map