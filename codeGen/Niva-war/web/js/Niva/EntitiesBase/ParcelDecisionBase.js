/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ParcelDecision.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcelDecisionBase = (function (_super) {
        __extends(ParcelDecisionBase, _super);
        function ParcelDecisionBase(padeId, decisionLight, rowVersion, demaId, pclaId) {
            _super.call(this);
            var self = this;
            this._padeId = new NpTypes.UIStringModel(padeId, this);
            this._decisionLight = new NpTypes.UINumberModel(decisionLight, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._demaId = new NpTypes.UIManyToOneModel(demaId, this);
            this._pclaId = new NpTypes.UIManyToOneModel(pclaId, this);
            self.postConstruct();
        }
        ParcelDecisionBase.prototype.getKey = function () {
            return this.padeId;
        };
        ParcelDecisionBase.prototype.getKeyName = function () {
            return "padeId";
        };
        ParcelDecisionBase.prototype.getEntityName = function () {
            return 'ParcelDecision';
        };
        ParcelDecisionBase.prototype.fromJSON = function (data) {
            return Entities.ParcelDecision.fromJSONComplete(data);
        };
        Object.defineProperty(ParcelDecisionBase.prototype, "padeId", {
            get: function () {
                return this._padeId.value;
            },
            set: function (padeId) {
                var self = this;
                self._padeId.value = padeId;
                //    self.padeId_listeners.forEach(cb => { cb(<ParcelDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelDecisionBase.prototype, "decisionLight", {
            get: function () {
                return this._decisionLight.value;
            },
            set: function (decisionLight) {
                var self = this;
                self._decisionLight.value = decisionLight;
                //    self.decisionLight_listeners.forEach(cb => { cb(<ParcelDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelDecisionBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<ParcelDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelDecisionBase.prototype, "demaId", {
            get: function () {
                return this._demaId.value;
            },
            set: function (demaId) {
                var self = this;
                self._demaId.value = demaId;
                //    self.demaId_listeners.forEach(cb => { cb(<ParcelDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelDecisionBase.prototype, "pclaId", {
            get: function () {
                return this._pclaId.value;
            },
            set: function (pclaId) {
                var self = this;
                self._pclaId.value = pclaId;
                //    self.pclaId_listeners.forEach(cb => { cb(<ParcelDecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public pclaId_listeners: Array<(c:ParcelDecision) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.padeId_listeners.splice(0);
            self.decisionLight_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.demaId_listeners.splice(0);
            self.pclaId_listeners.splice(0);
        }
        */
        ParcelDecisionBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "ParcelDecision:" + x.padeId;
            var ret = new Entities.ParcelDecision(x.padeId, x.decisionLight, x.rowVersion, x.demaId, x.pclaId);
            deserializedEntities[key] = ret;
            ret.demaId = (x.demaId !== undefined && x.demaId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.demaId.$entityName].fromJSONPartial(x.demaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.pclaId = (x.pclaId !== undefined && x.pclaId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.pclaId.$entityName].fromJSONPartial(x.pclaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        ParcelDecisionBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ParcelDecisionBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcelDecision:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcelDecisionBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcelDecision:"+x.padeId;
                    var cachedCopy = <ParcelDecision>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcelDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcelDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.ParcelDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ParcelDecisionBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.ParcelDecision.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ParcelDecisionBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/ParcelDecision/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.ParcelDecision.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ParcelDecisionBase.prototype.toJSON = function () {
            var ret = {};
            ret.padeId = this.padeId;
            ret.decisionLight = this.decisionLight;
            ret.rowVersion = this.rowVersion;
            ret.demaId =
                (this.demaId !== undefined && this.demaId !== null) ?
                    { demaId: this.demaId.demaId } :
                    (this.demaId === undefined ? undefined : null);
            ret.pclaId =
                (this.pclaId !== undefined && this.pclaId !== null) ?
                    { pclaId: this.pclaId.pclaId } :
                    (this.pclaId === undefined ? undefined : null);
            return ret;
        };
        ParcelDecisionBase.prototype.updateInstance = function (other) {
            var self = this;
            self.padeId = other.padeId;
            self.decisionLight = other.decisionLight;
            self.rowVersion = other.rowVersion;
            self.demaId = other.demaId;
            self.pclaId = other.pclaId;
        };
        ParcelDecisionBase.Create = function () {
            return new Entities.ParcelDecision(undefined, undefined, undefined, undefined, undefined);
        };
        ParcelDecisionBase.CreateById = function (padeId) {
            var ret = Entities.ParcelDecision.Create();
            ret.padeId = padeId;
            return ret;
        };
        ParcelDecisionBase.findById_unecrypted = function (padeId, $scope, $http, errFunc) {
            if (Entities.ParcelDecision.cashedEntities[padeId.toString()] !== undefined)
                return Entities.ParcelDecision.cashedEntities[padeId.toString()];
            var wsPath = "ParcelDecision/findByPadeId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['padeId'] = padeId;
            var ret = Entities.ParcelDecision.Create();
            Entities.ParcelDecision.cashedEntities[padeId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.ParcelDecision.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + padeId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ParcelDecisionBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        ParcelDecisionBase.cashedEntities = {};
        return ParcelDecisionBase;
    })(NpTypes.BaseEntity);
    Entities.ParcelDecisionBase = ParcelDecisionBase;
    NpTypes.BaseEntity.entitiesFactory['ParcelDecision'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.ParcelDecision.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.ParcelDecision.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcelDecisionBase.js.map