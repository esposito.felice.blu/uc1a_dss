/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/FileTemplate.ts" />
/// <reference path="../Entities/PredefCol.ts" />
/// <reference path="../Entities/TemplateColumn.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var TemplateColumnBase = (function (_super) {
        __extends(TemplateColumnBase, _super);
        function TemplateColumnBase(tecoId, clfierName, rowVersion, fiteId, prcoId) {
            _super.call(this);
            var self = this;
            this._tecoId = new NpTypes.UIStringModel(tecoId, this);
            this._clfierName = new NpTypes.UIStringModel(clfierName, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._fiteId = new NpTypes.UIManyToOneModel(fiteId, this);
            this._prcoId = new NpTypes.UIManyToOneModel(prcoId, this);
            self.postConstruct();
        }
        TemplateColumnBase.prototype.getKey = function () {
            return this.tecoId;
        };
        TemplateColumnBase.prototype.getKeyName = function () {
            return "tecoId";
        };
        TemplateColumnBase.prototype.getEntityName = function () {
            return 'TemplateColumn';
        };
        TemplateColumnBase.prototype.fromJSON = function (data) {
            return Entities.TemplateColumn.fromJSONComplete(data);
        };
        Object.defineProperty(TemplateColumnBase.prototype, "tecoId", {
            get: function () {
                return this._tecoId.value;
            },
            set: function (tecoId) {
                var self = this;
                self._tecoId.value = tecoId;
                //    self.tecoId_listeners.forEach(cb => { cb(<TemplateColumn>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TemplateColumnBase.prototype, "clfierName", {
            get: function () {
                return this._clfierName.value;
            },
            set: function (clfierName) {
                var self = this;
                self._clfierName.value = clfierName;
                //    self.clfierName_listeners.forEach(cb => { cb(<TemplateColumn>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TemplateColumnBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<TemplateColumn>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TemplateColumnBase.prototype, "fiteId", {
            get: function () {
                return this._fiteId.value;
            },
            set: function (fiteId) {
                var self = this;
                self._fiteId.value = fiteId;
                //    self.fiteId_listeners.forEach(cb => { cb(<TemplateColumn>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TemplateColumnBase.prototype, "prcoId", {
            get: function () {
                return this._prcoId.value;
            },
            set: function (prcoId) {
                var self = this;
                self._prcoId.value = prcoId;
                //    self.prcoId_listeners.forEach(cb => { cb(<TemplateColumn>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public prcoId_listeners: Array<(c:TemplateColumn) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.tecoId_listeners.splice(0);
            self.clfierName_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.fiteId_listeners.splice(0);
            self.prcoId_listeners.splice(0);
        }
        */
        TemplateColumnBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "TemplateColumn:" + x.tecoId;
            var ret = new Entities.TemplateColumn(x.tecoId, x.clfierName, x.rowVersion, x.fiteId, x.prcoId);
            deserializedEntities[key] = ret;
            ret.fiteId = (x.fiteId !== undefined && x.fiteId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.fiteId.$entityName].fromJSONPartial(x.fiteId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.prcoId = (x.prcoId !== undefined && x.prcoId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.prcoId.$entityName].fromJSONPartial(x.prcoId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        TemplateColumnBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        TemplateColumnBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "TemplateColumn:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "TemplateColumnBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "TemplateColumn:"+x.tecoId;
                    var cachedCopy = <TemplateColumn>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = TemplateColumn.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = TemplateColumn.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.TemplateColumn.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        TemplateColumnBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.TemplateColumn.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        TemplateColumnBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/TemplateColumn/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.TemplateColumn.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        TemplateColumnBase.prototype.toJSON = function () {
            var ret = {};
            ret.tecoId = this.tecoId;
            ret.clfierName = this.clfierName;
            ret.rowVersion = this.rowVersion;
            ret.fiteId =
                (this.fiteId !== undefined && this.fiteId !== null) ?
                    { fiteId: this.fiteId.fiteId } :
                    (this.fiteId === undefined ? undefined : null);
            ret.prcoId =
                (this.prcoId !== undefined && this.prcoId !== null) ?
                    { prcoId: this.prcoId.prcoId } :
                    (this.prcoId === undefined ? undefined : null);
            return ret;
        };
        TemplateColumnBase.prototype.updateInstance = function (other) {
            var self = this;
            self.tecoId = other.tecoId;
            self.clfierName = other.clfierName;
            self.rowVersion = other.rowVersion;
            self.fiteId = other.fiteId;
            self.prcoId = other.prcoId;
        };
        TemplateColumnBase.Create = function () {
            return new Entities.TemplateColumn(undefined, undefined, undefined, undefined, undefined);
        };
        TemplateColumnBase.CreateById = function (tecoId) {
            var ret = Entities.TemplateColumn.Create();
            ret.tecoId = tecoId;
            return ret;
        };
        TemplateColumnBase.findById_unecrypted = function (tecoId, $scope, $http, errFunc) {
            if (Entities.TemplateColumn.cashedEntities[tecoId.toString()] !== undefined)
                return Entities.TemplateColumn.cashedEntities[tecoId.toString()];
            var wsPath = "TemplateColumn/findByTecoId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['tecoId'] = tecoId;
            var ret = Entities.TemplateColumn.Create();
            Entities.TemplateColumn.cashedEntities[tecoId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.TemplateColumn.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + tecoId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        TemplateColumnBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        TemplateColumnBase.cashedEntities = {};
        return TemplateColumnBase;
    })(NpTypes.BaseEntity);
    Entities.TemplateColumnBase = TemplateColumnBase;
    NpTypes.BaseEntity.entitiesFactory['TemplateColumn'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.TemplateColumn.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.TemplateColumn.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=TemplateColumnBase.js.map