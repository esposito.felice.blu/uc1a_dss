/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Agency.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var AgencyBase = (function (_super) {
        __extends(AgencyBase, _super);
        function AgencyBase(agenId, name, country, rowVersion, srid) {
            _super.call(this);
            var self = this;
            this._agenId = new NpTypes.UIStringModel(agenId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._country = new NpTypes.UIStringModel(country, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._srid = new NpTypes.UINumberModel(srid, this);
            self.postConstruct();
        }
        AgencyBase.prototype.getKey = function () {
            return this.agenId;
        };
        AgencyBase.prototype.getKeyName = function () {
            return "agenId";
        };
        AgencyBase.prototype.getEntityName = function () {
            return 'Agency';
        };
        AgencyBase.prototype.fromJSON = function (data) {
            return Entities.Agency.fromJSONComplete(data);
        };
        Object.defineProperty(AgencyBase.prototype, "agenId", {
            get: function () {
                return this._agenId.value;
            },
            set: function (agenId) {
                var self = this;
                self._agenId.value = agenId;
                //    self.agenId_listeners.forEach(cb => { cb(<Agency>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgencyBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (name) {
                var self = this;
                self._name.value = name;
                //    self.name_listeners.forEach(cb => { cb(<Agency>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgencyBase.prototype, "country", {
            get: function () {
                return this._country.value;
            },
            set: function (country) {
                var self = this;
                self._country.value = country;
                //    self.country_listeners.forEach(cb => { cb(<Agency>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgencyBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<Agency>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgencyBase.prototype, "srid", {
            get: function () {
                return this._srid.value;
            },
            set: function (srid) {
                var self = this;
                self._srid.value = srid;
                //    self.srid_listeners.forEach(cb => { cb(<Agency>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public srid_listeners: Array<(c:Agency) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.agenId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.country_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.srid_listeners.splice(0);
        }
        */
        AgencyBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "Agency:" + x.agenId;
            var ret = new Entities.Agency(x.agenId, x.name, x.country, x.rowVersion, x.srid);
            deserializedEntities[key] = ret;
            return ret;
        };
        AgencyBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        AgencyBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Agency:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "AgencyBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Agency:"+x.agenId;
                    var cachedCopy = <Agency>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Agency.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Agency.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.Agency.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        AgencyBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.Agency.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        AgencyBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/Agency/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.Agency.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        AgencyBase.prototype.toJSON = function () {
            var ret = {};
            ret.agenId = this.agenId;
            ret.name = this.name;
            ret.country = this.country;
            ret.rowVersion = this.rowVersion;
            ret.srid = this.srid;
            return ret;
        };
        AgencyBase.prototype.updateInstance = function (other) {
            var self = this;
            self.agenId = other.agenId;
            self.name = other.name;
            self.country = other.country;
            self.rowVersion = other.rowVersion;
            self.srid = other.srid;
        };
        AgencyBase.Create = function () {
            return new Entities.Agency(undefined, undefined, undefined, undefined, undefined);
        };
        AgencyBase.CreateById = function (agenId) {
            var ret = Entities.Agency.Create();
            ret.agenId = agenId;
            return ret;
        };
        AgencyBase.findById_unecrypted = function (agenId, $scope, $http, errFunc) {
            if (Entities.Agency.cashedEntities[agenId.toString()] !== undefined)
                return Entities.Agency.cashedEntities[agenId.toString()];
            var wsPath = "Agency/findByAgenId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['agenId'] = agenId;
            var ret = Entities.Agency.Create();
            Entities.Agency.cashedEntities[agenId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.Agency.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + agenId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        AgencyBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        AgencyBase.cashedEntities = {};
        return AgencyBase;
    })(NpTypes.BaseEntity);
    Entities.AgencyBase = AgencyBase;
    NpTypes.BaseEntity.entitiesFactory['Agency'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.Agency.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.Agency.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=AgencyBase.js.map