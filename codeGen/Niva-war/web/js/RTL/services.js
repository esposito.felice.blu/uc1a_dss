/// <reference path="../DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../DefinitelyTyped/jqueryui/jqueryui.d.ts" />
/// <reference path="../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="npTypes.ts" />
/// <reference path="sprintf.d.ts" />
var Services;
(function (Services) {
    var g_dialogCounter = 1;
    var RouteAuthorization = (function () {
        function RouteAuthorization() {
            this.authorizedUrls = {};
        }
        Object.defineProperty(RouteAuthorization.prototype, "requestDeferred", {
            get: function () {
                return this._requestDeferred;
            },
            set: function (d) {
                this._requestDeferred = d;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RouteAuthorization.prototype, "url", {
            get: function () {
                return this._url;
            },
            set: function (u) {
                this._url = u;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RouteAuthorization.prototype, "privilege", {
            get: function () {
                return this._privilege;
            },
            set: function (p) {
                this._privilege = p;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RouteAuthorization.prototype, "useCustomMethod", {
            get: function () {
                return this._useCustomMethod;
            },
            set: function (b) {
                this._useCustomMethod = b;
            },
            enumerable: true,
            configurable: true
        });
        RouteAuthorization.prototype.addToSucceeded = function (url, val) {
            if (isVoid(url))
                return;
            this.authorizedUrls[url] = val;
        };
        RouteAuthorization.prototype.getSucceededValue = function (url) {
            if (isVoid(url))
                return undefined;
            return this.authorizedUrls[url];
        };
        return RouteAuthorization;
    })();
    var LoginService = (function () {
        function LoginService($scope, $cookies, $http, $timeout, $q, Plato, NavigationService) {
            this.$scope = $scope;
            this.$cookies = $cookies;
            this.$http = $http;
            this.$timeout = $timeout;
            this.$q = $q;
            this.Plato = Plato;
            this.NavigationService = NavigationService;
            this.logoutOnUnload = true;
            this.isSsoCreator = false;
            this.ssoDeleted = false;
            this.routeAuthor = new RouteAuthorization();
            this.onSuccessFullLogin = function () { };
            this.sessionInitializedDeferred = this.$q.defer();
            this.sessionInitializedPromise = this.sessionInitializedDeferred.promise;
        }
        Object.defineProperty(LoginService.prototype, "sessionCookieKey", {
            get: function () { return this.$scope.globals.sessionCookieKey; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginService.prototype, "subsCodeCookieKey", {
            get: function () { return this.$scope.globals.subsCodeCookieKey; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginService.prototype, "ssoCookieKey", {
            get: function () { return this.$scope.globals.ssoCookieKey; },
            enumerable: true,
            configurable: true
        });
        LoginService.prototype.isLoggedIn = function () {
            return this.$cookies[this.sessionCookieKey] !== undefined;
        };
        LoginService.prototype.existsSso = function () {
            return this.$cookies[this.ssoCookieKey] !== undefined;
        };
        LoginService.prototype.authenticationPromise = function () {
            var self = this;
            var defer = self.$q.defer();
            if (self.isLoggedIn()) {
                defer.resolve(true);
            }
            else {
                if (self.existsSso()) {
                    if (self.loginSsoPromise !== undefined) {
                        self.loginSsoPromise.then(function (response) {
                            defer.resolve(true);
                        }, function (error) {
                            defer.reject('UnAuthorized Sso');
                            self.$timeout(function () { self.NavigationService.goToNotLoggedInPage(); }, 0);
                        });
                    }
                    else {
                        defer.reject('loginSsoPromise has not been created because of reload');
                    }
                }
                else {
                    defer.reject('Not Authenticated User');
                    self.$timeout(function () { self.NavigationService.goToNotLoggedInPage(); }, 0);
                }
            }
            return self.sessionInitializedPromise.
                then(function () { return defer.promise; });
        };
        LoginService.prototype.requestAuthorizationPromise = function (url, privilege, onlyOnce, useCustomMethod) {
            var self = this;
            var isAuthorized = self.NavigationService.getIsAuthorizedByURL(url);
            if (isAuthorized) {
                return self.authenticationPromise();
            }
            if (!isVoid(self.routeAuthor.requestDeferred)) {
                self.routeAuthor.requestDeferred.reject("concurrent request");
                self.routeAuthor.requestDeferred = null;
            }
            self.routeAuthor.requestDeferred = self.$q.defer();
            self.routeAuthor.url = url;
            self.routeAuthor.privilege = privilege;
            self.routeAuthor.useCustomMethod = useCustomMethod;
            var authorizedData = self.routeAuthor.getSucceededValue(url);
            if (onlyOnce && !isVoid(authorizedData)) {
                self.requestAuthorization(authorizedData.password, authorizedData.username, authorizedData.userSubsCode);
            }
            else {
                var dialogOptions = new NpTypes.DialogOptions;
                dialogOptions.width = '23em';
                dialogOptions.className = "RequestAuthorizationClass";
                dialogOptions.showCloseButton = false;
                self.Plato.showDialog(self.$scope, self.$scope.globals.requestAuthorizationOptions.getFormTitle(self.routeAuthor.url), '/' + self.$scope.globals.appName + '/partials/requestAuthorization.html?rev=' + self.$scope.globals.version, dialogOptions);
            }
            return self.authenticationPromise().
                then(function () { return self.routeAuthor.requestDeferred.promise; });
        };
        LoginService.prototype.requestAuthorization = function (password, username, userSubsCode) {
            var self = this;
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout, true);
            var wsUserAuthorStatusPath = "Login/getUserAuthorizationStatus";
            var wsUserAuthorStatusStartTs = (new Date).getTime();
            self.$http.post("/" + self.$scope.globals.appName + "/rest/" + wsUserAuthorStatusPath, { privilege: self.routeAuthor.privilege, password: password, username: username, userSubsCode: userSubsCode }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsUserAuthorStatusPath, wsUserAuthorStatusStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                self.NavigationService.setIsAuthorizedByURL(self.routeAuthor.url, true);
                self.routeAuthor.addToSucceeded(self.routeAuthor.url, {
                    userSubsCode: userSubsCode,
                    username: username,
                    password: password,
                    responseData: response.data });
                self.routeAuthor.requestDeferred.resolve(true);
                self.routeAuthor.requestDeferred = null;
            }).error(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsUserAuthorStatusPath, wsUserAuthorStatusStartTs, (new Date).getTime(), false, status);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                self.routeAuthor.requestDeferred.reject(response);
                self.routeAuthor.requestDeferred = null;
                var myErrMsg = self.$scope.globals.requestAuthorizationOptions.getDisplayedErrMsg(self.routeAuthor.url, response);
                var errMsg = self.$scope.globals.getDynamicMessage(myErrMsg);
                if (status === 0 && (isVoid(errMsg) || errMsg === "")) {
                    errMsg = "Timeout occured waiting for server response.";
                }
                else {
                    self.routeAuthor.addToSucceeded(self.routeAuthor.url, null);
                }
                messageBox(self.$scope, self.Plato, self.$scope.globals.requestAuthorizationOptions.getFormTitle(self.routeAuthor.url), errMsg, IconKind.ERROR, [
                    new Tuple2("ΟΚ", function () { })
                ], 0, 0, "40em");
                self.NavigationService.navigateBackward();
            });
        };
        LoginService.prototype.cancelRequestAuthorization = function () {
            var self = this;
            self.routeAuthor.requestDeferred.reject("canceled by User");
            self.routeAuthor.requestDeferred = null;
            self.NavigationService.navigateBackward();
        };
        LoginService.prototype.addPasswordExpirarionAlertInBannerMsg = function (expirationDays) {
            var self = this;
            var msg = expirationDays === 1 ?
                self.$scope.globals.getDynamicMessage("PasswordExpirarionAlertInBannerMsg_OneDay") :
                self.$scope.globals.getDynamicMessage("PasswordExpirarionAlertInBannerMsg(" + expirationDays + ")");
            self.NavigationService.modelNavigation.banner = msg;
            self.NavigationService.modelNavigation.bannerClass = 'bannerMainWarning';
        };
        LoginService.prototype.getLogoProvider = function () {
            // Requires session cookie
            var self = this;
            var appName = self.$scope.globals.appName;
            var wsPath = "MainService/getLogoProvider";
            var wsStartTs = (new Date).getTime();
            self.$http.post("/" + appName + "/rest/" + wsPath + "?", {}, { timeout: self.$scope.globals.timeoutInMS, cache: false }).success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                self.NavigationService.modelNavigation.logoURL = response.data;
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        LoginService.prototype.afterLoginInit = function () {
            var self = this;
            if (!self.isLoggedIn())
                return;
            // getLogoProvider
            self.getLogoProvider();
            var wsMenuPath = "Menu/getMenu";
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsMenuStartTs = (new Date).getTime();
            self.$http.post("/" + self.$scope.globals.appName + "/rest/" + wsMenuPath, {}, { timeout: self.$scope.globals.timeoutInMS, cache: false }).success(function (response, status, header, config) {
                self.$scope.globals.appWsResponeInfoEnabled = response.isWsResponseInfoEnabled;
                self.$scope.globals.addWsResponseInfo(wsMenuPath, wsMenuStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                self.onSuccessFullLogin();
                self.$scope.globals.addPrivileges(response.privileges);
                self.$scope.globals.globalUserId = response.userId;
                self.$scope.globals.globalSubsId = response.subsId;
                self.$scope.globals.globalSubsDescription = response.subsDescription;
                self.$scope.globals.globalSubsCode = response.subsCode;
                self.$scope.globals.globalUserEmail = response.usrEmail;
                self.$scope.globals.globalUserActiveEmail = response.usrActiveEmail;
                self.$scope.globals.globalUserLoginName = response.userLoginName;
                self.$scope.globals.globalUserVat = response.userVat;
                self.$scope.globals.globalSubsSecClasses = response.subSecClasses;
                self.$scope.globals.sessionClientIp = response.sessionClientIp;
                self.$scope.globals.initializeServerSideGlobals(response);
                self.$scope.globals.initialize(self.$scope, self.$timeout, self.$http, self.Plato);
                self.NavigationService.createUserMenu(response.result);
                self.sessionInitializedDeferred.resolve(true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsMenuPath, wsMenuStartTs, (new Date).getTime(), false, status);
                self.sessionInitializedDeferred.reject(status);
                if (status == 406) {
                    console.error("Received 406 for:" + header + " # " + config);
                }
                else {
                    console.error("Status:" + status);
                    console.dir(config);
                }
                // if we don't logout on reload, afterLoginInit is called for existing session
                // which maybe is expired and we get 406.
                if (!self.logoutOnUnload) {
                    alert(self.$scope.globals.getDynamicMessage("SessionExpired"));
                }
                else {
                    alert(self.$scope.globals.getDynamicMessage("ServerCommunicationError"));
                }
                self.logout(true);
            });
        };
        LoginService.prototype.login = function (email, password, loginSubsCode, successFunc, errFunc) {
            var self = this;
            if (self.isLoggedIn()) {
                successFunc();
                return;
            }
            var wsPath = "Login/loginUser";
            var url = "/" + self.$scope.globals.appName + "/rest/" + wsPath;
            var paramData = {};
            paramData['login'] = email;
            paramData['password'] = password;
            if (!isVoid(loginSubsCode)) {
                paramData['loginSubsCode'] = loginSubsCode;
            }
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, paramData, { withCredentials: true, timeout: self.$scope.globals.timeoutInMS, cache: false }).success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                if (!isVoid(response.isSsoCreator) && response.isSsoCreator == true) {
                    self.isSsoCreator = true;
                }
                else {
                    self.isSsoCreator = false;
                }
                if (response.data !== "SUCCESS") {
                    self.addPasswordExpirarionAlertInBannerMsg(response.data);
                }
                self.onSuccessFullLogin = successFunc;
            }).error(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                self.deleteSessionCookie();
                if (status == 401) {
                    // HTTP Status Unauthorized
                    if (response.data === "USER_PASSWORD_HAS_EXPIRED") {
                        messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", self.$scope.globals.getDynamicMessage("USER_PASSWORD_HAS_EXPIRED"), IconKind.ERROR, [new Tuple2("OK", function () {
                                self.changePassword();
                            })], 0, 0);
                        return;
                    }
                    errFunc(self.$scope.globals.getDynamicMessage(response.data));
                }
                else if (status == 406) {
                    // HTTP Status Not Acceptable
                    console.log("Received 406 for:" + header + " # " + config);
                    //alert("Received 406 from web service..." + response);
                    errFunc(response);
                }
                else {
                    console.log("Status:" + status);
                    console.dir(config);
                    alert("Timed-out waiting for data from server..." + response);
                }
            });
        };
        LoginService.prototype.loginSso = function (successFunc) {
            var self = this;
            if (!self.existsSso()) {
                return;
            }
            // Call webservice to login via SSO cookie
            var wsPath = "Login/loginSsoUser";
            var url = "/" + self.$scope.globals.appName + "/rest/" + wsPath;
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.loginSsoPromise = self.$http.post(url, {}, { withCredentials: true, timeout: self.$scope.globals.timeoutInMS, cache: false });
            self.loginSsoPromise
                .success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                if (response.data !== "SUCCESS") {
                    self.addPasswordExpirarionAlertInBannerMsg(response.data);
                }
                self.onSuccessFullLogin = successFunc;
            })
                .error(function (response, status, header, config) {
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                console.log("Failed to login via Sso:" + status);
                if (status === 401 && response.data === "USER_PASSWORD_HAS_EXPIRED") {
                    alert(self.$scope.globals.getDynamicMessage("USER_PASSWORD_HAS_EXPIRED"));
                    self.NavigationService.assignLocation("partials/loginSsoError.html");
                }
                if (status === 406) {
                    // Wow, the SSO login service says we are Unauthorized.
                    self.$timeout(function () {
                        self.NavigationService.goToNotLoggedInPage();
                    }, 0);
                }
                else {
                    // The SSO login service failed in some other manner...
                    self.$timeout(function () {
                        self.NavigationService.goToNotLoggedInPage();
                    }, 0);
                }
            });
        };
        LoginService.prototype.changePassword = function () {
            var dialogOptions = new NpTypes.DialogOptions;
            dialogOptions.width = '33em';
            dialogOptions.className = "ChangePassClass";
            this.Plato.showDialog(this.$scope, 'ChangePasswdDialog_Title', '/' + this.$scope.globals.appName + '/partials/changePass.html?rev=' + this.$scope.globals.version, dialogOptions);
        };
        LoginService.prototype.setNewPassword = function (publicUserName, oldPassword, newPassword1, newPassword2, successFunc, errFunc) {
            var self = this;
            if (isVoid(oldPassword) || oldPassword === "" ||
                isVoid(newPassword1) || newPassword1 === "" ||
                isVoid(newPassword2) || newPassword2 === "") {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", self.$scope.globals.getDynamicMessage("ChangePasswdDialog_Null_PasswordsMsg"), IconKind.ERROR, [new Tuple2("OK", function () { })], 0, 0);
                return;
            }
            if (isVoid(publicUserName) || publicUserName === "") {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", self.$scope.globals.getDynamicMessage("ChangePasswdDialog_Null_UserMsg"), IconKind.ERROR, [new Tuple2("OK", function () { })], 0, 0);
                return;
            }
            if (newPassword1 !== newPassword2) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", self.$scope.globals.getDynamicMessage("ChangePasswdDialog_Null_NewPasswordsDifMsg"), IconKind.ERROR, [new Tuple2("OK", function () { })], 0, 0);
                return;
            }
            var wsPath = "Login/setNewPassword";
            var url = "/" + self.$scope.globals.appName + "/rest/" + wsPath;
            var paramData = {};
            paramData['publicUserName'] = publicUserName;
            paramData['oldPassword'] = oldPassword;
            paramData['newPassword'] = newPassword1;
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, paramData, { withCredentials: true, timeout: self.$scope.globals.timeoutInMS, cache: false }).success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                successFunc();
            }).error(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                if (status == 401) {
                    errFunc(self.$scope.globals.getDynamicMessage(response.data));
                }
                else if (status == 406) {
                    console.log("Received 406 for:" + header + " # " + config);
                    //alert("Received 406 from web service..." + response);
                    response = response.replace(/%%/g, '\<br>');
                    errFunc(response);
                }
                else {
                    console.log("Status:" + status);
                    console.dir(config);
                    alert("Timed-out waiting for data from server..." + response);
                }
            });
        };
        LoginService.prototype.finalReload = function () {
            var self = this;
            if (self.isSsoCreator !== true && (self.existsSso() || self.ssoDeleted) && self.NavigationService.modelNavigation.notLoggedInPagePath === '/login') {
                self.ssoDeleted = false;
                self.NavigationService.assignLocation("partials/logout.html");
            }
            else {
                self.isSsoCreator = false;
                self.NavigationService.assignLocation(self.NavigationService.onReloadAppLocation);
            }
        };
        LoginService.prototype.onSessionChanged = function () {
            var self = this;
            // we use this flag to avoid alert on page unload
            self.$scope.globals.bLoggingOut = true;
            // reload app
            self.NavigationService.assignLocation(self.NavigationService.onReloadAppLocation);
        };
        LoginService.prototype.deleteSessionCookie = function () {
            var self = this;
            // This doesn't work!!!: delete $cookies[LoginService.sessionCookieKey];
            // So we set the cookie expired:
            document.cookie = self.sessionCookieKey + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT; Path=' + '/' + self.$scope.globals.appName + '/';
            document.cookie = self.sessionCookieKey + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT; Path=' + '/' + self.$scope.globals.appName;
            self.deleteSubsCodeCookie();
        };
        LoginService.prototype.deleteSubsCodeCookie = function () {
            var self = this;
            // This doesn't work!!!: delete $cookies[LoginService.subsCodeCookieKey];
            // So we set the cookie expired:
            document.cookie = self.subsCodeCookieKey + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT; Path=' + '/' + self.$scope.globals.appName + '/';
            document.cookie = self.subsCodeCookieKey + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT; Path=' + '/' + self.$scope.globals.appName;
        };
        LoginService.prototype.sessionBelongToSso = function () {
            var self = this;
            var defer = self.$q.defer();
            var wsPath = "Login/sessionBelongToSso";
            var url = "/" + self.$scope.globals.appName + "/rest/" + wsPath;
            var paramData = {};
            self.$http.post(url, paramData, { withCredentials: true, timeout: 10000, cache: false }).success(function (response, status, header, config) {
                defer.resolve(response === "true");
            }).error(function (response, status, header, config) {
                defer.reject(response);
            });
            return defer.promise;
        };
        LoginService.prototype.logout = function (fromUnload) {
            var self = this;
            var bFromUnload = !isVoid(fromUnload) && fromUnload;
            if (!self.isLoggedIn())
                return;
            function realLogout() {
                self.NavigationService.modelNavigation.logoutCallbacks.forEach(function (fn) {
                    fn();
                });
                if (!bFromUnload) {
                    self.$scope.globals.bLoggingOut = true;
                }
                var url = "/" + self.$scope.globals.appName + "/rest/Login/logoutUser";
                // This call must be asynchronous in case logout is called from page unload.
                $.ajax({
                    url: url,
                    type: 'POST',
                    async: false
                });
                self.deleteSessionCookie();
            }
            ;
            // In Some cases (e.g. when transaction is dirty) function onLogout make asynchronus get request for messageBox template (in Plato.showDialog)
            // so in page unload realLogout is never called.
            // If we are here from page unload we call realLogout without calling onLogout.
            if (bFromUnload || isVoid(self.NavigationService.modelNavigation.onLogout)) {
                realLogout();
            }
            else {
                self.NavigationService.modelNavigation.onLogout(realLogout);
            }
        };
        return LoginService;
    })();
    Services.LoginService = LoginService;
    //NavigationService
    var ModelNavigation = (function () {
        function ModelNavigation() {
            this.current = -1;
            this.menuLabel = "Hide menu";
            this.epochOfLastResize = Utils.getTimeInMS();
            this.btnState = {};
            this.banner = '';
            this.bannerClass = '';
            this.logoURL = 'img/empty-logo.jpg';
            this.logoutCallbacks = [];
            // new
            this.notLoggedInPagePath = '/login';
            this.afterLoginPagePath = '/welcome';
        }
        return ModelNavigation;
    })();
    Services.ModelNavigation = ModelNavigation;
    var NavigationService = (function () {
        function NavigationService($scope, $location, $timeout, $window, $http, $route, Plato, modelNavigation) {
            var _this = this;
            this.$scope = $scope;
            this.$location = $location;
            this.$timeout = $timeout;
            this.$window = $window;
            this.$http = $http;
            this.$route = $route;
            this.Plato = Plato;
            this.modelNavigation = modelNavigation;
            this.registerLogoutCallback = function (fn) {
                _this.modelNavigation.logoutCallbacks.push(fn);
            };
            var homeCrumb = [];
            homeCrumb.push(new NpTypes.BreadcrumbStep('/welcome', 'welcome', "BreadCrumb_Home_Label", null));
            modelNavigation.steps = homeCrumb;
            // expose NavigationService properties and methods to rootScope
            $scope.modelNavigation = modelNavigation;
            // this methods run in the constructors of page controllers,
            // so they must be already available to the parent scope (rootScope) of ng-views
            // they called also from GlobalsFrm who's constructor's input is rootScope (called by initialize method of LoginService)
            $scope.navigateForward = function (url, label, pageToGoModel) {
                _this.navigateForward(url, label, pageToGoModel);
            };
            $scope.navigateBackward = function () {
                _this.navigateBackward();
            };
            $scope.navigateToRoute = function (url, askForGlobals) {
                if (askForGlobals === void 0) { askForGlobals = false; }
                _this.navigateToRoute(url, askForGlobals);
            };
            $scope.getCurrentPageModel = function () {
                return _this.getCurrentPageModel();
            };
            $scope.setCurrentPageModel = function (mdl) {
                _this.setCurrentPageModel(mdl);
            };
            $scope.getPageModelByURL = function (url) {
                return _this.getPageModelByURL(url);
            };
            $scope.setPageModelByURL = function (url, mdl) {
                _this.setPageModelByURL(url, mdl);
            };
            $scope.hasURL = function (url) {
                return _this.hasURL(url);
            };
            $scope.showF1 = function () {
                _this.showF1();
            };
        }
        Object.defineProperty(NavigationService.prototype, "strResetVar", {
            get: function () { return this.$scope.globals.appName + "_doReset"; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NavigationService.prototype, "onReloadAppLocation", {
            get: function () { return this.$scope.globals.onReloadAppLocation; },
            enumerable: true,
            configurable: true
        });
        NavigationService.prototype.goToNotLoggedInPage = function () {
            this.$location.path(this.modelNavigation.notLoggedInPagePath);
        };
        NavigationService.prototype.goToAfterLoginPage = function () {
            var self = this;
            //self.$location.path(self.modelNavigation.afterLoginPagePath);
            self.go(self.modelNavigation.afterLoginPagePath);
        };
        NavigationService.prototype.assignLocation = function (location) {
            var newLocation = window.location.href.split('?')[0];
            newLocation = newLocation.split('#')[0];
            window.location.assign(newLocation + (!isVoid(location) ? location : ""));
            if (!isVoid(location) && location.includes("#")) {
                window.location.reload();
            }
        };
        NavigationService.prototype.createUserMenu = function (menuJson) {
            var self = this;
            var toClass = {}.toString;
            var currentNumber = 0;
            var msg = "";
            function createMenu(nodeData, level) {
                if (level === void 0) { level = 0; }
                if (toClass.call(nodeData) !== '[object Array]')
                    alert("Unexpected - createMenu called without array data");
                _.each(nodeData, function (node) {
                    var singleQuotedCleanNodeName = "'" + node.label.replace("'", "\\'") + currentNumber + "'";
                    if (node.url !== undefined) {
                        msg += "<li class='Level" + (level + 1) + "' data-ng-class=\"getSelectedClass(" + currentNumber + ")\"";
                        if (node.invisibleFunc !== undefined)
                            msg += ' data-ng-show="!' + node.invisibleFunc + '"';
                        msg += " data-ng-click=\"onMenuClick(" + currentNumber + ", '" + node.url + "', '" + node.label + "', '" + node.menuId + "')\"><p>" + '{{getALString("' + node.label + '")}}</p></li>\n';
                        currentNumber += 1;
                    }
                    if (node.items !== undefined && node.items.length !== 0) {
                        if (node.btnState !== undefined && node.btnState === true) {
                            self.modelNavigation.btnState[node.label + currentNumber] = node.btnState;
                        }
                        msg += '<button ';
                        msg += " class='Level" + level + "' ";
                        msg += 'data-ng-class="getMenuParentClass(' + singleQuotedCleanNodeName + ')" ';
                        if (node.invisibleFunc !== undefined)
                            msg += 'data-ng-show="!' + node.invisibleFunc + '" ';
                        msg += 'data-ng-click="modelNavigation.btnState[' + singleQuotedCleanNodeName + '] = ' +
                            '!modelNavigation.btnState[' + singleQuotedCleanNodeName + ']"><p>{{getALString("' + node.label + '")}}</p></button>\n';
                        //msg += "<ul data-ng-animate=\"'fade'\" data-ng-show=\"btnState['" +
                        msg += '<ul data-ng-show="modelNavigation.btnState[' + singleQuotedCleanNodeName + ']' +
                            (node.invisibleFunc !== undefined ? ' && !' + node.invisibleFunc : '') +
                            '">\n';
                        createMenu(node.items, level + 1);
                        msg += "</ul>\n";
                    }
                });
            }
            msg = "<ul>\n";
            createMenu(menuJson);
            msg += "</ul>\n";
            self.modelNavigation.menuOptions = msg;
        };
        NavigationService.prototype.navigateForward = function (url, label, pageToGoModel) {
            var self = this;
            var doActualNavigation = function (pageScopeYouAreLeavingFrom) {
                $("html").addClass("wait");
                self.modelNavigation.steps.push(new NpTypes.BreadcrumbStep(url, null, label, pageToGoModel));
                self.go(url); // true: breadcrumb steps are set here, dont use the menu lookup
            };
            if (self.modelNavigation.onNavigation === undefined)
                doActualNavigation();
            else
                self.modelNavigation.onNavigation(doActualNavigation);
        };
        NavigationService.prototype.navigateBackward = function () {
            var self = this;
            var doActualNavigation = function (pageScopeYouAreLeavingFrom) {
                $("html").addClass("wait");
                self.modelNavigation.steps.pop();
                var l = self.modelNavigation.steps.length;
                if (l > 0) {
                    self.go(self.modelNavigation.steps.slice(-1)[0].url);
                }
                else {
                    self.go('/welcome');
                }
            };
            if (self.modelNavigation.onNavigation === undefined)
                doActualNavigation();
            else
                self.modelNavigation.onNavigation(doActualNavigation);
        };
        NavigationService.prototype.navigateToRoute = function (url, askForGlobals) {
            if (askForGlobals === void 0) { askForGlobals = false; }
            var self = this;
            var doActualNavigation = function (pageScopeYouAreLeavingFrom) {
                var l = self.modelNavigation.steps.length;
                while (l > 0) {
                    if (self.modelNavigation.steps.slice(-1)[0].url === url) {
                        self.go(url, undefined, askForGlobals);
                        if (askForGlobals) {
                            self.$scope.globals.showInitialDialog(self.$scope, self.$timeout, self.$http, self.Plato);
                        }
                        return;
                    }
                    self.modelNavigation.steps.pop();
                    l = self.modelNavigation.steps.length;
                }
                console.log("navigateToRoute(): failed to find url " + url);
                self.go('/welcome', undefined, askForGlobals); // false: use menu lookup for breadcrumb
            };
            if (self.modelNavigation.onNavigation === undefined)
                doActualNavigation();
            else
                self.modelNavigation.onNavigation(doActualNavigation);
        };
        NavigationService.prototype.getCurrentPageModel = function () {
            var self = this;
            var l = self.modelNavigation.steps.length;
            if (l >= 1)
                return self.modelNavigation.steps[l - 1].model;
            else {
                console.error("getCurrentPageModel(): nothing in breadcrumb stack...");
                return undefined;
            }
        };
        NavigationService.prototype.setCurrentPageModel = function (mdl) {
            var self = this;
            var l = self.modelNavigation.steps.length;
            if (l >= 1)
                self.modelNavigation.steps[l - 1].model = mdl;
            else {
                console.error("setCurrentPageModel(): nothing in breadcrumb stack...");
            }
        };
        NavigationService.prototype.getPageModelByURL = function (url) {
            var step = this.modelNavigation.steps.firstOrNull(function (x) { return x.url === url; });
            return isVoid(step) ? undefined : step.model;
        };
        NavigationService.prototype.setPageModelByURL = function (url, mdl) {
            var step = this.modelNavigation.steps.first(function (x) { return x.url === url; });
            step.model = mdl;
        };
        NavigationService.prototype.hasURL = function (url) {
            return !isVoid(this.modelNavigation.steps.firstOrNull(function (x) { return x.url === url; }));
        };
        NavigationService.prototype.getIsAuthorizedByURL = function (url) {
            var step = this.modelNavigation.steps.firstOrNull(function (x) { return x.url === url; });
            return isVoid(step) ? false : step.isAuthorized;
        };
        NavigationService.prototype.setIsAuthorizedByURL = function (url, isAuthorized) {
            var step = this.modelNavigation.steps.firstOrNull(function (x) { return x.url === url; });
            if (!isVoid(step))
                step.isAuthorized = isAuthorized;
        };
        // From ControllerLogin
        NavigationService.prototype.go = function (path, menuId, askForGlobals) {
            var self = this;
            if (menuId !== undefined) {
                if (menuId === self.$scope.globals.activeMenuId)
                    return;
                else
                    self.$scope.globals.activeMenuId = menuId;
            }
            else {
                if (self.$location.path() === path) {
                    if (askForGlobals === undefined || askForGlobals === false) {
                        return;
                    }
                }
            }
            // Stupid bootstrap bug - popover.destroy() won't remove them
            // if the controller is no longer there.
            // Compensating - removing them here, from the DOM itself.
            $('.popover').remove();
            function doActualNavigation(pageScopeYouAreLeavingFrom) {
                self.$scope.globals.nInFlightRequests = 0;
                self.$scope.globals.isCurrentTransactionDirty = false;
                for (var i = 0; i < self.modelNavigation.steps.length; i++) {
                    var step = self.modelNavigation.steps[i];
                    if (step.url === path) {
                        self.modelNavigation.steps = self.modelNavigation.steps.slice(0, i + 1);
                        self.$scope.globals.onMenuClick(menuId);
                        if (self.$location.path() === path) {
                            self.$route.reload();
                        }
                        else {
                            self.$location.path(path);
                        }
                        return;
                    }
                }
                self.$scope.globals.onMenuClick(menuId);
                if (self.$location.path() === path) {
                    self.$route.reload();
                }
                else {
                    self.$location.path(path);
                }
            }
            doActualNavigation();
        };
        NavigationService.prototype.showF1 = function () {
            var mySelf = this;
            if ($('.HelpClass').length == 0) {
                var dialogOptions = new NpTypes.DialogOptions;
                dialogOptions.width = '30em';
                dialogOptions.className = "HelpClass";
                mySelf.Plato.showDialog(mySelf.$scope, 'HelpDialog_Title', '/' + mySelf.$scope.globals.appName + '/partials/help.html?rev=' + mySelf.$scope.globals.version, dialogOptions);
            }
        };
        return NavigationService;
    })();
    Services.NavigationService = NavigationService;
    ;
    function addNewServices(application) {
        var setupNewDialog = function (strTitle, htmlContent, dialogOptions, $compile, scope) {
            var newDialogId = Sprintf.sprintf("npInnerDlg%d", g_dialogCounter);
            g_dialogCounter += 1;
            var divString;
            if (dialogOptions.className !== undefined)
                divString = '<div class="' + dialogOptions.className + '" id="' + newDialogId + '">';
            else
                divString = '<div id="' + newDialogId + '">';
            var modalEl = angular.element(divString);
            modalEl.html(htmlContent);
            $('body').append(modalEl);
            $compile(modalEl)(scope);
            var component = $('#' + newDialogId);
            dialogOptions.jquiDialog = component;
            function viewportHeight() {
                var e = window, a = 'inner';
                if (!('innerWidth' in window)) {
                    a = 'client';
                    e = document.documentElement || document.body;
                }
                //return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
                return e[a + 'Height'];
            }
            var options = {
                autoOpen: dialogOptions.autoOpen,
                modal: dialogOptions.modal,
                title: scope.getALString(strTitle),
                maxHeight: viewportHeight(),
                close: function () {
                    if (strTitle !== 'WaitDialog_Title') {
                        component.dialog("destroy"); // memory leak otherwise...
                        component.empty();
                        component.remove();
                    }
                }
            };
            if (!isVoid(dialogOptions.onCloseDialog)) {
                options.beforeClose = function (event, ui) {
                    dialogOptions.onCloseDialog();
                };
            }
            if (!dialogOptions.showCloseButton) {
                options.closeOnEscape = false;
                options.dialogClass = 'no-close';
            }
            if (dialogOptions.width !== undefined) {
                options.width = dialogOptions.width;
            }
            dialogOptions.closeDialog = options.close;
            // HTML5 full screen interferes with jquery UI dialogs - I needed to tell it
            // to create the dialog (as a 'div') UNDER the div that hosts the OL3 canvas.
            var doc = document;
            if (doc.webkitIsFullScreen || doc.mozFullScreen) {
                try {
                    options.appendTo = '#' + $('canvas').parent().parent()[0].id;
                }
                catch (err) {
                    alert("Internal error - dialog during fullscreen has no ID");
                    return;
                }
            }
            component.dialog(options);
            component.dialog("open");
        };
        application.factory('Plato', ['$http', '$compile', function ($http, $compile) {
                return {
                    showDialog: function (scope, strTitle, templateUrl, dialogOptions) {
                        if (dialogOptions.autoOpen === undefined) {
                            dialogOptions.autoOpen = false;
                        }
                        if (dialogOptions.modal === undefined) {
                            dialogOptions.modal = true;
                        }
                        if (dialogOptions.className === undefined) {
                            throw "className not set in dialogOptions";
                        }
                        scope.globals.addDialogOption(dialogOptions);
                        $http.get(templateUrl, {}, { timeout: scope.globals.timeoutInMS, cache: false }).success(function (response, status, header, config) {
                            setupNewDialog(strTitle, response, dialogOptions, $compile, scope);
                        }).error(function (data, status, header, config) {
                            if (scope.globals.bLoggingOut === true)
                                return;
                            if (status == 406) {
                                console.log("Received 406 for:" + header + " # " + config);
                                alert("Received 406 from web service...");
                            }
                            else {
                                console.log("Status:" + status);
                                console.dir(config);
                                alert("Timed-out waiting for data from server...");
                            }
                        });
                    },
                    showDialogHTML: function (scope, strTitle, htmlContent, controllerName, controller, dialogOptions) {
                        //scope.globals.controllerProvider.register(controllerName, controller);
                        if (dialogOptions.autoOpen === undefined) {
                            dialogOptions.autoOpen = false;
                        }
                        if (dialogOptions.modal === undefined) {
                            dialogOptions.modal = true;
                        }
                        if (dialogOptions.className === undefined) {
                            throw "className not set in dialogOptions";
                        }
                        scope.globals.addDialogOption(dialogOptions);
                        setupNewDialog(strTitle, htmlContent, dialogOptions, $compile, scope);
                    }
                };
            }]);
        application.service('NavigationService', ['$rootScope', '$location', '$timeout', '$window', '$http', '$route', 'Plato', function ($rootScope, $location, $timeout, $window, $http, $route, Plato) {
                return new NavigationService($rootScope, $location, $timeout, $window, $http, $route, Plato, new ModelNavigation());
            }]);
        application.factory('LoginService', ['$rootScope', '$cookies', '$http', '$timeout', '$q', 'Plato', 'NavigationService', function ($rootScope, $cookies, $http, $timeout, $q, Plato, NavigationService) {
                return new LoginService($rootScope, $cookies, $http, $timeout, $q, Plato, NavigationService);
            }]);
    }
    Services.addNewServices = addNewServices;
})(Services || (Services = {}));
//# sourceMappingURL=services.js.map