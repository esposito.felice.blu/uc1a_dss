/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../utils.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var RTL_Subscriber = (function (_super) {
        __extends(RTL_Subscriber, _super);
        function RTL_Subscriber(id, shortName, vat, legalName, taxOffice, occupation, phone, fax, address, addressNumber, postalCode, city, prefecture, country, email, webSite, database, applicationUrl, invoicingMethod, suvcId, companyVat, billingCompId, rowVersion, userAutoInactivate, loginPolicy, legalNameShort) {
            _super.call(this);
            var self = this;
            this._id = new NpTypes.UIStringModel(id, this);
            this._shortName = new NpTypes.UIStringModel(shortName, this);
            this._vat = new NpTypes.UIStringModel(vat, this);
            this._legalName = new NpTypes.UIStringModel(legalName, this);
            this._taxOffice = new NpTypes.UIStringModel(taxOffice, this);
            this._occupation = new NpTypes.UIStringModel(occupation, this);
            this._phone = new NpTypes.UIStringModel(phone, this);
            this._fax = new NpTypes.UIStringModel(fax, this);
            this._address = new NpTypes.UIStringModel(address, this);
            this._addressNumber = new NpTypes.UIStringModel(addressNumber, this);
            this._postalCode = new NpTypes.UIStringModel(postalCode, this);
            this._city = new NpTypes.UIStringModel(city, this);
            this._prefecture = new NpTypes.UIStringModel(prefecture, this);
            this._country = new NpTypes.UIStringModel(country, this);
            this._email = new NpTypes.UIStringModel(email, this);
            this._webSite = new NpTypes.UIStringModel(webSite, this);
            this._database = new NpTypes.UIStringModel(database, this);
            this._applicationUrl = new NpTypes.UIStringModel(applicationUrl, this);
            this._invoicingMethod = new NpTypes.UINumberModel(invoicingMethod, this);
            this._suvcId = new NpTypes.UINumberModel(suvcId, this);
            this._companyVat = new NpTypes.UIStringModel(companyVat, this);
            this._billingCompId = new NpTypes.UINumberModel(billingCompId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._userAutoInactivate = new NpTypes.UIBoolModel(userAutoInactivate, this);
            this._loginPolicy = new NpTypes.UINumberModel(loginPolicy, this);
            this._legalNameShort = new NpTypes.UIStringModel(legalNameShort, this);
            self.postConstruct();
        }
        RTL_Subscriber.prototype.getKey = function () {
            return this.id;
        };
        RTL_Subscriber.prototype.getKeyName = function () {
            return "id";
        };
        RTL_Subscriber.prototype.getEntityName = function () {
            return 'RTL_Subscriber';
        };
        RTL_Subscriber.prototype.fromJSON = function (data) {
            return RTL_Subscriber.fromJSONComplete(data);
        };
        Object.defineProperty(RTL_Subscriber.prototype, "id", {
            get: function () {
                return this._id.value;
            },
            set: function (id) {
                var self = this;
                self._id.value = id;
                //    self.id_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "shortName", {
            get: function () {
                return this._shortName.value;
            },
            set: function (shortName) {
                var self = this;
                self._shortName.value = shortName;
                //    self.shortName_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "vat", {
            get: function () {
                return this._vat.value;
            },
            set: function (vat) {
                var self = this;
                self._vat.value = vat;
                //    self.vat_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "legalName", {
            get: function () {
                return this._legalName.value;
            },
            set: function (legalName) {
                var self = this;
                self._legalName.value = legalName;
                //    self.legalName_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "taxOffice", {
            get: function () {
                return this._taxOffice.value;
            },
            set: function (taxOffice) {
                var self = this;
                self._taxOffice.value = taxOffice;
                //    self.taxOffice_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "occupation", {
            get: function () {
                return this._occupation.value;
            },
            set: function (occupation) {
                var self = this;
                self._occupation.value = occupation;
                //    self.occupation_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "phone", {
            get: function () {
                return this._phone.value;
            },
            set: function (phone) {
                var self = this;
                self._phone.value = phone;
                //    self.phone_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "fax", {
            get: function () {
                return this._fax.value;
            },
            set: function (fax) {
                var self = this;
                self._fax.value = fax;
                //    self.fax_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "address", {
            get: function () {
                return this._address.value;
            },
            set: function (address) {
                var self = this;
                self._address.value = address;
                //    self.address_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "addressNumber", {
            get: function () {
                return this._addressNumber.value;
            },
            set: function (addressNumber) {
                var self = this;
                self._addressNumber.value = addressNumber;
                //    self.addressNumber_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "postalCode", {
            get: function () {
                return this._postalCode.value;
            },
            set: function (postalCode) {
                var self = this;
                self._postalCode.value = postalCode;
                //    self.postalCode_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "city", {
            get: function () {
                return this._city.value;
            },
            set: function (city) {
                var self = this;
                self._city.value = city;
                //    self.city_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "prefecture", {
            get: function () {
                return this._prefecture.value;
            },
            set: function (prefecture) {
                var self = this;
                self._prefecture.value = prefecture;
                //    self.prefecture_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "country", {
            get: function () {
                return this._country.value;
            },
            set: function (country) {
                var self = this;
                self._country.value = country;
                //    self.country_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "email", {
            get: function () {
                return this._email.value;
            },
            set: function (email) {
                var self = this;
                self._email.value = email;
                //    self.email_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "webSite", {
            get: function () {
                return this._webSite.value;
            },
            set: function (webSite) {
                var self = this;
                self._webSite.value = webSite;
                //    self.webSite_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "database", {
            get: function () {
                return this._database.value;
            },
            set: function (database) {
                var self = this;
                self._database.value = database;
                //    self.database_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "applicationUrl", {
            get: function () {
                return this._applicationUrl.value;
            },
            set: function (applicationUrl) {
                var self = this;
                self._applicationUrl.value = applicationUrl;
                //    self.applicationUrl_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "invoicingMethod", {
            get: function () {
                return this._invoicingMethod.value;
            },
            set: function (invoicingMethod) {
                var self = this;
                self._invoicingMethod.value = invoicingMethod;
                //    self.invoicingMethod_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "suvcId", {
            get: function () {
                return this._suvcId.value;
            },
            set: function (suvcId) {
                var self = this;
                self._suvcId.value = suvcId;
                //    self.suvcId_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "companyVat", {
            get: function () {
                return this._companyVat.value;
            },
            set: function (companyVat) {
                var self = this;
                self._companyVat.value = companyVat;
                //    self.companyVat_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "billingCompId", {
            get: function () {
                return this._billingCompId.value;
            },
            set: function (billingCompId) {
                var self = this;
                self._billingCompId.value = billingCompId;
                //    self.billingCompId_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "userAutoInactivate", {
            get: function () {
                return this._userAutoInactivate.value;
            },
            set: function (userAutoInactivate) {
                var self = this;
                self._userAutoInactivate.value = userAutoInactivate;
                //    self.userAutoInactivate_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "loginPolicy", {
            get: function () {
                return this._loginPolicy.value;
            },
            set: function (loginPolicy) {
                var self = this;
                self._loginPolicy.value = loginPolicy;
                //    self.loginPolicy_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTL_Subscriber.prototype, "legalNameShort", {
            get: function () {
                return this._legalNameShort.value;
            },
            set: function (legalNameShort) {
                var self = this;
                self._legalNameShort.value = legalNameShort;
                //    self.legalNameShort_listeners.forEach(cb => { cb(<Subscriber>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public legalNameShort_listeners: Array<(c:Subscriber) => void> = [];
        RTL_Subscriber.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "RTL_Subscriber:" + x.id;
            var ret = new RTL_Subscriber(x.id, x.shortName, x.vat, x.legalName, x.taxOffice, x.occupation, x.phone, x.fax, x.address, x.addressNumber, x.postalCode, x.city, x.prefecture, x.country, x.email, x.webSite, x.database, x.applicationUrl, x.invoicingMethod, x.suvcId, x.companyVat, x.billingCompId, x.rowVersion, x.userAutoInactivate, x.loginPolicy, x.legalNameShort);
            deserializedEntities[key] = ret;
            return ret;
        };
        RTL_Subscriber.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        RTL_Subscriber.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "RTL_Subscriber:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "RTL_Subscriber::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                ret = RTL_Subscriber.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        RTL_Subscriber.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return RTL_Subscriber.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        RTL_Subscriber.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var self = this;
            var url = "/" + ctrl.$scope.globals.appName + "/rest/RTL_Subscriber/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = RTL_Subscriber.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        RTL_Subscriber.prototype.toJSON = function () {
            var ret = {};
            ret.id = this.id;
            ret.shortName = this.shortName;
            ret.vat = this.vat;
            ret.legalName = this.legalName;
            ret.taxOffice = this.taxOffice;
            ret.occupation = this.occupation;
            ret.phone = this.phone;
            ret.fax = this.fax;
            ret.address = this.address;
            ret.addressNumber = this.addressNumber;
            ret.postalCode = this.postalCode;
            ret.city = this.city;
            ret.prefecture = this.prefecture;
            ret.country = this.country;
            ret.email = this.email;
            ret.webSite = this.webSite;
            ret.database = this.database;
            ret.applicationUrl = this.applicationUrl;
            ret.invoicingMethod = this.invoicingMethod;
            ret.suvcId = this.suvcId;
            ret.companyVat = this.companyVat;
            ret.billingCompId = this.billingCompId;
            ret.rowVersion = this.rowVersion;
            ret.userAutoInactivate = this.userAutoInactivate;
            ret.loginPolicy = this.loginPolicy;
            ret.legalNameShort = this.legalNameShort;
            return ret;
        };
        RTL_Subscriber.findByCode_Login_subscriber = function (name, $scope, $http, onSuccess, errFunc) {
            var self = this;
            var key = "#" + name;
            if (RTL_Subscriber.cashedEntities_findByCode_Login_subscriber[key] !== undefined) {
                onSuccess(Entities.RTL_Subscriber.cashedEntities_findByCode_Login_subscriber[key]);
                return;
            }
            var wsPath = "Login/findByCode_Login_subscriber";
            var url = "/" + $scope.globals.appName + "/rest/" + wsPath + "?";
            var paramData = {};
            if (name !== undefined && name !== null) {
                paramData['shortName'] = name;
            }
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = RTL_Subscriber.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    var ret = dbEntities[0];
                    Entities.RTL_Subscriber.cashedEntities_findByCode_Login_subscriber[key] = ret;
                    onSuccess(ret);
                }
                else if (dbEntities.length > 1) {
                    errFunc('TooManyRows');
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityByCode_NotFoundMsg()"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
        };
        RTL_Subscriber.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        RTL_Subscriber.cashedEntities_findByCode_Login_subscriber = {};
        return RTL_Subscriber;
    })(NpTypes.BaseEntity);
    Entities.RTL_Subscriber = RTL_Subscriber;
    NpTypes.BaseEntity.entitiesFactory['RTL_Subscriber'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.RTL_Subscriber.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.RTL_Subscriber.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=RTL_Subscriber.js.map