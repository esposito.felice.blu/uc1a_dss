/// <reference path="../npTypes.ts"/>
/// <reference path="../utils.ts"/>
var Controllers;
(function (Controllers) {
    var ModelMsgBox = (function () {
        function ModelMsgBox() {
        }
        return ModelMsgBox;
    })();
    Controllers.ModelMsgBox = ModelMsgBox;
    var controllerMsgBox = (function () {
        function controllerMsgBox($scope) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("MsgBoxClass");
            var model = new ModelMsgBox();
            model.opt = dialogOptions;
            model.buttonWidth = {};
            model.buttonWidth['width'] = Math.floor(100 / model.opt.buttons.length) + "%";
            var allButtonWidth = 33 * model.opt.buttons.length;
            allButtonWidth = allButtonWidth > 100 ? 100 : allButtonWidth;
            model.allButtonWidth = { margin: 'auto', width: allButtonWidth + '%' };
            $scope.model = model;
            $scope.buttonPressed = function (btn) {
                dialogOptions.jquiDialog.dialog("close");
                btn.b();
            };
            $scope.triggerCancel = function () {
                dialogOptions.jquiDialog.dialog("close");
                if (dialogOptions.indexCancel !== undefined)
                    dialogOptions.buttons[dialogOptions.indexCancel].b();
            };
        }
        return controllerMsgBox;
    })();
    Controllers.controllerMsgBox = controllerMsgBox;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerMsgBox.js.map