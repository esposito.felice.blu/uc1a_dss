/// <reference path="../npTypes.ts" />
var Controllers;
(function (Controllers) {
    var ModelRequestAuthorization = (function () {
        function ModelRequestAuthorization() {
        }
        return ModelRequestAuthorization;
    })();
    Controllers.ModelRequestAuthorization = ModelRequestAuthorization;
    var controllerRequestAuthorization = (function () {
        function controllerRequestAuthorization($scope, $http, $timeout, Plato, LoginService) {
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.LoginService = LoginService;
            var scope = $scope;
            var dialogOptions = scope.globals.findAndRemoveDialogOptionByClassName("RequestAuthorizationClass");
            scope.modelRequestAuthorization = new ModelRequestAuthorization();
            scope.modelRequestAuthorization.passwordLabel = scope.globals.requestAuthorizationOptions.getPasswordLabel(LoginService.routeAuthor.url);
            scope.modelRequestAuthorization.userNameLabel = scope.globals.requestAuthorizationOptions.getUserNameLabel(LoginService.routeAuthor.url);
            scope.useCustomMethod = function () {
                return LoginService.routeAuthor.useCustomMethod;
            };
            scope.closeRequestAuthorization = function () {
                LoginService.cancelRequestAuthorization();
                dialogOptions.jquiDialog.dialog("close");
            };
            scope.requestAuthorization = function () {
                //When "enter" pressed then input button does not loose focus and model is not updated, 
                // so we programmatically focus on submit button
                document.getElementById('submitButton').focus();
                if (isVoid(scope.modelRequestAuthorization.password) || scope.modelRequestAuthorization.password === "")
                    return;
                LoginService.requestAuthorization(scope.modelRequestAuthorization.password, scope.modelRequestAuthorization.userName, scope.globals.globalSubsCode);
                dialogOptions.jquiDialog.dialog("close");
            };
        }
        return controllerRequestAuthorization;
    })();
    Controllers.controllerRequestAuthorization = controllerRequestAuthorization;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerRequestAuthorization.js.map