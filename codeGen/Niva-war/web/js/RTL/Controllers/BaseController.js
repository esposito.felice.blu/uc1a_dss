/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../DefinitelyTyped/jqueryui/jqueryui.d.ts" />
/// <reference path="../../DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../ng-grid-layout.ts" />
/// <reference path="../ng-grid-flexible-height.ts" />
/// <reference path="../sprintf.d.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../utils.ts" />
/// <reference path="../services.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
//DELETED
// /// <reference path="controllerNavigation.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    /*
        Items
    */
    var Item = (function () {
        function Item(label, unbounded, itemValue, uiModel, isRequired, isValid) {
            this.label = label;
            this.unbounded = unbounded;
            this.itemValue = itemValue;
            this.uiModel = uiModel;
            this.isRequired = isRequired;
            this.isValid = isValid;
        }
        Item.prototype.getLabelForMessage = function (scope, ent) {
            var self = this;
            var lbl = self.label(ent);
            if (isVoid(lbl))
                return lbl;
            var translatedLabel = lbl;
            if (!isVoid(scope))
                translatedLabel = scope.getALString(lbl, true);
            return translatedLabel.replace("<br/>", "&#160;").replace("&#160;&#160;", "&#160;");
        };
        Item.prototype.validateItem = function (scope, ent) {
            var self = this;
            var ret = [];
            var itmValue = self.itemValue(ent);
            var uiModelError = self.uiModel(ent).getErrors();
            for (var i = 0; i < uiModelError.length; i++) {
                ret.push(scope.globals.getDynamicMessage("FieldValidationErrorMsg(\"" + self.getLabelForMessage(scope, ent) + "\")") + ": " + uiModelError[i]);
            }
            //check item for errors only if there are not registered errors
            if (uiModelError.length == 0) {
                if (isVoid(itmValue) && self.isRequired(ent)) {
                    var errMsg1 = scope.globals.getDynamicMessage("FieldValidation_RequiredMsg1(\"" + self.getLabelForMessage(scope, ent) + "\")");
                    ret.push(errMsg1);
                    var errMsg2 = scope.globals.getDynamicMessage("FieldValidation_RequiredMsg2()");
                    self.uiModel(ent).addNewErrorMessage(errMsg2, false);
                }
                var custValidation = self.isValid(itmValue, ent);
                if (!custValidation.isValid)
                    ret.push(custValidation.errorMessage);
            }
            return ret;
        };
        return Item;
    })();
    Controllers.Item = Item;
    var TextItem = (function (_super) {
        __extends(TextItem, _super);
        function TextItem(label, unbounded, itemValue, uiModel, isRequired, isValid, maxLength) {
            _super.call(this, label, unbounded, itemValue, uiModel, isRequired, isValid);
            this.label = label;
            this.unbounded = unbounded;
            this.itemValue = itemValue;
            this.uiModel = uiModel;
            this.isRequired = isRequired;
            this.isValid = isValid;
            this.maxLength = maxLength;
        }
        TextItem.prototype.validateItem = function (scope, ent) {
            var self = this;
            var ret = _super.prototype.validateItem.call(this, scope, ent);
            var itmValue = self.itemValue(ent);
            if (!isVoid(itmValue)) {
                if (!isVoid(self.maxLength) && itmValue.length > self.maxLength) {
                    ret.push(scope.globals.getDynamicMessage("FieldValidation_MaxLengthMsg(\"" + self.getLabelForMessage(scope, ent) + "\", \"" + itmValue + "\", " + self.maxLength + ")"));
                }
            }
            return ret;
        };
        return TextItem;
    })(Item);
    Controllers.TextItem = TextItem;
    var MapItem = (function (_super) {
        __extends(MapItem, _super);
        function MapItem(label, unbounded, itemValue, uiModel, isRequired, isValid, maxLength) {
            _super.call(this, label, unbounded, itemValue, uiModel, isRequired, isValid);
            this.label = label;
            this.unbounded = unbounded;
            this.itemValue = itemValue;
            this.uiModel = uiModel;
            this.isRequired = isRequired;
            this.isValid = isValid;
            this.maxLength = maxLength;
        }
        return MapItem;
    })(Item);
    Controllers.MapItem = MapItem;
    var DateOrNumberItem = (function (_super) {
        __extends(DateOrNumberItem, _super);
        function DateOrNumberItem(label, unbounded, itemValue, uiModel, isRequired, isValid, minVal, maxVal) {
            _super.call(this, label, unbounded, itemValue, uiModel, isRequired, isValid);
            this.label = label;
            this.unbounded = unbounded;
            this.itemValue = itemValue;
            this.uiModel = uiModel;
            this.isRequired = isRequired;
            this.isValid = isValid;
            this.minVal = minVal;
            this.maxVal = maxVal;
        }
        DateOrNumberItem.prototype.validateItem = function (scope, ent) {
            var self = this;
            var ret = _super.prototype.validateItem.call(this, scope, ent);
            var itmValue = self.itemValue(ent);
            var minVal = (isVoid(self.minVal) || isVoid(self.minVal())) ? undefined : self.minVal();
            var maxVal = (isVoid(self.maxVal) || isVoid(self.maxVal())) ? undefined : self.maxVal();
            if (!isVoid(itmValue)) {
                if (!isVoid(minVal) && !isVoid(maxVal) && (itmValue < minVal || itmValue > maxVal)) {
                    ret.push(scope.globals.getDynamicMessage("FieldValidation_MinMaxValueMsg(\"" + self.getLabelForMessage(scope, ent) + "\", \"" + itmValue + "\", \"" + maxVal + "\", \"" + minVal + "\")"));
                }
                else if (!isVoid(minVal) && itmValue < minVal) {
                    ret.push(scope.globals.getDynamicMessage("FieldValidation_MinValueMsg(\"" + self.getLabelForMessage(scope, ent) + "\", \"" + itmValue + "\", \"" + minVal + "\")"));
                }
                else if (!isVoid(maxVal) && itmValue > maxVal) {
                    ret.push(scope.globals.getDynamicMessage("FieldValidation_MaxValueMsg(\"" + self.getLabelForMessage(scope, ent) + "\", \"" + itmValue + "\", \"" + maxVal + "\")"));
                }
            }
            return ret;
        };
        return DateOrNumberItem;
    })(Item);
    Controllers.DateOrNumberItem = DateOrNumberItem;
    var NumberItem = (function (_super) {
        __extends(NumberItem, _super);
        function NumberItem(label, unbounded, itemValue, uiModel, isRequired, isValid, minVal, maxVal, maxDecimals) {
            _super.call(this, label, unbounded, itemValue, uiModel, isRequired, isValid);
            this.label = label;
            this.unbounded = unbounded;
            this.itemValue = itemValue;
            this.uiModel = uiModel;
            this.isRequired = isRequired;
            this.isValid = isValid;
            this.minVal = minVal;
            this.maxVal = maxVal;
            this.maxDecimals = maxDecimals;
        }
        NumberItem.prototype.validateItem = function (scope, ent) {
            var self = this;
            var ret = _super.prototype.validateItem.call(this, scope, ent);
            var itmValue = self.itemValue(ent);
            if (!isVoid(self.maxDecimals) && !isVoid(itmValue)) {
                var delta = parseFloat(itmValue.toFixed(self.maxDecimals + 1)) - parseFloat(itmValue.toFixed(self.maxDecimals));
                if (delta !== 0) {
                    ret.push(scope.globals.getDynamicMessage("FieldValidation_MaxDecimalsMsg(\"" + self.getLabelForMessage(scope, ent) + "\", \"" + itmValue + "\", \"" + self.maxDecimals + "\")"));
                }
            }
            return ret;
        };
        return NumberItem;
    })(DateOrNumberItem);
    Controllers.NumberItem = NumberItem;
    var DateItem = (function (_super) {
        __extends(DateItem, _super);
        function DateItem() {
            _super.apply(this, arguments);
        }
        return DateItem;
    })(DateOrNumberItem);
    Controllers.DateItem = DateItem;
    var LovItem = (function (_super) {
        __extends(LovItem, _super);
        function LovItem() {
            _super.apply(this, arguments);
        }
        return LovItem;
    })(Item);
    Controllers.LovItem = LovItem;
    var LovItem2 = (function (_super) {
        __extends(LovItem2, _super);
        function LovItem2(label, unbounded, itemValue, uiModel, isRequired, isValid, grp, LovFrmTitle, sLovClassName, bMultiSelect, setLovValue, onValueChange, partialName, bHasNewButton, sEditPageTitle, sEditFrmWidth, soEditPageClassName, sEditPagePartial, sLovEntityName, sMainQueryID, sGetIdsQueryID, setExtraReturnValues, getInputParams, shownCols) {
            if (setExtraReturnValues === void 0) { setExtraReturnValues = function (grpEnt, lovEnt) { }; }
            if (getInputParams === void 0) { getInputParams = function (lovModel) { return {}; }; }
            if (shownCols === void 0) { shownCols = {}; }
            _super.call(this, label, unbounded, itemValue, uiModel, isRequired, isValid);
            this.label = label;
            this.unbounded = unbounded;
            this.itemValue = itemValue;
            this.uiModel = uiModel;
            this.isRequired = isRequired;
            this.isValid = isValid;
            this.grp = grp;
            this.LovFrmTitle = LovFrmTitle;
            this.sLovClassName = sLovClassName;
            this.bMultiSelect = bMultiSelect;
            this.setLovValue = setLovValue;
            this.onValueChange = onValueChange;
            this.partialName = partialName;
            this.bHasNewButton = bHasNewButton;
            this.sEditPageTitle = sEditPageTitle;
            this.sEditFrmWidth = sEditFrmWidth;
            this.soEditPageClassName = soEditPageClassName;
            this.sEditPagePartial = sEditPagePartial;
            this.sLovEntityName = sLovEntityName;
            this.sMainQueryID = sMainQueryID;
            this.sGetIdsQueryID = sGetIdsQueryID;
            this.setExtraReturnValues = setExtraReturnValues;
            this.getInputParams = getInputParams;
            this.shownCols = shownCols;
        }
        LovItem2.prototype.showBoundedLov = function (grpEntity) {
            var self = this;
            function httpPost(url, paramData, wsPath) {
                var promise = self.grp.$http.post(url, paramData, { timeout: self.grp.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            }
            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = self.LovFrmTitle;
            dialogOptions.previousModel = self._previousLovModel;
            dialogOptions.className = self.sLovClassName;
            dialogOptions.bMultiSelect = self.bMultiSelect;
            dialogOptions.makeWebRequestGetIds = function (lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var wsPath = self.sLovEntityName + '/' + self.sGetIdsQueryID;
                var url = "/" + self.grp.$scope.globals.appName + '/rest/' + wsPath + '?';
                var paramData = self.getInputParams(lovModel, grpEntity);
                paramData['exc_Id'] = excludedIds;
                Utils.showWaitWindow(self.grp.$scope, self.grp.Plato, self.grp.$timeout);
                return httpPost(url, paramData, wsPath);
            };
            var uimodel = self.uiModel(grpEntity);
            dialogOptions.onSelect = function (selectedEntity) {
                uimodel.clearAllErrors();
                if (self.isRequired(grpEntity) && isVoid(selectedEntity)) {
                    uimodel.addNewErrorMessage(self.grp.$scope.globals.getDynamicMessage("FieldValidation_RequiredMsg2()"), true);
                    self.grp.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                var validationResult = self.isValid(selectedEntity, grpEntity);
                if (!validationResult.isValid) {
                    uimodel.addNewErrorMessage(validationResult.errorMessage, true);
                    self.grp.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                if (!isVoid(selectedEntity) && selectedEntity.isEqual(self.itemValue(grpEntity)))
                    return;
                self.setLovValue(selectedEntity, grpEntity);
                //self.grp.markEntityAsUpdated(grpEntity, '<sItemId>');
                self.onValueChange(grpEntity);
                self.setExtraReturnValues(grpEntity, selectedEntity);
            };
            dialogOptions.hasNewButton = self.bHasNewButton;
            dialogOptions.openNewEntityDialog = function () {
                var dlgOptions = new NpTypes.DialogOptions();
                dlgOptions.title = self.sEditPageTitle;
                dlgOptions.width = self.sEditFrmWidth;
                dlgOptions.className = self.soEditPageClassName;
                self.grp.Plato.showDialog(self.grp.$scope, dlgOptions.title, "/" + self.grp.$scope.globals.appName + '/partials/' + self.sEditPagePartial + '?rev=' + self.grp.$scope.globals.version, dlgOptions);
            };
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                self._previousLovModel = lovModel;
                var paramData = self.getInputParams(lovModel, grpEntity);
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                Utils.showWaitWindow(self.grp.$scope, self.grp.Plato, self.grp.$timeout);
                var wsPath = self.sLovEntityName + '/' + self.sMainQueryID;
                var url = "/" + self.grp.$scope.globals.appName + '/rest/' + wsPath + '?';
                return httpPost(url, paramData, wsPath);
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                var paramData = self.getInputParams(lovModel, grpEntity);
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            dialogOptions.shownCols = self.shownCols;
            self.grp.Plato.showDialog(self.grp.$scope, dialogOptions.title, "/" + self.grp.$scope.globals.appName + '/partials/lovs/' + self.partialName + '?rev=' + self.grp.$scope.globals.version, dialogOptions);
        };
        LovItem2.prototype.findByCode = function (newCodeValue, component, grpEntity) {
            var self = this;
            var uimodel = self.uiModel(grpEntity);
            var onSuccess = function (selectedEntity) {
                uimodel.clearAllErrors();
                if (self.isRequired(grpEntity) && isVoid(selectedEntity)) {
                    uimodel.addNewErrorMessage(self.grp.$scope.globals.getDynamicMessage("FieldValidation_RequiredMsg2()"), true);
                    //component.element.val(<if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif > <sLovFieldName>.< sCodeField>);
                    self.grp.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                var validationResult = self.isValid(selectedEntity, grpEntity);
                if (!validationResult.isValid) {
                    uimodel.addNewErrorMessage(validationResult.errorMessage, true);
                    self.grp.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                if (!isVoid(selectedEntity) && selectedEntity.isEqual(self.itemValue(grpEntity)))
                    return;
                self.setLovValue(selectedEntity, grpEntity);
                //self.grp.markEntityAsUpdated(grpEntity, '<sItemId>');
                self.onValueChange(grpEntity);
                self.setExtraReturnValues(grpEntity, selectedEntity);
            };
        };
        return LovItem2;
    })(Item);
    Controllers.LovItem2 = LovItem2;
    var CheckBoxItem = (function (_super) {
        __extends(CheckBoxItem, _super);
        function CheckBoxItem() {
            _super.apply(this, arguments);
        }
        return CheckBoxItem;
    })(Item);
    Controllers.CheckBoxItem = CheckBoxItem;
    var BlobItem = (function (_super) {
        __extends(BlobItem, _super);
        function BlobItem() {
            _super.apply(this, arguments);
        }
        return BlobItem;
    })(Item);
    Controllers.BlobItem = BlobItem;
    var StaticListItem = (function (_super) {
        __extends(StaticListItem, _super);
        function StaticListItem() {
            _super.apply(this, arguments);
        }
        return StaticListItem;
    })(Item);
    Controllers.StaticListItem = StaticListItem;
    var ItemRegion /*implements IRegion*/ = (function () {
        function ItemRegion /*implements IRegion*/() {
        }
        return ItemRegion /*implements IRegion*/;
    })();
    Controllers.ItemRegion /*implements IRegion*/ = ItemRegion /*implements IRegion*/;
    var RegionContainer /*implements IRegion*/ = (function () {
        function RegionContainer /*implements IRegion*/() {
        }
        return RegionContainer /*implements IRegion*/;
    })();
    Controllers.RegionContainer /*implements IRegion*/ = RegionContainer /*implements IRegion*/;
    /*
        Groups
    */
    (function (LockKind) {
        LockKind[LockKind["DisabledLock"] = 0] = "DisabledLock";
        LockKind[LockKind["ReadOnlyLock"] = 1] = "ReadOnlyLock";
        LockKind[LockKind["InvisibleLock"] = 2] = "InvisibleLock";
        LockKind[LockKind["DeleteLock"] = 3] = "DeleteLock";
        LockKind[LockKind["UpdateLock"] = 4] = "UpdateLock";
    })(Controllers.LockKind || (Controllers.LockKind = {}));
    var LockKind = Controllers.LockKind;
    (function (ChangeStatus) {
        ChangeStatus[ChangeStatus["NEW"] = 0] = "NEW";
        ChangeStatus[ChangeStatus["UPDATE"] = 1] = "UPDATE";
        ChangeStatus[ChangeStatus["DELETE"] = 2] = "DELETE";
    })(Controllers.ChangeStatus || (Controllers.ChangeStatus = {}));
    var ChangeStatus = Controllers.ChangeStatus;
    var ChangeToCommit = (function () {
        function ChangeToCommit(status, when, entityName, entity) {
            this.status = status;
            this.when = when;
            this.entityName = entityName;
            this.entity = entity;
        }
        return ChangeToCommit;
    })();
    Controllers.ChangeToCommit = ChangeToCommit;
    var ChangedEntity = (function (_super) {
        __extends(ChangedEntity, _super);
        function ChangedEntity() {
            _super.apply(this, arguments);
        }
        return ChangedEntity;
    })(Tuple2);
    Controllers.ChangedEntity = ChangedEntity;
    // Group Models
    var AbstractGroupModel = (function () {
        function AbstractGroupModel($scope) {
            this.$scope = $scope;
            this.visibleEntities = [];
            this.selectedEntities = [];
            this.lastRequestId = 0;
            this.newEntities = [];
            this.updatedEntities = {};
            this.deletedEntities = {};
        }
        AbstractGroupModel.prototype.getPersistedModel = function () {
            return {};
        };
        AbstractGroupModel.prototype.setPersistedModel = function (vl) {
        };
        return AbstractGroupModel;
    })();
    Controllers.AbstractGroupModel = AbstractGroupModel;
    var AbstractGroupTableModel = (function (_super) {
        __extends(AbstractGroupTableModel, _super);
        function AbstractGroupTableModel($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this.showSearchParams = false;
            this.updateTotalCompleted = false;
            this.nInProgressCounts = 0;
            this.totalItems = 0;
        }
        return AbstractGroupTableModel;
    })(AbstractGroupModel);
    Controllers.AbstractGroupTableModel = AbstractGroupTableModel;
    var AbstractGroupFormModel = (function (_super) {
        __extends(AbstractGroupFormModel, _super);
        function AbstractGroupFormModel($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
        }
        return AbstractGroupFormModel;
    })(AbstractGroupModel);
    Controllers.AbstractGroupFormModel = AbstractGroupFormModel;
    var AbstractController = (function () {
        function AbstractController($scope, $http, $timeout, Plato) {
            var _this = this;
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.getALString = this.$scope.getALString;
            this.ongoingRequests = {};
            $scope.getLabelById = function (itmId) { return _this.getLabelById(itmId); };
        }
        AbstractController.prototype.initialize = function (navigationService, qService) {
            var self = this;
            self.NavigationService = navigationService;
            self.$q = qService;
        };
        AbstractController.prototype.dynamicMessage = function (sMsg) {
            throw "not implemented function";
        };
        AbstractController.prototype.getLabelById = function (itmId) {
            switch (itmId) {
                case 'saveBtn_id':
                    return 'Save';
                case 'searchBtn_id':
                    return 'Search';
                case 'clearBtn_id':
                    return 'Clear';
                case 'newBtn_id':
                case 'newPageBtn_id':
                    return 'New Record';
                case 'cancelBtn_id':
                    return 'Back';
                case 'deleteBtn_id':
                    return 'Delete';
                case 'auditBtn_id':
                    return '?';
            }
            return itmId;
        };
        Object.defineProperty(AbstractController.prototype, "AppName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractController.prototype, "ControllerClassName", {
            get: function () {
                throw "Not implemented function:PageModel";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractController.prototype, "PageModel", {
            get: function () {
                throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:PageModel");
            },
            enumerable: true,
            configurable: true
        });
        AbstractController.prototype.httpPost = function (url, data, successFunc, errFunc, requestConfig) {
            var _this = this;
            var timeout = !isVoid(requestConfig) && !isVoid(requestConfig.timeout) ? requestConfig.timeout : this.$scope.globals.timeoutInMS;
            this.httpMethod(url, data, successFunc, function () { return _this.$http.post(url, data, { timeout: timeout, cache: false }); }, errFunc);
        };
        AbstractController.prototype.httpGet = function (url, data, successFunc, errFunc, requestConfig) {
            var _this = this;
            var timeout = !isVoid(requestConfig) && !isVoid(requestConfig.timeout) ? requestConfig.timeout : this.$scope.globals.timeoutInMS;
            this.httpMethod(url, data, successFunc, function () { return _this.$http.get(url, { timeout: timeout, cache: false, params: data }); }, errFunc);
        };
        AbstractController.prototype.getWsPathFromUrl = function (url) { return url.split('/').reverse().slice(0, 2).reverse().join('/').replace('?', ''); };
        AbstractController.prototype.httpMethod = function (url, data, successFunc, httpMethod, errFunc) {
            var _this = this;
            var requestKey = url + JSON.stringify(data);
            var makeWebRequest = this.ongoingRequests[requestKey] === undefined;
            if (makeWebRequest) {
                this.ongoingRequests[requestKey] === true;
                Utils.showWaitWindow(this.$scope, this.Plato, this.$timeout);
                var wsPath = this.getWsPathFromUrl(url);
                var wsStartTs = (new Date).getTime();
                httpMethod().
                    success(function (response, status, header, config) {
                    _this.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (_this.$scope.globals.nInFlightRequests > 0)
                        _this.$scope.globals.nInFlightRequests--;
                    delete _this.ongoingRequests[requestKey];
                    successFunc(response);
                }).
                    error(function (data, status, header, config) {
                    _this.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (_this.$scope.globals.nInFlightRequests > 0)
                        _this.$scope.globals.nInFlightRequests--;
                    delete _this.ongoingRequests[requestKey];
                    var errMsg = data;
                    if (status === 0 && (isVoid(errMsg) || errMsg === "")) {
                        errMsg = "HttpRequestTimeOutMessage";
                    }
                    if (errFunc === undefined) {
                        NpTypes.AlertMessage.addDanger(_this.PageModel, _this.dynamicMessage(errMsg));
                    }
                    else {
                        errFunc(errMsg);
                    }
                    console.error("Error received in makeWebRequest:" + data);
                    console.dir(status);
                    console.dir(header);
                    console.dir(config);
                });
            }
        };
        return AbstractController;
    })();
    Controllers.AbstractController = AbstractController;
    // Group Controllers
    var AbstractGroupController = (function (_super) {
        __extends(AbstractGroupController, _super);
        function AbstractGroupController($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato);
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            $scope.model = model;
            model.selectedEntities = [];
            model.visibleEntities = [];
            $scope.newIsDisabled = function () { return self._newIsDisabled(); };
            $scope.multInsertIsDisabled = function () { return self._multInsertIsDisabled(); };
            $scope.expToExcelIsDisabled = function () { return self._expToExcelIsDisabled(); };
            $scope.deleteIsDisabled = function (ent) { return self._deleteIsDisabled(ent); };
            $scope.isDisabled = function () { return self._isDisabled(); };
            $scope.isInvisible = function () { return self._isInvisible(); };
            $scope.isTabActive = function (tabRegionId) {
                return self.isTabActive(tabRegionId);
            };
            $scope.isItemDisabled = function (x) {
                return false;
            };
            $scope.isItemInvisible = function (x) {
                return false;
            };
            $scope.createNewEntityAndAddToUI = function () {
                return self.createNewEntityAndAddToUI();
            };
            $scope.showAuditInfo = function () {
                self.showAuditInfo();
            };
            $scope.multipleInsertEntities = function () {
                self.multipleInsertEntities();
            };
            $scope.markEntityAsUpdated = function (ent, itemId) {
                self.markEntityAsUpdated(ent, itemId);
            };
            $scope.deleteEntity = function (c, triggerUpdate, rowIndex, bCascade) {
                self.deleteEntity(c, triggerUpdate, rowIndex, bCascade);
            };
            $scope.$on('$destroy', function (evt) {
                var cols = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    cols[_i - 1] = arguments[_i];
                }
                $scope.model.visibleEntities.splice(0);
                $scope.model.visibleEntities = null;
                $scope.model.selectedEntities.splice(0);
                $scope.model.selectedEntities = null;
                $scope.model = null;
            });
            $scope.cloneEntity = function (src) {
                return self.cloneEntity(src);
            };
        }
        AbstractGroupController.prototype.cloneEntity = function (src) {
            throw "unimplemented function";
        };
        AbstractGroupController.prototype.onCloneEntity = function (src) {
        };
        AbstractGroupController.prototype._newIsDisabled = function () {
            return false;
        };
        AbstractGroupController.prototype._multInsertIsDisabled = function () {
            return false;
        };
        AbstractGroupController.prototype._expToExcelIsDisabled = function () {
            return false;
        };
        AbstractGroupController.prototype._deleteIsDisabled = function (ent) {
            return false;
        };
        AbstractGroupController.prototype._isDisabled = function () {
            return false;
        };
        AbstractGroupController.prototype._isInvisible = function () {
            return false;
        };
        AbstractGroupController.prototype.isParentTabUnderConstruction = function (divId) {
            var tabRegionDiv = $('#' + divId);
            var parentTabControls = tabRegionDiv.closest('div.ui-tabs');
            if (parentTabControls.length === 0)
                return false; // not tab control
            //every surrounding tab control must have the .ui-widget class
            var bSomeTabAreUnderConstruction = parentTabControls.not('.ui-widget').length > 0;
            if (bSomeTabAreUnderConstruction)
                return true;
            return false;
        };
        AbstractGroupController.prototype.isTabActive = function (tabRegionId) {
            var tabRegionDiv = $('#' + tabRegionId);
            var parentTabControls = tabRegionDiv.closest('div.ui-tabs');
            if (parentTabControls.length === 0)
                return true; // not tab control
            //every surrounding tab control must have the .ui-widget class
            var bSomeTabAreUnderConstruction = parentTabControls.not('.ui-widget').length > 0;
            if (bSomeTabAreUnderConstruction)
                return true;
            var ulTabsNav = parentTabControls.tabs().children('ul.ui-tabs-nav');
            if (isVoid(ulTabsNav))
                return true;
            var activeTab = ulTabsNav.find('li.ui-state-active');
            if (isVoid(activeTab))
                return true;
            var activeRegionId = activeTab.attr('aria-controls');
            if (isVoid(activeRegionId))
                return true;
            return activeRegionId === tabRegionId;
        };
        AbstractGroupController.prototype.cleanUpAfterSave = function () {
            var self = this;
            self.model.deletedEntities = {};
            self.model.updatedEntities = {};
            self.model.newEntities = [];
        };
        AbstractGroupController.prototype.constructEntity = function () {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:constructEntity");
        };
        AbstractGroupController.prototype.lockParent = function () {
            return false;
        };
        Object.defineProperty(AbstractGroupController.prototype, "Parent", {
            get: function () {
                return undefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractGroupController.prototype, "ParentController", {
            get: function () {
                return undefined;
            },
            enumerable: true,
            configurable: true
        });
        AbstractGroupController.prototype.markParentEntityAsUpdated = function (itemId) {
            var self = this;
            if (!self.lockParent())
                return;
            if (isVoid(self.ParentController) || isVoid(self.Parent) || self.Parent.isNew())
                return;
            self.ParentController.markEntityAsUpdated(self.Parent, itemId);
        };
        // called by buttons Add
        // groups must override this method in 
        AbstractGroupController.prototype.createNewEntityAndAddToUI = function () {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:createNewEntityAndAddToUI");
        };
        Object.defineProperty(AbstractGroupController.prototype, "Current", {
            get: function () {
                return this.model.selectedEntities[0];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractGroupController.prototype, "CurrentRowIndex", {
            get: function () {
                var _this = this;
                var rowIndex = 0;
                if (!isVoid(this.Current) && !isVoid(this.model.visibleEntities) && this.model.visibleEntities.length > 0) {
                    this.model.visibleEntities.some(function (ent, indx) {
                        if (ent.getKey() === _this.Current.getKey()) {
                            rowIndex = indx;
                            return true;
                        }
                    });
                }
                return rowIndex;
            },
            enumerable: true,
            configurable: true
        });
        AbstractGroupController.prototype.showAuditInfo = function () {
            var self = this;
            if (!self.$scope.globals.hasPrivilege(self.$scope.globals.appName + "_" + "ShowAuditInfo"))
                return;
            function showAuditInfoMessage(usrIns, dteIns, usrUpd, dteUpd) {
                messageBox(self.$scope, self.Plato, self.getALString("Audit Info of Record"), '<table>' +
                    '<tbody>' +
                    '<tr>' +
                    '<td colspan="1" style="width:40%"><label class="text-right" style="font-size:13px !important; font-weight:bold !important; min-width:10em" >' + self.getALString("Insert User") + ':</label></td>' +
                    '<td colspan="1" style="width:60%"><label class="text-left" style="font-size:13px !important">' + usrIns + '</label></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td colspan="1" style="width:40%"><label class="text-right" style="font-size:13px !important; font-weight:bold !important; min-width:10em">' + self.getALString("Insert Timestamp") + ':</label></td>' +
                    '<td colspan="1" style="width:60%"><label class="text-left" style="font-size:13px !important">' + dteIns + '</label></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td colspan="1" style="width:40%"><label class="text-right" style="font-size:13px !important; font-weight:bold !important; min-width:10em">' + self.getALString("Last Update User") + ':</label></td>' +
                    '<td colspan="1" style="width:60%"><label class="text-left" style="font-size:13px !important">' + usrUpd + '</label></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td colspan="1" style="width:40%"><label class="text-right" style="font-size:13px !important; font-weight:bold !important; min-width:10em">' + self.getALString("Last Update Timestamp") + ':</label></td>' +
                    '<td colspan="1" style="width:60%"><label class="text-left" style="font-size:13px !important">' + dteUpd + '</label></td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>', IconKind.INFO, [new Tuple2("OK", function () { })], 0, 0, '30em');
            }
            var cr = this.Current;
            if (!isVoid(cr)) {
                var ai = cr.getAuditInfo();
                var dteIns = !isVoid(ai) && !isVoid(ai.dateInsert) ? Utils.convertDateToString(ai.dateInsert, 'dd/MM/yyyy HH:mm:ss') : '';
                var dteUpd = !isVoid(ai) && !isVoid(ai.dateUpdate) ? Utils.convertDateToString(ai.dateUpdate, 'dd/MM/yyyy HH:mm:ss') : '';
                var userInsert = !isVoid(ai) && !isVoid(ai.userInsert) ? ai.userInsert : '';
                var usrIns = '';
                var userUpdate = !isVoid(ai) && !isVoid(ai.userUpdate) ? ai.userUpdate : '';
                var usrUpd = '';
                if (userInsert !== '' || userUpdate !== '') {
                    var url = "/" + this.AppName + "/rest/Menu/getAuditInfoUsers?";
                    self.httpPost(url, { "userInsert": userInsert, "userUpdate": userUpdate }, function (rsp) {
                        usrIns =
                            ((isVoid(rsp.userinsertlastname) ? '' : rsp.userinsertlastname) + ' ' +
                                (isVoid(rsp.userinsertfirstname) ? '' : rsp.userinsertfirstname)).trim();
                        if (!isVoid(rsp.userinsertusername) && rsp.userinsertusername !== '')
                            usrIns = usrIns + ' (' + rsp.userinsertusername + ')';
                        usrUpd =
                            ((isVoid(rsp.userupdatelastname) ? '' : rsp.userupdatelastname) + ' ' +
                                (isVoid(rsp.userupdatefirstname) ? '' : rsp.userupdatefirstname)).trim();
                        if (!isVoid(rsp.userupdateusername) && rsp.userupdateusername !== '')
                            usrUpd = usrUpd + ' (' + rsp.userupdateusername + ')';
                        showAuditInfoMessage(usrIns, dteIns, usrUpd, dteUpd);
                    });
                }
                else {
                    showAuditInfoMessage(usrIns, dteIns, usrUpd, dteUpd);
                }
            }
        };
        // groups must override this method in 
        AbstractGroupController.prototype.multipleInsertEntities = function () {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:multipleInsertEntities");
        };
        AbstractGroupController.prototype.deleteNewEntitiesUnderParent = function (parentEntity) {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:deleteNewEntitiesUnderParent");
        };
        AbstractGroupController.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
            if (bCascade === void 0) { bCascade = false; }
            if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
            var self = this;
            if (ent === undefined) {
                console.warn('deleteEntity called for an undefined entity');
                return;
            }
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('delete of ' + ent.getEntityName());
            if (!ent.isNew()) {
                if (self.model.updatedEntities[ent.getKey()] !== undefined) {
                    delete self.model.updatedEntities[ent.getKey()];
                }
                self.model.deletedEntities[ent.getKey()] = new Tuple2(ent, Utils.getTimeInMS());
            }
            else {
                var i = 0;
                for (i = 0; i < self.model.newEntities.length; i++) {
                    var e = self.model.newEntities[i];
                    if (ent.getKey() === e.a.getKey()) {
                        self.model.newEntities.splice(i, 1);
                        break;
                    }
                }
            }
            afterDeleteAction();
            // for each child group
            // TODO: call deleteNewEntitiesUnderParent(ent)
        };
        AbstractGroupController.prototype.updateVisibleEntities = function (ent) {
            var visInstance = this.model.visibleEntities.firstOrNull(function (x) { return x.getKey() === ent.getKey(); });
            if (!isVoid(visInstance)) {
                visInstance.updateInstance(ent);
            }
        };
        AbstractGroupController.prototype.markEntityAsUpdated = function (ent, itemId) {
            if (ent === undefined) {
                console.warn('markEntityAsUpdated called for an undefined entity');
                return;
            }
            var self = this;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated(itemId);
            var isNew = _.any(self.model.newEntities, function (e) {
                return ent.getKey() === e.a.getKey();
            });
            this.updateVisibleEntities(ent);
            if (isNew)
                return;
            if (self.model.updatedEntities[ent.getKey()] === undefined) {
                var changeEntity = new Tuple2(ent, Utils.getTimeInMS());
                self.model.updatedEntities[ent.getKey()] = changeEntity;
            }
        };
        AbstractGroupController.prototype.getEntitiesFromJSON = function (webResponse) {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:getEntitiesFromJSON");
        };
        AbstractGroupController.prototype.updateUI = function () {
            if (!isVoid(this.model.selectedEntities[0])) {
                this.model.selectedEntities[0].refresh(this);
            }
        };
        AbstractGroupController.prototype.isVisible = function () { throw "not implemented function"; };
        AbstractGroupController.prototype.refreshVisibleEntities = function (newEntitiesIds, afterRefreshFunc) {
            var _this = this;
            if (!this.isVisible())
                return;
            if (this.model.selectedEntities.length === 0)
                return;
            var url = "/" + this.AppName + "/rest/" + this.getEntityName() + "/findAllByIds?";
            function getVisibleEntityDbId(vis) {
                return vis.isNew() ?
                    newEntitiesIds.first(function (y) { return y.tempId === vis.getKey(); }).databaseId :
                    vis.getKey();
            }
            var ids = this.model.visibleEntities.map(function (x) { return getVisibleEntityDbId(x); });
            this.httpPost(url, { "ids": ids }, function (rsp) {
                var refresedEntities = _this.getEntitiesFromJSON(rsp);
                var deletedEntities = [];
                _this.model.visibleEntities.forEach(function (vis) {
                    var visId = getVisibleEntityDbId(vis);
                    var refreshedEntity = refresedEntities.firstOrNull(function (x) { return x.getKey() === visId; });
                    if (!isVoid(refreshedEntity)) {
                        vis.updateInstance(refreshedEntity);
                    }
                    else {
                        deletedEntities.push(vis);
                    }
                });
                deletedEntities.forEach(function (x) {
                    _this.model.visibleEntities.removeFirstElement(function (v) { return v.getKey() === x.getKey(); });
                    _this.model.selectedEntities.removeFirstElement(function (v) { return v.getKey() === x.getKey(); });
                });
                if (deletedEntities.length > 0 && _this.model.selectedEntities.length === 0 && _this.model.visibleEntities.length > 0) {
                    _this.model.selectedEntities.push(_this.model.visibleEntities[0]);
                }
                if (afterRefreshFunc !== undefined)
                    afterRefreshFunc();
            });
        };
        AbstractGroupController.prototype.getEntityName = function () {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:getEntityName");
        };
        AbstractGroupController.prototype.getChangesToCommit = function () {
            var self = this;
            var ret = [];
            _.each(self.model.newEntities, function (ch) {
                var changeToCommit = new ChangeToCommit(ChangeStatus.NEW, ch.b, self.getEntityName(), ch.a.toJSON());
                ret.push(changeToCommit);
            });
            _.each(self.model.updatedEntities, function (ch) {
                var changeToCommit = new ChangeToCommit(ChangeStatus.UPDATE, ch.b, self.getEntityName(), ch.a.toJSON());
                ret.push(changeToCommit);
            });
            _.each(self.model.deletedEntities, function (ch) {
                var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, ch.b, self.getEntityName(), ch.a.toJSON());
                ret.push(changeToCommit);
            });
            return ret;
        };
        AbstractGroupController.prototype.getDeleteChangesToCommit = function () {
            var self = this;
            var ret = [];
            _.each(self.model.deletedEntities, function (ch) {
                var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, ch.b, self.getEntityName(), ch.a.toJSON());
                ret.push(changeToCommit);
            });
            return ret;
        };
        Object.defineProperty(AbstractGroupController.prototype, "PageModel", {
            get: function () {
                throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:PageModel");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractGroupController.prototype, "ControllerClassName", {
            get: function () {
                throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:ControllerClassName");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractGroupController.prototype, "HtmlDivId", {
            get: function () {
                throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:HtmlDivId");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractGroupController.prototype, "Items", {
            get: function () {
                return undefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractGroupController.prototype, "FirstLevelChildGroups", {
            get: function () {
                return undefined;
            },
            enumerable: true,
            configurable: true
        });
        AbstractGroupController.prototype.validateGroup = function () {
            var self = this;
            var ret = [];
            self.Items.filter(function (itm) { return itm.unbounded; }).forEach(function (itm) {
                ret.push.apply(ret, itm.validateItem(self.$scope));
            });
            var updatedEntities = Object.keys(self.model.updatedEntities).map(function (key) { return self.model.updatedEntities[key]; });
            var validatedEntities = updatedEntities.concat(self.model.newEntities).map(function (x) { return x.a; });
            /* var visibleEntities =
                 self.model.visibleEntities.filter(vis => !validatedEntities.some(vl => vl.isEqual(vis)));
 
             validatedEntities = validatedEntities.concat(visibleEntities); */
            validatedEntities.forEach(function (ent) {
                ret.push.apply(ret, self.validateEntity(ent));
            });
            self.FirstLevelChildGroups.forEach(function (grp) {
                ret.push.apply(ret, grp.validateGroup());
            });
            return ret;
        };
        AbstractGroupController.prototype.validateEntity = function (ent) {
            var self = this;
            var ret = [];
            self.Items.filter(function (itm) { return !itm.unbounded; }).forEach(function (itm) {
                ret.push.apply(ret, itm.validateItem(self.$scope, ent));
            });
            return ret;
        };
        return AbstractGroupController;
    })(AbstractController);
    Controllers.AbstractGroupController = AbstractGroupController;
    (function (EditMode) {
        EditMode[EditMode["NEW"] = 0] = "NEW";
        EditMode[EditMode["UPDATE"] = 1] = "UPDATE";
    })(Controllers.EditMode || (Controllers.EditMode = {}));
    var EditMode = Controllers.EditMode;
    var AbstractGroupFormController = (function (_super) {
        __extends(AbstractGroupFormController, _super);
        function AbstractGroupFormController($scope, $http, $timeout, plato, model) {
            _super.call(this, $scope, $http, $timeout, plato, model);
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.plato = plato;
            this.model = model;
        }
        Object.defineProperty(AbstractGroupFormController.prototype, "Mode", {
            get: function () {
                var self = this;
                return self.model.newEntities.length === 1 ? EditMode.NEW : EditMode.UPDATE;
            },
            enumerable: true,
            configurable: true
        });
        AbstractGroupFormController.prototype.getChangesToCommitForDeletion = function () {
            var self = this;
            var ret = [];
            if (self.Mode === EditMode.UPDATE) {
                var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, Utils.getTimeInMS(), self.getEntityName(), self.model.selectedEntities[0]);
                ret.push(changeToCommit);
            }
            return ret;
        };
        AbstractGroupFormController.prototype.isVisible = function () { return true; };
        AbstractGroupFormController.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
        };
        AbstractGroupFormController.prototype.updateUI = function () {
            var self = this;
            if (!isVoid(this.model.selectedEntities[0])) {
                self.model.selectedEntities[0].refresh(self, function () { self.setCurrentFormPageBreadcrumpStepModel(); });
            }
        };
        AbstractGroupFormController.prototype.refreshVisibleEntities = function (newEntitiesIds, afterRefreshFunc) {
            var self = this;
            _super.prototype.refreshVisibleEntities.call(this, newEntitiesIds, function () {
                self.setCurrentFormPageBreadcrumpStepModel();
                if (afterRefreshFunc !== undefined)
                    afterRefreshFunc();
            });
        };
        return AbstractGroupFormController;
    })(AbstractGroupController);
    Controllers.AbstractGroupFormController = AbstractGroupFormController;
    var AbstractGroupTableController = (function (_super) {
        __extends(AbstractGroupTableController, _super);
        function AbstractGroupTableController($scope, $http, $timeout, plato, model, gridAttrs, isLov) {
            if (isLov === void 0) { isLov = false; }
            _super.call(this, $scope, $http, $timeout, plato, model);
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.plato = plato;
            this.model = model;
            this.checkedEntities = {};
            this.focusFirstCellYouCan = false;
            this.lastRequestParams = "";
            this.isLov = false;
            var self = this;
            var _rowHeight = 21;
            model.pagingOptions = {
                pageSizes: (gridAttrs.pageSize === 5 || gridAttrs.pageSize === 10 || gridAttrs.pageSize === 15) ? [5, 10, 15] : [5, 10, 15, gridAttrs.pageSize],
                pageSize: gridAttrs.pageSize,
                currentPage: 1
            };
            model.flexibleHeightPlugin = new ngGridFlexibleHeightPlugin({
                minHeight: 58 + 20 * gridAttrs.maxLinesInHeader,
                maxViewPortHeight: isVoid(gridAttrs.maxPageVisibleRows) ? undefined : _rowHeight * gridAttrs.maxPageVisibleRows
            });
            model.flexibleLayoutPlugin = new ngGridLayoutPlugin();
            $scope.focusHack = function (idx) {
                model.grid.selectRow(idx, true);
            };
            model.grid = {
                data: 'model.visibleEntities',
                enablePaging: true,
                pagingOptions: model.pagingOptions,
                maxPageVisibleRows: gridAttrs.maxPageVisibleRows,
                totalServerItems: 'model.totalItems',
                multiSelect: false,
                selectedItems: model.selectedEntities,
                showFooter: true,
                selectedRowsCount: function () {
                    return self.getCheckedEntitiesIds().length;
                },
                enableColumnResize: true,
                enableCellSelection: true,
                enableSorting: true,
                useExternalSorting: true,
                keepLastSelected: true,
                headerRowHeight: gridAttrs.maxLinesInHeader * 20 + 4,
                footerRowHeight: 31,
                rowHeight: _rowHeight,
                maintainColumnRatios: false,
                plugins: [model.flexibleHeightPlugin, model.flexibleLayoutPlugin],
                localeCodeParam: 'globals.globalLang',
                columnDefs: self.getGridColumnDefinitions(),
                totalItemsLabel: self.getTotalItemsLabel(),
                showTotalItems: true,
                updateTotalOnDemand: false,
                nInProgressCounts: function () {
                    return model.nInProgressCounts;
                },
                updateTotalCompleted: function () {
                    return model.updateTotalCompleted;
                },
                updateTotalItems: function (forceUpdate) {
                    return self.updateTotalItems(forceUpdate);
                }
            };
            self.inProgress = false;
            $scope.selectEntityCheckBoxClicked = function (x) {
                self.selectEntityCheckBoxClicked(x);
            };
            $scope.isEntitySelected = function (x) {
                return self.isEntitySelected(x);
            };
            $scope.unUselectActiveSetEntities = function () {
                self.unUselectActiveSetEntities();
            };
            $scope.selectActiveSetEntities = function () {
                self.selectActiveSetEntities();
            };
            $scope.isMultiSelect = function () {
                return self.isMultiSelect();
            };
            $scope.exportToExcelBtnAction = function () {
                self.exportToExcelBtnAction();
            };
            $scope.updateGrid = function () {
                return self.updateGrid(0, false, true);
            };
            $scope.gridTitle = function () {
                return self.gridTitle();
            };
            $scope.getColumnTooltip = function (fieldName) {
                return self.getColumnTooltip(fieldName);
            };
            $scope.setSelectedRow = function (idx) {
                $scope.model.grid.selectRow(idx, true);
                //$("#controllerCustomersRoot .ngViewport input")[0].focus();
            };
            $scope.$on('$destroy', ($scope.$watch('model.pagingOptions', function (newVal, oldVal) {
                var bChangedCurrentPage = (newVal.currentPage !== oldVal.currentPage);
                var bChangedPageSize = (newVal.pageSize !== oldVal.pageSize);
                var prevCurrentPage = self.model.pagingOptions.currentPage;
                if (bChangedPageSize) {
                    self.model.pagingOptions.currentPage = 1;
                }
                if (newVal !== oldVal && (bChangedCurrentPage || (bChangedPageSize && prevCurrentPage === self.model.pagingOptions.currentPage))) {
                    self.updateGrid();
                }
                $timeout(function () {
                    self.model.flexibleLayoutPlugin.updateGridLayout();
                }, 0);
            }, true)));
            if (isLov === false) {
                $scope.$on('$destroy', ($scope.$watch(function () {
                    return self.isVisible();
                }, function (newVal, oldVal) {
                    if (oldVal === false && newVal === true) {
                        self.updateGrid(0, true);
                    }
                }, false)));
            }
            $scope.$on('ngGridEventSorted', function (evt) {
                var cols = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    cols[_i - 1] = arguments[_i];
                }
                var col = cols[0];
                evt.preventDefault();
                evt.stopPropagation();
                if (($scope.model.sortField !== col.fields[0]) || ($scope.model.sortOrder !== col.directions[0])) {
                    $scope.model.sortField = col.fields[0];
                    $scope.model.sortOrder = col.directions[0];
                    $timeout(function () { self.updateUI(); }, 0);
                }
            });
            $scope.$on('$destroy', ($scope.$watch('modelNavigation.epochOfLastResize', function (newVal, oldVal) {
                self.updateLayout();
            })));
            $scope.$on('$destroy', function (evt) {
                var cols = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    cols[_i - 1] = arguments[_i];
                }
                model.grid.data = null;
                model.grid = null;
            });
            $scope.gridPageDown = function () {
                var canPageDown = false;
                if ($scope.model.totalItems >= $scope.model.visibleEntities.length) {
                    if ($scope.model.pagingOptions.currentPage <
                        Math.ceil($scope.model.totalItems /
                            $scope.model.pagingOptions.pageSize)) {
                        canPageDown = true;
                    }
                }
                else {
                    if ($scope.model.visibleEntities.length >= $scope.model.pagingOptions.pageSize)
                        canPageDown = true;
                }
                if (canPageDown)
                    $scope.model.pagingOptions.currentPage += 1;
            };
            $scope.gridPageUp = function () {
                if ($scope.model.pagingOptions.currentPage > 1) {
                    $scope.model.pagingOptions.currentPage -= 1;
                }
            };
        }
        AbstractGroupTableController.prototype.getGridHeaderCellTemplate = function (attrs) {
            /*********************************************/
            /* This is the Default Header Cell Template of ng-grid (https://github.com/angular-ui/ng-grid/wiki/Templating) */
            /*
            <div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{'cursor': col.cursor}" ng-class="{ 'ngSorted': !noSortVisible }">
                <div ng-click="col.sort($event)" ng-class="'colt' + col.index" class="ngHeaderText">{{col.displayName}}</div>
                <div class="ngSortButtonDown" ng-show="col.showSortButtonDown()"></div>
                <div class="ngSortButtonUp" ng-show="col.showSortButtonUp()"></div>
                <div class="ngSortPriority">{{col.sortPriority}}</div>
                <div ng-class="{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }" ng-click="togglePin(col)" ng-show="col.pinnable"></div>
            </div>
            <div ng-show="col.resizable" class="ngHeaderGrip" ng-click="col.gripClick($event)" ng-mousedown="col.gripOnMouseDown($event)"></div>
            */
            /**********************************************/
            var colTitle = isVoid(attrs.tooltip) ? '' : 'title="' + attrs.tooltip + '"';
            var headerCellTemplate = '<div ' + colTitle + ' class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{\'cursor\': col.cursor}" ng-class="{ \'ngSorted\': !noSortVisible }">' +
                '    <div ng-click="col.sort($event)" ng-class="col.requiredAsterisk ? \'colt\'+col.index + \' requiredAsterisk\' : \'colt\'+col.index" class="ngHeaderText" ng-bind-html-unsafe="{{col.displayName}}"></div>' +
                '    <div class="ngSortButtonDown" ng-show="col.showSortButtonDown()"></div>' +
                '    <div class="ngSortButtonUp" ng-show="col.showSortButtonUp()"></div>' +
                '    <div class="ngSortPriority">{{col.sortPriority}}</div>' +
                '    <div ng-class="{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }" ng-click="togglePin(col)" ng-show="col.pinnable"></div>' +
                '</div>' +
                '<div ng-show="col.resizable" class="ngHeaderGrip" ng-click="col.gripClick($event)" ng-mousedown="col.gripOnMouseDown($event)"></div>';
            return headerCellTemplate;
        };
        AbstractGroupTableController.prototype.getTotalItemsLabel = function () {
            return 'Records:';
        };
        AbstractGroupTableController.prototype.isMultiSelect = function () {
            return false;
        };
        AbstractGroupTableController.prototype.getCheckedEntitiesIds = function () {
            return Object.keys(this.checkedEntities);
        };
        AbstractGroupTableController.prototype.selectEntityCheckBoxClicked = function (x) {
            if (isVoid(x.getKey()))
                return;
            if (this.checkedEntities[x.getKey()] === undefined) {
                this.checkedEntities[x.getKey()] = true;
            }
            else {
                delete this.checkedEntities[x.getKey()];
            }
        };
        AbstractGroupTableController.prototype.isEntitySelected = function (x) {
            if (isVoid(x.getKey()))
                return false;
            return this.checkedEntities[x.getKey()] === true;
        };
        AbstractGroupTableController.prototype.unUselectActiveSetEntities = function () {
            this.getAllIds(false);
        };
        AbstractGroupTableController.prototype.selectActiveSetEntities = function () {
            this.getAllIds(true);
        };
        AbstractGroupTableController.prototype.getAllIds = function (bSelectAll) {
            var self = this;
            function doRequest() {
                var excludedIds = Object.keys(self.model.deletedEntities);
                self.makeWebRequestGetIds(excludedIds).
                    success(function (response, status, header, config) {
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    var allIds = response.data;
                    allIds.forEach(function (id) {
                        if (bSelectAll) {
                            self.checkedEntities[id] = true;
                        }
                        else {
                            delete self.checkedEntities[id];
                        }
                    });
                }).error(function (data, status, header, config) {
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.PageModel, data);
                    console.error("Error received in makeWebRequest:" + data);
                    console.dir(status);
                    console.dir(header);
                    console.dir(config);
                });
            }
            if (self.model.totalItems <= 10000) {
                doRequest();
            }
            else {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", self.dynamicMessage("GetAllIds_TotalItemsMsg(" + self.model.totalItems + ")"), IconKind.INFO, [new Tuple2("OK", function () { })], 0, 0);
            }
        };
        AbstractGroupTableController.prototype.getExcelFields = function () {
            var self = this;
            return self.getGridColumnDefinitions()
                .filter(function (col) { return col.field !== undefined; })
                .map(function (col) { return col.field; });
        };
        AbstractGroupTableController.prototype.getExcelFieldDefinitions = function () {
            var self = this;
            return self.getGridColumnDefinitions()
                .filter(function (col) { return col.field !== undefined; })
                .map(function (col) { return { 'field': col.field, 'displayName': Utils.replaceHtmlChars(self.$scope.$eval(col.displayName)) }; });
        };
        AbstractGroupTableController.prototype.exportToExcelBtnAction = function () {
        };
        AbstractGroupTableController.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
            if (bCascade === void 0) { bCascade = false; }
            if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
            _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction);
            if (triggerUpdate) {
                this.updateGrid(rowIndex);
            }
            else {
                this.model.visibleEntities.removeFirstElement(function (x) { return x.isEqual(ent); });
                this.model.selectedEntities.removeFirstElement(function (x) { return x.isEqual(ent); });
            }
            this.model.totalItems--;
        };
        AbstractGroupTableController.prototype.updateLayout = function () {
            var self = this;
            if (!self.isVisible())
                return;
            self.$timeout(function () {
                if (!self.isVisible())
                    return;
                // George investigated, and claims this call is superfluous:
                //self.$scope.model.flexibleLayoutPlugin.updateGridLayout();
                self.$scope.model.grid.ngGrid.refreshDomSizes();
                // The call to buildColumns() below resets the column widths to their
                // percentage-driven values.  If the user has resized a column,
                // this means he loses his resizing with any "+ New" he presses!
                // 
                // We don't want that. But even worse, the login in ng-grid
                // in line 1725 says this:
                //
                //      if (isNaN(t) && !$scope.hasUserChangedGridColumnWidths)
                //
                // ...which skips the percentage-driven column width calculation
                // altogether, if the user has resized! This means that ANY resize
                // causes a catastrophic collapse the next time updateLayout 
                // is called (because the percentage-driven columns are not processed!)
                //
                // Disabling the call to buildColumns, and hoping the change
                // doesn't break anything else (God willing).
                //
                //self.$scope.model.grid.ngGrid.buildColumns();
                self.$scope.model.grid.ngGrid.configureColumnWidths();
                self.$scope.model.flexibleLayoutPlugin.updateGridLayout();
                // $('#CustomerTable_ControllerGrpCustomer').find('input')[15].focus();
                self.gridRepainted();
            }, 0);
        };
        AbstractGroupTableController.prototype.selectAndFocusTheFirstCellYouCan = function (row) {
            if (isVoid(row) || row.length === 0)
                return;
            var columns = angular.element(row[0].children).filter(function () { return this.nodeType !== 8; }); //Remove html comments for IE8
            var i = 0;
            if (this.$scope.model.grid.ngGrid.config.showSelectionCheckbox && angular.element(columns[i]).scope() && angular.element(columns[i]).scope().col.index === 0) {
                i = 1; //don't want to focus on checkbox
            }
            // Skip over disabled/readonly columns
            var ngCellElement, domElemToWorkOn;
            var isDisabledOrReadOnly = function (elem) {
                return $(elem).is(":disabled") || $(elem).is("[readonly]");
            };
            while (i >= 0 && i < columns.length && columns[i]) {
                ngCellElement = columns[i].children[1].children[0];
                var candidateDomElements = $(ngCellElement).find('input,button,select,a');
                for (var j = 0; j < candidateDomElements.length; j++) {
                    var domElem = candidateDomElements[j];
                    if (!isDisabledOrReadOnly(domElem) && domElem.className !== "npGridDelete") {
                        domElemToWorkOn = domElem;
                        break;
                    }
                }
                if (domElemToWorkOn !== undefined)
                    break;
                else
                    i++;
            }
            if (domElemToWorkOn !== undefined) {
                $(domElemToWorkOn).focus();
                var isInput = $(domElemToWorkOn).tagName == "INPUT";
                if (isInput) {
                    $(domElemToWorkOn).select();
                }
            }
        };
        AbstractGroupTableController.prototype.gridRepainted = function () {
            if (this.focusFirstCellYouCan) {
                this.$scope.setSelectedRow(0);
                var controllerVisibleDiv = $('#' + this.HtmlDivId + ':visible');
                var row = controllerVisibleDiv.find('.ngRow').first();
                this.selectAndFocusTheFirstCellYouCan(row);
                this.focusFirstCellYouCan = false;
            }
        };
        AbstractGroupTableController.prototype.isVisible = function () {
            var controllerVisibleDiv = $('#' + this.HtmlDivId + ':visible');
            if (controllerVisibleDiv.length !== 1)
                return false;
            //controller is visible but it may be within  a tab which is under construction
            var parentTabControls = controllerVisibleDiv.parents('div.ui-tabs');
            if (parentTabControls.length === 0)
                return true; // not tab control, so controller is visible
            //every surrounding tab control must have the .ui-widget class
            var bSomeTabAreUnderConstruction = parentTabControls.not('.ui-widget').length > 0;
            if (bSomeTabAreUnderConstruction)
                return false;
            return true;
        };
        AbstractGroupTableController.prototype.getGridColumnDefinitions = function () {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:getGridColumnDefinitions");
        };
        AbstractGroupTableController.prototype.updateUI = function (rowIndexToSelect) {
            if (rowIndexToSelect === void 0) { rowIndexToSelect = 0; }
            this.updateGrid(rowIndexToSelect);
            this.lastRequestParams = "";
        };
        AbstractGroupTableController.prototype.gridTitle = function () {
            return "";
        };
        AbstractGroupTableController.prototype.getColumnTooltip = function (fieldName) {
            return "";
        };
        AbstractGroupTableController.prototype.updateTotalItems = function (forceUpdate) {
            var self = this;
            if (!self.model.grid.showTotalItems)
                return;
            var myNewEntities = self.model.newEntities.filter(function (e) { return self.belongsToCurrentParent(e.a); });
            if (self.model.grid.updateTotalOnDemand && (isVoid(forceUpdate) || !forceUpdate)) {
                self.model.totalItems = myNewEntities.length;
                return;
            }
            var excludedIds = Object.keys(self.model.deletedEntities);
            self.model.nInProgressCounts++;
            self.makeWebRequest_count(excludedIds).
                success(function (response, status, header, config) {
                if (self.model.nInProgressCounts > 0)
                    self.model.nInProgressCounts--;
                self.model.totalItems = response.count + myNewEntities.length;
                self.model.updateTotalCompleted = true;
            }).error(function (data, status, header, config) {
                if (self.model.nInProgressCounts > 0)
                    self.model.nInProgressCounts--;
                console.error("Error received in makeWebRequest_count:" + data);
            });
        };
        AbstractGroupTableController.prototype.updateGrid = function (rowIndexToSelect, bUpdateOnlyIfWsParamsAreDifferent, goToFirstPage) {
            if (rowIndexToSelect === void 0) { rowIndexToSelect = 0; }
            if (bUpdateOnlyIfWsParamsAreDifferent === void 0) { bUpdateOnlyIfWsParamsAreDifferent = false; }
            if (goToFirstPage === void 0) { goToFirstPage = false; }
            var self = this;
            //if (self.inProgress)
            //  return;
            if (!self.isLov && !self.isVisible())
                return;
            self.inProgress = true;
            if (goToFirstPage) {
                self.model.pagingOptions.currentPage = 1;
            }
            var fromRowIndex = self.model.pagingOptions.pageSize * (self.model.pagingOptions.currentPage - 1);
            var toRowIndex = self.model.pagingOptions.pageSize * self.model.pagingOptions.currentPage;
            var myNewEntities = self.model.newEntities.filter(function (e) { return self.belongsToCurrentParent(e.a); });
            var newRowsOnTop = myNewEntities.length;
            // Start the filling-in from the local myNewEntities container
            var newData = [];
            var idx = fromRowIndex;
            for (; idx < toRowIndex; idx++) {
                if (idx < newRowsOnTop) {
                    var ent = myNewEntities[idx].a;
                    newData.push(ent);
                }
                else {
                    break;
                }
            }
            if ((idx !== toRowIndex) && !self.ParentIsNewOrUndefined) {
                var excludedIds = Object.keys(self.model.deletedEntities);
                var requestParams = self.getWebRequestParamsAsString(idx - newRowsOnTop, toRowIndex - newRowsOnTop, excludedIds);
                var makeWebRequest = bUpdateOnlyIfWsParamsAreDifferent === false ? true : self.lastRequestParams !== requestParams;
                self.lastRequestParams = requestParams;
                if (makeWebRequest) {
                    self.model.updateTotalCompleted = false;
                    self.makeWebRequest(idx - newRowsOnTop, toRowIndex - newRowsOnTop, excludedIds).
                        success(function (response, status, header, config) {
                        if (self.$scope.globals.nInFlightRequests > 0)
                            self.$scope.globals.nInFlightRequests--;
                        var wrappedFetchedEntities = self.processEntities(self.getEntitiesFromJSON(response), false);
                        self.model.visibleEntities = newData.concat(wrappedFetchedEntities);
                        if (response.count != undefined && response.count == -1) {
                            self.updateTotalItems();
                        }
                        else {
                            self.model.totalItems = response.count + myNewEntities.length;
                            self.model.updateTotalCompleted = true;
                        }
                        self.updateGridFinalSteps(rowIndexToSelect);
                        self.onDataReceived();
                    }).error(function (data, status, header, config) {
                        if (self.$scope.globals.nInFlightRequests > 0)
                            self.$scope.globals.nInFlightRequests--;
                        NpTypes.AlertMessage.addDanger(self.PageModel, data);
                        console.error("Error received in makeWebRequest:" + data);
                        console.dir(status);
                        console.dir(header);
                        console.dir(config);
                        if (data === "NO_READ_ACCESS") {
                            self.$scope.navigateBackward();
                        }
                    });
                }
                else {
                    //self.updateGridFinalSteps(rowIndexToSelect);
                    self.updateLayout();
                }
            }
            else {
                // All data are from recently added entities!
                self.model.visibleEntities = newData;
                self.updateGridFinalSteps(rowIndexToSelect);
            }
        };
        AbstractGroupTableController.prototype.onDataReceived = function () {
        };
        Object.defineProperty(AbstractGroupTableController.prototype, "ParentIsNewOrUndefined", {
            //return true only in child groups and when the parent entity is new (i.e. not yet submitted to database)
            get: function () {
                return false;
            },
            enumerable: true,
            configurable: true
        });
        AbstractGroupTableController.prototype.belongsToCurrentParent = function (x) {
            return true;
        };
        AbstractGroupTableController.prototype.updateGridFinalSteps = function (rowIndexToSelect) {
            var self = this;
            self.inProgress = false;
            self.model.selectedEntities.splice(0);
            var targetRowIndex = -1;
            if (rowIndexToSelect < self.model.visibleEntities.length) {
                targetRowIndex = rowIndexToSelect;
            }
            else {
                if (rowIndexToSelect - 1 < self.model.visibleEntities.length && (rowIndexToSelect - 1) >= 0) {
                    targetRowIndex = rowIndexToSelect - 1;
                }
            }
            if (targetRowIndex !== -1) {
                self.model.selectedEntities.push(self.model.visibleEntities[targetRowIndex]);
            }
            // https://github.com/angular-ui/ng-grid/issues/539
            self.$timeout(function () {
                if (!self.isVisible())
                    return;
                if (targetRowIndex !== -1) {
                    var grid = self.$scope.model.grid.ngGrid;
                    grid.$viewport.scrollTop(grid.rowMap[Math.max(targetRowIndex - 3, 0)] * grid.config.rowHeight);
                }
                self.updateLayout();
            }, 0);
        };
        AbstractGroupTableController.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            return "#" + paramIndexFrom + "#" + paramIndexTo + "#" + excludedIds.join("#");
        };
        AbstractGroupTableController.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:makeWebRequest");
        };
        AbstractGroupTableController.prototype.makeWebRequest_count = function (excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:makeWebRequest_count");
        };
        AbstractGroupTableController.prototype.makeWebRequestGetIds = function (excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            throw "Not implemented function:makeWebRequest";
        };
        AbstractGroupTableController.prototype.processEntities = function (dbEntities, bMergeVisibleEntities) {
            // remove deleted
            var self = this;
            var filteredEntities = _.filter(dbEntities, function (e) {
                // Remove the ones we have already deleted locally
                return undefined === self.model.deletedEntities[e.getKey()];
            });
            // merge updated
            var mergedEntities = _.map(filteredEntities, function (e) {
                // Use the ones we have already updated locally
                if (undefined !== self.model.updatedEntities[e.getKey()])
                    return self.model.updatedEntities[e.getKey()].a;
                else
                    return e;
            });
            //merge also with visible
            var mergedEntities2 = bMergeVisibleEntities ?
                _.map(mergedEntities, function (e) {
                    var visible = self.model.visibleEntities.firstOrNull(function (x) { return x.isEqual(e); });
                    if (isVoid(visible))
                        return e;
                    else
                        return visible;
                })
                : mergedEntities;
            return mergedEntities2;
        };
        AbstractGroupTableController.prototype.cleanUpAfterSave = function () {
            _super.prototype.cleanUpAfterSave.call(this);
            this.lastRequestParams = "";
        };
        return AbstractGroupTableController;
    })(AbstractGroupController);
    Controllers.AbstractGroupTableController = AbstractGroupTableController;
    var AbstractLovModel = (function (_super) {
        __extends(AbstractLovModel, _super);
        function AbstractLovModel($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this.alertData = [];
            this.alerts = [];
            this.showSearchParams = true;
        }
        Object.defineProperty(AbstractLovModel.prototype, "dialogOptions", {
            get: function () {
                return this._dialogOptions;
            },
            set: function (vl) {
                this._dialogOptions = vl;
            },
            enumerable: true,
            configurable: true
        });
        return AbstractLovModel;
    })(AbstractGroupTableModel);
    Controllers.AbstractLovModel = AbstractLovModel;
    var LovController = (function (_super) {
        __extends(LovController, _super);
        function LovController($scope, $http, $timeout, Plato, model, gridAttrs) {
            _super.call(this, $scope, $http, $timeout, Plato, model, gridAttrs, true);
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            self.isLov = true;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            self.initializeModelWithPreviousValues();
            if (self.model.dialogOptions !== undefined) {
                self.model.grid.showTotalItems = self.model.dialogOptions.showTotalItems;
                self.model.grid.updateTotalOnDemand = self.model.dialogOptions.updateTotalOnDemand;
            }
            if (self.model.dialogOptions !== undefined && self.model.dialogOptions.onConstructLovController !== undefined) {
                self.model.dialogOptions.onConstructLovController(self);
            }
            $scope.closeDialog = function () {
                self.closeDialog();
            };
            $scope.openNewEntityDialog = function () {
                self.model.dialogOptions.openNewEntityDialog();
            };
        }
        LovController.prototype.onDataReceived = function () {
            this.$timeout(function () {
                //                $('#' + this.HtmlDivId).find('.ngRow').first().find('input').first().click();
                //                $('#' + this.HtmlDivId + ' .ngViewport').focus();
            }, 200);
        };
        Object.defineProperty(LovController.prototype, "PageModel", {
            get: function () {
                return this.model;
            },
            enumerable: true,
            configurable: true
        });
        LovController.prototype.isMultiSelect = function () {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.bMultiSelect;
        };
        LovController.prototype.initializeModelWithPreviousValues = function () {
        };
        LovController.prototype.closeDialog = function () {
            var self = this;
            var selectedEntity = self.model.selectedEntities[0];
            self.model.dialogOptions.jquiDialog.dialog("close");
            if (!self.isMultiSelect()) {
                self.model.dialogOptions.onSelect(selectedEntity);
            }
            else {
                if (isVoid(selectedEntity)) {
                    self.model.dialogOptions.onMultipleSelect([]);
                }
                else {
                    self.model.dialogOptions.onMultipleSelect(self.getCheckedEntitiesIds());
                }
            }
        };
        LovController.prototype.makeWebRequestGetIds = function (excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.makeWebRequestGetIds(self.model, []);
        };
        LovController.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.makeWebRequest(paramIndexFrom, paramIndexTo, self.model, []);
        };
        LovController.prototype.makeWebRequest_count = function (excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.makeWebRequest_count(self.model, []);
        };
        LovController.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            var lovRes = self.model.dialogOptions.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, self.model, []);
            return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + lovRes;
        };
        LovController.prototype.getGridColumnDefinitions = function () {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:getGridColumnDefinitions");
        };
        return LovController;
    })(AbstractGroupTableController);
    Controllers.LovController = LovController;
    /*
        Page and Unbounded Pages
    */
    var AbstractPageModel = (function () {
        //        formatReport: number = 1; //used by reports
        function AbstractPageModel($scope) {
            this.$scope = $scope;
            this.alertData = [];
            this.alerts = [];
            this.pageInfoIsVisible = false;
        }
        return AbstractPageModel;
    })();
    Controllers.AbstractPageModel = AbstractPageModel;
    var AbstractPageController = (function (_super) {
        __extends(AbstractPageController, _super);
        function AbstractPageController($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato);
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            // The timeout promise that destroys the error popovers after X milliseconds
            this.promise = undefined;
            // Enable periodic (1-second) calls to $scope.$digest()
            $scope.globals.bDigestHackEnabled = true;
            var self = this;
            $scope.pageModel = model;
            $scope.saveIsDisabled = function () { return self._saveIsDisabled(); };
            $scope.newIsDisabled = function () { return self._newIsDisabled(); };
            $scope.deleteIsDisabled = function (cur) { return self._deleteIsDisabled(); };
            $scope.cancelIsDisabled = function () { return self._cancelIsDisabled(); };
            $scope.showAuditInfoIsVisible = function () {
                return $scope.globals.hasPrivilege($scope.globals.appName + "_" + "ShowAuditInfo");
            };
            $scope.getPageTitle = function () { return self._getPageTitle(); };
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            if (self.shownAsDialog()) {
                $scope.globals.createNewTransactionLevel();
            }
            else {
                self.setupMainAppTemplate();
                Utils.UpdateMainWindowWidthAndMainButtonPlacement($scope);
                $scope.modelNavigation.onNavigation = function (actualNavigation) {
                    self.onPageUnload(actualNavigation);
                };
            }
            $scope.onCancelBtnAction = function () {
                self.onCancelBtnAction();
            };
            $scope.closeAlert = function (idx) {
                $scope.pageModel.alerts.splice(idx, 1);
            };
            $scope.onPageInfoBtnAction = function () {
                if (self.model.pageInfoIsVisible) {
                    self.model.pageInfoIsVisible = false;
                }
                else {
                    self.model.pageInfoIsVisible = true;
                }
            };
            $scope.closePageInfo = function () {
                self.model.pageInfoIsVisible = false;
            };
            $scope.onEscWhileInDialog = function () {
                if (!self.model.dialogOptions.showCloseButton)
                    return;
                self.onPageUnload(function () { self.closeDialog(); });
            };
            if (!self.shownAsDialog()) {
                $scope.modelNavigation.onLogout = function (realLogout) {
                    self.onPageUnload(function () { realLogout(); });
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    $scope.modelNavigation.onLogout = null;
                });
            }
            NpTypes.BaseEntity.activePageController = this;
            $scope.$on('$destroy', function (evt) {
                var cols = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    cols[_i - 1] = arguments[_i];
                }
                NpTypes.BaseEntity.activePageController = null;
            });
            $scope.$on('$destroy', ($scope.$watch(function () {
                return $scope.globals.globalLang;
            }, function (newVal, oldVal) {
                if (isVoid(oldVal) || isVoid(newVal)) {
                    return;
                }
                if (oldVal !== newVal) {
                    $scope.globals.onGlobalLangChange(self);
                }
            }, false)));
        }
        AbstractPageController.prototype.setupMainAppTemplate = function () {
            this.$scope.globals.mainAppTemplate.showAll(true);
        };
        AbstractPageController.prototype.onPageUnload = function (actualNavigation) {
            var self = this;
            if (self.shownAsDialog()) {
                var errors = self.validatePage();
                if (errors.length > 0) {
                    NpTypes.AlertMessage.clearAlerts(self.model);
                    NpTypes.AlertMessage.addDanger(self.model, errors.join('<br>'));
                }
                else {
                    actualNavigation(self.$scope);
                }
            }
            else {
                actualNavigation(self.$scope);
            }
        };
        AbstractPageController.prototype._saveIsDisabled = function () {
            return false;
        };
        AbstractPageController.prototype._newIsDisabled = function () {
            return false;
        };
        AbstractPageController.prototype._deleteIsDisabled = function () {
            return false;
        };
        AbstractPageController.prototype._cancelIsDisabled = function () {
            return false;
        };
        AbstractPageController.prototype._getPageTitle = function () {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:_getPageTitle");
        };
        AbstractPageController.prototype.onCancelBtnAction = function () {
            var self = this;
            if (self.shownAsDialog()) {
                self.onPageUnload(function () { self.closeDialog(); });
            }
            else {
                self.$scope.navigateBackward();
            }
        };
        AbstractPageController.prototype.closeDialog = function () {
            var self = this;
            self.model.dialogOptions.jquiDialog.dialog("close");
            self.$scope.globals.dropLastTransactionLevel();
        };
        AbstractPageController.prototype.shownAsDialog = function () {
            return !isVoid(this.model.dialogOptions);
        };
        AbstractPageController.prototype.dialogSelectedEntity = function () {
            if (this.shownAsDialog() && !isVoid(this.model.dialogOptions.customParams)) {
                return this.model.dialogOptions.customParams.selectedEntity;
            }
            return null;
        };
        /*
        works only if the dirty = false
        */
        AbstractPageController.prototype.refresh = function () {
            if (!this.$scope.globals.isCurrentTransactionDirty) {
                this.refreshGroups([]);
            }
        };
        AbstractPageController.prototype.refreshGroups = function (newEntitiesIds) {
        };
        AbstractPageController.prototype.refreshVisibleEntities = function (newEntitiesIds) {
        };
        Object.defineProperty(AbstractPageController.prototype, "SaveBtnJQueryHandler", {
            get: function () {
                return $('#saveBtn_id');
            },
            enumerable: true,
            configurable: true
        });
        AbstractPageController.prototype.onSuccesfullSaveFromButton = function (response) {
            var self = this;
            var msg = self.dynamicMessage("EntitySuccessSave");
            var jqSaveBtn = self.SaveBtnJQueryHandler;
            self.showPopover(jqSaveBtn, msg);
        };
        AbstractPageController.prototype.onSuccesfullSave = function (response) {
        };
        AbstractPageController.prototype.showPopover = function (element, msg, warning) {
            if (warning === void 0) { warning = false; }
            var self = this;
            var popOverClass = warning ? "warningPopover" : "infoPopover";
            var htmlMsg = Sprintf.sprintf("<div class='%s'>%s</div>", popOverClass, msg);
            var container = "body"; //append on body by default
            if (NpGeoGlobals.fullScreenState === true) {
                container = '#NpMainContent';
            }
            // Is there an already visible popover?
            if (self.promise === undefined) {
                // No, so go ahead and show this new one then!
                element.popover({
                    animation: true, html: true, placement: 'bottom', trigger: 'manual', content: htmlMsg, container: container
                }).popover('show');
                // Schedule a popover destroying after 3000ms, and store it in .promise
                self.promise = self.$timeout(function () {
                    element.popover('destroy');
                    self.promise = undefined;
                }, 3000);
            }
        };
        AbstractPageController.showFormAsDialog = function (className, Plato, callerScope, partialFileName, title, showCloseButton, width, customParams) {
            if (showCloseButton === void 0) { showCloseButton = true; }
            if (width === void 0) { width = '45em'; }
            var dialogOptions = new NpTypes.DialogOptions();
            dialogOptions.title = title;
            dialogOptions.width = width;
            dialogOptions.showCloseButton = showCloseButton;
            dialogOptions.className = className;
            dialogOptions.customParams = customParams;
            Plato.showDialog(callerScope, dialogOptions.title, "/" + callerScope.globals.appName + '/partials/' + partialFileName + '?rev=' + callerScope.globals.version, dialogOptions);
        };
        Object.defineProperty(AbstractPageController.prototype, "PageModel", {
            get: function () {
                return this.model;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractPageController.prototype, "FirstLevelGroupControllers", {
            get: function () {
                return [];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractPageController.prototype, "AllGroupControllers", {
            get: function () {
                return [];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractPageController.prototype, "Items", {
            get: function () {
                return [];
            },
            enumerable: true,
            configurable: true
        });
        AbstractPageController.prototype.validatePage = function () {
            var self = this;
            var ret = [];
            self.Items.forEach(function (itm) {
                ret.push.apply(ret, itm.validateItem(self.$scope));
            });
            self.FirstLevelGroupControllers.forEach(function (grpCtr) {
                ret.push.apply(ret, grpCtr.validateGroup());
            });
            return ret;
        };
        AbstractPageController.prototype.getLabelById = function (itmId) {
            if (this.shownAsDialog() && itmId === 'cancelBtn_id') {
                return "OK";
            }
            return _super.prototype.getLabelById.call(this, itmId);
        };
        return AbstractPageController;
    })(AbstractController);
    Controllers.AbstractPageController = AbstractPageController;
    var AbstractSearchPageModel = (function (_super) {
        __extends(AbstractSearchPageModel, _super);
        function AbstractSearchPageModel($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this.alertData = [];
            this.alerts = [];
            this.newBtnPressed = false;
        }
        return AbstractSearchPageModel;
    })(AbstractGroupTableModel);
    Controllers.AbstractSearchPageModel = AbstractSearchPageModel;
    var AbstractSearchPageController = (function (_super) {
        __extends(AbstractSearchPageController, _super);
        function AbstractSearchPageController($scope, $http, $timeout, Plato, model, gridAttrs) {
            _super.call(this, $scope, $http, $timeout, Plato, model, gridAttrs);
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            $scope.newIsDisabled = function () { return self._newIsDisabled(); };
            $scope.deleteIsDisabled = function (ent) { return self._deleteIsDisabled(ent); };
            $scope.searchIsDisabled = function () { return self._searchIsDisabled(); };
            $scope.editBtnIsDisabled = function (ent) { return self._editBtnIsDisabled(ent); };
            $scope.getPageTitle = function () { return self._getPageTitle(); };
            $scope.modelNavigation.onNavigation = undefined;
            $scope.pageModel = model;
            $scope.onCancelBtnAction = function () {
                self.onCancelBtnAction();
            };
            $scope.closeAlert = function (idx) {
                $scope.pageModel.alerts.splice(idx, 1);
            };
            $scope.searchBtnAction = function () {
                self.updateGrid(0, false, true);
            };
            $scope.modelNavigation.onNavigation = function (actualNavigation) {
                self.onPageUnload(actualNavigation);
            };
            self.setupMainAppTemplate();
            Utils.UpdateMainWindowWidthAndMainButtonPlacement($scope);
            NpTypes.BaseEntity.activePageController = this;
            $scope.$on('$destroy', function (evt) {
                var cols = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    cols[_i - 1] = arguments[_i];
                }
                NpTypes.BaseEntity.activePageController = null;
            });
            $scope.$on('$destroy', ($scope.$watch(function () {
                return $scope.globals.globalLang;
            }, function (newVal, oldVal) {
                if (isVoid(oldVal) || isVoid(newVal)) {
                    return;
                }
                if (oldVal !== newVal) {
                    $scope.globals.onGlobalLangChange(self);
                }
            }, false)));
        }
        AbstractSearchPageController.prototype._searchIsDisabled = function () {
            return false;
        };
        AbstractSearchPageController.prototype._editBtnIsDisabled = function (ent) {
            return false;
        };
        AbstractSearchPageController.prototype._getPageTitle = function () {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:_getPageTitle");
        };
        AbstractSearchPageController.prototype.setupMainAppTemplate = function () {
            this.$scope.globals.mainAppTemplate.showAll(true);
        };
        AbstractSearchPageController.prototype.onPageUnload = function (actualNavigation) {
            var self = this;
            actualNavigation(self.$scope);
        };
        AbstractSearchPageController.prototype.onCancelBtnAction = function () {
            var self = this;
            self.$scope.navigateBackward();
        };
        AbstractSearchPageController.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
            if (bCascade === void 0) { bCascade = false; }
            if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", self.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(ent); }),
                new Tuple2("MessageBox_Button_No", function () { })
            ], 1, 1);
        };
        AbstractSearchPageController.prototype.deleteRecord = function (x) {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:deleteRecord");
        };
        Object.defineProperty(AbstractSearchPageController.prototype, "PageModel", {
            get: function () {
                return this.model;
            },
            enumerable: true,
            configurable: true
        });
        AbstractSearchPageController.prototype.refresh = function () {
            this.updateUI();
        };
        return AbstractSearchPageController;
    })(AbstractGroupTableController);
    Controllers.AbstractSearchPageController = AbstractSearchPageController;
    var AbstractSqlLayerFilterModel = (function (_super) {
        __extends(AbstractSqlLayerFilterModel, _super);
        function AbstractSqlLayerFilterModel($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._geoFunc = new NpTypes.UIStringModel(undefined);
            this._geoFuncParam = new NpTypes.UIStringModel(undefined);
            this.alertData = [];
            this.alerts = [];
            this.showSearchParams = true;
        }
        Object.defineProperty(AbstractSqlLayerFilterModel.prototype, "geoFunc", {
            get: function () {
                return this._geoFunc.value;
            },
            set: function (vl) {
                this._geoFunc.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractSqlLayerFilterModel.prototype, "geoFuncParam", {
            get: function () {
                return this._geoFuncParam.value;
            },
            set: function (vl) {
                this._geoFuncParam.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AbstractSqlLayerFilterModel.prototype, "dialogOptions", {
            get: function () {
                return this._dialogOptions;
            },
            set: function (vl) {
                this._dialogOptions = vl;
            },
            enumerable: true,
            configurable: true
        });
        AbstractSqlLayerFilterModel.prototype.getPersistedModel = function () {
            var ret = _super.prototype.getPersistedModel.call(this);
            var a = undefined;
            ret['geoFunc'] = this.geoFunc;
            ret['geoFuncParam'] = this.geoFuncParam;
            return ret;
        };
        AbstractSqlLayerFilterModel.prototype.setPersistedModel = function (vl) {
            _super.prototype.setPersistedModel.call(this, vl);
            this.geoFunc = vl['geoFunc'];
            this.geoFuncParam = vl['geoFuncParam'];
        };
        return AbstractSqlLayerFilterModel;
    })(AbstractGroupTableModel);
    Controllers.AbstractSqlLayerFilterModel = AbstractSqlLayerFilterModel;
    var LabelValue = (function () {
        function LabelValue(label, value, isPrmDisabled) {
            this.label = label;
            this.value = value;
            this.isPrmDisabled = isPrmDisabled;
        }
        return LabelValue;
    })();
    Controllers.LabelValue = LabelValue;
    var SqlLayerFilterController = (function (_super) {
        __extends(SqlLayerFilterController, _super);
        function SqlLayerFilterController($scope, $http, $timeout, Plato, model, gridAttrs) {
            var _this = this;
            _super.call(this, $scope, $http, $timeout, Plato, model, gridAttrs, true);
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            this.spatialFuncs = [
                new LabelValue("Επιλέξτε", null, function () { return true; }),
                new LabelValue("Απέχουν έως", "WITHIN_DISTANCE", function () { return false; }),
                //            new LabelValue("Είναι πιο κοντά", "NN", () => true),
                new LabelValue("Εμπεριέχονται", "INSIDE", function () { return true; }),
                //            new LabelValue("Εμπεριέχει", "CONTAINS", () => true),
                new LabelValue("Ακουμπάνε", "TOUCH", function () { return true; })
            ];
            this.coordinateSystem = 'EPSG:2100';
            var self = this;
            self.isLov = true;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            self.initializeModelWithPreviousValues();
            $scope.closeDialog = function () {
                self.closeDialog();
            };
            $scope.onApplyNewFilter = function () {
                self.onApplyNewFilter();
            };
            $scope.onClearFilters = function () {
                self.onClearFilters();
            };
            $scope.onGeoFuncChanged = function () {
                self.onGeoFuncChanged();
            };
            $scope.GEO_FUNC_disabled = function () {
                return self.GEO_FUNC_disabled();
            };
            $scope.isGeoFuncInvisible = function () {
                return self.isGeoFuncInvisible();
            };
            $scope.GEO_FUNC_PRM_disabled = function () {
                return self.GEO_FUNC_PRM_disabled();
            };
            $scope.isGeoFuncParamInvisible = function () {
                return self.isGeoFuncParamInvisible();
            };
            $scope.getGeoFunList = function () {
                return _this.spatialFuncs;
            };
            $scope.$on('$destroy', ($scope.$watch('model.selectedEntities[0]', function () {
                self.onRowSelect();
            }, false)));
            $scope.showSpatialSearchParams = function () {
                if (isVoid(self.model.dialogOptions))
                    return false;
                return self.model.dialogOptions.showSpatialSearchParams === true;
            };
            $scope.onExportGeoJsonFile = function () {
                self.onExportGeoJsonFile();
            };
            $scope.onExportExcel = function () {
                self.onExportExcel();
            };
        }
        Object.defineProperty(SqlLayerFilterController.prototype, "ControllerClassName", {
            get: function () {
                return SqlLayerFilterController._ControllerClassName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SqlLayerFilterController.prototype, "PageModel", {
            /*
            public onDataReceived() {
                var self = this;
                if (self.model.dialogOptions === undefined) {
                    self.model.dialogOptions = <NpTypes.SFLDialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
                }
                this.model.dialogOptions.onPageDataReceived(<NpGeo.SqlLayerEntity[]>this.model.visibleEntities);
            }
            */
            get: function () {
                return this.model;
            },
            enumerable: true,
            configurable: true
        });
        SqlLayerFilterController.prototype.onRowSelect = function () {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            var row = (self.model.selectedEntities.length > 0 ? self.model.selectedEntities[0] : undefined);
            this.model.dialogOptions.onRowSelect(row);
        };
        SqlLayerFilterController.prototype.initializeModelWithPreviousValues = function () {
        };
        SqlLayerFilterController.prototype.closeDialog = function () {
            var self = this;
            var selectedEntity = self.model.selectedEntities[0];
            self.model.dialogOptions.jquiDialog.dialog("close");
        };
        SqlLayerFilterController.prototype.onApplyNewFilter = function () {
            var self = this;
            self.model.dialogOptions.jquiDialog.dialog("close");
            self.model.dialogOptions.onApplyNewFilter(self.model);
        };
        SqlLayerFilterController.prototype.onClearFilters = function () {
            var self = this;
            self.model.dialogOptions.jquiDialog.dialog("close");
            self.model.dialogOptions.onClearFilters();
        };
        SqlLayerFilterController.prototype.getEntitiesFromJSON = function (webResponse) {
            var self = this;
            var ret = _.map(webResponse.data, function (sqlEntityData) {
                return new NpGeoLayers.SqlLayerEntity(sqlEntityData, self.coordinateSystem);
            });
            return ret;
        };
        SqlLayerFilterController.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.makeWebRequest(paramIndexFrom, paramIndexTo, self.model, []);
        };
        SqlLayerFilterController.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            var lovRes = self.model.dialogOptions.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, self.model, []);
            return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + lovRes;
        };
        /*
        public makeWebRequestGetIds(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.DialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.makeWebRequestGetIds(self.model, []);
        }
        */
        /*


        */
        SqlLayerFilterController.prototype.getGridColumnDefinitions = function () {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:getGridColumnDefinitions");
        };
        SqlLayerFilterController.prototype.onGeoFuncChanged = function () {
        };
        SqlLayerFilterController.prototype.GEO_FUNC_disabled = function () {
            var self = this;
            if (isVoid(self.model.dialogOptions.selectedGeometry))
                return true;
            return false;
        };
        SqlLayerFilterController.prototype.isGeoFuncInvisible = function () {
            return false;
        };
        SqlLayerFilterController.prototype.GEO_FUNC_PRM_disabled = function () {
            var self = this;
            if (isVoid(self.model.dialogOptions.selectedGeometry))
                return true;
            var lb = self.spatialFuncs.firstOrNull(function (x) { return x.value === self.model.geoFunc; });
            if (isVoid(lb)) {
                return true;
            }
            return lb.isPrmDisabled();
        };
        SqlLayerFilterController.prototype.isGeoFuncParamInvisible = function () {
            return false;
        };
        //                    <select name='GEO_FUNC' data-ng-model='modelLovSLF0.geoFunc' data-np-ui-model='modelLovSLF0._geoFunc' data-ng-change='onGeoFuncChanged()' data-ng-show='!isGeoFuncInvisible()' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:&#39;Επιλέξτε&#39;, value:null}, {label:&#39;Είναι σε απόσταση έως&#39;, value:&#39;WITHIN_DISTANCE&#39;}, {label:&#39;Είναι πιο κοντά&#39;, value:&#39;NN&#39;}, {label:&#39;Εμπεριέχει&#39;, value:&#39;RELATE_INSIDE&#39;}, {label:&#39;Ακουμπά&#39;, value:&#39;RELATE_TOUCH&#39;}]' data-ng-disabled='GEO_FUNC_disabled()' />
        SqlLayerFilterController.prototype.onExportGeoJsonFile = function () {
            this.model.dialogOptions.ExportGeoJsonFile(this.model);
        };
        SqlLayerFilterController.prototype.onExportExcel = function () {
            this.model.dialogOptions.ExportExcel(this.model);
        };
        SqlLayerFilterController._ControllerClassName = "SqlLayerFilterController";
        return SqlLayerFilterController;
    })(AbstractGroupTableController);
    Controllers.SqlLayerFilterController = SqlLayerFilterController;
    var DynamicSqlLayerFilterController = (function (_super) {
        __extends(DynamicSqlLayerFilterController, _super);
        function DynamicSqlLayerFilterController($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new AbstractSqlLayerFilterModel($scope), { pageSize: 10, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            var self = this;
            self.updateUI();
        }
        Object.defineProperty(DynamicSqlLayerFilterController.prototype, "ControllerClassName", {
            get: function () {
                return DynamicSqlLayerFilterController._ControllerClassName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DynamicSqlLayerFilterController.prototype, "HtmlDivId", {
            get: function () {
                return "DynamicSqlLayerFilterController_id";
            },
            enumerable: true,
            configurable: true
        });
        DynamicSqlLayerFilterController.prototype.getGridColumnDefinitions = function () {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName);
            }
            var ret = self.model.dialogOptions.dynInfo.fields.
                filter(function (f) { return f.field.toLowerCase() !== "geom"; }).
                map(function (f) {
                return {
                    cellClass: 'cellToolTip', field: f.field.toLowerCase(), displayName: 'getALString("' + f.label + '", true)', resizable: true, sortable: false, enableCellEdit: false, width: f.width == undefined ? '14%' : f.width, cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()'> <label data-ng-bind='row.entity.data." + f.field.toLowerCase() + "' /> </div>"
                };
            });
            ret.push({
                width: '1',
                cellTemplate: '<div></div>',
                cellClass: undefined,
                field: undefined,
                displayName: undefined,
                resizable: undefined,
                sortable: undefined,
                enableCellEdit: undefined
            });
            return ret;
        };
        DynamicSqlLayerFilterController._ControllerClassName = "DynamicSqlLayerFilterController";
        return DynamicSqlLayerFilterController;
    })(SqlLayerFilterController);
    Controllers.DynamicSqlLayerFilterController = DynamicSqlLayerFilterController;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=BaseController.js.map