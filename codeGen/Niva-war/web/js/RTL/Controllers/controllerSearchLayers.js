/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    var LayerSearchState = (function () {
        function LayerSearchState(sqlLayer) {
            this.sqlLayer = sqlLayer;
            this.id = LayerSearchState.cntOptions++;
            this.label = sqlLayer.label;
        }
        LayerSearchState.cntOptions = 0;
        return LayerSearchState;
    })();
    Controllers.LayerSearchState = LayerSearchState;
    var LayerSearchModel = (function () {
        function LayerSearchModel() {
            this.inputs = [];
            this.outputs = [];
        }
        return LayerSearchModel;
    })();
    Controllers.LayerSearchModel = LayerSearchModel;
    var controllerSearchLayers = (function () {
        function controllerSearchLayers($scope, $timeout, Plato) {
            this.$scope = $scope;
            this.Plato = Plato;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("ChooseSearchLayersDialog");
            var self = this;
            var npMapThis = dialogOptions.npMapThis;
            $scope.modelSearch = new LayerSearchModel();
            var getVectorLayers = function (isSearch) {
                return npMapThis.layers.
                    filter(function (l) { return l instanceof NpGeoLayers.SqlVectorLayer; }).
                    filter(function (l) {
                    if (isSearch === false) {
                        return l.isSearch === isSearch && !isVoid(l.searchControllerClassName);
                    }
                    else {
                        return l.isSearch === isSearch;
                    }
                }).
                    map(function (l) {
                    return new LayerSearchState(l);
                });
            };
            $scope.modelSearch.inputs = getVectorLayers(false);
            $scope.modelSearch.outputs = getVectorLayers(true);
            if ($scope.modelSearch.inputs.length === 0 || $scope.modelSearch.outputs.length === 0) {
                var msg = ($scope.modelSearch.inputs.length === 0) ? "προς αναζήτηση." : "αποτελεσμάτων.";
                messageBox($scope, self.Plato, "Δεν είναι δυνατή η αναζήτηση...", "Δεν υπάρχουν θεματικά επίπεδα " + msg, IconKind.ERROR, [
                    new Tuple2("OK", function () {
                    })
                ], 0, 0, '22em');
                $timeout(function () {
                    dialogOptions.jquiDialog.dialog("close");
                }, 10);
                return;
            }
            $scope.modelSearch.searchLayer = $scope.modelSearch.inputs[0];
            $scope.modelSearch.resultLayer = $scope.modelSearch.outputs[0];
            $scope.doTheWork = function () {
                dialogOptions.jquiDialog.dialog("close");
                var onOk;
                onOk = dialogOptions.onOk;
                onOk($scope.modelSearch.resultLayer.sqlLayer, $scope.modelSearch.searchLayer.sqlLayer);
            };
        }
        return controllerSearchLayers;
    })();
    Controllers.controllerSearchLayers = controllerSearchLayers;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerSearchLayers.js.map