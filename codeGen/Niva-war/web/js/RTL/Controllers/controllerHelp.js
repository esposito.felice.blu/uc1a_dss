/// <reference path="../npTypes.ts" />
var Controllers;
(function (Controllers) {
    var controllerHelp = (function () {
        function controllerHelp($scope) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("HelpClass");
            $scope.closeHelp = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
        }
        return controllerHelp;
    })();
    Controllers.controllerHelp = controllerHelp;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerHelp.js.map