/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../services.ts" />
/// <reference path="../npTypes.ts" />
var Controllers;
(function (Controllers) {
    var controllerMain = (function () {
        function controllerMain($scope, $cookies, $http, $timeout, $window, LoginService, NavigationService) {
            this.$scope = $scope;
            this.$cookies = $cookies;
            this.$http = $http;
            this.$timeout = $timeout;
            this.$window = $window;
            this.LoginService = LoginService;
            this.NavigationService = NavigationService;
            //new
            $scope.getNpHeaderTemplate = function () {
                return $scope.globals.mainAppTemplate.showNpHeader ? $scope.globals.mainAppTemplate.npHeaderPartialUrl : undefined;
            };
            $scope.getNpBreadcrumbTemplate = function () {
                return LoginService.isLoggedIn() && $scope.globals.mainAppTemplate.showNpBreadcrumb ? $scope.globals.mainAppTemplate.npBreadcrumbUrl : undefined;
            };
            $scope.showNpMenu = function () {
                return LoginService.isLoggedIn() && $scope.globals.mainAppTemplate.showNpMenu;
            };
            $scope.showNpFooter = function () {
                return $scope.globals.mainAppTemplate.showNpFooter;
            };
            $scope.showVersion = function () {
                return $scope.globals.mainAppTemplate.showVersion;
            };
            // set globalLang and add watchers
            var globalLangCookiekey = 'allApps' + '-globalLang';
            if ($scope.globals.languages.length > 1) {
                if (supports_html5_storage() && isVoid($cookies[globalLangCookiekey])) {
                    $cookies[globalLangCookiekey] = localStorage[globalLangCookiekey];
                }
                if (isVoid($cookies[globalLangCookiekey])) {
                    $cookies[globalLangCookiekey] = $scope.globals.languages[0];
                }
                else {
                    // check if lang exists in current project
                    var bLangExists = !isVoid($scope.globals.languages.firstOrNull(function (l) { return l === $cookies[globalLangCookiekey]; }));
                    // if not exists set default language
                    if (!bLangExists)
                        $cookies[globalLangCookiekey] = $scope.globals.languages[0];
                }
                $scope.globals.globalLang = $cookies[globalLangCookiekey];
                $scope.setActiveLangByGlobalLang();
                $scope.$watch(function () { return $scope.globals.globalLang; }, function (newVal, oldVal) {
                    if (isVoid(newVal)) {
                        return;
                    }
                    if (oldVal !== newVal) {
                        $cookies[globalLangCookiekey] = newVal;
                    }
                }, false);
                $scope.$watch(function () { return $cookies[globalLangCookiekey]; }, function (newVal, oldVal) {
                    if (isVoid(newVal)) {
                        return;
                    }
                    if (oldVal !== newVal) {
                        $scope.globals.globalLang = newVal;
                        $scope.setActiveLangByGlobalLang();
                        $.datepicker.setDefaults($.datepicker.regional[$scope.globals.globalLang]);
                    }
                }, false);
            }
            else {
                $scope.globals.globalLang = $scope.globals.languages[0];
                $scope.setActiveLangByGlobalLang();
            }
            $.datepicker.setDefaults($.datepicker.regional[$scope.globals.globalLang]);
            // add some watchers
            //From ControllerLogin
            $scope.$watch('globals.nInFlightRequests', function (newValue, oldValue) {
                if (newValue === 0) {
                    $("html").removeClass("wait");
                }
                else {
                    $("html").addClass("wait");
                }
            });
            //new watchers for login
            $scope.$watch(function () { return $cookies[LoginService.sessionCookieKey]; }, function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    if (!isVoid(newValue) && isVoid(oldValue)) {
                        // Just LoggedIn
                        LoginService.ssoDeleted = false;
                        LoginService.afterLoginInit();
                        if (NavigationService.$location.path() === '/login') {
                            $timeout(function () {
                                NavigationService.goToAfterLoginPage();
                            }, 0);
                        }
                    }
                    if (isVoid(newValue) && !isVoid(oldValue)) {
                        // Session Cookie deleted
                        LoginService.deleteSubsCodeCookie();
                        $timeout(function () {
                            NavigationService.goToNotLoggedInPage();
                            // reload (to clear up Javascript completely - globals, etc)
                            LoginService.finalReload();
                        }, 0);
                    }
                    if (!isVoid(newValue) && !isVoid(oldValue)) {
                        LoginService.onSessionChanged();
                    }
                }
            });
            $scope.$watch(function () { return $cookies[LoginService.ssoCookieKey]; }, function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    if (!isVoid(newValue)) {
                        // A New SsoSession created. Delete previous app Session if exists (with logout)
                        // On reload controllerMain will make login via Sso
                        if (LoginService.isLoggedIn()) {
                            LoginService.sessionBelongToSso().
                                then(
                            // if sessionId not belong to ssoSession logout
                            function (belongToSso) {
                                if (isVoid(belongToSso) || !belongToSso) {
                                    LoginService.logout(true);
                                }
                                ;
                            }, 
                            // if error
                            function (reason) { LoginService.logout(true); });
                        }
                        else {
                            LoginService.loginSso(function () { });
                        }
                    }
                    if (isVoid(newValue) && !isVoid(oldValue)) {
                        // SsoSession terminated
                        LoginService.ssoDeleted = true; // we need this in finalreload() which called after logout (because of sessionCoockie deletion)
                        // terminate app Session
                        if (LoginService.isLoggedIn()) {
                            LoginService.logout(true);
                        }
                    }
                }
            });
            // From ControllerNavigation
            var resizableOptions = {
                handles: 'e',
                minWidth: 150,
                maxWidth: 350
            };
            resizableOptions["resize"] = function () {
                Utils.UpdateMainWindowWidthAndMainButtonPlacement($scope);
            };
            $(".resizable1").resizable(resizableOptions);
            $(window).resize(function () {
                Utils.UpdateMainWindowWidthAndMainButtonPlacement($scope);
            });
            $window.onbeforeunload = function () {
                if ($scope.globals.bLoggingOut === true)
                    return undefined;
                if (LoginService.isLoggedIn()) {
                    var msg = $scope.globals.getDynamicMessage("OnbeforeunloadMsg");
                    return msg;
                }
                else
                    return undefined;
            };
            $window.addEventListener('unload', function () {
                if (LoginService.isLoggedIn()) {
                    if (supports_html5_storage()) {
                        sessionStorage[NavigationService.strResetVar] = true;
                    }
                    if (LoginService.logoutOnUnload) {
                        LoginService.logout(true);
                    }
                }
                if (!isVoid($cookies[globalLangCookiekey]) && supports_html5_storage()) {
                    localStorage[globalLangCookiekey] = $cookies[globalLangCookiekey];
                }
            });
            $scope.globals.initApp($scope, NavigationService, LoginService, $http, $timeout, function () {
                // Check if LoggedIn and if so Initialize app
                if (LoginService.isLoggedIn()) {
                    LoginService.afterLoginInit();
                }
                else if (LoginService.existsSso()) {
                    if (supports_html5_storage() && sessionStorage[NavigationService.strResetVar] && sessionStorage[NavigationService.strResetVar] === "true") {
                        sessionStorage[NavigationService.strResetVar] = false;
                        NavigationService.assignLocation("partials/logout.html");
                    }
                    else {
                        LoginService.loginSso(function () { });
                    }
                }
                else {
                    if (supports_html5_storage() && sessionStorage[NavigationService.strResetVar] && sessionStorage[NavigationService.strResetVar] === "true") {
                        sessionStorage[NavigationService.strResetVar] = false;
                        NavigationService.go(NavigationService.modelNavigation.notLoggedInPagePath);
                    }
                }
            });
            var checkForNewBannerMain = function () {
                if (!LoginService.isLoggedIn())
                    return;
                var url = "/" + $scope.globals.appName + "/rest/Menu/getBannerMessages";
                $http.post(url, { "wsResponseInfoList": $scope.globals.getAndResetWsResponseInfoList() }, { withCredentials: true, timeout: $scope.globals.timeoutInMS, cache: false }).success(function (response, status, header, config) {
                    $scope.globals.appWsResponeInfoEnabled = response.isWsResponseInfoEnabled;
                    var bannerMessages = isVoid(response.result) ? [] : response.result;
                    if (bannerMessages.length === 0) {
                        NavigationService.modelNavigation.banner = '';
                    }
                    else {
                        var sortedMessagesByImportance = _.sortBy(bannerMessages, function (msg) {
                            return msg.type;
                        });
                        NavigationService.modelNavigation.banner = sortedMessagesByImportance[0].message;
                        switch (sortedMessagesByImportance[0].type) {
                            case 1:
                                NavigationService.modelNavigation.bannerClass = 'bannerMainDanger';
                                break;
                            case 2:
                                NavigationService.modelNavigation.bannerClass = 'bannerMainWarning';
                                break;
                            case 3:
                                NavigationService.modelNavigation.bannerClass = 'bannerMainInfo';
                                break;
                            default:
                                NavigationService.modelNavigation.bannerClass = 'bannerMainInfo';
                        }
                    }
                }).error(function (response, status, header, config) {
                    NavigationService.modelNavigation.banner = '';
                });
            };
            window.setInterval(checkForNewBannerMain, 60000);
        }
        return controllerMain;
    })();
    Controllers.controllerMain = controllerMain;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerMain.js.map