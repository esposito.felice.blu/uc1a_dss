/// <reference path="../DefinitelyTyped/jquery/jquery.d.ts"/>
/// <reference path="../DefinitelyTyped/angularjs/angular.d.ts"/>
/// <reference path="openlayers3.d.ts"/>
/// <reference path="FileSaver.ts"/>
/// <reference path="utils.ts"/>
/// <reference path="sprintf.d.ts"/>
/// <reference path="Controllers/BaseController.ts"/>
/// <reference path="base64Utils.ts"/>
/// <reference path="NpMapLayers.ts"/>
/// <reference path="NpMapCustomControls.ts"/>
/// <reference path="NpMapGlobals.ts"/>
// Modified for NIVA by MT 20210610a.								
var NpGeo;
(function (NpGeo) {
    var NpMapButton = (function () {
        function NpMapButton(theMap, btnId, letter, title, onclick, updateState) {
            this.theMap = theMap;
            this.btnId = btnId;
            this.letter = letter;
            this.title = title;
            this.onclick = onclick;
            this.updateState = updateState;
            var npBtnThis = this;
            this.btnInstance = new NpGeoCustControls.CustomBtnControl({
                npMapThis: theMap,
                state: theMap.state,
                grp: theMap.grp,
                id: npBtnThis.btnId,
                letter: npBtnThis.letter,
                title: npBtnThis.title,
                onclick: npBtnThis.onclick,
                updateState: npBtnThis.updateState
            });
        }
        return NpMapButton;
    })();
    NpGeo.NpMapButton = NpMapButton;
    (function (NpMapMode) {
        NpMapMode[NpMapMode["NPMAP_MODE_SELECT"] = 0] = "NPMAP_MODE_SELECT";
        NpMapMode[NpMapMode["NPMAP_MODE_DRAW"] = 1] = "NPMAP_MODE_DRAW";
        NpMapMode[NpMapMode["NPMAP_MODE_MEASURE"] = 2] = "NPMAP_MODE_MEASURE";
        NpMapMode[NpMapMode["NPMAP_MODE_HOLES"] = 3] = "NPMAP_MODE_HOLES";
    })(NpGeo.NpMapMode || (NpGeo.NpMapMode = {}));
    var NpMapMode = NpGeo.NpMapMode;
    ;
    var NpMap = (function () {
        function NpMap(options) {
            var _this = this;
            // History of feature values per entity (the revert button targets)
            this.entityHistories = {};
            // Button construction state - keeping track of bottom offset
            this.state = { bottomOffset: 50, leftSide: false };
            // The timeout that destroys the error popover after X milliseconds
            this.popoverTimer = undefined;
            // The copy/paste button serialize/deserialize to this feature
            this.copiedFeature = null;
            // Custom snapping of our own for read-only layers
            this.snappedVertexFeature = null;
            // Snap to vertices of other layers' polygons - on/off
            this.snapEnabled = true;
            // Show selected polygon info - on/off
            this.infoEnabled = false;
            // To show a popover only the first time we hover over a feature...
            this.lastHoveredFeature = null;
            //@p.tsagkis verify whether last acion was a hole addition
            this.lastEditActionIsHoleAdd = false;
            this.oldFeatureSerialized = 'FreshFromCurrentEntity';
            this.createOrUpdateVertexFeature = function (coordinates) {
                var vertexFeature = this.snappedVertexFeature;
                if (isVoid(vertexFeature)) {
                    vertexFeature = new ol.Feature(new ol.geom.Point(coordinates));
                    this.snappedVertexFeature = vertexFeature;
                    this.featureOverlaySnap.getSource().addFeature(vertexFeature);
                }
                else {
                    var geometry = vertexFeature.getGeometry();
                    geometry.setCoordinates(coordinates);
                }
            };
            var npMapThis = this;
            NpGeoGlobals.globalMapThis = this; //asign the object so been accessible from NpMapGlobals
            this.grp = options.grp;
            this.Plato = options.Plato;
            this.divId = options.divId;
            this.getDataFromEntity = options.getDataFromEntity;
            this.getTwoPointSplitGeom = fallback(options.getTwoPointSplitGeom, function (x) { return undefined; });
            this.saveDataToEntity = options.saveDataToEntity;
            this.onSelectFeature =
                fallback(options.onSelectFeature, function (layer, feature) {
                    console.log("feature selected", feature);
                });
            //        getdDefaultMbrFromEntityOnEmptyGeom?: (ent: NpTypes.IBaseEntity, onRecmbr: (mbr: any) => void) => void;
            this.getdDefaultMbrFromEntityOnEmptyGeom =
                fallback(options.getdDefaultMbrFromEntityOnEmptyGeom, function (ent, onRecmbr) {
                    /*
                        by default do nothing
                        var xmax = 252830.4079064769;
                        var xmin = 251180.06215901175;
                        var ymax = 4281964.424262051;
                        var ymin = 4281322.560744904;

                        var extent2100 = new ol.geom.Polygon([[
                            ([xmin, ymin]),
                            ([xmin, ymax]),
                            ([xmax, ymax]),
                            ([xmax, ymin]),
                            ([xmin, ymin])
                        ]]);
                        var extent3857 = extent2100.transform(
                                ol.proj.get('EPSG:2100'),
                                ol.proj.get('EPSG:3857'));
                        onRecmbr(extent3857);
                        */
                });
            this.isDisabled = options.isDisabled;
            // additional DynamicSqlVectorLayer
            var dynamicLayersInfo = this.getDynamicSqlVectorsInfo();
            var dynamicLayers = dynamicLayersInfo.map(function (inf) { return new NpGeoLayers.DynamicSqlVectorLayer(_this.grp.AppName, inf); });
            this.layers = fallback(options.layers, [
                new NpGeoLayers.BingTileLayer(),
                new NpGeoLayers.NokiaTileLayer(),
                new NpGeoLayers.ESRITileLayer(),
                new NpGeoLayers.QuestTileLayer(),
                new NpGeoLayers.OpenStreetTileLayer(),
                new NpGeoLayers.OpenTopoTileLayer(),
                new NpGeoLayers.OpenStamenTileLayer1(),
                new NpGeoLayers.DigitalGlobeTileLayer(),
                new NpGeoLayers.GoogleTileLayer()
            ]);
            this.layers = this.layers.concat(dynamicLayers);
            //@p.tsagkis
            var indexLayers = this.layers.filter(function (l) { return l instanceof NpGeoLayers.SqlVectorLayer; });
            indexLayers.forEach(function (lyr) {
                lyr.layer.setZIndex(lyr.orderIndex);
            });
            this.zoomOnEntityChange = fallback(options.zoomOnEntityChange, true);
            this.showCoordinates = fallback(options.showCoordinates, true);
            this.showRuler = fallback(options.showRuler, true);
            this.getGeometryType = fallback(options.getGeometryType, function (ent) { return ol.geom.GeometryType.POLYGON; });
            this.onBeginNewDrawing = fallback(options.onBeginNewDrawing, function () { });
            this.isCurrentFeatureChangeAllowed = fallback(options.isCurrentFeatureChangeAllowed, function (f) { return new Tuple2(true, ""); });
            this.centerPoint = fallback(options.centerPoint, [24, 38]); // Center on Greece
            this.zoomLevel = fallback(options.zoomLevel, 19); // show all Greece
            this.fillColor = fallback(options.fillColor, 'rgba(0, 0, 255, 0.1)');
            this.borderColor = fallback(options.borderColor, 'blue');
            this.labelColor = fallback(options.labelColor, 'blue');
            this.penWidth = options.penWidth;
            this.opacity = fallback(options.opacity, NpGeoGlobals.DEFAULT_OPACITY);
            this.layerSelectorWidthInPx = fallback(options.layerSelectorWidthInPx, 170);
            this.coordinateSystem = fallback(options.coordinateSystem, 'EPSG:4326');
            this.mapId = fallback(options.mapId, options.divId);
            this.showJumpButton = fallback(options.showJumpButton, true);
            this.showLegendButton = fallback(options.showLegendButton, false);
            this.legendFnc = fallback(options.legendFnc, NpGeoGlobals.defaultLegendFnc);
            this.showEditButtons = fallback(options.showEditButtons, true);
            this.showMeasureButton = fallback(options.showMeasureButton, true);
            this.getEntityLabel = fallback(options.getEntityLabel, function (currentEntity) { return " "; });
            this.getFeatureLabel = fallback(options.getFeatureLabel, function (f, lightweight) {
                //console.log("lightweight", lightweight);
                if (lightweight)
                    return f.getGeometryName();
                else {
                    var bigDescription = _this.getGenericPosLengthAreaDescription(f, lightweight);
                    //console.log("f.entity", f.entity);
                    if (!isVoid(f.entity) && !isVoid(f.entity.data)) {
                        for (var key in f.entity.data) {
                            var field = f.entity.data[key];
                            if (field instanceof Object) {
                                continue;
                            }
                            else
                                bigDescription += "<BR><B>" + key + "</b>:" + f.entity.data[key];
                        }
                    }
                    return bigDescription;
                }
            });
            this.topologyType = fallback(options.topologyType, 0);
            this.getLevel1Features = options.getLevel1Features;
            this.getLevel2Features = options.getLevel2Features;
            this.fullScreenOnlyMap = fallback(options.fullScreenOnlyMap, false);
            var self = this;
            // Setup the custom controls on the map
            this.controls = new ol.Collection();
            // Zoom on the top-left
            this.controls.push(new ol.control.Zoom());
            // Full screen control
            var fsdivid;
            if (this.fullScreenOnlyMap === true) {
                fsdivid = document.getElementById(this.divId);
            }
            else {
                fsdivid = document.getElementById('NpMainContent');
            }
            this.fullScreenControl = new ol.control.FullScreen({
                //   target: document.getElementById('Customer_ControllerGrpCustomer')
                //source: document.getElementById('NpMainContent')
                source: fsdivid
            });
            this.fullScreenControl.on("propertychange", function (e) {
                //console.log("fullscreen change fnc eeeeee=====",e);
            });
            this.controls.push(this.fullScreenControl);
            // Tile select on the top-right
            this.tileSelect = new NpGeoCustControls.TileSelectorControl({ layers: npMapThis.layers });
            //this.controls.push(this.tileSelect);
            // DEPRECATED - Layer select on the bottom-right
            //if (this.layers.some(l => l.isVector)) {
            //    this.controls.push(new LayerToggleControl({
            //        layers: npMapThis.layers,
            //        layerSelectorWidthInPx: npMapThis.layerSelectorWidthInPx
            //    }));
            //}
            // The controls on the left
            if (this.showRuler) {
                var scaleLineControl = new ol.control.ScaleLine();
                this.controls.push(scaleLineControl);
                this.state.bottomOffset = 10; //35;
            }
            else
                this.state.bottomOffset = 10; //10
            // Layer select button on the bottom-right
            this.state.bottomOffset += 10;
            if (this.layers.some(function (l) { return l.isVector; })) {
                this.controls.push(new NpGeoCustControls.VectorLayerSelectorControl({ npMapThis: this, state: this.state, grp: this.grp }));
            }
            //if (!this.showEditButtons) {
            //    if (this.layers.some(l => l.isVector)) {
            //        this.controls.push(new NpGeoCustControls.VectorLayerSelectorControl({ npMapThis: this, state: this.state, grp: this.grp }));
            //    }
            //}
            //if (this.showEditButtons) {
            //    if (this.layers.some(l => l.isVector)) {
            //        this.state.bottomOffset += 10;
            //        this.controls.push(new NpGeoCustControls.VectorLayerSelectorControl({ npMapThis: this, state: this.state, grp: this.grp }));
            //    }
            //    this.controls.push(new NpGeoCustControls.RefreshControl({ npMapThis: this, state: this.state, grp: this.grp }));
            //}
            this.controls.push(new NpGeoCustControls.SelectGeometryControl({ npMapThis: this, state: this.state, grp: this.grp }));
            this.state.bottomOffset += 10;
            if (this.showMeasureButton)
                this.controls.push(new NpGeoCustControls.MeasureDistanceControl({ npMapThis: this, state: this.state, grp: this.grp }));
            if (this.showEditButtons) {
                this.digiInfoSpan = new NpGeoCustControls.DigiInfoControl({ npMapThis: this, state: this.state, grp: this.grp });
                this.controls.push(this.digiInfoSpan);
            }
            this.controls.push(new NpGeoCustControls.ZoomToPolyControl({ npMapThis: this, state: this.state, grp: this.grp }));
            this.state.bottomOffset += 10;
            if (this.showJumpButton) {
                this.controls.push(new NpGeoCustControls.JumpToCoordControl({ npMapThis: this, state: this.state, grp: this.grp }));
            }
            if (this.showLegendButton) {
                this.controls.push(new NpGeoCustControls.legendControl({ npMapThis: this, state: this.state, grp: this.grp }));
            }
            this.controls.push(new NpGeoCustControls.SavePDFControl({ npMapThis: this, state: this.state, grp: this.grp }));
            this.state.bottomOffset += 10;
            this.controls.push(new NpGeoCustControls.RefreshControl({ npMapThis: this, state: this.state, grp: this.grp }));
            /*
             this.controls.push(new NpGeoCustControls.SearchControl({ npMapThis: this, state: this.state, grp: this.grp }));
            */
            // The controls on the right
            this.state.leftSide = true;
            this.state.bottomOffset = 10;
            if (this.showCoordinates) {
                this.coordSpan = new NpGeoCustControls.CoordSpanControl({ npMapThis: this, state: this.state, grp: this.grp });
                this.controls.push(this.coordSpan);
            }
            if (this.showEditButtons) {
                this.controls.push(new NpGeoCustControls.RemoveGeometryControl({ npMapThis: this, state: this.state, grp: this.grp }));
                this.controls.push(new NpGeoCustControls.RevertGeometryControl({ npMapThis: this, state: this.state, grp: this.grp }));
                this.state.bottomOffset += 10;
                this.controls.push(new NpGeoCustControls.DrilHoleControl({ npMapThis: this, state: this.state, grp: this.grp }));
                this.controls.push(new NpGeoCustControls.HoleRemoverControl({ npMapThis: this, state: this.state, grp: this.grp }));
                this.state.bottomOffset += 10;
                this.controls.push(new NpGeoCustControls.EditGeometryControl({ npMapThis: this, state: this.state, grp: this.grp }));
                if (this.getGeometryType(this.Current) !== "TWOPOINTSPLIT") {
                    this.controls.push(new NpGeoCustControls.SnapControl({ npMapThis: this, state: this.state, grp: this.grp }));
                }
                this.state.bottomOffset += 10;
                if (this.topologyType !== 0) {
                    this.controls.push(new NpGeoCustControls.IntersectControl({ npMapThis: this, state: this.state, grp: this.grp }));
                }
                this.controls.push(new NpGeoCustControls.MergeControl({ npMapThis: this, state: this.state, grp: this.grp }));
                this.state.bottomOffset += 10;
                this.controls.push(new NpGeoCustControls.PasteControl({ npMapThis: this, state: this.state, grp: this.grp }));
                this.controls.push(new NpGeoCustControls.CopyControl({ npMapThis: this, state: this.state, grp: this.grp }));
                this.state.bottomOffset += 10;
            }
            this.layers.forEach(function (layer) { layer.Map = _this; });
            this.dragZoomInteraction = new ol.interaction.DragZoom({});
            this.dragZoomInteraction.on('boxdrag', function (ev) {
                document.body.style.cursor = 'crosshair';
            });
            this.dragZoomInteraction.on('boxend', function (ev) {
                document.body.style.cursor = ''; //'default';
            });
            // Create the OL3 map
            this.map = new ol.Map({
                target: npMapThis.divId,
                interactions: ol.interaction.defaults({
                    altShiftDragRotate: false //disable the rotate attitude
                }).extend([
                    this.dragZoomInteraction
                ]),
                layers: npMapThis.layers.map(function (value, index) {
                    return value.layer;
                }),
                view: new ol.View({
                    center: npMapThis.centerPoint,
                    //@p.tsagkis
                    //maxZoom: 19,
                    zoom: npMapThis.zoomLevel,
                    resolutions: NpGeoGlobals.getResolutions(),
                    maxResolution: 0.2799998488000817,
                    minResolution: 4888
                }),
                controls: npMapThis.controls
            });
            //set the map div resizable
            $("#" + npMapThis.divId).resizable({
                handles: 's',
                stop: function (event, ui) {
                    //and update the mapsize whenever a resize event is taking place
                    npMapThis.map.updateSize();
                }
            });
            $("#" + npMapThis.divId).on("contextmenu", function (event) {
                $('#featureContextMenu').remove(); //remove it either way
                if (npMapThis.currentMode == NpMapMode.NPMAP_MODE_DRAW) {
                    //get the right click pixel 
                    var pixel = [event.offsetX, event.offsetY];
                    var drawnFeature = npMapThis.getFeature();
                    var drawnGeom = drawnFeature.getGeometry();
                    var geomType = drawnGeom.getType();
                    if (geomType !== 'Polygon') {
                        return false;
                    }
                    else {
                        var polyRings = drawnGeom.getLinearRings();
                        if (polyRings.length === 1)
                            return false;
                        var clickCoordinate = npMapThis.map.getCoordinateFromPixel(pixel);
                        var checkObjs = [];
                        //t =1 cause we leave outside the outer ring
                        for (var t = 1; t < polyRings.length; t++) {
                            var holeCoords = polyRings[t].getCoordinates();
                            for (var f = 0; f < holeCoords.length; f++) {
                                var dist = Math.sqrt((holeCoords[f][0] -= clickCoordinate[0]) * holeCoords[f][0] + (holeCoords[f][1] -= clickCoordinate[1]) * holeCoords[f][1]);
                                checkObjs.push({ 'ringNum': t, 'vertNum': f, 'dist': dist });
                            }
                        }
                        //sort the array using the dist field
                        checkObjs.sort(function (a, b) {
                            return a.dist - b.dist;
                        });
                        // so the closest one is the first
                        // use the default pixel tolerance
                        // to translate in meters px*resolion
                        var acceptedDistOffset = NpMap.pixelTolerance * (npMapThis.map.getView().getResolution());
                        if (checkObjs[0].dist > acceptedDistOffset)
                            return false;
                        npMapThis.drawInteraction.abortDrawing_();
                        $("#" + npMapThis.divId).append('<ul style="z-index:9999;width:150px" id="featureContextMenu">' +
                            '<li id="singleholeremove">Hole Removal</li>' +
                            '</ul>');
                        $("#featureContextMenu").menu();
                        $("#featureContextMenu").position({
                            my: "left top",
                            of: event
                        });
                        $("#featureContextMenu").show();
                        $("#singleholeremove").mouseover(function () {
                            this.style.backgroundColor = '#FFF3D6';
                        });
                        $("#singleholeremove").mouseout(function () {
                            this.style.backgroundColor = 'white';
                        });
                        $("#singleholeremove").click(function () {
                            NpGeoGlobals.removeHoleAtIndex(drawnGeom, checkObjs[0]['ringNum']);
                        });
                    }
                }
                return false;
            });
            //remove the menu on every docuement click 
            $(document).click(function () {
                $('#featureContextMenu').remove(); //remove it either way
            });
            if (document.addEventListener) {
                //console.log("adding the listeners");
                document.addEventListener('webkitfullscreenchange', NpGeoGlobals.changeFullScreenState, false);
                document.addEventListener('mozfullscreenchange', NpGeoGlobals.changeFullScreenState, false);
                document.addEventListener('fullscreenchange', NpGeoGlobals.changeFullScreenState, false);
                document.addEventListener('MSFullscreenChange', NpGeoGlobals.changeFullScreenState, false);
            }
            // To allow the hack that calls us back inside
            //  ol.interaction.Modify.prototype.handlePointerUp
            // i.e.
            //  this.map_.npMap_.snapModifiedVertexes();
            //add the moveend
            this.map.npMap_ = this;
            // The icon-drawing callback passed to the featureOverlay where we draw
            var getIcon = function () {
                if (npMapThis.getGeometryType(_this.Current) === ol.geom.GeometryType.POINT) {
                    return new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                        anchor: [0.5, 1],
                        anchorXUnits: 'fraction',
                        anchorYUnits: 'fraction',
                        opacity: 1.0,
                        src: 'img/marker-icon.png'
                    }));
                }
                else {
                    return new ol.style.Circle({
                        radius: 7,
                        fill: new ol.style.Fill({
                            color: '#ffcc33'
                        })
                    });
                }
            };
            // The feature overlay where we draw during edit mode
            var featureOverlayStyle = {
                'Point': [new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 7,
                            fill: new ol.style.Fill({
                                color: 'rgba(255, 165, 0, 0.8)'
                            }),
                            stroke: new ol.style.Stroke({
                                color: 'red',
                                width: 2
                            })
                        })
                    })],
                // style for the case of line
                'LineString': [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: (self.borderColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[self.borderColor] : self.borderColor,
                            width: self.penWidth
                        })
                    }),
                    // style for the vertices
                    new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 5,
                            fill: new ol.style.Fill({
                                color: 'rgba(255, 165, 0, 0.4)'
                            })
                        }),
                        geometry: function (feature) {
                            if (feature.getGeometry().getType() === 'Polygon') {
                                var coordsToRet = new Array();
                                var linerings = feature.getGeometry().getLinearRings();
                                for (var b = 0; b < linerings.length; b++) {
                                    var coordsToAdd = linerings[b].getCoordinates();
                                    for (var p = 0; p < coordsToAdd.length; p++) {
                                        coordsToRet.push(coordsToAdd[p]);
                                    }
                                }
                                return new ol.geom.MultiPoint(coordsToRet);
                            }
                            else if (feature.getGeometry().getType() === 'LineString') {
                                return new ol.geom.MultiPoint(feature.getGeometry().getCoordinates());
                            }
                            else {
                                return new ol.geom.Point(feature.getGeometry().getCoordinates());
                            }
                        }
                    })],
                // style for the case of multiline
                'MultiLineString': [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: (self.borderColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[self.borderColor] : self.borderColor,
                            width: self.penWidth
                        })
                    }),
                    // style for the vertices
                    new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 5,
                            fill: new ol.style.Fill({
                                color: 'rgba(255, 165, 0, 0.4)'
                            })
                        }),
                        geometry: function (feature) {
                            if (feature.getGeometry().getType() === 'Polygon') {
                                var coordsToRet = new Array();
                                var linerings = feature.getGeometry().getLinearRings();
                                for (var b = 0; b < linerings.length; b++) {
                                    var coordsToAdd = linerings[b].getCoordinates();
                                    for (var p = 0; p < coordsToAdd.length; p++) {
                                        coordsToRet.push(coordsToAdd[p]);
                                    }
                                }
                                return new ol.geom.MultiPoint(coordsToRet);
                            }
                            else if (feature.getGeometry().getType() === 'LineString') {
                                return new ol.geom.MultiPoint(feature.getGeometry().getCoordinates());
                            }
                            else if (feature.getGeometry().getType() === 'MultiLineString') {
                                coordsToRet = new Array();
                                for (var f = 0; f < feature.getGeometry().getCoordinates().length; f++) {
                                    coordsToRet = coordsToRet.concat(feature.getGeometry().getCoordinates()[f]);
                                }
                                return new ol.geom.MultiPoint(coordsToRet);
                            }
                            else {
                                return new ol.geom.Point(feature.getGeometry().getCoordinates());
                            }
                        }
                    })],
                'Polygon': [
                    // style for the case of polygon
                    new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: (self.borderColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[self.borderColor] : self.borderColor,
                            width: self.penWidth
                        }),
                        fill: new ol.style.Fill({
                            color: (self.fillColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[self.fillColor] : self.fillColor
                        })
                    }),
                    // style for the vertices
                    new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 5,
                            fill: new ol.style.Fill({
                                color: 'rgba(255, 165, 0, 0.4)'
                            })
                        }),
                        geometry: function (feature) {
                            if (feature.getGeometry().getType() === 'Polygon') {
                                var coordsToRet = new Array();
                                var linerings = feature.getGeometry().getLinearRings();
                                for (var b = 0; b < linerings.length; b++) {
                                    var coordsToAdd = linerings[b].getCoordinates();
                                    for (var p = 0; p < coordsToAdd.length; p++) {
                                        coordsToRet.push(coordsToAdd[p]);
                                    }
                                }
                                return new ol.geom.MultiPoint(coordsToRet);
                            }
                            else if (feature.getGeometry().getType() === 'LineString') {
                                return new ol.geom.MultiPoint(feature.getGeometry().getCoordinates());
                            }
                            else {
                                return new ol.geom.Point(feature.getGeometry().getCoordinates());
                            }
                        }
                    })],
                'MultiPolygon': [
                    // style for the case of multipolygon
                    new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: (self.borderColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[self.borderColor] : self.borderColor,
                            width: self.penWidth
                        }),
                        fill: new ol.style.Fill({
                            color: (self.fillColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[self.fillColor] : self.fillColor
                        })
                    }),
                    // style for the vertices
                    new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 5,
                            fill: new ol.style.Fill({
                                color: 'rgba(255, 165, 0, 0.4)'
                            })
                        }),
                        geometry: function (feature) {
                            if (feature.getGeometry().getType() === 'MultiPolygon') {
                                var pols = feature.getGeometry().getPolygons();
                                var coordsToRet = new Array();
                                for (var g = 0; g < pols.length; g++) {
                                    var linerings = pols[g].getLinearRings();
                                    for (var w = 0; w < linerings.length; w++) {
                                        var coordsToAdd = linerings[w].getCoordinates();
                                        for (var p = 0; p < coordsToAdd.length; p++) {
                                            coordsToRet.push(coordsToAdd[p]);
                                        }
                                    }
                                }
                                return new ol.geom.MultiPoint(coordsToRet);
                            }
                            else if (feature.getGeometry().getType() === 'Polygon') {
                                var coordsToRet = new Array();
                                var linerings = feature.getGeometry().getLinearRings();
                                for (var b = 0; b < linerings.length; b++) {
                                    var coordsToAdd = linerings[b].getCoordinates();
                                    for (var p = 0; p < coordsToAdd.length; p++) {
                                        coordsToRet.push(coordsToAdd[p]);
                                    }
                                }
                                return new ol.geom.MultiPoint(coordsToRet);
                            }
                            else if (feature.getGeometry().getType() === 'LineString') {
                                return new ol.geom.MultiPoint(feature.getGeometry().getCoordinates());
                            }
                            else {
                                return new ol.geom.Point(feature.getGeometry().getCoordinates());
                            }
                        }
                    })
                ]
            };
            this.featureOverlayDraw = new ol.layer.Vector({
                source: new ol.source.Vector({
                    useSpatialIndex: false
                }),
                style: function (feature, resolution) {
                    var textStyleConfig = {
                        text: NpGeoGlobals.createTextStyle(feature, resolution, true, 'rgba(0, 0, 0, 1)')
                    };
                    var textStyle = new ol.style.Style(textStyleConfig);
                    var ret = featureOverlayStyle[feature.getGeometry().getType()];
                    ret = ret.concat(textStyle);
                    return ret;
                }
            });
            this.featureOverlayDraw.setMap(npMapThis.map);
            //@p.tsagkis add the events here
            this.featureOverlayDraw.getSource().on('changefeature', function (e) {
                $('#digiInfoCntrlSpan').html('Area:' + parseFloat(NpGeoGlobals.getAreaAndPerimeter(e.feature.getGeometry().clone()).area.toFixed(2)) + '&nbsp;' + 'm<sup>2</sup>,' +
                    'Perimeter:' + parseFloat(NpGeoGlobals.getAreaAndPerimeter(e.feature.getGeometry().clone()).perimeter.toFixed(2)).toFixed(2) + '&nbsp;' + 'm');
            });
            this.featureOverlayDraw.getSource().on('addfeature', function (e) {
                $('#digiInfoCntrlSpan').html('Area:' + parseFloat(NpGeoGlobals.getAreaAndPerimeter(e.feature.getGeometry().clone()).area.toFixed(2)) + '&nbsp;' + 'm<sup>2</sup>,' +
                    'Perimeter:' + parseFloat(NpGeoGlobals.getAreaAndPerimeter(e.feature.getGeometry().clone()).perimeter.toFixed(2)).toFixed(2) + '&nbsp;' + 'm');
            });
            //npMapThis.map.addLayer(this.featureOverlayDraw);
            //           this.featureOverlayDraw.setStyle(            
            //NpGeoGlobals.createPolygonStyleFunction(this.fillColor, this.borderColor, this.labelColor, this.penWidth, getIcon));
            //NpGeoGlobals.createPolygonWithVerticesStyleFunction(
            //    npMapThis,
            //    undefined,
            //    this.fillColor,
            //    this.borderColor,
            //    this.labelColor,
            //    this.penWidth,
            //    undefined,
            //    true,
            //    true)
            //);
            // The feature overlay where we draw during measure mode
            ['featureOverlayTemp', 'featureOverlaySnap'].forEach(function (layerName) {
                _this[layerName] = new ol.layer.Vector({
                    source: new ol.source.Vector({
                        useSpatialIndex: false
                    }),
                    style: new ol.style.Style({
                        fill: new ol.style.Fill({
                            color: 'rgba(255, 255, 255, 0.2)'
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#ffcc33',
                            width: 2
                        }),
                        image: getIcon()
                    })
                });
                _this[layerName].setMap(npMapThis.map);
                //npMapThis.map.addLayer(this[layerName]);
                _this[layerName].setStyle(NpGeoGlobals.createPolygonStyleFunction(_this.fillColor, _this.borderColor, _this.labelColor, _this.penWidth, getIcon));
            });
            // The feature overlay where we crop via the inclusive/exclusive flags
            this.featureOverlayCrop = new ol.layer.Vector({
                source: new ol.source.Vector({
                    useSpatialIndex: false
                }),
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 0, 0, 0.5)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#ffcc33',
                        width: 3
                    }),
                    image: getIcon()
                })
            });
            this.featureOverlayCrop.setMap(npMapThis.map);
            //npMapThis.map.addLayer(this.featureOverlayCrop);
            this.featureOverlayCrop.setStyle(NpGeoGlobals.createPolygonStyleFunction('rgba(255, 0, 0, 0.5)', this.borderColor, this.labelColor, this.penWidth, getIcon));
            this.popupOverlay = new ol.Overlay({
                element: document.getElementById(this.divId + "_popup")
            });
            this.map.addOverlay(this.popupOverlay);
            this.canvas = $('#' + this.divId).find('canvas').get(0);
            var popoverHider = function (e) {
                if (e.target.id == "popovercloseid") {
                    _this.killPopover();
                }
            };
            $(document).on('click', popoverHider);
            // Setup the periodic state update of the map. In all the measurements I took,
            // this doesn't take longer than 1ms.
            function updateMapState() {
                npMapThis.updateBtnState(); // Set the disabled/enabled image/state of the button
                if (npMapThis.currentMode == NpMapMode.NPMAP_MODE_DRAW ||
                    npMapThis.currentMode == NpMapMode.NPMAP_MODE_HOLES)
                    npMapThis.saveFeatures(); // Call the saveDataToEntity callback for changed data
                // Stupid OL3 have started a new drawing upon the click - zap it
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_HOLES)
                    npMapThis.drawInteraction.abortDrawing_();
                // Instead of setInterval (which may cause an avalanche if the body takes too long,
                // just reschedule a call when we've completed our logic.
                npMapThis.stateUpdateTimer = setTimeout(updateMapState, 500);
            }
            this.stateUpdateTimer = setTimeout(updateMapState, 500); // Schedule the 1st call
            this.grp.$scope.$on('$destroy', function (evt) {
                var cols = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    cols[_i - 1] = arguments[_i];
                }
                // Remember to clear the timer callback when the scope goes bye-bye
                clearTimeout(_this.stateUpdateTimer);
                $(document).off('click');
                npMapThis.entityHistories = {};
            });
            if (this.showCoordinates) {
                $(this.map.getViewport()).on('mousemove', function (evt) {
                    var pixel = npMapThis.map.getEventPixel(evt.originalEvent);
                    var coords = npMapThis.map.getCoordinateFromPixel(pixel);
                    var srcProj = 'EPSG:3857';
                    var dstProj = NpGeoGlobals.commonCoordinateSystems(npMapThis.coordinateSystem);
                    coords = proj4(srcProj, dstProj, coords);
                    var msg = "-,-";
                    if (!isVoid(coords) && !isVoid(coords[0]) && !isVoid(coords[1])) {
                        msg = Sprintf.sprintf('%10.6f, %10.6f', coords[0], coords[1]);
                    }
                    $('#coordSpanCtrl').text(msg);
                    // var height = npMapThis.map.getViewport().clientHeight;
                    // var width = npMapThis.map.getViewport().clientWidth;
                    // var coordsBL = npMapThis.map.getCoordinateFromPixel([0,height-1]);
                    // var coordsTR = npMapThis.map.getCoordinateFromPixel([width-1,0]);
                    // var coordsBLT = proj4(srcProj, dstProj, coordsBL);
                    // var coordsTRT = proj4(srcProj, dstProj, coordsTR);
                    // msg = Sprintf.sprintf('%10.2f - %10.2f = %10.2f', coordsTRT[0], coordsBLT[0], coordsTRT[0] - coordsBLT[0]);
                    // console.log("GREG1x: " + msg);
                    // msg = Sprintf.sprintf('%10.2f - %10.2f = %10.2f', coordsTRT[1], coordsBLT[1], coordsTRT[1] - coordsBLT[1]);
                    // console.log("GREG1y: " + msg);
                });
            }
            // When single-clicking takes place...
            this.map.on('click', function (evt) {
                npMapThis.handleClick(evt);
            });
            $(this.map.getViewport()).on('mousemove', function (evt) {
                if (evt.offsetX === undefined) {
                    // Firefox-specific
                    var pixel = npMapThis.map.getEventPixel(evt);
                    evt.offsetX = pixel[0];
                    evt.offsetY = pixel[1];
                    npMapThis.handleHover(evt);
                    evt.offsetX = undefined;
                    evt.offsetY = undefined;
                }
                else
                    npMapThis.handleHover(evt);
            });
            // Double-click can also be used in draw mode to end a polygon
            // so apply the same logic of removing all but last one
            this.map.on('dblclick', function (evt) {
                npMapThis.handleDblClick(evt);
            });
            // Add the (closured) keyboard handler
            $('#' + this.divId).keydown(function (evt) {
                return npMapThis.handleKeyDown(evt);
            });
            // Give the map the keyboard focus
            $('#' + this.divId).focus();
            // ...and let the games begin...
            if (this.showEditButtons)
                //this.enterSelectMode();
                this.enterDrawMode();
            else
                this.enterSelectMode();
            //this.currentMode === NpMapMode.NPMAP_MODE_SELECT
            this.enterSelectMode();
            this.grp.$scope.$on('$destroy', (
            // Watch for a change in the current entity (via the .getKey member)
            npMapThis.grp.$scope.$watch(function () {
                var curEntity = self.Current;
                if (isVoid(curEntity))
                    return undefined;
                else
                    return curEntity.getKeyCombinedWithRowVersion();
            }, function () {
                // When the current entity changes, update the map's featureOverlay with the new Feature
                // Also, if this is the first map we see this entity's key, keep the data in the
                // entityHistories ('pristine' copy) - i.e. the data we will use to perform 'undo'.
                var geo = undefined;
                var splitgeo = undefined;
                var curEntity = self.Current;
                //console.log("curEntity", curEntity);
                if (!isVoid(curEntity)) {
                    var geoJSON = npMapThis.getDataFromEntity(curEntity);
                    var geomType = npMapThis.getGeometryType(curEntity);
                    var polygonToSplitJSON = npMapThis.getTwoPointSplitGeom(curEntity);
                    // console.log("polygonToSplitJSON===", polygonToSplitJSON);
                    if (!isVoid(polygonToSplitJSON)) {
                        _this.handleTwoPointSplitChangeEntity(curEntity);
                        return;
                    }
                    else {
                        if (!isVoid(geoJSON)) {
                            geo = NpGeoGlobals.geoJSON_to_OL3feature({
                                obj: geoJSON,
                                coordinateSystem: npMapThis.coordinateSystem,
                                name: self.getEntityLabel(self.Current)
                            });
                            self.setFeature(geo);
                        }
                        else {
                            if (!isVoid(npMapThis.getdDefaultMbrFromEntityOnEmptyGeom)) {
                                npMapThis.getdDefaultMbrFromEntityOnEmptyGeom(curEntity, function (mbr) {
                                    npMapThis.map.getView().fit(mbr, npMapThis.map.getSize());
                                });
                                //clean the the drawn fetaure as current entity geom is null 
                                npMapThis.featureOverlayDraw.getSource().clear();
                            }
                            else {
                            }
                        }
                    }
                    //handle the history 
                    if (npMapThis.entityHistories[curEntity.getKey()] === undefined) {
                        var initialValue;
                        if (!isVoid(geo)) {
                            // console.log("geo=====", geo);
                            initialValue = geo.clone();
                        }
                        else {
                            initialValue = undefined;
                        }
                        npMapThis.entityHistories[curEntity.getKey()] = [initialValue];
                    }
                    npMapThis.currentEntityKey = curEntity.getKey();
                }
                else {
                    //console.log("current entity is not void");
                    self.setFeature(undefined);
                    npMapThis.currentEntityKey = undefined;
                }
            })));
            //this.grp.$scope.$on('$destroy', <(evt: ng.IAngularEvent, ...cols: any[]) => any>(               
            //    npMapThis.grp.$scope.$watch(    
            //        () => {
            //            var curEntity = self.Current;
            //            return isVoid(curEntity) ? undefined : npMapThis.getGeometryType(curEntity);
            //        },
            //        (newVal, oldVal) => {
            //            if (this.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
            //                //console.log("destroy");
            //                //this is a hack so whatever is the mode on startup select mode should be registerd
            //                //TODO needs to be registered with an extra constant like startUpMode = select .....etc.
            //                this.enterSelectMode();
            //                this.enterDrawMode();
            //                //this.currentMode === NpMapMode.NPMAP_MODE_SELECT
            //            }
            //        })));
        }
        NpMap.prototype.getDynamicSqlVectorsInfo = function () {
            var ret = [];
            //                [
            //                    new NpTypes.DynamicSqlVectorInfo('ILOTS', 'ΙΛΟΤΣ', [new NpTypes.DynamicSqlVectorInfoField('id', 'id'), new NpTypes.DynamicSqlVectorInfoField('13ψηφιο', 'shortdesc')]),
            //                ];
            return ret;
        };
        /**
         * This is the case of TWOPOINTSPLIT when changing entity
         *
         * @param curEntity
         */
        NpMap.prototype.handleTwoPointSplitChangeEntity = function (curEntity) {
            var _this = this;
            if (!isVoid(curEntity)) {
                var splitLayerOnMap = this.getOLLayerByName("splitLayer");
                var splitPolyLayerOnMap = this.getOLLayerByName("splitPolyLayer");
                var geo = undefined;
                var splitgeo = undefined;
                //remove the layer if allready exist
                if (splitLayerOnMap !== null) {
                    //clear and remove it
                    NpGeoGlobals.splitLayer.getSource().clear();
                    this.map.removeLayer(NpGeoGlobals.splitLayer);
                }
                if (splitPolyLayerOnMap !== null) {
                    //clear and remove it
                    NpGeoGlobals.splitPolyLayer.getSource().clear();
                    this.map.removeLayer(NpGeoGlobals.splitPolyLayer);
                }
                this.featureOverlayDraw.getSource().clear();
                var geoJSON = this.getDataFromEntity(curEntity);
                var polygonToSplitJSON = this.getTwoPointSplitGeom(curEntity);
                if (isVoid(geoJSON) && !isVoid(polygonToSplitJSON)) {
                    this.map.addLayer(NpGeoGlobals.splitLayer);
                    NpGeoGlobals.splitLayer.set("layerId", "splitLayer");
                    NpGeoGlobals.splitLayer.setZIndex(999); //just set a high number so make sure it is on top      
                    this.map.addLayer(NpGeoGlobals.splitPolyLayer);
                    NpGeoGlobals.splitPolyLayer.set("layerId", "splitPolyLayer");
                    NpGeoGlobals.splitPolyLayer.setZIndex(1000); //just set a high number so make sure it is on top                  
                    geo = NpGeoGlobals.geoJSON_to_OL3feature({
                        obj: polygonToSplitJSON,
                        coordinateSystem: this.coordinateSystem,
                        name: this.getEntityLabel(curEntity)
                    });
                    //now we also need to add the outer ring of the polygon ready for splitting
                    var lineString = new ol.geom.LineString(geo.getGeometry().getLinearRing(0).getCoordinates());
                    var newFeature = new ol.Feature();
                    newFeature.setGeometryName(geo.get('name'));
                    newFeature.setGeometry(lineString);
                    NpGeoGlobals.splitLayer.getSource().addFeature(newFeature);
                    NpGeoGlobals.splitPolyLayer.getSource().addFeature(geo);
                    this.map.getView().fit(geo.getGeometry(), this.map.getSize());
                    this.registerSplitListeners();
                }
                else if (!isVoid(geoJSON) && !isVoid(polygonToSplitJSON)) {
                    geo = NpGeoGlobals.geoJSON_to_OL3feature({
                        obj: geoJSON,
                        coordinateSystem: this.coordinateSystem,
                        name: this.getEntityLabel(curEntity)
                    });
                    //this is the existing splitted line
                    this.setFeature(geo);
                    //pass it to the entities history for reversing purposes
                    if (typeof this.entityHistories[curEntity.getKey()] == 'undefined') {
                        this.entityHistories[curEntity.getKey()] = [];
                    }
                    this.entityHistories[curEntity.getKey()].push(NpGeoGlobals.safeClone(this.getFeature()));
                    //this is the polygon for this entry
                    splitgeo = NpGeoGlobals.geoJSON_to_OL3feature({
                        obj: polygonToSplitJSON,
                        coordinateSystem: this.coordinateSystem,
                        name: this.getEntityLabel(curEntity)
                    });
                    this.map.addLayer(NpGeoGlobals.splitLayer);
                    NpGeoGlobals.splitLayer.set("layerId", "splitLayer");
                    NpGeoGlobals.splitLayer.setZIndex(999); //just set a high number so make sure it is on top                  
                    this.map.addLayer(NpGeoGlobals.splitPolyLayer);
                    NpGeoGlobals.splitPolyLayer.set("layerId", "splitPolyLayer");
                    NpGeoGlobals.splitPolyLayer.setZIndex(1000); //just set a high number so make sure it is on top                  
                    var lineString = new ol.geom.LineString(splitgeo.getGeometry().getLinearRing(0).getCoordinates());
                    var newFeature = new ol.Feature();
                    newFeature.setGeometryName(geo.get('name'));
                    newFeature.setGeometry(lineString);
                    NpGeoGlobals.splitLayer.getSource().addFeature(newFeature);
                    NpGeoGlobals.splitPolyLayer.getSource().addFeature(splitgeo);
                    this.map.getView().fit(splitgeo.getGeometry(), this.map.getSize());
                    this.registerSplitListeners();
                }
                else {
                    console.log("something wrong here!");
                    if (!isVoid(this.getdDefaultMbrFromEntityOnEmptyGeom)) {
                        this.getdDefaultMbrFromEntityOnEmptyGeom(curEntity, function (mbr) {
                            _this.map.getView().fit(mbr, _this.map.getSize());
                        });
                        //clean the the drawn fetaure as current entity geom is null 
                        this.featureOverlayDraw.getSource().clear();
                    }
                    else {
                    }
                }
                //handle the history mechanism if undefined
                if (this.entityHistories[curEntity.getKey()] === undefined) {
                    var initialValue;
                    if (this.getGeometryType(this.Current) == "TWOPOINTSPLIT") {
                        initialValue = undefined;
                    }
                    else {
                        if (!isVoid(geo)) {
                            // console.log("geo=====", geo);
                            initialValue = geo.clone();
                        }
                        else {
                            initialValue = undefined;
                        }
                    }
                    // console.log("undefined initialValue setting to =====", initialValue);
                    this.entityHistories[curEntity.getKey()] = [initialValue];
                }
                this.currentEntityKey = curEntity.getKey();
            }
            else {
                //console.log("current entity is not void");
                this.setFeature(undefined);
                this.currentEntityKey = undefined;
            }
        };
        NpMap.prototype.zoomToGeom = function (geom) {
            var self = this;
            self.map.getView().fit(geom, { size: self.map.getSize(), maxZoom: 16 });
        };
        // Show a bootstrap popover with 'msg'
        NpMap.prototype.showPopover = function (feature, msg, permanent) {
            var _this = this;
            if (permanent === void 0) { permanent = false; }
            // Clear any existing popover
            this.killPopover();
            var centerPoint;
            if (!isVoid(feature)) {
                // Figure out pixel position of the feature
                var extent = feature.getGeometry().getExtent();
                centerPoint = [
                    (extent[0] + extent[2]) / 2.,
                    (extent[1] + extent[3]) / 2.];
            }
            else {
                var centerX = this.map.getViewport().clientWidth / 2.;
                var centerY = this.map.getViewport().clientHeight / 2.;
                centerPoint = this.map.getCoordinateFromPixel([centerX, centerY]);
            }
            this.popupOverlay.setPosition(centerPoint);
            // No popover visible, so go ahead and show this new one
            var element = this.popupOverlay.getElement();
            var container = "body"; //append on body by default
            if (NpGeoGlobals.fullScreenState === true) {
                container = '#NpMainContent';
            }
            $(element).popover({
                html: true, placement: 'top', content: msg, title: '<center><b>Info</b></center>', container: container
            }).popover('show');
            $('.popover-title').append('<button style="float:right; margin-top: -26px; margin-right: -12px" ' +
                'id="popovercloseid" type="button" class="close">&times;</button>');
            if (!permanent) {
                // Schedule a popover destroying after 3000ms, and store the timer
                this.popoverTimer = setTimeout(function () {
                    _this.killPopover();
                }, 3000);
            }
        };
        // Close the popover before the timer expires...
        NpMap.prototype.killPopover = function () {
            // Is there a currently visible popover? If so, there's an active timer to destroy it...
            if (this.popoverTimer !== undefined) {
                // Cancel the timer
                clearTimeout(this.popoverTimer);
                this.popoverTimer = undefined;
            }
            // Destroy any existing popover
            var element = this.popupOverlay.getElement();
            $(element).popover('destroy');
        };
        NpMap.prototype.showVectorLayers = function () {
            var scope = this.grp.$scope;
            var dialogOptions = new NpTypes.DialogOptions();
            dialogOptions.title = "Thematic Layers";
            var widths = new Array();
            var width = 0;
            this.layers.
                filter(function (l) { return l instanceof NpGeoLayers.SqlVectorLayer; }).
                forEach(function (l) {
                var curWidth = NpGeoGlobals.getTextWidth(l.label, "1.1em Verdana,Arial,sans-serif");
                widths.push(curWidth);
            });
            width = Math.max.apply(Math, widths);
            widths = []; //prevent mem liks
            dialogOptions.width = ((40 * 6) + width + 50) + "px";
            //console.log("dialogOptions.width", dialogOptions.width);
            dialogOptions.className = "MapLayersDialog";
            dialogOptions.npMapThis = this;
            this.Plato.showDialog(scope, "Thematic Layers", "/" + scope.globals.appName + '/partials/layers.html?rev=' + scope.globals.version, dialogOptions);
        };
        // When a new feature is drawn, delete the old ones
        NpMap.prototype.removeAllButLastDrawn = function () {
            var npMapThis = this;
            if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW ||
                npMapThis.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                if (npMapThis.drawInteraction !== undefined) {
                    var fo = npMapThis.getDrawingOverlay();
                    var drawnFeatures = fo.getSource().getFeaturesCollection();
                    while (drawnFeatures.getLength() > 1) {
                        fo.getSource().removeFeature(drawnFeatures.item(0));
                    }
                }
            }
        };
        NpMap.prototype.handleHover = function (evt) {
            if (this.snapEnabled)
                //this.handleSnapHover(evt);
                if (this.infoEnabled) {
                    // Find the feature we hovered over on
                    var feature = this.map.forEachFeatureAtPixel([evt.offsetX, evt.offsetY], function (feature, layer) {
                        var g = feature.getGeometry();
                        if (g instanceof ol.geom.Polygon) {
                            return feature;
                        }
                        return null;
                    }, undefined, // unused 3rd arg in above callback
                    function (l) { return true; } // Don't filter any feature
                     // Don't filter any feature
                    );
                    if (isVoid(feature)) {
                        this.killPopover();
                    }
                    else if (feature !== this.lastHoveredFeature) {
                        var description = this.getFeatureLabel(feature, true);
                        if (!isVoid(description))
                            this.showPopover(feature, description);
                        this.lastHoveredFeature = feature;
                    }
                }
        };
        NpMap.prototype.handleSnapHover = function (evt) {
            var npMapThis = this;
            if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
                // In drawing mode, we want to snap on any vertex of any vector layer
                var pixel = [evt.offsetX, evt.offsetY];
                var map = npMapThis.map;
                // Calculate a box of +/- 'pixelTolerance x pixelTolerance' pixels
                // around the point that we clicked
                var lowerLeft = map.getCoordinateFromPixel([pixel[0] - NpMap.pixelTolerance, pixel[1] + NpMap.pixelTolerance]);
                var upperRight = map.getCoordinateFromPixel([pixel[0] + NpMap.pixelTolerance, pixel[1] - NpMap.pixelTolerance]);
                var box = ol.extent.boundingExtent([lowerLeft, upperRight]);
                // Find the coordinate that the pixel we clicked on maps to
                var pixelCoordinate = map.getCoordinateFromPixel(pixel);
                // Reset the 'result' boolean (whether we snapped or not)
                var snappedToVertex = false;
                npMapThis.layers.forEach(function (layer) {
                    // if we snapped already in a previous layer, abort early
                    if (snappedToVertex)
                        return;
                    if (layer.isVisible &&
                        layer.isVector &&
                        (layer instanceof NpGeoLayers.SqlVectorLayer) && layer.getVerticesVisible()) {
                        var testLayerSource = layer.source;
                        //console.log("layer", layer);
                        //console.log("testLayerSource", testLayerSource);
                        // Vector layers are built with a "feature-extent" RTree...
                        //var rBush = (<any>testLayerSource).featuresRtree_.rbush_;
                        //console.log("rBush", rBush);
                        // Use the RTree to find the features whose extents fall inside
                        // our pixelTolerance x pixelTolerance box
                        //var testFeats = rBush.getInExtent(box);
                        //console.log("testFeats", testFeats);
                        //console.log("testLayerSource",testLayerSource);
                        var features = testLayerSource.getFeaturesInExtent(box);
                        //console.log("features", features);
                        if (features.length > 0) {
                            var closestPoint, closestDistance;
                            features.forEach(function (f) {
                                // if we snapped already on a previous feature, abort early
                                if (snappedToVertex)
                                    return;
                                // This feature is a candidate! 
                                var g = f.getGeometry();
                                // For all the polygons inside it...
                                if (g instanceof ol.geom.Polygon) {
                                    var x, y;
                                    // Find the closest vertex to our hovered pixel coordinates
                                    g.flatCoordinates.forEach(function (n) {
                                        // (the flatCoordinates are an array of width N (N is even,
                                        // since the array is of the form [x1,y1,x2,y2,...])
                                        // so we need to do this check in two phases: one storing the x...
                                        if (isVoid(x)) {
                                            x = n;
                                        }
                                        else {
                                            // and one completing a coordinate pair (x,y)
                                            var c = [x, n];
                                            // Ignore the point if we've already snapped on it
                                            if (!isVoid(npMapThis.avoidResnapOnSamePoint)) {
                                                var avoidX = npMapThis.avoidResnapOnSamePoint[0];
                                                var avoidY = npMapThis.avoidResnapOnSamePoint[1];
                                                if (avoidX === x && avoidY === n) {
                                                    x = undefined;
                                                    return;
                                                }
                                            }
                                            var dist = ol.coordinate.squaredDistance(c, pixelCoordinate);
                                            if (isVoid(closestPoint)) {
                                                closestPoint = c.slice(0);
                                                closestDistance = dist;
                                            }
                                            else {
                                                if (dist < closestDistance) {
                                                    closestDistance = dist;
                                                    closestPoint = c.slice(0);
                                                }
                                            }
                                            x = undefined;
                                        }
                                    });
                                    // Was the closest vertex within pixelTolerance?
                                    if (!isVoid(closestDistance)) {
                                        var pixelClosest = map.getPixelFromCoordinate(closestPoint);
                                        var pixelClosestDist = Math.sqrt(ol.coordinate.squaredDistance(pixelClosest, pixel));
                                        snappedToVertex = pixelClosestDist < NpMap.pixelTolerance;
                                        if (snappedToVertex) {
                                            // It was - update or create the 'snap preview' vertex feature
                                            npMapThis.createOrUpdateVertexFeature(closestPoint);
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
                if (!snappedToVertex) {
                    // Too far away from vertices - snap point off
                    if (!isVoid(npMapThis.snappedVertexFeature)) {
                        npMapThis.featureOverlaySnap.getSource().removeFeature(npMapThis.snappedVertexFeature);
                        this.snappedVertexFeature = null;
                    }
                }
            }
        };
        NpMap.prototype.handleClick = function (evt) {
            var npMapThis = this;
            // There's no way a popover can survive a click
            this.killPopover();
            // Focus the map, just in case
            $('#' + npMapThis.divId).focus();
            if (this.currentMode === NpMapMode.NPMAP_MODE_MEASURE)
                return; // Allow click to pass-thru in Measure mode
            if (evt.originalEvent.shiftKey)
                return; // Allow shift-click to pass-thru (it removes vertices)
            if (this.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                this.handleSelectingFeature(evt);
            }
            else if (this.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
                this.removeAllButLastDrawn();
            }
            else if (this.currentMode === NpMapMode.NPMAP_MODE_HOLES) {
                this.drawInteraction.abortDrawing_();
                //get rid of it. It is not used any more
                //this.drilHole(evt);
                return;
            }
            else
                return;
        };
        NpMap.prototype.handleDblClick = function (evt) {
            if (this.currentMode === NpMapMode.NPMAP_MODE_DRAW ||
                this.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                this.removeAllButLastDrawn();
            }
        };
        Object.defineProperty(NpMap.prototype, "Current", {
            get: function () {
                return isVoid(this.grp.model.selectedEntities) ?
                    undefined :
                    this.grp.model.selectedEntities[0];
            },
            enumerable: true,
            configurable: true
        });
        NpMap.prototype.handleKeyDown = function (evt) {
            var npMapThis = this;
            if (!($('#' + npMapThis.divId).is(":focus")))
                return true;
            var charCode = evt.which || evt.keyCode;
            if (charCode === 113) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
                    npMapThis.enterSelectMode();
                }
                else {
                    npMapThis.enterDrawMode();
                }
            }
            else if (charCode === 27 || charCode === 81) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW ||
                    npMapThis.currentMode === NpMapMode.NPMAP_MODE_MEASURE) {
                    npMapThis.drawInteraction.abortDrawing_();
                    npMapThis.avoidResnapOnSamePoint = null;
                }
                else if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                    npMapThis.enterDrawMode();
                    npMapThis.enterSelectMode();
                }
                return false;
            }
            else if (charCode === 83) {
                npMapThis.enterSelectMode();
            }
            else if (charCode === 69) {
                npMapThis.enterDrawMode();
            }
            else if (charCode === 119) {
                npMapThis.showVectorLayers();
            }
            else if (charCode === 115) {
                npMapThis.enterMeasureMode(evt.shiftKey);
            }
            else if (charCode === 118) {
                npMapThis.showJumpDialog();
            }
            else if (charCode === 46) {
                npMapThis.removeFeatures(npMapThis.featureOverlayDraw);
                return false;
            }
            else if (charCode === 8) {
                npMapThis.removeLastVertex();
            }
            else if (charCode === 90) {
                if (evt.ctrlKey)
                    npMapThis.revert();
                else
                    npMapThis.zoomToCurrentFeatures();
            }
            return true;
        };
        NpMap.prototype.clearInteractions = function () {
            if (this.modifyInteraction !== undefined)
                this.map.removeInteraction(this.modifyInteraction);
            if (this.selectInteraction !== undefined)
                this.map.removeInteraction(this.selectInteraction);
            if (this.drawInteraction !== undefined)
                this.map.removeInteraction(this.drawInteraction);
            if (this.dragAndDropInteraction !== undefined)
                this.map.removeInteraction(this.dragAndDropInteraction);
            if (typeof this.snapInteraction !== 'undefined')
                this.map.removeInteraction(this.snapInteraction);
            if (typeof this.holeDraw !== 'undefined')
                this.map.removeInteraction(this.holeDraw);
            this.removeSplitLineInteraction();
        };
        NpMap.prototype.addSelectInteraction = function () {
            this.selectInteraction = new ol.interaction.Select({
                condition: ol.events.condition.never,
                style: NpGeoGlobals.createPolygonStyleFunction("whiteSelected", "red", "blue", 5, undefined)
            });
            this.map.addInteraction(this.selectInteraction);
        };
        NpMap.prototype.addModifyInteraction = function () {
            var npMapThis = this;
            var sourceFeatures;
            if (this.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                sourceFeatures = this.selectInteraction.getFeatures();
            }
            else {
                sourceFeatures = this.featureOverlayDraw.getSource().getFeaturesCollection();
            }
            this.modifyInteraction = new ol.interaction.Modify({
                features: sourceFeatures,
                //@p.tsagkis ALT pressed while clicking => delete vertices
                deleteCondition: function (event) {
                    return ol.events.condition.altKeyOnly(event) &&
                        ol.events.condition.singleClick(event);
                }
            });
            //during modification disable the draw interaction
            npMapThis.map.addInteraction(this.modifyInteraction);
            npMapThis.modifyInteraction.on('modifystart', function (e) {
                NpGeoGlobals.inModifyMode = true;
                //cancel any drawing 
                npMapThis.drawInteraction.abortDrawing_();
                //is only needed when deleting vertices
                if (ol.events.condition.altKeyOnly(e.mapBrowserPointerEvent) === true) {
                    npMapThis.drawInteraction.setActive(false);
                }
            });
            //and re-enable back when modify end
            npMapThis.modifyInteraction.on('modifyend', function (e) {
                NpGeoGlobals.inModifyMode = false;
                npMapThis.drawInteraction.abortDrawing_();
                //is only needed when deleting vertices
                if (ol.events.condition.altKeyOnly(e.mapBrowserPointerEvent) === true) {
                    npMapThis.drawInteraction.setActive(true);
                }
            });
            //function featureChangeHandler(e) {
            //    var feature = e.target;
            //    npMapThis.onModifyingExistingDrawing(feature);
            //}
            //sourceFeatures.on('add', function(e) {
            //    // feature selected for modification
            //    var feature = e.element;
            //    feature.on('change', featureChangeHandler);
            //});
            //sourceFeatures.on('remove', function(e) {
            //    // feature selected for modification
            //    var feature = e.element;
            //    feature.un('change', featureChangeHandler);
            //});
        };
        //@p.tsagkis
        NpMap.prototype.addSnapInteraction = function () {
            this.snapInteraction = new ol.interaction.Snap({
                features: this.snapFeats,
                pixelTolerance: NpMap.pixelTolerance
            });
            this.map.addInteraction(this.snapInteraction);
            this.snapInteraction.setActive(this.snapEnabled);
        };
        NpMap.prototype.removeSnapInteraction = function () {
            if (this.snapInteraction !== undefined)
                this.map.removeInteraction(this.snapInteraction);
        };
        /**
         * Add the split interaction to the map
         * If no split features exist then get them out of the outer polygon exist
         */
        NpMap.prototype.addSplitLineInteraction = function () {
            this.removeSplitLineInteraction();
            var sourceFeatures = NpGeoGlobals.splitLayer.getSource().getFeatures();
            //console.log("addSplitLineInteraction.......sourceFeatures=====", sourceFeatures);
            if (sourceFeatures == null || sourceFeatures.length == 0) {
                // console.log("no features exits for splitting", this);
                var polygonToSplitJSON = this.getTwoPointSplitGeom(this.Current);
                //  console.log("polygonToSplitJSON", polygonToSplitJSON);
                if (typeof polygonToSplitJSON !== 'undefined') {
                    var splitgeo = NpGeoGlobals.geoJSON_to_OL3feature({
                        obj: polygonToSplitJSON,
                        coordinateSystem: this.coordinateSystem,
                        name: this.getEntityLabel(this.Current)
                    });
                    var lineString = new ol.geom.LineString(splitgeo.getGeometry().getLinearRing(0).getCoordinates());
                    var newFeature = new ol.Feature();
                    newFeature.setGeometryName("geometry");
                    newFeature.setGeometry(lineString);
                    NpGeoGlobals.splitLayer.getSource().clear();
                    NpGeoGlobals.splitLayer.getSource().addFeature(newFeature);
                    NpGeoGlobals.splitPolyLayer.getSource().clear();
                    NpGeoGlobals.splitPolyLayer.getSource().addFeature(splitgeo);
                    this.map.getView().fit(splitgeo.getGeometry(), this.map.getSize());
                }
            }
            this.splitInteraction = new ol.interaction.Split({
                sources: NpGeoGlobals.splitLayer.getSource(),
                tolerance: 1e-5,
                snapDistance: 30
            });
            this.map.addInteraction(this.splitInteraction);
            this.registerSplitListeners();
        };
        /**
         * Remove the split interaction from map
         */
        NpMap.prototype.removeSplitLineInteraction = function () {
            if (typeof this.splitInteraction !== 'undefined') {
                this.unregitserSplitListeners();
                this.map.removeInteraction(this.splitInteraction);
            }
            ;
        };
        /**
        * Attach the listeners connected to the split interaction
        */
        NpMap.prototype.registerSplitListeners = function () {
            //just make sure to unregiter before re-register
            this.unregitserSplitListeners();
            if (typeof this.splitInteraction != 'undefined') {
                var this_ = this;
                //and register the listeners for the layer
                var coordToSearchFor = [];
                this.splitInteraction.isFirstPoint = true;
                this.splitInteraction.listenerBefore = this.splitInteraction.on("beforesplit", function (e) {
                    coordToSearchFor = e.original.getGeometry().getFirstCoordinate();
                });
                // Listen to split event
                this.splitInteraction.listenerAfter = this.splitInteraction.on("aftersplit", function (e) {
                    var firstFeat;
                    var secFeat;
                    for (var i = 0; i < e.features.length; i++) {
                        if (JSON.stringify(e.features[i].getGeometry().getLastCoordinate()) == JSON.stringify(coordToSearchFor)) {
                            firstFeat = e.features[i];
                        }
                        else {
                            secFeat = e.features[i];
                        }
                    }
                    // console.log("this_.splitInteraction.isFirstPoint=====", this_.splitInteraction.isFirstPoint);
                    if (this_.splitInteraction.isFirstPoint == true) {
                        var newLineCoords = firstFeat.getGeometry().getCoordinates().slice(0, -1).concat(secFeat.getGeometry().getCoordinates());
                        NpGeoGlobals.splitLayer.getSource().clear();
                        NpGeoGlobals.splitLayer.getSource().addFeature(new ol.Feature(new ol.geom.LineString(newLineCoords)));
                        //  console.log("features exist after first split====", NpGeoGlobals.splitLayer.getSource().getFeatures().length)
                        this_.splitInteraction.isFirstPoint = false;
                    }
                    else {
                        this_.splitInteraction.setActive(false);
                        this_.splitInteraction.overlayLayer_.getSource().clear();
                        this_.unregitserSplitListeners();
                        var dialogOptions = new NpTypes.DialogOptions();
                        dialogOptions.title = "Select part of line";
                        dialogOptions.width = "18em";
                        dialogOptions.className = "ChooseSplitLineDialog";
                        dialogOptions.onCloseDialog = function () {
                            this_.featureOverlayCrop.getSource().clear();
                            this_.splitInteraction.setActive(true);
                            this_.splitInteraction.overlayLayer_.getSource().clear();
                            NpGeoGlobals.splitLayer.getSource().clear();
                            this_.unregitserSplitListeners();
                            this_.splitInteraction.isFirstPoint = true;
                            //remove the existing entry from entities history to avoid dublications within undo mechanism
                            if (this_.entityHistories[this_.currentEntityKey] !== undefined && this_.entityHistories[this_.currentEntityKey].length > 0) {
                                this_.entityHistories[this_.currentEntityKey].pop();
                            }
                            this_.handleTwoPointSplitChangeEntity(this_.Current); //make the outline availble for splitting
                        };
                        dialogOptions.npMapThis = this_;
                        dialogOptions.linesExist = NpGeoGlobals.splitLayer.getSource().getFeatures();
                        dialogOptions.curEnt = this_.Current;
                        var scope = this_.grp.$scope;
                        this_.Plato.showDialog(scope, "Select line segment", "/" + scope.globals.appName + '/partials/chooseSplitLine.html?rev=' + scope.globals.version, dialogOptions);
                    }
                });
            }
        };
        /**
         * Remove the listeners connected to the split interaction
         * should be public to be accesible from Controller
         */
        NpMap.prototype.unregitserSplitListeners = function () {
            if (typeof this.splitInteraction != 'undefined') {
                this.splitInteraction.isFirstPoint = true;
                ol.Observable.unByKey(this.splitInteraction.listenerBefore);
                ol.Observable.unByKey(this.splitInteraction.listenerAfter);
            }
        };
        NpMap.prototype.addDrawInteraction = function (shift) {
            var npMapThis = this;
            var allowedGeometryType = this.currentMode == NpMapMode.NPMAP_MODE_DRAW ?
                npMapThis.getGeometryType(this.Current) :
                ol.geom.GeometryType.LINE_STRING;
            if (shift === true) {
                allowedGeometryType = ol.geom.GeometryType.POLYGON;
            }
            var sourceFeatures = this.getDrawingOverlay().getSource().getFeaturesCollection();
            npMapThis.drawInteraction = new ol.interaction.Draw({
                features: sourceFeatures,
                condition: function (e) {
                    if (e.originalEvent.button === 0)
                        return true; //allow only the left mouse click button
                    else
                        return false;
                },
                freehandCondition: function (e) {
                    return false;
                },
                type: allowedGeometryType
            });
            //add the draw interaction
            npMapThis.map.addInteraction(npMapThis.drawInteraction);
            npMapThis.drawInteraction.on('drawstart', function (evt) {
                // Call the user provided onBeginNewDrawing, but not in the measure mode!
                if (npMapThis.currentMode == NpMapMode.NPMAP_MODE_DRAW)
                    npMapThis.onBeginNewDrawing();
                else
                    npMapThis.removeFeatures(npMapThis.featureOverlayTemp);
            });
            //end register the events
            npMapThis.drawInteraction.on('drawend', function (evt) {
                // This wil be called for both draw and measure mode - be careful...
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
                    var fo = npMapThis.getDrawingOverlay();
                    //fo.getSource().addFeature(evt.feature);
                    // Baptize the newborn feature to ""
                    removeAllFeaturesAndRenameLastFeature(fo, function (f) { return ""; });
                    // Reset the logic of avoiding the last snapped point
                    npMapThis.avoidResnapOnSamePoint = null;
                }
                else if (npMapThis.currentMode == NpMapMode.NPMAP_MODE_MEASURE) {
                    // Baptize the newborn measured feature to its length representation!
                    removeAllFeaturesAndRenameLastFeature(npMapThis.featureOverlayTemp, function (f) {
                        var line = f.getGeometry();
                        return npMapThis.formatLength(line);
                    });
                    setTimeout(function () {
                        npMapThis.featureOverlayTemp.getSource().getFeaturesCollection().forEach(function (feature) {
                            var geomType = feature.getGeometry().getType();
                            var msg = "";
                            if (geomType === 'LineString') {
                                msg = "Length:<br><center><b>" +
                                    npMapThis.formatLength(feature.getGeometry()) +
                                    "</b></center>";
                            }
                            if (geomType === 'Polygon') {
                                msg = "Area:<br><center><b>" +
                                    npMapThis.formatArea(feature.getGeometry(), false) +
                                    "</b></center>";
                            }
                            npMapThis.showPopover(
                            //undefined, //@ptsagkis edit just try to place it on the right position
                            feature, //was passing undefined. dont know why. passing the feature leeds to place popover on measure geom MBR.
                            msg, true);
                        });
                    }, 250);
                }
            });
            npMapThis.dragAndDropInteraction = new ol.interaction.DragAndDrop({
                formatConstructors: [
                    ol.format.GeoJSON,
                    ol.format.GPX,
                    ol.format.KML
                ]
            });
            npMapThis.map.addInteraction(npMapThis.dragAndDropInteraction);
            npMapThis.dragAndDropInteraction.on('addfeatures', function (event) {
                //@p.tsagkis handler for GPX + KML
                var filename = event.file.name;
                var fileExt = filename.substring(filename.lastIndexOf(".") + 1);
                if (fileExt.toUpperCase() === "GPX" || fileExt.toUpperCase() === "KML") {
                    //gpx is another story
                    var layerOnMap = npMapThis.getOLLayerByName("importsLayer");
                    //if layer doesnt exist on map, add it
                    if (layerOnMap === null) {
                        npMapThis.map.addLayer(NpGeoGlobals.importsLayer);
                        NpGeoGlobals.importsLayer.setZIndex(1000); //just set a high number so make sure it is on top                  
                    }
                    else {
                        NpGeoGlobals.importsLayer.getSource().clear();
                    }
                    var featsToAdd = [];
                    for (var i = 0; i < event.features.length; i++) {
                        var g = event.features[i].getGeometry();
                        var newFeature = new ol.Feature();
                        newFeature.setGeometryName(event.features[i].get('name'));
                        newFeature.setGeometry(g);
                        featsToAdd.push(newFeature);
                    }
                    NpGeoGlobals.importsLayer.getSource().addFeatures(featsToAdd);
                    npMapThis.map.getView().fit(NpGeoGlobals.importsLayer.getSource().getExtent(), npMapThis.map.getSize());
                }
                else {
                    //while GeoJSON is about to replace the currently drawn feature
                    npMapThis.removeFeatures(npMapThis.featureOverlayDraw);
                    if (event.features.length > 0) {
                        npMapThis.featureOverlayDraw.getSource().addFeature(event.features[0]);
                        npMapThis.zoomToCurrentFeatures();
                    }
                }
            });
            var removeAllFeaturesAndRenameLastFeature = function (fo, namerFunc) {
                var features = fo.getSource().getFeaturesCollection();
                if (features.getLength() > 0) {
                    var lastFeature = features.item(features.getLength() - 1);
                    var label = namerFunc(lastFeature);
                    var geometry = lastFeature.getGeometry();
                    var newFeature = new ol.Feature();
                    newFeature.setGeometryName(label);
                    newFeature.setGeometry(geometry);
                    while (features.getLength() > 0) {
                        fo.getSource().removeFeature(features.item(0));
                        features = fo.getSource().getFeaturesCollection();
                    }
                    fo.getSource().addFeature(newFeature);
                }
            };
        };
        NpMap.prototype.saveFeatures = function () {
            var _this = this;
            var npMapThis = this;
            if (NpGeoGlobals.inModifyMode) {
                //  avoid doing any changes while a modifyInteraction is at work.
                return;
            }
            var feature = this.getFeature();
            var featureSerialized;
            if (isVoid(feature)) {
                featureSerialized = undefined;
            }
            else {
                var geoJsonEncoder = new ol.format.GeoJSON();
                featureSerialized = geoJsonEncoder.writeGeometry(feature.getGeometry());
            }
            if (this.oldFeatureSerialized === 'FreshFromCurrentEntity') {
                this.oldFeatureSerialized = featureSerialized;
                return;
            }
            if (!angular.equals(featureSerialized, this.oldFeatureSerialized)) {
                var undoAndShowMsg = function (msg, dorev) {
                    if (dorev === true) {
                        _this.revert({ dontPop: true });
                    }
                    var scope = _this.grp.$scope;
                    messageBox(scope, _this.Plato, "Not allowed change", msg, IconKind.ERROR, [
                        new Tuple2("OK", function () { })
                    ], 0, 0, '19em');
                };
                if (!isVoid(feature)) {
                    var g = feature.getGeometry();
                    if (g instanceof ol.geom.Polygon) {
                        var arrPolygonPieces = NpGeoGlobals.OL3feature_to_coordinates(feature);
                        var validity = isGeoJstsValid(feature.getGeometry());
                        //console.log("validity", validity);
                        if (npMapThis.currentMode === 3 && npMapThis.lastEditActionIsHoleAdd === true)
                            return;
                        if (validity !== true) {
                            if (npMapThis.lastEditActionIsHoleAdd === true) {
                                //console.log("need to remove the last hole");
                                undoAndShowMsg("Polygons of this type are not allowed", false);
                                var rings = getAllRingsExceptLastFromFeat(feature);
                                //get all rings and leave the last one out
                                feature.getGeometry().setCoordinates(rings);
                                npMapThis.lastEditActionIsHoleAdd = false;
                                return;
                            }
                            else {
                                //console.log('fall into else statement');                   
                                undoAndShowMsg("Polygons of this type are not allowed", true);
                                npMapThis.lastEditActionIsHoleAdd = false;
                                return;
                            }
                        }
                        npMapThis.lastEditActionIsHoleAdd = false;
                    }
                }
                var allowed = this.isCurrentFeatureChangeAllowed(feature);
                if (allowed.a) {
                    if (this.entityHistories[this.currentEntityKey] === undefined) {
                        // This should never happen
                        console.error("Unexpected: undo history of currentEntityKey is missing...");
                        this.entityHistories[this.currentEntityKey] = [];
                    }
                    this.entityHistories[this.currentEntityKey].push(NpGeoGlobals.safeClone(feature));
                    var geoJSON = NpGeoGlobals.OL3feature_to_geoJSON(feature, this.coordinateSystem);
                    this.saveDataToEntity(this.Current, geoJSON);
                    this.oldFeatureSerialized = featureSerialized;
                }
                else {
                    undoAndShowMsg(allowed.b, true);
                    return;
                }
            }
        };
        NpMap.prototype.updateBtnState = function () {
            this.controls.forEach(function (btn) {
                if (btn.updateState !== undefined)
                    btn.updateState();
            });
        };
        NpMap.prototype.getFeature = function () {
            var features = this.featureOverlayDraw.getSource().getFeaturesCollection();
            if (features.getLength() == 0)
                return null;
            else
                return features.item(0);
        };
        NpMap.prototype.setPristine = function () {
            this.oldFeatureSerialized = 'FreshFromCurrentEntity';
        };
        NpMap.prototype.setFeature = function (featureObj, autozoomDisabled) {
            if (autozoomDisabled === void 0) { autozoomDisabled = false; }
            //console.log("setting feature");
            if (this.selectInteraction !== undefined) {
                var selectedFeatures = this.selectInteraction.getFeatures();
                selectedFeatures.clear();
            }
            this.featureOverlayDraw.getSource().clear();
            //console.log("setFeature .... featureObj====", featureObj);
            if (!isVoid(featureObj)) {
                this.featureOverlayDraw.getSource().addFeature(featureObj);
                if (!this.showEditButtons || this.isDisabled(this.Current) || this.currentMode === 0)
                    this.enterSelectMode();
                else
                    this.enterDrawMode();
                if (this.zoomOnEntityChange && !autozoomDisabled) {
                    this.zoomToCurrentFeatures();
                }
            }
            else {
                if (!this.showEditButtons || this.currentMode === 0)
                    this.enterSelectMode();
                else
                    this.enterDrawMode();
            }
            this.setPristine();
        };
        NpMap.prototype.revert = function (options) {
            if (options === void 0) { options = { dontPop: false }; }
            var npMapThis = this;
            if (!isVoid(this.currentEntityKey)) {
                var oldMode = this.currentMode;
                var valuesHistoryArray = this.entityHistories[this.currentEntityKey];
                var savedCopy;
                if (valuesHistoryArray.length > 0) {
                    if (valuesHistoryArray.length > 1 && options.dontPop === false)
                        valuesHistoryArray.pop();
                    savedCopy = NpGeoGlobals.safeClone(valuesHistoryArray[valuesHistoryArray.length - 1]);
                }
                else {
                    // This should never happen
                    console.error("empty undo history...");
                    savedCopy = undefined;
                }
                this.setFeature(savedCopy, true); // autozoomDisabled - just undo, dont move
                var geoJSON = NpGeoGlobals.OL3feature_to_geoJSON(savedCopy, this.coordinateSystem);
                this.saveDataToEntity(this.Current, geoJSON);
                if (oldMode !== npMapThis.currentMode) {
                    if (oldMode === NpMapMode.NPMAP_MODE_DRAW)
                        this.enterDrawMode();
                    else if (oldMode === NpMapMode.NPMAP_MODE_HOLES) {
                        this.enterDrawMode();
                        this.currentMode = NpMapMode.NPMAP_MODE_HOLES;
                    }
                    else if (oldMode === NpMapMode.NPMAP_MODE_SELECT)
                        this.enterSelectMode();
                    else if (oldMode === NpMapMode.NPMAP_MODE_MEASURE)
                        this.enterMeasureMode(false);
                    else
                        console.error("Unexpected - unknown mode in revert()");
                }
            }
        };
        NpMap.prototype.enterSelectMode = function () {
            // console.log("entering select mode");
            this.killPopover();
            if (this.currentMode === NpMapMode.NPMAP_MODE_SELECT)
                return;
            this.currentMode = NpMapMode.NPMAP_MODE_SELECT;
            //enable the dragZoom interaction
            //this.dragZoomInteraction.setActive(true);
            this.removeFeatures(this.featureOverlaySnap);
            this.snappedVertexFeature = null;
            this.removeFeatures(this.featureOverlayTemp);
            //NpGeoGlobals.splitLayer.getSource().clear();
            this.unregitserSplitListeners();
            this.clearInteractions();
            this.addSelectInteraction();
        };
        NpMap.prototype.enterDrawMode = function () {
            // console.log("entering draw mode", this);
            this.killPopover();
            if (this.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
                return;
            }
            if (!isVoid(this.Current) && this.isDisabled(this.Current)) {
                return;
            }
            if (NpGeoGlobals.inModifyMode === true && this.currentMode === NpMapMode.NPMAP_MODE_HOLES && this.holeDraw && this.lastEditActionIsHoleAdd === true) {
                //console.log("this.holeDraw.sketchCoords_ instanceof Array",this.holeDraw.sketchCoords_ instanceof Array) 
                if (this.holeDraw.sketchCoords_ instanceof Array === true) {
                    if (this.holeDraw.sketchCoords_[0].length > 2) {
                        var feature = this.getFeature();
                        var rings = getAllRingsExceptLastFromFeat(feature);
                        //get all rings and leave the last one out
                        feature.getGeometry().setCoordinates(rings);
                    }
                }
            }
            this.currentMode = NpMapMode.NPMAP_MODE_DRAW;
            // this.dragZoomInteraction.setActive(false);
            this.removeFeatures(this.featureOverlaySnap);
            this.snappedVertexFeature = null;
            this.removeFeatures(this.featureOverlayTemp);
            this.clearInteractions();
            if (this.getGeometryType(this.Current) == "TWOPOINTSPLIT") {
                this.removeSplitLineInteraction(); //just remove it in case of
                this.addSplitLineInteraction(); //and add it back
                this.addSnapInteraction();
            }
            else {
                this.addModifyInteraction();
                this.addDrawInteraction(false); //false is no shift as only measure may have shift
                this.addSnapInteraction();
            }
        };
        NpMap.prototype.enterMeasureMode = function (shift) {
            this.killPopover();
            if (this.currentMode === NpMapMode.NPMAP_MODE_MEASURE)
                return;
            this.currentMode = NpMapMode.NPMAP_MODE_MEASURE;
            //this.dragZoomInteraction.setActive(false);
            this.removeFeatures(this.featureOverlaySnap);
            this.snappedVertexFeature = null;
            this.removeFeatures(this.featureOverlayTemp);
            this.clearInteractions();
            //console.log("shift pressed", shift);
            this.addDrawInteraction(shift); //true/false depending if press shift on init. if true measure polygon
        };
        NpMap.prototype.removeFeatures = function (fo) {
            var npMapThis = this;
            var drawnFeatures = fo.getSource().getFeaturesCollection();
            while (drawnFeatures.getLength() > 0) {
                fo.getSource().removeFeature(drawnFeatures.item(0));
                drawnFeatures = fo.getSource().getFeaturesCollection();
            }
        };
        NpMap.prototype.removeLastVertex = function () {
            var npMapThis = this;
            if ((npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW ||
                npMapThis.currentMode === NpMapMode.NPMAP_MODE_MEASURE)
                &&
                    npMapThis.drawInteraction !== undefined) {
                var fo = npMapThis.getDrawingOverlay();
                var diThis = npMapThis.drawInteraction;
                var geometry = diThis.sketchFeature_.getGeometry();
                var coordinates;
                if (diThis.mode_ === ol.interaction.DrawMode.POINT) {
                    npMapThis.removeFeatures(fo);
                }
                else if (diThis.mode_ === ol.interaction.DrawMode.LINE_STRING) {
                    coordinates = geometry.getCoordinates();
                    if (coordinates !== undefined && coordinates.length >= 2) {
                        coordinates.splice(coordinates.length - 1);
                        geometry.setCoordinates(coordinates);
                        diThis.updateSketchFeatures_();
                    }
                }
                else if (diThis.mode_ === ol.interaction.DrawMode.POLYGON) {
                    coordinates = diThis.sketchPolygonCoords_[0];
                    if (coordinates !== undefined && coordinates.length >= 2) {
                        coordinates.splice(coordinates.length - 1);
                        geometry.setCoordinates(diThis.sketchPolygonCoords_);
                        diThis.updateSketchFeatures_();
                    }
                }
            }
        };
        NpMap.prototype.cleanUndoBuffers = function () {
            // NeuroCode must generate calls to this function when the Form's 'Save' button is called.
            this.entityHistories = {};
            // Now that undo histories are gone, setup the history of the current row to the pre-existing data
            var curEntity = this.Current;
            if (!isVoid(curEntity)) {
                var geo = this.getFeature();
                if (this.entityHistories[curEntity.getKey()] === undefined) {
                    var initialValue;
                    if (!isVoid(geo))
                        initialValue = geo.clone();
                    else
                        initialValue = undefined;
                    this.entityHistories[curEntity.getKey()] = [initialValue];
                }
                this.currentEntityKey = curEntity.getKey();
            }
        };
        NpMap.prototype.constructVectorLayerButton = function (options) {
            var npMapThis = this;
            options.id = 'VectorLayer_mode';
            options.imgPath = 'veclayers.png';
            options.title = 'Thenatic Layers (F8)';
            options.onclick = function () {
                npMapThis.showVectorLayers();
            };
            options.updateState = function (self) {
                self.enable();
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructEditButton = function (options) {
            var npMapThis = this;
            options.id = 'Edit_mode';
            options.imgPath = 'edit.png';
            options.title = 'Create/Change Shape (F2)';
            options.onclick = function () {
                npMapThis.enterDrawMode();
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode !== NpMapMode.NPMAP_MODE_DRAW && (!npMapThis.isDisabled(npMapThis.Current))) {
                    self.enable();
                }
                else {
                    self.disable();
                }
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructRevertButton = function (options) {
            var npMapThis = this;
            options.id = 'Revert_mode';
            options.imgPath = 'undo.png';
            options.title = 'Revert Shape (Ctrl-z)';
            options.onclick = function () {
                npMapThis.revert();
            };
            options.updateState = function (self) {
                if ((npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW
                    || npMapThis.currentMode === NpMapMode.NPMAP_MODE_HOLES)
                    && !isVoid(npMapThis.currentEntityKey)
                    && npMapThis.entityHistories[npMapThis.currentEntityKey].length > 1) {
                    self.enable();
                }
                else {
                    self.disable();
                }
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructSaveButton = function (options) {
            var npMapThis = this;
            options.id = 'SavePDF_mode';
            options.imgPath = 'exp_map.png';
            options.title = 'Export Map';
            options.onclick = function (a, e) {
                npMapThis.saveMapAsPDF2();
                // npMapThis.buildMapExpReq(e.shiftKey);
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW || npMapThis.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                    self.enable();
                }
                else {
                    self.disable();
                }
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructMsreButton = function (options) {
            var npMapThis = this;
            options.id = 'Msre_mode';
            options.imgPath = 'msre.png';
            options.title = 'Calculate Length/Area (shift) (F4)';
            options.onclick = function (a, e) {
                npMapThis.enterMeasureMode(e.shiftKey);
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_MEASURE) {
                    self.disable();
                }
                else {
                    self.enable();
                }
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructSlctButton = function (options) {
            var npMapThis = this;
            options.id = 'Slct_mode';
            options.imgPath = 'info-sign.png';
            options.title = 'Selection/Info (F2)';
            options.onclick = function () {
                npMapThis.enterSelectMode();
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                    self.disable();
                }
                else {
                    self.enable();
                }
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructDrilHoleButton = function (options) {
            var npMapThis = this;
            options.id = 'Dril_mode';
            options.imgPath = 'hole-add.png';
            options.title = 'Δημιουργία κενού (τρύπας) στο πολύγωνο';
            options.onclick = function () {
                //npMapThis.enterDrawMode();
                npMapThis.holeDrawer();
                npMapThis.currentMode = NpMapMode.NPMAP_MODE_HOLES;
                //this.dragZoomInteraction.setActive(false);
            };
            options.updateState = function (self) {
                //hole creation may be enabled  only in edit mode and only for polygons  
                if (npMapThis.currentMode !== NpMapMode.NPMAP_MODE_DRAW || npMapThis.getGeometryType(npMapThis.Current) !== 'Polygon') {
                    self.disable();
                }
                else {
                    self.enable();
                }
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructRmveButton = function (options) {
            var npMapThis = this;
            options.id = 'Rmve_mode';
            options.imgPath = 'trash.png';
            options.title = 'Διαγραφή γεωμετρίας (Delete)';
            options.onclick = function () {
                npMapThis.removeFeatures(npMapThis.featureOverlayDraw);
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW &&
                    npMapThis.featureOverlayDraw.getSource().getFeaturesCollection().getLength() !== 0) {
                    self.enable();
                }
                else {
                    self.disable();
                }
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.getZoomSourceFeatureOverlay = function () {
            var npMapThis = this;
            var sourceFO;
            if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
                sourceFO = npMapThis.featureOverlayDraw;
            }
            else if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                if (!isVoid(npMapThis.selectInteraction))
                    sourceFO = npMapThis.selectInteraction.featureOverlay_;
                // in select mode, if nothing is selected, zoom to main feature
                if (isVoid(sourceFO) || sourceFO.getSource().getFeaturesCollection().getLength() == 0)
                    sourceFO = npMapThis.featureOverlayDraw;
            }
            return sourceFO;
        };
        NpMap.prototype.zoomToCurrentFeatures = function () {
            var npMapThis = this;
            // Choose where to zoom:
            // - in select mode, use the selectInteraction's featureOverlay_
            // - in draw mode, use the map's featureOverlay
            var sourceFO = npMapThis.getZoomSourceFeatureOverlay();
            if (isVoid(sourceFO))
                return;
            var extent;
            var modifyFeats = sourceFO.getSource().getFeaturesCollection();
            for (var i = 0; i < modifyFeats.getLength(); i++) {
                if (isVoid(extent)) {
                    extent = modifyFeats.item(i).getGeometry().getExtent();
                }
                else {
                    extent = extent.extend(modifyFeats.item(i).getGeometry().getExtent());
                }
            }
            //console.log("extent is============", extent);
            if (isVoid(extent)) {
                // Found nothing? Then try to zoom on whatever exists in the map's featureOverlay
                var featCol = sourceFO.getSource().getFeaturesCollection();
                //console.log("zoomToCurrentFeatures featCol", featCol);
                featCol.forEach(function (feature) {
                    if (isVoid(extent))
                        extent = feature.getGeometry().getExtent();
                    else
                        extent = extent.extend(feature.getGeometry().getExtent());
                });
            }
            if (isVoid(extent))
                return;
            var checkForPoint = Math.abs(extent[2] - extent[0]) + Math.abs(extent[3] - extent[1]);
            var view = npMapThis.map.getView();
            //console.log("checkForPoint", checkForPoint);
            if (checkForPoint < 10) {
                view.setCenter([extent[0], extent[1]]);
                var activeLayer = npMapThis.tileSelect.getActiveTileLayer();
                if (!isVoid(activeLayer)) {
                    //console.log("changing zoom level", activeLayer.getSource().getTileGrid().getMaxZoom() - 1);
                    view.setZoom(activeLayer.getSource().getTileGrid().getMaxZoom() - 1);
                }
            }
            else
                var mapSize = npMapThis.map.getSize();
            var mapSizeWithPadding = [mapSize[0] * 0.75, mapSize[1] * 0.75];
            view.fit(extent, mapSizeWithPadding);
        };
        NpMap.prototype.constructZoomButton = function (options) {
            var npMapThis = this;
            options.id = 'Zoom_mode';
            options.imgPath = 'zoom.png';
            options.title = 'Zoom to the Parcel (z)';
            options.onclick = function () {
                npMapThis.zoomToCurrentFeatures();
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode !== NpMapMode.NPMAP_MODE_MEASURE) {
                    self.enable();
                }
                else {
                    self.disable();
                }
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.mergeSelectedPolygon = function () {
            var npMapThis = this;
            if (this.currentMode !== NpMapMode.NPMAP_MODE_SELECT)
                return;
            var selectedFeatures = npMapThis.selectInteraction.getFeatures();
            if (selectedFeatures.getLength() == 1) {
                var selectedFeature = selectedFeatures.item(0);
                var drawnFeature = this.getFeature();
                // console.log("drawnFeature", drawnFeature.getGeometry().getType());
                // console.log("selectedFeature", selectedFeature.getGeometry().getType());
                //merge may take place only on polygons
                if (drawnFeature.getGeometry().getType() !== 'Polygon' || selectedFeature.getGeometry().getType() !== 'Polygon') {
                    messageBox(this.grp.$scope, this.Plato, "Προσοχή", "Μη αποδεκτή γεωμετρία για ένωση. (" + drawnFeature.getGeometry().getType() + " merge with  " + selectedFeature.getGeometry().getType() + ")", IconKind.ERROR, [
                        new Tuple2("OK", function () { })
                    ], 0, 0, '19em');
                    return;
                }
                var mergedFeature;
                if (isVoid(drawnFeature))
                    mergedFeature = drawnFeature;
                else {
                    var merged = mergePolygons(NpGeoGlobals.OL3feature_to_coordinates(drawnFeature), NpGeoGlobals.OL3feature_to_coordinates(selectedFeature));
                    mergedFeature = NpGeoGlobals.coordinates_to_OL3feature(merged);
                }
                this.featureOverlayDraw.getSource().clear();
                this.featureOverlayDraw.getSource().addFeature(mergedFeature);
                this.enterDrawMode();
            }
        };
        NpMap.prototype.copySelectedPolygon = function () {
            if (this.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                var selectedFeatures = this.selectInteraction.getFeatures();
                if (selectedFeatures.getLength() == 1)
                    this.copiedFeature = selectedFeatures.item(0).clone();
            }
            else if (this.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
                var feature = this.getFeature();
                if (!isVoid(feature))
                    this.copiedFeature = feature.clone();
            }
        };
        NpMap.prototype.pastePolygon = function () {
            //check if geometry types equal (paste polygon to polygon, point to point ...etc)
            if (this.copiedFeature.getGeometry().getType() !== this.getGeometryType(this.Current)) {
                messageBox(this.grp.$scope, this.Plato, "Προσοχή", "Shape is not suitable for pasting. (" + this.copiedFeature.getGeometry().getType() + " to  " + this.getGeometryType(this.Current) + ")", IconKind.ERROR, [
                    new Tuple2("OK", function () { })
                ], 0, 0, '19em');
                return;
            }
            this.setFeature(this.copiedFeature.clone());
            this.entityHistories[this.currentEntityKey].push(NpGeoGlobals.safeClone(this.copiedFeature));
            var geoJSON = NpGeoGlobals.OL3feature_to_geoJSON(this.copiedFeature, this.coordinateSystem);
            this.saveDataToEntity(this.Current, geoJSON);
        };
        NpMap.prototype.refreshVectorLayers = function () {
            //console.log("refreshing vector layers. Clear all snap feats");
            this.snapFeats.clear();
            //this.snapFeats = new ol.Collection([]);
            var self = this;
            this.layers.
                filter(function (x) { return x.isVisible && (x instanceof NpGeoLayers.SqlVectorLayer); }).
                forEach(function (x) {
                var cacheKey = self.mapId + "#@#" + x.layerId;
                for (var key in NpMap.cachedVectorsOfMap) {
                    if (key.startsWith(cacheKey)) {
                        delete NpMap.cachedVectorsOfMap[key];
                    }
                }
                x.source.clear();
            });
            this.layers.
                filter(function (x) { return x.isVisible && (x instanceof NpGeoLayers.SqlVectorLayer); }).
                forEach(function (x) {
                //console.log("call fetchLayerGeometries case 1");
                x.fetchLayerGeometries();
            });
        };
        NpMap.prototype.showSelectSearchAndTargetLayerDialog = function () {
            var self = this;
            var dialogOptions = new NpTypes.DialogOptions();
            var msg = "Select Search & Results Layers";
            dialogOptions.title = msg;
            dialogOptions.width = "18em";
            dialogOptions.className = "ChooseSearchLayersDialog";
            dialogOptions.npMapThis = this;
            dialogOptions.onOk = function (searchLayer, otherLayer) {
                searchLayer.showSearchDialog(otherLayer);
            };
            var scope = this.grp.$scope;
            this.Plato.showDialog(scope, "Search", "/" + scope.globals.appName + '/partials/chooseSearchLayers.html?rev=' + scope.globals.version, dialogOptions);
        };
        NpMap.prototype.commonCropAndIntersect = function (func) {
            //console.log("commonCropAndIntersect");
            if (this.currentMode !== NpMapMode.NPMAP_MODE_DRAW)
                return;
            var npMapThis = this;
            var drawnFeature = this.getFeature();
            //console.log("drawnFeature", drawnFeature);
            if (isVoid(drawnFeature))
                return;
            var drawnGeometry = drawnFeature.getGeometry();
            if (drawnGeometry instanceof ol.geom.Polygon) {
                var polygonsToSubtract = [];
                npMapThis.layers.forEach(function (l) {
                    if (l.isVector && l.isVisible) {
                        var fs = l.source.getFeatures();
                        fs.forEach(function (f) {
                            var polygonInCropForm = NpGeoGlobals.OL3feature_to_coordinates(f);
                            if (polygonInCropForm.length > 0)
                                polygonsToSubtract = polygonsToSubtract.concat(polygonInCropForm);
                        });
                    }
                });
                var cropped = func(NpGeoGlobals.OL3feature_to_coordinates(drawnFeature), polygonsToSubtract);
                //console.log("cropped", cropped);
                this.featureOverlayDraw.getSource().clear();
                if (cropped.length > 0) {
                    var croppedFeature = NpGeoGlobals.coordinates_to_OL3feature(cropped);
                    //console.log("croppedFeature2", croppedFeature);
                    this.featureOverlayDraw.getSource().addFeature(croppedFeature);
                }
                this.enterDrawMode();
            }
        };
        // cropOverlaps() {
        //     this.commonCropAndIntersect(subtractPolygonsFromPolygon);
        // }
        /**
         * @p.tsagkis
         * remove holes out of the currently drawn feature
         */
        NpMap.prototype.removeHoles = function () {
            if (this.currentMode !== NpMapMode.NPMAP_MODE_DRAW)
                return;
            var npMapThis = this;
            var drawnFeature = this.getFeature();
            if (isVoid(drawnFeature))
                return;
            var drawnGeometry = drawnFeature.getGeometry();
            if (!(drawnGeometry instanceof ol.geom.Polygon))
                return;
            //just keep the outer ring 
            drawnGeometry.setCoordinates([drawnGeometry.getCoordinates()[0]]);
        };
        /**
         * @p.tsagkis
         * function to build the legend content
         * and show the dialog containg the display info and styles
         */
        NpMap.prototype.showLegendDialog = function () {
            var npMapThis = this;
            if (isVoid(this.legendFnc)) {
                npMapThis.legendFnc = function () {
                    return NpGeoGlobals.defaultLegendFnc;
                };
            }
            var myLegendData = npMapThis.legendFnc();
            var scope = this.grp.$scope;
            //close the dialog (LegendDialog css class) if any exist
            $(".LegendDialog").dialog("close");
            var dialogOptions = new NpTypes.DialogOptions();
            dialogOptions.width = "20em";
            dialogOptions.className = "LegendDialog";
            dialogOptions.title = myLegendData[0].legendTitle;
            dialogOptions.legendData = myLegendData;
            //console.log("dialogOptions", dialogOptions);
            dialogOptions.modal = false;
            //console.log("dialogOptions", dialogOptions);
            dialogOptions.npMapThis = this;
            this.Plato.showDialog(scope, dialogOptions.title, "/" + scope.globals.appName + '/partials/legend.html?rev=' + scope.globals.version, dialogOptions);
        };
        /**
         * action for topolgical related
         * functions and correctness
         * @param shift
         */
        NpMap.prototype.executeTopoFn = function (shift) {
            if (this.topologyType === 1) {
                this.intersectOverlapsClient(shift);
            }
            else if (this.topologyType === 2) {
                this.intersectOverlapsCustom();
            }
            else {
                console.info("no topology type has been defined. Its value is:", this.topologyType);
            }
        };
        /**
         * @p.tsagkis
         * One more method completely recustructed
         * as geometry transformations on client causes problems
         * now all the geometry transformations are taking place on the DB
         * @param {Boolean} selectExclusionLayers (indicates whether shift key was pressed or not)
         */
        NpMap.prototype.intersectOverlapsCustom = function () {
            var _this = this;
            var npMapThis = this;
            var scope = npMapThis.grp.$scope;
            if (this.currentMode !== NpMapMode.NPMAP_MODE_DRAW)
                return;
            var drawnFeature = this.getFeature();
            //phase one is the inclusive layers
            if (!isVoid(npMapThis.getLevel1Features)) {
                npMapThis.getLevel1Features(drawnFeature, function (ilotIntFeatures) {
                    //there is some kind of problem here 
                    if (ilotIntFeatures.length === 0) {
                        messageBox(npMapThis.grp.$scope, npMapThis.Plato, "Warning", "Not found ilot in the digitized area", IconKind.ERROR, [
                            new Tuple2("OK", function () { })
                        ], 0, 0, '19em');
                        return;
                    }
                    else if (ilotIntFeatures.length === 1) {
                        var f = npMapThis.featureOverlayDraw.getSource().getFeaturesCollection();
                        var newFeature = new ol.Feature();
                        newFeature.setGeometryName("");
                        newFeature.setGeometry(ilotIntFeatures[0].getGeometry());
                        f.clear();
                        f.push(newFeature);
                        npMapThis.featureOverlayCrop.getSource().clear();
                        phaseTwo();
                    }
                    else {
                        var featsToChoose = [];
                        ilotIntFeatures.forEach(function (f) {
                            featsToChoose.push(f);
                        });
                        var dialogOptions = new NpTypes.DialogOptions();
                        dialogOptions.title = "Επιλογή τμήματος γεωμετρίας";
                        dialogOptions.width = "24em";
                        dialogOptions.className = "ChooseLevel1GeomDialog";
                        dialogOptions.onCloseDialog = function () {
                            npMapThis.featureOverlayCrop.getSource().clear();
                            phaseTwo();
                        };
                        dialogOptions.npMapThis = npMapThis;
                        dialogOptions.polygonsRemaining = featsToChoose;
                        var scope = npMapThis.grp.$scope;
                        npMapThis.Plato.showDialog(scope, "Select Geometry's Part", "/" + scope.globals.appName + '/partials/chooseLevel1Geoms.html?rev=' + scope.globals.version, dialogOptions);
                    }
                });
            }
            var phaseTwo = function () {
                var drawnFeature = _this.getFeature();
                if (!isVoid(npMapThis.getLevel2Features)) {
                    npMapThis.getLevel2Features(drawnFeature, function (intFeatures) {
                        //console.log("intFeatures=========", intFeatures);
                        if (intFeatures.length === 0)
                            return;
                        else {
                            var featsToChoose = [];
                            intFeatures.forEach(function (f) {
                                featsToChoose.push(f);
                            });
                            var dialogOptions = new NpTypes.DialogOptions();
                            dialogOptions.title = "Select Geometry's Part";
                            dialogOptions.width = "24em";
                            dialogOptions.className = "ChooseLevel2GeomDialog";
                            dialogOptions.onCloseDialog = function () {
                                npMapThis.featureOverlayCrop.getSource().clear();
                                //phaseTwo();
                            };
                            dialogOptions.npMapThis = npMapThis;
                            dialogOptions.polygonsRemaining = featsToChoose;
                            var scope = npMapThis.grp.$scope;
                            npMapThis.Plato.showDialog(scope, "Select Geometry's Part", "/" + scope.globals.appName + '/partials/chooseLevel2Geoms.html?rev=' + scope.globals.version, dialogOptions);
                        }
                    });
                }
            };
        };
        /**
         * @p.tsagkis DEPRECATED - NOT USED
         * Method completely recustructed
         * to fit the needs of AE
         * @param {Boolean} selectExclusionLayers (indicates whether shift key was pressed or not)
         */
        NpMap.prototype.intersectOverlaps2 = function (selectExclusionLayers) {
            var _this = this;
            var npMapThis = this;
            //get and set exclusive layers according to session storage and config
            npMapThis.layers.
                filter(function (l) { return l instanceof NpGeoLayers.SqlVectorLayer; }).
                filter(function (l) {
                if (l.exclusive === true || l.exclusive === undefined)
                    return true;
                // return  l.exclusive === undefined;
            }).
                map(function (l) {
                var isUsedForExclusions = NpGeoLayers.getSetting(l.layerId + l.label, "isUsedForExclusions", true);
                l.setUsedForExclusions(isUsedForExclusions);
            });
            if (this.currentMode !== NpMapMode.NPMAP_MODE_DRAW)
                return;
            var drawnFeature = this.getFeature();
            //console.log("about to call our new method");
            //this.getCorrectedGeometries(drawnFeature);
            //return;
            if (isVoid(drawnFeature))
                return;
            var drawnGeometry = drawnFeature.getGeometry();
            if (!(drawnGeometry instanceof ol.geom.Polygon))
                return;
            // now its time to go through the inclusive layers
            var polygonsToIntersect = [];
            var coordinatesOfDrawnFeature = NpGeoGlobals.OL3feature_to_coordinates(drawnFeature);
            //phase one is the inclusive layers
            var phaseOne = function () {
                var polygonIntersectionsWithInclusive = [];
                var inclusiveLayers = npMapThis.layers.filter(function (x) { return x.isVisible &&
                    (x instanceof NpGeoLayers.SqlVectorLayer) &&
                    x.inclusive === true; });
                if (inclusiveLayers.length > 0) {
                    inclusiveLayers.forEach(function (l) {
                        var fs = l.source.getFeatures();
                        fs.forEach(function (f) {
                            var polygonInCropForm = NpGeoGlobals.OL3feature_to_coordinates(f);
                            if (polygonInCropForm.length > 0) {
                                var cropped = keepIntersectionsOfPolygons(coordinatesOfDrawnFeature, polygonInCropForm);
                                if (cropped.length > 0) {
                                    polygonIntersectionsWithInclusive.push(cropped);
                                }
                            }
                        });
                    });
                }
                else {
                    polygonIntersectionsWithInclusive = [coordinatesOfDrawnFeature];
                }
                // console.log("polygonIntersectionsWithInclusive length====", polygonIntersectionsWithInclusive.length);
                if (polygonIntersectionsWithInclusive.length > 0) {
                    if (polygonIntersectionsWithInclusive.length === 1) {
                        var croppedFeature = NpGeoGlobals.coordinates_to_OL3feature(polygonIntersectionsWithInclusive[0]);
                        _this.featureOverlayDraw.getSource().clear();
                        _this.featureOverlayDraw.getSource().addFeature(croppedFeature);
                        phaseTwo();
                    }
                    else {
                        var dialogOptions = new NpTypes.DialogOptions();
                        dialogOptions.title = "Select Geometry's Part";
                        dialogOptions.width = "18em";
                        dialogOptions.className = "ChooseGeomDialog";
                        dialogOptions.onCloseDialog = function () {
                            npMapThis.featureOverlayCrop.getSource().clear();
                            phaseTwo();
                        };
                        dialogOptions.npMapThis = npMapThis;
                        dialogOptions.polygonsRemaining = polygonIntersectionsWithInclusive;
                        var scope = npMapThis.grp.$scope;
                        npMapThis.Plato.showDialog(scope, "Select Geometry's Part", "/" + scope.globals.appName + '/partials/chooseGeom.html?rev=' + scope.globals.version, dialogOptions);
                    }
                }
                else {
                    phaseTwo();
                }
            };
            //and here is the exclusive layers
            var phaseTwo = function () {
                var polygonsRemaining = [];
                var displayLayer = [];
                var displayLayerId = [];
                var drawnFeature = _this.getFeature();
                var drawnGeom = drawnFeature.getGeometry().clone();
                var polygonIntersectionsWithExclusive = [];
                var coordinatesOfDrawnFeature = NpGeoGlobals.OL3feature_to_coordinates(drawnFeature);
                var exclusiveLayers = npMapThis.layers.filter(function (x) { return x.isVisible &&
                    (x instanceof NpGeoLayers.SqlVectorLayer) &&
                    //(<NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>>x).exclusive === true);
                    x.exclusiveFilter === true; });
                if (exclusiveLayers.length > 0) {
                    exclusiveLayers.forEach(function (l) {
                        var fs = l.layer.getSource().getFeatures();
                        fs.forEach(function (f) {
                            var polygonInCropForm = NpGeoGlobals.OL3feature_to_coordinates(f);
                            var cropped = keepIntersectionsOfPolygons(NpGeoGlobals.OL3feature_to_coordinates(drawnFeature), polygonInCropForm);
                            //console.log("cropped.length", cropped.length);
                            if (cropped.length > 0) {
                                coordinatesOfDrawnFeature = cropped;
                            }
                            else {
                                coordinatesOfDrawnFeature = undefined;
                            }
                            // console.log("coordinatesOfDrawnFeature", coordinatesOfDrawnFeature);
                            if (!isVoid(coordinatesOfDrawnFeature)) {
                                //console.log("coordinatesOfDrawnFeature area is ", (<any>NpGeoGlobals.coordinates_to_OL3feature(coordinatesOfDrawnFeature).getGeometry()).getArea());
                                polygonsRemaining.push(coordinatesOfDrawnFeature);
                                //console.log("pushing coordinatesOfDrawnFeature", coordinatesOfDrawnFeature);
                                displayLayer.push(l.label);
                            }
                        });
                    });
                    if (polygonsRemaining.length === 0)
                        return;
                    var dialogOptions = new NpTypes.DialogOptions();
                    dialogOptions.title = "Select Geometry's Part";
                    dialogOptions.width = "20em";
                    dialogOptions.className = "ChooseExcGeomDialog";
                    dialogOptions.onCloseDialog = function () {
                        npMapThis.featureOverlayCrop.getSource().clear();
                    };
                    dialogOptions.npMapThis = npMapThis;
                    dialogOptions.polygonsRemaining = polygonsRemaining;
                    dialogOptions.displayLayer = displayLayer;
                    dialogOptions.startUpGeom = drawnFeature.getGeometry();
                    var scope = npMapThis.grp.$scope;
                    npMapThis.Plato.showDialog(scope, "Select Geometry's Part", "/" + scope.globals.appName + '/partials/chooseExcGeom.html?rev=' + scope.globals.version, dialogOptions);
                }
                else {
                    polygonIntersectionsWithExclusive = [coordinatesOfDrawnFeature];
                }
                _this.enterDrawMode();
            };
            //case of shift pressed so recunstract exclusive layers
            if (selectExclusionLayers) {
                var exclusiveUndefinedLayers = npMapThis.layers.filter(function (x) { return x.isVisible &&
                    (x instanceof NpGeoLayers.SqlVectorLayer) &&
                    x.exclusive === undefined; }); // @ptsagkis edit
                //(<NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>>x).exclusiveFilter === undefined);
                if (exclusiveUndefinedLayers.length > 0) {
                    var dialogOptions = new NpTypes.DialogOptions();
                    dialogOptions.title = "Select Filters";
                    dialogOptions.width = "18em";
                    dialogOptions.className = "ChooseExclusiveLayersDialog";
                    dialogOptions.onCloseDialog = function () {
                        phaseOne();
                    };
                    dialogOptions.npMapThis = npMapThis;
                    dialogOptions.layers = exclusiveUndefinedLayers;
                    var scope = npMapThis.grp.$scope;
                    npMapThis.Plato.showDialog(scope, "Select Filters", "/" + scope.globals.appName + '/partials/chooseExclusiveLayers.html?rev=' + scope.globals.version, dialogOptions);
                }
            }
            else {
                phaseOne();
            }
        };
        /**
         *
         * @param selectExclusionLayers
         */
        NpMap.prototype.intersectOverlapsClient = function (selectExclusionLayers) {
            var _this = this;
            var npMapThis = this;
            npMapThis.layers.
                filter(function (l) { return l instanceof NpGeoLayers.SqlVectorLayer; }).
                filter(function (l) {
                if (l.exclusive === true || l.exclusive === undefined)
                    return true;
                // return  l.exclusive === undefined;
            }).
                map(function (l) {
                var isUsedForExclusions = NpGeoLayers.getSetting(l.layerId + l.label, "isUsedForExclusions", true);
                l.setUsedForExclusions(isUsedForExclusions);
            });
            //console.log("intersectOverlaps selectExclusionLayers", selectExclusionLayers);
            if (this.currentMode !== NpMapMode.NPMAP_MODE_DRAW)
                return;
            var drawnFeature = this.getFeature();
            if (isVoid(drawnFeature))
                return;
            var drawnGeometry = drawnFeature.getGeometry();
            if (!(drawnGeometry instanceof ol.geom.Polygon))
                return;
            var polygonsToIntersect = [];
            var coordinatesOfDrawnFeature = NpGeoGlobals.OL3feature_to_coordinates(drawnFeature);
            var polygonIntersectionsWithInclusive = [];
            var inclusiveLayers = npMapThis.layers.filter(function (x) { return x.isVisible &&
                (x instanceof NpGeoLayers.SqlVectorLayer) &&
                x.inclusive === true; });
            if (inclusiveLayers.length > 0) {
                inclusiveLayers.forEach(function (l) {
                    var fs = l.source.getFeatures();
                    fs.forEach(function (f) {
                        var polygonInCropForm = NpGeoGlobals.OL3feature_to_coordinates(f);
                        if (polygonInCropForm.length > 0) {
                            var cropped = keepIntersectionsOfPolygons(coordinatesOfDrawnFeature, polygonInCropForm);
                            if (cropped.length > 0) {
                                polygonIntersectionsWithInclusive.push(cropped);
                            }
                        }
                    });
                });
            }
            else {
                polygonIntersectionsWithInclusive = [coordinatesOfDrawnFeature];
            }
            var phaseTwo = function () {
                var polygonsRemaining = [];
                var exclusiveLayers = npMapThis.layers.filter(function (x) { return x.isVisible &&
                    (x instanceof NpGeoLayers.SqlVectorLayer) &&
                    //(<NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>>x).exclusive === true);
                    x.exclusiveFilter === true; });
                //console.log("exclusiveLayers.length", exclusiveLayers.length);
                if (exclusiveLayers.length > 0) {
                    polygonIntersectionsWithInclusive.forEach(function (p) {
                        exclusiveLayers.forEach(function (l) {
                            var fs = l.source.getFeatures();
                            fs.forEach(function (f) {
                                if (isVoid(p))
                                    return;
                                var polygonInCropForm = NpGeoGlobals.OL3feature_to_coordinates(f);
                                if (polygonInCropForm.length > 0) {
                                    var cropped = subtractPolygonsFromPolygon(p, polygonInCropForm);
                                    if (cropped.length > 0)
                                        p = cropped;
                                    else
                                        p = undefined;
                                }
                            });
                        });
                        if (!isVoid(p))
                            polygonsRemaining.push(p);
                    });
                }
                else
                    polygonsRemaining = polygonIntersectionsWithInclusive;
                if (polygonsRemaining.length > 0) {
                    if (polygonsRemaining.length === 1) {
                        var croppedFeature = NpGeoGlobals.coordinates_to_OL3feature(polygonsRemaining[0]);
                        _this.featureOverlayDraw.getSource().clear();
                        _this.featureOverlayDraw.getSource().addFeature(croppedFeature);
                    }
                    else {
                        var dialogOptions = new NpTypes.DialogOptions();
                        dialogOptions.title = "Επιλογή τμήματος γεωμετρίας";
                        dialogOptions.width = "18em";
                        dialogOptions.className = "ChooseGeomDialog";
                        dialogOptions.onCloseDialog = function () {
                            npMapThis.featureOverlayCrop.getSource().clear();
                            npMapThis.enterDrawMode();
                        };
                        dialogOptions.npMapThis = npMapThis;
                        dialogOptions.polygonsRemaining = polygonsRemaining;
                        var scope = npMapThis.grp.$scope;
                        npMapThis.Plato.showDialog(scope, "Επιλογή τμήματος γεωμετρίας", "/" + scope.globals.appName + '/partials/chooseGeom.html?rev=' + scope.globals.version, dialogOptions);
                    }
                }
                _this.enterDrawMode();
            };
            if (selectExclusionLayers) {
                var exclusiveUndefinedLayers = npMapThis.layers.filter(function (x) { return x.isVisible &&
                    (x instanceof NpGeoLayers.SqlVectorLayer) &&
                    x.exclusive === undefined; }); // @ptsagkis edit
                //(<NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>>x).exclusiveFilter === undefined);
                if (exclusiveUndefinedLayers.length > 0) {
                    var dialogOptions = new NpTypes.DialogOptions();
                    dialogOptions.title = "Επιλογή φιλτραρίσματος";
                    dialogOptions.width = "18em";
                    dialogOptions.className = "ChooseExclusiveLayersDialog";
                    dialogOptions.onCloseDialog = function () {
                        phaseTwo();
                    };
                    dialogOptions.npMapThis = npMapThis;
                    dialogOptions.layers = exclusiveUndefinedLayers;
                    var scope = npMapThis.grp.$scope;
                    npMapThis.Plato.showDialog(scope, "Επιλογή φιλτραρίσματος", "/" + scope.globals.appName + '/partials/chooseExclusiveLayers.html?rev=' + scope.globals.version, dialogOptions);
                }
            }
            else
                phaseTwo();
        };
        NpMap.prototype.showHoleDialog = function () {
            var npMapThis = this;
            var dialogOptions = new NpTypes.DialogOptions();
            dialogOptions.title = "Εισαγωγή πολυγώνου με κενά";
            dialogOptions.width = "23em";
            dialogOptions.className = "CreateHoleDialog";
            dialogOptions.npMapThis = npMapThis;
            var scope = npMapThis.grp.$scope;
            npMapThis.Plato.showDialog(scope, "Εισαγωγή πολυγώνου με κενά", "/" + scope.globals.appName + '/partials/holes.html?rev=' + scope.globals.version, dialogOptions);
        };
        NpMap.prototype.showJumpDialog = function () {
            var npMapThis = this;
            var dialogOptions = new NpTypes.DialogOptions();
            dialogOptions.title = "Εισαγωγή συντεταγμένων";
            dialogOptions.width = "19em";
            dialogOptions.className = "JumpToCoordDialog";
            var centerX = npMapThis.map.getViewport().clientWidth / 2.;
            var centerY = npMapThis.map.getViewport().clientHeight / 2.;
            var coords = npMapThis.map.getCoordinateFromPixel([centerX, centerY]);
            var srcProj = 'EPSG:3857';
            var dstProj = NpGeoGlobals.commonCoordinateSystems(npMapThis.coordinateSystem);
            coords = proj4(srcProj, dstProj, coords);
            dialogOptions.coords = coords;
            dialogOptions.npMapThis = npMapThis;
            var scope = npMapThis.grp.$scope;
            npMapThis.Plato.showDialog(scope, "Input Coordinates", "/" + scope.globals.appName + '/partials/coords.html?rev=' + scope.globals.version, dialogOptions);
        };
        NpMap.prototype.jumpToCoordinates_EPSG3857 = function (x, y) {
            var view = this.map.getView();
            view.setCenter([x, y]);
        };
        NpMap.prototype.toggleSnap = function () {
            this.snapEnabled = !this.snapEnabled;
            this.snapInteraction.setActive(this.snapEnabled); //enable disable the snap interaction
        };
        NpMap.prototype.toggleInfo = function () {
            this.infoEnabled = !this.infoEnabled;
        };
        NpMap.prototype.constructJumpButton = function (options) {
            var npMapThis = this;
            options.id = 'Jump_mode';
            options.imgPath = 'jump.png';
            options.title = 'Jump to Location (F7)';
            var Plato = options.Plato;
            options.onclick = function () {
                npMapThis.showJumpDialog();
            };
            options.updateState = function (self) {
                self.enable();
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructPolyHoleMakerButton = function (options) {
            var npMapThis = this;
            options.id = 'Hole_mode';
            options.imgPath = 'hole.png';
            options.title = 'Create Polygon with Holes';
            var Plato = options.Plato;
            options.onclick = function () {
                npMapThis.showHoleDialog();
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW)
                    self.enable();
                else
                    self.disable();
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        // constructCropButton(options) {
        //     var npMapThis = this;
        //     options.id = 'Crop_mode';
        //     options.imgPath = 'crop.png';
        //     options.title = 'Αφαίρεση επικαλύψεων πολυγώνου';
        //     var Plato:Services.INpDialogMaker = options.Plato;
        //     options.onclick = function() {
        //         npMapThis.cropOverlaps();
        //     };
        //     options.updateState = function(self) {
        //         if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW)
        //             self.enable();
        //         else
        //             self.disable();
        //     };
        //     CommonBtnControl(options, options.controlThis);
        // }
        NpMap.prototype.constructHoleRemoverButton = function (options) {
            var npMapThis = this;
            options.id = 'HoleRemove_mode';
            options.imgPath = 'hole-remove.png';
            options.title = 'Remove Holes of the Polygon';
            var Plato = options.Plato;
            options.onclick = function (b, e) {
                npMapThis.removeHoles();
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode !== NpMapMode.NPMAP_MODE_DRAW || npMapThis.getGeometryType(npMapThis.Current) !== 'Polygon') {
                    self.disable();
                }
                else {
                    self.enable();
                }
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructIntersectButton = function (options) {
            var npMapThis = this;
            options.id = 'Intersect_mode';
            options.imgPath = 'intersect.png';
            options.title = 'Apply Topology'; //'Διατήρηση μόνο των επιτρεπόμενων επικαλύψεων του πολυγώνου';
            var Plato = options.Plato;
            options.onclick = function (b, e) {
                npMapThis.executeTopoFn(e.shiftKey);
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW)
                    self.enable();
                else
                    self.disable();
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructMergeButton = function (options) {
            var npMapThis = this;
            options.id = 'Merge_mode';
            options.imgPath = 'merge.png';
            options.title = 'Join with Selected Polygon';
            var Plato = options.Plato;
            options.onclick = function () {
                npMapThis.mergeSelectedPolygon();
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                    var selectedFeatures = npMapThis.selectInteraction.getFeatures();
                    if (selectedFeatures.getLength() == 1) {
                        self.enable();
                    }
                    else
                        self.disable();
                }
                else
                    self.disable();
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructCopyButton = function (options) {
            var npMapThis = this;
            options.id = 'Copy_mode';
            options.imgPath = 'copy.png';
            options.title = 'Copy';
            var Plato = options.Plato;
            options.onclick = function () {
                npMapThis.copySelectedPolygon();
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_SELECT) {
                    var selectedFeatures = npMapThis.selectInteraction.getFeatures();
                    if (selectedFeatures.getLength() == 1)
                        self.enable();
                    else
                        self.disable();
                }
                else if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
                    var feature = npMapThis.getFeature();
                    if (!isVoid(feature))
                        self.enable();
                    else
                        self.disable();
                }
                else
                    self.disable();
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructPasteButton = function (options) {
            var npMapThis = this;
            options.id = 'Paste_mode';
            options.imgPath = 'paste.png';
            options.title = 'Paste';
            var Plato = options.Plato;
            options.onclick = function () {
                npMapThis.pastePolygon();
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW && !isVoid(npMapThis.copiedFeature))
                    self.enable();
                else
                    self.disable();
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructRefreshButton = function (options) {
            var npMapThis = this;
            options.id = 'Refresh_mode';
            options.imgPath = 'refresh.png';
            options.title = 'Refresh Geometries';
            var Plato = options.Plato;
            options.onclick = function (a, e) {
                npMapThis.refreshVectorLayers();
                // console.log("refresh layer was shift e===", e.shiftKey);
                //remove any temporary imports if shift pressed 
                if (e.shiftKey === true && npMapThis.getOLLayerByName("importsLayer") !== null) {
                    NpGeoGlobals.importsLayer.getSource().clear();
                }
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW)
                    self.enable();
                else
                    self.disable();
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructSearchButton = function (options) {
            var npMapThis = this;
            options.id = 'Search_mode';
            options.imgPath = 'find.png';
            options.title = 'Search Geometries';
            var Plato = options.Plato;
            options.onclick = function () {
                npMapThis.showSelectSearchAndTargetLayerDialog();
            };
            options.updateState = function (self) {
                self.enable();
                // if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW ||
                // self.disable();
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructSnapButton = function (options) {
            var npMapThis = this;
            options.id = 'SnapToggle_mode';
            options.imgPath = 'snap.png';
            options.title = 'Snap to Nodes of Polygons on Other Layers';
            var Plato = options.Plato;
            options.onclick = function () {
                npMapThis.toggleSnap();
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
                    self.element.style.display = 'block'; //show it either way
                    if (npMapThis.snapEnabled) {
                        self.enable();
                        self.setHoverText('DISABLE Snapping to Nodes of Polygons on Other Layers');
                    }
                    else {
                        self.disable(false); // Dont hide, just 'disable' it (i.e. de-press it)
                        self.allowClicks();
                        self.setHoverText('ENABLE Snapping to Nodes of Polygons on Other Layers');
                    }
                }
                else {
                    self.disable(true); // Hide it completely
                    self.allowClicks();
                    self.setHoverText('ENABLE Snapping to Nodes of Polygons on Other Layers');
                }
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        //@p.tsagkis snap interaction start
        //constructSnapIntButton(options) {
        //    var npMapThis = this;
        //    options.id = 'SnapTIntoggle_mode';
        //    options.imgPath = 'snap.png';
        //    options.title = 'Snap Interaction σε κορυφές πολυγώνων άλλων layers';
        //    var Plato: Services.INpDialogMaker = options.Plato;
        //    options.onclick = function () {
        //        npMapThis.toggleSnap();
        //    };
        //    options.updateState = function (self) {
        //        if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW) {
        //            if (npMapThis.snapEnabled) {
        //                self.enable();
        //                self.setHoverText(
        //                    'Πιέστε για ΑΠΕΝΕΡΓΟΠΟΙΗΣΗ του snap Interaction σε κορυφές πολυγώνων άλλων layers');
        //            } else {
        //                self.disable(false); // Dont hide, just 'disable' it (i.e. de-press it)
        //                self.allowClicks();
        //                self.setHoverText(
        //                    'Πιέστε για ΕΝΕΡΓΟΠΟΙΗΣΗ του snap Interaction σε κορυφές πολυγώνων άλλων layers');
        //            }
        //        } else {
        //            self.disable(true); // Hide it completely
        //            self.allowClicks();
        //            self.setHoverText(
        //                'Πιέστε για ΕΝΕΡΓΟΠΟΙΗΣΗ του snap Interaction σε κορυφές πολυγώνων άλλων layers');
        //        }
        //    };
        //    NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        //}
        //@p.tsagkis snap interaction end
        // constructInfoToggleButton(options) {
        //     var npMapThis = this;
        //     options.id = 'InfoToggle_mode';
        //     options.imgPath = 'infoToggle.png';
        //     options.title = 'Πιέστε για ΑΠΕΝΕΡΓΟΠΟΙΗΣΗ πληροφοριών πολυγώνων';
        //     var Plato:Services.INpDialogMaker = options.Plato;
        //     options.onclick = function() {
        //         npMapThis.toggleInfo();
        //     };
        //     options.updateState = function(self) {
        //         if (npMapThis.infoEnabled) {
        //             self.enable();
        //             self.setHoverText(
        //                 'Πιέστε για ΑΠΕΝΕΡΓΟΠΟΙΗΣΗ πληροφοριών πολυγώνων');
        //         } else {
        //             self.disable();
        //             self.allowClicks();
        //             self.setHoverText(
        //                 'Πιέστε για ΕΝΕΡΓΟΠΟΙΗΣΗ πληροφοριών πολυγώνων');
        //         }
        //     };
        //     CommonBtnControl(options, options.controlThis);
        // }
        NpMap.prototype.constructLegendControl = function (options) {
            var npMapThis = this;
            options.id = 'Legend_mode';
            options.imgPath = 'legend.png';
            options.title = 'Legend';
            var Plato = options.Plato;
            options.onclick = function () {
                //npMapThis.showSelectSearchAndTargetLayerDialog();
                npMapThis.showLegendDialog();
            };
            options.updateState = function (self) {
                self.enable();
                // if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW ||
                // self.disable();
            };
            NpGeoCustControls.CommonBtnControl(options, options.controlThis);
        };
        NpMap.prototype.constructDigiInfoControl = function (opt_options) {
            var options = opt_options || {
                state: {
                    bottomOffset: 5,
                    leftSide: true
                }
            };
            var state = options.state;
            var bottomOffset = state.bottomOffset;
            var npMapThis = this;
            var this_ = options.controlThis;
            var span = document.createElement('span');
            span.id = 'digiInfoCntrlSpan';
            var spanjq = $(span);
            //to center on div
            spanjq.css('display', 'table');
            spanjq.css('margin', '0 auto');
            span.innerHTML = "";
            var element = document.createElement('div');
            element.id = 'digiInfoCntrl';
            var elementjq = $(element);
            elementjq.css('bottom', '10px');
            elementjq.css('left', '0');
            elementjq.css('max-width', '300px');
            element.className = 'NpMapCoordSpan ol-unselectable';
            element.appendChild(span);
            this_.updateState = function () {
                options.updateState(this_);
            };
            options.updateState = function (self) {
                if (npMapThis.currentMode === NpMapMode.NPMAP_MODE_DRAW ||
                    npMapThis.currentMode === NpMapMode.NPMAP_MODE_HOLES) {
                    //enable it
                    $('#digiInfoCntrl').show();
                }
                else {
                    //hide it
                    $('#digiInfoCntrl').hide();
                }
            };
            ol.control.Control.call(this_, {
                element: element,
                target: options.target
            });
        };
        NpMap.prototype.constructSpanControl = function (opt_options) {
            var options = opt_options || {
                state: {
                    bottomOffset: 5,
                    leftSide: true
                }
            };
            var state = options.state;
            var bottomOffset = state.bottomOffset;
            state.bottomOffset += 20;
            var npMapThis = this;
            var this_ = options.controlThis;
            var span = document.createElement('span');
            span.id = 'coordSpanCtrl';
            span.innerHTML = "";
            var element = document.createElement('div');
            var elementjq = $(element);
            elementjq.css('bottom', bottomOffset + 'px');
            element.className = 'NpMapCoordSpan ol-unselectable';
            element.appendChild(span);
            ol.control.Control.call(this_, {
                element: element,
                target: options.target
            });
        };
        NpMap.prototype.addButton = function (npMapBtn) {
            this.controls.push(npMapBtn.btnInstance);
        };
        NpMap.prototype.formatLength = function (line) {
            var coordinateSystem = fallback(this.coordinateSystem, 'EPSG:2100');
            var transLine = line.clone().transform(ol.proj.get('EPSG:3857'), ol.proj.get(coordinateSystem));
            var length = Math.round(transLine.getLength() * 100) / 100;
            var output;
            if (length > 100) {
                output = (Math.round(length / 1000 * 100) / 100) +
                    ' ' + 'km';
            }
            else {
                output = (Math.round(length * 100) / 100) +
                    ' ' + 'm';
            }
            return output;
        };
        NpMap.prototype.formatArea = function (polygon, hectaresOnly) {
            var coordinateSystem = fallback(this.coordinateSystem, 'EPSG:4326');
            var transGeom = polygon.clone().transform(ol.proj.get('EPSG:3857'), ol.proj.get(coordinateSystem));
            var area = transGeom.getArea();
            var output = "";
            if (hectaresOnly === true) {
                output = (Math.round(100 * area / 10000) / 100) +
                    ' ' + 'HA';
            }
            else {
                if (area >= 1000) {
                    output = (Math.round(100 * area / 10000) / 100) +
                        ' ' + 'HA';
                }
                else {
                    output = (Math.round(area * 100) / 100) +
                        ' ' + 'm<sup>2</sup>';
                }
            }
            return output;
        };
        NpMap.prototype.getDrawingOverlay = function () {
            return this.currentMode == NpMapMode.NPMAP_MODE_DRAW ?
                this.featureOverlayDraw :
                this.featureOverlayTemp;
        };
        /*
        * this is the hole drawer method
        * @TODO need to check the geometry type before enabling it
        * @p.tsagkis
        */
        NpMap.prototype.holeDrawer = function () {
            var npMapThis = this;
            var drawnFeature = this.getFeature();
            //@p.tsagkis start
            var holeStyle = [
                new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: 'rgba(0, 0, 0, 0.8)',
                        lineDash: [10, 10],
                        width: 3
                    }),
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 255, 255, 0)'
                    })
                }),
                new ol.style.Style({
                    image: new ol.style.RegularShape({
                        fill: new ol.style.Fill({
                            color: 'rgba(255, 0, 0, 0.5)'
                        }),
                        stroke: new ol.style.Stroke({
                            color: 'black',
                            width: 1
                        }),
                        points: 4,
                        radius: 6,
                        angle: Math.PI / 4
                    })
                })];
            var selFeatGeom = drawnFeature.getGeometry().clone();
            var vertsCouter = 0;
            npMapThis.holeDraw = new ol.interaction.Draw({
                source: new ol.source.Vector(),
                type: 'Polygon',
                style: holeStyle,
                condition: function (e) {
                    if (e.originalEvent.button === 0)
                        return true; //allow only the left mouse click button
                    else
                        return false;
                },
                //add the geometry function in order to disable
                //hole creation outside selected polygon
                geometryFunction: function (coords, geom) {
                    var retGeom; //define the geometry to return   
                    if (typeof (geom) !== 'undefined') {
                        geom.setCoordinates(coords);
                    }
                    else {
                        retGeom = new ol.geom.Polygon(coords);
                    }
                    if (coords[0].length > vertsCouter) {
                        //check if vertex drawn is within the selected polygon
                        var isIn = isPointInPoly(selFeatGeom, coords[0][coords[0].length - 1]);
                        //if outside get rid of it
                        if (isIn === false) {
                            coords[0].pop(); //remove the last coordinate element
                            retGeom = new ol.geom.Polygon(coords); //recunstract the geometry
                        }
                        vertsCouter = coords[0].length; //reset the length of vertex counter
                    }
                    return retGeom;
                }
            });
            //disable the draw and modify interaction during hole drawing
            npMapThis.drawInteraction.setActive(false);
            npMapThis.modifyInteraction.setActive(false);
            //add the draw hole interaction
            npMapThis.map.addInteraction(npMapThis.holeDraw);
            //add reinit the snap interaction       
            npMapThis.removeSnapInteraction();
            npMapThis.addSnapInteraction();
            npMapThis.snapInteraction.setActive(npMapThis.snapEnabled);
            npMapThis.holeDraw.on('drawstart', function (e) {
                npMapThis.lastEditActionIsHoleAdd = true;
                NpGeoGlobals.inModifyMode = true;
                var feature = e.feature; //grab the draw hole feature
                var ringAdded = false; //init boolen var to clarify whether drawn hole has allready been added or not
                //set the change feature listener so get the hole like visual effect
                feature.on('change', function (e) {
                    //get draw hole feature geometry
                    var drawCoords = feature.getGeometry().getCoordinates(false)[0];
                    //if hole has more than two cordinate pairs, add the interior ring to feature
                    if (drawCoords.length > 2) {
                        //if intirior ring has not been added yet, append it and set it as true
                        if (ringAdded === false) {
                            var testCoords = feature.getGeometry().getCoordinates(false)[0];
                            var testCoordsLength = testCoords.length;
                            //solves the bug when 2nd point outside polygon
                            if (testCoords.length === 3 &&
                                testCoords[testCoordsLength - 1][0] === testCoords[testCoordsLength - 2][0] &&
                                testCoords[testCoordsLength - 1][1] === testCoords[testCoordsLength - 2][1]) {
                                //console.log("do nothing!!!!!")
                                return;
                            }
                            else {
                                drawnFeature.getGeometry().appendLinearRing(new ol.geom.LinearRing(feature.getGeometry().getCoordinates()[0]));
                                ringAdded = true;
                            }
                        }
                        else {
                            //get the elements length of the geometry
                            var coordElemntLength = drawnFeature.getGeometry().getCoordinates().length;
                            var coordsExt = drawnFeature.getGeometry().getCoordinates()[0]; //exterior ring
                            var coordsInter = []; //interior rings 
                            //if we have more than 2 elements 
                            if (coordElemntLength > 2) {
                                //create the interior rings array
                                //note that we use i=1 to leave outside the first element (exterior ring) 
                                //and coordElemntLength-1 to leave outside the last element as this is the one we need to update
                                for (var i = 1; i < coordElemntLength - 1; i++) {
                                    coordsInter.push(drawnFeature.getGeometry().getCoordinates()[i]);
                                }
                            }
                            //construct the coordinates to use for the modify geometry feature
                            var setCoords = [];
                            //if interior rings allready exist
                            if (coordsInter.length > 0) {
                                //push the exterior ring
                                setCoords.push(coordsExt);
                                //push all the interior rings
                                for (var z = 0; z < coordsInter.length; z++) {
                                    setCoords.push(coordsInter[z]);
                                }
                                //and finally push the new drawn hole
                                setCoords.push(feature.getGeometry().getCoordinates(false)[0]);
                            }
                            else {
                                setCoords = [coordsExt, feature.getGeometry().getCoordinates(false)[0]];
                            }
                            drawnFeature.getGeometry().setCoordinates(setCoords);
                        }
                    }
                });
            });
            //create a listener when finish drawing and so remove the hole interaction 
            npMapThis.holeDraw.on('drawend', function (evt) {
                //disable the  holeDraw interaction
                npMapThis.map.removeInteraction(npMapThis.holeDraw);
                //npMapThis.drawInteraction.setActive(true);
                //npMapThis.modifyInteraction.setActive(true);
                npMapThis.lastEditActionIsHoleAdd = true;
                NpGeoGlobals.inModifyMode = false;
                //console.log("entering draw mode");
                npMapThis.enterDrawMode();
            });
        };
        // DEPRECATED - NOT USED
        // @p.tsagkis this is not used any more
        NpMap.prototype.drilHole = function (evt) {
            var npMapThis = this;
            var drawnFeature = this.getFeature();
            return;
            //@p.tsagkis end
            /*var holeWidth = this.map.getViewport().clientWidth / 20.;
            var holePixelUL = [evt.pixel[0] - holeWidth, evt.pixel[1] - holeWidth];
            var holePixelDR = [evt.pixel[0] + holeWidth, evt.pixel[1] + holeWidth];
            var holeCoordUL = npMapThis.map.getCoordinateFromPixel(holePixelUL);
            var holeCoordDR = npMapThis.map.getCoordinateFromPixel(holePixelDR);
            var xl = holeCoordUL[0];
            var yl = holeCoordUL[1];
            var xr = holeCoordDR[0];
            var yr = holeCoordDR[1];
            var hole = [[xl, yl, xl, yr, xr, yr, xr, yl]];
            var holyPoly = subtractPolygonsFromPolygon(
                NpGeoGlobals.OL3feature_to_coordinates(drawnFeature), hole);
            this.featureOverlayDraw.getSource().clear();
            if (holyPoly.length > 0) {
                var holyFeature = NpGeoGlobals.coordinates_to_OL3feature(holyPoly);
                this.featureOverlayDraw.getSource().addFeature(holyFeature);
            }*/
        };
        // This must be public, to allow the hack that calls it from inside
        //  ol.interaction.Modify.prototype.handlePointerUp
        // i.e.
        //  this.map_.npMap_.snapModifiedVertexes();
        // DEPRECATED
        //@TODO. p.tsagkis. This is not needed anymore. Has been replaced from the official ol3 snapping interaction 
        NpMap.prototype.snapModifiedVertexes = function () {
            var _this = this;
            var npMapThis = this;
            if (!this.snapEnabled)
                return;
            if (isVoid(this.snappedVertexFeature))
                return;
            var f = this.getFeature();
            if (isVoid(f))
                return;
            var g = f.getGeometry();
            if (isVoid(g))
                return;
            if (!(g instanceof ol.geom.Polygon))
                return;
            var snappedToX = this.snappedVertexFeature.getGeometry().flatCoordinates[0];
            var snappedToY = this.snappedVertexFeature.getGeometry().flatCoordinates[1];
            var snappedPixel = this.map.getPixelFromCoordinate([snappedToX, snappedToY]);
            var x, y;
            var found = false;
            //console.log("do some snapping here!!!!!. snappedToX", snappedToX)
            // Find the closest vertex to our snapped coordinates - there will be exactly one!
            g.flatCoordinates.forEach(function (n, idx) {
                if (found)
                    return;
                // (the flatCoordinates are an array of width N (N is even,
                // since the array is of the form [x1,y1,x2,y2,...])
                // so we need to do this check in two phases: one storing the x...
                if (isVoid(x)) {
                    x = n;
                }
                else {
                    // and one completing a coordinate pair (x,y)
                    var c = [x, n];
                    var vertexPixel = _this.map.getPixelFromCoordinate(c);
                    if (Math.sqrt(ol.coordinate.squaredDistance(vertexPixel, snappedPixel)) < NpMap.pixelTolerance) {
                        g.flatCoordinates[idx - 1] = snappedToX;
                        g.flatCoordinates[idx] = snappedToY;
                        found = true;
                    }
                    x = undefined;
                }
            });
            if (found) {
                ++g.revision_;
                g.dispatchEvent(goog.events.EventType.CHANGE);
            }
        };
        //DEPRECATED
        //@TODO. p.tsagkis. This is not needed anymore. Has been replaced from the official ol3 snapping interaction 
        NpMap.prototype.handleSnapping = function (evt) {
            //console.log("handling snapping this.snapEnabled", this.snapEnabled);
            var npMapThis = this;
            if (!this.snapEnabled) {
                return;
            }
            // In draw mode, if we must snap, check for a snapped point
            if (!isVoid(npMapThis.snappedVertexFeature)) {
                var snappedToX = npMapThis.snappedVertexFeature.getGeometry().flatCoordinates[0];
                var snappedToY = npMapThis.snappedVertexFeature.getGeometry().flatCoordinates[1];
                // We are a bit too late.
                // This handler is called AFTER the default OL3 handler, which has already updated
                // the drawn feature (sketchFeature_ variable of the drawInteraction)
                // Now that we have the coordinates we last snapped on, we need to update
                // the sketchFeature_'s coordinates.
                var coordinates;
                try {
                    coordinates = npMapThis.drawInteraction.sketchFeature_.values_.geometry.flatCoordinates;
                    console.log("coordinates to snap", coordinates);
                    console.log("npMapThis.drawInteraction", npMapThis.drawInteraction);
                }
                catch (err) {
                    console.error("we have an error", err);
                }
                if (!isVoid(coordinates) && !isVoid(coordinates.length)) {
                    // The default OL3 handler has already added the coordinates of the pixel we clicked on
                    var lastAddedCoordinateToReplace = npMapThis.map.getCoordinateFromPixel(evt.pixel);
                    var x = lastAddedCoordinateToReplace[0];
                    var y = lastAddedCoordinateToReplace[1];
                    // We need to replace them anywhere we find them in the coordinate list, with the snapped ones
                    for (var idx = 0; idx < coordinates.length; idx += 2) {
                        var xx = coordinates[idx];
                        var yy = coordinates[idx + 1];
                        if (xx === x && yy === y) {
                            coordinates[idx] = snappedToX;
                            coordinates[idx + 1] = snappedToY;
                        }
                    }
                    // And we need to do this not only on the sketchFeature_'s coordinates, but also on the
                    // sketchPolygonCoords_ - which is somehow used by OL3 elsewhere.
                    var coordinateArray;
                    try {
                        coordinateArray = npMapThis.drawInteraction.sketchPolygonCoords_[0];
                    }
                    catch (err) { }
                    if (!isVoid(coordinateArray)) {
                        for (var idx = 0; idx < coordinateArray.length; idx++) {
                            var c = coordinateArray[idx];
                            var xx = c[0];
                            var yy = c[1];
                            if (xx === x && yy === y) {
                                c[0] = snappedToX;
                                c[1] = snappedToY;
                            }
                        }
                    }
                    // Finally, now that we updated the sketch feature, OL3 needs to do some house keeping
                    npMapThis.drawInteraction.updateSketchFeatures_();
                    // Don't re-snap on these coordinates in the future
                    npMapThis.avoidResnapOnSamePoint = [snappedToX, snappedToY];
                }
            }
        };
        NpMap.prototype.getGenericPosLengthAreaDescription = function (feature, lightweight) {
            if (lightweight === void 0) { lightweight = false; }
            var html = "";
            var geometry = feature.getGeometry();
            if (geometry instanceof ol.geom.LineString) {
                var line = geometry;
                html += "<b>Length</b>:&nbsp;" + this.formatLength(line);
            }
            else if (geometry instanceof ol.geom.Polygon) {
                var poly = geometry;
                html += "<b>Area</b>:&nbsp;" + this.formatArea(poly, false);
            }
            else if (geometry instanceof ol.geom.MultiPolygon) {
                var polys = geometry.getPolygons();
                var sumArea = 0;
                for (var p = 0; p < polys.length; p++) {
                    var transGeom = polys[p].clone().transform(ol.proj.get('EPSG:3857'), ol.proj.get(this.coordinateSystem));
                    var area = transGeom.getArea();
                    sumArea = sumArea + transGeom.getArea();
                    var printArea = '';
                    if (sumArea >= 1000) {
                        printArea = (Math.round(100 * sumArea / 10000) / 100) +
                            ' ' + 'HA';
                    }
                    else {
                        printArea = (Math.round(area * sumArea) / 100) +
                            ' ' + 'm<sup>2</sup>';
                    }
                }
                html += "<b>Area</b>:&nbsp;" + printArea;
            }
            else if (geometry instanceof ol.geom.Point) {
                var point = geometry;
                var coords = point.getCoordinates();
                var msg = Sprintf.sprintf('<b>X</b>: %10.2f<br><b>Y</b>: %10.2f', coords[0], coords[1]);
                html += "<b>Point</b>:&nbsp;</br>" + msg;
            }
            if (lightweight)
                return html;
            var geoserverInfoFields = [
                "ACCEPT_FLAG",
                "ACQUISITION",
                "AIT_CH",
                "AIT_ID",
                "AIT_TE",
                "AREA_B",
                "AREA_C",
                "AREA_E",
                "AREA_M",
                "AREA_P",
                "AREA_R",
                "AREA_SUB",
                "AREA_TEST",
                "CH_DTE",
                "CKOIN_ID",
                "COMPATIBLE",
                "COORD_E",
                "COORD_N",
                "COVER_DESCR",
                "COVER_ID",
                "DAA_KOD",
                "DATE_",
                "DOWNLOAD_FLAG",
                "EFOID_NEW",
                "EFOPER_ID",
                "EFO_ID",
                "EXIST_PARCELS_FLAG",
                "FC1",
                "FC2",
                "FC3",
                "FN1",
                "FN2",
                "FN3",
                "ILOT_10",
                "I_REL2_Z",
                "LABEL",
                "MAP_ID",
                "NOMOS_CODE",
                "NOTES1",
                "NOT_ELIGIBLE_FLAG",
                "NO_ZONE",
                "OLIVE_TREE_DECLARATION",
                "OPD_ID",
                "PERM_PASTURE",
                "REQUEST_EFOID",
                "REQUEST_FLAG",
                "SAMPLE_ACR",
                "SDOAREA_HE",
                "SDOAREA_M2",
                "SDOLENGTH",
                "TSI",
                "ZONES"
            ];
            var extraHtml = ["<table>"];
            geoserverInfoFields.forEach(function (label) {
                var anarchy = feature;
                if (isVoid(anarchy.values_))
                    return;
                if (label in anarchy.values_) {
                    var data = anarchy.values_[label];
                    if (!isVoid(data)) {
                        extraHtml.push("<tr><td align='right'><b>" + label + "</b>:</td><td>" + data + "</td></tr>");
                    }
                }
            });
            if (extraHtml.length !== 1) {
                // When the geoserver provides data, show them.
                extraHtml.push("</table>");
                return extraHtml.join("\n");
            }
            else {
                return html;
            }
        };
        NpMap.prototype.handleSelectingFeature = function (evt) {
            var npMapThis = this;
            // Find the feature we clicked on
            var features = [];
            var minArea = Number.MAX_VALUE;
            features = []; //reset it here just in case
            npMapThis.map.forEachFeatureAtPixel(evt.pixel, function (f, layer) {
                //console.log("layer", layer);
                // console.log("Feature", f);
                var geometry = f.getGeometry();
                if (geometry instanceof ol.geom.Polygon) {
                    var poly = geometry;
                    features.push(new Tuple2(f, layer));
                }
                else if (geometry instanceof ol.geom.MultiPolygon) {
                    var poly = geometry;
                    features.push(new Tuple2(f, layer));
                }
                else if (geometry instanceof ol.geom.Point) {
                    var poly = geometry;
                    features.push(new Tuple2(f, layer));
                }
                else if (geometry instanceof ol.geom.LineString) {
                    var line = geometry;
                    if (layer !== null) {
                        if (layer.get('layerId') !== 'splitLayer' && layer.get('layerId') != 'splitPolyLayer') {
                            features.push(new Tuple2(f, layer));
                        }
                    }
                    else {
                        features.push(new Tuple2(f, layer));
                    }
                }
                return false;
            }, undefined, // unused 3rd arg in above callback
            function (l) {
                // Only allow the selectable layers
                var a = l;
                return (isVoid(a.getSqlVectorLayer) || a.getSqlVectorLayer().selectable);
            });
            // In select mode, either clear selection (if no feature found)
            // or add the one we just clicked on.
            var scope = npMapThis.grp.$scope;
            var selectedFeatures = npMapThis.selectInteraction.getFeatures();
            selectedFeatures.clear();
            if (features.length === 0) {
                // do nothing
                //console.log("no features at pixel");
                return;
            }
            if (features.length > 1) {
                //console.log("mulitple features at pixel",features);
                var dialogOptions = new NpTypes.DialogOptions();
                dialogOptions.title = "Select Feature";
                dialogOptions.width = "25em";
                dialogOptions.className = "ChooseFeatureDialog";
                dialogOptions.onCloseDialog = function () {
                    npMapThis.featureOverlayCrop.getSource().clear();
                };
                dialogOptions.npMapThis = npMapThis;
                dialogOptions.featuresRemaining = features;
                var scope = npMapThis.grp.$scope;
                npMapThis.Plato.showDialog(scope, "Select Feature", "/" + scope.globals.appName + '/partials/chooseFeature.html?rev=' + scope.globals.version, dialogOptions);
            }
            else {
                if (npMapThis.selectInteraction !== undefined) {
                    var feature = features[0].a;
                    var layer = features[0].b;
                    if (feature !== undefined &&
                        (selectedFeatures.getLength() == 1 &&
                            selectedFeatures.item(0) == feature)) {
                        // Toggle selection if we clicked on already selected
                        npMapThis.selectInteraction.getFeatures().remove(feature);
                    }
                    else {
                        if (selectedFeatures.getLength() !== 0) {
                            selectedFeatures.clear();
                        }
                        if (feature !== undefined) {
                            selectedFeatures.push(feature);
                            if (!isVoid(layer) && !isVoid(layer.getSqlVectorLayer)) {
                                layer.getSqlVectorLayer().onSelectFeature(feature, layer);
                            }
                            else {
                                var html = npMapThis.getFeatureLabel(feature, false);
                                //console.log("feature", feature);
                                //console.log("layer", layer)
                                //console.log("html", html);
                                if (!isVoid(html)) {
                                    messageBox(scope, npMapThis.Plato, "Info", html, IconKind.INFO, [
                                        new Tuple2("OK", function () { })
                                    ], 0, 0, 'auto');
                                }
                            }
                        }
                    }
                }
            }
        };
        /**
         * @p.tsagkis
         * supply the layername get back the ol.layer object
         * @param lyrname
         */
        NpMap.prototype.getOLLayerByName = function (lyrid) {
            var LyrCollection = this.map.getLayers();
            var lyrToRet = null;
            LyrCollection.forEach(function (lyr) {
                if (lyr.get("layerId") === lyrid) {
                    lyrToRet = lyr;
                }
            });
            return lyrToRet;
        };
        /**
         * @p.tsagkis
         * supply the layerid
         * get back the layer label
         * @param lyrid
         */
        NpMap.prototype.getLayerLabelById = function (lyrid) {
            var indexLayers = this.layers.filter(function (l) { return l instanceof NpGeoLayers.SqlVectorLayer; });
            var lyrLabelToRet;
            var found = false;
            for (var i = 0; i < indexLayers.length; i++) {
                if (indexLayers[i].layerId === lyrid) {
                    lyrLabelToRet = indexLayers[i].label;
                    found = true;
                }
            }
            if (found === false) {
                if (lyrid === 'importsLayer') {
                    lyrLabelToRet = "Temporary Features";
                }
                else {
                    lyrLabelToRet = "Digitized Features";
                }
            }
            return lyrLabelToRet;
        };
        /**
         * @p.tsagkis
         * supply the layername get back the np layer object
         * @param lyrname
         */
        NpMap.prototype.getNPLayerById = function (lyrid) {
            var indexLayers = this.layers.filter(function (l) { return l instanceof NpGeoLayers.SqlVectorLayer; });
            var lyrToRet;
            for (var i = 0; i < indexLayers.length; i++) {
                //console.log("indexLayers", indexLayers[i]);
                //console.log("lyrid", lyrid);
                if (indexLayers[i].layerId === lyrid) {
                    lyrToRet = indexLayers[i];
                }
            }
            return lyrToRet;
        };
        /**
         * helper function for printing module
         * suplly a features array
         * get back a json obj holding features grouped on geometry types
         *
         */
        NpMap.prototype.groupFeatsOnGeomTypes = function (feats) {
            var pointGroup = [];
            var lineGroup = [];
            var polGroup = [];
            for (var i = 0; i < feats.length; i++) {
                var gtype = feats[i].getGeometry().getType();
                //console.log("gtype", gtype);
                if (gtype === 'Point' || gtype === 'MultiPoint') {
                    pointGroup.push(feats[i]);
                }
                if (gtype === 'LineString' || gtype === 'MultiLineString') {
                    lineGroup.push(feats[i]);
                }
                if (gtype === 'Polygon') {
                    polGroup.push(feats[i]);
                    console.log("feats[i]", feats[i]);
                }
                if (gtype === 'MultiPolygon') {
                    //console.log("handling multipolygon", feats[i]);
                    var pols = feats[i].getGeometry().getPolygons();
                    var multiFeats = [];
                    for (var z = 0; z < pols.length; z++) {
                        //var newFeat = jQuery.extend(true, {}, feats[i]);//deep clone the feature
                        //console.log("set geom==", pols[z]);
                        var newFeat = new ol.Feature();
                        newFeat.setGeometryName(feats[i].getGeometryName());
                        newFeat.set('layerId', feats[i].get('layerId'));
                        newFeat.setGeometry(pols[z]);
                        //console.log("newFeat",newFeat);    
                        polGroup.push(newFeat);
                    }
                }
            }
            return { pointGroup: pointGroup, lineGroup: lineGroup, polGroup: polGroup };
        };
        /**
         * helper function for printing module
         * desides whether to execute a quick export
         * or (shift key pressed) give you a dialog of options
         * and then execute the export request
         *
         */
        NpMap.prototype.buildMapExpReq = function (shiftPressed) {
            var format = "";
            var fileExt = "";
            if (!shiftPressed) {
                format = "image/png"; //this is the default format
                fileExt = "png"; //and so on the deafault file extension
                this.executeInlineWms(format, fileExt);
            }
            else {
                var scope = this.grp.$scope;
                var dialogOptions = new NpTypes.DialogOptions();
                dialogOptions.title = "Export Map to File";
                dialogOptions.width = "20em";
                dialogOptions.className = "MapExportDialog";
                dialogOptions.npMapThis = this;
                this.Plato.showDialog(scope, dialogOptions.title, "/" + scope.globals.appName + '/partials/exportMapOptions.html?rev=' + scope.globals.version, dialogOptions);
            }
        };
        /**
         * Printing module. NOT USED FOR THE TIME BEING
         * generic function to create an inline sld
         * and sent it on geoserver as xml wms request
         * @returns an image for downloading
         * Something is wrong here in terms of Typescript
         * There is some kind of error when neurocoding, different typescript versions cause it.
         * So comment it out but keep it here for Future reference
         */
        NpMap.prototype.executeInlineWms = function (format, fileExt) {
            //var npMapThis = this;
            //var lyrsOnMap = this.map.getLayers().getArray();
            //var clonedLyrs = $.extend(true, [], lyrsOnMap);//deep clone them
            ////add a few more layrs that might have features on map
            //clonedLyrs.push(this.featureOverlayDraw);
            //clonedLyrs.push(this.featureOverlayCrop);
            //var objs = [];//create a json object to collect necessary values
            //for (var k = 0; k < clonedLyrs.length; k++) {
            //    var lyr = clonedLyrs[k];
            //    console.log("lyr", lyr);
            //    console.log("layername==", npMapThis.getLayerLabelById(<any>lyr.get("layerId")));
            //    console.log("lyr.getVisible", lyr.getVisible());
            //    if (lyr instanceof ol.layer.Vector && lyr.getVisible() === true && lyr.getSource().getFeatures().length > 0) {
            //        var lyrName = npMapThis.getLayerLabelById(lyr.get("layerId"));
            //        var npLyr = npMapThis.getNPLayerById(lyr.get("layerId"));
            //        var lyrOrderIndex = 0;
            //        if (typeof npLyr === 'undefined') { //these are the temporary objects on map, so place them on top
            //            lyrOrderIndex = 1000;           //set a high value if undefined.
            //        } else {
            //            lyrOrderIndex = npLyr.orderIndex;
            //        }
            //        var style = lyr.getStyle();
            //        var styleObj = [];//one more object to collect style values
            //        if (typeof style === 'function') {//case of style function 
            //            if (typeof npLyr !== 'undefined') {
            //                styleObj.push({
            //                    fillColor: NpGeoGlobals.rgba2hexAndOpacity((npLyr.fillColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[npLyr.fillColor] : npLyr.fillColor)[0].hex,
            //                    fillColorOpacity: NpGeoGlobals.rgba2hexAndOpacity((npLyr.fillColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[npLyr.fillColor] : npLyr.fillColor)[0].opacity,
            //                    borderColor: NpGeoGlobals.rgba2hexAndOpacity((npLyr.borderColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[npLyr.borderColor] : npLyr.borderColor)[0].hex,
            //                    borderColorOpacity: NpGeoGlobals.rgba2hexAndOpacity((npLyr.borderColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[npLyr.borderColor] : npLyr.borderColor)[0].opacity,
            //                    borderWidth: npLyr.penWidth = typeof (npLyr.penWidth) !== 'undefined' ? npLyr.penWidth : 1
            //                });
            //            }
            //            if (typeof npLyr === 'undefined' && lyr.get("layerId") === 'importsLayer') {//this is the case of gpx,kml etc
            //                styleObj.push({
            //                    fillColor: NpGeoGlobals.rgba2hexAndOpacity('rgba(255,0,255,0.5)')[0].hex,
            //                    fillColorOpacity: NpGeoGlobals.rgba2hexAndOpacity('rgba(255,0,255,0.5)')[0].opacity,
            //                    borderColor: '#f00',
            //                    borderColorOpacity: "1",
            //                    borderWidth: 3
            //                });
            //            }
            //        }
            //        if (Array === style.constructor) {//case of array of styles, just use the very first array element
            //            var bWidth = style[0].getStroke().getWidth();
            //            styleObj.push({
            //                fillColor: NpGeoGlobals.rgba2hexAndOpacity((style[0].getFill().getColor() in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[style[0].getFill().getColor()] : style[0].getFill().getColor())[0].hex,
            //                fillColorOpacity: NpGeoGlobals.rgba2hexAndOpacity((style[0].getFill().getColor() in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[style[0].getFill().getColor()] : style[0].getFill().getColor())[0].opacity,
            //                borderColor: NpGeoGlobals.rgba2hexAndOpacity((style[0].getStroke().getColor() in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[style[0].getStroke().getColor()] : style[0].getStroke().getColor())[0].hex,
            //                borderColorOpacity: NpGeoGlobals.rgba2hexAndOpacity((style[0].getStroke().getColor() in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[style[0].getStroke().getColor()] : style[0].getStroke().getColor())[0].opacity,
            //                borderWidth: bWidth = typeof (bWidth) !== 'undefined' ? bWidth : 1
            //            });
            //        }
            //        //collect only those within the mbr. Just to avoid overhead with uneeded features
            //        var featsToAdd = [];
            //        lyr.getSource().forEachFeatureInExtent(npMapThis.map.getView().calculateExtent(npMapThis.map.getSize()), function (f) {
            //            console.log("feats in extent lyranme:" + lyrName + " ====", f);
            //            featsToAdd.push(f);
            //        });
            //        if (lyrName === "Ψηφ.Γεωμετρία" && featsToAdd.length > 0) {
            //            objs.push({
            //                'lyrname': 'TempGroup',
            //                'feats': featsToAdd,
            //                'style': styleObj,
            //                'opacity': lyr.getOpacity(),
            //                'orderIndex': lyrOrderIndex
            //            });
            //        }
            //        //this 
            //        if (lyrName !== "Ψηφ.Γεωμετρία" && featsToAdd.length > 0) {
            //            //now need to verify the geometry types and group them to similar types
            //            var groupedFeats = this.groupFeatsOnGeomTypes(featsToAdd);
            //            if (groupedFeats.pointGroup.length > 0) {
            //                objs.push({
            //                    'lyrname': npMapThis.getLayerLabelById(<any>lyr.get("layerId")),
            //                    'feats': groupedFeats.pointGroup,
            //                    'style': styleObj,
            //                    'opacity': lyr.getOpacity(),
            //                    'orderIndex': lyrOrderIndex
            //                });
            //            }
            //            if (groupedFeats.lineGroup.length > 0) {
            //                objs.push({
            //                    'lyrname': npMapThis.getLayerLabelById(<any>lyr.get("layerId")),
            //                    'feats': groupedFeats.lineGroup,
            //                    'style': styleObj,
            //                    'opacity': lyr.getOpacity(),
            //                    'orderIndex': lyrOrderIndex
            //                });
            //            }
            //            if (groupedFeats.polGroup.length > 0) {
            //                objs.push({
            //                    'lyrname': npMapThis.getLayerLabelById(<any>lyr.get("layerId")),
            //                    'feats': groupedFeats.polGroup,
            //                    'style': styleObj,
            //                    'opacity': lyr.getOpacity(),
            //                    'orderIndex': lyrOrderIndex
            //                });
            //            }
            //        }
            //    }
            //}
            ////now its time to build the gml object for each layer and feature
            //var gmlParser = new ol.format.GML3();
            //var xmlBodyNodes = [];
            //for (var g = 0; g < objs.length; g++) {
            //    console.log("objs found", objs[g]);
            //    var featureMembers = [];//place the gml representation of features here
            //    var styleStr = "";
            //    if (objs[g].lyrname === 'Προσωρινές Γεωμετρίες') {//this is the case of multiple geometry types (importsLayer)
            //        styleStr =
            //            '<UserStyle>' +
            //            '<FeatureTypeStyle>' +
            //            '<Rule>' +
            //            '<Filter>' +
            //            '<And>' +
            //            '<PropertyIsEqualTo>' +
            //            '<PropertyName>lyrname</PropertyName>' +
            //            '<Literal>' + objs[g].lyrname + '</Literal>' +
            //            '</PropertyIsEqualTo>' +
            //            '<PropertyIsEqualTo>' +
            //            '<PropertyName>geomType</PropertyName>' +
            //            '<Literal>Point</Literal>' +
            //            '</PropertyIsEqualTo>' +
            //            '</And>' +
            //            '</Filter>' +
            //            '<PointSymbolizer>' +
            //            '<Graphic>' +
            //            '<Mark>' +
            //            '<WellKnownName>circle</WellKnownName>' +
            //            '<Fill>' +
            //            '<CssParameter name="fill">' +
            //            '<ogc:Literal>#FF00FF</ogc:Literal>' +
            //            '</CssParameter>' +
            //            '<CssParameter name="fill-opacity">' +
            //            '<ogc:Literal>0.5</ogc:Literal>' +
            //            '</CssParameter>' +
            //            '</Fill>' +
            //            '<Stroke>' +
            //            '<CssParameter name= "Stroke" >#FF0000</CssParameter>' +
            //            '<CssParameter name="stroke-width">3</CssParameter>' +
            //            '</Stroke>' +
            //            '</Mark>' +
            //            '<Size>6</Size>' +
            //            '</Graphic>' +
            //            '</PointSymbolizer>' +
            //            //labeling here
            //            '<TextSymbolizer>' +
            //            '<Label>' +
            //            '<ogc:PropertyName>label</ogc:PropertyName>' +
            //            '</Label>' +
            //            '<Halo>' +
            //            '<Radius>1</Radius>' +
            //            '<Fill>' +
            //            '<CssParameter name="fill"><ogc:Literal>#ffffff</ogc:Literal></CssParameter>' +
            //            '</Fill>' +
            //            '</Halo>' +
            //            '<Fill> ' +
            //            '<CssParameter name= "fill"><ogc:Literal>#000000</ogc:Literal></CssParameter>' +
            //            '</Fill>' +
            //            '</TextSymbolizer>' +
            //            //end of labeling here
            //            '</Rule>' +
            //            '<Rule>' +
            //            '<Filter>' +
            //            '<And>' +
            //            '<PropertyIsEqualTo>' +
            //            '<PropertyName>lyrname</PropertyName>' +
            //            '<Literal>' + objs[g].lyrname + '</Literal>' +
            //            '</PropertyIsEqualTo>' +
            //            '<Or>' +
            //            '<PropertyIsEqualTo>' +
            //            '<PropertyName>geomType</PropertyName>' +
            //            '<Literal>LineString</Literal>' +
            //            '</PropertyIsEqualTo>' +
            //            '<PropertyIsEqualTo>' +
            //            '<PropertyName>geomType</PropertyName>' +
            //            '<Literal>MultiLineString</Literal>' +
            //            '</PropertyIsEqualTo>' +
            //            '</Or>' +
            //            '</And>' +
            //            '</Filter>' +
            //            '<LineSymbolizer>' +
            //            '<Stroke>' +
            //            '<CssParameter name="stroke" >#FF0000</CssParameter>' +
            //            '<CssParameter name= "stroke-width"> 3 </CssParameter>' +
            //            '</Stroke>' +
            //            '</LineSymbolizer>' + 
            //            //labeling here
            //            '<TextSymbolizer>' +
            //            '<Label>' +
            //            '<ogc:PropertyName>label</ogc:PropertyName>' +
            //            '</Label>' +
            //            '<Halo>' +
            //            '<Radius>1</Radius>' +
            //            '<Fill>' +
            //            '<CssParameter name="fill"><ogc:Literal>#ffffff</ogc:Literal></CssParameter>' +
            //            '</Fill>' +
            //            '</Halo>' +
            //            '<Fill> ' +
            //            '<CssParameter name= "fill"><ogc:Literal>#000000</ogc:Literal></CssParameter>' +
            //            '</Fill>' +
            //            '</TextSymbolizer>' +
            //            '</Rule>' +
            //            //end of labeling here
            //            '</FeatureTypeStyle>' +
            //            '</UserStyle>';
            //    }
            //    else {//in any other case we should expect polygons or multipolygons
            //        styleStr =
            //            '<UserStyle>' +
            //            '<FeatureTypeStyle>' +
            //            '<Rule><Filter><PropertyIsEqualTo>' +
            //            '<PropertyName>lyrname</PropertyName>' +
            //            '<Literal>' + objs[g].lyrname + '</Literal>' +
            //            '</PropertyIsEqualTo></Filter>' +
            //            '<PolygonSymbolizer>' +
            //            '<Fill>' +
            //            '<CssParameter name="fill">' +
            //            '<ogc:Literal>' + objs[g].style[0].fillColor + '</ogc:Literal>' +
            //            '</CssParameter>' +
            //            '<CssParameter name="fill-opacity">' +
            //            '<ogc:Literal>' + objs[g].style[0].fillColorOpacity * objs[g].opacity + '</ogc:Literal>' +
            //            '</CssParameter>' +
            //            '</Fill>' +
            //            '<Stroke>' +
            //            '<CssParameter name="stroke">' +
            //            '<ogc:Literal>' + objs[g].style[0].borderColor + '</ogc:Literal>' +
            //            '</CssParameter>' +
            //            '<CssParameter name="stroke-width">' +
            //            '<ogc:Literal>' + objs[g].style[0].borderWidth + '</ogc:Literal>' +
            //            '</CssParameter>' +
            //            '</Stroke>' +
            //            '</PolygonSymbolizer>' + 
            //            //labeling here
            //            '<TextSymbolizer>' +
            //            '<Label>' +
            //            '<ogc:PropertyName>label</ogc:PropertyName>' +
            //            '</Label>' +
            //            '<Halo>' +
            //            '<Radius>1</Radius>' +
            //            '<Fill>' +
            //            '<CssParameter name="fill"><ogc:Literal>#ffffff</ogc:Literal></CssParameter>' +
            //            '</Fill>' +
            //            '</Halo>' +
            //            '<Fill> ' +
            //            '<CssParameter name= "fill"><ogc:Literal>#000000</ogc:Literal></CssParameter>' +
            //            '</Fill>' +
            //            '</TextSymbolizer>' +
            //            //end of labeling here
            //            '</Rule>' +
            //            '</FeatureTypeStyle>' +
            //            '</UserStyle>';
            //        // '</FeatureTypeStyle>';
            //    }
            //    for (var f = 0; f < objs[g].feats.length; f++) {
            //        var gmlGeomXml = gmlParser.writeGeometryNode(objs[g].feats[f].getGeometry(), {
            //            dataProjection: npMapThis.coordinateSystem,
            //            featureProjection: 'EPSG:3857',
            //            rightHanded: false
            //        });
            //        var currentGeomType = "";
            //        if ($(gmlGeomXml).find('Point').length > 0) {
            //            currentGeomType = "Point";
            //        }
            //        if ($(gmlGeomXml).find('Polygon').length > 0) {
            //            currentGeomType = "Polygon";
            //        }
            //        //LineString
            //        if ($(gmlGeomXml).find('LineString').length > 0) {
            //            currentGeomType = "LineString";
            //        }
            //        if ($(gmlGeomXml).find('MultiCurve').length > 0) {
            //            currentGeomType = "MultiLineString";
            //        }
            //        //console.log("$(gmlGeomXml", $(gmlGeomXml));
            //        //console.log("currentGeomType", currentGeomType);
            //        //****TAGNAME REPLACEMENTS FOR GML3 ----> GML1
            //        //****COORDINATES HERE ***** START ******//
            //        //ANY GEOM HERE EXCEPT POINTS
            //        $(gmlGeomXml).find('posList').replaceWith(function () {
            //            var str = $(this).text().split(' ');
            //            var result = '';
            //            for (var c = 0; c < str.length; c++) {
            //                result += JSON.stringify(str[c]);
            //                if (c % 2 === 0) {
            //                    result += ",";
            //                } else {
            //                    result += " ";
            //                }
            //            }
            //            return '<coordinates>' + result.replace(/['"]+/g, '') + '</coordinates>';
            //        });
            //        //JUST POINTS
            //        $(gmlGeomXml).find('pos').replaceWith(function () {
            //            var str = $(this).text().split(' ');
            //            var result = '';
            //            for (var c = 0; c < str.length; c++) {
            //                result += JSON.stringify(str[c]);
            //                if (c % 2 === 0) {
            //                    result += ",";
            //                } else {
            //                    result += " ";
            //                }
            //            }
            //            return '<coordinates>' + result.replace(/['"]+/g, '') + '</coordinates>';
            //        });
            //        //****COORDINATES HERE ***** END ******//
            //        //****GEOMETRY TAGS HERE
            //        //POLYGONS
            //        $(gmlGeomXml).find('exterior').replaceWith(function () {
            //            return '<outerBoundaryIs>' + $(this).html() + '</outerBoundaryIs>';
            //        });
            //        $(gmlGeomXml).find('interior').replaceWith(function () {
            //            return '<innerBoundaryIs>' + $(this).html() + '</innerBoundaryIs>';
            //        });
            //        //LINES-MULTILINES
            //        $(gmlGeomXml).find('curveMember').replaceWith(function () {
            //            return '<lineStringMember>' + $(this).html() + '</lineStringMember>';
            //        });
            //        $(gmlGeomXml).find('MultiCurve').replaceWith(function () {
            //            return '<MultiLineString>' + $(this).html() + '</MultiLineString>';
            //        });
            //        //BUILD THE GML NODES DEPENDING ON THE GEOMETRY TYPE
            //        var gmlNodeStr = "";
            //        if (currentGeomType === 'Point') {
            //            gmlNodeStr = '<featureMember><geom><geomType>' + currentGeomType + '</geomType><lyrname>' + objs[g].lyrname + '</lyrname><label>' + objs[g].feats[f].getGeometryName() + '</label><pointProperty>'
            //                + $(gmlGeomXml).html() +
            //                '</pointProperty></geom></featureMember>';
            //        }
            //        if (currentGeomType === 'Polygon') {
            //            gmlNodeStr = '<featureMember><geom><geomType>' + currentGeomType + '</geomType><lyrname>' + objs[g].lyrname + '</lyrname><label>' + objs[g].feats[f].getGeometryName() + '</label><polygonMember>'
            //                + $(gmlGeomXml).html() +
            //                '</polygonMember></geom></featureMember>';
            //        }
            //        if (currentGeomType === 'MultiLineString') {
            //            gmlNodeStr = '<featureMember><geom><geomType>' + currentGeomType + '</geomType><lyrname>' + objs[g].lyrname + '</lyrname><label>' + objs[g].feats[f].getGeometryName() + '</label>'
            //                + $(gmlGeomXml).html() +
            //                '</geom></featureMember>';
            //        }
            //        if (currentGeomType === 'LineString') {
            //            gmlNodeStr = '<featureMember><geom><geomType>' + currentGeomType + '</geomType><lyrname>' + objs[g].lyrname + '</lyrname><label>' + objs[g].feats[f].getGeometryName() + '</label><lineProperty>'
            //                + $(gmlGeomXml).html() +
            //                '</lineProperty></geom></featureMember>';
            //        }
            //        featureMembers.push(gmlNodeStr);
            //    }
            //    var xmlFeaturesBody = "";
            //    for (var t = 0; t < featureMembers.length; t++) {
            //        xmlFeaturesBody = xmlFeaturesBody + featureMembers[t];
            //    }
            //    var xmlBodyLyr =
            //        "<UserLayer><Name>Inline" + g + "</Name>" +
            //        "<InlineFeature><FeatureCollection>" +
            //        xmlFeaturesBody +
            //        "</FeatureCollection></InlineFeature>" +
            //        styleStr +
            //        "</UserLayer>";
            //    xmlBodyNodes.push(xmlBodyLyr);
            //}
            //var reqExtent = npMapThis.map.getView().calculateExtent(npMapThis.map.getSize()); 
            ////console.log("reqExtent", reqExtent);
            //var extentGeom = new ol.geom.Polygon([[
            //    [reqExtent[0], reqExtent[1]],
            //    [reqExtent[0], reqExtent[3]],
            //    [reqExtent[2], reqExtent[3]],
            //    [reqExtent[2], reqExtent[1]],
            //    [reqExtent[0], reqExtent[1]],
            //]]);
            //var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:2100')
            //extentGeom.transform(
            //    ol.proj.get('EPSG:3857'),
            //    ol.proj.get(coordinateSystem)
            //);
            //var reqExtentTrans = extentGeom.getExtent();
            ////console.log("extentGeom", extentGeom);
            //var xmlReq1Start =
            //    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            //    "<ogc:GetMap xmlns:ogc=\"http://www.opengis.net/ows\" \n" +
            //    " xmlns:gml=\"http://www.opengis.net/gml\" \n" +
            //    " version=\"1.2.0\"\n" +
            //    " service=\"WMS\">\n" +
            //    "\n" +
            //    "  <StyledLayerDescriptor version=\"1.0.0\" \n" +
            //    "                         xsi:schemaLocation=\"http://www.opengis.net/sld StyledLayerDescriptor.xsd\"\n" +
            //    "                         xmlns=\"http://www.opengis.net/sld\" \n" +
            //    "                         xmlns:ogc=\"http://www.opengis.net/ogc\" \n" +
            //    "                         xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n" +
            //    "                         xmlns:dave=\"http://blasby.com\" \n" +
            //    "                         xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n";
            //if (fileExt !== "kml") {//include orthos layer if not kml
            //    xmlReq1Start = xmlReq1Start + "<NamedLayer><Name>sdo_test:orthos14</Name></NamedLayer> ";
            //}
            //var xmlBodyNodesStr = '';
            //for (var t = 0; t < xmlBodyNodes.length; t++) {
            //    xmlBodyNodesStr = xmlBodyNodesStr + xmlBodyNodes[t];
            //}
            //var xmlFinal =
            //    xmlReq1Start +
            //    xmlBodyNodesStr +
            //    '</StyledLayerDescriptor>' +
            //    '<BoundingBox srsName="http://www.opengis.net/gml/srs/epsg.xml#2100">' +
            //    '<gml:coord>' +
            //    '<gml:X>' + reqExtentTrans[0] + '</gml:X>' +
            //    '<gml:Y>' + reqExtentTrans[1] + '</gml:Y>' +
            //    '</gml:coord>' +
            //    '<gml:coord>' +
            //    '<gml:X>' + reqExtentTrans[2] + '</gml:X>' +
            //    '<gml:Y>' + reqExtentTrans[3] + '</gml:Y>' +
            //    '</gml:coord>' +
            //    '</BoundingBox>' +
            //    '<Output>' +
            //    '<Format>' + format + '</Format>' +
            //    '<Transparent>false</Transparent>' +
            //    '<Size>' +
            //    '<Width>' + npMapThis.map.getSize()[0] + '</Width>' +
            //    '<Height>' + npMapThis.map.getSize()[1] + '</Height>' +
            //    '</Size>' +
            //    '</Output>' +
            //    '<Exceptions>application/vnd.ogc.se+xml</Exceptions>' +
            //    '</ogc:GetMap>';
            //console.log("xmlFinal", xmlFinal);
            //var self = this;
            //var url = '/Orders/rest/Customer/proxygetwmsinlinesld';
            //this.grp.httpPost(url,
            //    {
            //        targetURL: "http://geoserver.c-gaia.gr/geoserver",
            //        XMLRequest: xmlFinal
            //    },
            //    (response: any) => {
            //        var data = base64DecToArr(response);
            //        var blob = new Blob([data], { type: format });
            //        saveAs(blob, "mymap." + fileExt);
            //    });
        };
        /**
         * This is now used for exporting to PDF
         * But there is some CORS error when dealing with
         * geoserver tile layer as image header is missing
         * "Access-Control-Allow-Origin" attribute
         */
        NpMap.prototype.saveMapAsPDF2 = function () {
            var self = this;
            var dims = {
                a0: [1189, 841],
                a1: [841, 594],
                a2: [594, 420],
                a3: [420, 297],
                a4: [297, 210],
                a5: [210, 148]
            };
            var format = 'a4';
            var resolution = 72;
            var dim = dims[format];
            var width = Math.round(dim[0] * resolution / 25.4);
            var height = Math.round(dim[1] * resolution / 25.4);
            var size = this.map.getSize();
            var extent = this.map.getView().calculateExtent(size);
            var activeLayer = this.tileSelect.getActiveTileLayer();
            var source = activeLayer.getSource();
            var loading = 0;
            var loaded = 0;
            var tileLoadStart = function () {
                ++loading;
            };
            var tileLoadEnd = function () {
                ++loaded;
                if (loading === loaded) {
                    var canvas = this;
                    window.setTimeout(function () {
                        loading = 0;
                        loaded = 0;
                        var data = canvas.toDataURL('image/jpeg');
                        //data.setAttribute('crossOrigin', 'anonymous');
                        var pdf = new jsPDF('landscape', undefined, format);
                        var centerX = width / 2.;
                        var centerY = height / 2.;
                        var coords = self.map.getCoordinateFromPixel([centerX, centerY]);
                        var srcProj = 'EPSG:3857';
                        var dstProj = NpGeoGlobals.commonCoordinateSystems(self.coordinateSystem);
                        coords = proj4(srcProj, dstProj, coords);
                        var msg = Sprintf.sprintf('%.4f  %.4f', coords[0], coords[1]);
                        pdf.setFontSize(20);
                        pdf.text(10, 10, msg.replace(/\./g, ",") + ' - ' + (new Date()).toDateString());
                        pdf.addImage(data, 'JPEG', 0, 20, dim[0], dim[1]);
                        pdf.save('NIVAmap.pdf');
                        source.un('tileloadstart', tileLoadStart);
                        source.un('tileloadend', tileLoadEnd, canvas);
                        source.un('tileloaderror', tileLoadEnd, canvas);
                        self.map.setSize(size);
                        self.map.getView().fit(extent, size);
                        self.map.renderSync();
                    }, 100);
                }
            };
            this.map.once('postcompose', function (event) {
                source.on('tileloadstart', tileLoadStart);
                source.on('tileloadend', tileLoadEnd, event.context.canvas);
                source.on('tileloaderror', tileLoadEnd, event.context.canvas);
            });
            self.map.setSize([width, height]);
            self.map.getView().fit(extent, /** @type {ol.Size} */ (this.map.getSize()));
            self.map.renderSync();
        };
        NpMap.prototype.saveMapAsPDF1 = function () {
            var canvas = document.querySelector('canvas');
            var image = new Image();
            //image.src = (<any>canvas).toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");  // here is the most important part because if you dont replace you will get a DOM 18 exception.
            image.src = canvas.toDataURL('image/png');
            image.setAttribute('crossOrigin', 'anonymous');
            var doc = new jsPDF('landscape', undefined, 'a4');
            var width = this.map.getViewport().clientWidth;
            var height = this.map.getViewport().clientHeight;
            var yofs = 210 * height / width;
            doc.addImage(image, 'JPEG', 0, 0, 210, yofs);
            //doc.addImage(image);
            doc.save('map.pdf');
        };
        // get the current map contents in PDF format, in less than 250K
        NpMap.prototype.saveMapAsPDF = function () {
            var self = this;
            var ratio = 1.0;
            function blobProcessor(blobImage) {
                if (blobImage.size > 250000 && ratio > 0.3) {
                    ratio = ratio - 0.1;
                    self.canvas.toBlob(blobProcessor, 'image/jpeg', ratio);
                    return;
                }
                else {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        var base64data = reader.result;
                        var doc = new jsPDF();
                        var width = self.map.getViewport().clientWidth;
                        var height = self.map.getViewport().clientHeight;
                        var centerX = width / 2.;
                        var centerY = height / 2.;
                        var coords = self.map.getCoordinateFromPixel([centerX, centerY]);
                        var srcProj = 'EPSG:3857';
                        var dstProj = NpGeoGlobals.commonCoordinateSystems(self.coordinateSystem);
                        coords = proj4(srcProj, dstProj, coords);
                        var msg = Sprintf.sprintf('%.2f  %.2f', coords[0], coords[1]);
                        var yofs = 210 * height / width;
                        doc.addImage(base64data, 'JPEG', 0, 0, 210, yofs);
                        doc.text(msg.replace(/\./g, ",") + ' - ' + (new Date()).toDateString(), 5, yofs + 10, undefined);
                        doc.save('map.pdf');
                    };
                    reader.readAsDataURL(blobImage);
                }
            }
            self.canvas.toBlob(blobProcessor, 'image/jpeg', ratio);
        };
        NpMap.prototype.isVisible = function () {
            var controllerVisibleDiv = $('#' + this.divId + ':visible');
            if (controllerVisibleDiv.length !== 1)
                return false;
            //controller is visible but it may be within  a tab which is under construction
            var parentTabControls = controllerVisibleDiv.parents('div.ui-tabs');
            if (parentTabControls.length === 0)
                return true; // not tab control, so controller is visible
            //every surrounding tab control must have the .ui-widget class
            var bSomeTabAreUnderConstruction = parentTabControls.not('.ui-widget').length > 0;
            if (bSomeTabAreUnderConstruction)
                return false;
            return true;
        };
        NpMap.pixelTolerance = 10;
        // Global cache of SqlLayerEntities.
        //   Key: mapId + "#@#" + layerId + "#@#" + sqlLayerEntity.id
        //   Value: the SqlLayerEntity and the last timestamp of read access
        //                                 (used to evict from the global cache)
        NpMap.cachedVectorsOfMap = {};
        return NpMap;
    })();
    NpGeo.NpMap = NpMap;
})(NpGeo || (NpGeo = {}));
//# sourceMappingURL=geoLib.js.map