//CBB799A9D06D643A0287B914444081F6
//Εxtended classes
/// <reference path="../EntitiesBase/SuperClasBase.ts" />

module Entities {

    export class SuperClas extends Entities.SuperClasBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<SuperClas> {
            return Entities.SuperClasBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
