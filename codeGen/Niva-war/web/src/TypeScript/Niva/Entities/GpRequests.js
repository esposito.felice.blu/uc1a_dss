//8C2AB470591742554CABFC00A2F2C215
//Εxtended classes
/// <reference path="../EntitiesBase/GpRequestsBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var GpRequests = (function (_super) {
        __extends(GpRequests, _super);
        function GpRequests() {
            _super.apply(this, arguments);
        }
        GpRequests.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.GpRequestsBase.fromJSONComplete(data, deserializedEntities);
        };
        return GpRequests;
    })(Entities.GpRequestsBase);
    Entities.GpRequests = GpRequests;
})(Entities || (Entities = {}));
//# sourceMappingURL=GpRequests.js.map