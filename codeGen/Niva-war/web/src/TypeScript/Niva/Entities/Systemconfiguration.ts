//4D8336E4660E913332B629C22B184042
//Εxtended classes
/// <reference path="../EntitiesBase/SystemconfigurationBase.ts" />

module Entities {

    export class Systemconfiguration extends Entities.SystemconfigurationBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Systemconfiguration> {
            return Entities.SystemconfigurationBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
