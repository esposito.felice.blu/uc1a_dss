//B56E3E1C6E01E5888A2A0E2AEC2091D7
//Εxtended classes
/// <reference path="../EntitiesBase/ClassifierBase.ts" />

module Entities {

    export class Classifier extends Entities.ClassifierBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Classifier> {
            return Entities.ClassifierBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
