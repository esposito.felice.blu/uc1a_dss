//06448CD982E32B903599BB46B97B1281
//Εxtended classes
/// <reference path="../EntitiesBase/AgrisnapUsersProducersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var AgrisnapUsersProducers = (function (_super) {
        __extends(AgrisnapUsersProducers, _super);
        function AgrisnapUsersProducers() {
            _super.apply(this, arguments);
        }
        AgrisnapUsersProducers.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.AgrisnapUsersProducersBase.fromJSONComplete(data, deserializedEntities);
        };
        return AgrisnapUsersProducers;
    })(Entities.AgrisnapUsersProducersBase);
    Entities.AgrisnapUsersProducers = AgrisnapUsersProducers;
})(Entities || (Entities = {}));
//# sourceMappingURL=AgrisnapUsersProducers.js.map