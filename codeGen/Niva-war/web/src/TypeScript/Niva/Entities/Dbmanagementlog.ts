//8981DD36E2C64B40F2C71D4514612C92
//Εxtended classes
/// <reference path="../EntitiesBase/DbmanagementlogBase.ts" />

module Entities {

    export class Dbmanagementlog extends Entities.DbmanagementlogBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Dbmanagementlog> {
            return Entities.DbmanagementlogBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
