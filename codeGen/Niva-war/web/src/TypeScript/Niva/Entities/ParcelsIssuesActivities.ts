//63FD56D735E84F419E755E23BF0BB296
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelsIssuesActivitiesBase.ts" />

module Entities {

    export class ParcelsIssuesActivities extends Entities.ParcelsIssuesActivitiesBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelsIssuesActivities> {
            return Entities.ParcelsIssuesActivitiesBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
