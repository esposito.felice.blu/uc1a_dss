//8C2AB470591742554CABFC00A2F2C215
//Εxtended classes
/// <reference path="../EntitiesBase/GpRequestsBase.ts" />

module Entities {

    export class GpRequests extends Entities.GpRequestsBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpRequests> {
            return Entities.GpRequestsBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
