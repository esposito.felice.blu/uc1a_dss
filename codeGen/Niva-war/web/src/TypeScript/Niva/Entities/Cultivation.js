//1D330186A0A22CF942771EB1B15BC664
//Εxtended classes
/// <reference path="../EntitiesBase/CultivationBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Cultivation = (function (_super) {
        __extends(Cultivation, _super);
        function Cultivation() {
            _super.apply(this, arguments);
        }
        Cultivation.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.CultivationBase.fromJSONComplete(data, deserializedEntities);
        };
        return Cultivation;
    })(Entities.CultivationBase);
    Entities.Cultivation = Cultivation;
})(Entities || (Entities = {}));
//# sourceMappingURL=Cultivation.js.map