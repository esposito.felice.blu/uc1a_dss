//6874ABE2275E13EC443F4559D1C76B58
//Εxtended classes
/// <reference path="../EntitiesBase/EcCultDetailBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcCultDetail = (function (_super) {
        __extends(EcCultDetail, _super);
        function EcCultDetail() {
            _super.apply(this, arguments);
        }
        EcCultDetail.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.EcCultDetailBase.fromJSONComplete(data, deserializedEntities);
        };
        return EcCultDetail;
    })(Entities.EcCultDetailBase);
    Entities.EcCultDetail = EcCultDetail;
})(Entities || (Entities = {}));
//# sourceMappingURL=EcCultDetail.js.map