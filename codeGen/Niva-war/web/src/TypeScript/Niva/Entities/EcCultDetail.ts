//6874ABE2275E13EC443F4559D1C76B58
//Εxtended classes
/// <reference path="../EntitiesBase/EcCultDetailBase.ts" />

module Entities {

    export class EcCultDetail extends Entities.EcCultDetailBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcCultDetail> {
            return Entities.EcCultDetailBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
