//8BC5DADB19580B5822BD906B89FC71B9
//Εxtended classes
/// <reference path="../EntitiesBase/TemplateColumnBase.ts" />

module Entities {

    export class TemplateColumn extends Entities.TemplateColumnBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<TemplateColumn> {
            return Entities.TemplateColumnBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
