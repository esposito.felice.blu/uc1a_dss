//B22C91017AA9CB00834401391B017D44
//Εxtended classes
/// <reference path="../EntitiesBase/EcGroupCopyBase.ts" />

module Entities {

    export class EcGroupCopy extends Entities.EcGroupCopyBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcGroupCopy> {
            return Entities.EcGroupCopyBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
