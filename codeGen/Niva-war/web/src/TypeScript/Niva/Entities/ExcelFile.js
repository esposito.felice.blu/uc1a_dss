//75C6CAA90BB656AFB63C8002C1120C7D
//Εxtended classes
/// <reference path="../EntitiesBase/ExcelFileBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ExcelFile = (function (_super) {
        __extends(ExcelFile, _super);
        function ExcelFile() {
            _super.apply(this, arguments);
        }
        ExcelFile.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ExcelFileBase.fromJSONComplete(data, deserializedEntities);
        };
        return ExcelFile;
    })(Entities.ExcelFileBase);
    Entities.ExcelFile = ExcelFile;
})(Entities || (Entities = {}));
//# sourceMappingURL=ExcelFile.js.map