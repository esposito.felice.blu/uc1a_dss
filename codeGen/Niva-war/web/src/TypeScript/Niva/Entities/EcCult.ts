//679F4CAA3C958057503C79BC3484FF5A
//Εxtended classes
/// <reference path="../EntitiesBase/EcCultBase.ts" />

module Entities {

    export class EcCult extends Entities.EcCultBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcCult> {
            return Entities.EcCultBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
