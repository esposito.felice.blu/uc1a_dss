//1EC91412567CD2E07112D9939E1523B7
//Εxtended classes
/// <reference path="../EntitiesBase/GpDecisionBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var GpDecision = (function (_super) {
        __extends(GpDecision, _super);
        function GpDecision() {
            _super.apply(this, arguments);
        }
        GpDecision.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.GpDecisionBase.fromJSONComplete(data, deserializedEntities);
        };
        return GpDecision;
    })(Entities.GpDecisionBase);
    Entities.GpDecision = GpDecision;
})(Entities || (Entities = {}));
//# sourceMappingURL=GpDecision.js.map