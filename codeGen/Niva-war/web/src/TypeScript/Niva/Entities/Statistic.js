//0FFD49D2400DDDD038E9EB98547369D2
//Εxtended classes
/// <reference path="../EntitiesBase/StatisticBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Statistic = (function (_super) {
        __extends(Statistic, _super);
        function Statistic() {
            _super.apply(this, arguments);
        }
        Statistic.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.StatisticBase.fromJSONComplete(data, deserializedEntities);
        };
        return Statistic;
    })(Entities.StatisticBase);
    Entities.Statistic = Statistic;
})(Entities || (Entities = {}));
//# sourceMappingURL=Statistic.js.map