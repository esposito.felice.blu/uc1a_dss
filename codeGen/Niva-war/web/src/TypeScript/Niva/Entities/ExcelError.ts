//CB3B97821C9756A90CFD9F0AACF5E1F2
//Εxtended classes
/// <reference path="../EntitiesBase/ExcelErrorBase.ts" />

module Entities {

    export class ExcelError extends Entities.ExcelErrorBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ExcelError> {
            return Entities.ExcelErrorBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
