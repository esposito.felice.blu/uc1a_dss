//1C9A945047EF39DEADDBE29248B53328
//Εxtended classes
/// <reference path="../EntitiesBase/TestParcTmpBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var TestParcTmp = (function (_super) {
        __extends(TestParcTmp, _super);
        function TestParcTmp() {
            _super.apply(this, arguments);
        }
        TestParcTmp.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.TestParcTmpBase.fromJSONComplete(data, deserializedEntities);
        };
        return TestParcTmp;
    })(Entities.TestParcTmpBase);
    Entities.TestParcTmp = TestParcTmp;
})(Entities || (Entities = {}));
//# sourceMappingURL=TestParcTmp.js.map