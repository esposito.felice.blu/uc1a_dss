//789955616F10056F04D87443A31F5BB3
//Εxtended classes
/// <reference path="../EntitiesBase/FmisUploadBase.ts" />

module Entities {

    export class FmisUpload extends Entities.FmisUploadBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FmisUpload> {
            return Entities.FmisUploadBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
