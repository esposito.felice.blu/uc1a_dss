//B3935997F0AF3CEBA7F831687B4CE271
//Εxtended classes
/// <reference path="../EntitiesBase/FileDirPathBase.ts" />

module Entities {

    export class FileDirPath extends Entities.FileDirPathBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FileDirPath> {
            return Entities.FileDirPathBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
