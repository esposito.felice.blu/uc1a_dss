//E9BCA2B557C99BC1BD6A2BA0E968DAA6
//Εxtended classes
/// <reference path="../EntitiesBase/AgencyBase.ts" />

module Entities {

    export class Agency extends Entities.AgencyBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Agency> {
            return Entities.AgencyBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
