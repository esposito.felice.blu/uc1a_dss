/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/GpRequestsContexts.ts" />
/// <reference path="../Entities/GpRequests.ts" />
/// <reference path="../Entities/Producers.ts" />
/// <reference path="../Entities/GpRequestsProducers.ts" />

module Entities {

    export class GpRequestsProducersBase extends NpTypes.BaseEntity {
        constructor(
            gpRequestsProducersId: string,
            rowVersion: number,
            notes: string,
            gpRequestsId: GpRequests,
            producersId: Producers) 
        {
            super();
            var self = this;
            this._gpRequestsProducersId = new NpTypes.UIStringModel(gpRequestsProducersId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._notes = new NpTypes.UIStringModel(notes, this);
            this._gpRequestsId = new NpTypes.UIManyToOneModel<Entities.GpRequests>(gpRequestsId, this);
            this._producersId = new NpTypes.UIManyToOneModel<Entities.Producers>(producersId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.gpRequestsProducersId; 
        }
        
        public getKeyName(): string { 
            return "gpRequestsProducersId"; 
        }

        public getEntityName(): string { 
            return 'GpRequestsProducers'; 
        }
        
        public fromJSON(data: any): GpRequestsProducers[] { 
            return GpRequestsProducers.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.gpRequestsProducersId === null || this.gpRequestsProducersId === undefined)
                return true;
            if (this.gpRequestsProducersId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: GpRequestsProducersBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //gpRequestsProducersId property
        _gpRequestsProducersId: NpTypes.UIStringModel;
        public get gpRequestsProducersId():string {
            return this._gpRequestsProducersId.value;
        }
        public set gpRequestsProducersId(gpRequestsProducersId:string) {
            var self = this;
            self._gpRequestsProducersId.value = gpRequestsProducersId;
        //    self.gpRequestsProducersId_listeners.forEach(cb => { cb(<GpRequestsProducers>self); });
        }
        //public gpRequestsProducersId_listeners: Array<(c:GpRequestsProducers) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<GpRequestsProducers>self); });
        }
        //public rowVersion_listeners: Array<(c:GpRequestsProducers) => void> = [];
        //notes property
        _notes: NpTypes.UIStringModel;
        public get notes():string {
            return this._notes.value;
        }
        public set notes(notes:string) {
            var self = this;
            self._notes.value = notes;
        //    self.notes_listeners.forEach(cb => { cb(<GpRequestsProducers>self); });
        }
        //public notes_listeners: Array<(c:GpRequestsProducers) => void> = [];
        //gpRequestsId property
        _gpRequestsId: NpTypes.UIManyToOneModel<Entities.GpRequests>;
        public get gpRequestsId():GpRequests {
            return this._gpRequestsId.value;
        }
        public set gpRequestsId(gpRequestsId:GpRequests) {
            var self = this;
            self._gpRequestsId.value = gpRequestsId;
        //    self.gpRequestsId_listeners.forEach(cb => { cb(<GpRequestsProducers>self); });
        }
        //public gpRequestsId_listeners: Array<(c:GpRequestsProducers) => void> = [];
        //producersId property
        _producersId: NpTypes.UIManyToOneModel<Entities.Producers>;
        public get producersId():Producers {
            return this._producersId.value;
        }
        public set producersId(producersId:Producers) {
            var self = this;
            self._producersId.value = producersId;
        //    self.producersId_listeners.forEach(cb => { cb(<GpRequestsProducers>self); });
        }
        //public producersId_listeners: Array<(c:GpRequestsProducers) => void> = [];
        public static gpRequestsContextsCollection: (self:Entities.GpRequestsProducers, func: (gpRequestsContextsList: Entities.GpRequestsContexts[]) => void) => void = null; //(gpRequestsProducers, list) => { };
        public gpRequestsContextsCollection(func: (gpRequestsContextsList: Entities.GpRequestsContexts[]) => void) {
            var self: Entities.GpRequestsProducersBase = this;
        	if (!isVoid(GpRequestsProducers.gpRequestsContextsCollection)) {
        		if (self instanceof Entities.GpRequestsProducers) {
        			GpRequestsProducers.gpRequestsContextsCollection(<Entities.GpRequestsProducers>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.gpRequestsProducersId_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.notes_listeners.splice(0);
            self.gpRequestsId_listeners.splice(0);
            self.producersId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : GpRequestsProducers {
            var key = "GpRequestsProducers:"+x.gpRequestsProducersId;
            var ret =  new GpRequestsProducers(
                x.gpRequestsProducersId,
                x.rowVersion,
                x.notes,
                x.gpRequestsId,
                x.producersId
            );
            deserializedEntities[key] = ret;
            ret.gpRequestsId = (x.gpRequestsId !== undefined && x.gpRequestsId !== null) ? <GpRequests>NpTypes.BaseEntity.entitiesFactory[x.gpRequestsId.$entityName].fromJSONPartial(x.gpRequestsId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.producersId = (x.producersId !== undefined && x.producersId !== null) ? <Producers>NpTypes.BaseEntity.entitiesFactory[x.producersId.$entityName].fromJSONPartial(x.producersId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): GpRequestsProducers {
            
            return <GpRequestsProducers>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : GpRequestsProducers {
            var self = this;
            var key="";
            var ret:GpRequestsProducers = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "GpRequestsProducers:"+x.$refId;
                ret = <GpRequestsProducers>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "GpRequestsProducersBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "GpRequestsProducers:"+x.gpRequestsProducersId;
                    var cachedCopy = <GpRequestsProducers>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = GpRequestsProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = GpRequestsProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = GpRequestsProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpRequestsProducers> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<GpRequestsProducers> = _.map(data, (x:any):GpRequestsProducers => {
                return GpRequestsProducers.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:GpRequestsProducers[])=>void)  {
            var url = "/Niva/rest/GpRequestsProducers/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = GpRequestsProducers.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.gpRequestsProducersId = this.gpRequestsProducersId;
                ret.rowVersion = this.rowVersion;
                ret.notes = this.notes;
                ret.gpRequestsId = 
                    (this.gpRequestsId !== undefined && this.gpRequestsId !== null) ? 
                        { gpRequestsId :  this.gpRequestsId.gpRequestsId} :
                        (this.gpRequestsId === undefined ? undefined : null);
                ret.producersId = 
                    (this.producersId !== undefined && this.producersId !== null) ? 
                        { producersId :  this.producersId.producersId} :
                        (this.producersId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:GpRequestsProducers):void {
            var self = this;
            self.gpRequestsProducersId = other.gpRequestsProducersId
            self.rowVersion = other.rowVersion
            self.notes = other.notes
            self.gpRequestsId = other.gpRequestsId
            self.producersId = other.producersId
        }

        public static Create() : GpRequestsProducers {
            return new GpRequestsProducers(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(gpRequestsProducersId:string) : GpRequestsProducers {
            var ret = GpRequestsProducers.Create();
            ret.gpRequestsProducersId = gpRequestsProducersId;
            return ret;
        }

        public static cashedEntities: { [id: string]: GpRequestsProducers; } = {};
        public static findById_unecrypted(gpRequestsProducersId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : GpRequestsProducers {
            if (Entities.GpRequestsProducers.cashedEntities[gpRequestsProducersId.toString()] !== undefined)
                return Entities.GpRequestsProducers.cashedEntities[gpRequestsProducersId.toString()];

            var wsPath = "GpRequestsProducers/findByGpRequestsProducersId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['gpRequestsProducersId'] = gpRequestsProducersId;
            var ret = GpRequestsProducers.Create();
            Entities.GpRequestsProducers.cashedEntities[gpRequestsProducersId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.GpRequestsProducers.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + gpRequestsProducersId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['GpRequestsProducers'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.GpRequestsProducers.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.GpRequestsProducers.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}