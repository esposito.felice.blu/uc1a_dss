/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/ParcelsIssuesActivities.ts" />

module Entities {

    export class ParcelsIssuesActivitiesBase extends NpTypes.BaseEntity {
        constructor(
            parcelsIssuesActivitiesId: string,
            rowVersion: number,
            type: number,
            dteTimestamp: Date,
            typeExtrainfo: string,
            parcelsIssuesId: ParcelsIssues) 
        {
            super();
            var self = this;
            this._parcelsIssuesActivitiesId = new NpTypes.UIStringModel(parcelsIssuesActivitiesId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._type = new NpTypes.UINumberModel(type, this);
            this._dteTimestamp = new NpTypes.UIDateModel(dteTimestamp, this);
            this._typeExtrainfo = new NpTypes.UIStringModel(typeExtrainfo, this);
            this._parcelsIssuesId = new NpTypes.UIManyToOneModel<Entities.ParcelsIssues>(parcelsIssuesId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.parcelsIssuesActivitiesId; 
        }
        
        public getKeyName(): string { 
            return "parcelsIssuesActivitiesId"; 
        }

        public getEntityName(): string { 
            return 'ParcelsIssuesActivities'; 
        }
        
        public fromJSON(data: any): ParcelsIssuesActivities[] { 
            return ParcelsIssuesActivities.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.parcelsIssuesActivitiesId === null || this.parcelsIssuesActivitiesId === undefined)
                return true;
            if (this.parcelsIssuesActivitiesId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: ParcelsIssuesActivitiesBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //parcelsIssuesActivitiesId property
        _parcelsIssuesActivitiesId: NpTypes.UIStringModel;
        public get parcelsIssuesActivitiesId():string {
            return this._parcelsIssuesActivitiesId.value;
        }
        public set parcelsIssuesActivitiesId(parcelsIssuesActivitiesId:string) {
            var self = this;
            self._parcelsIssuesActivitiesId.value = parcelsIssuesActivitiesId;
        //    self.parcelsIssuesActivitiesId_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
        }
        //public parcelsIssuesActivitiesId_listeners: Array<(c:ParcelsIssuesActivities) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
        }
        //public rowVersion_listeners: Array<(c:ParcelsIssuesActivities) => void> = [];
        //type property
        _type: NpTypes.UINumberModel;
        public get type():number {
            return this._type.value;
        }
        public set type(type:number) {
            var self = this;
            self._type.value = type;
        //    self.type_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
        }
        //public type_listeners: Array<(c:ParcelsIssuesActivities) => void> = [];
        //dteTimestamp property
        _dteTimestamp: NpTypes.UIDateModel;
        public get dteTimestamp():Date {
            return this._dteTimestamp.value;
        }
        public set dteTimestamp(dteTimestamp:Date) {
            var self = this;
            self._dteTimestamp.value = dteTimestamp;
        //    self.dteTimestamp_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
        }
        //public dteTimestamp_listeners: Array<(c:ParcelsIssuesActivities) => void> = [];
        //typeExtrainfo property
        _typeExtrainfo: NpTypes.UIStringModel;
        public get typeExtrainfo():string {
            return this._typeExtrainfo.value;
        }
        public set typeExtrainfo(typeExtrainfo:string) {
            var self = this;
            self._typeExtrainfo.value = typeExtrainfo;
        //    self.typeExtrainfo_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
        }
        //public typeExtrainfo_listeners: Array<(c:ParcelsIssuesActivities) => void> = [];
        //parcelsIssuesId property
        _parcelsIssuesId: NpTypes.UIManyToOneModel<Entities.ParcelsIssues>;
        public get parcelsIssuesId():ParcelsIssues {
            return this._parcelsIssuesId.value;
        }
        public set parcelsIssuesId(parcelsIssuesId:ParcelsIssues) {
            var self = this;
            self._parcelsIssuesId.value = parcelsIssuesId;
        //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
        }
        //public parcelsIssuesId_listeners: Array<(c:ParcelsIssuesActivities) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.parcelsIssuesActivitiesId_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.type_listeners.splice(0);
            self.dteTimestamp_listeners.splice(0);
            self.typeExtrainfo_listeners.splice(0);
            self.parcelsIssuesId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : ParcelsIssuesActivities {
            var key = "ParcelsIssuesActivities:"+x.parcelsIssuesActivitiesId;
            var ret =  new ParcelsIssuesActivities(
                x.parcelsIssuesActivitiesId,
                x.rowVersion,
                x.type,
                isVoid(x.dteTimestamp) ? null : new Date(x.dteTimestamp),
                x.typeExtrainfo,
                x.parcelsIssuesId
            );
            deserializedEntities[key] = ret;
            ret.parcelsIssuesId = (x.parcelsIssuesId !== undefined && x.parcelsIssuesId !== null) ? <ParcelsIssues>NpTypes.BaseEntity.entitiesFactory[x.parcelsIssuesId.$entityName].fromJSONPartial(x.parcelsIssuesId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): ParcelsIssuesActivities {
            
            return <ParcelsIssuesActivities>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : ParcelsIssuesActivities {
            var self = this;
            var key="";
            var ret:ParcelsIssuesActivities = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcelsIssuesActivities:"+x.$refId;
                ret = <ParcelsIssuesActivities>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcelsIssuesActivitiesBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcelsIssuesActivities:"+x.parcelsIssuesActivitiesId;
                    var cachedCopy = <ParcelsIssuesActivities>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcelsIssuesActivities.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcelsIssuesActivities.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = ParcelsIssuesActivities.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelsIssuesActivities> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<ParcelsIssuesActivities> = _.map(data, (x:any):ParcelsIssuesActivities => {
                return ParcelsIssuesActivities.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:ParcelsIssuesActivities[])=>void)  {
            var url = "/Niva/rest/ParcelsIssuesActivities/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = ParcelsIssuesActivities.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.parcelsIssuesActivitiesId = this.parcelsIssuesActivitiesId;
                ret.rowVersion = this.rowVersion;
                ret.type = this.type;
                ret.dteTimestamp = this.dteTimestamp;
                ret.typeExtrainfo = this.typeExtrainfo;
                ret.parcelsIssuesId = 
                    (this.parcelsIssuesId !== undefined && this.parcelsIssuesId !== null) ? 
                        { parcelsIssuesId :  this.parcelsIssuesId.parcelsIssuesId} :
                        (this.parcelsIssuesId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:ParcelsIssuesActivities):void {
            var self = this;
            self.parcelsIssuesActivitiesId = other.parcelsIssuesActivitiesId
            self.rowVersion = other.rowVersion
            self.type = other.type
            self.dteTimestamp = other.dteTimestamp
            self.typeExtrainfo = other.typeExtrainfo
            self.parcelsIssuesId = other.parcelsIssuesId
        }

        public static Create() : ParcelsIssuesActivities {
            return new ParcelsIssuesActivities(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(parcelsIssuesActivitiesId:string) : ParcelsIssuesActivities {
            var ret = ParcelsIssuesActivities.Create();
            ret.parcelsIssuesActivitiesId = parcelsIssuesActivitiesId;
            return ret;
        }

        public static cashedEntities: { [id: string]: ParcelsIssuesActivities; } = {};
        public static findById_unecrypted(parcelsIssuesActivitiesId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : ParcelsIssuesActivities {
            if (Entities.ParcelsIssuesActivities.cashedEntities[parcelsIssuesActivitiesId.toString()] !== undefined)
                return Entities.ParcelsIssuesActivities.cashedEntities[parcelsIssuesActivitiesId.toString()];

            var wsPath = "ParcelsIssuesActivities/findByParcelsIssuesActivitiesId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['parcelsIssuesActivitiesId'] = parcelsIssuesActivitiesId;
            var ret = ParcelsIssuesActivities.Create();
            Entities.ParcelsIssuesActivities.cashedEntities[parcelsIssuesActivitiesId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.ParcelsIssuesActivities.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + parcelsIssuesActivitiesId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['ParcelsIssuesActivities'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.ParcelsIssuesActivities.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.ParcelsIssuesActivities.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}