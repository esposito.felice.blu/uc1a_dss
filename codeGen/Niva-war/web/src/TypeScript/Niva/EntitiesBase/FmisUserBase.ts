/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/FmisUser.ts" />

module Entities {

    export class FmisUserBase extends NpTypes.BaseEntity {
        constructor(
            fmisName: string,
            rowVersion: number,
            fmisUsersId: string,
            fmisUid: string) 
        {
            super();
            var self = this;
            this._fmisName = new NpTypes.UIStringModel(fmisName, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._fmisUsersId = new NpTypes.UIStringModel(fmisUsersId, this);
            this._fmisUid = new NpTypes.UIStringModel(fmisUid, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.fmisUsersId; 
        }
        
        public getKeyName(): string { 
            return "fmisUsersId"; 
        }

        public getEntityName(): string { 
            return 'FmisUser'; 
        }
        
        public fromJSON(data: any): FmisUser[] { 
            return FmisUser.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.fmisUsersId === null || this.fmisUsersId === undefined)
                return true;
            if (this.fmisUsersId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: FmisUserBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //fmisName property
        _fmisName: NpTypes.UIStringModel;
        public get fmisName():string {
            return this._fmisName.value;
        }
        public set fmisName(fmisName:string) {
            var self = this;
            self._fmisName.value = fmisName;
        //    self.fmisName_listeners.forEach(cb => { cb(<FmisUser>self); });
        }
        //public fmisName_listeners: Array<(c:FmisUser) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<FmisUser>self); });
        }
        //public rowVersion_listeners: Array<(c:FmisUser) => void> = [];
        //fmisUsersId property
        _fmisUsersId: NpTypes.UIStringModel;
        public get fmisUsersId():string {
            return this._fmisUsersId.value;
        }
        public set fmisUsersId(fmisUsersId:string) {
            var self = this;
            self._fmisUsersId.value = fmisUsersId;
        //    self.fmisUsersId_listeners.forEach(cb => { cb(<FmisUser>self); });
        }
        //public fmisUsersId_listeners: Array<(c:FmisUser) => void> = [];
        //fmisUid property
        _fmisUid: NpTypes.UIStringModel;
        public get fmisUid():string {
            return this._fmisUid.value;
        }
        public set fmisUid(fmisUid:string) {
            var self = this;
            self._fmisUid.value = fmisUid;
        //    self.fmisUid_listeners.forEach(cb => { cb(<FmisUser>self); });
        }
        //public fmisUid_listeners: Array<(c:FmisUser) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.fmisName_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.fmisUsersId_listeners.splice(0);
            self.fmisUid_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : FmisUser {
            var key = "FmisUser:"+x.fmisUsersId;
            var ret =  new FmisUser(
                x.fmisName,
                x.rowVersion,
                x.fmisUsersId,
                x.fmisUid
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): FmisUser {
            
            return <FmisUser>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : FmisUser {
            var self = this;
            var key="";
            var ret:FmisUser = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "FmisUser:"+x.$refId;
                ret = <FmisUser>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "FmisUserBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "FmisUser:"+x.fmisUsersId;
                    var cachedCopy = <FmisUser>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = FmisUser.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = FmisUser.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = FmisUser.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FmisUser> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<FmisUser> = _.map(data, (x:any):FmisUser => {
                return FmisUser.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:FmisUser[])=>void)  {
            var url = "/Niva/rest/FmisUser/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = FmisUser.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.fmisName = this.fmisName;
                ret.rowVersion = this.rowVersion;
                ret.fmisUsersId = this.fmisUsersId;
                ret.fmisUid = this.fmisUid;
            return ret;
        }

        public updateInstance(other:FmisUser):void {
            var self = this;
            self.fmisName = other.fmisName
            self.rowVersion = other.rowVersion
            self.fmisUsersId = other.fmisUsersId
            self.fmisUid = other.fmisUid
        }

        public static Create() : FmisUser {
            return new FmisUser(
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(fmisUsersId:string) : FmisUser {
            var ret = FmisUser.Create();
            ret.fmisUsersId = fmisUsersId;
            return ret;
        }

        public static cashedEntities: { [id: string]: FmisUser; } = {};
        public static findById_unecrypted(fmisUsersId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : FmisUser {
            if (Entities.FmisUser.cashedEntities[fmisUsersId.toString()] !== undefined)
                return Entities.FmisUser.cashedEntities[fmisUsersId.toString()];

            var wsPath = "FmisUser/findByFmisUsersId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fmisUsersId'] = fmisUsersId;
            var ret = FmisUser.Create();
            Entities.FmisUser.cashedEntities[fmisUsersId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.FmisUser.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + fmisUsersId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['FmisUser'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.FmisUser.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.FmisUser.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}