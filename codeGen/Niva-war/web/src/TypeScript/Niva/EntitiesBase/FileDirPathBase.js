/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/FileDirPath.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FileDirPathBase = (function (_super) {
        __extends(FileDirPathBase, _super);
        function FileDirPathBase(fidrId, directory, rowVersion, fileType) {
            _super.call(this);
            var self = this;
            this._fidrId = new NpTypes.UIStringModel(fidrId, this);
            this._directory = new NpTypes.UIStringModel(directory, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._fileType = new NpTypes.UINumberModel(fileType, this);
            self.postConstruct();
        }
        FileDirPathBase.prototype.getKey = function () {
            return this.fidrId;
        };
        FileDirPathBase.prototype.getKeyName = function () {
            return "fidrId";
        };
        FileDirPathBase.prototype.getEntityName = function () {
            return 'FileDirPath';
        };
        FileDirPathBase.prototype.fromJSON = function (data) {
            return Entities.FileDirPath.fromJSONComplete(data);
        };
        Object.defineProperty(FileDirPathBase.prototype, "fidrId", {
            get: function () {
                return this._fidrId.value;
            },
            set: function (fidrId) {
                var self = this;
                self._fidrId.value = fidrId;
                //    self.fidrId_listeners.forEach(cb => { cb(<FileDirPath>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FileDirPathBase.prototype, "directory", {
            get: function () {
                return this._directory.value;
            },
            set: function (directory) {
                var self = this;
                self._directory.value = directory;
                //    self.directory_listeners.forEach(cb => { cb(<FileDirPath>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FileDirPathBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<FileDirPath>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FileDirPathBase.prototype, "fileType", {
            get: function () {
                return this._fileType.value;
            },
            set: function (fileType) {
                var self = this;
                self._fileType.value = fileType;
                //    self.fileType_listeners.forEach(cb => { cb(<FileDirPath>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public fileType_listeners: Array<(c:FileDirPath) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.fidrId_listeners.splice(0);
            self.directory_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.fileType_listeners.splice(0);
        }
        */
        FileDirPathBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "FileDirPath:" + x.fidrId;
            var ret = new Entities.FileDirPath(x.fidrId, x.directory, x.rowVersion, x.fileType);
            deserializedEntities[key] = ret;
            return ret;
        };
        FileDirPathBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        FileDirPathBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "FileDirPath:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "FileDirPathBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "FileDirPath:"+x.fidrId;
                    var cachedCopy = <FileDirPath>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = FileDirPath.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = FileDirPath.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.FileDirPath.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        FileDirPathBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.FileDirPath.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        FileDirPathBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/FileDirPath/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.FileDirPath.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        FileDirPathBase.prototype.toJSON = function () {
            var ret = {};
            ret.fidrId = this.fidrId;
            ret.directory = this.directory;
            ret.rowVersion = this.rowVersion;
            ret.fileType = this.fileType;
            return ret;
        };
        FileDirPathBase.prototype.updateInstance = function (other) {
            var self = this;
            self.fidrId = other.fidrId;
            self.directory = other.directory;
            self.rowVersion = other.rowVersion;
            self.fileType = other.fileType;
        };
        FileDirPathBase.Create = function () {
            return new Entities.FileDirPath(undefined, undefined, undefined, undefined);
        };
        FileDirPathBase.CreateById = function (fidrId) {
            var ret = Entities.FileDirPath.Create();
            ret.fidrId = fidrId;
            return ret;
        };
        FileDirPathBase.findById_unecrypted = function (fidrId, $scope, $http, errFunc) {
            if (Entities.FileDirPath.cashedEntities[fidrId.toString()] !== undefined)
                return Entities.FileDirPath.cashedEntities[fidrId.toString()];
            var wsPath = "FileDirPath/findByFidrId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fidrId'] = fidrId;
            var ret = Entities.FileDirPath.Create();
            Entities.FileDirPath.cashedEntities[fidrId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.FileDirPath.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + fidrId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        FileDirPathBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        FileDirPathBase.cashedEntities = {};
        return FileDirPathBase;
    })(NpTypes.BaseEntity);
    Entities.FileDirPathBase = FileDirPathBase;
    NpTypes.BaseEntity.entitiesFactory['FileDirPath'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.FileDirPath.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.FileDirPath.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=FileDirPathBase.js.map