/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Agency.ts" />

module Entities {

    export class AgencyBase extends NpTypes.BaseEntity {
        constructor(
            agenId: string,
            name: string,
            country: string,
            rowVersion: number,
            srid: number) 
        {
            super();
            var self = this;
            this._agenId = new NpTypes.UIStringModel(agenId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._country = new NpTypes.UIStringModel(country, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._srid = new NpTypes.UINumberModel(srid, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.agenId; 
        }
        
        public getKeyName(): string { 
            return "agenId"; 
        }

        public getEntityName(): string { 
            return 'Agency'; 
        }
        
        public fromJSON(data: any): Agency[] { 
            return Agency.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.agenId === null || this.agenId === undefined)
                return true;
            if (this.agenId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: AgencyBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //agenId property
        _agenId: NpTypes.UIStringModel;
        public get agenId():string {
            return this._agenId.value;
        }
        public set agenId(agenId:string) {
            var self = this;
            self._agenId.value = agenId;
        //    self.agenId_listeners.forEach(cb => { cb(<Agency>self); });
        }
        //public agenId_listeners: Array<(c:Agency) => void> = [];
        //name property
        _name: NpTypes.UIStringModel;
        public get name():string {
            return this._name.value;
        }
        public set name(name:string) {
            var self = this;
            self._name.value = name;
        //    self.name_listeners.forEach(cb => { cb(<Agency>self); });
        }
        //public name_listeners: Array<(c:Agency) => void> = [];
        //country property
        _country: NpTypes.UIStringModel;
        public get country():string {
            return this._country.value;
        }
        public set country(country:string) {
            var self = this;
            self._country.value = country;
        //    self.country_listeners.forEach(cb => { cb(<Agency>self); });
        }
        //public country_listeners: Array<(c:Agency) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<Agency>self); });
        }
        //public rowVersion_listeners: Array<(c:Agency) => void> = [];
        //srid property
        _srid: NpTypes.UINumberModel;
        public get srid():number {
            return this._srid.value;
        }
        public set srid(srid:number) {
            var self = this;
            self._srid.value = srid;
        //    self.srid_listeners.forEach(cb => { cb(<Agency>self); });
        }
        //public srid_listeners: Array<(c:Agency) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.agenId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.country_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.srid_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : Agency {
            var key = "Agency:"+x.agenId;
            var ret =  new Agency(
                x.agenId,
                x.name,
                x.country,
                x.rowVersion,
                x.srid
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): Agency {
            
            return <Agency>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : Agency {
            var self = this;
            var key="";
            var ret:Agency = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Agency:"+x.$refId;
                ret = <Agency>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "AgencyBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Agency:"+x.agenId;
                    var cachedCopy = <Agency>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Agency.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Agency.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Agency.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Agency> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<Agency> = _.map(data, (x:any):Agency => {
                return Agency.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:Agency[])=>void)  {
            var url = "/Niva/rest/Agency/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = Agency.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.agenId = this.agenId;
                ret.name = this.name;
                ret.country = this.country;
                ret.rowVersion = this.rowVersion;
                ret.srid = this.srid;
            return ret;
        }

        public updateInstance(other:Agency):void {
            var self = this;
            self.agenId = other.agenId
            self.name = other.name
            self.country = other.country
            self.rowVersion = other.rowVersion
            self.srid = other.srid
        }

        public static Create() : Agency {
            return new Agency(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(agenId:string) : Agency {
            var ret = Agency.Create();
            ret.agenId = agenId;
            return ret;
        }

        public static cashedEntities: { [id: string]: Agency; } = {};
        public static findById_unecrypted(agenId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : Agency {
            if (Entities.Agency.cashedEntities[agenId.toString()] !== undefined)
                return Entities.Agency.cashedEntities[agenId.toString()];

            var wsPath = "Agency/findByAgenId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['agenId'] = agenId;
            var ret = Agency.Create();
            Entities.Agency.cashedEntities[agenId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.Agency.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + agenId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['Agency'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.Agency.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.Agency.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}