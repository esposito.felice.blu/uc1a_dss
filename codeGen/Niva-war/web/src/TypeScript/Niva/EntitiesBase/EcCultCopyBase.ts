/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCultDetailCopy.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/EcGroupCopy.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/SuperClas.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />

module Entities {

    export class EcCultCopyBase extends NpTypes.BaseEntity {
        constructor(
            ecccId: string,
            noneMatchDecision: number,
            rowVersion: number,
            cultId: Cultivation,
            ecgcId: EcGroupCopy,
            cotyId: CoverType,
            sucaId: SuperClas) 
        {
            super();
            var self = this;
            this._ecccId = new NpTypes.UIStringModel(ecccId, this);
            this._noneMatchDecision = new NpTypes.UINumberModel(noneMatchDecision, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._cultId = new NpTypes.UIManyToOneModel<Entities.Cultivation>(cultId, this);
            this._ecgcId = new NpTypes.UIManyToOneModel<Entities.EcGroupCopy>(ecgcId, this);
            this._cotyId = new NpTypes.UIManyToOneModel<Entities.CoverType>(cotyId, this);
            this._sucaId = new NpTypes.UIManyToOneModel<Entities.SuperClas>(sucaId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.ecccId; 
        }
        
        public getKeyName(): string { 
            return "ecccId"; 
        }

        public getEntityName(): string { 
            return 'EcCultCopy'; 
        }
        
        public fromJSON(data: any): EcCultCopy[] { 
            return EcCultCopy.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.ecccId === null || this.ecccId === undefined)
                return true;
            if (this.ecccId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: EcCultCopyBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //ecccId property
        _ecccId: NpTypes.UIStringModel;
        public get ecccId():string {
            return this._ecccId.value;
        }
        public set ecccId(ecccId:string) {
            var self = this;
            self._ecccId.value = ecccId;
        //    self.ecccId_listeners.forEach(cb => { cb(<EcCultCopy>self); });
        }
        //public ecccId_listeners: Array<(c:EcCultCopy) => void> = [];
        //noneMatchDecision property
        _noneMatchDecision: NpTypes.UINumberModel;
        public get noneMatchDecision():number {
            return this._noneMatchDecision.value;
        }
        public set noneMatchDecision(noneMatchDecision:number) {
            var self = this;
            self._noneMatchDecision.value = noneMatchDecision;
        //    self.noneMatchDecision_listeners.forEach(cb => { cb(<EcCultCopy>self); });
        }
        //public noneMatchDecision_listeners: Array<(c:EcCultCopy) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<EcCultCopy>self); });
        }
        //public rowVersion_listeners: Array<(c:EcCultCopy) => void> = [];
        //cultId property
        _cultId: NpTypes.UIManyToOneModel<Entities.Cultivation>;
        public get cultId():Cultivation {
            return this._cultId.value;
        }
        public set cultId(cultId:Cultivation) {
            var self = this;
            self._cultId.value = cultId;
        //    self.cultId_listeners.forEach(cb => { cb(<EcCultCopy>self); });
        }
        //public cultId_listeners: Array<(c:EcCultCopy) => void> = [];
        //ecgcId property
        _ecgcId: NpTypes.UIManyToOneModel<Entities.EcGroupCopy>;
        public get ecgcId():EcGroupCopy {
            return this._ecgcId.value;
        }
        public set ecgcId(ecgcId:EcGroupCopy) {
            var self = this;
            self._ecgcId.value = ecgcId;
        //    self.ecgcId_listeners.forEach(cb => { cb(<EcCultCopy>self); });
        }
        //public ecgcId_listeners: Array<(c:EcCultCopy) => void> = [];
        //cotyId property
        _cotyId: NpTypes.UIManyToOneModel<Entities.CoverType>;
        public get cotyId():CoverType {
            return this._cotyId.value;
        }
        public set cotyId(cotyId:CoverType) {
            var self = this;
            self._cotyId.value = cotyId;
        //    self.cotyId_listeners.forEach(cb => { cb(<EcCultCopy>self); });
        }
        //public cotyId_listeners: Array<(c:EcCultCopy) => void> = [];
        //sucaId property
        _sucaId: NpTypes.UIManyToOneModel<Entities.SuperClas>;
        public get sucaId():SuperClas {
            return this._sucaId.value;
        }
        public set sucaId(sucaId:SuperClas) {
            var self = this;
            self._sucaId.value = sucaId;
        //    self.sucaId_listeners.forEach(cb => { cb(<EcCultCopy>self); });
        }
        //public sucaId_listeners: Array<(c:EcCultCopy) => void> = [];
        public static ecCultDetailCopyCollection: (self:Entities.EcCultCopy, func: (ecCultDetailCopyList: Entities.EcCultDetailCopy[]) => void) => void = null; //(ecCultCopy, list) => { };
        public ecCultDetailCopyCollection(func: (ecCultDetailCopyList: Entities.EcCultDetailCopy[]) => void) {
            var self: Entities.EcCultCopyBase = this;
        	if (!isVoid(EcCultCopy.ecCultDetailCopyCollection)) {
        		if (self instanceof Entities.EcCultCopy) {
        			EcCultCopy.ecCultDetailCopyCollection(<Entities.EcCultCopy>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.ecccId_listeners.splice(0);
            self.noneMatchDecision_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.cultId_listeners.splice(0);
            self.ecgcId_listeners.splice(0);
            self.cotyId_listeners.splice(0);
            self.sucaId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : EcCultCopy {
            var key = "EcCultCopy:"+x.ecccId;
            var ret =  new EcCultCopy(
                x.ecccId,
                x.noneMatchDecision,
                x.rowVersion,
                x.cultId,
                x.ecgcId,
                x.cotyId,
                x.sucaId
            );
            deserializedEntities[key] = ret;
            ret.cultId = (x.cultId !== undefined && x.cultId !== null) ? <Cultivation>NpTypes.BaseEntity.entitiesFactory[x.cultId.$entityName].fromJSONPartial(x.cultId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.ecgcId = (x.ecgcId !== undefined && x.ecgcId !== null) ? <EcGroupCopy>NpTypes.BaseEntity.entitiesFactory[x.ecgcId.$entityName].fromJSONPartial(x.ecgcId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.cotyId = (x.cotyId !== undefined && x.cotyId !== null) ? <CoverType>NpTypes.BaseEntity.entitiesFactory[x.cotyId.$entityName].fromJSONPartial(x.cotyId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.sucaId = (x.sucaId !== undefined && x.sucaId !== null) ? <SuperClas>NpTypes.BaseEntity.entitiesFactory[x.sucaId.$entityName].fromJSONPartial(x.sucaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): EcCultCopy {
            
            return <EcCultCopy>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : EcCultCopy {
            var self = this;
            var key="";
            var ret:EcCultCopy = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcCultCopy:"+x.$refId;
                ret = <EcCultCopy>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcCultCopyBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcCultCopy:"+x.ecccId;
                    var cachedCopy = <EcCultCopy>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcCultCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcCultCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = EcCultCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcCultCopy> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<EcCultCopy> = _.map(data, (x:any):EcCultCopy => {
                return EcCultCopy.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:EcCultCopy[])=>void)  {
            var url = "/Niva/rest/EcCultCopy/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = EcCultCopy.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.ecccId = this.ecccId;
                ret.noneMatchDecision = this.noneMatchDecision;
                ret.rowVersion = this.rowVersion;
                ret.cultId = 
                    (this.cultId !== undefined && this.cultId !== null) ? 
                        { cultId :  this.cultId.cultId} :
                        (this.cultId === undefined ? undefined : null);
                ret.ecgcId = 
                    (this.ecgcId !== undefined && this.ecgcId !== null) ? 
                        { ecgcId :  this.ecgcId.ecgcId} :
                        (this.ecgcId === undefined ? undefined : null);
                ret.cotyId = 
                    (this.cotyId !== undefined && this.cotyId !== null) ? 
                        { cotyId :  this.cotyId.cotyId} :
                        (this.cotyId === undefined ? undefined : null);
                ret.sucaId = 
                    (this.sucaId !== undefined && this.sucaId !== null) ? 
                        { sucaId :  this.sucaId.sucaId} :
                        (this.sucaId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:EcCultCopy):void {
            var self = this;
            self.ecccId = other.ecccId
            self.noneMatchDecision = other.noneMatchDecision
            self.rowVersion = other.rowVersion
            self.cultId = other.cultId
            self.ecgcId = other.ecgcId
            self.cotyId = other.cotyId
            self.sucaId = other.sucaId
        }

        public static Create() : EcCultCopy {
            return new EcCultCopy(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(ecccId:string) : EcCultCopy {
            var ret = EcCultCopy.Create();
            ret.ecccId = ecccId;
            return ret;
        }

        public static cashedEntities: { [id: string]: EcCultCopy; } = {};
        public static findById_unecrypted(ecccId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : EcCultCopy {
            if (Entities.EcCultCopy.cashedEntities[ecccId.toString()] !== undefined)
                return Entities.EcCultCopy.cashedEntities[ecccId.toString()];

            var wsPath = "EcCultCopy/findByEcccId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['ecccId'] = ecccId;
            var ret = EcCultCopy.Create();
            Entities.EcCultCopy.cashedEntities[ecccId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.EcCultCopy.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + ecccId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['EcCultCopy'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.EcCultCopy.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.EcCultCopy.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}