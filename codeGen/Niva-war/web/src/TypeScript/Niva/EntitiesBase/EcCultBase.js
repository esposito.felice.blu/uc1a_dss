/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCultDetail.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/EcGroup.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/SuperClas.ts" />
/// <reference path="../Entities/EcCult.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcCultBase = (function (_super) {
        __extends(EcCultBase, _super);
        function EcCultBase(eccuId, noneMatchDecision, rowVersion, cultId, ecgrId, cotyId, sucaId) {
            _super.call(this);
            var self = this;
            this._eccuId = new NpTypes.UIStringModel(eccuId, this);
            this._noneMatchDecision = new NpTypes.UINumberModel(noneMatchDecision, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._cultId = new NpTypes.UIManyToOneModel(cultId, this);
            this._ecgrId = new NpTypes.UIManyToOneModel(ecgrId, this);
            this._cotyId = new NpTypes.UIManyToOneModel(cotyId, this);
            this._sucaId = new NpTypes.UIManyToOneModel(sucaId, this);
            self.postConstruct();
        }
        EcCultBase.prototype.getKey = function () {
            return this.eccuId;
        };
        EcCultBase.prototype.getKeyName = function () {
            return "eccuId";
        };
        EcCultBase.prototype.getEntityName = function () {
            return 'EcCult';
        };
        EcCultBase.prototype.fromJSON = function (data) {
            return Entities.EcCult.fromJSONComplete(data);
        };
        Object.defineProperty(EcCultBase.prototype, "eccuId", {
            get: function () {
                return this._eccuId.value;
            },
            set: function (eccuId) {
                var self = this;
                self._eccuId.value = eccuId;
                //    self.eccuId_listeners.forEach(cb => { cb(<EcCult>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultBase.prototype, "noneMatchDecision", {
            get: function () {
                return this._noneMatchDecision.value;
            },
            set: function (noneMatchDecision) {
                var self = this;
                self._noneMatchDecision.value = noneMatchDecision;
                //    self.noneMatchDecision_listeners.forEach(cb => { cb(<EcCult>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<EcCult>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultBase.prototype, "cultId", {
            get: function () {
                return this._cultId.value;
            },
            set: function (cultId) {
                var self = this;
                self._cultId.value = cultId;
                //    self.cultId_listeners.forEach(cb => { cb(<EcCult>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultBase.prototype, "ecgrId", {
            get: function () {
                return this._ecgrId.value;
            },
            set: function (ecgrId) {
                var self = this;
                self._ecgrId.value = ecgrId;
                //    self.ecgrId_listeners.forEach(cb => { cb(<EcCult>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultBase.prototype, "cotyId", {
            get: function () {
                return this._cotyId.value;
            },
            set: function (cotyId) {
                var self = this;
                self._cotyId.value = cotyId;
                //    self.cotyId_listeners.forEach(cb => { cb(<EcCult>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultBase.prototype, "sucaId", {
            get: function () {
                return this._sucaId.value;
            },
            set: function (sucaId) {
                var self = this;
                self._sucaId.value = sucaId;
                //    self.sucaId_listeners.forEach(cb => { cb(<EcCult>self); });
            },
            enumerable: true,
            configurable: true
        });
        EcCultBase.prototype.ecCultDetailCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.EcCult.ecCultDetailCollection)) {
                if (self instanceof Entities.EcCult) {
                    Entities.EcCult.ecCultDetailCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.eccuId_listeners.splice(0);
            self.noneMatchDecision_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.cultId_listeners.splice(0);
            self.ecgrId_listeners.splice(0);
            self.cotyId_listeners.splice(0);
            self.sucaId_listeners.splice(0);
        }
        */
        EcCultBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "EcCult:" + x.eccuId;
            var ret = new Entities.EcCult(x.eccuId, x.noneMatchDecision, x.rowVersion, x.cultId, x.ecgrId, x.cotyId, x.sucaId);
            deserializedEntities[key] = ret;
            ret.cultId = (x.cultId !== undefined && x.cultId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cultId.$entityName].fromJSONPartial(x.cultId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.ecgrId = (x.ecgrId !== undefined && x.ecgrId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.ecgrId.$entityName].fromJSONPartial(x.ecgrId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.cotyId = (x.cotyId !== undefined && x.cotyId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cotyId.$entityName].fromJSONPartial(x.cotyId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.sucaId = (x.sucaId !== undefined && x.sucaId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.sucaId.$entityName].fromJSONPartial(x.sucaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        EcCultBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        EcCultBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcCult:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcCultBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcCult:"+x.eccuId;
                    var cachedCopy = <EcCult>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcCult.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcCult.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.EcCult.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        EcCultBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.EcCult.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        EcCultBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/EcCult/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.EcCult.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        EcCultBase.prototype.toJSON = function () {
            var ret = {};
            ret.eccuId = this.eccuId;
            ret.noneMatchDecision = this.noneMatchDecision;
            ret.rowVersion = this.rowVersion;
            ret.cultId =
                (this.cultId !== undefined && this.cultId !== null) ?
                    { cultId: this.cultId.cultId } :
                    (this.cultId === undefined ? undefined : null);
            ret.ecgrId =
                (this.ecgrId !== undefined && this.ecgrId !== null) ?
                    { ecgrId: this.ecgrId.ecgrId } :
                    (this.ecgrId === undefined ? undefined : null);
            ret.cotyId =
                (this.cotyId !== undefined && this.cotyId !== null) ?
                    { cotyId: this.cotyId.cotyId } :
                    (this.cotyId === undefined ? undefined : null);
            ret.sucaId =
                (this.sucaId !== undefined && this.sucaId !== null) ?
                    { sucaId: this.sucaId.sucaId } :
                    (this.sucaId === undefined ? undefined : null);
            return ret;
        };
        EcCultBase.prototype.updateInstance = function (other) {
            var self = this;
            self.eccuId = other.eccuId;
            self.noneMatchDecision = other.noneMatchDecision;
            self.rowVersion = other.rowVersion;
            self.cultId = other.cultId;
            self.ecgrId = other.ecgrId;
            self.cotyId = other.cotyId;
            self.sucaId = other.sucaId;
        };
        EcCultBase.Create = function () {
            return new Entities.EcCult(undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        EcCultBase.CreateById = function (eccuId) {
            var ret = Entities.EcCult.Create();
            ret.eccuId = eccuId;
            return ret;
        };
        EcCultBase.findById_unecrypted = function (eccuId, $scope, $http, errFunc) {
            if (Entities.EcCult.cashedEntities[eccuId.toString()] !== undefined)
                return Entities.EcCult.cashedEntities[eccuId.toString()];
            var wsPath = "EcCult/findByEccuId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['eccuId'] = eccuId;
            var ret = Entities.EcCult.Create();
            Entities.EcCult.cashedEntities[eccuId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.EcCult.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + eccuId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        EcCultBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public sucaId_listeners: Array<(c:EcCult) => void> = [];
        EcCultBase.ecCultDetailCollection = null; //(ecCult, list) => { };
        EcCultBase.cashedEntities = {};
        return EcCultBase;
    })(NpTypes.BaseEntity);
    Entities.EcCultBase = EcCultBase;
    NpTypes.BaseEntity.entitiesFactory['EcCult'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.EcCult.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.EcCult.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=EcCultBase.js.map