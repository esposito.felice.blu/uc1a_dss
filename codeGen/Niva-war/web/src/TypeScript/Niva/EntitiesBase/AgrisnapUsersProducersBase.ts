/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/AgrisnapUsers.ts" />
/// <reference path="../Entities/Producers.ts" />
/// <reference path="../Entities/AgrisnapUsersProducers.ts" />

module Entities {

    export class AgrisnapUsersProducersBase extends NpTypes.BaseEntity {
        constructor(
            dteRelCreated: Date,
            dteRelCanceled: Date,
            agrisnapUsersProducersId: string,
            rowVersion: number,
            agrisnapUsersId: AgrisnapUsers,
            producersId: Producers) 
        {
            super();
            var self = this;
            this._dteRelCreated = new NpTypes.UIDateModel(dteRelCreated, this);
            this._dteRelCanceled = new NpTypes.UIDateModel(dteRelCanceled, this);
            this._agrisnapUsersProducersId = new NpTypes.UIStringModel(agrisnapUsersProducersId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._agrisnapUsersId = new NpTypes.UIManyToOneModel<Entities.AgrisnapUsers>(agrisnapUsersId, this);
            this._producersId = new NpTypes.UIManyToOneModel<Entities.Producers>(producersId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.agrisnapUsersProducersId; 
        }
        
        public getKeyName(): string { 
            return "agrisnapUsersProducersId"; 
        }

        public getEntityName(): string { 
            return 'AgrisnapUsersProducers'; 
        }
        
        public fromJSON(data: any): AgrisnapUsersProducers[] { 
            return AgrisnapUsersProducers.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.agrisnapUsersProducersId === null || this.agrisnapUsersProducersId === undefined)
                return true;
            if (this.agrisnapUsersProducersId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: AgrisnapUsersProducersBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //dteRelCreated property
        _dteRelCreated: NpTypes.UIDateModel;
        public get dteRelCreated():Date {
            return this._dteRelCreated.value;
        }
        public set dteRelCreated(dteRelCreated:Date) {
            var self = this;
            self._dteRelCreated.value = dteRelCreated;
        //    self.dteRelCreated_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
        }
        //public dteRelCreated_listeners: Array<(c:AgrisnapUsersProducers) => void> = [];
        //dteRelCanceled property
        _dteRelCanceled: NpTypes.UIDateModel;
        public get dteRelCanceled():Date {
            return this._dteRelCanceled.value;
        }
        public set dteRelCanceled(dteRelCanceled:Date) {
            var self = this;
            self._dteRelCanceled.value = dteRelCanceled;
        //    self.dteRelCanceled_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
        }
        //public dteRelCanceled_listeners: Array<(c:AgrisnapUsersProducers) => void> = [];
        //agrisnapUsersProducersId property
        _agrisnapUsersProducersId: NpTypes.UIStringModel;
        public get agrisnapUsersProducersId():string {
            return this._agrisnapUsersProducersId.value;
        }
        public set agrisnapUsersProducersId(agrisnapUsersProducersId:string) {
            var self = this;
            self._agrisnapUsersProducersId.value = agrisnapUsersProducersId;
        //    self.agrisnapUsersProducersId_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
        }
        //public agrisnapUsersProducersId_listeners: Array<(c:AgrisnapUsersProducers) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
        }
        //public rowVersion_listeners: Array<(c:AgrisnapUsersProducers) => void> = [];
        //agrisnapUsersId property
        _agrisnapUsersId: NpTypes.UIManyToOneModel<Entities.AgrisnapUsers>;
        public get agrisnapUsersId():AgrisnapUsers {
            return this._agrisnapUsersId.value;
        }
        public set agrisnapUsersId(agrisnapUsersId:AgrisnapUsers) {
            var self = this;
            self._agrisnapUsersId.value = agrisnapUsersId;
        //    self.agrisnapUsersId_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
        }
        //public agrisnapUsersId_listeners: Array<(c:AgrisnapUsersProducers) => void> = [];
        //producersId property
        _producersId: NpTypes.UIManyToOneModel<Entities.Producers>;
        public get producersId():Producers {
            return this._producersId.value;
        }
        public set producersId(producersId:Producers) {
            var self = this;
            self._producersId.value = producersId;
        //    self.producersId_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
        }
        //public producersId_listeners: Array<(c:AgrisnapUsersProducers) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.dteRelCreated_listeners.splice(0);
            self.dteRelCanceled_listeners.splice(0);
            self.agrisnapUsersProducersId_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.agrisnapUsersId_listeners.splice(0);
            self.producersId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : AgrisnapUsersProducers {
            var key = "AgrisnapUsersProducers:"+x.agrisnapUsersProducersId;
            var ret =  new AgrisnapUsersProducers(
                isVoid(x.dteRelCreated) ? null : new Date(x.dteRelCreated),
                isVoid(x.dteRelCanceled) ? null : new Date(x.dteRelCanceled),
                x.agrisnapUsersProducersId,
                x.rowVersion,
                x.agrisnapUsersId,
                x.producersId
            );
            deserializedEntities[key] = ret;
            ret.agrisnapUsersId = (x.agrisnapUsersId !== undefined && x.agrisnapUsersId !== null) ? <AgrisnapUsers>NpTypes.BaseEntity.entitiesFactory[x.agrisnapUsersId.$entityName].fromJSONPartial(x.agrisnapUsersId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.producersId = (x.producersId !== undefined && x.producersId !== null) ? <Producers>NpTypes.BaseEntity.entitiesFactory[x.producersId.$entityName].fromJSONPartial(x.producersId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): AgrisnapUsersProducers {
            
            return <AgrisnapUsersProducers>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : AgrisnapUsersProducers {
            var self = this;
            var key="";
            var ret:AgrisnapUsersProducers = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "AgrisnapUsersProducers:"+x.$refId;
                ret = <AgrisnapUsersProducers>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "AgrisnapUsersProducersBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "AgrisnapUsersProducers:"+x.agrisnapUsersProducersId;
                    var cachedCopy = <AgrisnapUsersProducers>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = AgrisnapUsersProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = AgrisnapUsersProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = AgrisnapUsersProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<AgrisnapUsersProducers> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<AgrisnapUsersProducers> = _.map(data, (x:any):AgrisnapUsersProducers => {
                return AgrisnapUsersProducers.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:AgrisnapUsersProducers[])=>void)  {
            var url = "/Niva/rest/AgrisnapUsersProducers/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = AgrisnapUsersProducers.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.dteRelCreated = this.dteRelCreated;
                ret.dteRelCanceled = this.dteRelCanceled;
                ret.agrisnapUsersProducersId = this.agrisnapUsersProducersId;
                ret.rowVersion = this.rowVersion;
                ret.agrisnapUsersId = 
                    (this.agrisnapUsersId !== undefined && this.agrisnapUsersId !== null) ? 
                        { agrisnapUsersId :  this.agrisnapUsersId.agrisnapUsersId} :
                        (this.agrisnapUsersId === undefined ? undefined : null);
                ret.producersId = 
                    (this.producersId !== undefined && this.producersId !== null) ? 
                        { producersId :  this.producersId.producersId} :
                        (this.producersId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:AgrisnapUsersProducers):void {
            var self = this;
            self.dteRelCreated = other.dteRelCreated
            self.dteRelCanceled = other.dteRelCanceled
            self.agrisnapUsersProducersId = other.agrisnapUsersProducersId
            self.rowVersion = other.rowVersion
            self.agrisnapUsersId = other.agrisnapUsersId
            self.producersId = other.producersId
        }

        public static Create() : AgrisnapUsersProducers {
            return new AgrisnapUsersProducers(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(agrisnapUsersProducersId:string) : AgrisnapUsersProducers {
            var ret = AgrisnapUsersProducers.Create();
            ret.agrisnapUsersProducersId = agrisnapUsersProducersId;
            return ret;
        }

        public static cashedEntities: { [id: string]: AgrisnapUsersProducers; } = {};
        public static findById_unecrypted(agrisnapUsersProducersId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : AgrisnapUsersProducers {
            if (Entities.AgrisnapUsersProducers.cashedEntities[agrisnapUsersProducersId.toString()] !== undefined)
                return Entities.AgrisnapUsersProducers.cashedEntities[agrisnapUsersProducersId.toString()];

            var wsPath = "AgrisnapUsersProducers/findByAgrisnapUsersProducersId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['agrisnapUsersProducersId'] = agrisnapUsersProducersId;
            var ret = AgrisnapUsersProducers.Create();
            Entities.AgrisnapUsersProducers.cashedEntities[agrisnapUsersProducersId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.AgrisnapUsersProducers.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + agrisnapUsersProducersId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['AgrisnapUsersProducers'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.AgrisnapUsersProducers.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.AgrisnapUsersProducers.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}