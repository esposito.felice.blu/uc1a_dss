/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/FmisUpload.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FmisUploadBase = (function (_super) {
        __extends(FmisUploadBase, _super);
        function FmisUploadBase(fmisUploadsId, metadata, dteUpload, docfile, usrUpload, parcelsIssuesId) {
            _super.call(this);
            var self = this;
            this._fmisUploadsId = new NpTypes.UIStringModel(fmisUploadsId, this);
            this._metadata = new NpTypes.UIStringModel(metadata, this);
            this._dteUpload = new NpTypes.UIDateModel(dteUpload, this);
            this._docfile = new NpTypes.UIBlobModel(docfile, function () { return 'FMIS Document'; }, function (fileName) { }, this);
            this._usrUpload = new NpTypes.UIStringModel(usrUpload, this);
            this._parcelsIssuesId = new NpTypes.UIManyToOneModel(parcelsIssuesId, this);
            self.postConstruct();
        }
        FmisUploadBase.prototype.getKey = function () {
            return this.fmisUploadsId;
        };
        FmisUploadBase.prototype.getKeyName = function () {
            return "fmisUploadsId";
        };
        FmisUploadBase.prototype.getEntityName = function () {
            return 'FmisUpload';
        };
        FmisUploadBase.prototype.fromJSON = function (data) {
            return Entities.FmisUpload.fromJSONComplete(data);
        };
        Object.defineProperty(FmisUploadBase.prototype, "fmisUploadsId", {
            get: function () {
                return this._fmisUploadsId.value;
            },
            set: function (fmisUploadsId) {
                var self = this;
                self._fmisUploadsId.value = fmisUploadsId;
                //    self.fmisUploadsId_listeners.forEach(cb => { cb(<FmisUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisUploadBase.prototype, "metadata", {
            get: function () {
                return this._metadata.value;
            },
            set: function (metadata) {
                var self = this;
                self._metadata.value = metadata;
                //    self.metadata_listeners.forEach(cb => { cb(<FmisUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisUploadBase.prototype, "dteUpload", {
            get: function () {
                return this._dteUpload.value;
            },
            set: function (dteUpload) {
                var self = this;
                self._dteUpload.value = dteUpload;
                //    self.dteUpload_listeners.forEach(cb => { cb(<FmisUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisUploadBase.prototype, "docfile", {
            get: function () {
                return this._docfile.value;
            },
            set: function (docfile) {
                var self = this;
                self._docfile.value = docfile;
                //    self.docfile_listeners.forEach(cb => { cb(<FmisUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisUploadBase.prototype, "usrUpload", {
            get: function () {
                return this._usrUpload.value;
            },
            set: function (usrUpload) {
                var self = this;
                self._usrUpload.value = usrUpload;
                //    self.usrUpload_listeners.forEach(cb => { cb(<FmisUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisUploadBase.prototype, "parcelsIssuesId", {
            get: function () {
                return this._parcelsIssuesId.value;
            },
            set: function (parcelsIssuesId) {
                var self = this;
                self._parcelsIssuesId.value = parcelsIssuesId;
                //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<FmisUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public parcelsIssuesId_listeners: Array<(c:FmisUpload) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.fmisUploadsId_listeners.splice(0);
            self.metadata_listeners.splice(0);
            self.dteUpload_listeners.splice(0);
            self.docfile_listeners.splice(0);
            self.usrUpload_listeners.splice(0);
            self.parcelsIssuesId_listeners.splice(0);
        }
        */
        FmisUploadBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "FmisUpload:" + x.fmisUploadsId;
            var ret = new Entities.FmisUpload(x.fmisUploadsId, x.metadata, isVoid(x.dteUpload) ? null : new Date(x.dteUpload), x.docfile, x.usrUpload, x.parcelsIssuesId);
            deserializedEntities[key] = ret;
            ret.parcelsIssuesId = (x.parcelsIssuesId !== undefined && x.parcelsIssuesId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.parcelsIssuesId.$entityName].fromJSONPartial(x.parcelsIssuesId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        FmisUploadBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        FmisUploadBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "FmisUpload:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "FmisUploadBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "FmisUpload:"+x.fmisUploadsId;
                    var cachedCopy = <FmisUpload>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = FmisUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = FmisUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.FmisUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        FmisUploadBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.FmisUpload.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        FmisUploadBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/FmisUpload/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.FmisUpload.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        FmisUploadBase.prototype.toJSON = function () {
            var ret = {};
            ret.fmisUploadsId = this.fmisUploadsId;
            ret.metadata = this.metadata;
            ret.dteUpload = this.dteUpload;
            ret.docfile = this.docfile;
            ret.usrUpload = this.usrUpload;
            ret.parcelsIssuesId =
                (this.parcelsIssuesId !== undefined && this.parcelsIssuesId !== null) ?
                    { parcelsIssuesId: this.parcelsIssuesId.parcelsIssuesId } :
                    (this.parcelsIssuesId === undefined ? undefined : null);
            return ret;
        };
        FmisUploadBase.prototype.updateInstance = function (other) {
            var self = this;
            self.fmisUploadsId = other.fmisUploadsId;
            self.metadata = other.metadata;
            self.dteUpload = other.dteUpload;
            self.docfile = other.docfile;
            self.usrUpload = other.usrUpload;
            self.parcelsIssuesId = other.parcelsIssuesId;
        };
        FmisUploadBase.Create = function () {
            return new Entities.FmisUpload(undefined, undefined, undefined, undefined, undefined, undefined);
        };
        FmisUploadBase.CreateById = function (fmisUploadsId) {
            var ret = Entities.FmisUpload.Create();
            ret.fmisUploadsId = fmisUploadsId;
            return ret;
        };
        FmisUploadBase.findById_unecrypted = function (fmisUploadsId, $scope, $http, errFunc) {
            if (Entities.FmisUpload.cashedEntities[fmisUploadsId.toString()] !== undefined)
                return Entities.FmisUpload.cashedEntities[fmisUploadsId.toString()];
            var wsPath = "FmisUpload/findByFmisUploadsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fmisUploadsId'] = fmisUploadsId;
            var ret = Entities.FmisUpload.Create();
            Entities.FmisUpload.cashedEntities[fmisUploadsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.FmisUpload.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + fmisUploadsId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        FmisUploadBase.prototype.getRowVersion = function () {
            return 0;
        };
        FmisUploadBase.cashedEntities = {};
        return FmisUploadBase;
    })(NpTypes.BaseEntity);
    Entities.FmisUploadBase = FmisUploadBase;
    NpTypes.BaseEntity.entitiesFactory['FmisUpload'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.FmisUpload.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.FmisUpload.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=FmisUploadBase.js.map