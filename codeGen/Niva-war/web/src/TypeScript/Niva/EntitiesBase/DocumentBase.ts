/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Document.ts" />

module Entities {

    export class DocumentBase extends NpTypes.BaseEntity {
        constructor(
            docuId: string,
            attachedFile: string,
            filePath: string,
            description: string,
            rowVersion: number) 
        {
            super();
            var self = this;
            this._docuId = new NpTypes.UIStringModel(docuId, this);
            this._attachedFile = new NpTypes.UIBlobModel(
                attachedFile, 
                ()  => self.filePath,
                (fileName:string)   => {self.filePath = fileName},
                this);
            this._filePath = new NpTypes.UIStringModel(filePath, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.docuId; 
        }
        
        public getKeyName(): string { 
            return "docuId"; 
        }

        public getEntityName(): string { 
            return 'Document'; 
        }
        
        public fromJSON(data: any): Document[] { 
            return Document.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.docuId === null || this.docuId === undefined)
                return true;
            if (this.docuId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: DocumentBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //docuId property
        _docuId: NpTypes.UIStringModel;
        public get docuId():string {
            return this._docuId.value;
        }
        public set docuId(docuId:string) {
            var self = this;
            self._docuId.value = docuId;
        //    self.docuId_listeners.forEach(cb => { cb(<Document>self); });
        }
        //public docuId_listeners: Array<(c:Document) => void> = [];
        //attachedFile property
        _attachedFile: NpTypes.UIBlobModel;
        public get attachedFile():string {
            return this._attachedFile.value;
        }
        public set attachedFile(attachedFile:string) {
            var self = this;
            self._attachedFile.value = attachedFile;
        //    self.attachedFile_listeners.forEach(cb => { cb(<Document>self); });
        }
        //public attachedFile_listeners: Array<(c:Document) => void> = [];
        //filePath property
        _filePath: NpTypes.UIStringModel;
        public get filePath():string {
            return this._filePath.value;
        }
        public set filePath(filePath:string) {
            var self = this;
            self._filePath.value = filePath;
        //    self.filePath_listeners.forEach(cb => { cb(<Document>self); });
        }
        //public filePath_listeners: Array<(c:Document) => void> = [];
        //description property
        _description: NpTypes.UIStringModel;
        public get description():string {
            return this._description.value;
        }
        public set description(description:string) {
            var self = this;
            self._description.value = description;
        //    self.description_listeners.forEach(cb => { cb(<Document>self); });
        }
        //public description_listeners: Array<(c:Document) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<Document>self); });
        }
        //public rowVersion_listeners: Array<(c:Document) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.docuId_listeners.splice(0);
            self.attachedFile_listeners.splice(0);
            self.filePath_listeners.splice(0);
            self.description_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : Document {
            var key = "Document:"+x.docuId;
            var ret =  new Document(
                x.docuId,
                x.attachedFile,
                x.filePath,
                x.description,
                x.rowVersion
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): Document {
            
            return <Document>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : Document {
            var self = this;
            var key="";
            var ret:Document = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Document:"+x.$refId;
                ret = <Document>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "DocumentBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Document:"+x.docuId;
                    var cachedCopy = <Document>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Document.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Document.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Document.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Document> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<Document> = _.map(data, (x:any):Document => {
                return Document.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:Document[])=>void)  {
            var url = "/Niva/rest/Document/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = Document.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.docuId = this.docuId;
                ret.attachedFile = this.attachedFile;
                ret.filePath = this.filePath;
                ret.description = this.description;
                ret.rowVersion = this.rowVersion;
            return ret;
        }

        public updateInstance(other:Document):void {
            var self = this;
            self.docuId = other.docuId
            self.attachedFile = other.attachedFile
            self.filePath = other.filePath
            self.description = other.description
            self.rowVersion = other.rowVersion
        }

        public static Create() : Document {
            return new Document(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(docuId:string) : Document {
            var ret = Document.Create();
            ret.docuId = docuId;
            return ret;
        }

        public static cashedEntities: { [id: string]: Document; } = {};
        public static findById_unecrypted(docuId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : Document {
            if (Entities.Document.cashedEntities[docuId.toString()] !== undefined)
                return Entities.Document.cashedEntities[docuId.toString()];

            var wsPath = "Document/findByDocuId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['docuId'] = docuId;
            var ret = Document.Create();
            Entities.Document.cashedEntities[docuId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.Document.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + docuId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['Document'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.Document.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.Document.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}