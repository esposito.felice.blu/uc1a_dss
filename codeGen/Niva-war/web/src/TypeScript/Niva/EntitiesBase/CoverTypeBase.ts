/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/EcCult.ts" />
/// <reference path="../Entities/FmisDecision.ts" />
/// <reference path="../Entities/GpDecision.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ExcelFile.ts" />
/// <reference path="../Entities/CoverType.ts" />

module Entities {

    export class CoverTypeBase extends NpTypes.BaseEntity {
        constructor(
            cotyId: string,
            code: number,
            name: string,
            rowVersion: number,
            exfiId: ExcelFile) 
        {
            super();
            var self = this;
            this._cotyId = new NpTypes.UIStringModel(cotyId, this);
            this._code = new NpTypes.UINumberModel(code, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._exfiId = new NpTypes.UIManyToOneModel<Entities.ExcelFile>(exfiId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.cotyId; 
        }
        
        public getKeyName(): string { 
            return "cotyId"; 
        }

        public getEntityName(): string { 
            return 'CoverType'; 
        }
        
        public fromJSON(data: any): CoverType[] { 
            return CoverType.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.cotyId === null || this.cotyId === undefined)
                return true;
            if (this.cotyId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: CoverTypeBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //cotyId property
        _cotyId: NpTypes.UIStringModel;
        public get cotyId():string {
            return this._cotyId.value;
        }
        public set cotyId(cotyId:string) {
            var self = this;
            self._cotyId.value = cotyId;
        //    self.cotyId_listeners.forEach(cb => { cb(<CoverType>self); });
        }
        //public cotyId_listeners: Array<(c:CoverType) => void> = [];
        //code property
        _code: NpTypes.UINumberModel;
        public get code():number {
            return this._code.value;
        }
        public set code(code:number) {
            var self = this;
            self._code.value = code;
        //    self.code_listeners.forEach(cb => { cb(<CoverType>self); });
        }
        //public code_listeners: Array<(c:CoverType) => void> = [];
        //name property
        _name: NpTypes.UIStringModel;
        public get name():string {
            return this._name.value;
        }
        public set name(name:string) {
            var self = this;
            self._name.value = name;
        //    self.name_listeners.forEach(cb => { cb(<CoverType>self); });
        }
        //public name_listeners: Array<(c:CoverType) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<CoverType>self); });
        }
        //public rowVersion_listeners: Array<(c:CoverType) => void> = [];
        //exfiId property
        _exfiId: NpTypes.UIManyToOneModel<Entities.ExcelFile>;
        public get exfiId():ExcelFile {
            return this._exfiId.value;
        }
        public set exfiId(exfiId:ExcelFile) {
            var self = this;
            self._exfiId.value = exfiId;
        //    self.exfiId_listeners.forEach(cb => { cb(<CoverType>self); });
        }
        //public exfiId_listeners: Array<(c:CoverType) => void> = [];
        public static cultivationCollection: (self:Entities.CoverType, func: (cultivationList: Entities.Cultivation[]) => void) => void = null; //(coverType, list) => { };
        public cultivationCollection(func: (cultivationList: Entities.Cultivation[]) => void) {
            var self: Entities.CoverTypeBase = this;
        	if (!isVoid(CoverType.cultivationCollection)) {
        		if (self instanceof Entities.CoverType) {
        			CoverType.cultivationCollection(<Entities.CoverType>self, func);
                }
            }
        }
        public static ecCultCollection: (self:Entities.CoverType, func: (ecCultList: Entities.EcCult[]) => void) => void = null; //(coverType, list) => { };
        public ecCultCollection(func: (ecCultList: Entities.EcCult[]) => void) {
            var self: Entities.CoverTypeBase = this;
        	if (!isVoid(CoverType.ecCultCollection)) {
        		if (self instanceof Entities.CoverType) {
        			CoverType.ecCultCollection(<Entities.CoverType>self, func);
                }
            }
        }
        public static fmisDecisionCollection: (self:Entities.CoverType, func: (fmisDecisionList: Entities.FmisDecision[]) => void) => void = null; //(coverType, list) => { };
        public fmisDecisionCollection(func: (fmisDecisionList: Entities.FmisDecision[]) => void) {
            var self: Entities.CoverTypeBase = this;
        	if (!isVoid(CoverType.fmisDecisionCollection)) {
        		if (self instanceof Entities.CoverType) {
        			CoverType.fmisDecisionCollection(<Entities.CoverType>self, func);
                }
            }
        }
        public static gpDecisionCollection: (self:Entities.CoverType, func: (gpDecisionList: Entities.GpDecision[]) => void) => void = null; //(coverType, list) => { };
        public gpDecisionCollection(func: (gpDecisionList: Entities.GpDecision[]) => void) {
            var self: Entities.CoverTypeBase = this;
        	if (!isVoid(CoverType.gpDecisionCollection)) {
        		if (self instanceof Entities.CoverType) {
        			CoverType.gpDecisionCollection(<Entities.CoverType>self, func);
                }
            }
        }
        public static ecCultCopyCollection: (self:Entities.CoverType, func: (ecCultCopyList: Entities.EcCultCopy[]) => void) => void = null; //(coverType, list) => { };
        public ecCultCopyCollection(func: (ecCultCopyList: Entities.EcCultCopy[]) => void) {
            var self: Entities.CoverTypeBase = this;
        	if (!isVoid(CoverType.ecCultCopyCollection)) {
        		if (self instanceof Entities.CoverType) {
        			CoverType.ecCultCopyCollection(<Entities.CoverType>self, func);
                }
            }
        }
        public static parcelClascoty_id_declCollection: (self:Entities.CoverType, func: (parcelClasList: Entities.ParcelClas[]) => void) => void = null; //(coverType, list) => { };
        public parcelClascoty_id_declCollection(func: (parcelClasList: Entities.ParcelClas[]) => void) {
            var self: Entities.CoverTypeBase = this;
        	if (!isVoid(CoverType.parcelClascoty_id_declCollection)) {
        		if (self instanceof Entities.CoverType) {
        			CoverType.parcelClascoty_id_declCollection(<Entities.CoverType>self, func);
                }
            }
        }
        public static parcelClascoty_id_predCollection: (self:Entities.CoverType, func: (parcelClasList: Entities.ParcelClas[]) => void) => void = null; //(coverType, list) => { };
        public parcelClascoty_id_predCollection(func: (parcelClasList: Entities.ParcelClas[]) => void) {
            var self: Entities.CoverTypeBase = this;
        	if (!isVoid(CoverType.parcelClascoty_id_predCollection)) {
        		if (self instanceof Entities.CoverType) {
        			CoverType.parcelClascoty_id_predCollection(<Entities.CoverType>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.cotyId_listeners.splice(0);
            self.code_listeners.splice(0);
            self.name_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.exfiId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : CoverType {
            var key = "CoverType:"+x.cotyId;
            var ret =  new CoverType(
                x.cotyId,
                x.code,
                x.name,
                x.rowVersion,
                x.exfiId
            );
            deserializedEntities[key] = ret;
            ret.exfiId = (x.exfiId !== undefined && x.exfiId !== null) ? <ExcelFile>NpTypes.BaseEntity.entitiesFactory[x.exfiId.$entityName].fromJSONPartial(x.exfiId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): CoverType {
            
            return <CoverType>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : CoverType {
            var self = this;
            var key="";
            var ret:CoverType = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "CoverType:"+x.$refId;
                ret = <CoverType>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "CoverTypeBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "CoverType:"+x.cotyId;
                    var cachedCopy = <CoverType>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = CoverType.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = CoverType.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = CoverType.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<CoverType> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<CoverType> = _.map(data, (x:any):CoverType => {
                return CoverType.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:CoverType[])=>void)  {
            var url = "/Niva/rest/CoverType/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = CoverType.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.cotyId = this.cotyId;
                ret.code = this.code;
                ret.name = this.name;
                ret.rowVersion = this.rowVersion;
                ret.exfiId = 
                    (this.exfiId !== undefined && this.exfiId !== null) ? 
                        { id :  this.exfiId.id} :
                        (this.exfiId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:CoverType):void {
            var self = this;
            self.cotyId = other.cotyId
            self.code = other.code
            self.name = other.name
            self.rowVersion = other.rowVersion
            self.exfiId = other.exfiId
        }

        public static Create() : CoverType {
            return new CoverType(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(cotyId:string) : CoverType {
            var ret = CoverType.Create();
            ret.cotyId = cotyId;
            return ret;
        }

        public static cashedEntities: { [id: string]: CoverType; } = {};
        public static findById_unecrypted(cotyId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : CoverType {
            if (Entities.CoverType.cashedEntities[cotyId.toString()] !== undefined)
                return Entities.CoverType.cashedEntities[cotyId.toString()];

            var wsPath = "CoverType/findByCotyId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['cotyId'] = cotyId;
            var ret = CoverType.Create();
            Entities.CoverType.cashedEntities[cotyId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.CoverType.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + cotyId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['CoverType'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.CoverType.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.CoverType.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}