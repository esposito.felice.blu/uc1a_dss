/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/TmpBlob.ts" />

module Entities {

    export class TmpBlobBase extends NpTypes.BaseEntity {
        constructor(
            id: string,
            data: string,
            rowVersion: number) 
        {
            super();
            var self = this;
            this._id = new NpTypes.UIStringModel(id, this);
            this._data = new NpTypes.UIBlobModel(
                data, 
                ()  => '...',
                (fileName:string)   => {},
                this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.id; 
        }
        
        public getKeyName(): string { 
            return "id"; 
        }

        public getEntityName(): string { 
            return 'TmpBlob'; 
        }
        
        public fromJSON(data: any): TmpBlob[] { 
            return TmpBlob.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.id === null || this.id === undefined)
                return true;
            if (this.id.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: TmpBlobBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //id property
        _id: NpTypes.UIStringModel;
        public get id():string {
            return this._id.value;
        }
        public set id(id:string) {
            var self = this;
            self._id.value = id;
        //    self.id_listeners.forEach(cb => { cb(<TmpBlob>self); });
        }
        //public id_listeners: Array<(c:TmpBlob) => void> = [];
        //data property
        _data: NpTypes.UIBlobModel;
        public get data():string {
            return this._data.value;
        }
        public set data(data:string) {
            var self = this;
            self._data.value = data;
        //    self.data_listeners.forEach(cb => { cb(<TmpBlob>self); });
        }
        //public data_listeners: Array<(c:TmpBlob) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<TmpBlob>self); });
        }
        //public rowVersion_listeners: Array<(c:TmpBlob) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.id_listeners.splice(0);
            self.data_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : TmpBlob {
            var key = "TmpBlob:"+x.id;
            var ret =  new TmpBlob(
                x.id,
                x.data,
                x.rowVersion
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): TmpBlob {
            
            return <TmpBlob>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : TmpBlob {
            var self = this;
            var key="";
            var ret:TmpBlob = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "TmpBlob:"+x.$refId;
                ret = <TmpBlob>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "TmpBlobBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "TmpBlob:"+x.id;
                    var cachedCopy = <TmpBlob>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = TmpBlob.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = TmpBlob.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = TmpBlob.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<TmpBlob> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<TmpBlob> = _.map(data, (x:any):TmpBlob => {
                return TmpBlob.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:TmpBlob[])=>void)  {
            var url = "/Niva/rest/TmpBlob/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = TmpBlob.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.id = this.id;
                ret.data = this.data;
                ret.rowVersion = this.rowVersion;
            return ret;
        }

        public updateInstance(other:TmpBlob):void {
            var self = this;
            self.id = other.id
            self.data = other.data
            self.rowVersion = other.rowVersion
        }

        public static Create() : TmpBlob {
            return new TmpBlob(
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(id:string) : TmpBlob {
            var ret = TmpBlob.Create();
            ret.id = id;
            return ret;
        }

        public static cashedEntities: { [id: string]: TmpBlob; } = {};
        public static findById_unecrypted(id:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : TmpBlob {
            if (Entities.TmpBlob.cashedEntities[id.toString()] !== undefined)
                return Entities.TmpBlob.cashedEntities[id.toString()];

            var wsPath = "TmpBlob/findById_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['id'] = id;
            var ret = TmpBlob.Create();
            Entities.TmpBlob.cashedEntities[id.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.TmpBlob.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + id + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['TmpBlob'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.TmpBlob.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.TmpBlob.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}