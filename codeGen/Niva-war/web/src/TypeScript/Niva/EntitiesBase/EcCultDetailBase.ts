/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCult.ts" />
/// <reference path="../Entities/EcCultDetail.ts" />

module Entities {

    export class EcCultDetailBase extends NpTypes.BaseEntity {
        constructor(
            eccdId: string,
            orderingNumber: number,
            agreesDeclar: number,
            comparisonOper: number,
            probabThres: number,
            decisionLight: number,
            rowVersion: number,
            agreesDeclar2: number,
            comparisonOper2: number,
            probabThres2: number,
            probabThresSum: number,
            comparisonOper3: number,
            eccuId: EcCult) 
        {
            super();
            var self = this;
            this._eccdId = new NpTypes.UIStringModel(eccdId, this);
            this._orderingNumber = new NpTypes.UINumberModel(orderingNumber, this);
            this._agreesDeclar = new NpTypes.UINumberModel(agreesDeclar, this);
            this._comparisonOper = new NpTypes.UINumberModel(comparisonOper, this);
            this._probabThres = new NpTypes.UINumberModel(probabThres, this);
            this._decisionLight = new NpTypes.UINumberModel(decisionLight, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._agreesDeclar2 = new NpTypes.UINumberModel(agreesDeclar2, this);
            this._comparisonOper2 = new NpTypes.UINumberModel(comparisonOper2, this);
            this._probabThres2 = new NpTypes.UINumberModel(probabThres2, this);
            this._probabThresSum = new NpTypes.UINumberModel(probabThresSum, this);
            this._comparisonOper3 = new NpTypes.UINumberModel(comparisonOper3, this);
            this._eccuId = new NpTypes.UIManyToOneModel<Entities.EcCult>(eccuId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.eccdId; 
        }
        
        public getKeyName(): string { 
            return "eccdId"; 
        }

        public getEntityName(): string { 
            return 'EcCultDetail'; 
        }
        
        public fromJSON(data: any): EcCultDetail[] { 
            return EcCultDetail.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.eccdId === null || this.eccdId === undefined)
                return true;
            if (this.eccdId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: EcCultDetailBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //eccdId property
        _eccdId: NpTypes.UIStringModel;
        public get eccdId():string {
            return this._eccdId.value;
        }
        public set eccdId(eccdId:string) {
            var self = this;
            self._eccdId.value = eccdId;
        //    self.eccdId_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public eccdId_listeners: Array<(c:EcCultDetail) => void> = [];
        //orderingNumber property
        _orderingNumber: NpTypes.UINumberModel;
        public get orderingNumber():number {
            return this._orderingNumber.value;
        }
        public set orderingNumber(orderingNumber:number) {
            var self = this;
            self._orderingNumber.value = orderingNumber;
        //    self.orderingNumber_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public orderingNumber_listeners: Array<(c:EcCultDetail) => void> = [];
        //agreesDeclar property
        _agreesDeclar: NpTypes.UINumberModel;
        public get agreesDeclar():number {
            return this._agreesDeclar.value;
        }
        public set agreesDeclar(agreesDeclar:number) {
            var self = this;
            self._agreesDeclar.value = agreesDeclar;
        //    self.agreesDeclar_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public agreesDeclar_listeners: Array<(c:EcCultDetail) => void> = [];
        //comparisonOper property
        _comparisonOper: NpTypes.UINumberModel;
        public get comparisonOper():number {
            return this._comparisonOper.value;
        }
        public set comparisonOper(comparisonOper:number) {
            var self = this;
            self._comparisonOper.value = comparisonOper;
        //    self.comparisonOper_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public comparisonOper_listeners: Array<(c:EcCultDetail) => void> = [];
        //probabThres property
        _probabThres: NpTypes.UINumberModel;
        public get probabThres():number {
            return this._probabThres.value;
        }
        public set probabThres(probabThres:number) {
            var self = this;
            self._probabThres.value = probabThres;
        //    self.probabThres_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public probabThres_listeners: Array<(c:EcCultDetail) => void> = [];
        //decisionLight property
        _decisionLight: NpTypes.UINumberModel;
        public get decisionLight():number {
            return this._decisionLight.value;
        }
        public set decisionLight(decisionLight:number) {
            var self = this;
            self._decisionLight.value = decisionLight;
        //    self.decisionLight_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public decisionLight_listeners: Array<(c:EcCultDetail) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public rowVersion_listeners: Array<(c:EcCultDetail) => void> = [];
        //agreesDeclar2 property
        _agreesDeclar2: NpTypes.UINumberModel;
        public get agreesDeclar2():number {
            return this._agreesDeclar2.value;
        }
        public set agreesDeclar2(agreesDeclar2:number) {
            var self = this;
            self._agreesDeclar2.value = agreesDeclar2;
        //    self.agreesDeclar2_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public agreesDeclar2_listeners: Array<(c:EcCultDetail) => void> = [];
        //comparisonOper2 property
        _comparisonOper2: NpTypes.UINumberModel;
        public get comparisonOper2():number {
            return this._comparisonOper2.value;
        }
        public set comparisonOper2(comparisonOper2:number) {
            var self = this;
            self._comparisonOper2.value = comparisonOper2;
        //    self.comparisonOper2_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public comparisonOper2_listeners: Array<(c:EcCultDetail) => void> = [];
        //probabThres2 property
        _probabThres2: NpTypes.UINumberModel;
        public get probabThres2():number {
            return this._probabThres2.value;
        }
        public set probabThres2(probabThres2:number) {
            var self = this;
            self._probabThres2.value = probabThres2;
        //    self.probabThres2_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public probabThres2_listeners: Array<(c:EcCultDetail) => void> = [];
        //probabThresSum property
        _probabThresSum: NpTypes.UINumberModel;
        public get probabThresSum():number {
            return this._probabThresSum.value;
        }
        public set probabThresSum(probabThresSum:number) {
            var self = this;
            self._probabThresSum.value = probabThresSum;
        //    self.probabThresSum_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public probabThresSum_listeners: Array<(c:EcCultDetail) => void> = [];
        //comparisonOper3 property
        _comparisonOper3: NpTypes.UINumberModel;
        public get comparisonOper3():number {
            return this._comparisonOper3.value;
        }
        public set comparisonOper3(comparisonOper3:number) {
            var self = this;
            self._comparisonOper3.value = comparisonOper3;
        //    self.comparisonOper3_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public comparisonOper3_listeners: Array<(c:EcCultDetail) => void> = [];
        //eccuId property
        _eccuId: NpTypes.UIManyToOneModel<Entities.EcCult>;
        public get eccuId():EcCult {
            return this._eccuId.value;
        }
        public set eccuId(eccuId:EcCult) {
            var self = this;
            self._eccuId.value = eccuId;
        //    self.eccuId_listeners.forEach(cb => { cb(<EcCultDetail>self); });
        }
        //public eccuId_listeners: Array<(c:EcCultDetail) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.eccdId_listeners.splice(0);
            self.orderingNumber_listeners.splice(0);
            self.agreesDeclar_listeners.splice(0);
            self.comparisonOper_listeners.splice(0);
            self.probabThres_listeners.splice(0);
            self.decisionLight_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.agreesDeclar2_listeners.splice(0);
            self.comparisonOper2_listeners.splice(0);
            self.probabThres2_listeners.splice(0);
            self.probabThresSum_listeners.splice(0);
            self.comparisonOper3_listeners.splice(0);
            self.eccuId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : EcCultDetail {
            var key = "EcCultDetail:"+x.eccdId;
            var ret =  new EcCultDetail(
                x.eccdId,
                x.orderingNumber,
                x.agreesDeclar,
                x.comparisonOper,
                x.probabThres,
                x.decisionLight,
                x.rowVersion,
                x.agreesDeclar2,
                x.comparisonOper2,
                x.probabThres2,
                x.probabThresSum,
                x.comparisonOper3,
                x.eccuId
            );
            deserializedEntities[key] = ret;
            ret.eccuId = (x.eccuId !== undefined && x.eccuId !== null) ? <EcCult>NpTypes.BaseEntity.entitiesFactory[x.eccuId.$entityName].fromJSONPartial(x.eccuId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): EcCultDetail {
            
            return <EcCultDetail>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : EcCultDetail {
            var self = this;
            var key="";
            var ret:EcCultDetail = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcCultDetail:"+x.$refId;
                ret = <EcCultDetail>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcCultDetailBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcCultDetail:"+x.eccdId;
                    var cachedCopy = <EcCultDetail>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcCultDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcCultDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = EcCultDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcCultDetail> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<EcCultDetail> = _.map(data, (x:any):EcCultDetail => {
                return EcCultDetail.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:EcCultDetail[])=>void)  {
            var url = "/Niva/rest/EcCultDetail/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = EcCultDetail.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.eccdId = this.eccdId;
                ret.orderingNumber = this.orderingNumber;
                ret.agreesDeclar = this.agreesDeclar;
                ret.comparisonOper = this.comparisonOper;
                ret.probabThres = this.probabThres;
                ret.decisionLight = this.decisionLight;
                ret.rowVersion = this.rowVersion;
                ret.agreesDeclar2 = this.agreesDeclar2;
                ret.comparisonOper2 = this.comparisonOper2;
                ret.probabThres2 = this.probabThres2;
                ret.probabThresSum = this.probabThresSum;
                ret.comparisonOper3 = this.comparisonOper3;
                ret.eccuId = 
                    (this.eccuId !== undefined && this.eccuId !== null) ? 
                        { eccuId :  this.eccuId.eccuId} :
                        (this.eccuId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:EcCultDetail):void {
            var self = this;
            self.eccdId = other.eccdId
            self.orderingNumber = other.orderingNumber
            self.agreesDeclar = other.agreesDeclar
            self.comparisonOper = other.comparisonOper
            self.probabThres = other.probabThres
            self.decisionLight = other.decisionLight
            self.rowVersion = other.rowVersion
            self.agreesDeclar2 = other.agreesDeclar2
            self.comparisonOper2 = other.comparisonOper2
            self.probabThres2 = other.probabThres2
            self.probabThresSum = other.probabThresSum
            self.comparisonOper3 = other.comparisonOper3
            self.eccuId = other.eccuId
        }

        public static Create() : EcCultDetail {
            return new EcCultDetail(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(eccdId:string) : EcCultDetail {
            var ret = EcCultDetail.Create();
            ret.eccdId = eccdId;
            return ret;
        }

        public static cashedEntities: { [id: string]: EcCultDetail; } = {};
        public static findById_unecrypted(eccdId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : EcCultDetail {
            if (Entities.EcCultDetail.cashedEntities[eccdId.toString()] !== undefined)
                return Entities.EcCultDetail.cashedEntities[eccdId.toString()];

            var wsPath = "EcCultDetail/findByEccdId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['eccdId'] = eccdId;
            var ret = EcCultDetail.Create();
            Entities.EcCultDetail.cashedEntities[eccdId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.EcCultDetail.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + eccdId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['EcCultDetail'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.EcCultDetail.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.EcCultDetail.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}