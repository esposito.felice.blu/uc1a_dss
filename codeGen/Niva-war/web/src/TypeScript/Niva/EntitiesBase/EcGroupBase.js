/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCult.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
/// <reference path="../Entities/EcGroup.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcGroupBase = (function (_super) {
        __extends(EcGroupBase, _super);
        function EcGroupBase(ecgrId, description, name, rowVersion, recordtype, cropLevel) {
            _super.call(this);
            var self = this;
            this._ecgrId = new NpTypes.UIStringModel(ecgrId, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._recordtype = new NpTypes.UINumberModel(recordtype, this);
            this._cropLevel = new NpTypes.UINumberModel(cropLevel, this);
            self.postConstruct();
        }
        EcGroupBase.prototype.getKey = function () {
            return this.ecgrId;
        };
        EcGroupBase.prototype.getKeyName = function () {
            return "ecgrId";
        };
        EcGroupBase.prototype.getEntityName = function () {
            return 'EcGroup';
        };
        EcGroupBase.prototype.fromJSON = function (data) {
            return Entities.EcGroup.fromJSONComplete(data);
        };
        Object.defineProperty(EcGroupBase.prototype, "ecgrId", {
            get: function () {
                return this._ecgrId.value;
            },
            set: function (ecgrId) {
                var self = this;
                self._ecgrId.value = ecgrId;
                //    self.ecgrId_listeners.forEach(cb => { cb(<EcGroup>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupBase.prototype, "description", {
            get: function () {
                return this._description.value;
            },
            set: function (description) {
                var self = this;
                self._description.value = description;
                //    self.description_listeners.forEach(cb => { cb(<EcGroup>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (name) {
                var self = this;
                self._name.value = name;
                //    self.name_listeners.forEach(cb => { cb(<EcGroup>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<EcGroup>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupBase.prototype, "recordtype", {
            get: function () {
                return this._recordtype.value;
            },
            set: function (recordtype) {
                var self = this;
                self._recordtype.value = recordtype;
                //    self.recordtype_listeners.forEach(cb => { cb(<EcGroup>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupBase.prototype, "cropLevel", {
            get: function () {
                return this._cropLevel.value;
            },
            set: function (cropLevel) {
                var self = this;
                self._cropLevel.value = cropLevel;
                //    self.cropLevel_listeners.forEach(cb => { cb(<EcGroup>self); });
            },
            enumerable: true,
            configurable: true
        });
        EcGroupBase.prototype.ecCultCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.EcGroup.ecCultCollection)) {
                if (self instanceof Entities.EcGroup) {
                    Entities.EcGroup.ecCultCollection(self, func);
                }
            }
        };
        EcGroupBase.prototype.decisionMakingCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.EcGroup.decisionMakingCollection)) {
                if (self instanceof Entities.EcGroup) {
                    Entities.EcGroup.decisionMakingCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.ecgrId_listeners.splice(0);
            self.description_listeners.splice(0);
            self.name_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.recordtype_listeners.splice(0);
            self.cropLevel_listeners.splice(0);
        }
        */
        EcGroupBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "EcGroup:" + x.ecgrId;
            var ret = new Entities.EcGroup(x.ecgrId, x.description, x.name, x.rowVersion, x.recordtype, x.cropLevel);
            deserializedEntities[key] = ret;
            return ret;
        };
        EcGroupBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        EcGroupBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcGroup:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcGroupBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcGroup:"+x.ecgrId;
                    var cachedCopy = <EcGroup>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcGroup.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcGroup.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.EcGroup.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        EcGroupBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.EcGroup.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        EcGroupBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/EcGroup/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.EcGroup.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        EcGroupBase.prototype.toJSON = function () {
            var ret = {};
            ret.ecgrId = this.ecgrId;
            ret.description = this.description;
            ret.name = this.name;
            ret.rowVersion = this.rowVersion;
            ret.recordtype = this.recordtype;
            ret.cropLevel = this.cropLevel;
            return ret;
        };
        EcGroupBase.prototype.updateInstance = function (other) {
            var self = this;
            self.ecgrId = other.ecgrId;
            self.description = other.description;
            self.name = other.name;
            self.rowVersion = other.rowVersion;
            self.recordtype = other.recordtype;
            self.cropLevel = other.cropLevel;
        };
        EcGroupBase.Create = function () {
            return new Entities.EcGroup(undefined, undefined, undefined, undefined, undefined, undefined);
        };
        EcGroupBase.CreateById = function (ecgrId) {
            var ret = Entities.EcGroup.Create();
            ret.ecgrId = ecgrId;
            return ret;
        };
        EcGroupBase.findById_unecrypted = function (ecgrId, $scope, $http, errFunc) {
            if (Entities.EcGroup.cashedEntities[ecgrId.toString()] !== undefined)
                return Entities.EcGroup.cashedEntities[ecgrId.toString()];
            var wsPath = "EcGroup/findByEcgrId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['ecgrId'] = ecgrId;
            var ret = Entities.EcGroup.Create();
            Entities.EcGroup.cashedEntities[ecgrId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.EcGroup.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + ecgrId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        EcGroupBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public cropLevel_listeners: Array<(c:EcGroup) => void> = [];
        EcGroupBase.ecCultCollection = null; //(ecGroup, list) => { };
        EcGroupBase.decisionMakingCollection = null; //(ecGroup, list) => { };
        EcGroupBase.cashedEntities = {};
        return EcGroupBase;
    })(NpTypes.BaseEntity);
    Entities.EcGroupBase = EcGroupBase;
    NpTypes.BaseEntity.entitiesFactory['EcGroup'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.EcGroup.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.EcGroup.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=EcGroupBase.js.map