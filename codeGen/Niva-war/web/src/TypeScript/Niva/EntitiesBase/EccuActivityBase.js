/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Activity.ts" />
/// <reference path="../Entities/EcCultDetail.ts" />
/// <reference path="../Entities/EccuActivity.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EccuActivityBase = (function (_super) {
        __extends(EccuActivityBase, _super);
        function EccuActivityBase(eactId, rowVersion, actiId, eccdId) {
            _super.call(this);
            var self = this;
            this._eactId = new NpTypes.UIStringModel(eactId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._actiId = new NpTypes.UIManyToOneModel(actiId, this);
            this._eccdId = new NpTypes.UIManyToOneModel(eccdId, this);
            self.postConstruct();
        }
        EccuActivityBase.prototype.getKey = function () {
            return this.eactId;
        };
        EccuActivityBase.prototype.getKeyName = function () {
            return "eactId";
        };
        EccuActivityBase.prototype.getEntityName = function () {
            return 'EccuActivity';
        };
        EccuActivityBase.prototype.fromJSON = function (data) {
            return Entities.EccuActivity.fromJSONComplete(data);
        };
        Object.defineProperty(EccuActivityBase.prototype, "eactId", {
            get: function () {
                return this._eactId.value;
            },
            set: function (eactId) {
                var self = this;
                self._eactId.value = eactId;
                //    self.eactId_listeners.forEach(cb => { cb(<EccuActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EccuActivityBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<EccuActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EccuActivityBase.prototype, "actiId", {
            get: function () {
                return this._actiId.value;
            },
            set: function (actiId) {
                var self = this;
                self._actiId.value = actiId;
                //    self.actiId_listeners.forEach(cb => { cb(<EccuActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EccuActivityBase.prototype, "eccdId", {
            get: function () {
                return this._eccdId.value;
            },
            set: function (eccdId) {
                var self = this;
                self._eccdId.value = eccdId;
                //    self.eccdId_listeners.forEach(cb => { cb(<EccuActivity>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public eccdId_listeners: Array<(c:EccuActivity) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.eactId_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.actiId_listeners.splice(0);
            self.eccdId_listeners.splice(0);
        }
        */
        EccuActivityBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "EccuActivity:" + x.eactId;
            var ret = new Entities.EccuActivity(x.eactId, x.rowVersion, x.actiId, x.eccdId);
            deserializedEntities[key] = ret;
            ret.actiId = (x.actiId !== undefined && x.actiId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.actiId.$entityName].fromJSONPartial(x.actiId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.eccdId = (x.eccdId !== undefined && x.eccdId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.eccdId.$entityName].fromJSONPartial(x.eccdId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        EccuActivityBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        EccuActivityBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EccuActivity:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EccuActivityBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EccuActivity:"+x.eactId;
                    var cachedCopy = <EccuActivity>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EccuActivity.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EccuActivity.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.EccuActivity.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        EccuActivityBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.EccuActivity.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        EccuActivityBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/EccuActivity/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.EccuActivity.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        EccuActivityBase.prototype.toJSON = function () {
            var ret = {};
            ret.eactId = this.eactId;
            ret.rowVersion = this.rowVersion;
            ret.actiId =
                (this.actiId !== undefined && this.actiId !== null) ?
                    { actiId: this.actiId.actiId } :
                    (this.actiId === undefined ? undefined : null);
            ret.eccdId =
                (this.eccdId !== undefined && this.eccdId !== null) ?
                    { eccdId: this.eccdId.eccdId } :
                    (this.eccdId === undefined ? undefined : null);
            return ret;
        };
        EccuActivityBase.prototype.updateInstance = function (other) {
            var self = this;
            self.eactId = other.eactId;
            self.rowVersion = other.rowVersion;
            self.actiId = other.actiId;
            self.eccdId = other.eccdId;
        };
        EccuActivityBase.Create = function () {
            return new Entities.EccuActivity(undefined, undefined, undefined, undefined);
        };
        EccuActivityBase.CreateById = function (eactId) {
            var ret = Entities.EccuActivity.Create();
            ret.eactId = eactId;
            return ret;
        };
        EccuActivityBase.findById_unecrypted = function (eactId, $scope, $http, errFunc) {
            if (Entities.EccuActivity.cashedEntities[eactId.toString()] !== undefined)
                return Entities.EccuActivity.cashedEntities[eactId.toString()];
            var wsPath = "EccuActivity/findByEactId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['eactId'] = eactId;
            var ret = Entities.EccuActivity.Create();
            Entities.EccuActivity.cashedEntities[eactId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.EccuActivity.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + eactId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        EccuActivityBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        EccuActivityBase.cashedEntities = {};
        return EccuActivityBase;
    })(NpTypes.BaseEntity);
    Entities.EccuActivityBase = EccuActivityBase;
    NpTypes.BaseEntity.entitiesFactory['EccuActivity'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.EccuActivity.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.EccuActivity.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=EccuActivityBase.js.map