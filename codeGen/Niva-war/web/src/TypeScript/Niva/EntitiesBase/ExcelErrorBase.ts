/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ExcelFile.ts" />
/// <reference path="../Entities/ExcelError.ts" />

module Entities {

    export class ExcelErrorBase extends NpTypes.BaseEntity {
        constructor(
            id: string,
            excelRowNum: number,
            errMessage: string,
            rowVersion: number,
            exfiId: ExcelFile) 
        {
            super();
            var self = this;
            this._id = new NpTypes.UIStringModel(id, this);
            this._excelRowNum = new NpTypes.UINumberModel(excelRowNum, this);
            this._errMessage = new NpTypes.UIStringModel(errMessage, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._exfiId = new NpTypes.UIManyToOneModel<Entities.ExcelFile>(exfiId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.id; 
        }
        
        public getKeyName(): string { 
            return "id"; 
        }

        public getEntityName(): string { 
            return 'ExcelError'; 
        }
        
        public fromJSON(data: any): ExcelError[] { 
            return ExcelError.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.id === null || this.id === undefined)
                return true;
            if (this.id.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: ExcelErrorBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //id property
        _id: NpTypes.UIStringModel;
        public get id():string {
            return this._id.value;
        }
        public set id(id:string) {
            var self = this;
            self._id.value = id;
        //    self.id_listeners.forEach(cb => { cb(<ExcelError>self); });
        }
        //public id_listeners: Array<(c:ExcelError) => void> = [];
        //excelRowNum property
        _excelRowNum: NpTypes.UINumberModel;
        public get excelRowNum():number {
            return this._excelRowNum.value;
        }
        public set excelRowNum(excelRowNum:number) {
            var self = this;
            self._excelRowNum.value = excelRowNum;
        //    self.excelRowNum_listeners.forEach(cb => { cb(<ExcelError>self); });
        }
        //public excelRowNum_listeners: Array<(c:ExcelError) => void> = [];
        //errMessage property
        _errMessage: NpTypes.UIStringModel;
        public get errMessage():string {
            return this._errMessage.value;
        }
        public set errMessage(errMessage:string) {
            var self = this;
            self._errMessage.value = errMessage;
        //    self.errMessage_listeners.forEach(cb => { cb(<ExcelError>self); });
        }
        //public errMessage_listeners: Array<(c:ExcelError) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<ExcelError>self); });
        }
        //public rowVersion_listeners: Array<(c:ExcelError) => void> = [];
        //exfiId property
        _exfiId: NpTypes.UIManyToOneModel<Entities.ExcelFile>;
        public get exfiId():ExcelFile {
            return this._exfiId.value;
        }
        public set exfiId(exfiId:ExcelFile) {
            var self = this;
            self._exfiId.value = exfiId;
        //    self.exfiId_listeners.forEach(cb => { cb(<ExcelError>self); });
        }
        //public exfiId_listeners: Array<(c:ExcelError) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.id_listeners.splice(0);
            self.excelRowNum_listeners.splice(0);
            self.errMessage_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.exfiId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : ExcelError {
            var key = "ExcelError:"+x.id;
            var ret =  new ExcelError(
                x.id,
                x.excelRowNum,
                x.errMessage,
                x.rowVersion,
                x.exfiId
            );
            deserializedEntities[key] = ret;
            ret.exfiId = (x.exfiId !== undefined && x.exfiId !== null) ? <ExcelFile>NpTypes.BaseEntity.entitiesFactory[x.exfiId.$entityName].fromJSONPartial(x.exfiId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): ExcelError {
            
            return <ExcelError>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : ExcelError {
            var self = this;
            var key="";
            var ret:ExcelError = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ExcelError:"+x.$refId;
                ret = <ExcelError>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ExcelErrorBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ExcelError:"+x.id;
                    var cachedCopy = <ExcelError>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ExcelError.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ExcelError.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = ExcelError.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ExcelError> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<ExcelError> = _.map(data, (x:any):ExcelError => {
                return ExcelError.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:ExcelError[])=>void)  {
            var url = "/Niva/rest/ExcelError/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = ExcelError.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.id = this.id;
                ret.excelRowNum = this.excelRowNum;
                ret.errMessage = this.errMessage;
                ret.rowVersion = this.rowVersion;
                ret.exfiId = 
                    (this.exfiId !== undefined && this.exfiId !== null) ? 
                        { id :  this.exfiId.id} :
                        (this.exfiId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:ExcelError):void {
            var self = this;
            self.id = other.id
            self.excelRowNum = other.excelRowNum
            self.errMessage = other.errMessage
            self.rowVersion = other.rowVersion
            self.exfiId = other.exfiId
        }

        public static Create() : ExcelError {
            return new ExcelError(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(id:string) : ExcelError {
            var ret = ExcelError.Create();
            ret.id = id;
            return ret;
        }

        public static cashedEntities: { [id: string]: ExcelError; } = {};
        public static findById_unecrypted(id:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : ExcelError {
            if (Entities.ExcelError.cashedEntities[id.toString()] !== undefined)
                return Entities.ExcelError.cashedEntities[id.toString()];

            var wsPath = "ExcelError/findById_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['id'] = id;
            var ret = ExcelError.Create();
            Entities.ExcelError.cashedEntities[id.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.ExcelError.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + id + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['ExcelError'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.ExcelError.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.ExcelError.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}