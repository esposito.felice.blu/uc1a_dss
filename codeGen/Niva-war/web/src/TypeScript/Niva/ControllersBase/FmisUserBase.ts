/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/FmisUserBase.ts" />
/// <reference path="../Controllers/FmisUser.ts" />
module Controllers.FmisUser {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpFmisUser:ModelGrpFmisUser;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
        onNewBtnAction(): void;
        onDeleteBtnAction():void;

    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            

            $scope.onNewBtnAction = () => { 
                self.onNewBtnAction();
            };
            
            $scope.onDeleteBtnAction = () => { 
                self.onDeleteBtnAction();
            };


            $timeout(function() {
                $('#FmisUser').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "FmisUser";
        }
    
        public _getPageTitle(): string {
            return "FMIS Users";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_FmisUser_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpFmisUser.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpFmisUser.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpFmisUser.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpFmisUser.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpFmisUser.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
            if (bForDelete) {
                pageChanges = pageChanges.concat(self.model.modelGrpFmisUser.controller.getChangesToCommitForDeletion());
                pageChanges = pageChanges.concat(self.model.modelGrpFmisUser.controller.getDeleteChangesToCommit());
            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpFmisUser.controller.getChangesToCommit());
            }
            
            var hasFmisUser = pageChanges.some(change => change.entityName === "FmisUser")
            if (!hasFmisUser) {
                var validateEntity = new ChangeToCommit(ChangeStatus.UPDATE, 0, "FmisUser", self.model.modelGrpFmisUser.controller.Current);
                pageChanges = pageChanges.concat(validateEntity);
            }
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_FmisUser"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpFmisUser.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;

                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.FmisUser>>self.$scope.getCurrentPageModel();
                        var newFmisUserId = response.newEntitiesIds.firstOrNull(x => x.entityName === 'FmisUser');
                        if (!isVoid(newFmisUserId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.FmisUser.CreateById(newFmisUserId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        } else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }

                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }

        public getInitialEntity(): Entities.FmisUser {
            if (this.shownAsDialog()) {
                return <Entities.FmisUser>this.dialogSelectedEntity();
            } else {
                var breadCrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.FmisUser>>this.$scope.getPageModelByURL('/FmisUser');
                if (isVoid(breadCrumbStepModel)) {
                    return null;
                } else {
                    return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                }
            }
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


        public onNewBtnAction(): void {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => { 
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                self.addNewRecord();
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {self.addNewRecord();}),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                self.addNewRecord();
            }
        }

        public addNewRecord(): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            self.model.modelGrpFmisUser.controller.cleanUpAfterSave();
            self.model.modelGrpFmisUser.controller.createNewEntityAndAddToUI();
        }

        public onDeleteBtnAction():void {
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, 
                [
                    new Tuple2("MessageBox_Button_Yes", () => {self.deleteRecord();}),
                    new Tuple2("MessageBox_Button_No", () => {})
                ], 1,1);
        }

        public deleteRecord(): void {
            var self = this;
            if (self.Mode === EditMode.NEW) {
                console.log("Delete pressed in a form while being in new mode!");
                return;
            }
            NpTypes.AlertMessage.clearAlerts(self.model);
            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData(true);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
            });
        }
        
        public get Mode(): EditMode {
            var self = this;
            if (isVoid(self.model.modelGrpFmisUser) || isVoid(self.model.modelGrpFmisUser.controller))
                return EditMode.NEW;
            return self.model.modelGrpFmisUser.controller.Mode;
        }

        public _newIsDisabled(): boolean {
            var self = this;
            if (isVoid(self.model.modelGrpFmisUser) || isVoid(self.model.modelGrpFmisUser.controller))
                return true;
            return self.model.modelGrpFmisUser.controller._newIsDisabled();
        }
        public _deleteIsDisabled(): boolean {
            var self = this;
            if (self.Mode === EditMode.NEW)
                return true;
            if (isVoid(self.model.modelGrpFmisUser) || isVoid(self.model.modelGrpFmisUser.controller))
                return true;
            return self.model.modelGrpFmisUser.controller._deleteIsDisabled(self.model.modelGrpFmisUser.controller.Current);
        }


    }


    

    // GROUP GrpFmisUser

    export class ModelGrpFmisUserBase extends Controllers.AbstractGroupFormModel {
        controller: ControllerGrpFmisUser;
        public get fmisName():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisUser>this.selectedEntities[0]).fmisName;
        }

        public set fmisName(fmisName_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisUser>this.selectedEntities[0]).fmisName = fmisName_newVal;
        }

        public get _fmisName():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._fmisName;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisUser>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisUser>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get fmisUsersId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisUser>this.selectedEntities[0]).fmisUsersId;
        }

        public set fmisUsersId(fmisUsersId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisUser>this.selectedEntities[0]).fmisUsersId = fmisUsersId_newVal;
        }

        public get _fmisUsersId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._fmisUsersId;
        }

        public get fmisUid():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisUser>this.selectedEntities[0]).fmisUid;
        }

        public set fmisUid(fmisUid_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisUser>this.selectedEntities[0]).fmisUid = fmisUid_newVal;
        }

        public get _fmisUid():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._fmisUid;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpFmisUser) { super($scope); }
    }



    export interface IScopeGrpFmisUserBase extends Controllers.IAbstractFormGroupScope, IPageScope{
        globals: Globals;
        modelGrpFmisUser : ModelGrpFmisUser;
        GrpFmisUser_itm__fmisName_disabled(fmisUser:Entities.FmisUser):boolean; 
        GrpFmisUser_itm__fmisUid_disabled(fmisUser:Entities.FmisUser):boolean; 

    }



    export class ControllerGrpFmisUserBase extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpFmisUser,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpFmisUser)
        {
            super($scope, $http, $timeout, Plato, model);
            var self:ControllerGrpFmisUserBase = this;
            model.controller = <ControllerGrpFmisUser>self;
            $scope.modelGrpFmisUser = self.model;

            var selectedEntity: Entities.FmisUser = this.$scope.pageModel.controller.getInitialEntity();
            if (isVoid(selectedEntity)) {
                self.createNewEntityAndAddToUI();
            } else {
                var clonedEntity = Entities.FmisUser.Create();
                clonedEntity.updateInstance(selectedEntity);
                $scope.modelGrpFmisUser.visibleEntities[0] = clonedEntity;
                $scope.modelGrpFmisUser.selectedEntities[0] = clonedEntity;
            } 
            $scope.GrpFmisUser_itm__fmisName_disabled = 
                (fmisUser:Entities.FmisUser) => {
                    return self.GrpFmisUser_itm__fmisName_disabled(fmisUser);
                };
            $scope.GrpFmisUser_itm__fmisUid_disabled = 
                (fmisUser:Entities.FmisUser) => {
                    return self.GrpFmisUser_itm__fmisUid_disabled(fmisUser);
                };


            $scope.pageModel.modelGrpFmisUser = $scope.modelGrpFmisUser;


    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.FmisUser[] , oldVisible:Entities.FmisUser[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpFmisUser";
        }
        public get HtmlDivId(): string {
            return "FmisUser_ControllerGrpFmisUser";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: Entities.FmisUser) => 'Name',
                        false ,
                        (ent?: Entities.FmisUser) => ent.fmisName,  
                        (ent?: Entities.FmisUser) => ent._fmisName,  
                        (ent?: Entities.FmisUser) => true, 
                        (vl: string, ent?: Entities.FmisUser) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.FmisUser) => 'UserId',
                        false ,
                        (ent?: Entities.FmisUser) => ent.fmisUid,  
                        (ent?: Entities.FmisUser) => ent._fmisUid,  
                        (ent?: Entities.FmisUser) => true, 
                        (vl: string, ent?: Entities.FmisUser) => new NpTypes.ValidationResult(true, ""), 
                        undefined)
                ];
            }
            return this._items;
        }

        

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpFmisUser():ModelGrpFmisUser {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "FmisUser";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
        }

        public setCurrentFormPageBreadcrumpStepModel() {
            var self = this;
            if (self.$scope.pageModel.controller.shownAsDialog())
                return;

            var breadCrumbStepModel: IFormPageBreadcrumbStepModel<Entities.FmisUser> = { selectedEntity: null };
            if (!isVoid(self.Current)) {
                var selectedEntity: Entities.FmisUser = Entities.FmisUser.Create();
                selectedEntity.updateInstance(self.Current);
                breadCrumbStepModel.selectedEntity = selectedEntity;
            } else {
                breadCrumbStepModel.selectedEntity = null;
            }
            self.$scope.setCurrentPageModel(breadCrumbStepModel);
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.FmisUser.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.FmisUser {
            var self = this;
            return <Entities.FmisUser>self.$scope.modelGrpFmisUser.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.FmisUser, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }







        public constructEntity(): Entities.FmisUser {
            var self = this;
            var ret = new Entities.FmisUser(
                /*fmisName:string*/ null,
                /*rowVersion:number*/ null,
                /*fmisUsersId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*fmisUid:string*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.FmisUser = <Entities.FmisUser>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.$scope.modelGrpFmisUser.visibleEntities[0] = newEnt;
            self.$scope.modelGrpFmisUser.selectedEntities[0] = newEnt;
            return newEnt;

        }

        public cloneEntity(src: Entities.FmisUser): Entities.FmisUser {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.FmisUser, calledByParent: boolean= false): Entities.FmisUser {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.FmisUser.Create();
            ret.updateInstance(src);
            ret.fmisUsersId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.visibleEntities[0] = ret;
            this.model.selectedEntities[0] = ret;

            this.$timeout( () => {
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.FmisUser, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public GrpFmisUser_itm__fmisName_disabled(fmisUser:Entities.FmisUser):boolean {
            var self = this;
            if (fmisUser === undefined || fmisUser === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_FmisUser_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(fmisUser, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpFmisUser_itm__fmisUid_disabled(fmisUser:Entities.FmisUser):boolean {
            var self = this;
            if (fmisUser === undefined || fmisUser === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_FmisUser_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(fmisUser, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_FmisUser_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = false;
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(fmisUser:Entities.FmisUser):boolean  {
            var self = this;
            if (fmisUser === null || fmisUser === undefined || fmisUser.getEntityName() !== "FmisUser")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_FmisUser_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(fmisUser, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }

}
