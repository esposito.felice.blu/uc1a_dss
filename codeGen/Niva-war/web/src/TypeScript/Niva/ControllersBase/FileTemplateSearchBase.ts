/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />

// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/FileTemplateBase.ts" />
/// <reference path="../Controllers/FileTemplateSearch.ts" />

module Controllers {

    export class ModelFileTemplateSearchBase extends Controllers.AbstractSearchPageModel {
        _name:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get name():string {
            return this._name.value;
        }
        public set name(vl:string) {
            this._name.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        controller: ControllerFileTemplateSearch;
        constructor(public $scope: IScopeFileTemplateSearch) { super($scope); }
    }
    
    export class ModelFileTemplateSearchPersistedModel implements NpTypes.IBreadcrumbStepModel {
        _pageSize: number;
        _currentPage: number;
        _currentRow: number;
        sortField: string;
        sortOrder: string;
        name:string;
    }



    export interface IScopeFileTemplateSearchBase extends Controllers.IAbstractSearchPageScope {
        globals: Globals;
        pageModel : ModelFileTemplateSearch;
        _newIsDisabled():boolean; 
        _searchIsDisabled():boolean; 
        _deleteIsDisabled(fileTemplate:Entities.FileTemplate):boolean; 
        _editBtnIsDisabled(fileTemplate:Entities.FileTemplate):boolean; 
        _cancelIsDisabled():boolean; 
        d__name_disabled():boolean; 
    }

    export class ControllerFileTemplateSearchBase extends Controllers.AbstractSearchPageController {
        constructor(
            public $scope: IScopeFileTemplateSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:ModelFileTemplateSearch)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self: ControllerFileTemplateSearchBase = this;
            model.controller = <ControllerFileTemplateSearch>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'Search';
            
            var lastSearchPageModel = <ModelFileTemplateSearchPersistedModel>$scope.getPageModelByURL('/FileTemplateSearch');
            var rowIndexToSelect: number = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.name = lastSearchPageModel.name;
            }


            $scope._newIsDisabled = 
                () => {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled = 
                () => {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled = 
                (fileTemplate:Entities.FileTemplate) => {
                    return self._deleteIsDisabled(fileTemplate);
                };
            $scope._editBtnIsDisabled = 
                (fileTemplate:Entities.FileTemplate) => {
                    return self._editBtnIsDisabled(fileTemplate);
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };
            $scope.d__name_disabled = 
                () => {
                    return self.d__name_disabled();
                };

            $scope.clearBtnAction = () => { 
                self.pageModel.name = undefined;
                self.updateGrid(0, false, true);
            };

            $scope.newBtnAction = () => { 
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };

            $scope.onEdit = (x:Entities.FileTemplate):void => {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };

            $timeout(function() {
                $('#Search_FileTemplate').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);

            self.updateUI(rowIndexToSelect);
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerFileTemplateSearch";
        }
        public get HtmlDivId(): string {
            return "Search_FileTemplate";
        }
    
        public _getPageTitle(): string {
            return "Data Import Template";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined},
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'name', displayName:'getALString("Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'28%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.name' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }


        public getPersistedModel():ModelFileTemplateSearchPersistedModel {
            var self = this;
            var ret = new ModelFileTemplateSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.name = self.pageModel.name;
            return ret;
        }

        public get pageModel():ModelFileTemplateSearch {
            return <ModelFileTemplateSearch>this.model;
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            return Entities.FileTemplate.fromJSONComplete(webResponse.data);
        }

        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = "FileTemplate/findLazyFileTemplate";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};

            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;

            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.name)) {
                paramData['name'] = self.pageModel.name;
            }

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(
                url,
                paramData,
                {timeout:self.$scope.globals.timeoutInMS, cache:false});
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });

        }

        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = "FileTemplate/findLazyFileTemplate_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.name)) {
                paramData['name'] = self.pageModel.name;
            }
            
            var promise = self.$http.post(
                url,
                paramData,
                { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        }

        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
            var paramData = {};

            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.name)) {
                paramData['name'] = self.pageModel.name;
            }
                
            var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
            return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        }


        public get Current():Entities.FileTemplate {
            var self = this;
            return <Entities.FileTemplate>self.$scope.pageModel.selectedEntities[0];
        }

        public onNew():void {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/FileTemplate', self._getPageTitle(), {});
        }

        public onEdit(x:Entities.FileTemplate):void {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel:IFormPageBreadcrumbStepModel<Entities.FileTemplate>= {"selectedEntity":x};
            self.$scope.navigateForward('/FileTemplate', self._getPageTitle(), visitedPageModel);
        }
        
        private getSynchronizeChangesWithDbUrl(): string {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_FileTemplate";
        }
        public getSynchronizeChangesWithDbData(x:Entities.FileTemplate):any {
            var self = this;
            var paramData:any = {
                data: <ChangeToCommit[]>[]
            };
            var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);

            paramData.data.push(changeToCommit);
            return paramData;
        }

        public deleteRecord(x:Entities.FileTemplate):void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);

            console.log("deleting FileTemplate with PK field:" + x.fiteId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                    self.updateGrid();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        }
        
        public _newIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _searchIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _deleteIsDisabled(fileTemplate:Entities.FileTemplate):boolean {
            var self = this;
            if (fileTemplate === undefined || fileTemplate === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_D"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _editBtnIsDisabled(fileTemplate:Entities.FileTemplate):boolean {
            var self = this;
            if (fileTemplate === undefined || fileTemplate === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_R"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public d__name_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }

    }
}
