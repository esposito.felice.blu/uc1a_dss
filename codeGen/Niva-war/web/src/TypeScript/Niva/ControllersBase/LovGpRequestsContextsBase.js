/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/GpRequestsContextsBase.ts" />
/// <reference path="../Controllers/LovGpRequestsContexts.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovGpRequestsContextsBase = (function (_super) {
        __extends(ModelLovGpRequestsContextsBase, _super);
        function ModelLovGpRequestsContextsBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._fsch_GpRequestsContextsLov_type = new NpTypes.UIStringModel(undefined);
            this._fsch_GpRequestsContextsLov_label = new NpTypes.UIStringModel(undefined);
            this._fsch_GpRequestsContextsLov_comment = new NpTypes.UIStringModel(undefined);
            this._fsch_GpRequestsContextsLov_geomHexewkb = new NpTypes.UIStringModel(undefined);
        }
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "fsch_GpRequestsContextsLov_type", {
            get: function () {
                return this._fsch_GpRequestsContextsLov_type.value;
            },
            set: function (vl) {
                this._fsch_GpRequestsContextsLov_type.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "fsch_GpRequestsContextsLov_label", {
            get: function () {
                return this._fsch_GpRequestsContextsLov_label.value;
            },
            set: function (vl) {
                this._fsch_GpRequestsContextsLov_label.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "fsch_GpRequestsContextsLov_comment", {
            get: function () {
                return this._fsch_GpRequestsContextsLov_comment.value;
            },
            set: function (vl) {
                this._fsch_GpRequestsContextsLov_comment.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "fsch_GpRequestsContextsLov_geomHexewkb", {
            get: function () {
                return this._fsch_GpRequestsContextsLov_geomHexewkb.value;
            },
            set: function (vl) {
                this._fsch_GpRequestsContextsLov_geomHexewkb.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovGpRequestsContextsBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelLovGpRequestsContextsBase;
    })(Controllers.AbstractLovModel);
    Controllers.ModelLovGpRequestsContextsBase = ModelLovGpRequestsContextsBase;
    var ControllerLovGpRequestsContextsBase = (function (_super) {
        __extends(ControllerLovGpRequestsContextsBase, _super);
        function ControllerLovGpRequestsContextsBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 5, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            $scope.clearBtnAction = function () {
                self.model.fsch_GpRequestsContextsLov_type = undefined;
                self.model.fsch_GpRequestsContextsLov_label = undefined;
                self.model.fsch_GpRequestsContextsLov_comment = undefined;
                self.model.fsch_GpRequestsContextsLov_geomHexewkb = undefined;
                self.updateGrid();
            };
            $scope._disabled =
                function () {
                    return self._disabled();
                };
            $scope._invisible =
                function () {
                    return self._invisible();
                };
            $scope.LovGpRequestsContexts_fsch_GpRequestsContextsLov_type_disabled =
                function () {
                    return self.LovGpRequestsContexts_fsch_GpRequestsContextsLov_type_disabled();
                };
            $scope.LovGpRequestsContexts_fsch_GpRequestsContextsLov_label_disabled =
                function () {
                    return self.LovGpRequestsContexts_fsch_GpRequestsContextsLov_label_disabled();
                };
            $scope.LovGpRequestsContexts_fsch_GpRequestsContextsLov_comment_disabled =
                function () {
                    return self.LovGpRequestsContexts_fsch_GpRequestsContextsLov_comment_disabled();
                };
            $scope.LovGpRequestsContexts_fsch_GpRequestsContextsLov_geomHexewkb_disabled =
                function () {
                    return self.LovGpRequestsContexts_fsch_GpRequestsContextsLov_geomHexewkb_disabled();
                };
            $scope.modelLovGpRequestsContexts = model;
            self.updateUI();
        }
        ControllerLovGpRequestsContextsBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerLovGpRequestsContextsBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerLovGpRequestsContexts";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerLovGpRequestsContextsBase.prototype, "HtmlDivId", {
            get: function () {
                return "GpRequestsContexts_Id";
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovGpRequestsContextsBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName);
            }
            var ret = [
                { cellClass: 'cellToolTip', field: 'type', displayName: 'getALString("type", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.type' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'label', displayName: 'getALString("label", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.label' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'comment', displayName: 'getALString("comment", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.comment' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'geomHexewkb', displayName: 'getALString("geomHexewkb", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.geomHexewkb' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'referencepoint', displayName: 'getALString("referencepoint", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.referencepoint' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'hash', displayName: 'getALString("hash", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '12%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.hash' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter((function (cl) { return cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true; }));
            if (self.isMultiSelect()) {
                ret.splice(0, 0, { width: '22', cellTemplate: "<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined });
            }
            return ret;
        };
        ControllerLovGpRequestsContextsBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.GpRequestsContexts.fromJSONComplete(webResponse.data);
        };
        Object.defineProperty(ControllerLovGpRequestsContextsBase.prototype, "modelLovGpRequestsContexts", {
            get: function () {
                var self = this;
                return self.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovGpRequestsContextsBase.prototype.initializeModelWithPreviousValues = function () {
            var self = this;
            var prevModel = self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovGpRequestsContexts.fsch_GpRequestsContextsLov_type = prevModel.fsch_GpRequestsContextsLov_type;
                self.modelLovGpRequestsContexts.fsch_GpRequestsContextsLov_label = prevModel.fsch_GpRequestsContextsLov_label;
                self.modelLovGpRequestsContexts.fsch_GpRequestsContextsLov_comment = prevModel.fsch_GpRequestsContextsLov_comment;
                self.modelLovGpRequestsContexts.fsch_GpRequestsContextsLov_geomHexewkb = prevModel.fsch_GpRequestsContextsLov_geomHexewkb;
            }
        };
        ControllerLovGpRequestsContextsBase.prototype._disabled = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovGpRequestsContextsBase.prototype._invisible = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovGpRequestsContextsBase.prototype.LovGpRequestsContexts_fsch_GpRequestsContextsLov_type_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovGpRequestsContextsBase.prototype.LovGpRequestsContexts_fsch_GpRequestsContextsLov_label_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovGpRequestsContextsBase.prototype.LovGpRequestsContexts_fsch_GpRequestsContextsLov_comment_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovGpRequestsContextsBase.prototype.LovGpRequestsContexts_fsch_GpRequestsContextsLov_geomHexewkb_disabled = function () {
            var self = this;
            return false;
        };
        return ControllerLovGpRequestsContextsBase;
    })(Controllers.LovController);
    Controllers.ControllerLovGpRequestsContextsBase = ControllerLovGpRequestsContextsBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovGpRequestsContextsBase.js.map