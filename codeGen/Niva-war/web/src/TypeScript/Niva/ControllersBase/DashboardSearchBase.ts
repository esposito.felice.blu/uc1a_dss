/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />

// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../Controllers/DashboardSearch.ts" />

module Controllers {

    export class ModelDashboardSearchBase extends Controllers.AbstractSearchPageModel {
        _parcIdentifier:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get parcIdentifier():string {
            return this._parcIdentifier.value;
        }
        public set parcIdentifier(vl:string) {
            this._parcIdentifier.value = vl;
        }
        _parcCode:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get parcCode():string {
            return this._parcCode.value;
        }
        public set parcCode(vl:string) {
            this._parcCode.value = vl;
        }
        _prodCode:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get prodCode():number {
            return this._prodCode.value;
        }
        public set prodCode(vl:number) {
            this._prodCode.value = vl;
        }
        _clasId:NpTypes.UIManyToOneModel<Entities.Classification> = new NpTypes.UIManyToOneModel<Entities.Classification>(undefined);
        public get clasId():Entities.Classification {
            return this._clasId.value;
        }
        public set clasId(vl:Entities.Classification) {
            this._clasId.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        controller: ControllerDashboardSearch;
        constructor(public $scope: IScopeDashboardSearch) { super($scope); }
    }
    
    export class ModelDashboardSearchPersistedModel implements NpTypes.IBreadcrumbStepModel {
        _pageSize: number;
        _currentPage: number;
        _currentRow: number;
        sortField: string;
        sortOrder: string;
        parcIdentifier:string;
        parcCode:string;
        prodCode:number;
        clasId:Entities.Classification;
    }



    export interface IScopeDashboardSearchBase extends Controllers.IAbstractSearchPageScope {
        globals: Globals;
        pageModel : ModelDashboardSearch;
        _newIsDisabled():boolean; 
        _searchIsDisabled():boolean; 
        _deleteIsDisabled(parcelClas:Entities.ParcelClas):boolean; 
        _editBtnIsDisabled(parcelClas:Entities.ParcelClas):boolean; 
        _cancelIsDisabled():boolean; 
        d__parcIdentifier_disabled():boolean; 
        d__parcCode_disabled():boolean; 
        d__prodCode_disabled():boolean; 
        d__clasId_disabled():boolean; 
        showLov_d__clasId():void; 
    }

    export class ControllerDashboardSearchBase extends Controllers.AbstractSearchPageController {
        constructor(
            public $scope: IScopeDashboardSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:ModelDashboardSearch)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self: ControllerDashboardSearchBase = this;
            model.controller = <ControllerDashboardSearch>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'Parcels';
            
            var lastSearchPageModel = <ModelDashboardSearchPersistedModel>$scope.getPageModelByURL('/DashboardSearch');
            var rowIndexToSelect: number = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.parcIdentifier = lastSearchPageModel.parcIdentifier;
                self.pageModel.parcCode = lastSearchPageModel.parcCode;
                self.pageModel.prodCode = lastSearchPageModel.prodCode;
                self.pageModel.clasId = lastSearchPageModel.clasId;
            }


            $scope._newIsDisabled = 
                () => {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled = 
                () => {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled = 
                (parcelClas:Entities.ParcelClas) => {
                    return self._deleteIsDisabled(parcelClas);
                };
            $scope._editBtnIsDisabled = 
                (parcelClas:Entities.ParcelClas) => {
                    return self._editBtnIsDisabled(parcelClas);
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };
            $scope.d__parcIdentifier_disabled = 
                () => {
                    return self.d__parcIdentifier_disabled();
                };
            $scope.d__parcCode_disabled = 
                () => {
                    return self.d__parcCode_disabled();
                };
            $scope.d__prodCode_disabled = 
                () => {
                    return self.d__prodCode_disabled();
                };
            $scope.d__clasId_disabled = 
                () => {
                    return self.d__clasId_disabled();
                };
            $scope.showLov_d__clasId = 
                () => {
                    $timeout( () => {        
                        self.showLov_d__clasId();
                    }, 0);
                };

            $scope.clearBtnAction = () => { 
                self.pageModel.parcIdentifier = undefined;
                self.pageModel.parcCode = undefined;
                self.pageModel.prodCode = undefined;
                self.pageModel.clasId = undefined;
                self.updateGrid(0, false, true);
            };

            $scope.newBtnAction = () => { 
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };

            $scope.onEdit = (x:Entities.ParcelClas):void => {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };

            $timeout(function() {
                $('#Search_Dashboard').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);

            self.updateUI(rowIndexToSelect);
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerDashboardSearch";
        }
        public get HtmlDivId(): string {
            return "Search_Dashboard";
        }
    
        public _getPageTitle(): string {
            return "Parcels";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined},
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'parcIdentifier', displayName:'getALString("Parcel Identifier", true)', requiredAsterisk:false, resizable:true, sortable:false, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.parcIdentifier' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'parcCode', displayName:'getALString("Parcel Code", true)', requiredAsterisk:false, resizable:true, sortable:false, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.parcCode' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'prodCode', displayName:'getALString("Producer Code", true)', requiredAsterisk:false, resizable:true, sortable:false, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.prodCode.toStringFormatted(\",\", \"\", 0)' style='width: 100%;text-align: right;padding-right: 6px;' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'clasId.name', displayName:'getALString("Classification", true)', requiredAsterisk:false, resizable:true, sortable:false, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.clasId.name' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }


        public getPersistedModel():ModelDashboardSearchPersistedModel {
            var self = this;
            var ret = new ModelDashboardSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.parcIdentifier = self.pageModel.parcIdentifier;
            ret.parcCode = self.pageModel.parcCode;
            ret.prodCode = self.pageModel.prodCode;
            ret.clasId = self.pageModel.clasId;
            return ret;
        }

        public get pageModel():ModelDashboardSearch {
            return <ModelDashboardSearch>this.model;
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            return Entities.ParcelClas.fromJSONComplete(webResponse.data);
        }

        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = "ParcelClas/findLazyDashboard";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};

            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;

            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodCode)) {
                paramData['prodCode'] = self.pageModel.prodCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcIdentifier)) {
                paramData['parcIdentifier'] = self.pageModel.parcIdentifier;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcCode)) {
                paramData['parcCode'] = self.pageModel.parcCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(
                url,
                paramData,
                {timeout:self.$scope.globals.timeoutInMS, cache:false});
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });

        }

        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = "ParcelClas/findLazyDashboard_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodCode)) {
                paramData['prodCode'] = self.pageModel.prodCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcIdentifier)) {
                paramData['parcIdentifier'] = self.pageModel.parcIdentifier;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcCode)) {
                paramData['parcCode'] = self.pageModel.parcCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            
            var promise = self.$http.post(
                url,
                paramData,
                { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        }

        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
            var paramData = {};

            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodCode)) {
                paramData['prodCode'] = self.pageModel.prodCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcIdentifier)) {
                paramData['parcIdentifier'] = self.pageModel.parcIdentifier;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcCode)) {
                paramData['parcCode'] = self.pageModel.parcCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
                
            var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
            return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        }


        public get Current():Entities.ParcelClas {
            var self = this;
            return <Entities.ParcelClas>self.$scope.pageModel.selectedEntities[0];
        }

        public onNew():void {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/Dashboard', self._getPageTitle(), {});
        }

        public onEdit(x:Entities.ParcelClas):void {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel:IFormPageBreadcrumbStepModel<Entities.ParcelClas>= {"selectedEntity":x};
            self.$scope.navigateForward('/Dashboard', self._getPageTitle(), visitedPageModel);
        }
        
        private getSynchronizeChangesWithDbUrl(): string {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_Dashboard";
        }
        public getSynchronizeChangesWithDbData(x:Entities.ParcelClas):any {
            var self = this;
            var paramData:any = {
                data: <ChangeToCommit[]>[]
            };
            var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);

            paramData.data.push(changeToCommit);
            return paramData;
        }

        public deleteRecord(x:Entities.ParcelClas):void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);

            console.log("deleting ParcelClas with PK field:" + x.pclaId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                    self.updateGrid();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        }
        
        public _newIsDisabled():boolean  {
            return true;
        }
        public _searchIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _deleteIsDisabled(parcelClas:Entities.ParcelClas):boolean  {
            return true;
        }
        public _editBtnIsDisabled(parcelClas:Entities.ParcelClas):boolean {
            var self = this;
            if (parcelClas === undefined || parcelClas === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_R"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public d__parcIdentifier_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public d__parcCode_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public d__prodCode_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public d__clasId_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_d__clasId:ModelLovClassificationBase;
        public showLov_d__clasId() {
            var self = this;
            var uimodel:NpTypes.IUIModel = self.pageModel._clasId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Data Import Name';
            dialogOptions.previousModel= self.cachedLovModel_d__clasId;
            dialogOptions.className = "ControllerLovClassification";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var classification1:Entities.Classification =   <Entities.Classification>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(classification1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(classification1) && classification1.isEqual(self.pageModel.clasId))
                    return;
                self.pageModel.clasId = classification1;
            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovClassificationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_d__clasId = lovModel;
                    var wsPath = "Classification/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovClassificationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Classification/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;
            dialogOptions.shownCols['dateTime'] = true;
            dialogOptions.shownCols['recordtype'] = true;
            dialogOptions.shownCols['filePath'] = true;
            dialogOptions.shownCols['attachedFile'] = true;
            dialogOptions.shownCols['year'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Classification.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }


    }
}
