/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
/// <reference path="../Controllers/EcGroupSearch.ts" />
var Controllers;
(function (Controllers) {
    var ModelEcGroupSearchBase = (function (_super) {
        __extends(ModelEcGroupSearchBase, _super);
        function ModelEcGroupSearchBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._name = new NpTypes.UIStringModel(undefined);
            this._description = new NpTypes.UIStringModel(undefined);
        }
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (vl) {
                this._name.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "description", {
            get: function () {
                return this._description.value;
            },
            set: function (vl) {
                this._description.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelEcGroupSearchBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelEcGroupSearchBase;
    })(Controllers.AbstractSearchPageModel);
    Controllers.ModelEcGroupSearchBase = ModelEcGroupSearchBase;
    var ModelEcGroupSearchPersistedModel = (function () {
        function ModelEcGroupSearchPersistedModel() {
        }
        return ModelEcGroupSearchPersistedModel;
    })();
    Controllers.ModelEcGroupSearchPersistedModel = ModelEcGroupSearchPersistedModel;
    var ControllerEcGroupSearchBase = (function (_super) {
        __extends(ControllerEcGroupSearchBase, _super);
        function ControllerEcGroupSearchBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'BRE Rules Sets';
            var lastSearchPageModel = $scope.getPageModelByURL('/EcGroupSearch');
            var rowIndexToSelect = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.name = lastSearchPageModel.name;
                self.pageModel.description = lastSearchPageModel.description;
            }
            $scope._newIsDisabled =
                function () {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled =
                function () {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled =
                function (ecGroup) {
                    return self._deleteIsDisabled(ecGroup);
                };
            $scope._editBtnIsDisabled =
                function (ecGroup) {
                    return self._editBtnIsDisabled(ecGroup);
                };
            $scope._cancelIsDisabled =
                function () {
                    return self._cancelIsDisabled();
                };
            $scope.d__name_disabled =
                function () {
                    return self.d__name_disabled();
                };
            $scope.d__description_disabled =
                function () {
                    return self.d__description_disabled();
                };
            $scope.clearBtnAction = function () {
                self.pageModel.name = undefined;
                self.pageModel.description = undefined;
                self.updateGrid(0, false, true);
            };
            $scope.newBtnAction = function () {
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };
            $scope.onEdit = function (x) {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };
            $timeout(function () {
                $('#Search_EcGroup').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);
            self.updateUI(rowIndexToSelect);
        }
        ControllerEcGroupSearchBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerEcGroupSearchBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerEcGroupSearch";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerEcGroupSearchBase.prototype, "HtmlDivId", {
            get: function () {
                return "Search_EcGroup";
            },
            enumerable: true,
            configurable: true
        });
        ControllerEcGroupSearchBase.prototype._getPageTitle = function () {
            return "Eligibility Criteria";
        };
        ControllerEcGroupSearchBase.prototype.gridColumnFilter = function (field) {
            return true;
        };
        ControllerEcGroupSearchBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            return [
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { cellClass: 'cellToolTip', field: 'name', displayName: 'getALString("Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.name' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'description', displayName: 'getALString("Description", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.description' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
        };
        ControllerEcGroupSearchBase.prototype.getPersistedModel = function () {
            var self = this;
            var ret = new ModelEcGroupSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.name = self.pageModel.name;
            ret.description = self.pageModel.description;
            return ret;
        };
        Object.defineProperty(ControllerEcGroupSearchBase.prototype, "pageModel", {
            get: function () {
                return this.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerEcGroupSearchBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.EcGroup.fromJSONComplete(webResponse.data);
        };
        ControllerEcGroupSearchBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "EcGroup/findLazyEcGroup";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.description)) {
                paramData['description'] = self.pageModel.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.name)) {
                paramData['name'] = self.pageModel.name;
            }
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerEcGroupSearchBase.prototype.makeWebRequest_count = function (excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "EcGroup/findLazyEcGroup_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.description)) {
                paramData['description'] = self.pageModel.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.name)) {
                paramData['name'] = self.pageModel.name;
            }
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerEcGroupSearchBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var paramData = {};
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.description)) {
                paramData['description'] = self.pageModel.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.name)) {
                paramData['name'] = self.pageModel.name;
            }
            var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
            return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        };
        Object.defineProperty(ControllerEcGroupSearchBase.prototype, "Current", {
            get: function () {
                var self = this;
                return self.$scope.pageModel.selectedEntities[0];
            },
            enumerable: true,
            configurable: true
        });
        ControllerEcGroupSearchBase.prototype.onNew = function () {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/EcGroup', self._getPageTitle(), {});
        };
        ControllerEcGroupSearchBase.prototype.onEdit = function (x) {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel = { "selectedEntity": x };
            self.$scope.navigateForward('/EcGroup', self._getPageTitle(), visitedPageModel);
        };
        ControllerEcGroupSearchBase.prototype.getSynchronizeChangesWithDbUrl = function () {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_EcGroup";
        };
        ControllerEcGroupSearchBase.prototype.getSynchronizeChangesWithDbData = function (x) {
            var self = this;
            var paramData = {
                data: []
            };
            var changeToCommit = new Controllers.ChangeToCommit(Controllers.ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);
            paramData.data.push(changeToCommit);
            return paramData;
        };
        ControllerEcGroupSearchBase.prototype.deleteRecord = function (x) {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
            console.log("deleting EcGroup with PK field:" + x.ecgrId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                self.updateGrid();
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        };
        ControllerEcGroupSearchBase.prototype._newIsDisabled = function () {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerEcGroupSearchBase.prototype._searchIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerEcGroupSearchBase.prototype._deleteIsDisabled = function (ecGroup) {
            var self = this;
            if (ecGroup === undefined || ecGroup === null)
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerEcGroupSearchBase.prototype._editBtnIsDisabled = function (ecGroup) {
            var self = this;
            if (ecGroup === undefined || ecGroup === null)
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_R"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerEcGroupSearchBase.prototype._cancelIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerEcGroupSearchBase.prototype.d__name_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerEcGroupSearchBase.prototype.d__description_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        return ControllerEcGroupSearchBase;
    })(Controllers.AbstractSearchPageController);
    Controllers.ControllerEcGroupSearchBase = ControllerEcGroupSearchBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=EcGroupSearchBase.js.map