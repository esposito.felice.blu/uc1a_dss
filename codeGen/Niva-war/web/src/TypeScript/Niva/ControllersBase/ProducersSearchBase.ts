/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />

// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ProducersBase.ts" />
/// <reference path="../Controllers/ProducersSearch.ts" />

module Controllers {

    export class ModelProducersSearchBase extends Controllers.AbstractSearchPageModel {
        _prodName:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get prodName():string {
            return this._prodName.value;
        }
        public set prodName(vl:string) {
            this._prodName.value = vl;
        }
        _prodCode:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get prodCode():number {
            return this._prodCode.value;
        }
        public set prodCode(vl:number) {
            this._prodCode.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        controller: ControllerProducersSearch;
        constructor(public $scope: IScopeProducersSearch) { super($scope); }
    }
    
    export class ModelProducersSearchPersistedModel implements NpTypes.IBreadcrumbStepModel {
        _pageSize: number;
        _currentPage: number;
        _currentRow: number;
        sortField: string;
        sortOrder: string;
        prodName:string;
        prodCode:number;
    }



    export interface IScopeProducersSearchBase extends Controllers.IAbstractSearchPageScope {
        globals: Globals;
        pageModel : ModelProducersSearch;
        _newIsDisabled():boolean; 
        _searchIsDisabled():boolean; 
        _deleteIsDisabled(producers:Entities.Producers):boolean; 
        _editBtnIsDisabled(producers:Entities.Producers):boolean; 
        _cancelIsDisabled():boolean; 
        d__prodName_disabled():boolean; 
        d__prodCode_disabled():boolean; 
    }

    export class ControllerProducersSearchBase extends Controllers.AbstractSearchPageController {
        constructor(
            public $scope: IScopeProducersSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:ModelProducersSearch)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self: ControllerProducersSearchBase = this;
            model.controller = <ControllerProducersSearch>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'Producers (Farmers)';
            
            var lastSearchPageModel = <ModelProducersSearchPersistedModel>$scope.getPageModelByURL('/ProducersSearch');
            var rowIndexToSelect: number = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.prodName = lastSearchPageModel.prodName;
                self.pageModel.prodCode = lastSearchPageModel.prodCode;
            }


            $scope._newIsDisabled = 
                () => {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled = 
                () => {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled = 
                (producers:Entities.Producers) => {
                    return self._deleteIsDisabled(producers);
                };
            $scope._editBtnIsDisabled = 
                (producers:Entities.Producers) => {
                    return self._editBtnIsDisabled(producers);
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };
            $scope.d__prodName_disabled = 
                () => {
                    return self.d__prodName_disabled();
                };
            $scope.d__prodCode_disabled = 
                () => {
                    return self.d__prodCode_disabled();
                };

            $scope.clearBtnAction = () => { 
                self.pageModel.prodName = undefined;
                self.pageModel.prodCode = undefined;
                self.updateGrid(0, false, true);
            };

            $scope.newBtnAction = () => { 
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };

            $scope.onEdit = (x:Entities.Producers):void => {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };

            $timeout(function() {
                $('#Search_Producers').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);

            self.updateUI(rowIndexToSelect);
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerProducersSearch";
        }
        public get HtmlDivId(): string {
            return "Search_Producers";
        }
    
        public _getPageTitle(): string {
            return "Producers";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined},
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'prodName', displayName:'getALString("Full Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.prodName' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'prodCode', displayName:'getALString("Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.prodCode.toStringFormatted(\",\", \"\", 0)' style='width: 100%;text-align: right;padding-right: 6px;' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }


        public getPersistedModel():ModelProducersSearchPersistedModel {
            var self = this;
            var ret = new ModelProducersSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.prodName = self.pageModel.prodName;
            ret.prodCode = self.pageModel.prodCode;
            return ret;
        }

        public get pageModel():ModelProducersSearch {
            return <ModelProducersSearch>this.model;
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            return Entities.Producers.fromJSONComplete(webResponse.data);
        }

        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = "Producers/findLazyProducers";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};

            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;

            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodName)) {
                paramData['prodName'] = self.pageModel.prodName;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodCode)) {
                paramData['prodCode'] = self.pageModel.prodCode;
            }

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(
                url,
                paramData,
                {timeout:self.$scope.globals.timeoutInMS, cache:false});
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });

        }

        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = "Producers/findLazyProducers_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodName)) {
                paramData['prodName'] = self.pageModel.prodName;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodCode)) {
                paramData['prodCode'] = self.pageModel.prodCode;
            }
            
            var promise = self.$http.post(
                url,
                paramData,
                { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        }

        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
            var paramData = {};

            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodName)) {
                paramData['prodName'] = self.pageModel.prodName;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodCode)) {
                paramData['prodCode'] = self.pageModel.prodCode;
            }
                
            var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
            return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        }


        public get Current():Entities.Producers {
            var self = this;
            return <Entities.Producers>self.$scope.pageModel.selectedEntities[0];
        }

        public onNew():void {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/Producers', self._getPageTitle(), {});
        }

        public onEdit(x:Entities.Producers):void {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel:IFormPageBreadcrumbStepModel<Entities.Producers>= {"selectedEntity":x};
            self.$scope.navigateForward('/Producers', self._getPageTitle(), visitedPageModel);
        }
        
        private getSynchronizeChangesWithDbUrl(): string {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_Producers";
        }
        public getSynchronizeChangesWithDbData(x:Entities.Producers):any {
            var self = this;
            var paramData:any = {
                data: <ChangeToCommit[]>[]
            };
            var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);

            paramData.data.push(changeToCommit);
            return paramData;
        }

        public deleteRecord(x:Entities.Producers):void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);

            console.log("deleting Producers with PK field:" + x.producersId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                    self.updateGrid();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        }
        
        public _newIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_Producers_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _searchIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _deleteIsDisabled(producers:Entities.Producers):boolean {
            var self = this;
            if (producers === undefined || producers === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Producers_D"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _editBtnIsDisabled(producers:Entities.Producers):boolean {
            var self = this;
            if (producers === undefined || producers === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Producers_R"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public d__prodName_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public d__prodCode_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }

    }
}
