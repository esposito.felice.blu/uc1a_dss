/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
/// <reference path="../Controllers/LovDecisionMaking.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovDecisionMakingBase = (function (_super) {
        __extends(ModelLovDecisionMakingBase, _super);
        function ModelLovDecisionMakingBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._fsch_DecisionMakingLov_description = new NpTypes.UIStringModel(undefined);
            this._fsch_DecisionMakingLov_dateTime = new NpTypes.UIDateModel(undefined);
        }
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "fsch_DecisionMakingLov_description", {
            get: function () {
                return this._fsch_DecisionMakingLov_description.value;
            },
            set: function (vl) {
                this._fsch_DecisionMakingLov_description.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "fsch_DecisionMakingLov_dateTime", {
            get: function () {
                return this._fsch_DecisionMakingLov_dateTime.value;
            },
            set: function (vl) {
                this._fsch_DecisionMakingLov_dateTime.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovDecisionMakingBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelLovDecisionMakingBase;
    })(Controllers.AbstractLovModel);
    Controllers.ModelLovDecisionMakingBase = ModelLovDecisionMakingBase;
    var ControllerLovDecisionMakingBase = (function (_super) {
        __extends(ControllerLovDecisionMakingBase, _super);
        function ControllerLovDecisionMakingBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 5, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            $scope.clearBtnAction = function () {
                self.model.fsch_DecisionMakingLov_description = undefined;
                self.model.fsch_DecisionMakingLov_dateTime = undefined;
                self.updateGrid();
            };
            $scope._disabled =
                function () {
                    return self._disabled();
                };
            $scope._invisible =
                function () {
                    return self._invisible();
                };
            $scope.LovDecisionMaking_fsch_DecisionMakingLov_description_disabled =
                function () {
                    return self.LovDecisionMaking_fsch_DecisionMakingLov_description_disabled();
                };
            $scope.LovDecisionMaking_fsch_DecisionMakingLov_dateTime_disabled =
                function () {
                    return self.LovDecisionMaking_fsch_DecisionMakingLov_dateTime_disabled();
                };
            $scope.modelLovDecisionMaking = model;
            self.updateUI();
        }
        ControllerLovDecisionMakingBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerLovDecisionMakingBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerLovDecisionMaking";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerLovDecisionMakingBase.prototype, "HtmlDivId", {
            get: function () {
                return "DecisionMaking_Id";
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovDecisionMakingBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName);
            }
            var ret = [
                { cellClass: 'cellToolTip', field: 'description', displayName: 'getALString("Description", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '39%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.description' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'dateTime', displayName: 'getALString("Date", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '15%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <input data-ng-model='row.entity.dateTime' data-np-date='dummy' data-ng-readonly='true' data-np-format='dd-MM-yyyy' data-np-ui-model='row.entity._dateTime' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'ecgrId.name', displayName: 'getALString("Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '39%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.ecgrId.name' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter((function (cl) { return cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true; }));
            if (self.isMultiSelect()) {
                ret.splice(0, 0, { width: '22', cellTemplate: "<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined });
            }
            return ret;
        };
        ControllerLovDecisionMakingBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.DecisionMaking.fromJSONComplete(webResponse.data);
        };
        Object.defineProperty(ControllerLovDecisionMakingBase.prototype, "modelLovDecisionMaking", {
            get: function () {
                var self = this;
                return self.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovDecisionMakingBase.prototype.initializeModelWithPreviousValues = function () {
            var self = this;
            var prevModel = self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovDecisionMaking.fsch_DecisionMakingLov_description = prevModel.fsch_DecisionMakingLov_description;
                self.modelLovDecisionMaking.fsch_DecisionMakingLov_dateTime = prevModel.fsch_DecisionMakingLov_dateTime;
            }
        };
        ControllerLovDecisionMakingBase.prototype._disabled = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovDecisionMakingBase.prototype._invisible = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovDecisionMakingBase.prototype.LovDecisionMaking_fsch_DecisionMakingLov_description_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovDecisionMakingBase.prototype.LovDecisionMaking_fsch_DecisionMakingLov_dateTime_disabled = function () {
            var self = this;
            return false;
        };
        return ControllerLovDecisionMakingBase;
    })(Controllers.LovController);
    Controllers.ControllerLovDecisionMakingBase = ControllerLovDecisionMakingBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovDecisionMakingBase.js.map