var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />
/// <reference path="../EntitiesBase/GpDecisionBase.ts" />
/// <reference path="../EntitiesBase/GpRequestsContextsBase.ts" />
/// <reference path="../EntitiesBase/GpUploadBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="LovCoverTypeBase.ts" />
/// <reference path="../Controllers/ParcelGP.ts" />
var Controllers;
(function (Controllers) {
    var ParcelGP;
    (function (ParcelGP) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        ParcelGP.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $scope.onNewBtnAction = function () {
                    self.onNewBtnAction();
                };
                $scope.onDeleteBtnAction = function () {
                    self.onDeleteBtnAction();
                };
                $timeout(function () {
                    $('#ParcelGP').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelGP";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Parcels Geotagged Photos Decisions";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpParcelClas.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpParcelClas.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelsIssues.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpRequestsContexts.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpUpload.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpParcelClas.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelsIssues.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpRequestsContexts.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpUpload.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpParcelClas.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpParcelClas.controller,
                            self.model.modelGrpParcelsIssues.controller,
                            self.model.modelGrpGpDecision.controller,
                            self.model.modelGrpGpRequestsContexts.controller,
                            self.model.modelGrpGpUpload.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getChangesToCommitForDeletion());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpRequestsContexts.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpUpload.controller.getDeleteChangesToCommit());
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpRequestsContexts.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpUpload.controller.getChangesToCommit());
                }
                var hasParcelClas = pageChanges.some(function (change) { return change.entityName === "ParcelClas"; });
                if (!hasParcelClas) {
                    var validateEntity = new Controllers.ChangeToCommit(Controllers.ChangeStatus.UPDATE, 0, "ParcelClas", self.model.modelGrpParcelClas.controller.Current);
                    pageChanges = pageChanges.concat(validateEntity);
                }
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_ParcelGP";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpParcelClas.controller.cleanUpAfterSave();
                self.model.modelGrpParcelsIssues.controller.cleanUpAfterSave();
                self.model.modelGrpGpDecision.controller.cleanUpAfterSave();
                self.model.modelGrpGpRequestsContexts.controller.cleanUpAfterSave();
                self.model.modelGrpGpUpload.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = self.$scope.getCurrentPageModel();
                        var newParcelClasId = response.newEntitiesIds.firstOrNull(function (x) { return x.entityName === 'ParcelClas'; });
                        if (!isVoid(newParcelClasId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.ParcelClas.CreateById(newParcelClasId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.getInitialEntity = function () {
                if (this.shownAsDialog()) {
                    return this.dialogSelectedEntity();
                }
                else {
                    var breadCrumbStepModel = this.$scope.getPageModelByURL('/ParcelGP');
                    if (isVoid(breadCrumbStepModel)) {
                        return null;
                    }
                    else {
                        return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                    }
                }
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            PageControllerBase.prototype.onNewBtnAction = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                self.addNewRecord();
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () { self.addNewRecord(); }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    self.addNewRecord();
                }
            };
            PageControllerBase.prototype.addNewRecord = function () {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                self.model.modelGrpParcelClas.controller.cleanUpAfterSave();
                self.model.modelGrpParcelsIssues.controller.cleanUpAfterSave();
                self.model.modelGrpGpDecision.controller.cleanUpAfterSave();
                self.model.modelGrpGpRequestsContexts.controller.cleanUpAfterSave();
                self.model.modelGrpGpUpload.controller.cleanUpAfterSave();
                self.model.modelGrpParcelClas.controller.createNewEntityAndAddToUI();
            };
            PageControllerBase.prototype.onDeleteBtnAction = function () {
                var self = this;
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                    new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(); }),
                    new Tuple2("MessageBox_Button_No", function () { })
                ], 1, 1);
            };
            PageControllerBase.prototype.deleteRecord = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW) {
                    console.log("Delete pressed in a form while being in new mode!");
                    return;
                }
                NpTypes.AlertMessage.clearAlerts(self.model);
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData(true);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "Mode", {
                get: function () {
                    var self = this;
                    if (isVoid(self.model.modelGrpParcelClas) || isVoid(self.model.modelGrpParcelClas.controller))
                        return Controllers.EditMode.NEW;
                    return self.model.modelGrpParcelClas.controller.Mode;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._newIsDisabled = function () {
                var self = this;
                if (isVoid(self.model.modelGrpParcelClas) || isVoid(self.model.modelGrpParcelClas.controller))
                    return true;
                return self.model.modelGrpParcelClas.controller._newIsDisabled();
            };
            PageControllerBase.prototype._deleteIsDisabled = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW)
                    return true;
                if (isVoid(self.model.modelGrpParcelClas) || isVoid(self.model.modelGrpParcelClas.controller))
                    return true;
                return self.model.modelGrpParcelClas.controller._deleteIsDisabled(self.model.modelGrpParcelClas.controller.Current);
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        ParcelGP.PageControllerBase = PageControllerBase;
        // GROUP GrpParcelClas
        var ModelGrpParcelClasBase = (function (_super) {
            __extends(ModelGrpParcelClasBase, _super);
            function ModelGrpParcelClasBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].pclaId;
                },
                set: function (pclaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].pclaId = pclaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "probPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probPred;
                },
                set: function (probPred_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probPred = probPred_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_probPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probPred;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "probPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probPred2;
                },
                set: function (probPred2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probPred2 = probPred2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_probPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probPred2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "prodCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].prodCode;
                },
                set: function (prodCode_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].prodCode = prodCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_prodCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._prodCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "parcIdentifier", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcIdentifier;
                },
                set: function (parcIdentifier_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcIdentifier = parcIdentifier_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_parcIdentifier", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcIdentifier;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "parcCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcCode;
                },
                set: function (parcCode_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcCode = parcCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_parcCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "geom4326", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].geom4326;
                },
                set: function (geom4326_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].geom4326 = geom4326_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_geom4326", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._geom4326;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].clasId;
                },
                set: function (clasId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].clasId = clasId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._clasId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cultIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultIdDecl;
                },
                set: function (cultIdDecl_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultIdDecl = cultIdDecl_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cultIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultIdDecl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cultIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultIdPred;
                },
                set: function (cultIdPred_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultIdPred = cultIdPred_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cultIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultIdPred;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cotyIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyIdDecl;
                },
                set: function (cotyIdDecl_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyIdDecl = cotyIdDecl_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cotyIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyIdDecl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cotyIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyIdPred;
                },
                set: function (cotyIdPred_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyIdPred = cotyIdPred_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cotyIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyIdPred;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cultIdPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultIdPred2;
                },
                set: function (cultIdPred2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultIdPred2 = cultIdPred2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cultIdPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultIdPred2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpParcelClasBase;
        })(Controllers.AbstractGroupFormModel);
        ParcelGP.ModelGrpParcelClasBase = ModelGrpParcelClasBase;
        var ControllerGrpParcelClasBase = (function (_super) {
            __extends(ControllerGrpParcelClasBase, _super);
            function ControllerGrpParcelClasBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                $scope.modelGrpParcelClas = self.model;
                var selectedEntity = this.$scope.pageModel.controller.getInitialEntity();
                if (isVoid(selectedEntity)) {
                    self.createNewEntityAndAddToUI();
                }
                else {
                    var clonedEntity = Entities.ParcelClas.Create();
                    clonedEntity.updateInstance(selectedEntity);
                    $scope.modelGrpParcelClas.visibleEntities[0] = clonedEntity;
                    $scope.modelGrpParcelClas.selectedEntities[0] = clonedEntity;
                }
                $scope.GrpParcelClas_0_disabled =
                    function () {
                        return self.GrpParcelClas_0_disabled();
                    };
                $scope.GrpParcelClas_0_invisible =
                    function () {
                        return self.GrpParcelClas_0_invisible();
                    };
                $scope.GrpParcelClas_0_1_disabled =
                    function () {
                        return self.GrpParcelClas_0_1_disabled();
                    };
                $scope.GrpParcelClas_0_1_invisible =
                    function () {
                        return self.GrpParcelClas_0_1_invisible();
                    };
                $scope.child_group_GrpParcelsIssues_isInvisible =
                    function () {
                        return self.child_group_GrpParcelsIssues_isInvisible();
                    };
                $scope.pageModel.modelGrpParcelClas = $scope.modelGrpParcelClas;
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelClas[] , oldVisible:Entities.ParcelClas[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                //bind entity child collection with child controller merged items
                Entities.ParcelClas.parcelsIssuesCollection = function (parcelClas, func) {
                    _this.model.modelGrpParcelsIssues.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelClas);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelClas.parcelsIssuesCollection = null; //(parcelClas, func) => { }
                });
            }
            ControllerGrpParcelClasBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpParcelClas";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelGP_ControllerGrpParcelClas";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Parcel Identifier'; }, false, function (ent) { return ent.parcIdentifier; }, function (ent) { return ent._parcIdentifier; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'Parcel Code'; }, false, function (ent) { return ent.parcCode; }, function (ent) { return ent._parcCode; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.NumberItem(function (ent) { return 'Producer Code'; }, false, function (ent) { return ent.prodCode; }, function (ent) { return ent._prodCode; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "modelGrpParcelClas", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.getEntityName = function () {
                return "ParcelClas";
            };
            ControllerGrpParcelClasBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
            };
            ControllerGrpParcelClasBase.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
                var self = this;
                if (self.$scope.pageModel.controller.shownAsDialog())
                    return;
                var breadCrumbStepModel = { selectedEntity: null };
                if (!isVoid(self.Current)) {
                    var selectedEntity = Entities.ParcelClas.Create();
                    selectedEntity.updateInstance(self.Current);
                    breadCrumbStepModel.selectedEntity = selectedEntity;
                }
                else {
                    breadCrumbStepModel.selectedEntity = null;
                }
                self.$scope.setCurrentPageModel(breadCrumbStepModel);
            };
            ControllerGrpParcelClasBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.ParcelClas.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpParcelClasBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.ParcelClas(
                /*pclaId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*probPred:number*/ null, 
                /*probPred2:number*/ null, 
                /*prodCode:number*/ null, 
                /*parcIdentifier:string*/ null, 
                /*parcCode:string*/ null, 
                /*geom4326:ol.Feature*/ null, 
                /*clasId:Entities.Classification*/ null, 
                /*cultIdDecl:Entities.Cultivation*/ null, 
                /*cultIdPred:Entities.Cultivation*/ null, 
                /*cotyIdDecl:Entities.CoverType*/ null, 
                /*cotyIdPred:Entities.CoverType*/ null, 
                /*cultIdPred2:Entities.Cultivation*/ null);
                return ret;
            };
            ControllerGrpParcelClasBase.prototype.createNewEntityAndAddToUI = function () {
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.$scope.modelGrpParcelClas.visibleEntities[0] = newEnt;
                self.$scope.modelGrpParcelClas.selectedEntities[0] = newEnt;
                return newEnt;
            };
            ControllerGrpParcelClasBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpParcelClasBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.ParcelClas.Create();
                ret.updateInstance(src);
                ret.pclaId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.visibleEntities[0] = ret;
                this.model.selectedEntities[0] = ret;
                this.$timeout(function () {
                    _this.modelGrpParcelClas.modelGrpParcelsIssues.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpParcelClas.modelGrpParcelsIssues.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpParcelClas.modelGrpParcelsIssues.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpParcelClas.modelGrpParcelsIssues.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_1_disabled = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_1_invisible = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpParcelClasBase.prototype._deleteIsDisabled = function (parcelClas) {
                return true;
            };
            ControllerGrpParcelClasBase.prototype.child_group_GrpParcelsIssues_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpParcelsIssues === undefined) || (self.model.modelGrpParcelsIssues.controller === undefined))
                    return false;
                return self.model.modelGrpParcelsIssues.controller._isInvisible();
            };
            return ControllerGrpParcelClasBase;
        })(Controllers.AbstractGroupFormController);
        ParcelGP.ControllerGrpParcelClasBase = ControllerGrpParcelClasBase;
        // GROUP GrpParcelsIssues
        var ModelGrpParcelsIssuesBase = (function (_super) {
            __extends(ModelGrpParcelsIssuesBase, _super);
            function ModelGrpParcelsIssuesBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_status = new NpTypes.UINumberModel(undefined);
                this._fsch_dteStatusUpdate = new NpTypes.UIDateModel(undefined);
                this._fsch_dteCreated = new NpTypes.UIDateModel(undefined);
                this._type = new NpTypes.UINumberModel(undefined);
            }
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesId;
                },
                set: function (parcelsIssuesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesId = parcelsIssuesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "dteCreated", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteCreated;
                },
                set: function (dteCreated_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteCreated = dteCreated_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_dteCreated", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteCreated;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "status", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].status;
                },
                set: function (status_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].status = status_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_status", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._status;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "dteStatusUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteStatusUpdate;
                },
                set: function (dteStatusUpdate_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteStatusUpdate = dteStatusUpdate_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_dteStatusUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteStatusUpdate;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "typeOfIssue", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].typeOfIssue;
                },
                set: function (typeOfIssue_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].typeOfIssue = typeOfIssue_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_typeOfIssue", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._typeOfIssue;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "active", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].active;
                },
                set: function (active_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].active = active_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_active", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._active;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].pclaId;
                },
                set: function (pclaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].pclaId = pclaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "fsch_status", {
                get: function () {
                    return this._fsch_status.value;
                },
                set: function (vl) {
                    this._fsch_status.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "fsch_dteStatusUpdate", {
                get: function () {
                    return this._fsch_dteStatusUpdate.value;
                },
                set: function (vl) {
                    this._fsch_dteStatusUpdate.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "fsch_dteCreated", {
                get: function () {
                    return this._fsch_dteCreated.value;
                },
                set: function (vl) {
                    this._fsch_dteCreated.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "type", {
                get: function () {
                    return this._type.value;
                },
                set: function (vl) {
                    this._type.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpParcelsIssuesBase;
        })(Controllers.AbstractGroupTableModel);
        ParcelGP.ModelGrpParcelsIssuesBase = ModelGrpParcelsIssuesBase;
        var ControllerGrpParcelsIssuesBase = (function (_super) {
            __extends(ControllerGrpParcelsIssuesBase, _super);
            function ControllerGrpParcelsIssuesBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpParcelsIssues = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpParcelsIssues_itm__status_disabled =
                    function (parcelsIssues) {
                        return self.GrpParcelsIssues_itm__status_disabled(parcelsIssues);
                    };
                $scope.GrpParcelsIssues_itm__dteStatusUpdate_disabled =
                    function (parcelsIssues) {
                        return self.GrpParcelsIssues_itm__dteStatusUpdate_disabled(parcelsIssues);
                    };
                $scope.GrpParcelsIssues_itm__dteCreated_disabled =
                    function (parcelsIssues) {
                        return self.GrpParcelsIssues_itm__dteCreated_disabled(parcelsIssues);
                    };
                $scope.child_group_GrpGpDecision_isInvisible =
                    function () {
                        return self.child_group_GrpGpDecision_isInvisible();
                    };
                $scope.child_group_GrpGpRequestsContexts_isInvisible =
                    function () {
                        return self.child_group_GrpGpRequestsContexts_isInvisible();
                    };
                $scope.pageModel.modelGrpParcelsIssues = $scope.modelGrpParcelsIssues;
                $scope.clearBtnAction = function () {
                    self.modelGrpParcelsIssues.fsch_status = undefined;
                    self.modelGrpParcelsIssues.fsch_dteStatusUpdate = undefined;
                    self.modelGrpParcelsIssues.fsch_dteCreated = undefined;
                    self.modelGrpParcelsIssues.type = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelClas.modelGrpParcelsIssues = $scope.modelGrpParcelsIssues;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelClas.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelsIssues[] , oldVisible:Entities.ParcelsIssues[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.ParcelsIssues.gpDecisionCollection = function (parcelsIssues, func) {
                    _this.model.modelGrpGpDecision.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelsIssues);
                };
                //bind entity child collection with child controller merged items
                Entities.ParcelsIssues.gpRequestsContextsCollection = function (parcelsIssues, func) {
                    _this.model.modelGrpGpRequestsContexts.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelsIssues);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelsIssues.gpDecisionCollection = null; //(parcelsIssues, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelsIssues.gpRequestsContextsCollection = null; //(parcelsIssues, func) => { }
                });
            }
            ControllerGrpParcelsIssuesBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpParcelsIssues";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelGP_ControllerGrpParcelsIssues";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.NumberItem(function (ent) { return 'Current Status'; }, true, function (ent) { return self.model.fsch_status; }, function (ent) { return self.model._fsch_status; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, function () { return 0; }, function () { return 32767; }, 0),
                            new Controllers.DateItem(function (ent) { return 'Date Updated'; }, true, function (ent) { return self.model.fsch_dteStatusUpdate; }, function (ent) { return self.model._fsch_dteStatusUpdate; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.DateItem(function (ent) { return 'Date Created'; }, true, function (ent) { return self.model.fsch_dteCreated; }, function (ent) { return self.model._fsch_dteCreated; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.StaticListItem(function (ent) { return 'Type'; }, true, function (ent) { return self.model.type; }, function (ent) { return self.model._type; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Current Status'; }, false, function (ent) { return ent.status; }, function (ent) { return ent._status; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, function () { return 0; }, function () { return 999; }, 0),
                            new Controllers.DateItem(function (ent) { return 'Date Updated'; }, false, function (ent) { return ent.dteStatusUpdate; }, function (ent) { return ent._dteStatusUpdate; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.DateItem(function (ent) { return 'Date Created'; }, false, function (ent) { return ent.dteCreated; }, function (ent) { return ent._dteCreated; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.gridTitle = function () {
                return "Parcel\'s Issues";
            };
            ControllerGrpParcelsIssuesBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpParcelsIssuesBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'status', displayName: 'getALString("Current Status", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssues_itm__status' data-ng-model='row.entity.status' data-np-ui-model='row.entity._status' data-ng-change='markEntityAsUpdated(row.entity,&quot;status&quot;)' data-ng-readonly='GrpParcelsIssues_itm__status_disabled(row.entity)' data-np-number='dummy' data-np-min='0' data-np-max='999' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteStatusUpdate', displayName: 'getALString("Date Updated", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssues_itm__dteStatusUpdate' data-ng-model='row.entity.dteStatusUpdate' data-np-ui-model='row.entity._dteStatusUpdate' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteStatusUpdate&quot;)' data-ng-readonly='GrpParcelsIssues_itm__dteStatusUpdate_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteCreated', displayName: 'getALString("Date Created", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssues_itm__dteCreated' data-ng-model='row.entity.dteCreated' data-np-ui-model='row.entity._dteCreated' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteCreated&quot;)' data-ng-readonly='GrpParcelsIssues_itm__dteCreated_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "modelGrpParcelsIssues", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.getEntityName = function () {
                return "ParcelsIssues";
            };
            ControllerGrpParcelsIssuesBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpParcelsIssuesBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.ParcelsIssues.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.pclaId = _this.Parent;
                    }
                    else {
                        x.pclaId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelClas.controller.isEntityLocked(cur.pclaId, lockKind);
            };
            ControllerGrpParcelsIssuesBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_dteCreated)) {
                    paramData['fsch_dteCreated'] = self.modelGrpParcelsIssues.fsch_dteCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_status)) {
                    paramData['fsch_status'] = self.modelGrpParcelsIssues.fsch_status;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_dteStatusUpdate)) {
                    paramData['fsch_dteStatusUpdate'] = self.modelGrpParcelsIssues.fsch_dteStatusUpdate;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpParcelsIssuesBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelsIssues/findAllByCriteriaRange_ParcelGPGrpParcelsIssues_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_dteCreated)) {
                    paramData['fsch_dteCreated'] = self.modelGrpParcelsIssues.fsch_dteCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_status)) {
                    paramData['fsch_status'] = self.modelGrpParcelsIssues.fsch_status;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_dteStatusUpdate)) {
                    paramData['fsch_dteStatusUpdate'] = self.modelGrpParcelsIssues.fsch_dteStatusUpdate;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelsIssuesBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelsIssues/findAllByCriteriaRange_ParcelGPGrpParcelsIssues";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_dteCreated)) {
                    paramData['fsch_dteCreated'] = self.modelGrpParcelsIssues.fsch_dteCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_status)) {
                    paramData['fsch_status'] = self.modelGrpParcelsIssues.fsch_status;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_dteStatusUpdate)) {
                    paramData['fsch_dteStatusUpdate'] = self.modelGrpParcelsIssues.fsch_dteStatusUpdate;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelsIssuesBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelsIssues/findAllByCriteriaRange_ParcelGPGrpParcelsIssues_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_dteCreated)) {
                    paramData['fsch_dteCreated'] = self.modelGrpParcelsIssues.fsch_dteCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_status)) {
                    paramData['fsch_status'] = self.modelGrpParcelsIssues.fsch_status;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssues) && !isVoid(self.modelGrpParcelsIssues.fsch_dteStatusUpdate)) {
                    paramData['fsch_dteStatusUpdate'] = self.modelGrpParcelsIssues.fsch_dteStatusUpdate;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelsIssuesBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.pclaId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "ParcelsIssues/findAllByCriteriaRange_ParcelGPGrpParcelsIssues";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['pclaId_pclaId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpParcelsIssuesBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.pclaId === undefined)
                    return false;
                return x.pclaId.getKey() === this.Parent.getKey();
            };
            ControllerGrpParcelsIssuesBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "pclaId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.ParcelsIssues(
                /*parcelsIssuesId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*dteCreated:Date*/ null, 
                /*status:number*/ null, 
                /*dteStatusUpdate:Date*/ null, 
                /*rowVersion:number*/ null, 
                /*typeOfIssue:number*/ null, 
                /*active:boolean*/ null, 
                /*pclaId:Entities.ParcelClas*/ null);
                return ret;
            };
            ControllerGrpParcelsIssuesBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.pclaId = self.pclaId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpParcelsIssuesBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpParcelsIssuesBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.ParcelsIssues.Create();
                ret.updateInstance(src);
                ret.parcelsIssuesId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelGrpParcelsIssues.modelGrpGpDecision.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerGrpParcelsIssuesBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (parcelsIssuesList) {
                    parcelsIssuesList.forEach(function (parcelsIssues) {
                        var parcelsIssuesCloned = _this.cloneEntity_internal(parcelsIssues, true);
                        parcelsIssuesCloned.pclaId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpParcelsIssuesBase.prototype.deleteNewEntitiesUnderParent = function (parcelClas) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var parcelsIssues = change.a;
                    if (parcelClas.getKey() === parcelsIssues.pclaId.getKey()) {
                        toBeDeleted.push(parcelsIssues);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteNewEntitiesUnderParent(x);
                    self.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerGrpParcelsIssuesBase.prototype.deleteAllEntitiesUnderParent = function (parcelClas, afterDeleteAction) {
                var self = this;
                function deleteParcelsIssuesList(parcelsIssuesList) {
                    if (parcelsIssuesList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = parcelsIssuesList[0];
                        var tail = parcelsIssuesList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteParcelsIssuesList(tail);
                        });
                    }
                }
                this.getMergedItems(function (parcelsIssuesList) {
                    deleteParcelsIssuesList(parcelsIssuesList);
                }, false, parcelClas);
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpParcelsIssues.modelGrpGpDecision.controller,
                            self.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteAllEntitiesUnderParent(ent, function () {
                        self.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller.deleteAllEntitiesUnderParent(ent, function () {
                            _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                        });
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpParcelsIssuesBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_itm__status_disabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined || parcelsIssues === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_itm__dteStatusUpdate_disabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined || parcelsIssues === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_itm__dteCreated_disabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined || parcelsIssues === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelClas_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelClas_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpParcelsIssuesBase.prototype._deleteIsDisabled = function (parcelsIssues) {
                return true;
            };
            ControllerGrpParcelsIssuesBase.prototype.child_group_GrpGpDecision_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpGpDecision === undefined) || (self.model.modelGrpGpDecision.controller === undefined))
                    return false;
                return self.model.modelGrpGpDecision.controller._isInvisible();
            };
            ControllerGrpParcelsIssuesBase.prototype.child_group_GrpGpRequestsContexts_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpGpRequestsContexts === undefined) || (self.model.modelGrpGpRequestsContexts.controller === undefined))
                    return false;
                return self.model.modelGrpGpRequestsContexts.controller._isInvisible();
            };
            return ControllerGrpParcelsIssuesBase;
        })(Controllers.AbstractGroupTableController);
        ParcelGP.ControllerGrpParcelsIssuesBase = ControllerGrpParcelsIssuesBase;
        // GROUP GrpGpDecision
        var ModelGrpGpDecisionBase = (function (_super) {
            __extends(ModelGrpGpDecisionBase, _super);
            function ModelGrpGpDecisionBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_cultId = new NpTypes.UIManyToOneModel(undefined);
                this._fsch_cotyId = new NpTypes.UIManyToOneModel(undefined);
            }
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "gpDecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpDecisionsId;
                },
                set: function (gpDecisionsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpDecisionsId = gpDecisionsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_gpDecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpDecisionsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "cropOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cropOk;
                },
                set: function (cropOk_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cropOk = cropOk_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_cropOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cropOk;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "landcoverOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].landcoverOk;
                },
                set: function (landcoverOk_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].landcoverOk = landcoverOk_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_landcoverOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._landcoverOk;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "dteInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteInsert;
                },
                set: function (dteInsert_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteInsert = dteInsert_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_dteInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteInsert;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "usrInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].usrInsert;
                },
                set: function (usrInsert_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].usrInsert = usrInsert_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_usrInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._usrInsert;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesId;
                },
                set: function (parcelsIssuesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesId = parcelsIssuesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "fsch_cultId", {
                get: function () {
                    return this._fsch_cultId.value;
                },
                set: function (vl) {
                    this._fsch_cultId.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "fsch_cotyId", {
                get: function () {
                    return this._fsch_cotyId.value;
                },
                set: function (vl) {
                    this._fsch_cotyId.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpGpDecisionBase;
        })(Controllers.AbstractGroupTableModel);
        ParcelGP.ModelGrpGpDecisionBase = ModelGrpGpDecisionBase;
        var ControllerGrpGpDecisionBase = (function (_super) {
            __extends(ControllerGrpGpDecisionBase, _super);
            function ControllerGrpGpDecisionBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 2, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpGpDecision = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpGpDecision_srchr__fsch_cultId_disabled =
                    function (gpDecision) {
                        return self.GrpGpDecision_srchr__fsch_cultId_disabled(gpDecision);
                    };
                $scope.showLov_GrpGpDecision_srchr__fsch_cultId =
                    function () {
                        $timeout(function () {
                            self.showLov_GrpGpDecision_srchr__fsch_cultId();
                        }, 0);
                    };
                $scope.GrpGpDecision_srchr__fsch_cotyId_disabled =
                    function (gpDecision) {
                        return self.GrpGpDecision_srchr__fsch_cotyId_disabled(gpDecision);
                    };
                $scope.showLov_GrpGpDecision_srchr__fsch_cotyId =
                    function () {
                        $timeout(function () {
                            self.showLov_GrpGpDecision_srchr__fsch_cotyId();
                        }, 0);
                    };
                $scope.GrpGpDecision_itm__cultId_disabled =
                    function (gpDecision) {
                        return self.GrpGpDecision_itm__cultId_disabled(gpDecision);
                    };
                $scope.showLov_GrpGpDecision_itm__cultId =
                    function (gpDecision) {
                        $timeout(function () {
                            self.showLov_GrpGpDecision_itm__cultId(gpDecision);
                        }, 0);
                    };
                $scope.GrpGpDecision_itm__cotyId_disabled =
                    function (gpDecision) {
                        return self.GrpGpDecision_itm__cotyId_disabled(gpDecision);
                    };
                $scope.showLov_GrpGpDecision_itm__cotyId =
                    function (gpDecision) {
                        $timeout(function () {
                            self.showLov_GrpGpDecision_itm__cotyId(gpDecision);
                        }, 0);
                    };
                $scope.pageModel.modelGrpGpDecision = $scope.modelGrpGpDecision;
                $scope.clearBtnAction = function () {
                    self.modelGrpGpDecision.fsch_cultId = undefined;
                    self.modelGrpGpDecision.fsch_cotyId = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelsIssues.modelGrpGpDecision = $scope.modelGrpGpDecision;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpDecision[] , oldVisible:Entities.GpDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpGpDecisionBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpGpDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelGP_ControllerGrpGpDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.LovItem(function (ent) { return 'Presumed Crop'; }, true, function (ent) { return self.model.fsch_cultId; }, function (ent) { return self.model._fsch_cultId; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.LovItem(function (ent) { return 'Presumed Land Cover'; }, true, function (ent) { return self.model.fsch_cotyId; }, function (ent) { return self.model._fsch_cotyId; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.LovItem(function (ent) { return 'Presumed Crop'; }, false, function (ent) { return ent.cultId; }, function (ent) { return ent._cultId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.LovItem(function (ent) { return 'Presumed Land Cover'; }, false, function (ent) { return ent.cotyId; }, function (ent) { return ent._cotyId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.DateItem(function (ent) { return 'Date'; }, false, function (ent) { return ent.dteInsert; }, function (ent) { return ent._dteInsert; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.TextItem(function (ent) { return 'User'; }, false, function (ent) { return ent.usrInsert; }, function (ent) { return ent._usrInsert; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.gridTitle = function () {
                return "Presumed Crop n Land Cover";
            };
            ControllerGrpGpDecisionBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpGpDecisionBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'cultId.name', displayName: 'getALString("Presumed Crop", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpGpDecision_itm__cultId_disabled(row.entity) && showLov_GrpGpDecision_itm__cultId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpGpDecision_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpGpDecision_itm__cultId(row.entity)'   data-ng-disabled=\"GrpGpDecision_itm__cultId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cotyId.name', displayName: 'getALString("Presumed Land Cover", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpGpDecision_itm__cotyId_disabled(row.entity) && showLov_GrpGpDecision_itm__cotyId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cotyId.name' data-np-ui-model='row.entity._cotyId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpGpDecision_itm__cotyId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpGpDecision_itm__cotyId(row.entity)'   data-ng-disabled=\"GrpGpDecision_itm__cotyId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteInsert', displayName: 'getALString("Date", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpDecision_itm__dteInsert' data-ng-model='row.entity.dteInsert' data-np-ui-model='row.entity._dteInsert' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteInsert&quot;)' data-np-required='true' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy hh:mm' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'usrInsert', displayName: 'getALString("User", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpDecision_itm__usrInsert' data-ng-model='row.entity.usrInsert' data-np-ui-model='row.entity._usrInsert' data-ng-change='markEntityAsUpdated(row.entity,&quot;usrInsert&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "modelGrpGpDecision", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.getEntityName = function () {
                return "GpDecision";
            };
            ControllerGrpGpDecisionBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpGpDecisionBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.GpDecision.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.parcelsIssuesId = _this.Parent;
                    }
                    else {
                        x.parcelsIssuesId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpDecision.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(cur.parcelsIssuesId, lockKind);
            };
            ControllerGrpGpDecisionBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cultId) && !isVoid(self.modelGrpGpDecision.fsch_cultId.cultId)) {
                    paramData['fsch_cultId_cultId'] = self.modelGrpGpDecision.fsch_cultId.cultId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cotyId) && !isVoid(self.modelGrpGpDecision.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpGpDecision.fsch_cotyId.cotyId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpGpDecisionBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_ParcelGPGrpGpDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cultId) && !isVoid(self.modelGrpGpDecision.fsch_cultId.cultId)) {
                    paramData['fsch_cultId_cultId'] = self.modelGrpGpDecision.fsch_cultId.cultId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cotyId) && !isVoid(self.modelGrpGpDecision.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpGpDecision.fsch_cotyId.cotyId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpDecisionBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_ParcelGPGrpGpDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cultId) && !isVoid(self.modelGrpGpDecision.fsch_cultId.cultId)) {
                    paramData['fsch_cultId_cultId'] = self.modelGrpGpDecision.fsch_cultId.cultId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cotyId) && !isVoid(self.modelGrpGpDecision.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpGpDecision.fsch_cotyId.cotyId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpDecisionBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_ParcelGPGrpGpDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cultId) && !isVoid(self.modelGrpGpDecision.fsch_cultId.cultId)) {
                    paramData['fsch_cultId_cultId'] = self.modelGrpGpDecision.fsch_cultId.cultId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cotyId) && !isVoid(self.modelGrpGpDecision.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpGpDecision.fsch_cotyId.cotyId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpDecisionBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.parcelsIssuesId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "GpDecision/findAllByCriteriaRange_ParcelGPGrpGpDecision";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpGpDecisionBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                    return false;
                return x.parcelsIssuesId.getKey() === this.Parent.getKey();
            };
            ControllerGrpGpDecisionBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "parcelsIssuesId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.GpDecision(
                /*gpDecisionsId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*cropOk:number*/ null, 
                /*landcoverOk:number*/ null, 
                /*dteInsert:Date*/ new Date, 
                /*usrInsert:string*/ (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.globalUserLoginName)) ? self.modelGrpGpDecision.globalUserLoginName : undefined, 
                /*rowVersion:number*/ null, 
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null, 
                /*cultId:Entities.Cultivation*/ null, 
                /*cotyId:Entities.CoverType*/ null);
                return ret;
            };
            ControllerGrpGpDecisionBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.parcelsIssuesId = self.parcelsIssuesId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpGpDecisionBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpGpDecisionBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.GpDecision.Create();
                ret.updateInstance(src);
                ret.gpDecisionsId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpGpDecisionBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (gpDecisionList) {
                    gpDecisionList.forEach(function (gpDecision) {
                        var gpDecisionCloned = _this.cloneEntity_internal(gpDecision, true);
                        gpDecisionCloned.parcelsIssuesId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpGpDecisionBase.prototype.deleteNewEntitiesUnderParent = function (parcelsIssues) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var gpDecision = change.a;
                    if (parcelsIssues.getKey() === gpDecision.parcelsIssuesId.getKey()) {
                        toBeDeleted.push(gpDecision);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpGpDecisionBase.prototype.deleteAllEntitiesUnderParent = function (parcelsIssues, afterDeleteAction) {
                var self = this;
                function deleteGpDecisionList(gpDecisionList) {
                    if (gpDecisionList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = gpDecisionList[0];
                        var tail = gpDecisionList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteGpDecisionList(tail);
                        });
                    }
                }
                this.getMergedItems(function (gpDecisionList) {
                    deleteGpDecisionList(gpDecisionList);
                }, false, parcelsIssues);
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpGpDecisionBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpDecisionBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpDecisionBase.prototype.GrpGpDecision_srchr__fsch_cultId_disabled = function (gpDecision) {
                var self = this;
                return false;
            };
            ControllerGrpGpDecisionBase.prototype.showLov_GrpGpDecision_srchr__fsch_cultId = function () {
                var self = this;
                var uimodel = self.modelGrpGpDecision._fsch_cultId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Cultivation';
                dialogOptions.previousModel = self.cachedLovModel_GrpGpDecision_srchr__fsch_cultId;
                dialogOptions.className = "ControllerLovCultivation";
                dialogOptions.onSelect = function (selectedEntity) {
                    var cultivation1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(cultivation1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(cultivation1) && cultivation1.isEqual(self.modelGrpGpDecision.fsch_cultId))
                        return;
                    self.modelGrpGpDecision.fsch_cultId = cultivation1;
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpGpDecision_srchr__fsch_cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpGpDecisionBase.prototype.GrpGpDecision_srchr__fsch_cotyId_disabled = function (gpDecision) {
                var self = this;
                return false;
            };
            ControllerGrpGpDecisionBase.prototype.showLov_GrpGpDecision_srchr__fsch_cotyId = function () {
                var self = this;
                var uimodel = self.modelGrpGpDecision._fsch_cotyId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Land Cover';
                dialogOptions.previousModel = self.cachedLovModel_GrpGpDecision_srchr__fsch_cotyId;
                dialogOptions.className = "ControllerLovCoverType";
                dialogOptions.onSelect = function (selectedEntity) {
                    var coverType1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(coverType1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(coverType1) && coverType1.isEqual(self.modelGrpGpDecision.fsch_cotyId))
                        return;
                    self.modelGrpGpDecision.fsch_cotyId = coverType1;
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpGpDecision_srchr__fsch_cotyId = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpGpDecisionBase.prototype.isCheckMade = function (gpDecision) {
                console.warn("Unimplemented function isCheckMade()");
                return false;
            };
            ControllerGrpGpDecisionBase.prototype.GrpGpDecision_itm__cultId_disabled = function (gpDecision) {
                var self = this;
                if (gpDecision === undefined || gpDecision === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpDecision, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isCheckMade(gpDecision);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpDecisionBase.prototype.showLov_GrpGpDecision_itm__cultId = function (gpDecision) {
                var self = this;
                if (gpDecision === undefined)
                    return;
                var uimodel = gpDecision._cultId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Cultivation';
                dialogOptions.previousModel = self.cachedLovModel_GrpGpDecision_itm__cultId;
                dialogOptions.className = "ControllerLovCultivation";
                dialogOptions.onSelect = function (selectedEntity) {
                    var cultivation1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(cultivation1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(cultivation1) && cultivation1.isEqual(gpDecision.cultId))
                        return;
                    gpDecision.cultId = cultivation1;
                    self.markEntityAsUpdated(gpDecision, 'GrpGpDecision_itm__cultId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpGpDecision_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpGpDecisionBase.prototype.GrpGpDecision_itm__cotyId_disabled = function (gpDecision) {
                var self = this;
                if (gpDecision === undefined || gpDecision === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpDecision, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isCheckMade(gpDecision);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpDecisionBase.prototype.showLov_GrpGpDecision_itm__cotyId = function (gpDecision) {
                var self = this;
                if (gpDecision === undefined)
                    return;
                var uimodel = gpDecision._cotyId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Land Cover';
                dialogOptions.previousModel = self.cachedLovModel_GrpGpDecision_itm__cotyId;
                dialogOptions.className = "ControllerLovCoverType";
                dialogOptions.onSelect = function (selectedEntity) {
                    var coverType1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(coverType1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(coverType1) && coverType1.isEqual(gpDecision.cotyId))
                        return;
                    gpDecision.cotyId = coverType1;
                    self.markEntityAsUpdated(gpDecision, 'GrpGpDecision_itm__cotyId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpGpDecision_itm__cotyId = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpGpDecisionBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpDecisionBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpDecisionBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpGpDecisionBase.prototype._deleteIsDisabled = function (gpDecision) {
                return true;
            };
            return ControllerGrpGpDecisionBase;
        })(Controllers.AbstractGroupTableController);
        ParcelGP.ControllerGrpGpDecisionBase = ControllerGrpGpDecisionBase;
        // GROUP GrpGpRequestsContexts
        var ModelGrpGpRequestsContextsBase = (function (_super) {
            __extends(ModelGrpGpRequestsContextsBase, _super);
            function ModelGrpGpRequestsContextsBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_hash = new NpTypes.UIStringModel(undefined);
            }
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "gpRequestsContextsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpRequestsContextsId;
                },
                set: function (gpRequestsContextsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpRequestsContextsId = gpRequestsContextsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_gpRequestsContextsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpRequestsContextsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "type", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].type;
                },
                set: function (type_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].type = type_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_type", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._type;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "label", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].label;
                },
                set: function (label_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].label = label_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_label", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._label;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "comment", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comment;
                },
                set: function (comment_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comment = comment_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_comment", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comment;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "geomHexewkb", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].geomHexewkb;
                },
                set: function (geomHexewkb_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].geomHexewkb = geomHexewkb_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_geomHexewkb", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._geomHexewkb;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "referencepoint", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].referencepoint;
                },
                set: function (referencepoint_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].referencepoint = referencepoint_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_referencepoint", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._referencepoint;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "hash", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].hash;
                },
                set: function (hash_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].hash = hash_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_hash", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._hash;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "gpRequestsProducersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpRequestsProducersId;
                },
                set: function (gpRequestsProducersId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpRequestsProducersId = gpRequestsProducersId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_gpRequestsProducersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpRequestsProducersId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesId;
                },
                set: function (parcelsIssuesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesId = parcelsIssuesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "fsch_hash", {
                get: function () {
                    return this._fsch_hash.value;
                },
                set: function (vl) {
                    this._fsch_hash.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpGpRequestsContextsBase;
        })(Controllers.AbstractGroupTableModel);
        ParcelGP.ModelGrpGpRequestsContextsBase = ModelGrpGpRequestsContextsBase;
        var ControllerGrpGpRequestsContextsBase = (function (_super) {
            __extends(ControllerGrpGpRequestsContextsBase, _super);
            function ControllerGrpGpRequestsContextsBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpGpRequestsContexts = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpGpRequestsContexts_srchr__fsch_hash_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_srchr__fsch_hash_disabled(gpRequestsContexts);
                    };
                $scope.child_group_GrpGpUpload_isInvisible =
                    function () {
                        return self.child_group_GrpGpUpload_isInvisible();
                    };
                $scope.pageModel.modelGrpGpRequestsContexts = $scope.modelGrpGpRequestsContexts;
                $scope.clearBtnAction = function () {
                    self.modelGrpGpRequestsContexts.fsch_hash = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelsIssues.modelGrpGpRequestsContexts = $scope.modelGrpGpRequestsContexts;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpRequestsContexts[] , oldVisible:Entities.GpRequestsContexts[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.GpRequestsContexts.gpUploadCollection = function (gpRequestsContexts, func) {
                    _this.model.modelGrpGpUpload.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, gpRequestsContexts);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.GpRequestsContexts.gpUploadCollection = null; //(gpRequestsContexts, func) => { }
                });
            }
            ControllerGrpGpRequestsContextsBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpGpRequestsContexts";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelGP_ControllerGrpGpRequestsContexts";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'hash'; }, true, function (ent) { return self.model.fsch_hash; }, function (ent) { return self.model._fsch_hash; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'Hash'; }, false, function (ent) { return ent.hash; }, function (ent) { return ent._hash; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpRequestsContextsBase.prototype.gridTitle = function () {
                return "Requests\' Contexts";
            };
            ControllerGrpGpRequestsContextsBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpGpRequestsContextsBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'hash', displayName: 'getALString("Hash", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__hash' data-ng-model='row.entity.hash' data-np-ui-model='row.entity._hash' data-ng-change='markEntityAsUpdated(row.entity,&quot;hash&quot;)' data-np-required='true' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "modelGrpGpRequestsContexts", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpRequestsContextsBase.prototype.getEntityName = function () {
                return "GpRequestsContexts";
            };
            ControllerGrpGpRequestsContextsBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpGpRequestsContextsBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.GpRequestsContexts.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.parcelsIssuesId = _this.Parent;
                    }
                    else {
                        x.parcelsIssuesId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpRequestsContexts.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpRequestsContextsBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(cur.parcelsIssuesId, lockKind);
            };
            ControllerGrpGpRequestsContextsBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpRequestsContexts.fsch_hash;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpGpRequestsContextsBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpRequestsContexts.fsch_hash;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpRequestsContextsBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpRequestsContexts.fsch_hash;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpRequestsContextsBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpRequestsContexts.fsch_hash;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpRequestsContextsBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.parcelsIssuesId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelGPGrpGpRequestsContexts";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpGpRequestsContextsBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                    return false;
                return x.parcelsIssuesId.getKey() === this.Parent.getKey();
            };
            ControllerGrpGpRequestsContextsBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "parcelsIssuesId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpRequestsContextsBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.GpRequestsContexts(
                /*gpRequestsContextsId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*type:string*/ null, 
                /*label:string*/ null, 
                /*comment:string*/ null, 
                /*geomHexewkb:string*/ null, 
                /*referencepoint:string*/ null, 
                /*rowVersion:number*/ null, 
                /*hash:string*/ null, 
                /*gpRequestsProducersId:Entities.GpRequestsProducers*/ null, 
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null);
                return ret;
            };
            ControllerGrpGpRequestsContextsBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.parcelsIssuesId = self.parcelsIssuesId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpGpRequestsContextsBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpGpRequestsContextsBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.GpRequestsContexts.Create();
                ret.updateInstance(src);
                ret.gpRequestsContextsId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerGrpGpRequestsContextsBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (gpRequestsContextsList) {
                    gpRequestsContextsList.forEach(function (gpRequestsContexts) {
                        var gpRequestsContextsCloned = _this.cloneEntity_internal(gpRequestsContexts, true);
                        gpRequestsContextsCloned.parcelsIssuesId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpGpRequestsContextsBase.prototype.deleteNewEntitiesUnderParent = function (parcelsIssues) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var gpRequestsContexts = change.a;
                    if (parcelsIssues.getKey() === gpRequestsContexts.parcelsIssuesId.getKey()) {
                        toBeDeleted.push(gpRequestsContexts);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerGrpGpRequestsContextsBase.prototype.deleteAllEntitiesUnderParent = function (parcelsIssues, afterDeleteAction) {
                var self = this;
                function deleteGpRequestsContextsList(gpRequestsContextsList) {
                    if (gpRequestsContextsList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = gpRequestsContextsList[0];
                        var tail = gpRequestsContextsList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteGpRequestsContextsList(tail);
                        });
                    }
                }
                this.getMergedItems(function (gpRequestsContextsList) {
                    deleteGpRequestsContextsList(gpRequestsContextsList);
                }, false, parcelsIssues);
            };
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpRequestsContextsBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpGpRequestsContextsBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpRequestsContextsBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_srchr__fsch_hash_disabled = function (gpRequestsContexts) {
                var self = this;
                return false;
            };
            ControllerGrpGpRequestsContextsBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpRequestsContextsBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpRequestsContextsBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpGpRequestsContextsBase.prototype._deleteIsDisabled = function (gpRequestsContexts) {
                return true;
            };
            ControllerGrpGpRequestsContextsBase.prototype.child_group_GrpGpUpload_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpGpUpload === undefined) || (self.model.modelGrpGpUpload.controller === undefined))
                    return false;
                return self.model.modelGrpGpUpload.controller._isInvisible();
            };
            return ControllerGrpGpRequestsContextsBase;
        })(Controllers.AbstractGroupTableController);
        ParcelGP.ControllerGrpGpRequestsContextsBase = ControllerGrpGpRequestsContextsBase;
        // GROUP GrpGpUpload
        var ModelGrpGpUploadBase = (function (_super) {
            __extends(ModelGrpGpUploadBase, _super);
            function ModelGrpGpUploadBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_dteUpload = new NpTypes.UIDateModel(undefined);
            }
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "gpUploadsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpUploadsId;
                },
                set: function (gpUploadsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpUploadsId = gpUploadsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_gpUploadsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpUploadsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "data", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].data;
                },
                set: function (data_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].data = data_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_data", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._data;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "environment", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].environment;
                },
                set: function (environment_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].environment = environment_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_environment", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._environment;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "dteUpload", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteUpload;
                },
                set: function (dteUpload_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteUpload = dteUpload_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_dteUpload", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteUpload;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "hash", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].hash;
                },
                set: function (hash_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].hash = hash_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_hash", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._hash;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "image", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].image;
                },
                set: function (image_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].image = image_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_image", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._image;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "gpRequestsContextsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpRequestsContextsId;
                },
                set: function (gpRequestsContextsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpRequestsContextsId = gpRequestsContextsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_gpRequestsContextsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpRequestsContextsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "fsch_dteUpload", {
                get: function () {
                    return this._fsch_dteUpload.value;
                },
                set: function (vl) {
                    this._fsch_dteUpload.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpGpUploadBase;
        })(Controllers.AbstractGroupTableModel);
        ParcelGP.ModelGrpGpUploadBase = ModelGrpGpUploadBase;
        var ControllerGrpGpUploadBase = (function (_super) {
            __extends(ControllerGrpGpUploadBase, _super);
            function ControllerGrpGpUploadBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpGpUpload = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpGpUpload_srchr__fsch_dteUpload_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_srchr__fsch_dteUpload_disabled(gpUpload);
                    };
                $scope.GrpGpUpload_itm__dteUpload_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_itm__dteUpload_disabled(gpUpload);
                    };
                $scope.GrpGpUpload_itm__image_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_itm__image_disabled(gpUpload);
                    };
                $scope.createBlobModel_GrpGpUpload_itm__image =
                    function () {
                        var tmp = self.createBlobModel_GrpGpUpload_itm__image();
                        return tmp;
                    };
                $scope.GrpGpUpload_itm__2_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_itm__2_disabled(gpUpload);
                    };
                $scope.viewGeotaggegPhoto =
                    function (gpUpload) {
                        self.viewGeotaggegPhoto(gpUpload);
                    };
                $scope.pageModel.modelGrpGpUpload = $scope.modelGrpGpUpload;
                $scope.clearBtnAction = function () {
                    self.modelGrpGpUpload.fsch_dteUpload = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpGpRequestsContexts.modelGrpGpUpload = $scope.modelGrpGpUpload;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpGpRequestsContexts.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpUpload[] , oldVisible:Entities.GpUpload[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpGpUploadBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpGpUpload";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelGP_ControllerGrpGpUpload";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.DateItem(function (ent) { return 'Date Uploaded'; }, true, function (ent) { return self.model.fsch_dteUpload; }, function (ent) { return self.model._fsch_dteUpload; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.DateItem(function (ent) { return 'Date Uploaded'; }, false, function (ent) { return ent.dteUpload; }, function (ent) { return ent._dteUpload; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.BlobItem(function (ent) { return 'Photo'; }, false, function (ent) { return ent.image; }, function (ent) { return ent._image; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpUploadBase.prototype.gridTitle = function () {
                return "Geotagged Photos";
            };
            ControllerGrpGpUploadBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpGpUploadBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'dteUpload', displayName: 'getALString("Date Uploaded", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpUpload_itm__dteUpload' data-ng-model='row.entity.dteUpload' data-np-ui-model='row.entity._dteUpload' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteUpload&quot;)' data-np-required='true' data-ng-readonly='GrpGpUpload_itm__dteUpload_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'image', displayName: 'getALString("Photo", true)', requiredAsterisk: false, resizable: true, sortable: false, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <span data-ng-model='row.entity._image' data-np-factory='createBlobModel_GrpGpUpload_itm__image()' data-np-blob='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'grpGpUpload_itm__2', displayName: 'getALString("View", true)', requiredAsterisk: false, resizable: true, sortable: false, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <button  id=\"GrpGpUpload_itm__2\"   data-np-click  =\"viewGeotaggegPhoto(modelGrpGpUpload.selectedEntities[0])\" data-ng-class=\"\"  \
                    data-ng-disabled=\"GrpGpUpload_itm__2_disabled(row.entity)\"  > \
                        {{getALString('View')}} \
                    </button> \
                 \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "modelGrpGpUpload", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpUploadBase.prototype.getEntityName = function () {
                return "GpUpload";
            };
            ControllerGrpGpUploadBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpGpUploadBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.GpUpload.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.gpRequestsContextsId = _this.Parent;
                    }
                    else {
                        x.gpRequestsContextsId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpUpload.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpUploadBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpGpRequestsContexts.controller.isEntityLocked(cur.gpRequestsContextsId, lockKind);
            };
            ControllerGrpGpUploadBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpGpUploadBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpUpload/findAllByCriteriaRange_ParcelGPGrpGpUpload_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpUploadBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpUpload/findAllByCriteriaRange_ParcelGPGrpGpUpload";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpUploadBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpUpload/findAllByCriteriaRange_ParcelGPGrpGpUpload_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpUploadBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.gpRequestsContextsId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "GpUpload/findAllByCriteriaRange_ParcelGPGrpGpUpload";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['gpRequestsContextsId_gpRequestsContextsId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpGpUploadBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.gpRequestsContextsId === undefined)
                    return false;
                return x.gpRequestsContextsId.getKey() === this.Parent.getKey();
            };
            ControllerGrpGpUploadBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.gpRequestsContextsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpRequestsContexts.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "gpRequestsContextsId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpRequestsContexts.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpUploadBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.GpUpload(
                /*gpUploadsId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*data:string*/ null, 
                /*environment:string*/ null, 
                /*dteUpload:Date*/ null, 
                /*hash:string*/ null, 
                /*image:string*/ null, 
                /*gpRequestsContextsId:Entities.GpRequestsContexts*/ null);
                return ret;
            };
            ControllerGrpGpUploadBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.gpRequestsContextsId = self.gpRequestsContextsId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpGpUploadBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpGpUploadBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.GpUpload.Create();
                ret.updateInstance(src);
                ret.gpUploadsId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpGpUploadBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (gpUploadList) {
                    gpUploadList.forEach(function (gpUpload) {
                        var gpUploadCloned = _this.cloneEntity_internal(gpUpload, true);
                        gpUploadCloned.gpRequestsContextsId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpGpUploadBase.prototype.deleteNewEntitiesUnderParent = function (gpRequestsContexts) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var gpUpload = change.a;
                    if (gpRequestsContexts.getKey() === gpUpload.gpRequestsContextsId.getKey()) {
                        toBeDeleted.push(gpUpload);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpGpUploadBase.prototype.deleteAllEntitiesUnderParent = function (gpRequestsContexts, afterDeleteAction) {
                var self = this;
                function deleteGpUploadList(gpUploadList) {
                    if (gpUploadList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = gpUploadList[0];
                        var tail = gpUploadList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteGpUploadList(tail);
                        });
                    }
                }
                this.getMergedItems(function (gpUploadList) {
                    deleteGpUploadList(gpUploadList);
                }, false, gpRequestsContexts);
            };
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpUploadBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpGpUploadBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpUploadBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_srchr__fsch_dteUpload_disabled = function (gpUpload) {
                var self = this;
                return false;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_itm__dteUpload_disabled = function (gpUpload) {
                var self = this;
                if (gpUpload === undefined || gpUpload === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpUpload, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_itm__image_disabled = function (gpUpload) {
                var self = this;
                if (gpUpload === undefined || gpUpload === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelGP_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpUpload, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpUploadBase.prototype.createBlobModel_GrpGpUpload_itm__image = function () {
                var self = this;
                var ret = new NpTypes.NpBlob(function (e) { self.markEntityAsUpdated(e, "image"); }, // on value change
                function (e) { return self.getDownloadUrl_createBlobModel_GrpGpUpload_itm__image(e); }, // download url
                function (e) { return self.GrpGpUpload_itm__image_disabled(e); }, // is disabled
                function (e) { return false; }, // is invisible
                "/Niva/rest/GpUpload/setImage", // post url
                false, // add is enabled
                false, // del is enabled
                "", // valid extensions
                function (e) { return 195; } //size in KB
                 //size in KB
                );
                return ret;
            };
            ControllerGrpGpUploadBase.prototype.getDownloadUrl_createBlobModel_GrpGpUpload_itm__image = function (gpUpload) {
                var self = this;
                if (isVoid(gpUpload))
                    return 'javascript:void(0)';
                if (gpUpload.isNew() && isVoid(gpUpload.image))
                    return 'javascript:void(0)';
                var url = "/Niva/rest/GpUpload/getImage?";
                if (!gpUpload.isNew()) {
                    url += "&id=" + encodeURIComponent(gpUpload.gpUploadsId);
                }
                if (!isVoid(gpUpload.image)) {
                    url += "&temp_id=" + encodeURIComponent(gpUpload.image);
                }
                return url;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_itm__2_disabled = function (gpUpload) {
                var self = this;
                if (gpUpload === undefined || gpUpload === null)
                    return true;
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            ControllerGrpGpUploadBase.prototype.viewGeotaggegPhoto = function (gpUpload) {
                var self = this;
                console.log("Method: viewGeotaggegPhoto() called");
                console.log(gpUpload);
            };
            ControllerGrpGpUploadBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpUploadBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpUploadBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpGpUploadBase.prototype._deleteIsDisabled = function (gpUpload) {
                return true;
            };
            return ControllerGrpGpUploadBase;
        })(Controllers.AbstractGroupTableController);
        ParcelGP.ControllerGrpGpUploadBase = ControllerGrpGpUploadBase;
    })(ParcelGP = Controllers.ParcelGP || (Controllers.ParcelGP = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelGPBase.js.map