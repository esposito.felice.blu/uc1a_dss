/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="../EntitiesBase/ParcelDecisionBase.ts" />
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />
/// <reference path="../EntitiesBase/GpDecisionBase.ts" />
/// <reference path="../EntitiesBase/FmisDecisionBase.ts" />
/// <reference path="../EntitiesBase/IntegrateddecisionBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="LovCoverTypeBase.ts" />
/// <reference path="../Controllers/Dashboard.ts" />
module Controllers.Dashboard {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpParcelClas:ModelGrpParcelClas;
        modelGrpParcelDecision:ModelGrpParcelDecision;
        modelGrpParcelsIssues:ModelGrpParcelsIssues;
        modelGrpGpDecision:ModelGrpGpDecision;
        modelGrpFmisDecision:ModelGrpFmisDecision;
        modelGrpIntegrateddecision:ModelGrpIntegrateddecision;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
        onNewBtnAction(): void;
        onDeleteBtnAction():void;

    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            

            $scope.onNewBtnAction = () => { 
                self.onNewBtnAction();
            };
            
            $scope.onDeleteBtnAction = () => { 
                self.onDeleteBtnAction();
            };


            $timeout(function() {
                $('#Dashboard').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "Dashboard";
        }
    
        public _getPageTitle(): string {
            return "Parcels";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpParcelClas.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpParcelClas.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpParcelDecision.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpParcelsIssues.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpGpDecision.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpFmisDecision.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpIntegrateddecision.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpParcelClas.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpParcelDecision.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpParcelsIssues.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpGpDecision.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpFmisDecision.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpIntegrateddecision.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpParcelClas.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpParcelClas.controller,
                    self.model.modelGrpParcelDecision.controller,
                    self.model.modelGrpParcelsIssues.controller,
                    self.model.modelGrpGpDecision.controller,
                    self.model.modelGrpFmisDecision.controller,
                    self.model.modelGrpIntegrateddecision.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
            if (bForDelete) {
                pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getChangesToCommitForDeletion());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelDecision.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpFmisDecision.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getDeleteChangesToCommit());
            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelDecision.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpFmisDecision.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getChangesToCommit());
            }
            
            var hasParcelClas = pageChanges.some(change => change.entityName === "ParcelClas")
            if (!hasParcelClas) {
                var validateEntity = new ChangeToCommit(ChangeStatus.UPDATE, 0, "ParcelClas", self.model.modelGrpParcelClas.controller.Current);
                pageChanges = pageChanges.concat(validateEntity);
            }
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_Dashboard"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpParcelClas.controller.cleanUpAfterSave();
            self.model.modelGrpParcelDecision.controller.cleanUpAfterSave();
            self.model.modelGrpParcelsIssues.controller.cleanUpAfterSave();
            self.model.modelGrpGpDecision.controller.cleanUpAfterSave();
            self.model.modelGrpFmisDecision.controller.cleanUpAfterSave();
            self.model.modelGrpIntegrateddecision.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;

                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.ParcelClas>>self.$scope.getCurrentPageModel();
                        var newParcelClasId = response.newEntitiesIds.firstOrNull(x => x.entityName === 'ParcelClas');
                        if (!isVoid(newParcelClasId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.ParcelClas.CreateById(newParcelClasId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        } else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }

                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }

        public getInitialEntity(): Entities.ParcelClas {
            if (this.shownAsDialog()) {
                return <Entities.ParcelClas>this.dialogSelectedEntity();
            } else {
                var breadCrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.ParcelClas>>this.$scope.getPageModelByURL('/Dashboard');
                if (isVoid(breadCrumbStepModel)) {
                    return null;
                } else {
                    return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                }
            }
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


        public onNewBtnAction(): void {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => { 
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                self.addNewRecord();
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {self.addNewRecord();}),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                self.addNewRecord();
            }
        }

        public addNewRecord(): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            self.model.modelGrpParcelClas.controller.cleanUpAfterSave();
            self.model.modelGrpParcelDecision.controller.cleanUpAfterSave();
            self.model.modelGrpParcelsIssues.controller.cleanUpAfterSave();
            self.model.modelGrpGpDecision.controller.cleanUpAfterSave();
            self.model.modelGrpFmisDecision.controller.cleanUpAfterSave();
            self.model.modelGrpIntegrateddecision.controller.cleanUpAfterSave();
            self.model.modelGrpParcelClas.controller.createNewEntityAndAddToUI();
        }

        public onDeleteBtnAction():void {
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, 
                [
                    new Tuple2("MessageBox_Button_Yes", () => {self.deleteRecord();}),
                    new Tuple2("MessageBox_Button_No", () => {})
                ], 1,1);
        }

        public deleteRecord(): void {
            var self = this;
            if (self.Mode === EditMode.NEW) {
                console.log("Delete pressed in a form while being in new mode!");
                return;
            }
            NpTypes.AlertMessage.clearAlerts(self.model);
            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData(true);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
            });
        }
        
        public get Mode(): EditMode {
            var self = this;
            if (isVoid(self.model.modelGrpParcelClas) || isVoid(self.model.modelGrpParcelClas.controller))
                return EditMode.NEW;
            return self.model.modelGrpParcelClas.controller.Mode;
        }

        public _newIsDisabled(): boolean {
            var self = this;
            if (isVoid(self.model.modelGrpParcelClas) || isVoid(self.model.modelGrpParcelClas.controller))
                return true;
            return self.model.modelGrpParcelClas.controller._newIsDisabled();
        }
        public _deleteIsDisabled(): boolean {
            var self = this;
            if (self.Mode === EditMode.NEW)
                return true;
            if (isVoid(self.model.modelGrpParcelClas) || isVoid(self.model.modelGrpParcelClas.controller))
                return true;
            return self.model.modelGrpParcelClas.controller._deleteIsDisabled(self.model.modelGrpParcelClas.controller.Current);
        }


    }


    

    // GROUP GrpParcelClas

    export class ModelGrpParcelClasBase extends Controllers.AbstractGroupFormModel {
        modelGrpParcelDecision:ModelGrpParcelDecision;
        modelGrpParcelsIssues:ModelGrpParcelsIssues;
        modelGrpIntegrateddecision:ModelGrpIntegrateddecision;
        controller: ControllerGrpParcelClas;
        public get pclaId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).pclaId;
        }

        public set pclaId(pclaId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).pclaId = pclaId_newVal;
        }

        public get _pclaId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._pclaId;
        }

        public get probPred():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).probPred;
        }

        public set probPred(probPred_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).probPred = probPred_newVal;
        }

        public get _probPred():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probPred;
        }

        public get probPred2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).probPred2;
        }

        public set probPred2(probPred2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).probPred2 = probPred2_newVal;
        }

        public get _probPred2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probPred2;
        }

        public get prodCode():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).prodCode;
        }

        public set prodCode(prodCode_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).prodCode = prodCode_newVal;
        }

        public get _prodCode():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._prodCode;
        }

        public get parcIdentifier():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).parcIdentifier;
        }

        public set parcIdentifier(parcIdentifier_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).parcIdentifier = parcIdentifier_newVal;
        }

        public get _parcIdentifier():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcIdentifier;
        }

        public get parcCode():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).parcCode;
        }

        public set parcCode(parcCode_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).parcCode = parcCode_newVal;
        }

        public get _parcCode():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcCode;
        }

        public get geom4326():ol.Feature {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).geom4326;
        }

        public set geom4326(geom4326_newVal:ol.Feature) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).geom4326 = geom4326_newVal;
        }

        public get _geom4326():NpTypes.UIGeoModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._geom4326;
        }

        public get clasId():Entities.Classification {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).clasId;
        }

        public set clasId(clasId_newVal:Entities.Classification) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).clasId = clasId_newVal;
        }

        public get _clasId():NpTypes.UIManyToOneModel<Entities.Classification> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._clasId;
        }

        public get cultIdDecl():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).cultIdDecl;
        }

        public set cultIdDecl(cultIdDecl_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).cultIdDecl = cultIdDecl_newVal;
        }

        public get _cultIdDecl():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultIdDecl;
        }

        public get cultIdPred():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).cultIdPred;
        }

        public set cultIdPred(cultIdPred_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).cultIdPred = cultIdPred_newVal;
        }

        public get _cultIdPred():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultIdPred;
        }

        public get cotyIdDecl():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).cotyIdDecl;
        }

        public set cotyIdDecl(cotyIdDecl_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).cotyIdDecl = cotyIdDecl_newVal;
        }

        public get _cotyIdDecl():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyIdDecl;
        }

        public get cotyIdPred():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).cotyIdPred;
        }

        public set cotyIdPred(cotyIdPred_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).cotyIdPred = cotyIdPred_newVal;
        }

        public get _cotyIdPred():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyIdPred;
        }

        public get cultIdPred2():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).cultIdPred2;
        }

        public set cultIdPred2(cultIdPred2_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).cultIdPred2 = cultIdPred2_newVal;
        }

        public get _cultIdPred2():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultIdPred2;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpParcelClas) { super($scope); }
    }



    export interface IScopeGrpParcelClasBase extends Controllers.IAbstractFormGroupScope, IPageScope{
        globals: Globals;
        modelGrpParcelClas : ModelGrpParcelClas;
        GrpParcelClas_0_disabled():boolean; 
        GrpParcelClas_0_invisible():boolean; 
        GrpParcelClas_0_0_disabled():boolean; 
        GrpParcelClas_0_0_invisible():boolean; 
        GrpParcelClas_0_1_disabled():boolean; 
        GrpParcelClas_0_1_invisible():boolean; 
        XartoReg_disabled():boolean; 
        XartoReg_invisible():boolean; 
        INPMPID_disabled(parcelClas:Entities.ParcelClas):boolean; 
        GrpParcelClas_0_2_disabled():boolean; 
        GrpParcelClas_0_2_invisible():boolean; 
        child_group_GrpParcelDecision_isInvisible():boolean; 
        child_group_GrpParcelsIssues_isInvisible():boolean; 
        child_group_GrpIntegrateddecision_isInvisible():boolean; 

    }



    export class ControllerGrpParcelClasBase extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpParcelClas,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpParcelClas)
        {
            super($scope, $http, $timeout, Plato, model);
            var self:ControllerGrpParcelClasBase = this;
            model.controller = <ControllerGrpParcelClas>self;
            $scope.modelGrpParcelClas = self.model;

            var selectedEntity: Entities.ParcelClas = this.$scope.pageModel.controller.getInitialEntity();
            if (isVoid(selectedEntity)) {
                self.createNewEntityAndAddToUI();
            } else {
                var clonedEntity = Entities.ParcelClas.Create();
                clonedEntity.updateInstance(selectedEntity);
                $scope.modelGrpParcelClas.visibleEntities[0] = clonedEntity;
                $scope.modelGrpParcelClas.selectedEntities[0] = clonedEntity;
            } 
            $scope.GrpParcelClas_0_disabled = 
                () => {
                    return self.GrpParcelClas_0_disabled();
                };
            $scope.GrpParcelClas_0_invisible = 
                () => {
                    return self.GrpParcelClas_0_invisible();
                };
            $scope.GrpParcelClas_0_0_disabled = 
                () => {
                    return self.GrpParcelClas_0_0_disabled();
                };
            $scope.GrpParcelClas_0_0_invisible = 
                () => {
                    return self.GrpParcelClas_0_0_invisible();
                };
            $scope.GrpParcelClas_0_1_disabled = 
                () => {
                    return self.GrpParcelClas_0_1_disabled();
                };
            $scope.GrpParcelClas_0_1_invisible = 
                () => {
                    return self.GrpParcelClas_0_1_invisible();
                };
            $scope.XartoReg_disabled = 
                () => {
                    return self.XartoReg_disabled();
                };
            $scope.XartoReg_invisible = 
                () => {
                    return self.XartoReg_invisible();
                };
            $scope.INPMPID_disabled = 
                (parcelClas:Entities.ParcelClas) => {
                    return self.INPMPID_disabled(parcelClas);
                };
            $scope.GrpParcelClas_0_2_disabled = 
                () => {
                    return self.GrpParcelClas_0_2_disabled();
                };
            $scope.GrpParcelClas_0_2_invisible = 
                () => {
                    return self.GrpParcelClas_0_2_invisible();
                };
            $scope.child_group_GrpParcelDecision_isInvisible = 
                () => {
                    return self.child_group_GrpParcelDecision_isInvisible();
                };
            $scope.child_group_GrpParcelsIssues_isInvisible = 
                () => {
                    return self.child_group_GrpParcelsIssues_isInvisible();
                };
            $scope.child_group_GrpIntegrateddecision_isInvisible = 
                () => {
                    return self.child_group_GrpIntegrateddecision_isInvisible();
                };


            $scope.pageModel.modelGrpParcelClas = $scope.modelGrpParcelClas;


    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelClas[] , oldVisible:Entities.ParcelClas[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.ParcelClas.parcelDecisionCollection = (parcelClas, func) => {
                this.model.modelGrpParcelDecision.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, parcelClas);
            }
            //bind entity child collection with child controller merged items
            Entities.ParcelClas.parcelsIssuesCollection = (parcelClas, func) => {
                this.model.modelGrpParcelsIssues.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, parcelClas);
            }
            //bind entity child collection with child controller merged items
            Entities.ParcelClas.integrateddecisionCollection = (parcelClas, func) => {
                this.model.modelGrpIntegrateddecision.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, parcelClas);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.ParcelClas.parcelDecisionCollection = null;//(parcelClas, func) => { }
                //unbind entity child collection with child controller merged items
                Entities.ParcelClas.parcelsIssuesCollection = null;//(parcelClas, func) => { }
                //unbind entity child collection with child controller merged items
                Entities.ParcelClas.integrateddecisionCollection = null;//(parcelClas, func) => { }
            });

            this.createGeom4326Map({
                grp:this,
                Plato:Plato,
                divId:"INPMPID",
                getDataFromEntity:(x: Entities.ParcelClas) => isVoid(x) ? undefined:x.geom4326,
                saveDataToEntity:(x: Entities.ParcelClas, f: ol.Feature) => { 
                    if (!isVoid(x)) {
                        x.geom4326 = f; 
                        this.markEntityAsUpdated(x,"geom4326");
                    } 
                },
                isDisabled:(x: Entities.ParcelClas) => this.INPMPID_disabled(x),
                layers : [
                            new NpGeoLayers.OpenStreetTileLayer(true),
                            new NpGeoLayers.SqlVectorLayer<Entities.ParcelClas, AbstractSqlLayerFilterModel>({
                                layerId:'INPMPID1',
                                label:'Search1',
                                isVisible:true,
                                isSearch:true
                            }),
                            new NpGeoLayers.SqlVectorLayer<Entities.ParcelClas, AbstractSqlLayerFilterModel>({
                                layerId:'INPMPID2',
                                label:'Search2',
                                isVisible:true,
                                isSearch:true
                            }),
                            new NpGeoLayers.SqlVectorLayer<Entities.ParcelClas, AbstractSqlLayerFilterModel>({
                                layerId:'INPMPID3',
                                label:'Search3',
                                isVisible:true,
                                isSearch:true
                            })
                         ],
                coordinateSystem : "EPSG:2100"
            });
            
        }

        public geom4326Map : NpGeo.NpMap;

        public createGeom4326Map(defaultOptions: NpGeo.NpMapConstructionOptions) {
            this.geom4326Map = new NpGeo.NpMap(defaultOptions);
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpParcelClas";
        }
        public get HtmlDivId(): string {
            return "Dashboard_ControllerGrpParcelClas";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: Entities.ParcelClas) => 'Parcel Identifier',
                        false ,
                        (ent?: Entities.ParcelClas) => ent.parcIdentifier,  
                        (ent?: Entities.ParcelClas) => ent._parcIdentifier,  
                        (ent?: Entities.ParcelClas) => true, 
                        (vl: string, ent?: Entities.ParcelClas) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.ParcelClas) => 'Parcel Code',
                        false ,
                        (ent?: Entities.ParcelClas) => ent.parcCode,  
                        (ent?: Entities.ParcelClas) => ent._parcCode,  
                        (ent?: Entities.ParcelClas) => true, 
                        (vl: string, ent?: Entities.ParcelClas) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new NumberItem (
                        (ent?: Entities.ParcelClas) => 'Producer Code',
                        false ,
                        (ent?: Entities.ParcelClas) => ent.prodCode,  
                        (ent?: Entities.ParcelClas) => ent._prodCode,  
                        (ent?: Entities.ParcelClas) => false, //isRequired
                        (vl: number, ent?: Entities.ParcelClas) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        0),
                    new MapItem (
                        (ent?: Entities.ParcelClas) => '',
                        false ,
                        (ent?: Entities.ParcelClas) => ent.geom4326,  
                        (ent?: Entities.ParcelClas) => ent._geom4326,  
                        (ent?: Entities.ParcelClas) => false, //isRequired
                        (vl: ol.Feature, ent?: Entities.ParcelClas) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpParcelClas():ModelGrpParcelClas {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "ParcelClas";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.geom4326Map.cleanUndoBuffers();
        }

        public setCurrentFormPageBreadcrumpStepModel() {
            var self = this;
            if (self.$scope.pageModel.controller.shownAsDialog())
                return;

            var breadCrumbStepModel: IFormPageBreadcrumbStepModel<Entities.ParcelClas> = { selectedEntity: null };
            if (!isVoid(self.Current)) {
                var selectedEntity: Entities.ParcelClas = Entities.ParcelClas.Create();
                selectedEntity.updateInstance(self.Current);
                breadCrumbStepModel.selectedEntity = selectedEntity;
            } else {
                breadCrumbStepModel.selectedEntity = null;
            }
            self.$scope.setCurrentPageModel(breadCrumbStepModel);
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.ParcelClas.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.ParcelClas {
            var self = this;
            return <Entities.ParcelClas>self.$scope.modelGrpParcelClas.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.ParcelClas, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }







        public constructEntity(): Entities.ParcelClas {
            var self = this;
            var ret = new Entities.ParcelClas(
                /*pclaId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*probPred:number*/ null,
                /*probPred2:number*/ null,
                /*prodCode:number*/ null,
                /*parcIdentifier:string*/ null,
                /*parcCode:string*/ null,
                /*geom4326:ol.Feature*/ null,
                /*clasId:Entities.Classification*/ null,
                /*cultIdDecl:Entities.Cultivation*/ null,
                /*cultIdPred:Entities.Cultivation*/ null,
                /*cotyIdDecl:Entities.CoverType*/ null,
                /*cotyIdPred:Entities.CoverType*/ null,
                /*cultIdPred2:Entities.Cultivation*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.ParcelClas = <Entities.ParcelClas>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.$scope.modelGrpParcelClas.visibleEntities[0] = newEnt;
            self.$scope.modelGrpParcelClas.selectedEntities[0] = newEnt;
            return newEnt;

        }

        public cloneEntity(src: Entities.ParcelClas): Entities.ParcelClas {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.ParcelClas, calledByParent: boolean= false): Entities.ParcelClas {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.ParcelClas.Create();
            ret.updateInstance(src);
            ret.pclaId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.visibleEntities[0] = ret;
            this.model.selectedEntities[0] = ret;

            this.$timeout( () => {
                this.modelGrpParcelClas.modelGrpParcelDecision.controller.cloneAllEntitiesUnderParent(src, ret);
                this.modelGrpParcelClas.modelGrpParcelsIssues.controller.cloneAllEntitiesUnderParent(src, ret);
                this.modelGrpParcelClas.modelGrpIntegrateddecision.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpParcelClas.modelGrpParcelDecision.controller,
                    self.modelGrpParcelClas.modelGrpParcelsIssues.controller,
                    self.modelGrpParcelClas.modelGrpIntegrateddecision.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.ParcelClas, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpParcelClas.modelGrpParcelDecision.controller.deleteAllEntitiesUnderParent(ent, () => {
                    self.modelGrpParcelClas.modelGrpParcelsIssues.controller.deleteAllEntitiesUnderParent(ent, () => {
                        self.modelGrpParcelClas.modelGrpIntegrateddecision.controller.deleteAllEntitiesUnderParent(ent, () => {
                            super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                        });
                    });
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpParcelClas.modelGrpParcelDecision.controller.deleteNewEntitiesUnderParent(ent);
                    self.modelGrpParcelClas.modelGrpParcelsIssues.controller.deleteNewEntitiesUnderParent(ent);
                    self.modelGrpParcelClas.modelGrpIntegrateddecision.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public GrpParcelClas_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelClas_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelClas_0_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this.GrpParcelClas_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelClas_0_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this.GrpParcelClas_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelClas_0_1_disabled():boolean { 
            if (false)
                return true;
            var parControl = this.GrpParcelClas_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelClas_0_1_invisible():boolean { 
            if (false)
                return true;
            var parControl = this.GrpParcelClas_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public XartoReg_disabled():boolean { 
            if (false)
                return true;
            var parControl = this.GrpParcelClas_0_1_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public XartoReg_invisible():boolean { 
            if (false)
                return true;
            var parControl = this.GrpParcelClas_0_1_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public INPMPID_disabled(parcelClas:Entities.ParcelClas):boolean {
            var self = this;
            if (parcelClas === undefined || parcelClas === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelClas, LockKind.DisabledLock);
            var isContainerControlDisabled   = self.XartoReg_disabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpParcelClas_0_2_disabled():boolean { 
            if (false)
                return true;
            var parControl = this.GrpParcelClas_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelClas_0_2_invisible():boolean { 
            if (false)
                return true;
            var parControl = this.GrpParcelClas_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(parcelClas:Entities.ParcelClas):boolean  {
            return true;
        }
        public child_group_GrpParcelDecision_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpParcelDecision === undefined) || (self.model.modelGrpParcelDecision.controller === undefined))
                return false;
            return self.model.modelGrpParcelDecision.controller._isInvisible()
        }
        public child_group_GrpParcelsIssues_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpParcelsIssues === undefined) || (self.model.modelGrpParcelsIssues.controller === undefined))
                return false;
            return self.model.modelGrpParcelsIssues.controller._isInvisible()
        }
        public child_group_GrpIntegrateddecision_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpIntegrateddecision === undefined) || (self.model.modelGrpIntegrateddecision.controller === undefined))
                return false;
            return self.model.modelGrpIntegrateddecision.controller._isInvisible()
        }

    }


    // GROUP GrpParcelDecision

    export class ModelGrpParcelDecisionBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpParcelDecision;
        public get padeId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelDecision>this.selectedEntities[0]).padeId;
        }

        public set padeId(padeId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelDecision>this.selectedEntities[0]).padeId = padeId_newVal;
        }

        public get _padeId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._padeId;
        }

        public get decisionLight():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelDecision>this.selectedEntities[0]).decisionLight;
        }

        public set decisionLight(decisionLight_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelDecision>this.selectedEntities[0]).decisionLight = decisionLight_newVal;
        }

        public get _decisionLight():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionLight;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelDecision>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelDecision>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get demaId():Entities.DecisionMaking {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelDecision>this.selectedEntities[0]).demaId;
        }

        public set demaId(demaId_newVal:Entities.DecisionMaking) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelDecision>this.selectedEntities[0]).demaId = demaId_newVal;
        }

        public get _demaId():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._demaId;
        }

        public get pclaId():Entities.ParcelClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelDecision>this.selectedEntities[0]).pclaId;
        }

        public set pclaId(pclaId_newVal:Entities.ParcelClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelDecision>this.selectedEntities[0]).pclaId = pclaId_newVal;
        }

        public get _pclaId():NpTypes.UIManyToOneModel<Entities.ParcelClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._pclaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpParcelDecision) { super($scope); }
    }



    export interface IScopeGrpParcelDecisionBase extends Controllers.IAbstractTableGroupScope, IScopeGrpParcelClasBase{
        globals: Globals;
        modelGrpParcelDecision : ModelGrpParcelDecision;

    }



    export class ControllerGrpParcelDecisionBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpParcelDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpParcelDecision)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpParcelDecisionBase = this;
            model.controller = <ControllerGrpParcelDecision>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpParcelDecision = self.model;




            $scope.pageModel.modelGrpParcelDecision = $scope.modelGrpParcelDecision;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpParcelClas.modelGrpParcelDecision = $scope.modelGrpParcelDecision;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpParcelClas.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelDecision[] , oldVisible:Entities.ParcelDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpParcelDecision";
        }
        public get HtmlDivId(): string {
            return "Dashboard_ControllerGrpParcelDecision";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Parcel Decision";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'decisionLight', displayName:'getALString("Decision Light", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpParcelDecision_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'demaId.description', displayName:'getALString("Decision Making Run", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'14%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__demaId_description' data-ng-model='row.entity.demaId.description' data-np-ui-model='row.entity.demaId._description' data-ng-change='markEntityAsUpdated(row.entity,&quot;demaId.description&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'demaId.ecgrId.name', displayName:'getALString("BRE Rules Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'14%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__demaId_ecgrId_name' data-ng-model='row.entity.demaId.ecgrId.name' data-np-ui-model='row.entity.demaId.ecgrId._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;demaId.ecgrId.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'demaId.dateTime', displayName:'getALString("Date", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'7%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__demaId_dateTime' data-ng-model='row.entity.demaId.dateTime' data-np-ui-model='row.entity.demaId._dateTime' data-ng-change='markEntityAsUpdated(row.entity,&quot;demaId.dateTime&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpParcelDecision():ModelGrpParcelDecision {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "ParcelDecision";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.ParcelClas): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.ParcelDecision.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.pclaId = this.Parent;
                } else {
                    x.pclaId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.ParcelDecision {
            var self = this;
            return <Entities.ParcelDecision>self.$scope.modelGrpParcelDecision.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.ParcelDecision, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpParcelClas.controller.isEntityLocked(<Entities.ParcelClas>cur.pclaId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DashboardGrpParcelDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DashboardGrpParcelDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DashboardGrpParcelDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.ParcelDecision[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.ParcelDecision[]) => void> = [];


        public getMergedItems(func: (items: Entities.ParcelDecision[]) => void, bContinuousCaller:boolean=true, parent?: Entities.ParcelClas, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.ParcelDecision[]):void {
                var mergedEntities = <Entities.ParcelDecision[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.ParcelDecision>e.a).pclaId)).
                        map(e => <Entities.ParcelDecision>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "ParcelDecision/findAllByCriteriaRange_DashboardGrpParcelDecision";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['pclaId_pclaId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.ParcelDecision[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.ParcelDecision):boolean {
            if (this.Parent === undefined || x.pclaId === undefined)
                return false;
            return x.pclaId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.ParcelClas {
            var self = this;
            return self.pclaId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpParcelClas.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get pclaId():Entities.ParcelClas {
            var self = this;
            return <Entities.ParcelClas>self.$scope.modelGrpParcelClas.selectedEntities[0];
        }









        public constructEntity(): Entities.ParcelDecision {
            var self = this;
            var ret = new Entities.ParcelDecision(
                /*padeId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*decisionLight:number*/ null,
                /*rowVersion:number*/ null,
                /*demaId:Entities.DecisionMaking*/ null,
                /*pclaId:Entities.ParcelClas*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.ParcelDecision = <Entities.ParcelDecision>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.pclaId = self.pclaId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.ParcelDecision): Entities.ParcelDecision {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.ParcelDecision, calledByParent: boolean= false): Entities.ParcelDecision {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.ParcelDecision.Create();
            ret.updateInstance(src);
            ret.padeId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.ParcelClas, newParent:Entities.ParcelClas) {
        
            this.model.totalItems = 0;

            this.getMergedItems(parcelDecisionList => {
                parcelDecisionList.forEach(parcelDecision => {
                    var parcelDecisionCloned = this.cloneEntity_internal(parcelDecision, true);
                    parcelDecisionCloned.pclaId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(parcelClas: Entities.ParcelClas) {
        

            var self = this;
            var toBeDeleted:Array<Entities.ParcelDecision> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var parcelDecision = <Entities.ParcelDecision>change.a
                if (parcelClas.getKey() === parcelDecision.pclaId.getKey()) {
                    toBeDeleted.push(parcelDecision);
                }
            }

            _.each(toBeDeleted, (x:Entities.ParcelDecision) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(parcelClas: Entities.ParcelClas, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteParcelDecisionList(parcelDecisionList: Entities.ParcelDecision[]) {
                if (parcelDecisionList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = parcelDecisionList[0];
                    var tail = parcelDecisionList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteParcelDecisionList(tail);
                    });
                }
            }


            this.getMergedItems(parcelDecisionList => {
                deleteParcelDecisionList(parcelDecisionList);
            }, false, parcelClas);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.ParcelDecision, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelClas_0_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelClas_0_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(parcelDecision:Entities.ParcelDecision):boolean  {
            return true;
        }

    }


    // GROUP GrpParcelsIssues

    export class ModelGrpParcelsIssuesBase extends Controllers.AbstractGroupTableModel {
        modelGrpGpDecision:ModelGrpGpDecision;
        modelGrpFmisDecision:ModelGrpFmisDecision;
        controller: ControllerGrpParcelsIssues;
        public get parcelsIssuesId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).parcelsIssuesId;
        }

        public set parcelsIssuesId(parcelsIssuesId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).parcelsIssuesId = parcelsIssuesId_newVal;
        }

        public get _parcelsIssuesId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcelsIssuesId;
        }

        public get dteCreated():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).dteCreated;
        }

        public set dteCreated(dteCreated_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).dteCreated = dteCreated_newVal;
        }

        public get _dteCreated():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteCreated;
        }

        public get status():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).status;
        }

        public set status(status_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).status = status_newVal;
        }

        public get _status():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._status;
        }

        public get dteStatusUpdate():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).dteStatusUpdate;
        }

        public set dteStatusUpdate(dteStatusUpdate_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).dteStatusUpdate = dteStatusUpdate_newVal;
        }

        public get _dteStatusUpdate():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteStatusUpdate;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get typeOfIssue():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).typeOfIssue;
        }

        public set typeOfIssue(typeOfIssue_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).typeOfIssue = typeOfIssue_newVal;
        }

        public get _typeOfIssue():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._typeOfIssue;
        }

        public get active():boolean {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).active;
        }

        public set active(active_newVal:boolean) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).active = active_newVal;
        }

        public get _active():NpTypes.UIBoolModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._active;
        }

        public get pclaId():Entities.ParcelClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).pclaId;
        }

        public set pclaId(pclaId_newVal:Entities.ParcelClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).pclaId = pclaId_newVal;
        }

        public get _pclaId():NpTypes.UIManyToOneModel<Entities.ParcelClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._pclaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpParcelsIssues) { super($scope); }
    }



    export interface IScopeGrpParcelsIssuesBase extends Controllers.IAbstractTableGroupScope, IScopeGrpParcelClasBase{
        globals: Globals;
        modelGrpParcelsIssues : ModelGrpParcelsIssues;
        GrpParcelsIssues_itm__status_disabled(parcelsIssues:Entities.ParcelsIssues):boolean; 
        GrpParcelsIssues_itm__dteStatusUpdate_disabled(parcelsIssues:Entities.ParcelsIssues):boolean; 
        GrpParcelsIssues_itm__dteCreated_disabled(parcelsIssues:Entities.ParcelsIssues):boolean; 
        child_group_GrpGpDecision_isInvisible():boolean; 
        child_group_GrpFmisDecision_isInvisible():boolean; 

    }



    export class ControllerGrpParcelsIssuesBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpParcelsIssues,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpParcelsIssues)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpParcelsIssuesBase = this;
            model.controller = <ControllerGrpParcelsIssues>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpParcelsIssues = self.model;

            $scope.GrpParcelsIssues_itm__status_disabled = 
                (parcelsIssues:Entities.ParcelsIssues) => {
                    return self.GrpParcelsIssues_itm__status_disabled(parcelsIssues);
                };
            $scope.GrpParcelsIssues_itm__dteStatusUpdate_disabled = 
                (parcelsIssues:Entities.ParcelsIssues) => {
                    return self.GrpParcelsIssues_itm__dteStatusUpdate_disabled(parcelsIssues);
                };
            $scope.GrpParcelsIssues_itm__dteCreated_disabled = 
                (parcelsIssues:Entities.ParcelsIssues) => {
                    return self.GrpParcelsIssues_itm__dteCreated_disabled(parcelsIssues);
                };
            $scope.child_group_GrpGpDecision_isInvisible = 
                () => {
                    return self.child_group_GrpGpDecision_isInvisible();
                };
            $scope.child_group_GrpFmisDecision_isInvisible = 
                () => {
                    return self.child_group_GrpFmisDecision_isInvisible();
                };


            $scope.pageModel.modelGrpParcelsIssues = $scope.modelGrpParcelsIssues;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpParcelClas.modelGrpParcelsIssues = $scope.modelGrpParcelsIssues;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpParcelClas.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelsIssues[] , oldVisible:Entities.ParcelsIssues[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.ParcelsIssues.gpDecisionCollection = (parcelsIssues, func) => {
                this.model.modelGrpGpDecision.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, parcelsIssues);
            }
            //bind entity child collection with child controller merged items
            Entities.ParcelsIssues.fmisDecisionCollection = (parcelsIssues, func) => {
                this.model.modelGrpFmisDecision.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, parcelsIssues);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.ParcelsIssues.gpDecisionCollection = null;//(parcelsIssues, func) => { }
                //unbind entity child collection with child controller merged items
                Entities.ParcelsIssues.fmisDecisionCollection = null;//(parcelsIssues, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpParcelsIssues";
        }
        public get HtmlDivId(): string {
            return "Dashboard_ControllerGrpParcelsIssues";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new NumberItem (
                        (ent?: Entities.ParcelsIssues) => 'Current Status',
                        false ,
                        (ent?: Entities.ParcelsIssues) => ent.status,  
                        (ent?: Entities.ParcelsIssues) => ent._status,  
                        (ent?: Entities.ParcelsIssues) => false, //isRequired
                        (vl: number, ent?: Entities.ParcelsIssues) => new NpTypes.ValidationResult(true, ""), 
                        () => 0,
                        () => 999,
                        0),
                    new DateItem (
                        (ent?: Entities.ParcelsIssues) => 'Date Updated',
                        false ,
                        (ent?: Entities.ParcelsIssues) => ent.dteStatusUpdate,  
                        (ent?: Entities.ParcelsIssues) => ent._dteStatusUpdate,  
                        (ent?: Entities.ParcelsIssues) => false, //isRequired
                        (vl: Date, ent?: Entities.ParcelsIssues) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new DateItem (
                        (ent?: Entities.ParcelsIssues) => 'Date Created',
                        false ,
                        (ent?: Entities.ParcelsIssues) => ent.dteCreated,  
                        (ent?: Entities.ParcelsIssues) => ent._dteCreated,  
                        (ent?: Entities.ParcelsIssues) => false, //isRequired
                        (vl: Date, ent?: Entities.ParcelsIssues) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined)
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Parcel\'s Issues";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'status', displayName:'getALString("Current Status", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssues_itm__status' data-ng-model='row.entity.status' data-np-ui-model='row.entity._status' data-ng-change='markEntityAsUpdated(row.entity,&quot;status&quot;)' data-ng-readonly='GrpParcelsIssues_itm__status_disabled(row.entity)' data-np-number='dummy' data-np-min='0' data-np-max='999' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteStatusUpdate', displayName:'getALString("Date Updated", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssues_itm__dteStatusUpdate' data-ng-model='row.entity.dteStatusUpdate' data-np-ui-model='row.entity._dteStatusUpdate' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteStatusUpdate&quot;)' data-ng-readonly='GrpParcelsIssues_itm__dteStatusUpdate_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteCreated', displayName:'getALString("Date Created", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssues_itm__dteCreated' data-ng-model='row.entity.dteCreated' data-np-ui-model='row.entity._dteCreated' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteCreated&quot;)' data-ng-readonly='GrpParcelsIssues_itm__dteCreated_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpParcelsIssues():ModelGrpParcelsIssues {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "ParcelsIssues";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.ParcelClas): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.ParcelsIssues.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.pclaId = this.Parent;
                } else {
                    x.pclaId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.ParcelsIssues {
            var self = this;
            return <Entities.ParcelsIssues>self.$scope.modelGrpParcelsIssues.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.ParcelsIssues, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpParcelClas.controller.isEntityLocked(<Entities.ParcelClas>cur.pclaId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelsIssues/findAllByCriteriaRange_DashboardGrpParcelsIssues_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelsIssues/findAllByCriteriaRange_DashboardGrpParcelsIssues";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelsIssues/findAllByCriteriaRange_DashboardGrpParcelsIssues_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.ParcelsIssues[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.ParcelsIssues[]) => void> = [];


        public getMergedItems(func: (items: Entities.ParcelsIssues[]) => void, bContinuousCaller:boolean=true, parent?: Entities.ParcelClas, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.ParcelsIssues[]):void {
                var mergedEntities = <Entities.ParcelsIssues[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.ParcelsIssues>e.a).pclaId)).
                        map(e => <Entities.ParcelsIssues>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "ParcelsIssues/findAllByCriteriaRange_DashboardGrpParcelsIssues";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['pclaId_pclaId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.ParcelsIssues[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.ParcelsIssues):boolean {
            if (this.Parent === undefined || x.pclaId === undefined)
                return false;
            return x.pclaId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.ParcelClas {
            var self = this;
            return self.pclaId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpParcelClas.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get pclaId():Entities.ParcelClas {
            var self = this;
            return <Entities.ParcelClas>self.$scope.modelGrpParcelClas.selectedEntities[0];
        }









        public constructEntity(): Entities.ParcelsIssues {
            var self = this;
            var ret = new Entities.ParcelsIssues(
                /*parcelsIssuesId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*dteCreated:Date*/ null,
                /*status:number*/ null,
                /*dteStatusUpdate:Date*/ null,
                /*rowVersion:number*/ null,
                /*typeOfIssue:number*/ null,
                /*active:boolean*/ null,
                /*pclaId:Entities.ParcelClas*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.ParcelsIssues = <Entities.ParcelsIssues>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.pclaId = self.pclaId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.ParcelsIssues): Entities.ParcelsIssues {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.ParcelsIssues, calledByParent: boolean= false): Entities.ParcelsIssues {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.ParcelsIssues.Create();
            ret.updateInstance(src);
            ret.parcelsIssuesId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
                this.modelGrpParcelsIssues.modelGrpGpDecision.controller.cloneAllEntitiesUnderParent(src, ret);
                this.modelGrpParcelsIssues.modelGrpFmisDecision.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.ParcelClas, newParent:Entities.ParcelClas) {
        
            this.model.totalItems = 0;

            this.getMergedItems(parcelsIssuesList => {
                parcelsIssuesList.forEach(parcelsIssues => {
                    var parcelsIssuesCloned = this.cloneEntity_internal(parcelsIssues, true);
                    parcelsIssuesCloned.pclaId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(parcelClas: Entities.ParcelClas) {
        

            var self = this;
            var toBeDeleted:Array<Entities.ParcelsIssues> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var parcelsIssues = <Entities.ParcelsIssues>change.a
                if (parcelClas.getKey() === parcelsIssues.pclaId.getKey()) {
                    toBeDeleted.push(parcelsIssues);
                }
            }

            _.each(toBeDeleted, (x:Entities.ParcelsIssues) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
                self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteNewEntitiesUnderParent(x);
                self.modelGrpParcelsIssues.modelGrpFmisDecision.controller.deleteNewEntitiesUnderParent(x);
            });

        }

        public deleteAllEntitiesUnderParent(parcelClas: Entities.ParcelClas, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteParcelsIssuesList(parcelsIssuesList: Entities.ParcelsIssues[]) {
                if (parcelsIssuesList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = parcelsIssuesList[0];
                    var tail = parcelsIssuesList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteParcelsIssuesList(tail);
                    });
                }
            }


            this.getMergedItems(parcelsIssuesList => {
                deleteParcelsIssuesList(parcelsIssuesList);
            }, false, parcelClas);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpParcelsIssues.modelGrpGpDecision.controller,
                    self.modelGrpParcelsIssues.modelGrpFmisDecision.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.ParcelsIssues, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteAllEntitiesUnderParent(ent, () => {
                    self.modelGrpParcelsIssues.modelGrpFmisDecision.controller.deleteAllEntitiesUnderParent(ent, () => {
                        super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteNewEntitiesUnderParent(ent);
                    self.modelGrpParcelsIssues.modelGrpFmisDecision.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public GrpParcelsIssues_itm__status_disabled(parcelsIssues:Entities.ParcelsIssues):boolean {
            var self = this;
            if (parcelsIssues === undefined || parcelsIssues === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelsIssues, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpParcelsIssues_itm__dteStatusUpdate_disabled(parcelsIssues:Entities.ParcelsIssues):boolean {
            var self = this;
            if (parcelsIssues === undefined || parcelsIssues === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelsIssues, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpParcelsIssues_itm__dteCreated_disabled(parcelsIssues:Entities.ParcelsIssues):boolean {
            var self = this;
            if (parcelsIssues === undefined || parcelsIssues === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelsIssues, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public _isDisabled():boolean { 
            if (true)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelClas_0_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(parcelsIssues:Entities.ParcelsIssues):boolean  {
            return true;
        }
        public child_group_GrpGpDecision_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpGpDecision === undefined) || (self.model.modelGrpGpDecision.controller === undefined))
                return false;
            return self.model.modelGrpGpDecision.controller._isInvisible()
        }
        public child_group_GrpFmisDecision_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpFmisDecision === undefined) || (self.model.modelGrpFmisDecision.controller === undefined))
                return false;
            return self.model.modelGrpFmisDecision.controller._isInvisible()
        }

    }


    // GROUP GrpGpDecision

    export class ModelGrpGpDecisionBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpGpDecision;
        public get gpDecisionsId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).gpDecisionsId;
        }

        public set gpDecisionsId(gpDecisionsId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).gpDecisionsId = gpDecisionsId_newVal;
        }

        public get _gpDecisionsId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._gpDecisionsId;
        }

        public get cropOk():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).cropOk;
        }

        public set cropOk(cropOk_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).cropOk = cropOk_newVal;
        }

        public get _cropOk():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cropOk;
        }

        public get landcoverOk():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).landcoverOk;
        }

        public set landcoverOk(landcoverOk_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).landcoverOk = landcoverOk_newVal;
        }

        public get _landcoverOk():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._landcoverOk;
        }

        public get dteInsert():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).dteInsert;
        }

        public set dteInsert(dteInsert_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).dteInsert = dteInsert_newVal;
        }

        public get _dteInsert():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteInsert;
        }

        public get usrInsert():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).usrInsert;
        }

        public set usrInsert(usrInsert_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).usrInsert = usrInsert_newVal;
        }

        public get _usrInsert():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._usrInsert;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get parcelsIssuesId():Entities.ParcelsIssues {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).parcelsIssuesId;
        }

        public set parcelsIssuesId(parcelsIssuesId_newVal:Entities.ParcelsIssues) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).parcelsIssuesId = parcelsIssuesId_newVal;
        }

        public get _parcelsIssuesId():NpTypes.UIManyToOneModel<Entities.ParcelsIssues> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcelsIssuesId;
        }

        public get cultId():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).cultId;
        }

        public set cultId(cultId_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).cultId = cultId_newVal;
        }

        public get _cultId():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultId;
        }

        public get cotyId():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).cotyId;
        }

        public set cotyId(cotyId_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).cotyId = cotyId_newVal;
        }

        public get _cotyId():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpGpDecision) { super($scope); }
    }



    export interface IScopeGrpGpDecisionBase extends Controllers.IAbstractTableGroupScope, IScopeGrpParcelsIssuesBase{
        globals: Globals;
        modelGrpGpDecision : ModelGrpGpDecision;
        GrpGpDecision_itm__cultId_disabled(gpDecision:Entities.GpDecision):boolean; 
        showLov_GrpGpDecision_itm__cultId(gpDecision:Entities.GpDecision):void; 
        GrpGpDecision_itm__cotyId_disabled(gpDecision:Entities.GpDecision):boolean; 
        showLov_GrpGpDecision_itm__cotyId(gpDecision:Entities.GpDecision):void; 

    }



    export class ControllerGrpGpDecisionBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpGpDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpGpDecision)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 2, maxLinesInHeader: 1});
            var self:ControllerGrpGpDecisionBase = this;
            model.controller = <ControllerGrpGpDecision>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpGpDecision = self.model;

            $scope.GrpGpDecision_itm__cultId_disabled = 
                (gpDecision:Entities.GpDecision) => {
                    return self.GrpGpDecision_itm__cultId_disabled(gpDecision);
                };
            $scope.showLov_GrpGpDecision_itm__cultId = 
                (gpDecision:Entities.GpDecision) => {
                    $timeout( () => {        
                        self.showLov_GrpGpDecision_itm__cultId(gpDecision);
                    }, 0);
                };
            $scope.GrpGpDecision_itm__cotyId_disabled = 
                (gpDecision:Entities.GpDecision) => {
                    return self.GrpGpDecision_itm__cotyId_disabled(gpDecision);
                };
            $scope.showLov_GrpGpDecision_itm__cotyId = 
                (gpDecision:Entities.GpDecision) => {
                    $timeout( () => {        
                        self.showLov_GrpGpDecision_itm__cotyId(gpDecision);
                    }, 0);
                };


            $scope.pageModel.modelGrpGpDecision = $scope.modelGrpGpDecision;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpParcelsIssues.modelGrpGpDecision = $scope.modelGrpGpDecision;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpDecision[] , oldVisible:Entities.GpDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpGpDecision";
        }
        public get HtmlDivId(): string {
            return "Dashboard_ControllerGrpGpDecision";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new LovItem (
                        (ent?: Entities.GpDecision) => 'Presumed Crop',
                        false ,
                        (ent?: Entities.GpDecision) => ent.cultId,  
                        (ent?: Entities.GpDecision) => ent._cultId,  
                        (ent?: Entities.GpDecision) => true, //isRequired
                        (vl: Entities.Cultivation, ent?: Entities.GpDecision) => new NpTypes.ValidationResult(true, "")),
                    new LovItem (
                        (ent?: Entities.GpDecision) => 'Presumed Land Cover',
                        false ,
                        (ent?: Entities.GpDecision) => ent.cotyId,  
                        (ent?: Entities.GpDecision) => ent._cotyId,  
                        (ent?: Entities.GpDecision) => true, //isRequired
                        (vl: Entities.CoverType, ent?: Entities.GpDecision) => new NpTypes.ValidationResult(true, "")),
                    new DateItem (
                        (ent?: Entities.GpDecision) => 'Date',
                        false ,
                        (ent?: Entities.GpDecision) => ent.dteInsert,  
                        (ent?: Entities.GpDecision) => ent._dteInsert,  
                        (ent?: Entities.GpDecision) => true, //isRequired
                        (vl: Date, ent?: Entities.GpDecision) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new TextItem (
                        (ent?: Entities.GpDecision) => 'User',
                        false ,
                        (ent?: Entities.GpDecision) => ent.usrInsert,  
                        (ent?: Entities.GpDecision) => ent._usrInsert,  
                        (ent?: Entities.GpDecision) => false, 
                        (vl: string, ent?: Entities.GpDecision) => new NpTypes.ValidationResult(true, ""), 
                        undefined)
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Geotagged Photos Decisions";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'cultId.name', displayName:'getALString("Presumed Crop", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpGpDecision_itm__cultId_disabled(row.entity) && showLov_GrpGpDecision_itm__cultId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpGpDecision_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpGpDecision_itm__cultId(row.entity)'   data-ng-disabled=\"GrpGpDecision_itm__cultId_disabled(row.entity)\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'cotyId.name', displayName:'getALString("Presumed Land Cover", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpGpDecision_itm__cotyId_disabled(row.entity) && showLov_GrpGpDecision_itm__cotyId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cotyId.name' data-np-ui-model='row.entity._cotyId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpGpDecision_itm__cotyId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpGpDecision_itm__cotyId(row.entity)'   data-ng-disabled=\"GrpGpDecision_itm__cotyId_disabled(row.entity)\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteInsert', displayName:'getALString("Date", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpDecision_itm__dteInsert' data-ng-model='row.entity.dteInsert' data-np-ui-model='row.entity._dteInsert' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteInsert&quot;)' data-np-required='true' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy hh:mm' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'usrInsert', displayName:'getALString("User", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpDecision_itm__usrInsert' data-ng-model='row.entity.usrInsert' data-np-ui-model='row.entity._usrInsert' data-ng-change='markEntityAsUpdated(row.entity,&quot;usrInsert&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpGpDecision():ModelGrpGpDecision {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "GpDecision";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.ParcelsIssues): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.GpDecision.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.parcelsIssuesId = this.Parent;
                } else {
                    x.parcelsIssuesId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.GpDecision {
            var self = this;
            return <Entities.GpDecision>self.$scope.modelGrpGpDecision.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.GpDecision, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(<Entities.ParcelsIssues>cur.parcelsIssuesId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_DashboardGrpGpDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_DashboardGrpGpDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_DashboardGrpGpDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.GpDecision[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.GpDecision[]) => void> = [];


        public getMergedItems(func: (items: Entities.GpDecision[]) => void, bContinuousCaller:boolean=true, parent?: Entities.ParcelsIssues, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.GpDecision[]):void {
                var mergedEntities = <Entities.GpDecision[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.GpDecision>e.a).parcelsIssuesId)).
                        map(e => <Entities.GpDecision>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "GpDecision/findAllByCriteriaRange_DashboardGrpGpDecision";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.GpDecision[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.GpDecision):boolean {
            if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                return false;
            return x.parcelsIssuesId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.ParcelsIssues {
            var self = this;
            return self.parcelsIssuesId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpParcelsIssues.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get parcelsIssuesId():Entities.ParcelsIssues {
            var self = this;
            return <Entities.ParcelsIssues>self.$scope.modelGrpParcelsIssues.selectedEntities[0];
        }









        public constructEntity(): Entities.GpDecision {
            var self = this;
            var ret = new Entities.GpDecision(
                /*gpDecisionsId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*cropOk:number*/ null,
                /*landcoverOk:number*/ null,
                /*dteInsert:Date*/ null,
                /*usrInsert:string*/ null,
                /*rowVersion:number*/ null,
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null,
                /*cultId:Entities.Cultivation*/ null,
                /*cotyId:Entities.CoverType*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.GpDecision = <Entities.GpDecision>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.parcelsIssuesId = self.parcelsIssuesId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.GpDecision): Entities.GpDecision {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.GpDecision, calledByParent: boolean= false): Entities.GpDecision {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.GpDecision.Create();
            ret.updateInstance(src);
            ret.gpDecisionsId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.ParcelsIssues, newParent:Entities.ParcelsIssues) {
        
            this.model.totalItems = 0;

            this.getMergedItems(gpDecisionList => {
                gpDecisionList.forEach(gpDecision => {
                    var gpDecisionCloned = this.cloneEntity_internal(gpDecision, true);
                    gpDecisionCloned.parcelsIssuesId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(parcelsIssues: Entities.ParcelsIssues) {
        

            var self = this;
            var toBeDeleted:Array<Entities.GpDecision> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var gpDecision = <Entities.GpDecision>change.a
                if (parcelsIssues.getKey() === gpDecision.parcelsIssuesId.getKey()) {
                    toBeDeleted.push(gpDecision);
                }
            }

            _.each(toBeDeleted, (x:Entities.GpDecision) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(parcelsIssues: Entities.ParcelsIssues, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteGpDecisionList(gpDecisionList: Entities.GpDecision[]) {
                if (gpDecisionList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = gpDecisionList[0];
                    var tail = gpDecisionList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteGpDecisionList(tail);
                    });
                }
            }


            this.getMergedItems(gpDecisionList => {
                deleteGpDecisionList(gpDecisionList);
            }, false, parcelsIssues);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.GpDecision, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public isCheckMade(gpDecision:Entities.GpDecision):boolean { 
            console.warn("Unimplemented function isCheckMade()");
            return false; 
        }
        public GrpGpDecision_itm__cultId_disabled(gpDecision:Entities.GpDecision):boolean {
            var self = this;
            if (gpDecision === undefined || gpDecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpDecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isCheckMade(gpDecision);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpGpDecision_itm__cultId:ModelLovCultivationBase;
        public showLov_GrpGpDecision_itm__cultId(gpDecision:Entities.GpDecision) {
            var self = this;
            if (gpDecision === undefined)
                return;
            var uimodel:NpTypes.IUIModel = gpDecision._cultId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Cultivation';
            dialogOptions.previousModel= self.cachedLovModel_GrpGpDecision_itm__cultId;
            dialogOptions.className = "ControllerLovCultivation";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var cultivation1:Entities.Cultivation =   <Entities.Cultivation>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(cultivation1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(cultivation1) && cultivation1.isEqual(gpDecision.cultId))
                    return;
                gpDecision.cultId = cultivation1;
                self.markEntityAsUpdated(gpDecision, 'GrpGpDecision_itm__cultId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpGpDecision_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpGpDecision_itm__cotyId_disabled(gpDecision:Entities.GpDecision):boolean {
            var self = this;
            if (gpDecision === undefined || gpDecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpDecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isCheckMade(gpDecision);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpGpDecision_itm__cotyId:ModelLovCoverTypeBase;
        public showLov_GrpGpDecision_itm__cotyId(gpDecision:Entities.GpDecision) {
            var self = this;
            if (gpDecision === undefined)
                return;
            var uimodel:NpTypes.IUIModel = gpDecision._cotyId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Land Cover';
            dialogOptions.previousModel= self.cachedLovModel_GrpGpDecision_itm__cotyId;
            dialogOptions.className = "ControllerLovCoverType";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var coverType1:Entities.CoverType =   <Entities.CoverType>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(coverType1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(coverType1) && coverType1.isEqual(gpDecision.cotyId))
                    return;
                gpDecision.cotyId = coverType1;
                self.markEntityAsUpdated(gpDecision, 'GrpGpDecision_itm__cotyId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCoverTypeBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpGpDecision_itm__cotyId = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCoverTypeBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public _isDisabled():boolean { 
            if (true)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(gpDecision:Entities.GpDecision):boolean  {
            return true;
        }

    }


    // GROUP GrpFmisDecision

    export class ModelGrpFmisDecisionBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpFmisDecision;
        public get fmisDecisionsId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisDecision>this.selectedEntities[0]).fmisDecisionsId;
        }

        public set fmisDecisionsId(fmisDecisionsId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisDecision>this.selectedEntities[0]).fmisDecisionsId = fmisDecisionsId_newVal;
        }

        public get _fmisDecisionsId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._fmisDecisionsId;
        }

        public get cropOk():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisDecision>this.selectedEntities[0]).cropOk;
        }

        public set cropOk(cropOk_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisDecision>this.selectedEntities[0]).cropOk = cropOk_newVal;
        }

        public get _cropOk():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cropOk;
        }

        public get landcoverOk():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisDecision>this.selectedEntities[0]).landcoverOk;
        }

        public set landcoverOk(landcoverOk_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisDecision>this.selectedEntities[0]).landcoverOk = landcoverOk_newVal;
        }

        public get _landcoverOk():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._landcoverOk;
        }

        public get dteInsert():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisDecision>this.selectedEntities[0]).dteInsert;
        }

        public set dteInsert(dteInsert_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisDecision>this.selectedEntities[0]).dteInsert = dteInsert_newVal;
        }

        public get _dteInsert():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteInsert;
        }

        public get usrInsert():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisDecision>this.selectedEntities[0]).usrInsert;
        }

        public set usrInsert(usrInsert_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisDecision>this.selectedEntities[0]).usrInsert = usrInsert_newVal;
        }

        public get _usrInsert():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._usrInsert;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisDecision>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisDecision>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get parcelsIssuesId():Entities.ParcelsIssues {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisDecision>this.selectedEntities[0]).parcelsIssuesId;
        }

        public set parcelsIssuesId(parcelsIssuesId_newVal:Entities.ParcelsIssues) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisDecision>this.selectedEntities[0]).parcelsIssuesId = parcelsIssuesId_newVal;
        }

        public get _parcelsIssuesId():NpTypes.UIManyToOneModel<Entities.ParcelsIssues> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcelsIssuesId;
        }

        public get cultId():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisDecision>this.selectedEntities[0]).cultId;
        }

        public set cultId(cultId_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisDecision>this.selectedEntities[0]).cultId = cultId_newVal;
        }

        public get _cultId():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultId;
        }

        public get cotyId():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FmisDecision>this.selectedEntities[0]).cotyId;
        }

        public set cotyId(cotyId_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FmisDecision>this.selectedEntities[0]).cotyId = cotyId_newVal;
        }

        public get _cotyId():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpFmisDecision) { super($scope); }
    }



    export interface IScopeGrpFmisDecisionBase extends Controllers.IAbstractTableGroupScope, IScopeGrpParcelsIssuesBase{
        globals: Globals;
        modelGrpFmisDecision : ModelGrpFmisDecision;
        GrpFmisDecision_itm__cultId_disabled(fmisDecision:Entities.FmisDecision):boolean; 
        showLov_GrpFmisDecision_itm__cultId(fmisDecision:Entities.FmisDecision):void; 
        GrpFmisDecision_itm__cotyId_disabled(fmisDecision:Entities.FmisDecision):boolean; 
        showLov_GrpFmisDecision_itm__cotyId(fmisDecision:Entities.FmisDecision):void; 

    }



    export class ControllerGrpFmisDecisionBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpFmisDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpFmisDecision)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 2, maxLinesInHeader: 1, maxPageVisibleRows: 2});
            var self:ControllerGrpFmisDecisionBase = this;
            model.controller = <ControllerGrpFmisDecision>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpFmisDecision = self.model;

            $scope.GrpFmisDecision_itm__cultId_disabled = 
                (fmisDecision:Entities.FmisDecision) => {
                    return self.GrpFmisDecision_itm__cultId_disabled(fmisDecision);
                };
            $scope.showLov_GrpFmisDecision_itm__cultId = 
                (fmisDecision:Entities.FmisDecision) => {
                    $timeout( () => {        
                        self.showLov_GrpFmisDecision_itm__cultId(fmisDecision);
                    }, 0);
                };
            $scope.GrpFmisDecision_itm__cotyId_disabled = 
                (fmisDecision:Entities.FmisDecision) => {
                    return self.GrpFmisDecision_itm__cotyId_disabled(fmisDecision);
                };
            $scope.showLov_GrpFmisDecision_itm__cotyId = 
                (fmisDecision:Entities.FmisDecision) => {
                    $timeout( () => {        
                        self.showLov_GrpFmisDecision_itm__cotyId(fmisDecision);
                    }, 0);
                };


            $scope.pageModel.modelGrpFmisDecision = $scope.modelGrpFmisDecision;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpParcelsIssues.modelGrpFmisDecision = $scope.modelGrpFmisDecision;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.FmisDecision[] , oldVisible:Entities.FmisDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpFmisDecision";
        }
        public get HtmlDivId(): string {
            return "Dashboard_ControllerGrpFmisDecision";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new LovItem (
                        (ent?: Entities.FmisDecision) => 'Presumed Crop',
                        false ,
                        (ent?: Entities.FmisDecision) => ent.cultId,  
                        (ent?: Entities.FmisDecision) => ent._cultId,  
                        (ent?: Entities.FmisDecision) => true, //isRequired
                        (vl: Entities.Cultivation, ent?: Entities.FmisDecision) => new NpTypes.ValidationResult(true, "")),
                    new LovItem (
                        (ent?: Entities.FmisDecision) => 'Presumed Land Cover',
                        false ,
                        (ent?: Entities.FmisDecision) => ent.cotyId,  
                        (ent?: Entities.FmisDecision) => ent._cotyId,  
                        (ent?: Entities.FmisDecision) => true, //isRequired
                        (vl: Entities.CoverType, ent?: Entities.FmisDecision) => new NpTypes.ValidationResult(true, "")),
                    new DateItem (
                        (ent?: Entities.FmisDecision) => 'Date',
                        false ,
                        (ent?: Entities.FmisDecision) => ent.dteInsert,  
                        (ent?: Entities.FmisDecision) => ent._dteInsert,  
                        (ent?: Entities.FmisDecision) => false, //isRequired
                        (vl: Date, ent?: Entities.FmisDecision) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new TextItem (
                        (ent?: Entities.FmisDecision) => 'User',
                        false ,
                        (ent?: Entities.FmisDecision) => ent.usrInsert,  
                        (ent?: Entities.FmisDecision) => ent._usrInsert,  
                        (ent?: Entities.FmisDecision) => false, 
                        (vl: string, ent?: Entities.FmisDecision) => new NpTypes.ValidationResult(true, ""), 
                        undefined)
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "FMIS Calendars Decisions";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'cultId.name', displayName:'getALString("Presumed Crop", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpFmisDecision_itm__cultId_disabled(row.entity) && showLov_GrpFmisDecision_itm__cultId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpFmisDecision_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpFmisDecision_itm__cultId(row.entity)'   data-ng-disabled=\"GrpFmisDecision_itm__cultId_disabled(row.entity)\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'cotyId.name', displayName:'getALString("Presumed Land Cover", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpFmisDecision_itm__cotyId_disabled(row.entity) && showLov_GrpFmisDecision_itm__cotyId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cotyId.name' data-np-ui-model='row.entity._cotyId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpFmisDecision_itm__cotyId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpFmisDecision_itm__cotyId(row.entity)'   data-ng-disabled=\"GrpFmisDecision_itm__cotyId_disabled(row.entity)\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteInsert', displayName:'getALString("Date", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpFmisDecision_itm__dteInsert' data-ng-model='row.entity.dteInsert' data-np-ui-model='row.entity._dteInsert' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteInsert&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy hh:mm' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'usrInsert', displayName:'getALString("User", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpFmisDecision_itm__usrInsert' data-ng-model='row.entity.usrInsert' data-np-ui-model='row.entity._usrInsert' data-ng-change='markEntityAsUpdated(row.entity,&quot;usrInsert&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpFmisDecision():ModelGrpFmisDecision {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "FmisDecision";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.ParcelsIssues): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.FmisDecision.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.parcelsIssuesId = this.Parent;
                } else {
                    x.parcelsIssuesId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.FmisDecision {
            var self = this;
            return <Entities.FmisDecision>self.$scope.modelGrpFmisDecision.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.FmisDecision, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(<Entities.ParcelsIssues>cur.parcelsIssuesId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "FmisDecision/findAllByCriteriaRange_DashboardGrpFmisDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "FmisDecision/findAllByCriteriaRange_DashboardGrpFmisDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "FmisDecision/findAllByCriteriaRange_DashboardGrpFmisDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.FmisDecision[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.FmisDecision[]) => void> = [];


        public getMergedItems(func: (items: Entities.FmisDecision[]) => void, bContinuousCaller:boolean=true, parent?: Entities.ParcelsIssues, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.FmisDecision[]):void {
                var mergedEntities = <Entities.FmisDecision[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.FmisDecision>e.a).parcelsIssuesId)).
                        map(e => <Entities.FmisDecision>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "FmisDecision/findAllByCriteriaRange_DashboardGrpFmisDecision";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.FmisDecision[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.FmisDecision):boolean {
            if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                return false;
            return x.parcelsIssuesId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.ParcelsIssues {
            var self = this;
            return self.parcelsIssuesId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpParcelsIssues.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get parcelsIssuesId():Entities.ParcelsIssues {
            var self = this;
            return <Entities.ParcelsIssues>self.$scope.modelGrpParcelsIssues.selectedEntities[0];
        }









        public constructEntity(): Entities.FmisDecision {
            var self = this;
            var ret = new Entities.FmisDecision(
                /*fmisDecisionsId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*cropOk:number*/ null,
                /*landcoverOk:number*/ null,
                /*dteInsert:Date*/ null,
                /*usrInsert:string*/ null,
                /*rowVersion:number*/ null,
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null,
                /*cultId:Entities.Cultivation*/ null,
                /*cotyId:Entities.CoverType*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.FmisDecision = <Entities.FmisDecision>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.parcelsIssuesId = self.parcelsIssuesId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.FmisDecision): Entities.FmisDecision {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.FmisDecision, calledByParent: boolean= false): Entities.FmisDecision {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.FmisDecision.Create();
            ret.updateInstance(src);
            ret.fmisDecisionsId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.ParcelsIssues, newParent:Entities.ParcelsIssues) {
        
            this.model.totalItems = 0;

            this.getMergedItems(fmisDecisionList => {
                fmisDecisionList.forEach(fmisDecision => {
                    var fmisDecisionCloned = this.cloneEntity_internal(fmisDecision, true);
                    fmisDecisionCloned.parcelsIssuesId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(parcelsIssues: Entities.ParcelsIssues) {
        

            var self = this;
            var toBeDeleted:Array<Entities.FmisDecision> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var fmisDecision = <Entities.FmisDecision>change.a
                if (parcelsIssues.getKey() === fmisDecision.parcelsIssuesId.getKey()) {
                    toBeDeleted.push(fmisDecision);
                }
            }

            _.each(toBeDeleted, (x:Entities.FmisDecision) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(parcelsIssues: Entities.ParcelsIssues, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteFmisDecisionList(fmisDecisionList: Entities.FmisDecision[]) {
                if (fmisDecisionList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = fmisDecisionList[0];
                    var tail = fmisDecisionList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteFmisDecisionList(tail);
                    });
                }
            }


            this.getMergedItems(fmisDecisionList => {
                deleteFmisDecisionList(fmisDecisionList);
            }, false, parcelsIssues);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.FmisDecision, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public isCheckMade(fmisDecision:Entities.FmisDecision):boolean { 
            console.warn("Unimplemented function isCheckMade()");
            return false; 
        }
        public GrpFmisDecision_itm__cultId_disabled(fmisDecision:Entities.FmisDecision):boolean {
            var self = this;
            if (fmisDecision === undefined || fmisDecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(fmisDecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isCheckMade(fmisDecision);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpFmisDecision_itm__cultId:ModelLovCultivationBase;
        public showLov_GrpFmisDecision_itm__cultId(fmisDecision:Entities.FmisDecision) {
            var self = this;
            if (fmisDecision === undefined)
                return;
            var uimodel:NpTypes.IUIModel = fmisDecision._cultId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Cultivation';
            dialogOptions.previousModel= self.cachedLovModel_GrpFmisDecision_itm__cultId;
            dialogOptions.className = "ControllerLovCultivation";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var cultivation1:Entities.Cultivation =   <Entities.Cultivation>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(cultivation1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(cultivation1) && cultivation1.isEqual(fmisDecision.cultId))
                    return;
                fmisDecision.cultId = cultivation1;
                self.markEntityAsUpdated(fmisDecision, 'GrpFmisDecision_itm__cultId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpFmisDecision_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpFmisDecision_itm__cotyId_disabled(fmisDecision:Entities.FmisDecision):boolean {
            var self = this;
            if (fmisDecision === undefined || fmisDecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(fmisDecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isCheckMade(fmisDecision);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpFmisDecision_itm__cotyId:ModelLovCoverTypeBase;
        public showLov_GrpFmisDecision_itm__cotyId(fmisDecision:Entities.FmisDecision) {
            var self = this;
            if (fmisDecision === undefined)
                return;
            var uimodel:NpTypes.IUIModel = fmisDecision._cotyId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Land Cover';
            dialogOptions.previousModel= self.cachedLovModel_GrpFmisDecision_itm__cotyId;
            dialogOptions.className = "ControllerLovCoverType";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var coverType1:Entities.CoverType =   <Entities.CoverType>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(coverType1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(coverType1) && coverType1.isEqual(fmisDecision.cotyId))
                    return;
                fmisDecision.cotyId = coverType1;
                self.markEntityAsUpdated(fmisDecision, 'GrpFmisDecision_itm__cotyId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCoverTypeBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpFmisDecision_itm__cotyId = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCoverTypeBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public _isDisabled():boolean { 
            if (true)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(fmisDecision:Entities.FmisDecision):boolean  {
            return true;
        }

    }


    // GROUP GrpIntegrateddecision

    export class ModelGrpIntegrateddecisionBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpIntegrateddecision;
        public get integrateddecisionsId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).integrateddecisionsId;
        }

        public set integrateddecisionsId(integrateddecisionsId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).integrateddecisionsId = integrateddecisionsId_newVal;
        }

        public get _integrateddecisionsId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._integrateddecisionsId;
        }

        public get decisionCode():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).decisionCode;
        }

        public set decisionCode(decisionCode_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).decisionCode = decisionCode_newVal;
        }

        public get _decisionCode():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionCode;
        }

        public get dteUpdate():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).dteUpdate;
        }

        public set dteUpdate(dteUpdate_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).dteUpdate = dteUpdate_newVal;
        }

        public get _dteUpdate():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteUpdate;
        }

        public get usrUpdate():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).usrUpdate;
        }

        public set usrUpdate(usrUpdate_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).usrUpdate = usrUpdate_newVal;
        }

        public get _usrUpdate():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._usrUpdate;
        }

        public get pclaId():Entities.ParcelClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).pclaId;
        }

        public set pclaId(pclaId_newVal:Entities.ParcelClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).pclaId = pclaId_newVal;
        }

        public get _pclaId():NpTypes.UIManyToOneModel<Entities.ParcelClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._pclaId;
        }

        public get demaId():Entities.DecisionMaking {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).demaId;
        }

        public set demaId(demaId_newVal:Entities.DecisionMaking) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).demaId = demaId_newVal;
        }

        public get _demaId():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._demaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpIntegrateddecision) { super($scope); }
    }



    export interface IScopeGrpIntegrateddecisionBase extends Controllers.IAbstractTableGroupScope, IScopeGrpParcelClasBase{
        globals: Globals;
        modelGrpIntegrateddecision : ModelGrpIntegrateddecision;
        GrpIntegrateddecision_itm__dteUpdate_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        GrpIntegrateddecision_itm__usrUpdate_disabled(integrateddecision:Entities.Integrateddecision):boolean; 

    }



    export class ControllerGrpIntegrateddecisionBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpIntegrateddecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpIntegrateddecision)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 5, maxLinesInHeader: 1});
            var self:ControllerGrpIntegrateddecisionBase = this;
            model.controller = <ControllerGrpIntegrateddecision>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpIntegrateddecision = self.model;

            $scope.GrpIntegrateddecision_itm__dteUpdate_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_itm__dteUpdate_disabled(integrateddecision);
                };
            $scope.GrpIntegrateddecision_itm__usrUpdate_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_itm__usrUpdate_disabled(integrateddecision);
                };


            $scope.pageModel.modelGrpIntegrateddecision = $scope.modelGrpIntegrateddecision;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpParcelClas.modelGrpIntegrateddecision = $scope.modelGrpIntegrateddecision;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpParcelClas.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Integrateddecision[] , oldVisible:Entities.Integrateddecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpIntegrateddecision";
        }
        public get HtmlDivId(): string {
            return "Dashboard_ControllerGrpIntegrateddecision";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new DateItem (
                        (ent?: Entities.Integrateddecision) => 'Date',
                        false ,
                        (ent?: Entities.Integrateddecision) => ent.dteUpdate,  
                        (ent?: Entities.Integrateddecision) => ent._dteUpdate,  
                        (ent?: Entities.Integrateddecision) => true, //isRequired
                        (vl: Date, ent?: Entities.Integrateddecision) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new TextItem (
                        (ent?: Entities.Integrateddecision) => 'User',
                        false ,
                        (ent?: Entities.Integrateddecision) => ent.usrUpdate,  
                        (ent?: Entities.Integrateddecision) => ent._usrUpdate,  
                        (ent?: Entities.Integrateddecision) => false, 
                        (vl: string, ent?: Entities.Integrateddecision) => new NpTypes.ValidationResult(true, ""), 
                        undefined)
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Parcels";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'decisionCode', displayName:'getALString("Decision Light", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpIntegrateddecision_itm__decisionCode' data-ng-model='row.entity.decisionCode' data-np-ui-model='row.entity._decisionCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionCode&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteUpdate', displayName:'getALString("Date", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_Dashboard_W"), resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__dteUpdate' data-ng-model='row.entity.dteUpdate' data-np-ui-model='row.entity._dteUpdate' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteUpdate&quot;)' data-np-required='true' data-ng-readonly='GrpIntegrateddecision_itm__dteUpdate_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy hh:mm' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'usrUpdate', displayName:'getALString("User", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__usrUpdate' data-ng-model='row.entity.usrUpdate' data-np-ui-model='row.entity._usrUpdate' data-ng-change='markEntityAsUpdated(row.entity,&quot;usrUpdate&quot;)' data-ng-readonly='GrpIntegrateddecision_itm__usrUpdate_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'demaId.description', displayName:'getALString("Decision Making Run", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__demaId_description' data-ng-model='row.entity.demaId.description' data-np-ui-model='row.entity.demaId._description' data-ng-change='markEntityAsUpdated(row.entity,&quot;demaId.description&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpIntegrateddecision():ModelGrpIntegrateddecision {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "Integrateddecision";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.ParcelClas): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.Integrateddecision.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.pclaId = this.Parent;
                } else {
                    x.pclaId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.Integrateddecision {
            var self = this;
            return <Entities.Integrateddecision>self.$scope.modelGrpIntegrateddecision.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.Integrateddecision, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpParcelClas.controller.isEntityLocked(<Entities.ParcelClas>cur.pclaId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DashboardGrpIntegrateddecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DashboardGrpIntegrateddecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DashboardGrpIntegrateddecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.pclaId)) {
                    paramData['pclaId_pclaId'] = self.Parent.pclaId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.Integrateddecision[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.Integrateddecision[]) => void> = [];


        public getMergedItems(func: (items: Entities.Integrateddecision[]) => void, bContinuousCaller:boolean=true, parent?: Entities.ParcelClas, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.Integrateddecision[]):void {
                var mergedEntities = <Entities.Integrateddecision[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.Integrateddecision>e.a).pclaId)).
                        map(e => <Entities.Integrateddecision>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "Integrateddecision/findAllByCriteriaRange_DashboardGrpIntegrateddecision";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['pclaId_pclaId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.Integrateddecision[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.Integrateddecision):boolean {
            if (this.Parent === undefined || x.pclaId === undefined)
                return false;
            return x.pclaId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.ParcelClas {
            var self = this;
            return self.pclaId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpParcelClas.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get pclaId():Entities.ParcelClas {
            var self = this;
            return <Entities.ParcelClas>self.$scope.modelGrpParcelClas.selectedEntities[0];
        }









        public constructEntity(): Entities.Integrateddecision {
            var self = this;
            var ret = new Entities.Integrateddecision(
                /*integrateddecisionsId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*decisionCode:number*/ null,
                /*dteUpdate:Date*/ null,
                /*usrUpdate:string*/ null,
                /*pclaId:Entities.ParcelClas*/ null,
                /*demaId:Entities.DecisionMaking*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.Integrateddecision = <Entities.Integrateddecision>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.pclaId = self.pclaId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.Integrateddecision): Entities.Integrateddecision {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.Integrateddecision, calledByParent: boolean= false): Entities.Integrateddecision {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.Integrateddecision.Create();
            ret.updateInstance(src);
            ret.integrateddecisionsId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.ParcelClas, newParent:Entities.ParcelClas) {
        
            this.model.totalItems = 0;

            this.getMergedItems(integrateddecisionList => {
                integrateddecisionList.forEach(integrateddecision => {
                    var integrateddecisionCloned = this.cloneEntity_internal(integrateddecision, true);
                    integrateddecisionCloned.pclaId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(parcelClas: Entities.ParcelClas) {
        

            var self = this;
            var toBeDeleted:Array<Entities.Integrateddecision> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var integrateddecision = <Entities.Integrateddecision>change.a
                if (parcelClas.getKey() === integrateddecision.pclaId.getKey()) {
                    toBeDeleted.push(integrateddecision);
                }
            }

            _.each(toBeDeleted, (x:Entities.Integrateddecision) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(parcelClas: Entities.ParcelClas, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteIntegrateddecisionList(integrateddecisionList: Entities.Integrateddecision[]) {
                if (integrateddecisionList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = integrateddecisionList[0];
                    var tail = integrateddecisionList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteIntegrateddecisionList(tail);
                    });
                }
            }


            this.getMergedItems(integrateddecisionList => {
                deleteIntegrateddecisionList(integrateddecisionList);
            }, false, parcelClas);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.Integrateddecision, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public GrpIntegrateddecision_itm__dteUpdate_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(integrateddecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpIntegrateddecision_itm__usrUpdate_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Dashboard_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(integrateddecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelClas_0_2_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelClas_0_2_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(integrateddecision:Entities.Integrateddecision):boolean  {
            return true;
        }

    }

}
