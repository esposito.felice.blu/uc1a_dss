/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="LovParcelClasBase.ts" />
/// <reference path="../Controllers/ParcelsIssuesSearch.ts" />
var Controllers;
(function (Controllers) {
    var ModelParcelsIssuesSearchBase = (function (_super) {
        __extends(ModelParcelsIssuesSearchBase, _super);
        function ModelParcelsIssuesSearchBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._dteCreated = new NpTypes.UIDateModel(undefined);
            this._status = new NpTypes.UINumberModel(undefined);
            this._dteStatusUpdate = new NpTypes.UIDateModel(undefined);
            this._pclaId = new NpTypes.UIManyToOneModel(undefined);
        }
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "dteCreated", {
            get: function () {
                return this._dteCreated.value;
            },
            set: function (vl) {
                this._dteCreated.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "status", {
            get: function () {
                return this._status.value;
            },
            set: function (vl) {
                this._status.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "dteStatusUpdate", {
            get: function () {
                return this._dteStatusUpdate.value;
            },
            set: function (vl) {
                this._dteStatusUpdate.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "pclaId", {
            get: function () {
                return this._pclaId.value;
            },
            set: function (vl) {
                this._pclaId.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelsIssuesSearchBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelParcelsIssuesSearchBase;
    })(Controllers.AbstractSearchPageModel);
    Controllers.ModelParcelsIssuesSearchBase = ModelParcelsIssuesSearchBase;
    var ModelParcelsIssuesSearchPersistedModel = (function () {
        function ModelParcelsIssuesSearchPersistedModel() {
        }
        return ModelParcelsIssuesSearchPersistedModel;
    })();
    Controllers.ModelParcelsIssuesSearchPersistedModel = ModelParcelsIssuesSearchPersistedModel;
    var ControllerParcelsIssuesSearchBase = (function (_super) {
        __extends(ControllerParcelsIssuesSearchBase, _super);
        function ControllerParcelsIssuesSearchBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'Κριτήρια Αναζήτησης';
            var lastSearchPageModel = $scope.getPageModelByURL('/ParcelsIssuesSearch');
            var rowIndexToSelect = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.dteCreated = lastSearchPageModel.dteCreated;
                self.pageModel.status = lastSearchPageModel.status;
                self.pageModel.dteStatusUpdate = lastSearchPageModel.dteStatusUpdate;
                self.pageModel.pclaId = lastSearchPageModel.pclaId;
            }
            $scope._newIsDisabled =
                function () {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled =
                function () {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled =
                function (parcelsIssues) {
                    return self._deleteIsDisabled(parcelsIssues);
                };
            $scope._editBtnIsDisabled =
                function (parcelsIssues) {
                    return self._editBtnIsDisabled(parcelsIssues);
                };
            $scope._cancelIsDisabled =
                function () {
                    return self._cancelIsDisabled();
                };
            $scope.d__dteCreated_disabled =
                function () {
                    return self.d__dteCreated_disabled();
                };
            $scope.d__status_disabled =
                function () {
                    return self.d__status_disabled();
                };
            $scope.d__dteStatusUpdate_disabled =
                function () {
                    return self.d__dteStatusUpdate_disabled();
                };
            $scope.d__pclaId_disabled =
                function () {
                    return self.d__pclaId_disabled();
                };
            $scope.showLov_d__pclaId =
                function () {
                    $timeout(function () {
                        self.showLov_d__pclaId();
                    }, 0);
                };
            $scope.clearBtnAction = function () {
                self.pageModel.dteCreated = undefined;
                self.pageModel.status = undefined;
                self.pageModel.dteStatusUpdate = undefined;
                self.pageModel.pclaId = undefined;
                self.updateGrid(0, false, true);
            };
            $scope.newBtnAction = function () {
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };
            $scope.onEdit = function (x) {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };
            $timeout(function () {
                $('#Search_ParcelsIssues').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);
            self.updateUI(rowIndexToSelect);
        }
        ControllerParcelsIssuesSearchBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerParcelsIssuesSearchBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerParcelsIssuesSearch";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerParcelsIssuesSearchBase.prototype, "HtmlDivId", {
            get: function () {
                return "Search_ParcelsIssues";
            },
            enumerable: true,
            configurable: true
        });
        ControllerParcelsIssuesSearchBase.prototype._getPageTitle = function () {
            return "ParcelsIssues";
        };
        ControllerParcelsIssuesSearchBase.prototype.gridColumnFilter = function (field) {
            return true;
        };
        ControllerParcelsIssuesSearchBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            return [
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { cellClass: 'cellToolTip', field: 'dteCreated', displayName: 'getALString("dteCreated", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_dteCreated' data-ng-model='row.entity.dteCreated' data-np-ui-model='row.entity._dteCreated' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteCreated&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'status', displayName: 'getALString("Κατάσταση", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.status.toStringFormatted(\",\", \".\", 0)' style='width: 100%;text-align: right;padding-right: 6px;' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'dteStatusUpdate', displayName: 'getALString("Ημερομηνία", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_dteStatusUpdate' data-ng-model='row.entity.dteStatusUpdate' data-np-ui-model='row.entity._dteStatusUpdate' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteStatusUpdate&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'pclaId.probPred', displayName: 'getALString("pclaId", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.pclaId.probPred' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
        };
        ControllerParcelsIssuesSearchBase.prototype.getPersistedModel = function () {
            var self = this;
            var ret = new ModelParcelsIssuesSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.dteCreated = self.pageModel.dteCreated;
            ret.status = self.pageModel.status;
            ret.dteStatusUpdate = self.pageModel.dteStatusUpdate;
            ret.pclaId = self.pageModel.pclaId;
            return ret;
        };
        Object.defineProperty(ControllerParcelsIssuesSearchBase.prototype, "pageModel", {
            get: function () {
                return this.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerParcelsIssuesSearchBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.ParcelsIssues.fromJSONComplete(webResponse.data);
        };
        ControllerParcelsIssuesSearchBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "ParcelsIssues/findLazyParcelsIssues";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dteCreated)) {
                paramData['dteCreated'] = self.pageModel.dteCreated;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.status)) {
                paramData['status'] = self.pageModel.status;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dteStatusUpdate)) {
                paramData['dteStatusUpdate'] = self.pageModel.dteStatusUpdate;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.pclaId) && !isVoid(self.pageModel.pclaId.pclaId)) {
                paramData['pclaId_pclaId'] = self.pageModel.pclaId.pclaId;
            }
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerParcelsIssuesSearchBase.prototype.makeWebRequest_count = function (excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "ParcelsIssues/findLazyParcelsIssues_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dteCreated)) {
                paramData['dteCreated'] = self.pageModel.dteCreated;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.status)) {
                paramData['status'] = self.pageModel.status;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dteStatusUpdate)) {
                paramData['dteStatusUpdate'] = self.pageModel.dteStatusUpdate;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.pclaId) && !isVoid(self.pageModel.pclaId.pclaId)) {
                paramData['pclaId_pclaId'] = self.pageModel.pclaId.pclaId;
            }
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerParcelsIssuesSearchBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var paramData = {};
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dteCreated)) {
                paramData['dteCreated'] = self.pageModel.dteCreated;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.status)) {
                paramData['status'] = self.pageModel.status;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dteStatusUpdate)) {
                paramData['dteStatusUpdate'] = self.pageModel.dteStatusUpdate;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.pclaId) && !isVoid(self.pageModel.pclaId.pclaId)) {
                paramData['pclaId_pclaId'] = self.pageModel.pclaId.pclaId;
            }
            var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
            return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        };
        Object.defineProperty(ControllerParcelsIssuesSearchBase.prototype, "Current", {
            get: function () {
                var self = this;
                return self.$scope.pageModel.selectedEntities[0];
            },
            enumerable: true,
            configurable: true
        });
        ControllerParcelsIssuesSearchBase.prototype.onNew = function () {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/ParcelsIssues', self._getPageTitle(), {});
        };
        ControllerParcelsIssuesSearchBase.prototype.onEdit = function (x) {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel = { "selectedEntity": x };
            self.$scope.navigateForward('/ParcelsIssues', self._getPageTitle(), visitedPageModel);
        };
        ControllerParcelsIssuesSearchBase.prototype.getSynchronizeChangesWithDbUrl = function () {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_ParcelsIssues";
        };
        ControllerParcelsIssuesSearchBase.prototype.getSynchronizeChangesWithDbData = function (x) {
            var self = this;
            var paramData = {
                data: []
            };
            var changeToCommit = new Controllers.ChangeToCommit(Controllers.ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);
            paramData.data.push(changeToCommit);
            return paramData;
        };
        ControllerParcelsIssuesSearchBase.prototype.deleteRecord = function (x) {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
            console.log("deleting ParcelsIssues with PK field:" + x.parcelsIssuesId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                self.updateGrid();
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        };
        ControllerParcelsIssuesSearchBase.prototype._newIsDisabled = function () {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerParcelsIssuesSearchBase.prototype._searchIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerParcelsIssuesSearchBase.prototype._deleteIsDisabled = function (parcelsIssues) {
            var self = this;
            if (parcelsIssues === undefined || parcelsIssues === null)
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerParcelsIssuesSearchBase.prototype._editBtnIsDisabled = function (parcelsIssues) {
            var self = this;
            if (parcelsIssues === undefined || parcelsIssues === null)
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_R"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerParcelsIssuesSearchBase.prototype._cancelIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerParcelsIssuesSearchBase.prototype.d__dteCreated_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerParcelsIssuesSearchBase.prototype.d__status_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerParcelsIssuesSearchBase.prototype.d__dteStatusUpdate_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerParcelsIssuesSearchBase.prototype.d__pclaId_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerParcelsIssuesSearchBase.prototype.showLov_d__pclaId = function () {
            var self = this;
            var uimodel = self.pageModel._pclaId;
            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'ParcelClas';
            dialogOptions.previousModel = self.cachedLovModel_d__pclaId;
            dialogOptions.className = "ControllerLovParcelClas";
            dialogOptions.onSelect = function (selectedEntity) {
                var parcelClas1 = selectedEntity;
                uimodel.clearAllErrors();
                if (false && isVoid(parcelClas1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                if (!isVoid(parcelClas1) && parcelClas1.isEqual(self.pageModel.pclaId))
                    return;
                self.pageModel.pclaId = parcelClas1;
            };
            dialogOptions.openNewEntityDialog = function () {
            };
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                self.cachedLovModel_d__pclaId = lovModel;
                var wsPath = "ParcelClas/findAllByCriteriaRange_forLov";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                    paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                    paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                    paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                    paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var wsPath = "ParcelClas/findAllByCriteriaRange_forLov_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                    paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                    paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                    paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                    paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                var paramData = {};
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                    paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                    paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                    paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                    paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            dialogOptions.shownCols['probPred'] = true;
            dialogOptions.shownCols['probPred2'] = true;
            dialogOptions.shownCols['prodCode'] = true;
            dialogOptions.shownCols['parcIdentifier'] = true;
            dialogOptions.shownCols['parcCode'] = true;
            dialogOptions.shownCols['geom4326'] = true;
            self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/ParcelClas.html?rev=' + self.$scope.globals.version, dialogOptions);
        };
        return ControllerParcelsIssuesSearchBase;
    })(Controllers.AbstractSearchPageController);
    Controllers.ControllerParcelsIssuesSearchBase = ControllerParcelsIssuesSearchBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelsIssuesSearchBase.js.map