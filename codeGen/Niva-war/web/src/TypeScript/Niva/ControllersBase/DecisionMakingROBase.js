var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
/// <reference path="../EntitiesBase/IntegrateddecisionBase.ts" />
/// <reference path="LovDecisionMakingBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
/// <reference path="LovEcGroupBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../Controllers/DecisionMakingRO.ts" />
var Controllers;
(function (Controllers) {
    var DecisionMakingRO;
    (function (DecisionMakingRO) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        DecisionMakingRO.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $scope.onNewBtnAction = function () {
                    self.onNewBtnAction();
                };
                $scope.onDeleteBtnAction = function () {
                    self.onDeleteBtnAction();
                };
                $timeout(function () {
                    $('#DecisionMakingRO').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
                var selectedEntity = this.getInitialEntity();
                if (!isVoid(selectedEntity)) {
                    this.model.globalDema = Entities.DecisionMaking.Create();
                    this.model.globalDema.updateInstance(selectedEntity);
                }
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMakingRO";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Field Map";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                return true;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpDema.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpDema.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpIntegrateddecision.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpDema.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpIntegrateddecision.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpDema.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpDema.controller,
                            self.model.modelGrpIntegrateddecision.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                    pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getChangesToCommitForDeletion());
                    pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getDeleteChangesToCommit());
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getChangesToCommit());
                }
                var hasDecisionMaking = pageChanges.some(function (change) { return change.entityName === "DecisionMaking"; });
                if (!hasDecisionMaking) {
                    var masterEntity = new Controllers.ChangeToCommit(Controllers.ChangeStatus.UPDATE, 0, "DecisionMaking", self.model.globalDema);
                    pageChanges = pageChanges.concat(masterEntity);
                }
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_DecisionMakingRO";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpDema.controller.cleanUpAfterSave();
                self.model.modelGrpIntegrateddecision.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var _this = this;
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    var newDecisionMakingId = response.newEntitiesIds.firstOrNull(function (x) { return x.entityName === 'DecisionMaking'; });
                    if (!isVoid(newDecisionMakingId)) {
                        _this.model.globalDema = Entities.DecisionMaking.Create();
                        _this.model.globalDema.demaId = newDecisionMakingId.databaseId;
                        self.$scope.globals.globalDema.refresh(self);
                    }
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = self.$scope.getCurrentPageModel();
                        if (!isVoid(newDecisionMakingId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.DecisionMaking.CreateById(newDecisionMakingId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.getInitialEntity = function () {
                if (this.shownAsDialog()) {
                    return this.dialogSelectedEntity();
                }
                else {
                    var breadCrumbStepModel = this.$scope.getPageModelByURL('/DecisionMakingRO');
                    if (isVoid(breadCrumbStepModel)) {
                        return null;
                    }
                    else {
                        return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                    }
                }
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            PageControllerBase.prototype.onNewBtnAction = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                self.addNewRecord();
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () { self.addNewRecord(); }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    self.addNewRecord();
                }
            };
            PageControllerBase.prototype.addNewRecord = function () {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                self.model.modelGrpDema.controller.cleanUpAfterSave();
                self.model.modelGrpIntegrateddecision.controller.cleanUpAfterSave();
                self.model.modelGrpDema.controller.createNewEntityAndAddToUI();
            };
            PageControllerBase.prototype.onDeleteBtnAction = function () {
                var self = this;
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                    new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(); }),
                    new Tuple2("MessageBox_Button_No", function () { })
                ], 1, 1);
            };
            PageControllerBase.prototype.deleteRecord = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW) {
                    console.log("Delete pressed in a form while being in new mode!");
                    return;
                }
                NpTypes.AlertMessage.clearAlerts(self.model);
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData(true);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.model.globalDema = null;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "Mode", {
                get: function () {
                    var self = this;
                    if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                        return Controllers.EditMode.NEW;
                    return self.model.modelGrpDema.controller.Mode;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._newIsDisabled = function () {
                var self = this;
                if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                    return true;
                return self.model.modelGrpDema.controller._newIsDisabled();
            };
            PageControllerBase.prototype._deleteIsDisabled = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW)
                    return true;
                if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                    return true;
                return self.model.modelGrpDema.controller._deleteIsDisabled(self.model.modelGrpDema.controller.Current);
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        DecisionMakingRO.PageControllerBase = PageControllerBase;
        // GROUP GrpDema
        var ModelGrpDemaBase = (function (_super) {
            __extends(ModelGrpDemaBase, _super);
            function ModelGrpDemaBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpDemaBase.prototype, "demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].demaId;
                },
                set: function (demaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].demaId = demaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._demaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "description", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].description;
                },
                set: function (description_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].description = description_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_description", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._description;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "dateTime", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dateTime;
                },
                set: function (dateTime_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dateTime = dateTime_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_dateTime", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dateTime;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "recordtype", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].recordtype;
                },
                set: function (recordtype_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].recordtype = recordtype_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_recordtype", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._recordtype;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "hasBeenRun", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].hasBeenRun;
                },
                set: function (hasBeenRun_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].hasBeenRun = hasBeenRun_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_hasBeenRun", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._hasBeenRun;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "hasPopulatedIntegratedDecisionsAndIssues", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].hasPopulatedIntegratedDecisionsAndIssues;
                },
                set: function (hasPopulatedIntegratedDecisionsAndIssues_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].hasPopulatedIntegratedDecisionsAndIssues = hasPopulatedIntegratedDecisionsAndIssues_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_hasPopulatedIntegratedDecisionsAndIssues", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._hasPopulatedIntegratedDecisionsAndIssues;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].clasId;
                },
                set: function (clasId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].clasId = clasId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._clasId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecgrId;
                },
                set: function (ecgrId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecgrId = ecgrId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecgrId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpDemaBase;
        })(Controllers.AbstractGroupFormModel);
        DecisionMakingRO.ModelGrpDemaBase = ModelGrpDemaBase;
        var ControllerGrpDemaBase = (function (_super) {
            __extends(ControllerGrpDemaBase, _super);
            function ControllerGrpDemaBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                $scope.modelGrpDema = self.model;
                var selectedEntity = this.$scope.pageModel.controller.getInitialEntity();
                if (isVoid(selectedEntity)) {
                    self.createNewEntityAndAddToUI();
                }
                else {
                    var clonedEntity = Entities.DecisionMaking.Create();
                    clonedEntity.updateInstance(selectedEntity);
                    $scope.modelGrpDema.visibleEntities[0] = clonedEntity;
                    $scope.modelGrpDema.selectedEntities[0] = clonedEntity;
                }
                $scope.GrpDema_itm__description_disabled =
                    function (decisionMaking) {
                        return self.GrpDema_itm__description_disabled(decisionMaking);
                    };
                $scope.GrpDema_itm__clasId_disabled =
                    function (decisionMaking) {
                        return self.GrpDema_itm__clasId_disabled(decisionMaking);
                    };
                $scope.showLov_GrpDema_itm__clasId =
                    function (decisionMaking) {
                        $timeout(function () {
                            self.showLov_GrpDema_itm__clasId(decisionMaking);
                        }, 0);
                    };
                $scope.GrpDema_itm__ecgrId_disabled =
                    function (decisionMaking) {
                        return self.GrpDema_itm__ecgrId_disabled(decisionMaking);
                    };
                $scope.showLov_GrpDema_itm__ecgrId =
                    function (decisionMaking) {
                        $timeout(function () {
                            self.showLov_GrpDema_itm__ecgrId(decisionMaking);
                        }, 0);
                    };
                $scope.GrpDema_itm__dateTime_disabled =
                    function (decisionMaking) {
                        return self.GrpDema_itm__dateTime_disabled(decisionMaking);
                    };
                $scope.GrpDema_0_disabled =
                    function () {
                        return self.GrpDema_0_disabled();
                    };
                $scope.GrpDema_0_invisible =
                    function () {
                        return self.GrpDema_0_invisible();
                    };
                $scope.child_group_GrpIntegrateddecision_isInvisible =
                    function () {
                        return self.child_group_GrpIntegrateddecision_isInvisible();
                    };
                $scope.pageModel.modelGrpDema = $scope.modelGrpDema;
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.DecisionMaking[] , oldVisible:Entities.DecisionMaking[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                //bind entity child collection with child controller merged items
                Entities.DecisionMaking.integrateddecisionCollection = function (decisionMaking, func) {
                    _this.model.modelGrpIntegrateddecision.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, decisionMaking);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.DecisionMaking.integrateddecisionCollection = null; //(decisionMaking, func) => { }
                });
            }
            ControllerGrpDemaBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpDemaBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpDema";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpDemaBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMakingRO_ControllerGrpDema";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpDemaBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Decision Making'; }, false, function (ent) { return ent.description; }, function (ent) { return ent._description; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.LovItem(function (ent) { return 'Classification'; }, false, function (ent) { return ent.clasId; }, function (ent) { return ent._clasId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.LovItem(function (ent) { return 'BRE Rules Name'; }, false, function (ent) { return ent.ecgrId; }, function (ent) { return ent._ecgrId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.DateItem(function (ent) { return 'Date'; }, false, function (ent) { return ent.dateTime; }, function (ent) { return ent._dateTime; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpDemaBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpDemaBase.prototype, "modelGrpDema", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpDemaBase.prototype.getEntityName = function () {
                return "DecisionMaking";
            };
            ControllerGrpDemaBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
            };
            ControllerGrpDemaBase.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
                var self = this;
                if (self.$scope.pageModel.controller.shownAsDialog())
                    return;
                var breadCrumbStepModel = { selectedEntity: null };
                if (!isVoid(self.Current)) {
                    var selectedEntity = Entities.DecisionMaking.Create();
                    selectedEntity.updateInstance(self.Current);
                    breadCrumbStepModel.selectedEntity = selectedEntity;
                }
                else {
                    breadCrumbStepModel.selectedEntity = null;
                }
                self.$scope.setCurrentPageModel(breadCrumbStepModel);
            };
            ControllerGrpDemaBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var entlist = Entities.DecisionMaking.fromJSONComplete(webResponse.data);
                return entlist;
            };
            Object.defineProperty(ControllerGrpDemaBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpDema.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpDemaBase.prototype.isEntityLocked = function (cur, lockKind) {
                return true;
            };
            ControllerGrpDemaBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.DecisionMaking(
                /*demaId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*description:string*/ null, 
                /*dateTime:Date*/ null, 
                /*rowVersion:number*/ null, 
                /*recordtype:number*/ null, 
                /*hasBeenRun:boolean*/ null, 
                /*hasPopulatedIntegratedDecisionsAndIssues:boolean*/ null, 
                /*clasId:Entities.Classification*/ null, 
                /*ecgrId:Entities.EcGroup*/ null);
                return ret;
            };
            ControllerGrpDemaBase.prototype.createNewEntityAndAddToUI = function () {
                throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "createNewEntityAndAddToUI() called for a view");
            };
            ControllerGrpDemaBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpDemaBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.DecisionMaking.Create();
                ret.updateInstance(src);
                ret.demaId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.visibleEntities[0] = ret;
                this.model.selectedEntities[0] = ret;
                this.$timeout(function () {
                    _this.modelGrpDema.modelGrpIntegrateddecision.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpDemaBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpDema.modelGrpIntegrateddecision.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpDemaBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
            };
            ControllerGrpDemaBase.prototype.GrpDema_itm__description_disabled = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined || decisionMaking === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_DecisionMakingRO_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(decisionMaking, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpDemaBase.prototype.GrpDema_itm__clasId_disabled = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined || decisionMaking === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_DecisionMakingRO_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(decisionMaking, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpDemaBase.prototype.showLov_GrpDema_itm__clasId = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined)
                    return;
                var uimodel = decisionMaking._clasId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Data Import Name';
                dialogOptions.previousModel = self.cachedLovModel_GrpDema_itm__clasId;
                dialogOptions.className = "ControllerLovClassification";
                dialogOptions.onSelect = function (selectedEntity) {
                    var classification1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(classification1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(classification1) && classification1.isEqual(decisionMaking.clasId))
                        return;
                    decisionMaking.clasId = classification1;
                    self.markEntityAsUpdated(decisionMaking, 'GrpDema_itm__clasId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpDema_itm__clasId = lovModel;
                    var wsPath = "Classification/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Classification/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['description'] = true;
                dialogOptions.shownCols['dateTime'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Classification.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpDemaBase.prototype.GrpDema_itm__ecgrId_disabled = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined || decisionMaking === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_DecisionMakingRO_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(decisionMaking, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpDemaBase.prototype.showLov_GrpDema_itm__ecgrId = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined)
                    return;
                var uimodel = decisionMaking._ecgrId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'BRE Rules';
                dialogOptions.previousModel = self.cachedLovModel_GrpDema_itm__ecgrId;
                dialogOptions.className = "ControllerLovEcGroup";
                dialogOptions.onSelect = function (selectedEntity) {
                    var ecGroup1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(ecGroup1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(ecGroup1) && ecGroup1.isEqual(decisionMaking.ecgrId))
                        return;
                    decisionMaking.ecgrId = ecGroup1;
                    self.markEntityAsUpdated(decisionMaking, 'GrpDema_itm__ecgrId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpDema_itm__ecgrId = lovModel;
                    var wsPath = "EcGroup/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "EcGroup/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['description'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/EcGroup.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpDemaBase.prototype.GrpDema_itm__dateTime_disabled = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined || decisionMaking === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_DecisionMakingRO_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(decisionMaking, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpDemaBase.prototype.GrpDema_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpDemaBase.prototype.GrpDema_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpDemaBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpDemaBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpDemaBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpDemaBase.prototype._deleteIsDisabled = function (decisionMaking) {
                return true;
            };
            ControllerGrpDemaBase.prototype.child_group_GrpIntegrateddecision_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpIntegrateddecision === undefined) || (self.model.modelGrpIntegrateddecision.controller === undefined))
                    return false;
                return self.model.modelGrpIntegrateddecision.controller._isInvisible();
            };
            return ControllerGrpDemaBase;
        })(Controllers.AbstractGroupFormController);
        DecisionMakingRO.ControllerGrpDemaBase = ControllerGrpDemaBase;
        // GROUP GrpIntegrateddecision
        var ModelGrpIntegrateddecisionBase = (function (_super) {
            __extends(ModelGrpIntegrateddecisionBase, _super);
            function ModelGrpIntegrateddecisionBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_decisionCode = new NpTypes.UINumberModel(undefined);
                this._fsch_Parcel_Identifier = new NpTypes.UIStringModel(undefined);
                this._fsch_prodCode = new NpTypes.UINumberModel(undefined);
                this._fsch_Code = new NpTypes.UIStringModel(undefined);
                this._fsch_CultDeclCode = new NpTypes.UIManyToOneModel(undefined);
            }
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "integrateddecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].integrateddecisionsId;
                },
                set: function (integrateddecisionsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].integrateddecisionsId = integrateddecisionsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_integrateddecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._integrateddecisionsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "decisionCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].decisionCode;
                },
                set: function (decisionCode_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].decisionCode = decisionCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_decisionCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._decisionCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "dteUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteUpdate;
                },
                set: function (dteUpdate_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteUpdate = dteUpdate_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_dteUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteUpdate;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "usrUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].usrUpdate;
                },
                set: function (usrUpdate_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].usrUpdate = usrUpdate_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_usrUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._usrUpdate;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].pclaId;
                },
                set: function (pclaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].pclaId = pclaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].demaId;
                },
                set: function (demaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].demaId = demaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._demaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "fsch_decisionCode", {
                get: function () {
                    return this._fsch_decisionCode.value;
                },
                set: function (vl) {
                    this._fsch_decisionCode.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "fsch_Parcel_Identifier", {
                get: function () {
                    return this._fsch_Parcel_Identifier.value;
                },
                set: function (vl) {
                    this._fsch_Parcel_Identifier.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "fsch_prodCode", {
                get: function () {
                    return this._fsch_prodCode.value;
                },
                set: function (vl) {
                    this._fsch_prodCode.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "fsch_Code", {
                get: function () {
                    return this._fsch_Code.value;
                },
                set: function (vl) {
                    this._fsch_Code.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "fsch_CultDeclCode", {
                get: function () {
                    return this._fsch_CultDeclCode.value;
                },
                set: function (vl) {
                    this._fsch_CultDeclCode.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpIntegrateddecisionBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpIntegrateddecisionBase;
        })(Controllers.AbstractGroupTableModel);
        DecisionMakingRO.ModelGrpIntegrateddecisionBase = ModelGrpIntegrateddecisionBase;
        var ControllerGrpIntegrateddecisionBase = (function (_super) {
            __extends(ControllerGrpIntegrateddecisionBase, _super);
            function ControllerGrpIntegrateddecisionBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 5, maxLinesInHeader: 3 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpIntegrateddecision = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpIntegrateddecision_srchr__fsch_decisionCode_disabled =
                    function (integrateddecision) {
                        return self.GrpIntegrateddecision_srchr__fsch_decisionCode_disabled(integrateddecision);
                    };
                $scope.GrpIntegrateddecision_srchr__fsch_Parcel_Identifier_disabled =
                    function (integrateddecision) {
                        return self.GrpIntegrateddecision_srchr__fsch_Parcel_Identifier_disabled(integrateddecision);
                    };
                $scope.GrpIntegrateddecision_srchr__fsch_prodCode_disabled =
                    function (integrateddecision) {
                        return self.GrpIntegrateddecision_srchr__fsch_prodCode_disabled(integrateddecision);
                    };
                $scope.GrpIntegrateddecision_srchr__fsch_Code_disabled =
                    function (integrateddecision) {
                        return self.GrpIntegrateddecision_srchr__fsch_Code_disabled(integrateddecision);
                    };
                $scope.GrpIntegrateddecision_srchr__fsch_CultDeclCode_disabled =
                    function (integrateddecision) {
                        return self.GrpIntegrateddecision_srchr__fsch_CultDeclCode_disabled(integrateddecision);
                    };
                $scope.showLov_GrpIntegrateddecision_srchr__fsch_CultDeclCode =
                    function () {
                        $timeout(function () {
                            self.showLov_GrpIntegrateddecision_srchr__fsch_CultDeclCode();
                        }, 0);
                    };
                $scope.GrpIntegrateddecision_0_disabled =
                    function () {
                        return self.GrpIntegrateddecision_0_disabled();
                    };
                $scope.GrpIntegrateddecision_0_invisible =
                    function () {
                        return self.GrpIntegrateddecision_0_invisible();
                    };
                $scope.GrpIntegrateddecision_0_0_disabled =
                    function (integrateddecision) {
                        return self.GrpIntegrateddecision_0_0_disabled(integrateddecision);
                    };
                $scope.exportIntegratedDecisionsToCSV =
                    function (integrateddecision) {
                        self.exportIntegratedDecisionsToCSV(integrateddecision);
                    };
                $scope.GrpIntegrateddecision_0_1_disabled =
                    function (integrateddecision) {
                        return self.GrpIntegrateddecision_0_1_disabled(integrateddecision);
                    };
                $scope.actionPushToCommonsAPI =
                    function (integrateddecision) {
                        self.actionPushToCommonsAPI(integrateddecision);
                    };
                $scope.GrpIntegrateddecision_1_disabled =
                    function () {
                        return self.GrpIntegrateddecision_1_disabled();
                    };
                $scope.GrpIntegrateddecision_1_invisible =
                    function () {
                        return self.GrpIntegrateddecision_1_invisible();
                    };
                $scope.XartoReg_disabled =
                    function () {
                        return self.XartoReg_disabled();
                    };
                $scope.XartoReg_invisible =
                    function () {
                        return self.XartoReg_invisible();
                    };
                $scope.pageModel.modelGrpIntegrateddecision = $scope.modelGrpIntegrateddecision;
                $scope.clearBtnAction = function () {
                    self.modelGrpIntegrateddecision.fsch_decisionCode = undefined;
                    self.modelGrpIntegrateddecision.fsch_Parcel_Identifier = undefined;
                    self.modelGrpIntegrateddecision.fsch_prodCode = undefined;
                    self.modelGrpIntegrateddecision.fsch_Code = undefined;
                    self.modelGrpIntegrateddecision.fsch_CultDeclCode = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpDema.modelGrpIntegrateddecision = $scope.modelGrpIntegrateddecision;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpDema.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Integrateddecision[] , oldVisible:Entities.Integrateddecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
                this.createPclaId_geom4326Map({
                    grp: this,
                    Plato: Plato,
                    divId: "INPMPDMRO",
                    getDataFromEntity: function (x) { return isVoid(x) ? undefined : x.pclaId.geom4326; },
                    saveDataToEntity: function (x, f) {
                        if (!isVoid(x)) {
                            x.pclaId.geom4326 = f;
                            _this.markEntityAsUpdated(x, "pclaId.geom4326");
                        }
                    },
                    isDisabled: function (x) { return true; },
                    layers: [
                        new NpGeoLayers.OpenStreetTileLayer(true),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMPDMRO_ParcelGreen',
                            label: 'Green Parcels',
                            isVisible: true,
                            getQueryUrl: function () { return '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen?'; },
                            getQueryInputs: function (integrateddecision) { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null }; },
                            fillColor: 'rgba(0, 255, 0, 0.8)',
                            borderColor: 'rgba(0,0,0,1.0)',
                            penWidth: 1,
                            selectable: true,
                            showSpatialSearchFilters: true
                        }),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMPDMRO_ParcelYellow',
                            label: 'Yellow Parcels',
                            isVisible: true,
                            getQueryUrl: function () { return '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow?'; },
                            getQueryInputs: function (integrateddecision) { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null }; },
                            fillColor: 'rgba(255, 255, 0, 0.8)',
                            borderColor: 'rgba(0,0,0,1.0)',
                            penWidth: 1,
                            selectable: true,
                            showSpatialSearchFilters: true
                        }),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMPDMRO_ParcelRed',
                            label: 'Red Parcels',
                            isVisible: true,
                            getQueryUrl: function () { return '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed?'; },
                            getQueryInputs: function (integrateddecision) { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null }; },
                            fillColor: 'rgba(255, 0, 0, 0.8)',
                            borderColor: 'rgba(0,0,0,1.0)',
                            penWidth: 1,
                            selectable: true,
                            showSpatialSearchFilters: true
                        }),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMPDMRO_ParcelUndefined',
                            label: 'Undefined Parcels',
                            isVisible: true,
                            getQueryUrl: function () { return '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined?'; },
                            getQueryInputs: function (integrateddecision) { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null }; },
                            fillColor: 'rgba(255,255,255, 0.5)',
                            borderColor: 'rgba(0,0,0,0.5)',
                            penWidth: 1,
                            selectable: true,
                            showSpatialSearchFilters: true
                        }),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMPDMRO5',
                            label: 'Search1',
                            isVisible: true,
                            isSearch: true
                        }),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMPDMRO6',
                            label: 'Search2',
                            isVisible: true,
                            isSearch: true
                        }),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMPDMRO7',
                            label: 'Search3',
                            isVisible: true,
                            isSearch: true
                        })
                    ],
                    coordinateSystem: "EPSG:2100"
                });
            }
            ControllerGrpIntegrateddecisionBase.prototype.createPclaId_geom4326Map = function (defaultOptions) {
                this.pclaId_geom4326Map = new NpGeo.NpMap(defaultOptions);
            };
            ControllerGrpIntegrateddecisionBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpIntegrateddecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMakingRO_ControllerGrpIntegrateddecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.StaticListItem(function (ent) { return 'Decision Light'; }, true, function (ent) { return self.model.fsch_decisionCode; }, function (ent) { return self.model._fsch_decisionCode; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.TextItem(function (ent) { return 'Identifier'; }, true, function (ent) { return self.model.fsch_Parcel_Identifier; }, function (ent) { return self.model._fsch_Parcel_Identifier; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.NumberItem(function (ent) { return 'Farmer Code'; }, true, function (ent) { return self.model.fsch_prodCode; }, function (ent) { return self.model._fsch_prodCode; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0),
                            new Controllers.TextItem(function (ent) { return 'Parcel Code'; }, true, function (ent) { return self.model.fsch_Code; }, function (ent) { return self.model._fsch_Code; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.LovItem(function (ent) { return 'Declared Cultivation'; }, true, function (ent) { return self.model.fsch_CultDeclCode; }, function (ent) { return self.model._fsch_CultDeclCode; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpIntegrateddecisionBase.prototype.gridTitle = function () {
                return "Parcels";
            };
            ControllerGrpIntegrateddecisionBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpIntegrateddecisionBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'decisionCode', displayName: 'getALString("Integrated Decision \n Light", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpIntegrateddecision_itm__decisionCode' data-ng-model='row.entity.decisionCode' data-np-ui-model='row.entity._decisionCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionCode&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.parcIdentifier', displayName: 'getALString("Identifier", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_parcIdentifier' data-ng-model='row.entity.pclaId.parcIdentifier' data-np-ui-model='row.entity.pclaId._parcIdentifier' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.parcIdentifier&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.parcCode', displayName: 'getALString("Parcel\'s \n Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_parcCode' data-ng-model='row.entity.pclaId.parcCode' data-np-ui-model='row.entity.pclaId._parcCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.parcCode&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.prodCode', displayName: 'getALString("Farmer\'s \n Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_prodCode' data-ng-model='row.entity.pclaId.prodCode' data-np-ui-model='row.entity.pclaId._prodCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.prodCode&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdDecl.code', displayName: 'getALString("Cultivation \n Declared Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '7%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdDecl_code' data-ng-model='row.entity.pclaId.cultIdDecl.code' data-np-ui-model='row.entity.pclaId.cultIdDecl._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdDecl.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdDecl.name', displayName: 'getALString("Cultivation \n Declared Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdDecl_name' data-ng-model='row.entity.pclaId.cultIdDecl.name' data-np-ui-model='row.entity.pclaId.cultIdDecl._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdDecl.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdPred.code', displayName: 'getALString("Cultivation \n 1st Prediction \n Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '7%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdPred_code' data-ng-model='row.entity.pclaId.cultIdPred.code' data-np-ui-model='row.entity.pclaId.cultIdPred._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdPred.name', displayName: 'getALString("Cultivation \n 1st Prediction \n Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '10%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdPred_name' data-ng-model='row.entity.pclaId.cultIdPred.name' data-np-ui-model='row.entity.pclaId.cultIdPred._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.probPred', displayName: 'getALString("Probability \n 1st Prediction", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_probPred' data-ng-model='row.entity.pclaId.probPred' data-np-ui-model='row.entity.pclaId._probPred' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.probPred&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdPred2.code', displayName: 'getALString("Cultivation \n 2nd Prediction  \nCode", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '7%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdPred2_code' data-ng-model='row.entity.pclaId.cultIdPred2.code' data-np-ui-model='row.entity.pclaId.cultIdPred2._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred2.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdPred2.name', displayName: 'getALString("Cultivation \n 2nd Prediction \n Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '10%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdPred2_name' data-ng-model='row.entity.pclaId.cultIdPred2.name' data-np-ui-model='row.entity.pclaId.cultIdPred2._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred2.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.probPred2', displayName: 'getALString("Probability \n 2nd Prediction", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_probPred2' data-ng-model='row.entity.pclaId.probPred2' data-np-ui-model='row.entity.pclaId._probPred2' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.probPred2&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "modelGrpIntegrateddecision", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpIntegrateddecisionBase.prototype.getEntityName = function () {
                return "Integrateddecision";
            };
            ControllerGrpIntegrateddecisionBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
                this.pclaId_geom4326Map.cleanUndoBuffers();
            };
            ControllerGrpIntegrateddecisionBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.Integrateddecision.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.demaId = _this.Parent;
                    }
                    else {
                        x.demaId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpIntegrateddecision.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpIntegrateddecisionBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpDema.controller.isEntityLocked(cur.demaId, lockKind);
            };
            ControllerGrpIntegrateddecisionBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_decisionCode)) {
                    paramData['fsch_decisionCode'] = self.modelGrpIntegrateddecision.fsch_decisionCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpIntegrateddecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpIntegrateddecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpIntegrateddecision.fsch_Parcel_Identifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId)) {
                    paramData['fsch_CultDeclCode_cultId'] = self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpIntegrateddecisionBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_decisionCode)) {
                    paramData['fsch_decisionCode'] = self.modelGrpIntegrateddecision.fsch_decisionCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpIntegrateddecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpIntegrateddecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpIntegrateddecision.fsch_Parcel_Identifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId)) {
                    paramData['fsch_CultDeclCode_cultId'] = self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpIntegrateddecisionBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_decisionCode)) {
                    paramData['fsch_decisionCode'] = self.modelGrpIntegrateddecision.fsch_decisionCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpIntegrateddecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpIntegrateddecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpIntegrateddecision.fsch_Parcel_Identifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId)) {
                    paramData['fsch_CultDeclCode_cultId'] = self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpIntegrateddecisionBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_decisionCode)) {
                    paramData['fsch_decisionCode'] = self.modelGrpIntegrateddecision.fsch_decisionCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpIntegrateddecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpIntegrateddecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpIntegrateddecision.fsch_Parcel_Identifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId)) {
                    paramData['fsch_CultDeclCode_cultId'] = self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpIntegrateddecisionBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.demaId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['demaId_demaId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpIntegrateddecisionBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.demaId === undefined)
                    return false;
                return x.demaId.getKey() === this.Parent.getKey();
            };
            ControllerGrpIntegrateddecisionBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.demaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpDema.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "demaId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpDema.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpIntegrateddecisionBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.Integrateddecision(
                /*integrateddecisionsId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*decisionCode:number*/ null, 
                /*dteUpdate:Date*/ null, 
                /*usrUpdate:string*/ null, 
                /*pclaId:Entities.ParcelClas*/ null, 
                /*demaId:Entities.DecisionMaking*/ null);
                return ret;
            };
            ControllerGrpIntegrateddecisionBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.demaId = self.demaId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpIntegrateddecisionBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpIntegrateddecisionBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.Integrateddecision.Create();
                ret.updateInstance(src);
                ret.integrateddecisionsId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpIntegrateddecisionBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (integrateddecisionList) {
                    integrateddecisionList.forEach(function (integrateddecision) {
                        var integrateddecisionCloned = _this.cloneEntity_internal(integrateddecision, true);
                        integrateddecisionCloned.demaId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpIntegrateddecisionBase.prototype.deleteNewEntitiesUnderParent = function (decisionMaking) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var integrateddecision = change.a;
                    if (decisionMaking.getKey() === integrateddecision.demaId.getKey()) {
                        toBeDeleted.push(integrateddecision);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpIntegrateddecisionBase.prototype.deleteAllEntitiesUnderParent = function (decisionMaking, afterDeleteAction) {
                var self = this;
                function deleteIntegrateddecisionList(integrateddecisionList) {
                    if (integrateddecisionList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = integrateddecisionList[0];
                        var tail = integrateddecisionList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteIntegrateddecisionList(tail);
                        });
                    }
                }
                this.getMergedItems(function (integrateddecisionList) {
                    deleteIntegrateddecisionList(integrateddecisionList);
                }, false, decisionMaking);
            };
            Object.defineProperty(ControllerGrpIntegrateddecisionBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpIntegrateddecisionBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpIntegrateddecisionBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_srchr__fsch_decisionCode_disabled = function (integrateddecision) {
                var self = this;
                return false;
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_srchr__fsch_Parcel_Identifier_disabled = function (integrateddecision) {
                var self = this;
                return false;
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_srchr__fsch_prodCode_disabled = function (integrateddecision) {
                var self = this;
                return false;
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_srchr__fsch_Code_disabled = function (integrateddecision) {
                var self = this;
                return false;
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_srchr__fsch_CultDeclCode_disabled = function (integrateddecision) {
                var self = this;
                return false;
            };
            ControllerGrpIntegrateddecisionBase.prototype.showLov_GrpIntegrateddecision_srchr__fsch_CultDeclCode = function () {
                var self = this;
                var uimodel = self.modelGrpIntegrateddecision._fsch_CultDeclCode;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Cultivation';
                dialogOptions.previousModel = self.cachedLovModel_GrpIntegrateddecision_srchr__fsch_CultDeclCode;
                dialogOptions.className = "ControllerLovCultivation";
                dialogOptions.onSelect = function (selectedEntity) {
                    var cultivation1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(cultivation1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(cultivation1) && cultivation1.isEqual(self.modelGrpIntegrateddecision.fsch_CultDeclCode))
                        return;
                    self.modelGrpIntegrateddecision.fsch_CultDeclCode = cultivation1;
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpIntegrateddecision_srchr__fsch_CultDeclCode = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_0_0_disabled = function (integrateddecision) {
                var self = this;
                if (integrateddecision === undefined || integrateddecision === null)
                    return true;
                var isContainerControlDisabled = self.GrpIntegrateddecision_0_disabled();
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            ControllerGrpIntegrateddecisionBase.prototype.exportIntegratedDecisionsToCSV = function (integrateddecision) {
                var self = this;
                console.log("Method: exportIntegratedDecisionsToCSV() called");
                console.log(integrateddecision);
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_0_1_disabled = function (integrateddecision) {
                var self = this;
                if (integrateddecision === undefined || integrateddecision === null)
                    return true;
                var isContainerControlDisabled = self.GrpIntegrateddecision_0_disabled();
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            ControllerGrpIntegrateddecisionBase.prototype.actionPushToCommonsAPI = function (integrateddecision) {
                var self = this;
                console.log("Method: actionPushToCommonsAPI() called");
                console.log(integrateddecision);
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_1_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype.GrpIntegrateddecision_1_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype.XartoReg_disabled = function () {
                if (false)
                    return true;
                var parControl = this.GrpIntegrateddecision_1_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype.XartoReg_invisible = function () {
                if (false)
                    return true;
                var parControl = this.GrpIntegrateddecision_1_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpDema_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpDema_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpIntegrateddecisionBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpIntegrateddecisionBase.prototype._deleteIsDisabled = function (integrateddecision) {
                return true;
            };
            return ControllerGrpIntegrateddecisionBase;
        })(Controllers.AbstractGroupTableController);
        DecisionMakingRO.ControllerGrpIntegrateddecisionBase = ControllerGrpIntegrateddecisionBase;
    })(DecisionMakingRO = Controllers.DecisionMakingRO || (Controllers.DecisionMakingRO = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DecisionMakingROBase.js.map