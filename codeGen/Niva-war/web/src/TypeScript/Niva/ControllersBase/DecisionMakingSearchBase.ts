/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />

// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
/// <reference path="LovEcGroupBase.ts" />
/// <reference path="../Controllers/DecisionMakingSearch.ts" />

module Controllers {

    export class ModelDecisionMakingSearchBase extends Controllers.AbstractSearchPageModel {
        _clasId:NpTypes.UIManyToOneModel<Entities.Classification> = new NpTypes.UIManyToOneModel<Entities.Classification>(undefined);
        public get clasId():Entities.Classification {
            return this._clasId.value;
        }
        public set clasId(vl:Entities.Classification) {
            this._clasId.value = vl;
        }
        _ecgrId:NpTypes.UIManyToOneModel<Entities.EcGroup> = new NpTypes.UIManyToOneModel<Entities.EcGroup>(undefined);
        public get ecgrId():Entities.EcGroup {
            return this._ecgrId.value;
        }
        public set ecgrId(vl:Entities.EcGroup) {
            this._ecgrId.value = vl;
        }
        _description:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get description():string {
            return this._description.value;
        }
        public set description(vl:string) {
            this._description.value = vl;
        }
        _dateTime:NpTypes.UIDateModel = new NpTypes.UIDateModel(undefined);
        public get dateTime():Date {
            return this._dateTime.value;
        }
        public set dateTime(vl:Date) {
            this._dateTime.value = vl;
        }
        _hasBeenRun:NpTypes.UIBoolModel = new NpTypes.UIBoolModel(undefined);
        public get hasBeenRun():boolean {
            return this._hasBeenRun.value;
        }
        public set hasBeenRun(vl:boolean) {
            this._hasBeenRun.value = vl;
        }
        _hasPopulatedIntegratedDecisionsAndIssues:NpTypes.UIBoolModel = new NpTypes.UIBoolModel(undefined);
        public get hasPopulatedIntegratedDecisionsAndIssues():boolean {
            return this._hasPopulatedIntegratedDecisionsAndIssues.value;
        }
        public set hasPopulatedIntegratedDecisionsAndIssues(vl:boolean) {
            this._hasPopulatedIntegratedDecisionsAndIssues.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        controller: ControllerDecisionMakingSearch;
        constructor(public $scope: IScopeDecisionMakingSearch) { super($scope); }
    }
    
    export class ModelDecisionMakingSearchPersistedModel implements NpTypes.IBreadcrumbStepModel {
        _pageSize: number;
        _currentPage: number;
        _currentRow: number;
        sortField: string;
        sortOrder: string;
        clasId:Entities.Classification;
        ecgrId:Entities.EcGroup;
        description:string;
        dateTime:Date;
        hasBeenRun:boolean;
        hasPopulatedIntegratedDecisionsAndIssues:boolean;
    }



    export interface IScopeDecisionMakingSearchBase extends Controllers.IAbstractSearchPageScope {
        globals: Globals;
        pageModel : ModelDecisionMakingSearch;
        _newIsDisabled():boolean; 
        _searchIsDisabled():boolean; 
        _deleteIsDisabled(decisionMaking:Entities.DecisionMaking):boolean; 
        _editBtnIsDisabled(decisionMaking:Entities.DecisionMaking):boolean; 
        _cancelIsDisabled():boolean; 
        d__clasId_disabled():boolean; 
        showLov_d__clasId():void; 
        d__ecgrId_disabled():boolean; 
        showLov_d__ecgrId():void; 
        d__description_disabled():boolean; 
        d__dateTime_disabled():boolean; 
        d__hasBeenRun_disabled():boolean; 
        d__hasPopulatedIntegratedDecisionsAndIssues_disabled():boolean; 
    }

    export class ControllerDecisionMakingSearchBase extends Controllers.AbstractSearchPageController {
        constructor(
            public $scope: IScopeDecisionMakingSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:ModelDecisionMakingSearch)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self: ControllerDecisionMakingSearchBase = this;
            model.controller = <ControllerDecisionMakingSearch>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'Decision Making';
            
            var lastSearchPageModel = <ModelDecisionMakingSearchPersistedModel>$scope.getPageModelByURL('/DecisionMakingSearch');
            var rowIndexToSelect: number = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.clasId = lastSearchPageModel.clasId;
                self.pageModel.ecgrId = lastSearchPageModel.ecgrId;
                self.pageModel.description = lastSearchPageModel.description;
                self.pageModel.dateTime = lastSearchPageModel.dateTime;
                self.pageModel.hasBeenRun = lastSearchPageModel.hasBeenRun;
                self.pageModel.hasPopulatedIntegratedDecisionsAndIssues = lastSearchPageModel.hasPopulatedIntegratedDecisionsAndIssues;
            }


            $scope._newIsDisabled = 
                () => {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled = 
                () => {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self._deleteIsDisabled(decisionMaking);
                };
            $scope._editBtnIsDisabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self._editBtnIsDisabled(decisionMaking);
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };
            $scope.d__clasId_disabled = 
                () => {
                    return self.d__clasId_disabled();
                };
            $scope.showLov_d__clasId = 
                () => {
                    $timeout( () => {        
                        self.showLov_d__clasId();
                    }, 0);
                };
            $scope.d__ecgrId_disabled = 
                () => {
                    return self.d__ecgrId_disabled();
                };
            $scope.showLov_d__ecgrId = 
                () => {
                    $timeout( () => {        
                        self.showLov_d__ecgrId();
                    }, 0);
                };
            $scope.d__description_disabled = 
                () => {
                    return self.d__description_disabled();
                };
            $scope.d__dateTime_disabled = 
                () => {
                    return self.d__dateTime_disabled();
                };
            $scope.d__hasBeenRun_disabled = 
                () => {
                    return self.d__hasBeenRun_disabled();
                };
            $scope.d__hasPopulatedIntegratedDecisionsAndIssues_disabled = 
                () => {
                    return self.d__hasPopulatedIntegratedDecisionsAndIssues_disabled();
                };

            $scope.clearBtnAction = () => { 
                self.pageModel.clasId = undefined;
                self.pageModel.ecgrId = undefined;
                self.pageModel.description = undefined;
                self.pageModel.dateTime = undefined;
                self.pageModel.hasBeenRun = undefined;
                self.pageModel.hasPopulatedIntegratedDecisionsAndIssues = undefined;
                self.updateGrid(0, false, true);
            };

            $scope.newBtnAction = () => { 
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };

            $scope.onEdit = (x:Entities.DecisionMaking):void => {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };

            $timeout(function() {
                $('#Search_DecisionMaking').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);

            self.updateUI(rowIndexToSelect);
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerDecisionMakingSearch";
        }
        public get HtmlDivId(): string {
            return "Search_DecisionMaking";
        }
    
        public _getPageTitle(): string {
            return "Decision Making";
        }

        public getColumnTooltip(fieldName: string): string {
            if (fieldName === 'hasBeenRun') return "If YES, the Decision Making process has been executed";
            if (fieldName === 'hasPopulatedIntegratedDecisionsAndIssues') return "If YES, the Decision Making has populated the Integrated Decisions and Isues";
            return "";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined},
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'description', displayName:'getALString("Description", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'28%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.description' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'dateTime', displayName:'getALString("Date", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_dateTime' data-ng-model='row.entity.dateTime' data-np-ui-model='row.entity._dateTime' data-ng-change='markEntityAsUpdated(row.entity,&quot;dateTime&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'clasId.name', displayName:'getALString("Data Import Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.clasId.name' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'ecgrId.name', displayName:'getALString("BRE Rules Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.ecgrId.name' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'hasBeenRun', displayName:'getALString("Has Been Run", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'6%', headerCellTemplate: this.getGridHeaderCellTemplate({ tooltip: "{{getColumnTooltip('hasBeenRun')}}" }), cellTemplate:"<div data-ng-class='col.colIndex()' class='ngCellCheckBox' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_hasBeenRun' data-ng-model='row.entity.hasBeenRun' data-np-ui-model='row.entity._hasBeenRun' data-ng-change='markEntityAsUpdated(row.entity,&quot;hasBeenRun&quot;)' type='checkbox' data-np-checkbox='dummy' data-ng-click='setSelectedRow(row.rowIndex)' data-ng-disabled='true' class='ngCellCheckBox' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'hasPopulatedIntegratedDecisionsAndIssues', displayName:'getALString("is Used", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'6%', headerCellTemplate: this.getGridHeaderCellTemplate({ tooltip: "{{getColumnTooltip('hasPopulatedIntegratedDecisionsAndIssues')}}" }), cellTemplate:"<div data-ng-class='col.colIndex()' class='ngCellCheckBox' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_hasPopulatedIntegratedDecisionsAndIssues' data-ng-model='row.entity.hasPopulatedIntegratedDecisionsAndIssues' data-np-ui-model='row.entity._hasPopulatedIntegratedDecisionsAndIssues' data-ng-change='markEntityAsUpdated(row.entity,&quot;hasPopulatedIntegratedDecisionsAndIssues&quot;)' type='checkbox' data-np-checkbox='dummy' data-ng-click='setSelectedRow(row.rowIndex)' data-ng-disabled='true' class='ngCellCheckBox' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }


        public getPersistedModel():ModelDecisionMakingSearchPersistedModel {
            var self = this;
            var ret = new ModelDecisionMakingSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.clasId = self.pageModel.clasId;
            ret.ecgrId = self.pageModel.ecgrId;
            ret.description = self.pageModel.description;
            ret.dateTime = self.pageModel.dateTime;
            ret.hasBeenRun = self.pageModel.hasBeenRun;
            ret.hasPopulatedIntegratedDecisionsAndIssues = self.pageModel.hasPopulatedIntegratedDecisionsAndIssues;
            return ret;
        }

        public get pageModel():ModelDecisionMakingSearch {
            return <ModelDecisionMakingSearch>this.model;
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            return Entities.DecisionMaking.fromJSONComplete(webResponse.data);
        }

        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = "DecisionMaking/findLazyDecisionMaking";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};

            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;

            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.description)) {
                paramData['description'] = self.pageModel.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.ecgrId) && !isVoid(self.pageModel.ecgrId.ecgrId)) {
                paramData['ecgrId_ecgrId'] = self.pageModel.ecgrId.ecgrId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasBeenRun)) {
                paramData['hasBeenRun'] = self.pageModel.hasBeenRun;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasPopulatedIntegratedDecisionsAndIssues)) {
                paramData['hasPopulatedIntegratedDecisionsAndIssues'] = self.pageModel.hasPopulatedIntegratedDecisionsAndIssues;
            }

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(
                url,
                paramData,
                {timeout:self.$scope.globals.timeoutInMS, cache:false});
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });

        }

        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = "DecisionMaking/findLazyDecisionMaking_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.description)) {
                paramData['description'] = self.pageModel.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.ecgrId) && !isVoid(self.pageModel.ecgrId.ecgrId)) {
                paramData['ecgrId_ecgrId'] = self.pageModel.ecgrId.ecgrId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasBeenRun)) {
                paramData['hasBeenRun'] = self.pageModel.hasBeenRun;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasPopulatedIntegratedDecisionsAndIssues)) {
                paramData['hasPopulatedIntegratedDecisionsAndIssues'] = self.pageModel.hasPopulatedIntegratedDecisionsAndIssues;
            }
            
            var promise = self.$http.post(
                url,
                paramData,
                { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        }

        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
            var paramData = {};

            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.description)) {
                paramData['description'] = self.pageModel.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.ecgrId) && !isVoid(self.pageModel.ecgrId.ecgrId)) {
                paramData['ecgrId_ecgrId'] = self.pageModel.ecgrId.ecgrId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasBeenRun)) {
                paramData['hasBeenRun'] = self.pageModel.hasBeenRun;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasPopulatedIntegratedDecisionsAndIssues)) {
                paramData['hasPopulatedIntegratedDecisionsAndIssues'] = self.pageModel.hasPopulatedIntegratedDecisionsAndIssues;
            }
                
            var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
            return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        }


        public get Current():Entities.DecisionMaking {
            var self = this;
            return <Entities.DecisionMaking>self.$scope.pageModel.selectedEntities[0];
        }

        public onNew():void {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/DecisionMaking', self._getPageTitle(), {});
        }

        public onEdit(x:Entities.DecisionMaking):void {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel:IFormPageBreadcrumbStepModel<Entities.DecisionMaking>= {"selectedEntity":x};
            self.$scope.navigateForward('/DecisionMaking', self._getPageTitle(), visitedPageModel);
        }
        
        private getSynchronizeChangesWithDbUrl(): string {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_DecisionMaking";
        }
        public getSynchronizeChangesWithDbData(x:Entities.DecisionMaking):any {
            var self = this;
            var paramData:any = {
                data: <ChangeToCommit[]>[]
            };
            var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);

            paramData.data.push(changeToCommit);
            return paramData;
        }

        public deleteRecord(x:Entities.DecisionMaking):void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);

            console.log("deleting DecisionMaking with PK field:" + x.demaId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                    self.updateGrid();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        }
        
        public _newIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _searchIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _deleteIsDisabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_D"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = self.isFieldsDisabled(decisionMaking);
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public isFieldsDisabled(decisionMaking:Entities.DecisionMaking):boolean { 
            console.warn("Unimplemented function isFieldsDisabled()");
            return false; 
        }
        public _editBtnIsDisabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_R"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public d__clasId_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_d__clasId:ModelLovClassificationBase;
        public showLov_d__clasId() {
            var self = this;
            var uimodel:NpTypes.IUIModel = self.pageModel._clasId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Data Import Name';
            dialogOptions.previousModel= self.cachedLovModel_d__clasId;
            dialogOptions.className = "ControllerLovClassification";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var classification1:Entities.Classification =   <Entities.Classification>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(classification1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(classification1) && classification1.isEqual(self.pageModel.clasId))
                    return;
                self.pageModel.clasId = classification1;
            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovClassificationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_d__clasId = lovModel;
                    var wsPath = "Classification/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovClassificationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Classification/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;
            dialogOptions.shownCols['dateTime'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Classification.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public d__ecgrId_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_d__ecgrId:ModelLovEcGroupBase;
        public showLov_d__ecgrId() {
            var self = this;
            var uimodel:NpTypes.IUIModel = self.pageModel._ecgrId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'BRE Rules';
            dialogOptions.previousModel= self.cachedLovModel_d__ecgrId;
            dialogOptions.className = "ControllerLovEcGroup";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var ecGroup1:Entities.EcGroup =   <Entities.EcGroup>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(ecGroup1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(ecGroup1) && ecGroup1.isEqual(self.pageModel.ecgrId))
                    return;
                self.pageModel.ecgrId = ecGroup1;
            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovEcGroupBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_d__ecgrId = lovModel;
                    var wsPath = "EcGroup/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovEcGroupBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "EcGroup/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/EcGroup.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public d__description_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public d__dateTime_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public d__hasBeenRun_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public d__hasPopulatedIntegratedDecisionsAndIssues_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }

    }
}
