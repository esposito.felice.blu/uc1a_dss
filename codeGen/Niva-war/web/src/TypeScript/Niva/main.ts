/// <reference path="version.ts" />
// /// <reference path="common.ts" />
/// <reference path="globals.ts" />
/// <reference path="../RTL/directives.ts" />
/// <reference path="../RTL/services.ts" />
/// <reference path="../RTL/npTypes.ts" />
/// <reference path="../RTL/Entities/RTL_Subscriber.ts" />
/// <reference path="../RTL/Controllers/controllerMain.ts" />
/// <reference path="../RTL/Controllers/controllerNpBreadcrumb.ts" />
/// <reference path="../RTL/Controllers/controllerMenu.ts" />
/// <reference path="../RTL/Controllers/controllerHelp.ts" />
/// <reference path="../RTL/Controllers/controllerChangePass.ts" />
/// <reference path="../RTL/Controllers/controllerLogin.ts" />
/// <reference path="../RTL/Controllers/controllerLovRTL_Subscriber.ts" />
/// <reference path="../RTL/Controllers/controllerRequestAuthorization.ts" />
/// <reference path="../RTL/Controllers/controllerWait.ts" />
/// <reference path="../RTL/Controllers/controllerMsgBox.ts" />
/// <reference path="../RTL/Controllers/controllerJump.ts" />
/// <reference path="../RTL/Controllers/controllerHoles.ts" />
/// <reference path="../RTL/Controllers/controllerInput.ts" />
/// <reference path="../RTL/Controllers/controllerLayers.ts" />
/// <reference path="../RTL/Controllers/controllerChooseGeom.ts" />
/// <reference path="../RTL/Controllers/controllerChooseFeature.ts" />
/// <reference path="../RTL/Controllers/controllerExclusiveLayers.ts" />
/// <reference path="../RTL/Controllers/controllerSearchLayers.ts" />
/// <reference path="../RTL/Controllers/BaseController.ts" />
/// <reference path="../RTL/Controllers/controllerChooseExclusiveGeom.ts" />
/// <reference path="../RTL/Controllers/controllerChooseLevel1Geom.ts" />
/// <reference path="../RTL/Controllers/controllerChooseLevel2Geom.ts" />
/// <reference path="../RTL/Controllers/controllerChooseFeature.ts" />
/// <reference path="../RTL/Controllers/controllerExportMap.ts" />
/// <reference path="../RTL/Controllers/controllerLegend.ts" />
/// <reference path="../RTL/Controllers/controllerChooseSplitLine.ts" />
/// <reference path="../RTL/Controllers/controllerExcelFileImportResultsDlg.ts" />
/// <reference path="messages.ts" />



var application = angular.module('angularNivaApp', ['ui.bootstrap', 'ngGrid', 'ngCookies'/*, '$strap.directives'*/]);

application.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/login', {templateUrl: 'partials/login.html?rev=' + g_version}).
    when('/welcome', {templateUrl: 'partials/welcome.html?rev=' + g_version,
                      resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }  }).
    when('/Agency', { templateUrl: 'partials/Agency/Agency.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/ClassificationSearch', { templateUrl: 'partials/Classification/ClassificationSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/Classification', { templateUrl: 'partials/Classification/Classification.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/Classifier', { templateUrl: 'partials/Classifier/Classifier.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/CoverType', { templateUrl: 'partials/CoverType/CoverType.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/Cultivation', { templateUrl: 'partials/Cultivation/Cultivation.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/DecisionMakingSearch', { templateUrl: 'partials/DecisionMaking/DecisionMakingSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/DecisionMaking', { templateUrl: 'partials/DecisionMaking/DecisionMaking.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/DecisionMakingROSearch', { templateUrl: 'partials/DecisionMakingRO/DecisionMakingROSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/DecisionMakingRO', { templateUrl: 'partials/DecisionMakingRO/DecisionMakingRO.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/EcGroupSearch', { templateUrl: 'partials/EcGroup/EcGroupSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/EcGroup', { templateUrl: 'partials/EcGroup/EcGroup.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/FileTemplateSearch', { templateUrl: 'partials/FileTemplate/FileTemplateSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/FileTemplate', { templateUrl: 'partials/FileTemplate/FileTemplate.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/Document', { templateUrl: 'partials/Document/Document.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/FileDirPath', { templateUrl: 'partials/FileDirPath/FileDirPath.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/IntegrateddecisionSearch', { templateUrl: 'partials/Integrateddecision/IntegrateddecisionSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/Integrateddecision', { templateUrl: 'partials/Integrateddecision/Integrateddecision.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/ParcelGPSearch', { templateUrl: 'partials/ParcelGP/ParcelGPSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/ParcelGP', { templateUrl: 'partials/ParcelGP/ParcelGP.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/AgrisnapUsersSearch', { templateUrl: 'partials/AgrisnapUsers/AgrisnapUsersSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/AgrisnapUsers', { templateUrl: 'partials/AgrisnapUsers/AgrisnapUsers.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/ParcelsIssuesSearch', { templateUrl: 'partials/ParcelsIssues/ParcelsIssuesSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/ParcelsIssues', { templateUrl: 'partials/ParcelsIssues/ParcelsIssues.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/ProducersSearch', { templateUrl: 'partials/Producers/ProducersSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/Producers', { templateUrl: 'partials/Producers/Producers.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/DashboardSearch', { templateUrl: 'partials/Dashboard/DashboardSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/Dashboard', { templateUrl: 'partials/Dashboard/Dashboard.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/ParcelFMISSearch', { templateUrl: 'partials/ParcelFMIS/ParcelFMISSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/ParcelFMIS', { templateUrl: 'partials/ParcelFMIS/ParcelFMIS.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/FmisUserSearch', { templateUrl: 'partials/FmisUser/FmisUserSearch.html?rev=' + g_version,
                               resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    when('/FmisUser', { templateUrl: 'partials/FmisUser/FmisUser.html?rev=' + g_version,
                         resolve: { isAuthenticated: (LoginService: Services.LoginService) => { return LoginService.authenticationPromise() } }}).
    otherwise(                 { redirectTo: '/welcome' });
}]);


// The application's controllers
application.controller('controllerMain', Controllers.controllerMain);
application.controller('controllerNpBreadcrumb', Controllers.controllerNpBreadcrumb);
application.controller('controllerMenu', Controllers.controllerMenu);
application.controller('controllerHelp', Controllers.controllerHelp);
application.controller('controllerChangePass', Controllers.controllerChangePass);
application.controller('controllerLogin', Controllers.controllerLogin);
application.controller('ControllerLovRTL_Subscriber', Controllers.ControllerLovRTL_Subscriber);
application.controller('controllerRequestAuthorization', Controllers.controllerRequestAuthorization);
application.controller('controllerWait', Controllers.controllerWait);
application.controller('controllerMsgBox', Controllers.controllerMsgBox);
application.controller('controllerJump', Controllers.controllerJump);
application.controller('controllerHole', Controllers.controllerHole);                                                                   
application.controller('controllerInput', Controllers.controllerInput);
application.controller('controllerLayers', Controllers.controllerLayers);
application.controller('controllerChooseGeom', Controllers.controllerChooseGeom);
application.controller('controllerChooseFeature', Controllers.controllerChooseFeature);
application.controller('controllerExclusiveLayers', Controllers.controllerExclusiveLayers);
application.controller('controllerSearchLayers', Controllers.controllerSearchLayers);
application.controller('dynamicSqlLayerFilterController', Controllers.DynamicSqlLayerFilterController);

application.controller('controllerChooseExclusiveGeom', Controllers.controllerChooseExclusiveGeom);
application.controller('controllerChooseLevel1Geom', Controllers.controllerChooseLevel1Geom);
application.controller('controllerChooseLevel2Geom', Controllers.controllerChooseLevel2Geom);
application.controller('controllerChooseFeature', Controllers.controllerChooseFeature);
application.controller('controllerExportMap', Controllers.controllerExportMap);
application.controller('controllerLegend', Controllers.controllerLegend);
application.controller('controllerChooseSplitLine', Controllers.controllerChooseSplitLine);

application.controller('ExcelFileImportResultsDlg.PageController', Controllers.ExcelFileImportResultsDlg.PageController);
application.controller('ExcelFileImportResultsDlg.ControllerGrpExcelFile', Controllers.ExcelFileImportResultsDlg.ControllerGrpExcelFile);
application.controller('ExcelFileImportResultsDlg.ControllerGrpExcelError', Controllers.ExcelFileImportResultsDlg.ControllerGrpExcelError);


application.controller('ControllerWelcomePage', 
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        if (g_controllers['ControllerWelcomePage'] === undefined) throw 'Controller ControllerWelcomePage not found!';
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerWelcomePage($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Agency.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'Agency', 'Agency.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Agency.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Agency.ControllerGrpAgency',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Agency.ControllerGrpAgency($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Classification.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'Classification', 'Classification.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Classification.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerClassificationSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'ClassificationSearch', 'ControllerClassificationSearch', 'ControllerClassificationSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerClassificationSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Classification.ControllerGrpClas',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Classification.ControllerGrpClas($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Classification.ControllerGrpParcelClas',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Classification.ControllerGrpParcelClas($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Classifier.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'Classifier', 'Classifier.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Classifier.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Classifier.ControllerGrpClassifier',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Classifier.ControllerGrpClassifier($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('CoverType.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'CoverType', 'CoverType.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).CoverType.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('CoverType.ControllerGrpCoverType',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).CoverType.ControllerGrpCoverType($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Cultivation.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'Cultivation', 'Cultivation.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Cultivation.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Cultivation.ControllerGrpCultivation',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Cultivation.ControllerGrpCultivation($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMaking.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'DecisionMaking', 'DecisionMaking.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMaking.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerDecisionMakingSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'DecisionMakingSearch', 'ControllerDecisionMakingSearch', 'ControllerDecisionMakingSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerDecisionMakingSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMaking.ControllerGrpDema',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMaking.ControllerGrpDema($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMaking.ControllerGrpParcelDecision',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMaking.ControllerGrpParcelDecision($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMaking.ControllerGrpEcGroupCopy',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMaking.ControllerGrpEcGroupCopy($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMaking.ControllerGrpEcCultCopy',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMaking.ControllerGrpEcCultCopy($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMaking.ControllerGrpEcCultDetailCopy',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMaking.ControllerGrpEcCultDetailCopy($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMaking.ControllerEcCultCopyCover',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMaking.ControllerEcCultCopyCover($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMaking.ControllerEcCultDetailCopyCover',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMaking.ControllerEcCultDetailCopyCover($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMaking.ControllerEcCultCopySuper',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMaking.ControllerEcCultCopySuper($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMaking.ControllerEcCultDetailCopySuper',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMaking.ControllerEcCultDetailCopySuper($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMakingRO.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'DecisionMakingRO', 'DecisionMakingRO.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMakingRO.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerDecisionMakingROSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'DecisionMakingROSearch', 'ControllerDecisionMakingROSearch', 'ControllerDecisionMakingROSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerDecisionMakingROSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMakingRO.ControllerGrpDema',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMakingRO.ControllerGrpDema($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('DecisionMakingRO.ControllerGrpIntegrateddecision',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).DecisionMakingRO.ControllerGrpIntegrateddecision($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('EcGroup.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'EcGroup', 'EcGroup.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).EcGroup.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerEcGroupSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'EcGroupSearch', 'ControllerEcGroupSearch', 'ControllerEcGroupSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerEcGroupSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('EcGroup.ControllerGrpEcGroup',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).EcGroup.ControllerGrpEcGroup($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('EcGroup.ControllerGrpEcCult',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).EcGroup.ControllerGrpEcCult($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('EcGroup.ControllerGrpEcCultDetail',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).EcGroup.ControllerGrpEcCultDetail($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('EcGroup.ControllerEcCultCover',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).EcGroup.ControllerEcCultCover($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('EcGroup.ControllerEcCultDetailCover',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).EcGroup.ControllerEcCultDetailCover($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('EcGroup.ControllerEcCultSuper',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).EcGroup.ControllerEcCultSuper($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('EcGroup.ControllerEcCultDetailSuper',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).EcGroup.ControllerEcCultDetailSuper($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('FileTemplate.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'FileTemplate', 'FileTemplate.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).FileTemplate.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerFileTemplateSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'FileTemplateSearch', 'ControllerFileTemplateSearch', 'ControllerFileTemplateSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerFileTemplateSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('FileTemplate.ControllerGrpFileTemplate',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).FileTemplate.ControllerGrpFileTemplate($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('FileTemplate.ControllerGrpTemplateColumn',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).FileTemplate.ControllerGrpTemplateColumn($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Document.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'Document', 'Document.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Document.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Document.ControllerGrpDocument',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Document.ControllerGrpDocument($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('FileDirPath.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'FileDirPath', 'FileDirPath.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).FileDirPath.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('FileDirPath.ControllerGrpFileDirPath',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).FileDirPath.ControllerGrpFileDirPath($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Integrateddecision.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'Integrateddecision', 'Integrateddecision.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Integrateddecision.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerIntegrateddecisionSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'IntegrateddecisionSearch', 'ControllerIntegrateddecisionSearch', 'ControllerIntegrateddecisionSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerIntegrateddecisionSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Integrateddecision.ControllerGrpIntegrateddecision',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Integrateddecision.ControllerGrpIntegrateddecision($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelGP.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'ParcelGP', 'ParcelGP.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelGP.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerParcelGPSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'ParcelGPSearch', 'ControllerParcelGPSearch', 'ControllerParcelGPSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerParcelGPSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelGP.ControllerGrpParcelClas',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelGP.ControllerGrpParcelClas($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelGP.ControllerGrpParcelsIssues',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelGP.ControllerGrpParcelsIssues($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelGP.ControllerGrpGpDecision',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelGP.ControllerGrpGpDecision($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelGP.ControllerGrpGpRequestsContexts',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelGP.ControllerGrpGpRequestsContexts($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelGP.ControllerGrpGpUpload',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelGP.ControllerGrpGpUpload($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('AgrisnapUsers.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'AgrisnapUsers', 'AgrisnapUsers.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).AgrisnapUsers.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerAgrisnapUsersSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'AgrisnapUsersSearch', 'ControllerAgrisnapUsersSearch', 'ControllerAgrisnapUsersSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerAgrisnapUsersSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('AgrisnapUsers.ControllerGrpAgrisnapUsers',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).AgrisnapUsers.ControllerGrpAgrisnapUsers($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('AgrisnapUsers.ControllerGrpAgrisnapUsersProducers',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).AgrisnapUsers.ControllerGrpAgrisnapUsersProducers($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelsIssues.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'ParcelsIssues', 'ParcelsIssues.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelsIssues.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerParcelsIssuesSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'ParcelsIssuesSearch', 'ControllerParcelsIssuesSearch', 'ControllerParcelsIssuesSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerParcelsIssuesSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelsIssues.ControllerGrpParcelsIssues',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelsIssues.ControllerGrpParcelsIssues($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelsIssues.ControllerGrpGpDecision',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelsIssues.ControllerGrpGpDecision($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelsIssues.ControllerGrpGpRequestsContexts',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelsIssues.ControllerGrpGpRequestsContexts($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelsIssues.ControllerGrpGpUpload',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelsIssues.ControllerGrpGpUpload($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelsIssues.ControllerGrpParcelsIssuesActivities',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelsIssues.ControllerGrpParcelsIssuesActivities($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Producers.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'Producers', 'Producers.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Producers.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerProducersSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'ProducersSearch', 'ControllerProducersSearch', 'ControllerProducersSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerProducersSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Producers.ControllerGrpProducers',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Producers.ControllerGrpProducers($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Dashboard.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'Dashboard', 'Dashboard.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Dashboard.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerDashboardSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'DashboardSearch', 'ControllerDashboardSearch', 'ControllerDashboardSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerDashboardSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Dashboard.ControllerGrpParcelClas',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Dashboard.ControllerGrpParcelClas($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Dashboard.ControllerGrpParcelDecision',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Dashboard.ControllerGrpParcelDecision($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Dashboard.ControllerGrpParcelsIssues',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Dashboard.ControllerGrpParcelsIssues($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Dashboard.ControllerGrpGpDecision',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Dashboard.ControllerGrpGpDecision($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Dashboard.ControllerGrpFmisDecision',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Dashboard.ControllerGrpFmisDecision($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('Dashboard.ControllerGrpIntegrateddecision',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).Dashboard.ControllerGrpIntegrateddecision($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelFMIS.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'ParcelFMIS', 'ParcelFMIS.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelFMIS.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerParcelFMISSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'ParcelFMISSearch', 'ControllerParcelFMISSearch', 'ControllerParcelFMISSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerParcelFMISSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelFMIS.ControllerGrpParcelClas',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelFMIS.ControllerGrpParcelClas($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelFMIS.ControllerGrpParcelsIssues',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelFMIS.ControllerGrpParcelsIssues($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelFMIS.ControllerGrpFmisDecision',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelFMIS.ControllerGrpFmisDecision($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ParcelFMIS.ControllerGrpFmisUpload',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ParcelFMIS.ControllerGrpFmisUpload($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('FmisUser.PageController',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'FmisUser', 'FmisUser.PageController', 'PageController');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).FmisUser.PageController($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerFmisUserSearch',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'FmisUserSearch', 'ControllerFmisUserSearch', 'ControllerFmisUserSearch');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerFmisUserSearch($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('FmisUser.ControllerGrpFmisUser',
    ($scope, $http, $timeout, Plato, NavigationService, $q) => {
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).FmisUser.ControllerGrpFmisUser($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovClassification',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovClassification', 'ControllerLovClassification', 'ControllerLovClassification');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovClassification($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovClassifier',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovClassifier', 'ControllerLovClassifier', 'ControllerLovClassifier');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovClassifier($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovCoverType',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovCoverType', 'ControllerLovCoverType', 'ControllerLovCoverType');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovCoverType($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovCultivation',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovCultivation', 'ControllerLovCultivation', 'ControllerLovCultivation');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovCultivation($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovDecisionMaking',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovDecisionMaking', 'ControllerLovDecisionMaking', 'ControllerLovDecisionMaking');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovDecisionMaking($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovEcGroup',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovEcGroup', 'ControllerLovEcGroup', 'ControllerLovEcGroup');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovEcGroup($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovFileTemplate',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovFileTemplate', 'ControllerLovFileTemplate', 'ControllerLovFileTemplate');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovFileTemplate($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovParcelClas',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovParcelClas', 'ControllerLovParcelClas', 'ControllerLovParcelClas');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovParcelClas($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovPredefCol',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovPredefCol', 'ControllerLovPredefCol', 'ControllerLovPredefCol');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovPredefCol($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovGpRequestsContexts',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovGpRequestsContexts', 'ControllerLovGpRequestsContexts', 'ControllerLovGpRequestsContexts');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovGpRequestsContexts($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovGpRequestsProducers',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovGpRequestsProducers', 'ControllerLovGpRequestsProducers', 'ControllerLovGpRequestsProducers');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovGpRequestsProducers($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovParcelsIssues',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovParcelsIssues', 'ControllerLovParcelsIssues', 'ControllerLovParcelsIssues');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovParcelsIssues($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });
application.controller('ControllerLovProducers',
    ($scope, $http, $timeout, Plato, NavigationService, $q, $element) => {
        controllerFactory($scope, $element, 'LovProducers', 'ControllerLovProducers', 'ControllerLovProducers');
        var ctrl: NpTypes.IAbstractController = new (<any>Controllers).ControllerLovProducers($scope, $http, $timeout, Plato);
        ctrl.initialize(NavigationService, $q);
    });

var controllerFactory = function ($scope, $element, ctrlScriptName:string, pageCtrlName:string, pageCtrlClassName:string): void {
    var ctrlLoadedSuccessfully: boolean = false;
    if (g_controllers[pageCtrlName] == undefined) {
        ctrlLoadedSuccessfully = syncControllerLoader($scope, ctrlScriptName);
    } else {
        ctrlLoadedSuccessfully = true;
    }

    var elem:HTMLElement = $element[0];

    // Initially, in partial, the div where the controller is used, has property style="display:none".
    // Tis is done because, until Controller is loaded, the div is vissible unconstructed
    // If the controller is downloaded successfully we reomve the "display:none" property.
    if (ctrlLoadedSuccessfully) {
        if (!isVoid(elem)) { elem.style.display = ''; };
    } else {
        if (!isVoid(elem)) {
            var parElem = elem.parentElement;
            if (parElem.id.startsWith("npInnerDlg")) {
                $('#' + parElem.id).remove()
                $scope.globals.findAndRemoveDialogOptionByClassName(pageCtrlClassName);
            } else {
                $('#' + elem.id).remove();
            }
        }
        alert($scope.globals.getDynamicMessage("ServerCommunicationError"));
    }
}
var syncControllerLoader = function ($scope, ctrlScriptName: string): boolean {
    var xhrObj = new XMLHttpRequest;
    var url = 'js/Niva/ControllersBase/' + ctrlScriptName + 'Base.js?rev=' + g_version;
    var res = false; 
    xhrObj.open('GET', url, false);
    xhrObj.send();
    if (xhrObj.status == 200) {
        var responseScript = xhrObj.responseText;
        jQuery.globalEval(responseScript);

        url = 'js/Niva/Controllers/' + ctrlScriptName + '.js?rev=' + g_version;
        xhrObj.open('GET', url, false);
        xhrObj.send();

        if (xhrObj.status == 200) {
            responseScript = xhrObj.responseText;
            jQuery.globalEval(responseScript);
            res = true;
        } 
    } 
    return res;
}
application.run(function($rootScope: NpTypes.IApplicationScope, $timeout) {
    $rootScope.globals = new Globals();
    $rootScope.globals.appName = 'Niva';
    $rootScope.globals.languages = ['en'];
    
    $rootScope.setActiveLangByGlobalLang = () => {Messages.setActiveLangByGlobalLang($rootScope)};
    $rootScope.getALString = Messages.getActiveLangString;
    
    $rootScope.globals.version = g_version;
    $rootScope.globals.mainAppTemplate.npHeaderPartialUrl = "/" + $rootScope.globals.appName + "/partials/mainAppNpHeader.html?rev=" + $rootScope.globals.version;
    $rootScope.globals.mainAppTemplate.npBreadcrumbUrl = "/" + $rootScope.globals.appName + "/partials/mainAppNpBreadcrumb.html?rev=" + $rootScope.globals.version;

    window.setInterval(() => {
        if ($rootScope.globals.bDigestHackEnabled) {
            //console.log("$digest called");
            $rootScope.$digest();
            if ($rootScope.globals.nInFlightRequests === 0) {
                $("html").removeClass("wait");
                $('.plzWaitDialog').remove();
            }
        }
    }, 1000);
});

application.run(['$route', function($route)  {
    $route.reload();
}]);

Directives.addNewDirectives(application);
Services.addNewServices(application);
