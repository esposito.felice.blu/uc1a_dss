//96CB564F9A66B8C8236FE26F5902F7D9
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovCoverTypeBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovCoverType = (function (_super) {
        __extends(ModelLovCoverType, _super);
        function ModelLovCoverType() {
            _super.apply(this, arguments);
        }
        return ModelLovCoverType;
    })(Controllers.ModelLovCoverTypeBase);
    Controllers.ModelLovCoverType = ModelLovCoverType;
    var ControllerLovCoverType = (function (_super) {
        __extends(ControllerLovCoverType, _super);
        function ControllerLovCoverType($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovCoverType($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovCoverType;
    })(Controllers.ControllerLovCoverTypeBase);
    Controllers.ControllerLovCoverType = ControllerLovCoverType;
    g_controllers['ControllerLovCoverType'] = Controllers.ControllerLovCoverType;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovCoverType.js.map