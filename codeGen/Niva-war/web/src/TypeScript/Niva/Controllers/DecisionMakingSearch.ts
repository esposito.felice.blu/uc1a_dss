//5D7BDBF32C05A2C467C3B671DDD8DE96
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DecisionMakingSearchBase.ts" />

module Controllers {

    export class ModelDecisionMakingSearch extends ModelDecisionMakingSearchBase {
    }

    export interface IScopeDecisionMakingSearch extends IScopeDecisionMakingSearchBase {
    }

    export class ControllerDecisionMakingSearch extends ControllerDecisionMakingSearchBase {
        
        constructor(
            public $scope: IScopeDecisionMakingSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelDecisionMakingSearch($scope));
        }

        public getLabelById(itmId: string): string {
            switch (itmId) {
                case 'saveBtn_id':
                    return 'Save'
                case 'searchBtn_id':
                    return 'Search';
                case 'clearBtn_id':
                    return 'Clear';
                case 'newBtn_id':
                    return 'Add New Decision Making';
                case 'newPageBtn_id':
                    return 'Add New Decision Making';
                case 'cancelBtn_id':
                    return 'Back';
                case 'deleteBtn_id':
                    return 'Delete';
                case 'auditBtn_id':
                    return '?';
            }
            return itmId;
        }

    }

    g_controllers['ControllerDecisionMakingSearch'] = Controllers.ControllerDecisionMakingSearch;


    
}
