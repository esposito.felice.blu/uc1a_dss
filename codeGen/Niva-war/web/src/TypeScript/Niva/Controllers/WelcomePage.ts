//02F6B7243CE13729C516CE4022303143
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
/// <reference path="../version.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../ControllersBase/WelcomePageBase.ts" />

module Controllers {

    export class ModelWelcomePage extends ModelWelcomePageBase {
    }

    export interface IScopeWelcomePage extends IScopeWelcomePageBase {
    }

    export class ControllerWelcomePage extends Controllers.ControllerWelcomePageBase {
        
        constructor(
            public $scope: IScopeWelcomePage,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelWelcomePage($scope));
        }
    }

    
    g_controllers['ControllerWelcomePage'] = Controllers.ControllerWelcomePage;
}

