//2955DD99667ED098043A73C8E3BD5F32
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/EcGroupSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelEcGroupSearch = (function (_super) {
        __extends(ModelEcGroupSearch, _super);
        function ModelEcGroupSearch() {
            _super.apply(this, arguments);
        }
        return ModelEcGroupSearch;
    })(Controllers.ModelEcGroupSearchBase);
    Controllers.ModelEcGroupSearch = ModelEcGroupSearch;
    var ControllerEcGroupSearch = (function (_super) {
        __extends(ControllerEcGroupSearch, _super);
        function ControllerEcGroupSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelEcGroupSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerEcGroupSearch;
    })(Controllers.ControllerEcGroupSearchBase);
    Controllers.ControllerEcGroupSearch = ControllerEcGroupSearch;
    g_controllers['ControllerEcGroupSearch'] = Controllers.ControllerEcGroupSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=EcGroupSearch.js.map