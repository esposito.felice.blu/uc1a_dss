//AAD86DB52F28FDF30BB1C6E1F6F85104
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovGpRequestsBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovGpRequests = (function (_super) {
        __extends(ModelLovGpRequests, _super);
        function ModelLovGpRequests() {
            _super.apply(this, arguments);
        }
        return ModelLovGpRequests;
    })(Controllers.ModelLovGpRequestsBase);
    Controllers.ModelLovGpRequests = ModelLovGpRequests;
    var ControllerLovGpRequests = (function (_super) {
        __extends(ControllerLovGpRequests, _super);
        function ControllerLovGpRequests($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovGpRequests($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovGpRequests;
    })(Controllers.ControllerLovGpRequestsBase);
    Controllers.ControllerLovGpRequests = ControllerLovGpRequests;
    g_controllers['ControllerLovGpRequests'] = Controllers.ControllerLovGpRequests;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovGpRequests.js.map