//E2C5A98641EBEDD2CD9CD5A528AED9F6
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/CultivationBase.ts" />

module Controllers.Cultivation {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {

        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
        public successUpdate(fileName: string, jsonResponse: any): void {
            var self = this;

            self.model.modelGrpCultivation.controller.updateUI();
        }


        public isDeleteSelectedCropsDisabled(): boolean {
            // MT - Enables or disables the Delete Selected Crops button according to the existence of checked crops.

            var self = this;
            var nrof_checkedEntities = self.model.modelGrpCultivation.grid.selectedRowsCount();
            var isDisabled = false;

            if (nrof_checkedEntities > 0) {
                isDisabled = false;
            } else {
                isDisabled = true;
            }
            return isDisabled;
        }




        public deleteSelectedCrops(): void {

            // MT - Deletes multiple Crop records, selected by checkboxes.

            var self = this;
            var rowIndex: number = 0;

            let func: (entList: Entities.Cultivation[]) => void = (entList: Entities.Cultivation[]) => {
                //console.log(entList);
                entList.forEach(function (ent: Entities.Cultivation) {
                    //console.log(ent);
                    self.model.modelGrpCultivation.controller.deleteEntity(ent, false, rowIndex, false);
                });
                self.model.modelGrpCultivation.controller.checkedEntities = {};
                self.model.modelGrpCultivation.controller.updateUI();
            };

            self.model.modelGrpCultivation.controller.getCheckedEntities(func);
        }

    }

    g_controllers['Cultivation.PageController'] = Controllers.Cultivation.PageController;



    export class ModelGrpCultivation extends ModelGrpCultivationBase {
    }


    export interface IScopeGrpCultivation extends IScopeGrpCultivationBase {
    }

    export class ControllerGrpCultivation extends ControllerGrpCultivationBase {
        constructor(
            public $scope: IScopeGrpCultivation,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpCultivation($scope));
        }

    }


    g_controllers['Cultivation.ControllerGrpCultivation'] = Controllers.Cultivation.ControllerGrpCultivation;

}
