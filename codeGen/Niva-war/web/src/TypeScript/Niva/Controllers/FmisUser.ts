//22D138BE5E0838ED30F6828758F4D1EA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/FmisUserBase.ts" />

module Controllers.FmisUser {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['FmisUser.PageController'] = Controllers.FmisUser.PageController;



    export class ModelGrpFmisUser extends ModelGrpFmisUserBase {
    }


    export interface IScopeGrpFmisUser extends IScopeGrpFmisUserBase {
    }

    export class ControllerGrpFmisUser extends ControllerGrpFmisUserBase {
        constructor(
            public $scope: IScopeGrpFmisUser,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpFmisUser($scope) );
        }
    }
    g_controllers['FmisUser.ControllerGrpFmisUser'] = Controllers.FmisUser.ControllerGrpFmisUser;
    
}
