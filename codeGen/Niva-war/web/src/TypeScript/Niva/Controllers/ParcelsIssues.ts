//C205B88873999D9571E04DF8FC4FFC7B
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelsIssuesBase.ts" />

module Controllers.ParcelsIssues {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['ParcelsIssues.PageController'] = Controllers.ParcelsIssues.PageController;



    export class ModelGrpParcelsIssues extends ModelGrpParcelsIssuesBase {
    }


    export interface IScopeGrpParcelsIssues extends IScopeGrpParcelsIssuesBase {
    }

    export class ControllerGrpParcelsIssues extends ControllerGrpParcelsIssuesBase {
        constructor(
            public $scope: IScopeGrpParcelsIssues,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelsIssues($scope) );
        }
    }
    g_controllers['ParcelsIssues.ControllerGrpParcelsIssues'] = Controllers.ParcelsIssues.ControllerGrpParcelsIssues;

    export class ModelGrpGpDecision extends ModelGrpGpDecisionBase {
    }


    export interface IScopeGrpGpDecision extends IScopeGrpGpDecisionBase {
    }

    export class ControllerGrpGpDecision extends ControllerGrpGpDecisionBase {
        constructor(
            public $scope: IScopeGrpGpDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpGpDecision($scope) );
        }
    }
    g_controllers['ParcelsIssues.ControllerGrpGpDecision'] = Controllers.ParcelsIssues.ControllerGrpGpDecision;

    export class ModelGrpGpRequestsContexts extends ModelGrpGpRequestsContextsBase {
    }


    export interface IScopeGrpGpRequestsContexts extends IScopeGrpGpRequestsContextsBase {
    }

    export class ControllerGrpGpRequestsContexts extends ControllerGrpGpRequestsContextsBase {
        constructor(
            public $scope: IScopeGrpGpRequestsContexts,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpGpRequestsContexts($scope) );
        }
    }
    g_controllers['ParcelsIssues.ControllerGrpGpRequestsContexts'] = Controllers.ParcelsIssues.ControllerGrpGpRequestsContexts;

    export class ModelGrpGpUpload extends ModelGrpGpUploadBase {
    }


    export interface IScopeGrpGpUpload extends IScopeGrpGpUploadBase {
    }

    export class ControllerGrpGpUpload extends ControllerGrpGpUploadBase {
        constructor(
            public $scope: IScopeGrpGpUpload,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpGpUpload($scope) );
        }
    }
    g_controllers['ParcelsIssues.ControllerGrpGpUpload'] = Controllers.ParcelsIssues.ControllerGrpGpUpload;

    export class ModelGrpParcelsIssuesActivities extends ModelGrpParcelsIssuesActivitiesBase {
    }


    export interface IScopeGrpParcelsIssuesActivities extends IScopeGrpParcelsIssuesActivitiesBase {
    }

    export class ControllerGrpParcelsIssuesActivities extends ControllerGrpParcelsIssuesActivitiesBase {
        constructor(
            public $scope: IScopeGrpParcelsIssuesActivities,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelsIssuesActivities($scope) );
        }
    }
    g_controllers['ParcelsIssues.ControllerGrpParcelsIssuesActivities'] = Controllers.ParcelsIssues.ControllerGrpParcelsIssuesActivities;
    
}
