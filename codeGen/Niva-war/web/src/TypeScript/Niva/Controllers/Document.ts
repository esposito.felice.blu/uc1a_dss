//1865ADC11B521158221BD2E9F71ACF6F
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DocumentBase.ts" />

module Controllers.Document {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }


    }

    g_controllers['Document.PageController'] = Controllers.Document.PageController;



    export class ModelGrpDocument extends ModelGrpDocumentBase {
    }


    export interface IScopeGrpDocument extends IScopeGrpDocumentBase {
    }

    export class ControllerGrpDocument extends ControllerGrpDocumentBase {
        constructor(
            public $scope: IScopeGrpDocument,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpDocument($scope) );
            // document.getElementById('vgp').innerHTML = "<button class="btnDownload"><i class="fa fa-download"></i> Download</button>";
        }

 

    }
    g_controllers['Document.ControllerGrpDocument'] = Controllers.Document.ControllerGrpDocument;
    
}
