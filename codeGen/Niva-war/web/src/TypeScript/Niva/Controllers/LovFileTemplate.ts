//931F954669BB6A042C0EDB85B9574AC7
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovFileTemplateBase.ts" />

module Controllers {

    export class ModelLovFileTemplate extends ModelLovFileTemplateBase {
    }

    export interface IScopeLovFileTemplate extends IScopeLovFileTemplateBase {
    }

    export class ControllerLovFileTemplate extends ControllerLovFileTemplateBase {
        
        constructor(
            public $scope: IScopeLovFileTemplate,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovFileTemplate($scope));
        }
    }

    g_controllers['ControllerLovFileTemplate'] = Controllers.ControllerLovFileTemplate;


    
}
