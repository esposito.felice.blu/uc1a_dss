//194AB1DB5B87D7A7447D037C9A97C397
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelGPBase.ts" />
/// <reference path="../Services/MainService.ts" />

module Controllers.ParcelGP {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {

    }

    export class PageController extends PageControllerBase {

        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }


        public getPageChanges(bForDelete: boolean = false): Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];

            if (bForDelete) {

            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getChangesToCommit());
            }

            var hasParcelClas = pageChanges.some(change => change.entityName === "ParcelClas")

            return pageChanges;
        }




        public onSuccesfullSaveFromButton(response: NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);


            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);

            //call web service
            var paramData: Services.MainService_updateIntegratedDecisionsNIssues_req = new Services.MainService_updateIntegratedDecisionsNIssues_req();
            paramData.pclaId = self.$scope.pageModel.modelGrpParcelClas.pclaId;
            console.log("Calling Update Integrated Decisions Service...." + self.$scope.pageModel.modelGrpParcelClas.pclaId);

            Services.MainService.updateIntegratedDecisionsNIssues(self.$scope.pageModel.controller, paramData, respDataList => {

                //self.updateUI();

                NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Update was Completed Succesfully.");

                respDataList.forEach(item => NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item));
            }
            );



        }
    }

    g_controllers['ParcelGP.PageController'] = Controllers.ParcelGP.PageController;



    export class ModelGrpParcelClas extends ModelGrpParcelClasBase {
    }


    export interface IScopeGrpParcelClas extends IScopeGrpParcelClasBase {
    }

    export class ControllerGrpParcelClas extends ControllerGrpParcelClasBase {
        constructor(
            public $scope: IScopeGrpParcelClas,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelClas($scope));
        }
    }
    g_controllers['ParcelGP.ControllerGrpParcelClas'] = Controllers.ParcelGP.ControllerGrpParcelClas;

    export class ModelGrpParcelsIssues extends ModelGrpParcelsIssuesBase {
    }


    export interface IScopeGrpParcelsIssues extends IScopeGrpParcelsIssuesBase {
    }

    export class ControllerGrpParcelsIssues extends ControllerGrpParcelsIssuesBase {
        constructor(
            public $scope: IScopeGrpParcelsIssues,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelsIssues($scope));
        }
    }
    g_controllers['ParcelGP.ControllerGrpParcelsIssues'] = Controllers.ParcelGP.ControllerGrpParcelsIssues;

    export class ModelGrpGpDecision extends ModelGrpGpDecisionBase {
    }


    export interface IScopeGrpGpDecision extends IScopeGrpGpDecisionBase {


    }

    export class ControllerGrpGpDecision extends ControllerGrpGpDecisionBase {
        constructor(
            public $scope: IScopeGrpGpDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpGpDecision($scope));
        }

        public isCheckMade(gpDecision: Entities.GpDecision): boolean {
            var self = this;
            return !gpDecision.isNew();
        }


    }
    g_controllers['ParcelGP.ControllerGrpGpDecision'] = Controllers.ParcelGP.ControllerGrpGpDecision;

    export class ModelGrpGpRequestsContexts extends ModelGrpGpRequestsContextsBase {
    }


    export interface IScopeGrpGpRequestsContexts extends IScopeGrpGpRequestsContextsBase {
    }

    export class ControllerGrpGpRequestsContexts extends ControllerGrpGpRequestsContextsBase {
        constructor(
            public $scope: IScopeGrpGpRequestsContexts,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpGpRequestsContexts($scope));
        }
    }
    g_controllers['ParcelGP.ControllerGrpGpRequestsContexts'] = Controllers.ParcelGP.ControllerGrpGpRequestsContexts;

    export class ModelGrpGpUpload extends ModelGrpGpUploadBase {
    }


    export interface IScopeGrpGpUpload extends IScopeGrpGpUploadBase {
    }

    export class ControllerGrpGpUpload extends ControllerGrpGpUploadBase {
        constructor(
            public $scope: IScopeGrpGpUpload,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpGpUpload($scope));
        }


        public viewGeotaggegPhoto(gpUpload: Entities.GpUpload) {
            var self = this;

            var srcUrlEncoded = "/Niva/rest/GpUpload/getImage?id=" + encodeURIComponent(gpUpload.gpUploadsId);


            document.getElementById('vgp').innerHTML = "<img id=\"vgpgp\" src=\"" + srcUrlEncoded + "\" />";

        }

    }
    g_controllers['ParcelGP.ControllerGrpGpUpload'] = Controllers.ParcelGP.ControllerGrpGpUpload;

}
