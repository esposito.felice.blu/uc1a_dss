//E8C07693F14ADAB0018CD310888546C5
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DecisionMakingROBase.ts" />
/// <reference path="../Services/MainService.ts" />

module Controllers.DecisionMakingRO {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {

        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['DecisionMakingRO.PageController'] = Controllers.DecisionMakingRO.PageController;



    export class ModelGrpDema extends ModelGrpDemaBase {
    }


    export interface IScopeGrpDema extends IScopeGrpDemaBase {
    }

    export class ControllerGrpDema extends ControllerGrpDemaBase {
        constructor(
            public $scope: IScopeGrpDema,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpDema($scope));
        }


    }
    g_controllers['DecisionMakingRO.ControllerGrpDema'] = Controllers.DecisionMakingRO.ControllerGrpDema;

    export class ModelGrpIntegrateddecision extends ModelGrpIntegrateddecisionBase {
    }


    export interface IScopeGrpIntegrateddecision extends IScopeGrpIntegrateddecisionBase {
    }

    export class ControllerGrpIntegrateddecision extends ControllerGrpIntegrateddecisionBase {
        constructor(
            public $scope: IScopeGrpIntegrateddecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpIntegrateddecision($scope));

            }


            public exportIntegratedDecisionsToCSV_fileName() {
                var self = this;
                return "IntegratedParcelDecisions" + "_" + Utils.convertDateToString(new Date(), "yyyy_MM_dd_HHmm") + ".csv";
            }
            public exportIntegratedDecisionsToCSV() {
                var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_toCSV";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['__fields'] = self.getExcelFieldDefinitions();
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_decisionCode)) {
                    paramData['fsch_decisionCode'] = self.modelGrpIntegrateddecision.fsch_decisionCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpIntegrateddecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpIntegrateddecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpIntegrateddecision.fsch_Parcel_Identifier;
                }
    
                NpTypes.AlertMessage.clearAlerts(self.PageModel);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                        if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                        var data = response;
                        var blob = new Blob([data], { type: "text/csv;charset=utf-8" });
                        saveAs(blob, self.exportIntegratedDecisionsToCSV_fileName());
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                        if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                        NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                    });
    
            }

            public actionPushToCommonsAPI(integrateddecision:Entities.Integrateddecision) {


                var self = this;
                
                // Checking Modifications
                if (self.$scope.globals.isCurrentTransactionDirty) {
    
                    //Error was detected
                    NpTypes.AlertMessage.clearAlerts(this.$scope.pageModel);
                    NpTypes.AlertMessage.addDanger(this.$scope.pageModel, "Please save before Push");
                    return;
                }
    
                //Ask Confirmatiom 
    
                messageBox(self.$scope, self.Plato, "Info", "Do you want to Push Traffic Lights to WP4 CMDS API?",
                    IconKind.QUESTION,
                    [
                        new Tuple2("Yes", () => {
    
                            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
    
                            //call web service
                            var paramData: Services.MainService_actionPushToCommonsAPI_req = new Services.MainService_actionPushToCommonsAPI_req();
                            paramData.demaId = integrateddecision.demaId.demaId;
                            Services.MainService.actionPushToCommonsAPI(self.$scope.pageModel.controller, paramData, respDataList => {
                                     
                                self.updateUI;
                               
                                NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                                NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Push was executed succesfully!");
    
                                respDataList.forEach(item => NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item));
                            });
                        }),
                        new Tuple2("No", () => { })
                    ], 1, 1, '18em');
    
    
            }



        

        public createPclaId_geom4326Map(defaultOptions: NpGeo.NpMapConstructionOptions) {
            var self: ControllerGrpIntegrateddecisionBase = this;

            defaultOptions.layers = [
            new NpGeoLayers.OpenStreetTileLayer(true),
           
            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                layerId: 'INPMP_ParcelGreen',
                label: 'Green Parcels',
                isVisible: true,
                getQueryUrl: () => '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen?',
                getQueryInputs: (Integrateddecision: Entities.Integrateddecision) => { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null } },
                fillColor: 'rgba(0, 255, 0, 0.8)',
                borderColor: 'rgba(0,0,0,1.0)',
                penWidth: 1,
                selectable: true,
                showSpatialSearchFilters: false
            }),
            
            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                layerId: 'INPMP_ParcelYellow',
                label: 'Yellow Parcels',
                isVisible: true,
                getQueryUrl: () => '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow?',
                getQueryInputs: (Integrateddecision: Entities.Integrateddecision) => { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null } },
                fillColor: 'rgba(255, 255, 0, 0.8)',
                borderColor: 'rgba(0,0,0,1.0)',
                penWidth: 1,
                selectable: true,
                showSpatialSearchFilters: false
            }),
            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                layerId: 'INPMP_ParcelRed',
                label: 'Red Parcels',
                isVisible: true,
                getQueryUrl: () => '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed?',
                getQueryInputs: (Integrateddecision: Entities.Integrateddecision) => { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null } },
                fillColor: 'rgba(255, 0, 0, 0.8)',
                borderColor: 'rgba(0,0,0,1.0)',
                penWidth: 1,
                selectable: true,
                showSpatialSearchFilters: false
            }),
            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                layerId: 'INPMP_ParcelUndefined',
                label: 'Undefined Parcels',
                isVisible: true,
                getQueryUrl: () => '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined?',
                getQueryInputs: (Integrateddecision: Entities.Integrateddecision) => { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null } },
                fillColor: 'rgba(255,255,255, 0.5)',
                borderColor: 'rgba(0,0,0,0.5)',
                penWidth: 1,
                selectable: true,
                showSpatialSearchFilters: false
            })
        
        ];
            defaultOptions.zoomOnEntityChange = true;
            defaultOptions.showCoordinates = true;
            defaultOptions.showRuler = true;
            defaultOptions.centerPoint = [49.843056, 9.901944];
            defaultOptions.zoomLevel = 10;
            defaultOptions.fillColor = "rgba(50, 0,255, 0.1)";
            defaultOptions.borderColor = "blue";
            //  defaultOptions.labelColor?: string;
            defaultOptions.penWidth = 4;
            defaultOptions.opacity = 0.5;
            defaultOptions.layerSelectorWidthInPx = 170;
            defaultOptions.coordinateSystem = "EPSG:4326";
            // mapId?: string;
            defaultOptions.showJumpButton = true;
            defaultOptions.showEditButtons = false;
            defaultOptions.showMeasureButton = false;
            defaultOptions.showLegendButton = false;




            // defaultOptions.getEntityLabel?: (currentEntity: any) => string;

            // defaultOptions.getFeatureLabel?: (feature: ol.Feature, lightweight: boolean) => string; // it should be property of vectory layer
            //  defaultOptions.topologyType?: number; //0|undfined = none, 1=client topology, 2:serverside topology
            //topologyFunc?: (feature: ol.geom.Geometry, resp : ) => void


            defaultOptions.fullScreenOnlyMap = false;//true or false. If true only mapdiv shall be used for full screen control. If false or ommited use map+datagrid

            this.pclaId_geom4326Map = new NpGeo.NpMap(defaultOptions);

        }
    }
    g_controllers['DecisionMakingRO.ControllerGrpIntegrateddecision'] = Controllers.DecisionMakingRO.ControllerGrpIntegrateddecision;

}
