//BFB627E1548FEB44F048D5C7553232B7
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/SuperClasBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var SuperClas;
    (function (SuperClas) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(SuperClas.PageModelBase);
        SuperClas.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(SuperClas.PageControllerBase);
        SuperClas.PageController = PageController;
        g_controllers['SuperClas.PageController'] = Controllers.SuperClas.PageController;
        var ModelGrpSuperClas = (function (_super) {
            __extends(ModelGrpSuperClas, _super);
            function ModelGrpSuperClas() {
                _super.apply(this, arguments);
            }
            return ModelGrpSuperClas;
        })(SuperClas.ModelGrpSuperClasBase);
        SuperClas.ModelGrpSuperClas = ModelGrpSuperClas;
        var ControllerGrpSuperClas = (function (_super) {
            __extends(ControllerGrpSuperClas, _super);
            function ControllerGrpSuperClas($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpSuperClas($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpSuperClas;
        })(SuperClas.ControllerGrpSuperClasBase);
        SuperClas.ControllerGrpSuperClas = ControllerGrpSuperClas;
        g_controllers['SuperClas.ControllerGrpSuperClas'] = Controllers.SuperClas.ControllerGrpSuperClas;
        var ModelGrpSuperClassDetail = (function (_super) {
            __extends(ModelGrpSuperClassDetail, _super);
            function ModelGrpSuperClassDetail() {
                _super.apply(this, arguments);
            }
            return ModelGrpSuperClassDetail;
        })(SuperClas.ModelGrpSuperClassDetailBase);
        SuperClas.ModelGrpSuperClassDetail = ModelGrpSuperClassDetail;
        var ControllerGrpSuperClassDetail = (function (_super) {
            __extends(ControllerGrpSuperClassDetail, _super);
            function ControllerGrpSuperClassDetail($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpSuperClassDetail($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpSuperClassDetail;
        })(SuperClas.ControllerGrpSuperClassDetailBase);
        SuperClas.ControllerGrpSuperClassDetail = ControllerGrpSuperClassDetail;
        g_controllers['SuperClas.ControllerGrpSuperClassDetail'] = Controllers.SuperClas.ControllerGrpSuperClassDetail;
    })(SuperClas = Controllers.SuperClas || (Controllers.SuperClas = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=SuperClas.js.map