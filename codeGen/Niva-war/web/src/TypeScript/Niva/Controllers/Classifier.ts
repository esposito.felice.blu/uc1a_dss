//78DDFE2B9900FB37659CC313596AB963
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ClassifierBase.ts" />

module Controllers.Classifier {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['Classifier.PageController'] = Controllers.Classifier.PageController;



    export class ModelGrpClassifier extends ModelGrpClassifierBase {
    }


    export interface IScopeGrpClassifier extends IScopeGrpClassifierBase {
    }

    export class ControllerGrpClassifier extends ControllerGrpClassifierBase {
        constructor(
            public $scope: IScopeGrpClassifier,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpClassifier($scope) );
        }
    }
    g_controllers['Classifier.ControllerGrpClassifier'] = Controllers.Classifier.ControllerGrpClassifier;
    
}
