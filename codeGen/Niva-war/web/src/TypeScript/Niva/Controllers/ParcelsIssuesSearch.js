//5585D323DF5C3F1CE617EEE20CAD0E0E
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelsIssuesSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelParcelsIssuesSearch = (function (_super) {
        __extends(ModelParcelsIssuesSearch, _super);
        function ModelParcelsIssuesSearch() {
            _super.apply(this, arguments);
        }
        return ModelParcelsIssuesSearch;
    })(Controllers.ModelParcelsIssuesSearchBase);
    Controllers.ModelParcelsIssuesSearch = ModelParcelsIssuesSearch;
    var ControllerParcelsIssuesSearch = (function (_super) {
        __extends(ControllerParcelsIssuesSearch, _super);
        function ControllerParcelsIssuesSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelParcelsIssuesSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerParcelsIssuesSearch;
    })(Controllers.ControllerParcelsIssuesSearchBase);
    Controllers.ControllerParcelsIssuesSearch = ControllerParcelsIssuesSearch;
    g_controllers['ControllerParcelsIssuesSearch'] = Controllers.ControllerParcelsIssuesSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelsIssuesSearch.js.map