//3C60C77742B50F57EF2272DCCEC66767
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovGpRequestsContextsBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovGpRequestsContexts = (function (_super) {
        __extends(ModelLovGpRequestsContexts, _super);
        function ModelLovGpRequestsContexts() {
            _super.apply(this, arguments);
        }
        return ModelLovGpRequestsContexts;
    })(Controllers.ModelLovGpRequestsContextsBase);
    Controllers.ModelLovGpRequestsContexts = ModelLovGpRequestsContexts;
    var ControllerLovGpRequestsContexts = (function (_super) {
        __extends(ControllerLovGpRequestsContexts, _super);
        function ControllerLovGpRequestsContexts($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovGpRequestsContexts($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovGpRequestsContexts;
    })(Controllers.ControllerLovGpRequestsContextsBase);
    Controllers.ControllerLovGpRequestsContexts = ControllerLovGpRequestsContexts;
    g_controllers['ControllerLovGpRequestsContexts'] = Controllers.ControllerLovGpRequestsContexts;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovGpRequestsContexts.js.map