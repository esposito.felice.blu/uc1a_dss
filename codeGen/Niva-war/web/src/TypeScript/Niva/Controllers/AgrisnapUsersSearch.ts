//A69668F14683EA59BE5264D36FF27952
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/AgrisnapUsersSearchBase.ts" />

module Controllers {

    export class ModelAgrisnapUsersSearch extends ModelAgrisnapUsersSearchBase {
    }

    export interface IScopeAgrisnapUsersSearch extends IScopeAgrisnapUsersSearchBase {
    }

    export class ControllerAgrisnapUsersSearch extends ControllerAgrisnapUsersSearchBase {
        
        constructor(
            public $scope: IScopeAgrisnapUsersSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelAgrisnapUsersSearch($scope));
        }
    }

    g_controllers['ControllerAgrisnapUsersSearch'] = Controllers.ControllerAgrisnapUsersSearch;


    
}
