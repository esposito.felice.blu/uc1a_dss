//02F6B7243CE13729C516CE4022303143
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
/// <reference path="../version.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../ControllersBase/WelcomePageBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelWelcomePage = (function (_super) {
        __extends(ModelWelcomePage, _super);
        function ModelWelcomePage() {
            _super.apply(this, arguments);
        }
        return ModelWelcomePage;
    })(Controllers.ModelWelcomePageBase);
    Controllers.ModelWelcomePage = ModelWelcomePage;
    var ControllerWelcomePage = (function (_super) {
        __extends(ControllerWelcomePage, _super);
        function ControllerWelcomePage($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelWelcomePage($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerWelcomePage;
    })(Controllers.ControllerWelcomePageBase);
    Controllers.ControllerWelcomePage = ControllerWelcomePage;
    g_controllers['ControllerWelcomePage'] = Controllers.ControllerWelcomePage;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=WelcomePage.js.map