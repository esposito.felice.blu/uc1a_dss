//E2DEC7B5D5EB1B50DAC472DE03D72A2E
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovDecisionMakingBase.ts" />

module Controllers {

    export class ModelLovDecisionMaking extends ModelLovDecisionMakingBase {
    }

    export interface IScopeLovDecisionMaking extends IScopeLovDecisionMakingBase {
    }

    export class ControllerLovDecisionMaking extends ControllerLovDecisionMakingBase {
        
        constructor(
            public $scope: IScopeLovDecisionMaking,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovDecisionMaking($scope));
        }
    }

    g_controllers['ControllerLovDecisionMaking'] = Controllers.ControllerLovDecisionMaking;


    
}
