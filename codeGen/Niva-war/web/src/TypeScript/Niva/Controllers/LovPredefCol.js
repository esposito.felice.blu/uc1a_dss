//B0318D8B8F3E90006E3DB9705DDB0F00
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovPredefColBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovPredefCol = (function (_super) {
        __extends(ModelLovPredefCol, _super);
        function ModelLovPredefCol() {
            _super.apply(this, arguments);
        }
        return ModelLovPredefCol;
    })(Controllers.ModelLovPredefColBase);
    Controllers.ModelLovPredefCol = ModelLovPredefCol;
    var ControllerLovPredefCol = (function (_super) {
        __extends(ControllerLovPredefCol, _super);
        function ControllerLovPredefCol($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovPredefCol($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovPredefCol;
    })(Controllers.ControllerLovPredefColBase);
    Controllers.ControllerLovPredefCol = ControllerLovPredefCol;
    g_controllers['ControllerLovPredefCol'] = Controllers.ControllerLovPredefCol;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovPredefCol.js.map