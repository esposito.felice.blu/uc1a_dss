//52B523633F88A673B7F04246A8949715
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DeclarationBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var Declaration;
    (function (Declaration) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(Declaration.PageModelBase);
        Declaration.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(Declaration.PageControllerBase);
        Declaration.PageController = PageController;
        g_controllers['Declaration.PageController'] = Controllers.Declaration.PageController;
        var ModelGrpDeclaration = (function (_super) {
            __extends(ModelGrpDeclaration, _super);
            function ModelGrpDeclaration() {
                _super.apply(this, arguments);
            }
            return ModelGrpDeclaration;
        })(Declaration.ModelGrpDeclarationBase);
        Declaration.ModelGrpDeclaration = ModelGrpDeclaration;
        var ControllerGrpDeclaration = (function (_super) {
            __extends(ControllerGrpDeclaration, _super);
            function ControllerGrpDeclaration($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpDeclaration($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpDeclaration;
        })(Declaration.ControllerGrpDeclarationBase);
        Declaration.ControllerGrpDeclaration = ControllerGrpDeclaration;
        g_controllers['Declaration.ControllerGrpDeclaration'] = Controllers.Declaration.ControllerGrpDeclaration;
    })(Declaration = Controllers.Declaration || (Controllers.Declaration = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=Declaration.js.map