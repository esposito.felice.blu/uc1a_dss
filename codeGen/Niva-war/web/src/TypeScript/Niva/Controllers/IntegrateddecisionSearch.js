//89686FBEB0A899B6E187414A4E96473B
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/IntegrateddecisionSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelIntegrateddecisionSearch = (function (_super) {
        __extends(ModelIntegrateddecisionSearch, _super);
        function ModelIntegrateddecisionSearch() {
            _super.apply(this, arguments);
        }
        return ModelIntegrateddecisionSearch;
    })(Controllers.ModelIntegrateddecisionSearchBase);
    Controllers.ModelIntegrateddecisionSearch = ModelIntegrateddecisionSearch;
    var ControllerIntegrateddecisionSearch = (function (_super) {
        __extends(ControllerIntegrateddecisionSearch, _super);
        function ControllerIntegrateddecisionSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelIntegrateddecisionSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerIntegrateddecisionSearch;
    })(Controllers.ControllerIntegrateddecisionSearchBase);
    Controllers.ControllerIntegrateddecisionSearch = ControllerIntegrateddecisionSearch;
    g_controllers['ControllerIntegrateddecisionSearch'] = Controllers.ControllerIntegrateddecisionSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=IntegrateddecisionSearch.js.map