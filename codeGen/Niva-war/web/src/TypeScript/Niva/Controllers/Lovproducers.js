//1CA554B1E3970220602949BC13DDB5A8
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovProducersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovProducers = (function (_super) {
        __extends(ModelLovProducers, _super);
        function ModelLovProducers() {
            _super.apply(this, arguments);
        }
        return ModelLovProducers;
    })(Controllers.ModelLovProducersBase);
    Controllers.ModelLovProducers = ModelLovProducers;
    var ControllerLovProducers = (function (_super) {
        __extends(ControllerLovProducers, _super);
        function ControllerLovProducers($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovProducers($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovProducers;
    })(Controllers.ControllerLovProducersBase);
    Controllers.ControllerLovProducers = ControllerLovProducers;
    g_controllers['ControllerLovProducers'] = Controllers.ControllerLovProducers;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovProducers.js.map