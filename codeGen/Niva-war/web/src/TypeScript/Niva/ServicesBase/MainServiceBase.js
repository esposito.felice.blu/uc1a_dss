/// <reference path="../../RTL/npTypes.ts" />
var Services;
(function (Services) {
    var MainServiceBase = (function () {
        function MainServiceBase() {
        }
        MainServiceBase.finalizationEcGroup = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/finalizationEcGroup?";
            ctrl.httpPost(url, dataRequest, function (response) {
                var respList = response.data;
                func(respList);
            }, errFunc);
        };
        MainServiceBase.runningDecisionMaking = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/runningDecisionMaking?";
            ctrl.httpPost(url, dataRequest, function (response) {
                var respList = response.data;
                func(respList);
            }, errFunc, { timeout: 300000 });
        };
        MainServiceBase.importingClassification = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/importingClassification?";
            ctrl.httpPost(url, dataRequest, function (response) {
                var respList = response.data;
                func(respList);
            }, errFunc, { timeout: 300000 });
        };
        MainServiceBase.getPhotoRequests = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/getPhotoRequests?";
            ctrl.httpPost(url, dataRequest, function (response) {
                func(response.data);
            }, errFunc);
        };
        MainServiceBase.uploadPhoto = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/uploadPhoto?";
            ctrl.httpPost(url, dataRequest, function (response) {
                func(response.data);
            }, errFunc, { timeout: 180000 });
        };
        MainServiceBase.getBRERunsResultsList = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/getBRERunsResultsList?";
            ctrl.httpPost(url, dataRequest, function (response) {
                func(response.data);
            }, errFunc);
        };
        MainServiceBase.getBRERunResults = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/getBRERunResults?";
            ctrl.httpPost(url, dataRequest, function (response) {
                func(response.data);
            }, errFunc, { timeout: 300000 });
        };
        MainServiceBase.getFMISRequests = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/getFMISRequests?";
            ctrl.httpPost(url, dataRequest, function (response) {
                func(response.data);
            }, errFunc);
        };
        MainServiceBase.uploadFMIS = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/uploadFMIS?";
            ctrl.httpPost(url, dataRequest, function (response) {
                func(response.data);
            }, errFunc, { timeout: 120000 });
        };
        MainServiceBase.updateIntegratedDecisionsNIssues = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/updateIntegratedDecisionsNIssues?";
            ctrl.httpPost(url, dataRequest, function (response) {
                var respList = response.data;
                func(respList);
            }, errFunc);
        };
        MainServiceBase.updateIntegratedDecisionsNIssuesFromDema = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/updateIntegratedDecisionsNIssuesFromDema?";
            ctrl.httpPost(url, dataRequest, function (response) {
                var respList = response.data;
                func(respList);
            }, errFunc);
        };
        MainServiceBase.actionPushToCommonsAPI = function (ctrl, dataRequest, func, errFunc) {
            var url = "/Niva/rest/MainService/actionPushToCommonsAPI?";
            ctrl.httpPost(url, dataRequest, function (response) {
                var respList = response.data;
                func(respList);
            }, errFunc, { timeout: 180000 });
        };
        return MainServiceBase;
    })();
    Services.MainServiceBase = MainServiceBase;
    var MainService_finalizationEcGroup_req = (function () {
        function MainService_finalizationEcGroup_req() {
        }
        return MainService_finalizationEcGroup_req;
    })();
    Services.MainService_finalizationEcGroup_req = MainService_finalizationEcGroup_req;
    var MainService_runningDecisionMaking_req = (function () {
        function MainService_runningDecisionMaking_req() {
        }
        return MainService_runningDecisionMaking_req;
    })();
    Services.MainService_runningDecisionMaking_req = MainService_runningDecisionMaking_req;
    var MainService_importingClassification_req = (function () {
        function MainService_importingClassification_req() {
        }
        return MainService_importingClassification_req;
    })();
    Services.MainService_importingClassification_req = MainService_importingClassification_req;
    var MainService_getPhotoRequests_req = (function () {
        function MainService_getPhotoRequests_req() {
        }
        return MainService_getPhotoRequests_req;
    })();
    Services.MainService_getPhotoRequests_req = MainService_getPhotoRequests_req;
    var MainService_uploadPhoto_req = (function () {
        function MainService_uploadPhoto_req() {
        }
        return MainService_uploadPhoto_req;
    })();
    Services.MainService_uploadPhoto_req = MainService_uploadPhoto_req;
    var MainService_getBRERunsResultsList_req = (function () {
        function MainService_getBRERunsResultsList_req() {
        }
        return MainService_getBRERunsResultsList_req;
    })();
    Services.MainService_getBRERunsResultsList_req = MainService_getBRERunsResultsList_req;
    var MainService_getBRERunResults_req = (function () {
        function MainService_getBRERunResults_req() {
        }
        return MainService_getBRERunResults_req;
    })();
    Services.MainService_getBRERunResults_req = MainService_getBRERunResults_req;
    var MainService_getFMISRequests_req = (function () {
        function MainService_getFMISRequests_req() {
        }
        return MainService_getFMISRequests_req;
    })();
    Services.MainService_getFMISRequests_req = MainService_getFMISRequests_req;
    var MainService_uploadFMIS_req = (function () {
        function MainService_uploadFMIS_req() {
        }
        return MainService_uploadFMIS_req;
    })();
    Services.MainService_uploadFMIS_req = MainService_uploadFMIS_req;
    var MainService_updateIntegratedDecisionsNIssues_req = (function () {
        function MainService_updateIntegratedDecisionsNIssues_req() {
        }
        return MainService_updateIntegratedDecisionsNIssues_req;
    })();
    Services.MainService_updateIntegratedDecisionsNIssues_req = MainService_updateIntegratedDecisionsNIssues_req;
    var MainService_updateIntegratedDecisionsNIssuesFromDema_req = (function () {
        function MainService_updateIntegratedDecisionsNIssuesFromDema_req() {
        }
        return MainService_updateIntegratedDecisionsNIssuesFromDema_req;
    })();
    Services.MainService_updateIntegratedDecisionsNIssuesFromDema_req = MainService_updateIntegratedDecisionsNIssuesFromDema_req;
    var MainService_actionPushToCommonsAPI_req = (function () {
        function MainService_actionPushToCommonsAPI_req() {
        }
        return MainService_actionPushToCommonsAPI_req;
    })();
    Services.MainService_actionPushToCommonsAPI_req = MainService_actionPushToCommonsAPI_req;
})(Services || (Services = {}));
//# sourceMappingURL=MainServiceBase.js.map