/// <reference path="version.ts" />
// /// <reference path="common.ts" />
/// <reference path="globals.ts" />
/// <reference path="../RTL/directives.ts" />
/// <reference path="../RTL/services.ts" />
/// <reference path="../RTL/npTypes.ts" />
/// <reference path="../RTL/Entities/RTL_Subscriber.ts" />
/// <reference path="../RTL/Controllers/controllerMain.ts" />
/// <reference path="../RTL/Controllers/controllerNpBreadcrumb.ts" />
/// <reference path="../RTL/Controllers/controllerMenu.ts" />
/// <reference path="../RTL/Controllers/controllerHelp.ts" />
/// <reference path="../RTL/Controllers/controllerChangePass.ts" />
/// <reference path="../RTL/Controllers/controllerLogin.ts" />
/// <reference path="../RTL/Controllers/controllerLovRTL_Subscriber.ts" />
/// <reference path="../RTL/Controllers/controllerRequestAuthorization.ts" />
/// <reference path="../RTL/Controllers/controllerWait.ts" />
/// <reference path="../RTL/Controllers/controllerMsgBox.ts" />
/// <reference path="../RTL/Controllers/controllerJump.ts" />
/// <reference path="../RTL/Controllers/controllerHoles.ts" />
/// <reference path="../RTL/Controllers/controllerInput.ts" />
/// <reference path="../RTL/Controllers/controllerLayers.ts" />
/// <reference path="../RTL/Controllers/controllerChooseGeom.ts" />
/// <reference path="../RTL/Controllers/controllerChooseFeature.ts" />
/// <reference path="../RTL/Controllers/controllerExclusiveLayers.ts" />
/// <reference path="../RTL/Controllers/controllerSearchLayers.ts" />
/// <reference path="../RTL/Controllers/BaseController.ts" />
/// <reference path="../RTL/Controllers/controllerChooseExclusiveGeom.ts" />
/// <reference path="../RTL/Controllers/controllerChooseLevel1Geom.ts" />
/// <reference path="../RTL/Controllers/controllerChooseLevel2Geom.ts" />
/// <reference path="../RTL/Controllers/controllerChooseFeature.ts" />
/// <reference path="../RTL/Controllers/controllerExportMap.ts" />
/// <reference path="../RTL/Controllers/controllerLegend.ts" />
/// <reference path="../RTL/Controllers/controllerChooseSplitLine.ts" />
/// <reference path="../RTL/Controllers/controllerExcelFileImportResultsDlg.ts" />
/// <reference path="messages.ts" />
var application = angular.module('angularNivaApp', ['ui.bootstrap', 'ngGrid', 'ngCookies' /*, '$strap.directives'*/]);
application.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/login', { templateUrl: 'partials/login.html?rev=' + g_version }).
            when('/welcome', { templateUrl: 'partials/welcome.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/Agency', { templateUrl: 'partials/Agency/Agency.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/ClassificationSearch', { templateUrl: 'partials/Classification/ClassificationSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/Classification', { templateUrl: 'partials/Classification/Classification.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/Classifier', { templateUrl: 'partials/Classifier/Classifier.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/CoverType', { templateUrl: 'partials/CoverType/CoverType.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/Cultivation', { templateUrl: 'partials/Cultivation/Cultivation.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/DecisionMakingSearch', { templateUrl: 'partials/DecisionMaking/DecisionMakingSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/DecisionMaking', { templateUrl: 'partials/DecisionMaking/DecisionMaking.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/DecisionMakingROSearch', { templateUrl: 'partials/DecisionMakingRO/DecisionMakingROSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/DecisionMakingRO', { templateUrl: 'partials/DecisionMakingRO/DecisionMakingRO.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/EcGroupSearch', { templateUrl: 'partials/EcGroup/EcGroupSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/EcGroup', { templateUrl: 'partials/EcGroup/EcGroup.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/FileTemplateSearch', { templateUrl: 'partials/FileTemplate/FileTemplateSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/FileTemplate', { templateUrl: 'partials/FileTemplate/FileTemplate.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/Document', { templateUrl: 'partials/Document/Document.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/FileDirPath', { templateUrl: 'partials/FileDirPath/FileDirPath.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/IntegrateddecisionSearch', { templateUrl: 'partials/Integrateddecision/IntegrateddecisionSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/Integrateddecision', { templateUrl: 'partials/Integrateddecision/Integrateddecision.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/ParcelGPSearch', { templateUrl: 'partials/ParcelGP/ParcelGPSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/ParcelGP', { templateUrl: 'partials/ParcelGP/ParcelGP.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/AgrisnapUsersSearch', { templateUrl: 'partials/AgrisnapUsers/AgrisnapUsersSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/AgrisnapUsers', { templateUrl: 'partials/AgrisnapUsers/AgrisnapUsers.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/ParcelsIssuesSearch', { templateUrl: 'partials/ParcelsIssues/ParcelsIssuesSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/ParcelsIssues', { templateUrl: 'partials/ParcelsIssues/ParcelsIssues.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/ProducersSearch', { templateUrl: 'partials/Producers/ProducersSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/Producers', { templateUrl: 'partials/Producers/Producers.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/DashboardSearch', { templateUrl: 'partials/Dashboard/DashboardSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/Dashboard', { templateUrl: 'partials/Dashboard/Dashboard.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/ParcelFMISSearch', { templateUrl: 'partials/ParcelFMIS/ParcelFMISSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/ParcelFMIS', { templateUrl: 'partials/ParcelFMIS/ParcelFMIS.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/FmisUserSearch', { templateUrl: 'partials/FmisUser/FmisUserSearch.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            when('/FmisUser', { templateUrl: 'partials/FmisUser/FmisUser.html?rev=' + g_version,
            resolve: { isAuthenticated: function (LoginService) { return LoginService.authenticationPromise(); } } }).
            otherwise({ redirectTo: '/welcome' });
    }]);
// The application's controllers
application.controller('controllerMain', Controllers.controllerMain);
application.controller('controllerNpBreadcrumb', Controllers.controllerNpBreadcrumb);
application.controller('controllerMenu', Controllers.controllerMenu);
application.controller('controllerHelp', Controllers.controllerHelp);
application.controller('controllerChangePass', Controllers.controllerChangePass);
application.controller('controllerLogin', Controllers.controllerLogin);
application.controller('ControllerLovRTL_Subscriber', Controllers.ControllerLovRTL_Subscriber);
application.controller('controllerRequestAuthorization', Controllers.controllerRequestAuthorization);
application.controller('controllerWait', Controllers.controllerWait);
application.controller('controllerMsgBox', Controllers.controllerMsgBox);
application.controller('controllerJump', Controllers.controllerJump);
application.controller('controllerHole', Controllers.controllerHole);
application.controller('controllerInput', Controllers.controllerInput);
application.controller('controllerLayers', Controllers.controllerLayers);
application.controller('controllerChooseGeom', Controllers.controllerChooseGeom);
application.controller('controllerChooseFeature', Controllers.controllerChooseFeature);
application.controller('controllerExclusiveLayers', Controllers.controllerExclusiveLayers);
application.controller('controllerSearchLayers', Controllers.controllerSearchLayers);
application.controller('dynamicSqlLayerFilterController', Controllers.DynamicSqlLayerFilterController);
application.controller('controllerChooseExclusiveGeom', Controllers.controllerChooseExclusiveGeom);
application.controller('controllerChooseLevel1Geom', Controllers.controllerChooseLevel1Geom);
application.controller('controllerChooseLevel2Geom', Controllers.controllerChooseLevel2Geom);
application.controller('controllerChooseFeature', Controllers.controllerChooseFeature);
application.controller('controllerExportMap', Controllers.controllerExportMap);
application.controller('controllerLegend', Controllers.controllerLegend);
application.controller('controllerChooseSplitLine', Controllers.controllerChooseSplitLine);
application.controller('ExcelFileImportResultsDlg.PageController', Controllers.ExcelFileImportResultsDlg.PageController);
application.controller('ExcelFileImportResultsDlg.ControllerGrpExcelFile', Controllers.ExcelFileImportResultsDlg.ControllerGrpExcelFile);
application.controller('ExcelFileImportResultsDlg.ControllerGrpExcelError', Controllers.ExcelFileImportResultsDlg.ControllerGrpExcelError);
application.controller('ControllerWelcomePage', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    if (g_controllers['ControllerWelcomePage'] === undefined)
        throw 'Controller ControllerWelcomePage not found!';
    var ctrl = new Controllers.ControllerWelcomePage($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Agency.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'Agency', 'Agency.PageController', 'PageController');
    var ctrl = new Controllers.Agency.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Agency.ControllerGrpAgency', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Agency.ControllerGrpAgency($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Classification.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'Classification', 'Classification.PageController', 'PageController');
    var ctrl = new Controllers.Classification.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerClassificationSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'ClassificationSearch', 'ControllerClassificationSearch', 'ControllerClassificationSearch');
    var ctrl = new Controllers.ControllerClassificationSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Classification.ControllerGrpClas', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Classification.ControllerGrpClas($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Classification.ControllerGrpParcelClas', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Classification.ControllerGrpParcelClas($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Classifier.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'Classifier', 'Classifier.PageController', 'PageController');
    var ctrl = new Controllers.Classifier.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Classifier.ControllerGrpClassifier', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Classifier.ControllerGrpClassifier($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('CoverType.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'CoverType', 'CoverType.PageController', 'PageController');
    var ctrl = new Controllers.CoverType.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('CoverType.ControllerGrpCoverType', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.CoverType.ControllerGrpCoverType($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Cultivation.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'Cultivation', 'Cultivation.PageController', 'PageController');
    var ctrl = new Controllers.Cultivation.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Cultivation.ControllerGrpCultivation', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Cultivation.ControllerGrpCultivation($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMaking.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'DecisionMaking', 'DecisionMaking.PageController', 'PageController');
    var ctrl = new Controllers.DecisionMaking.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerDecisionMakingSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'DecisionMakingSearch', 'ControllerDecisionMakingSearch', 'ControllerDecisionMakingSearch');
    var ctrl = new Controllers.ControllerDecisionMakingSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMaking.ControllerGrpDema', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMaking.ControllerGrpDema($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMaking.ControllerGrpParcelDecision', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMaking.ControllerGrpParcelDecision($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMaking.ControllerGrpEcGroupCopy', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMaking.ControllerGrpEcGroupCopy($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMaking.ControllerGrpEcCultCopy', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMaking.ControllerGrpEcCultCopy($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMaking.ControllerGrpEcCultDetailCopy', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMaking.ControllerGrpEcCultDetailCopy($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMaking.ControllerEcCultCopyCover', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMaking.ControllerEcCultCopyCover($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMaking.ControllerEcCultDetailCopyCover', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMaking.ControllerEcCultDetailCopyCover($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMaking.ControllerEcCultCopySuper', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMaking.ControllerEcCultCopySuper($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMaking.ControllerEcCultDetailCopySuper', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMaking.ControllerEcCultDetailCopySuper($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMakingRO.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'DecisionMakingRO', 'DecisionMakingRO.PageController', 'PageController');
    var ctrl = new Controllers.DecisionMakingRO.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerDecisionMakingROSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'DecisionMakingROSearch', 'ControllerDecisionMakingROSearch', 'ControllerDecisionMakingROSearch');
    var ctrl = new Controllers.ControllerDecisionMakingROSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMakingRO.ControllerGrpDema', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMakingRO.ControllerGrpDema($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('DecisionMakingRO.ControllerGrpIntegrateddecision', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.DecisionMakingRO.ControllerGrpIntegrateddecision($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('EcGroup.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'EcGroup', 'EcGroup.PageController', 'PageController');
    var ctrl = new Controllers.EcGroup.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerEcGroupSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'EcGroupSearch', 'ControllerEcGroupSearch', 'ControllerEcGroupSearch');
    var ctrl = new Controllers.ControllerEcGroupSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('EcGroup.ControllerGrpEcGroup', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.EcGroup.ControllerGrpEcGroup($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('EcGroup.ControllerGrpEcCult', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.EcGroup.ControllerGrpEcCult($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('EcGroup.ControllerGrpEcCultDetail', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.EcGroup.ControllerGrpEcCultDetail($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('EcGroup.ControllerEcCultCover', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.EcGroup.ControllerEcCultCover($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('EcGroup.ControllerEcCultDetailCover', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.EcGroup.ControllerEcCultDetailCover($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('EcGroup.ControllerEcCultSuper', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.EcGroup.ControllerEcCultSuper($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('EcGroup.ControllerEcCultDetailSuper', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.EcGroup.ControllerEcCultDetailSuper($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('FileTemplate.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'FileTemplate', 'FileTemplate.PageController', 'PageController');
    var ctrl = new Controllers.FileTemplate.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerFileTemplateSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'FileTemplateSearch', 'ControllerFileTemplateSearch', 'ControllerFileTemplateSearch');
    var ctrl = new Controllers.ControllerFileTemplateSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('FileTemplate.ControllerGrpFileTemplate', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.FileTemplate.ControllerGrpFileTemplate($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('FileTemplate.ControllerGrpTemplateColumn', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.FileTemplate.ControllerGrpTemplateColumn($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Document.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'Document', 'Document.PageController', 'PageController');
    var ctrl = new Controllers.Document.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Document.ControllerGrpDocument', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Document.ControllerGrpDocument($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('FileDirPath.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'FileDirPath', 'FileDirPath.PageController', 'PageController');
    var ctrl = new Controllers.FileDirPath.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('FileDirPath.ControllerGrpFileDirPath', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.FileDirPath.ControllerGrpFileDirPath($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Integrateddecision.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'Integrateddecision', 'Integrateddecision.PageController', 'PageController');
    var ctrl = new Controllers.Integrateddecision.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerIntegrateddecisionSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'IntegrateddecisionSearch', 'ControllerIntegrateddecisionSearch', 'ControllerIntegrateddecisionSearch');
    var ctrl = new Controllers.ControllerIntegrateddecisionSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Integrateddecision.ControllerGrpIntegrateddecision', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Integrateddecision.ControllerGrpIntegrateddecision($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelGP.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'ParcelGP', 'ParcelGP.PageController', 'PageController');
    var ctrl = new Controllers.ParcelGP.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerParcelGPSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'ParcelGPSearch', 'ControllerParcelGPSearch', 'ControllerParcelGPSearch');
    var ctrl = new Controllers.ControllerParcelGPSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelGP.ControllerGrpParcelClas', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelGP.ControllerGrpParcelClas($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelGP.ControllerGrpParcelsIssues', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelGP.ControllerGrpParcelsIssues($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelGP.ControllerGrpGpDecision', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelGP.ControllerGrpGpDecision($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelGP.ControllerGrpGpRequestsContexts', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelGP.ControllerGrpGpRequestsContexts($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelGP.ControllerGrpGpUpload', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelGP.ControllerGrpGpUpload($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('AgrisnapUsers.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'AgrisnapUsers', 'AgrisnapUsers.PageController', 'PageController');
    var ctrl = new Controllers.AgrisnapUsers.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerAgrisnapUsersSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'AgrisnapUsersSearch', 'ControllerAgrisnapUsersSearch', 'ControllerAgrisnapUsersSearch');
    var ctrl = new Controllers.ControllerAgrisnapUsersSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('AgrisnapUsers.ControllerGrpAgrisnapUsers', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.AgrisnapUsers.ControllerGrpAgrisnapUsers($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('AgrisnapUsers.ControllerGrpAgrisnapUsersProducers', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.AgrisnapUsers.ControllerGrpAgrisnapUsersProducers($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelsIssues.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'ParcelsIssues', 'ParcelsIssues.PageController', 'PageController');
    var ctrl = new Controllers.ParcelsIssues.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerParcelsIssuesSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'ParcelsIssuesSearch', 'ControllerParcelsIssuesSearch', 'ControllerParcelsIssuesSearch');
    var ctrl = new Controllers.ControllerParcelsIssuesSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelsIssues.ControllerGrpParcelsIssues', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelsIssues.ControllerGrpParcelsIssues($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelsIssues.ControllerGrpGpDecision', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelsIssues.ControllerGrpGpDecision($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelsIssues.ControllerGrpGpRequestsContexts', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelsIssues.ControllerGrpGpRequestsContexts($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelsIssues.ControllerGrpGpUpload', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelsIssues.ControllerGrpGpUpload($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelsIssues.ControllerGrpParcelsIssuesActivities', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelsIssues.ControllerGrpParcelsIssuesActivities($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Producers.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'Producers', 'Producers.PageController', 'PageController');
    var ctrl = new Controllers.Producers.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerProducersSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'ProducersSearch', 'ControllerProducersSearch', 'ControllerProducersSearch');
    var ctrl = new Controllers.ControllerProducersSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Producers.ControllerGrpProducers', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Producers.ControllerGrpProducers($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Dashboard.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'Dashboard', 'Dashboard.PageController', 'PageController');
    var ctrl = new Controllers.Dashboard.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerDashboardSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'DashboardSearch', 'ControllerDashboardSearch', 'ControllerDashboardSearch');
    var ctrl = new Controllers.ControllerDashboardSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Dashboard.ControllerGrpParcelClas', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Dashboard.ControllerGrpParcelClas($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Dashboard.ControllerGrpParcelDecision', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Dashboard.ControllerGrpParcelDecision($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Dashboard.ControllerGrpParcelsIssues', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Dashboard.ControllerGrpParcelsIssues($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Dashboard.ControllerGrpGpDecision', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Dashboard.ControllerGrpGpDecision($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Dashboard.ControllerGrpFmisDecision', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Dashboard.ControllerGrpFmisDecision($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('Dashboard.ControllerGrpIntegrateddecision', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.Dashboard.ControllerGrpIntegrateddecision($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelFMIS.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'ParcelFMIS', 'ParcelFMIS.PageController', 'PageController');
    var ctrl = new Controllers.ParcelFMIS.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerParcelFMISSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'ParcelFMISSearch', 'ControllerParcelFMISSearch', 'ControllerParcelFMISSearch');
    var ctrl = new Controllers.ControllerParcelFMISSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelFMIS.ControllerGrpParcelClas', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelFMIS.ControllerGrpParcelClas($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelFMIS.ControllerGrpParcelsIssues', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelFMIS.ControllerGrpParcelsIssues($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelFMIS.ControllerGrpFmisDecision', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelFMIS.ControllerGrpFmisDecision($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ParcelFMIS.ControllerGrpFmisUpload', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.ParcelFMIS.ControllerGrpFmisUpload($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('FmisUser.PageController', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'FmisUser', 'FmisUser.PageController', 'PageController');
    var ctrl = new Controllers.FmisUser.PageController($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerFmisUserSearch', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'FmisUserSearch', 'ControllerFmisUserSearch', 'ControllerFmisUserSearch');
    var ctrl = new Controllers.ControllerFmisUserSearch($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('FmisUser.ControllerGrpFmisUser', function ($scope, $http, $timeout, Plato, NavigationService, $q) {
    var ctrl = new Controllers.FmisUser.ControllerGrpFmisUser($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovClassification', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovClassification', 'ControllerLovClassification', 'ControllerLovClassification');
    var ctrl = new Controllers.ControllerLovClassification($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovClassifier', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovClassifier', 'ControllerLovClassifier', 'ControllerLovClassifier');
    var ctrl = new Controllers.ControllerLovClassifier($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovCoverType', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovCoverType', 'ControllerLovCoverType', 'ControllerLovCoverType');
    var ctrl = new Controllers.ControllerLovCoverType($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovCultivation', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovCultivation', 'ControllerLovCultivation', 'ControllerLovCultivation');
    var ctrl = new Controllers.ControllerLovCultivation($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovDecisionMaking', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovDecisionMaking', 'ControllerLovDecisionMaking', 'ControllerLovDecisionMaking');
    var ctrl = new Controllers.ControllerLovDecisionMaking($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovEcGroup', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovEcGroup', 'ControllerLovEcGroup', 'ControllerLovEcGroup');
    var ctrl = new Controllers.ControllerLovEcGroup($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovFileTemplate', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovFileTemplate', 'ControllerLovFileTemplate', 'ControllerLovFileTemplate');
    var ctrl = new Controllers.ControllerLovFileTemplate($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovParcelClas', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovParcelClas', 'ControllerLovParcelClas', 'ControllerLovParcelClas');
    var ctrl = new Controllers.ControllerLovParcelClas($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovPredefCol', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovPredefCol', 'ControllerLovPredefCol', 'ControllerLovPredefCol');
    var ctrl = new Controllers.ControllerLovPredefCol($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovGpRequestsContexts', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovGpRequestsContexts', 'ControllerLovGpRequestsContexts', 'ControllerLovGpRequestsContexts');
    var ctrl = new Controllers.ControllerLovGpRequestsContexts($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovGpRequestsProducers', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovGpRequestsProducers', 'ControllerLovGpRequestsProducers', 'ControllerLovGpRequestsProducers');
    var ctrl = new Controllers.ControllerLovGpRequestsProducers($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovParcelsIssues', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovParcelsIssues', 'ControllerLovParcelsIssues', 'ControllerLovParcelsIssues');
    var ctrl = new Controllers.ControllerLovParcelsIssues($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
application.controller('ControllerLovProducers', function ($scope, $http, $timeout, Plato, NavigationService, $q, $element) {
    controllerFactory($scope, $element, 'LovProducers', 'ControllerLovProducers', 'ControllerLovProducers');
    var ctrl = new Controllers.ControllerLovProducers($scope, $http, $timeout, Plato);
    ctrl.initialize(NavigationService, $q);
});
var controllerFactory = function ($scope, $element, ctrlScriptName, pageCtrlName, pageCtrlClassName) {
    var ctrlLoadedSuccessfully = false;
    if (g_controllers[pageCtrlName] == undefined) {
        ctrlLoadedSuccessfully = syncControllerLoader($scope, ctrlScriptName);
    }
    else {
        ctrlLoadedSuccessfully = true;
    }
    var elem = $element[0];
    // Initially, in partial, the div where the controller is used, has property style="display:none".
    // Tis is done because, until Controller is loaded, the div is vissible unconstructed
    // If the controller is downloaded successfully we reomve the "display:none" property.
    if (ctrlLoadedSuccessfully) {
        if (!isVoid(elem)) {
            elem.style.display = '';
        }
        ;
    }
    else {
        if (!isVoid(elem)) {
            var parElem = elem.parentElement;
            if (parElem.id.startsWith("npInnerDlg")) {
                $('#' + parElem.id).remove();
                $scope.globals.findAndRemoveDialogOptionByClassName(pageCtrlClassName);
            }
            else {
                $('#' + elem.id).remove();
            }
        }
        alert($scope.globals.getDynamicMessage("ServerCommunicationError"));
    }
};
var syncControllerLoader = function ($scope, ctrlScriptName) {
    var xhrObj = new XMLHttpRequest;
    var url = 'js/Niva/ControllersBase/' + ctrlScriptName + 'Base.js?rev=' + g_version;
    var res = false;
    xhrObj.open('GET', url, false);
    xhrObj.send();
    if (xhrObj.status == 200) {
        var responseScript = xhrObj.responseText;
        jQuery.globalEval(responseScript);
        url = 'js/Niva/Controllers/' + ctrlScriptName + '.js?rev=' + g_version;
        xhrObj.open('GET', url, false);
        xhrObj.send();
        if (xhrObj.status == 200) {
            responseScript = xhrObj.responseText;
            jQuery.globalEval(responseScript);
            res = true;
        }
    }
    return res;
};
application.run(function ($rootScope, $timeout) {
    $rootScope.globals = new Globals();
    $rootScope.globals.appName = 'Niva';
    $rootScope.globals.languages = ['en'];
    $rootScope.setActiveLangByGlobalLang = function () { Messages.setActiveLangByGlobalLang($rootScope); };
    $rootScope.getALString = Messages.getActiveLangString;
    $rootScope.globals.version = g_version;
    $rootScope.globals.mainAppTemplate.npHeaderPartialUrl = "/" + $rootScope.globals.appName + "/partials/mainAppNpHeader.html?rev=" + $rootScope.globals.version;
    $rootScope.globals.mainAppTemplate.npBreadcrumbUrl = "/" + $rootScope.globals.appName + "/partials/mainAppNpBreadcrumb.html?rev=" + $rootScope.globals.version;
    window.setInterval(function () {
        if ($rootScope.globals.bDigestHackEnabled) {
            //console.log("$digest called");
            $rootScope.$digest();
            if ($rootScope.globals.nInFlightRequests === 0) {
                $("html").removeClass("wait");
                $('.plzWaitDialog').remove();
            }
        }
    }, 1000);
});
application.run(['$route', function ($route) {
        $route.reload();
    }]);
Directives.addNewDirectives(application);
Services.addNewServices(application);
//# sourceMappingURL=main.js.map