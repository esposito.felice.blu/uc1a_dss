#!/usr/bin/env python
import os
import sys
import time
import re
from sys import stdout

# Colored message ANSI constants
g_red = chr(27) + "[31m" if stdout.isatty() else ""
g_cyan = chr(27) + "[36m" if stdout.isatty() else ""
g_green = chr(27) + "[32m" if stdout.isatty() else ""
g_yellow = chr(27) + "[33m" if stdout.isatty() else ""
g_normal = chr(27) + "[0m" if stdout.isatty() else ""


def panic(msg):
    '''Print error message and abort'''
    if not msg.endswith('\n'):
        msg += "\n"
    if sys.stdout.isatty():
        sys.stderr.write("\n"+chr(27)+"[32m" + msg + chr(27) + "[0m\n")
    else:
        sys.stderr.write(msg)
    sys.exit(1)


def DetectGlobalSettings():
    if "JBOSS_HOME" not in os.environ:
        panic("The JBOSS_HOME environment var is not set... Aborting.")

    if not os.path.exists("project.xml"):
        panic("Please run me from the folder containing project.xml.")

    for line in open("project.xml"):
        m = re.match(r'^<WebApplication.*[ \t]Name="([^"]+)"', line)
        if m:
            appName = m.group(1)
        m = re.match(r'^.*<CodeGenerationFolder>([^<]+)</CodeGe', line)
        if m:
            codeGenFolder = m.group(1)

    if appName == '':
        panic("Could not find application name in project.xml ...")

    print "Detected application " + g_cyan + appName + g_normal + \
        " in folder " + g_yellow + codeGenFolder + g_normal + "..."
    staticPath = codeGenFolder + "/" + appName + '-war/web/'
    return os.path.abspath(os.environ["JBOSS_HOME"]), \
        appName, codeGenFolder, staticPath


def main():
    jbossPath, appName, codeGenFolder, unused_staticPath = \
        DetectGlobalSettings()

    tmpVfs = os.path.abspath(
        os.path.join(jbossPath, "standalone", "tmp", "vfs"))
    if not os.path.isdir(tmpVfs):
        panic("Folder '" + tmpVfs + "' missing. Deploy to JBOSS...")

    if not os.path.isdir(codeGenFolder):
        panic("You need to compile first - '" + codeGenFolder + "' missing.")
    codeGenFolder = os.path.abspath(codeGenFolder)

    def getFullPath(d):
        return os.path.join(tmpVfs, d)

    deployedFolders = filter(
        lambda d:
        os.path.isdir(d) and d.startswith(tmpVfs + os.sep + "deployment"),
        (getFullPath(x) for x in os.listdir(tmpVfs)))

    appDeployedFolders = filter(
        lambda x: any(y.startswith(appName+"-war") for y in os.listdir(x)),
        deployedFolders)

    if not appDeployedFolders:
        panic("Found no deployment for '" + appName + "'. Deploy to JBOSS...")

    appDeployedFoldersSortedByMTime = sorted(
        (os.stat(x).st_mtime, x)
        for x in appDeployedFolders)

    print "\nDeployed " + appName + ":\n\n", "\n".join(
        x[1].split(os.sep)[-1]+":\t"+time.ctime(x[0])
        for x in appDeployedFoldersSortedByMTime)

    targetDeployFolder = appDeployedFoldersSortedByMTime[-1][1]
    targetWarFolder = os.path.join(
        targetDeployFolder,
        [x for x in os.listdir(targetDeployFolder)
         if x.startswith(appName+"-war")][0])

    srcFolder = os.path.join(
        codeGenFolder,
        [x for x in os.listdir(codeGenFolder)
         if x.startswith(appName+"-war")][0],
        "web")

    os.system(
        "rsync -vrlt "
        "--exclude='*.md5' --exclude='*.class' --exclude='*.eot' "
        "--exclude='*.svg' --exclude='*.ttf' --exclude='*.woff' "
        '"' + srcFolder + '/" '
        '"' + targetWarFolder + '/"')

    print "\nI just rsync-ed from:\n" + g_green + "\t" + srcFolder + \
        "\n" + g_normal + "to:\n" + g_cyan + "\t" + targetWarFolder+g_normal


if __name__ == "__main__":
    main()
