/// <reference path="../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="validators.ts" />
/// <reference path="utils.ts" />
/// <reference path="openlayers3.d.ts" />


module NpTypes {
    
    export enum ExceptionSeverity { generatorError, programmerError, userError }

    export class NpException {
        constructor(
            public severity: ExceptionSeverity,
            public message: string) {
        }
    }


    export class EntityAuditInfo {
        constructor(
            public userInsert: string,
            public dateInsert: Date,
            public userUpdate: string,
            public dateUpdate: Date) {
        }
    }
    export interface IBaseEntity {
        // Returns primary key field value as string
        getKey(): string;
        getRowVersion(): number;
        getKeyCombinedWithRowVersion(): string;
        // Serializer
        toJSON(): any;
        // was this entity added via 'add new' button?
        isNew(): boolean;
        // compare to other entity
        isEqual(other: IBaseEntity): boolean;
        // get Extended class name (RTTI-ish)
        getEntityName(): string;
        updateInstance(other: IBaseEntity): void;
        getKeyName(): string;
        refresh(ctrl: IAbstractController, afterRefreshFunc?: () => void): void;
        getAuditInfo(): EntityAuditInfo;
        markAsDirty: (x: IBaseEntity, itemId: string) => void;
    }

    export class DynamicSqlVectorInfoField {
        constructor(
            public label: string,
            public field: string,
            public width?: string
        ) { }
    }

    export class DynamicSqlVectorInfo {
        constructor(
            public code: string,
            public label: string,
            public fillColor: string,
            public borderColor: string,
            public penWidth: number,
            public orderIndex: number,
            public opacity: number,
            public fields: DynamicSqlVectorInfoField[])
        {
        }
    }

    export class entitiesFactoryRecord {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => NpTypes.IBaseEntity;
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => NpTypes.IBaseEntity;
    }

    export class BaseEntity implements IBaseEntity {
        public static activePageController: IAbstractController;
        public get $scope(): IApplicationScope {
            if (!isVoid(BaseEntity.activePageController))
                return BaseEntity.activePageController.$scope;
            return undefined;
        }
        public get $http(): ng.IHttpService {
            if (!isVoid(BaseEntity.activePageController))
                return BaseEntity.activePageController.$http;
            return undefined;
        }

        public get $timeout(): ng.ITimeoutService {
            if (!isVoid(BaseEntity.activePageController))
                return BaseEntity.activePageController.$timeout;
            return undefined;
        }

        public getKey(): string { throw "Not Impemented function" }
        public getRowVersion(): number { throw "Not Impemented function"}
        public getKeyCombinedWithRowVersion(): string {
            var rowVersion: number = !isVoid(this.getRowVersion()) ? this.getRowVersion() : 0;
            return this.getKey() + "#" + rowVersion.toString();
        }

        public toJSON(): any { throw "Not Impemented function" }
        public getEntityName(): string { throw "Not Impemented function"  }
        public updateInstance(other: IBaseEntity): void { throw "Not Impemented function" }
        public getKeyName(): string { throw "Not Impemented function" }
        public fromJSON(data: any): IBaseEntity[]{ throw "Not Impemented function" }
        public postConstruct() { }
        public isNew(): boolean {
            var key = this.getKey();
            if (isVoid(key))
                return true;
            if (this.getKey().indexOf('TEMP_ID') === 0)
                return true;
            return false;
        }
        public isEqual(other: IBaseEntity): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey() && this.getEntityName() === other.getEntityName();
        }
        public refresh(ctrl: IAbstractController, afterRefreshFunc: () => void = () => { }):void {
            if (!this.isNew()) {
                var url = "/" + ctrl.AppName + "/rest/" + this.getEntityName() + "/findBy" + this.getKeyName().U1() + "?";
                var requestData = {}
                requestData[this.getKeyName()] = this.getKey();
                ctrl.httpPost(url, requestData, rsp => {
                    var refresedEntities = this.fromJSON(rsp.data);
                    this.updateInstance(refresedEntities[0]);
                    afterRefreshFunc();
                });
            }
        }
        public markAsDirty = (x: IBaseEntity, itemId: string) => {}
        public static entitiesFactory: { [entityName: string]: entitiesFactoryRecord; } = {}

        public getAuditInfo(): EntityAuditInfo {
            return new EntityAuditInfo('', undefined, '', undefined);
        }

    }

    export function checkProducerStatus(ctrl: NpTypes.IAbstractController, producerVat: string, func: () => void, errFunc?: (response: any) => void) {
        AlertMessage.clearAlerts(ctrl.PageModel);
        if (!Utils.checkGreekVAT(producerVat)) {
            var errKey = Sprintf.sprintf("INVALID_VAT('%s')", producerVat);
            if (errFunc === undefined) {
                AlertMessage.addDanger(ctrl.PageModel, ctrl.dynamicMessage(errKey));
            } else {
                errFunc(ctrl.dynamicMessage(errKey));
            }
        } else {
            var url = "/" + ctrl.AppName + "/rest/MainService/checkProducerStatus";
            ctrl.httpPost(url, { producerVat: producerVat }, response => {
                func();
            }, errFunc);
        }
    }
    export function getProducerStatus(ctrl: NpTypes.IAbstractController, producerVat: string, func: (status:string) => void) {
        var url = "/" + ctrl.AppName + "/rest/MainService/getProducerStatus";
        ctrl.httpPost(url, { producerVat: producerVat }, response => {
            func(response);
        });
    }


    export interface NewEntityId {
        entityName : string;
        tempId: string;
        databaseId: string;
    }

    export interface SaveResponce {
        newEntitiesIds: NewEntityId[];
        warningMessages: string
        infoMessages: string;
    }

    export class EntityManager {
        entitiesCache: {
            [id: string]: NpTypes.EntityCacheEntry
        }
        public clearCache() {
            this.entitiesCache = {};
        }
        public findById(entityClassName: string, id: string): NpTypes.EntityCacheEntry {
            return this.entitiesCache[entityClassName + '#' + id];
        }


    }
    export enum EntityCachedKind { MASTER, MANY_TO_ONE };

    export class EntityCacheEntry {
        constructor(
            public entity: IBaseEntity,
            public kind: EntityCachedKind
            ) { }
    }

    export interface IUIModel {
        addNewErrorMessage(msg: string, noHover: boolean);
        clearAllErrors();
        getErrors():Array<string>
    };

    export interface IAbstractController {
        $scope: IApplicationScope;
        $http: ng.IHttpService;
        $timeout: ng.ITimeoutService;
        Plato: Services.INpDialogMaker;
        $q: ng.IQService;
        NavigationService: Services.NavigationService;
        initialize(navigationService: Services.NavigationService, qService: ng.IQService): void;
        AppName: string;
        PageModel: IPageModel;
        httpPost(url: string, data: any, successFunc: (response: any) => void, errFunc?: (response: any) => void, requestConfig?: any);
        httpGet(url: string, data: any, successFunc: (response: any) => void, errFunc?: (response: any) => void, requestConfig?: any);
        dynamicMessage(sMsg: string): string;
        getWsPathFromUrl(url: string): string;
    }

    export interface IPage extends IAbstractController {
        refresh();
        setupMainAppTemplate();
    }


    // Kept per UIModel (would have been an inner class if TypeScript supported it)
    // the error message of the validation, and whether we hover it or not on mousemoves.
    // We don't colorize/hover for selects and checkboxes, since these can't be kept
    // showing an invalid value - they can only show their model contents. In these
    // cases, the popover is shown for 3 seconds, and then this ValidationStateEntry
    // is removed.
    export class ValidationStateEntry {
        public constructor(public msg:string, public noHover:boolean=false) {}
    };

    // The per-entity state, kept separately from the component, because of grid paging.
    // (i.e. the same component (e.g. an <input>) is used to show multiple entities' fields
    // (different in page1, page2, etc) and we therefore need to keep state OUTSIDE the component.
    export class UIModel<T> implements IUIModel {
        constructor(
            value: T,
            public entity?: IBaseEntity)
        {
            this._value = value;
        }
        private _value: T
        public get value(): T {
            return this._value;
        }
        public set value(vl:T) {
            this._value = vl;
        }
        // The underlying entity's field model value - the one we revert to when validations fail.
        public oldModelValue:T;

        // The last value the user entered in the component - which may have triggered a validation failure.
        // We reset to this when page prev/next brings us into existence.
        public lastViewValue:string;

        // Array with tuples of (strValidationMessage,bNoHover) (when empty, the associated component has valid data)
        public validationMsgs: Array<ValidationStateEntry> = [];

        // State of validity. 
        // At the beginning of all $parser in all directives, it is set to true (via clearAllErrors())
        // Subsequent checks, on failure, call addNewErrorMessage, which sets it (implicitely) to false
        // and triggers the showing of a popover. 
        //
        // This is also checked in showError, to avoid showing another popover if one is already visible.
        public get isValid(): boolean {
            return this.validationMsgs.length === 0;
        }

        // To avoid adding a message we already know about, we need to be able to search for it.
        public indexOfValidationMsg(msg:string) {
            for(var i=0; i<this.validationMsgs.length; i++) {
                if (this.validationMsgs[i].msg === msg)
                    return i;
            }
            return -1;
        }

        // Bark an error... An implicit popover will be shown due to the $watch-er in the component.
        public addNewErrorMessage(msg:string, dontColorizeOrHover:boolean=false) {
            var self = this;
            if (self.indexOfValidationMsg(msg) === -1) {
                self.validationMsgs.push(new ValidationStateEntry(msg, dontColorizeOrHover));
                if (dontColorizeOrHover) {
                    // For checkboxes and selects, we need to clear the error state ;
                    // there is no 'memory' of the bad value kept, since these can't be kept
                    // showing an invalid value - they can only show their model contents. In these
                    // cases, the popover is shown for 3 seconds, and then this ValidationStateEntry
                    // is removed.
                    setTimeout(() => {
                        // In the meantime, indexes may have shifted - search again:
                        var idx = self.indexOfValidationMsg(msg);
                        self.validationMsgs.splice(idx, 1);
                    }, 3000);
                }
            }
        }

        // Silence them all (implicitely hides all popovers)
        public clearAllErrors() {
            this.validationMsgs.splice(0);
        }
        public getErrors(): Array<string> {
            return this.validationMsgs.map( x => x.msg);
        }

    }

    // Instances of the UIModel<T> base, one per type of directive content:

    export class UIStringModel extends UIModel<string> { }

    export class UIReadOnlyStringModel extends UIStringModel {
        constructor(
            public getValue: () => string,
            public entity?: IBaseEntity)
        {
            super(undefined, entity);
        }
        public get value(): string {
            return this.getValue();
        }
    }
    
    export class UINumberModel extends UIModel<number> { }

    export class UIReadOnlyNumberModel extends UINumberModel {
        constructor(
            public getValue: () => number,
            public entity?: IBaseEntity)
        {
            super(undefined, entity);
        }
        public get value(): number {
            return this.getValue();
        }
    }


    export class UIDateModel extends UIModel<Date> {}

    export class UIReadOnlyDateModel extends UIDateModel {
        constructor(
            public getValue: () => Date,
            public entity?: IBaseEntity)
        {
            super(undefined, entity);
        }
        public get value(): Date {
            return this.getValue();
        }
    }

    export class UIBoolModel extends UIModel<boolean> { }

    export class UIGeoModel extends UIModel<ol.Feature> { }

    export class UIReadOnlyBoolModel extends UIBoolModel {
        constructor(
            public getValue: () => boolean,
            public entity?: IBaseEntity)
        {
            super(undefined, entity);
        }
        public get value(): boolean {
            return this.getValue();
        }
    }


    export class UIManyToOneModel<T> extends UIModel<T> {}

    export class UIReadOnlyManyToOneModel<T extends IBaseEntity> extends UIManyToOneModel<T> {
        constructor(
            public getValue: () => T,
            public entity?: IBaseEntity)
        {
            super(undefined, entity);
        }
        public get value(): T {
            return this.getValue();
        }
    }
    
    export class UIBlobModel extends UIModel<string> {
        public progress: number = 0;
        public progressVisible: boolean = false;
        public get filename(): string {
            var f = this.fileNameGetter();
            if (f === null || f === undefined)
                return "No files selected";
            return f;
        }
        public set filename(vl: string) {
            this.fileNameSetter(vl);
        }
        public get fileid(): string {
            return this.value;
        }
        public set fileid(vl: string) {
            this.value = vl;
        }
        constructor(
            public value: string,
            public fileNameGetter: () => string,
            public fileNameSetter: (v: string) => void,
            public entity?: IBaseEntity)
        {
            super(value, entity);
        }

    }

    export interface IBreadcrumbStepModel {
    }

    // Store the info necessary per breadcrumb step (in the top bar)
    export class BreadcrumbStep {
        // George needed the associated model, too (not just the url and the label)
        constructor(
            public url: string,
            public menuId: string,
            public label: string,
            public model: IBreadcrumbStepModel) {

            this.isAuthorized = false;
        }
        public isAuthorized: boolean;
    }

    // When spawning a dialog, this is what we pass in:
    export class DialogOptions {
        // The jQueryUI element
        public jquiDialog: any;
        // The width of the dialog generated
        public width: string = '45em';
        // Whether the dialog should autoshow itself immediately
        public autoOpen: boolean = true;
        // Whether the dialog should be modal or not
        public modal: boolean = true;
        // The dialog title
        public title: string;
        // What CSS class to assign to the dialog's root <div> element
        public className: string = undefined;
        // Should the dialog show a close button?
        public showCloseButton: boolean = true;
        // This is not passed-in by the developer, it is written back from Plato,
        // and it can be used to close the dialog
        public closeDialog: ()=>void;
        // If this is set, clicking on 'X' or hitting 'ESC' will invoke this
        public onCloseDialog: () => void;
        public customParams: any;
    }
    export class SFLDialogOptions extends DialogOptions {
        public constructor() {
            super();
            this.className = Controllers.SqlLayerFilterController._ControllerClassName;
        }
        // The model that stores the filter values used in previous invocations of the dialog
        public previousModel: any;
        public makeWebRequest: (paramIndexFrom: number, paramIndexTo: number, lovMdl: any /*Controllers.AbstractLovModel */, excludedIds: Array<string>) => ng.IHttpPromise<any>;
        public onApplyNewFilter: (searchModel: Controllers.AbstractSqlLayerFilterModel) => void;
        public onClearFilters: () => void;
        //public onPageDataReceived: (rows:NpGeo.SqlLayerEntity[]) => void;
        public onRowSelect: (rowEntity: NpGeoLayers.SqlLayerEntity) => void;
        public selectedGeometry: any;
        public showSpatialSearchParams: boolean;
        public getWebRequestParamsAsString: (paramIndexFrom: number, paramIndexTo: number, lovMdl: any, excludedIds: Array<string>) => string;
        public dynInfo: DynamicSqlVectorInfo
        public ExportGeoJsonFile: (lovMdl: any) => void;
        public ExportExcel: (lovMdl: any) => void;

    }

    export class LovDialogOptions extends DialogOptions{
        public hasNewButton: boolean = false;
        // What code to run upon LOV selecting completed
        public onSelect: (selectedEntity: IBaseEntity) => void;
        // What code to run upon LOV selecting multiple entities completed
        public onMultipleSelect: (selectedEntityIds: string[]) => void;
        // Invoke the Entity Edit Form
        public openNewEntityDialog: () => void;
        public makeWebRequest: (paramIndexFrom: number, paramIndexTo: number, lovMdl: any /*Controllers.AbstractLovModel */, excludedIds:Array<string>) => ng.IHttpPromise<any>;
        public showTotalItems: boolean = true;
        public updateTotalOnDemand: boolean = false;
        public makeWebRequest_count: (lovMdl: any, excludedIds: Array<string>) => ng.IHttpPromise<any>;
        public makeWebRequestGetIds: (lovMdl: any, excludedIds: Array<string>) => ng.IHttpPromise<any>;
        public getWebRequestParamsAsString: (paramIndexFrom: number, paramIndexTo: number, lovMdl: any, excludedIds: Array<string>) => string;
        // Dictionary of columns shown in this LOV
        public shownCols: { [id: string]: boolean; } = {};
        // The LOV model that stores the filter values used in previous invocations of the LOV dialog
        public previousModel: any /* Controllers.AbstractLovModel*/;

        public bMultiSelect: boolean = false;
        // called in Lov Constructor, before updateUI()
        public onConstructLovController: (lovCtrl: Controllers.LovController) => void;
    }

    var useAsserts = false;

    ///////////////////////////////////////////////////////////
    // Base class for all components inside directives.ts

    export class NpComponent<T> {

        public formatter: (newModelValue:T) => any;

        private checkUImodelAndAssertOrRunStatement(func) {
            var self = this;
            var condition = self.uiModel !== undefined && self.uiModel !== null;
            if (useAsserts) {
                // In debug mode
                if (!condition) {
                    console.error("ASSERTION FAILED! uiModel is void...");
                } else
                    func();
            } else {
                // In production mode - silence the error that would happen otherwise...
                if (condition) 
                    func();
            }
        }

        private checkUImodelAndAssertOrReturnExpression<U>(func:()=>U, defaultValue:U):U {
            var self = this;
            var condition = self.uiModel !== undefined && self.uiModel !== null;
            if (useAsserts) {
                // In debug mode
                if (!condition) {
                    console.error("ASSERTION FAILED! uiModel is void...");
                    return defaultValue;
                } else
                    return func();
            } else {
                // In production mode - silence the error that would happen otherwise...
                if (condition) 
                    return func();
                else
                    return defaultValue;
            }
        }

        constructor(public element:JQuery, public attrs:any, public $timeout, public scope, public bDebug:boolean) {

            // Is this component supposed to be filled-in, as in, mandatory?
            // This used to be a flag...
            //
            //if (attrs.npRequired !== undefined)
            //    this.required = scope.$eval(attrs.npRequired);
            //
            // ...but we now do it via $eval-ed property (see above),
            // so we don't need to store it.

            // But we do store any npValidate expression.
            if (attrs.npValidate !== undefined)
                this.validationCallback = attrs.npValidate;

            if (attrs.npUserDefinedFormatter !== undefined) {
                this.userDefinedFormatter = (mv: T, e: IBaseEntity) => {
                    return scope[attrs.npUserDefinedFormatter](mv, e);
                }
            }

            if (attrs.npUserDefinedParser !== undefined) {
                this.userDefinedParser = (vv: string, e: IBaseEntity) => {
                    return scope[attrs.npUserDefinedParser](vv, e);
                }
            }

            // The uiModel may be unset - but the moment it is set for the first time,
            // we will $watch its validationMsgs.
            var self = this;

            // ...and setup the real one that will watch the uiModel's validationMsgs!
            scope.$watch(
                () => {
                    var uim = self.uiModel;
                    return (uim !== undefined && uim !== null)?uim.validationMsgs:[];
                },
                (newVal2:Array<string>, oldVal2:Array<string>) => {
                    if (self.bDebug)
                        console.log("$watch-er on validationMsgs just triggered...");
                    self.markAsGood();
                    if (newVal2.length != 0) {
                        self.showError(
                            self.uiModel.validationMsgs[0].msg,
                            self.uiModel.validationMsgs[0].noHover);
                    }
                },
                true);

            scope.$watch(
                () => {
                    var entity = self.entity;
                    return (!isVoid(entity))?entity.getKey():'';
                },
                (newEntityKey:string, oldEntityKey:string) => {
                    var entity = self.entity;
                    if (isVoid(entity))         { return; }
                    if (isVoid(self.formatter)) { return; }

                    var value = scope.$eval(attrs.ngModel);
                    value = self.formatter(value);

                    self.element.val(value);
                });
        }

        public safelyGetOldValue(defaultValueInCaseOfFailure:T):T {
            var self = this;
            return self.checkUImodelAndAssertOrReturnExpression( () => {
                return self.uiModel.oldModelValue;
            }, defaultValueInCaseOfFailure);
        }

        public safelySetOldValue(newValue:T) {
            var self = this;
            return self.checkUImodelAndAssertOrRunStatement( () => {
                self.uiModel.oldModelValue = newValue;
            });
        }

        public safelyGetLastViewValue() {
            var self = this;
            return self.checkUImodelAndAssertOrReturnExpression( () => {
                return self.uiModel.lastViewValue;
            }, '');
        }

        public safelySetLastViewValue(newViewValue:string) {
            var self = this;
            return self.checkUImodelAndAssertOrRunStatement(() => {
                self.uiModel.lastViewValue = newViewValue;
            });
        }

        public clearAllErrors() {
            var self = this;
            return self.checkUImodelAndAssertOrRunStatement( () => {
                self.uiModel.clearAllErrors();
            });
        }

        public addNewErrorMessage(msg:string, dontColorizeOrHover:boolean=false) {
            var self = this;
            return self.checkUImodelAndAssertOrRunStatement( () => {
                self.uiModel.addNewErrorMessage(msg, dontColorizeOrHover);
            });
        }

        // To work with 'row.entity' based directives that live inside ng-grids,
        // we need to evaluate the NeuroCode-provided np-ui-model variable.
        get uiModel():UIModel<T> {
            return <UIModel<T>>(this.scope.$eval(this.attrs.npUiModel));
        }

        // Similarly, entities are accessible via the corresponding UIModels,
        // but to avoid exceptions, have this check and return undefined 
        // when the UIModel is stillborn.
        get entity():NpTypes.IBaseEntity {
            var uim = this.uiModel;
            if (isVoid(uim))
                return undefined;
            else
                return uim.entity;
        }

        // The timeout promise that destroys the error popovers after X milliseconds
        public promise:ng.IPromise<any> = undefined;

        // Is this component supposed to be filled-in, as in, mandatory?
        // This used to be a flag...
        //public required:boolean = false;
        // ...but it turns out we need to evaluate this at runtime, 
        // so it was turned to a property that $evals the input string.
        // And of course, that meant we have to store the $scope inside ourselves!
        // (see the constructor)
        public get required():boolean {
            return this.scope.$eval(this.attrs.npRequired);
        }

        // Whenever $parse is called, it will also check whether there is a
        // user-provided validation. If there is, it will $eval it,
        // and check its return value (ValidationResult)
        public validationCallback: string = undefined;

        public userDefinedFormatter: (mv: T, ent: IBaseEntity) => string;

        public userDefinedParser: (vv: string, ent: IBaseEntity) => T;

        // Some validation failed - show a bootstrap popover with 'msg':
        private showPopover(msg:string):void {
            var self = this;
            // Is there an already visible popover?
            if (self.promise === undefined) {
                // No. Is the the element still visible?
                if (self.element.is(":visible")) {
                    // Yes, so go ahead and show this new popover
                    (<any>self.element).popover({
                        animation:true, html:false, placement:'bottom', trigger:'manual', content:msg
                    }).popover('show');
                    // Schedule a popover destroying after 3000ms, and store it in .promise
                    self.promise = self.$timeout(function() {
                        (<any>self.element).popover('destroy');
                        // Unfortunately, the element we 'hang' the popover on, may have been destroyed
                        // (e.g. a dialog closing will kill its DOM elements)
                        // When this happens, the .popover('destroy') above will NOT work
                        // We therefore 'sweep' all .popover, just in case
                        $('.popover').remove();
                        self.promise = undefined;
                    }, 3000);
                }
            }
        }

        // Close the popover before the timer expires...
        private killPopover():void {
            var self = this;
            // Is there a currently visible popover? If so, there's an active promise to destroy it...
            if (self.promise !== undefined) {
                // Cancel the timer, destroy the popover:
                self.$timeout.cancel(self.promise);
                (<any>self.element).popover('destroy');
                // Unfortunately, the element we 'hang' the popover on, may have been destroyed
                // (e.g. a dialog closing will kill its DOM elements)
                // When this happens, the .popover('destroy') above will NOT work
                // We therefore 'sweep' all .popover, just in case
                $('.popover').remove();
                self.promise = undefined;
            }
        }

        private markAsGood():void {
            var self = this;
            // Close the popover if the timer hasn't expired yet.
            self.killPopover();
            // Stop hover in/out triggering the showing of popovers
            $(self.element).off( "mouseenter mouseleave" );
            // Stop highlighting the element with error colors
            $(self.element).removeClass('alert-error');
        }

        // Use a Bootstrap popover to notify the user of erroneous data
        private showError(msg:string, dontColorizeOrHover:boolean=false):void {
            var self = this;
            // If this message doesn't already exist in the uimodel state, add it.
            if (self.uiModel !== undefined)
                self.uiModel.addNewErrorMessage(msg, dontColorizeOrHover);
            // Destroy any old error popover
            self.killPopover();
            // Highlight the control with Bootstrap's error color
            if (!dontColorizeOrHover)
                $(self.element).addClass('alert-error');
            // Show the error message via a Bootstrap popover
            self.showPopover(msg);
            // The first time showError is called, when isValid is not set to false yet,
            // setup the hover/unhover to show/hide the popover
            if ((!self.uiModel.isValid) && (!dontColorizeOrHover)) {
                (<any>self.element).hover(
                    // On hover in...
                    function() {
                        // Only show the popover if it is not already there
                        if (self.promise === undefined) {
                            self.showPopover(msg);
                        }
                    },
                    // On hover out...
                    function() {
                        self.killPopover();
                    }
                );
            }
        }

        public getDynamicMessage(msgKey: string): string {
            var ret = (<NpTypes.IApplicationScope>this.scope.$parent).globals.getDynamicMessage(msgKey);
            return !isVoid(ret) ? ret : msgKey;
        }
    }

    export class NpBoolean extends NpComponent<boolean> {}

    export class NpText extends NpComponent<string> {}

    export var localeDecimalSeparator: string = '.';
    export var defaultDecimals: number = 0;
    export var defaultDecimalSeparator: string = ',';
    export var defaultThousandSeparator: string = '.';

    export class NpNumber extends NpComponent<number> {
        public compiledRegExValidate:RegExp;
        public compiledRegExClean:RegExp;
        public decimals: number = defaultDecimals;
        public decSep: string = defaultDecimalSeparator;
        public thSep: string = defaultThousandSeparator;
        public min:number = Number.NEGATIVE_INFINITY;
        public max:number = Number.POSITIVE_INFINITY;

        // This is how the decimal point is represented in a native floating point number's string representation:
        public localeDecSep = localeDecimalSeparator;

        constructor(public element:JQuery, public attrs:any, public $timeout, public scope, bDebug: boolean) {
            super(element, attrs, $timeout, scope, bDebug);
            // Parse input params: min, max, number of decimals, decimal separator and thousands separator:
            if (attrs.npMin !== undefined)
                this.min = parseFloat(attrs.npMin);
            if (attrs.npMax !== undefined)
                this.max = parseFloat(attrs.npMax);
            if (attrs.npDecimals !== undefined)
                this.decimals = parseInt(attrs.npDecimals);
            if (attrs.npDecsep !== undefined)
                this.decSep = attrs.npDecsep;
            if (attrs.npThsep !== undefined)
                this.thSep = attrs.npThsep;
            // cache the validation and cleaning regexps inside ourselves
            // (i.e. don't compile them all the time)
            //
            // Validation regexp:
            var regex = "^(-)?[0-9]*(" + this.decSep + "([0-9]{0," + this.decimals + "}))?$";
            this.compiledRegExValidate = new RegExp(regex);
            // Clean regexp (used to remove the thousands separator char:
            this.compiledRegExClean = new RegExp(this.thSep.replace(/\./g, "\\."), 'g');
        }

        public viewTextToNumber(t:string):number {
            if (!this.required && t == '')
                return null;
            // Use the clean regexp to remove the thousands separators,
            // and then replace the decimal separator with the locale-specific one.
            return parseFloat(t.replace(this.compiledRegExClean, '').replace(this.decSep, this.localeDecSep));
        }
    }

    export class NpDate extends NpComponent<Date> {
        public formatNumbersOnly:string;
        public compiledRegExValidate:RegExp;
        public format: string = 'dd/MM/yyyy';
        public minDate: Date = new Date(-3000000000000);
        public maxDate: Date = new Date( 4000000000000);
        public sanitizedDatePartLengths: Array<number> = [];
        public datePickerYearRange: string = 'c-10:c+10';

        constructor(public element:JQuery, public attrs:any, public $timeout, public scope, bDebug:boolean) {
            super(element, attrs, $timeout, scope, bDebug);
            // Parse input params: format...
            if (attrs.npFormat !== undefined)
                this.format = attrs.npFormat;
            // one without the dashes and the slashes (numbers only):
            this.formatNumbersOnly = this.format.replace(/\//g, '').replace(/-/g, '');

            if (attrs.npDatePickerYearRange !== undefined)
                this.datePickerYearRange = attrs.npDatePickerYearRange;

            var viewTextToDate = (t:string):Date => {
                return (<any>Date).parseString(t, this.format);
            }

            // min and max allowed dates:
            if (attrs.npMinDate !== undefined) {
                this.minDate = viewTextToDate(scope.$eval(attrs.npMinDate));
                if (this.minDate === null)
                    console.error("Invalid specification for np-min-date (" + attrs.npMinDate + ")" +
                                  " with format " + attrs.npFormat);
            }

            if (attrs.npMaxDate !== undefined) {
                this.maxDate = viewTextToDate(scope.$eval(attrs.npMaxDate));
                if (this.maxDate === null)
                    console.error("Invalid specification for np-min-date (" + attrs.npMaxDate + ")" +
                                  " with format " + attrs.npFormat);
            }

            // cache the validation regexp inside ourselves (don't compile it all the time)
            var regex = this.format.slice(0);
            var regex = regex.replace(/d/ig, '\\d');
            var regex = regex.replace(/m/ig, '\\d');
            var regex = regex.replace(/y/ig, '\\d');
            var regex = regex.replace(/\//g, '/?');
            var regex = regex.replace(/-/g, '/?');
            this.compiledRegExValidate = new RegExp('^'+regex+'$');
            this.sanitizedDatePartLengths = _.map(
                this.format.replace(/[\/-]/g, '@').split('@'),
                (part:string) => { return part.length; });
        }
    }

    export class NpRichtext extends NpComponent<string> {}

    export class NpSelect extends NpComponent<any> {
        public choices: string = 'x[1] as x[0] for x in [["yes", 1], ["no", 0]]';
    }

    export class NpLookup extends NpComponent<string> {
        public npLookup:string;
        public npSource:string;
        public npContext:string;

        constructor(public element:JQuery, public attrs:any, public $timeout, public scope, bDebug:boolean) {
            super(element, attrs, $timeout, scope, bDebug);
            if (attrs.npLookup === undefined) {
                console.error("npLookup: you forgot to pass data-np-lookup");
                return;
            }
            this.npLookup = attrs.npLookup;

            if (attrs.npSource === undefined) {
                console.error("npLookup: you forgot to pass data-np-source");
                return;
            }
            this.npSource = attrs.npSource;
            this.npContext = attrs.npContext;
        }
    }

    export class NpBlob {
        constructor(
            public blobChangedCallback: (entity:NpTypes.IBaseEntity)=>void,
            public getURL: (entity:NpTypes.IBaseEntity) => string,
            public isDisabled: (entity:NpTypes.IBaseEntity)=>boolean,
            public isInvisible: (entity:NpTypes.IBaseEntity) => boolean,
            public postURL: string,
            public isPlusEnabled: boolean,
            public isMinusEnabled: boolean,
            public extensions: string = '',
            public getMaximumSizeInKB: (entity: NpTypes.IBaseEntity) => number = (entity) => 1024)
        {
        }
    }

    export class NpFileUpload {
        constructor(
            public formDataAppendCallback: (fd: FormData, entity?: NpTypes.IBaseEntity) => void,
            public fileUpLoadedCallback: (fileName: string, uploadResponseText: any, entity?: NpTypes.IBaseEntity) => void,
            public getLabel: (entity?: NpTypes.IBaseEntity) => string,
            public isDisabled: (entity?: NpTypes.IBaseEntity) => boolean,
            public isInvisible: (entity?: NpTypes.IBaseEntity) => boolean,
            public postURL: string,
            public helpButtonAction: () => void,
            public extensions: string = '',
            public maximumSizeInKB: number = 1024) {
        }
    }

    export class ValidationResult {
        constructor(public isValid:boolean, public errorMessage:string) {}
    }

    // For the Bootstrap alert messages
    export enum AlertType { SUCCESS, INFO, WARNING, DANGER }
    export class AlertData {
        constructor(public kind: AlertType, public msg: string) {}
    }

    export interface IPageModel {
        alertData: Array<AlertData>;
        alerts: Array<AlertMessage>;
    }

    export class AlertMessage {
        constructor(public type:string, public msg:string)
        {
            if (type !== "success" && type !== "info" && type !== "warning" && type !== "danger")
                console.error("AlertMessage: " + type + " is an unsupported option");
        }

        public static addSuccess(model:IPageModel, msg:string) {
            model.alertData.push(new AlertData(AlertType.SUCCESS, msg));
            AlertMessage.updateAlerts(model);
        }
        public static addInfo   (model:IPageModel, msg:string) {
            model.alertData.push(new AlertData(AlertType.INFO,    msg));
            AlertMessage.updateAlerts(model);
        }
        public static addWarning(model:IPageModel, msg:string) {
            model.alertData.push(new AlertData(AlertType.WARNING, msg));
            AlertMessage.updateAlerts(model);
        }
        public static addDanger (model:IPageModel, msg:string) {
            model.alertData.push(new AlertData(AlertType.DANGER,  msg));
            AlertMessage.updateAlerts(model);
        }

        public static updateAlerts(model:IPageModel) {
            var success:string = _.map(
                _.filter( model.alertData, function(d:AlertData) { return d.kind == AlertType.SUCCESS; }),
                function(d:AlertData) { return d.msg; }).join("</div><div>");
            var info:string = _.map(
                _.filter( model.alertData, function(d:AlertData) { return d.kind == AlertType.INFO; }),
                function(d:AlertData) { return d.msg; }).join("</div><div>");
            var warning:string = _.map(
                _.filter( model.alertData, function(d:AlertData) { return d.kind == AlertType.WARNING; }),
                function(d:AlertData) { return d.msg; }).join("</div><div>");
            var danger:string = _.map(
                _.filter( model.alertData, function(d:AlertData) { return d.kind == AlertType.DANGER; }),
                function(d:AlertData) { return d.msg; }).join("</div><div>");

            var newAlerts:Array<AlertMessage> = [];
            if (success != "") newAlerts.push(new AlertMessage('success', success));
            if (info    != "") newAlerts.push(new AlertMessage('info'   ,    info));
            if (warning != "") newAlerts.push(new AlertMessage('warning', warning));
            if (danger  != "") newAlerts.push(new AlertMessage('danger' ,  danger));
            model.alerts = newAlerts;
        }

        public static clearAlerts(model:IPageModel) {
            model.alertData = [];
            AlertMessage.updateAlerts(model);
        }
    }

    export interface WSResponeInfo {
        path: string;
        startTs: number;
        endTs: number;
        success: boolean;
        errorCode: string;
    }

    export class MainAppTemplate {
        npHeaderPartialUrl: string;
        showNpHeader: boolean = false;

        npBreadcrumbUrl: string;
        showNpBreadcrumb: boolean = false;

        showNpMenu: boolean = false;
        showNpFooter: boolean = false;
        showVersion: boolean = false;

        showAll = (bShow: boolean) => {
            this.showNpHeader = bShow;
            this.showNpBreadcrumb = bShow;
            this.showNpMenu = bShow;
            this.showNpFooter = bShow;
            this.showVersion = bShow;
        };
    }

    export class RequestAuthorizationOptions {
        getFormTitle: (url:string) => string;
        getPasswordLabel: (url:string) => string;
        getUserNameLabel: (url: string) => string;
        getDisplayedErrMsg: (url: string, errMsg: string) => string;
    }

    export class RTLGlobals {
        version: string;
        languages: string[]; // first is the default
        bLoggingOut: boolean;
        dialogOptionsList: DialogOptions[] = [];
        activeMenuId: string;
        wsResponseInfoList: WSResponeInfo[] = [];
        // For future use: dynamic loading of controllers
        //
        // controllerProvider: ng.IControllerProvider;

        mainAppTemplate: MainAppTemplate = new MainAppTemplate();

        // called by controllerMain
        public initApp($scope: NpTypes.IApplicationScope, NavigationService: Services.NavigationService, loginService: Services.ILoginService, $http: ng.IHttpService, $timeout: ng.ITimeoutService, onInitFunc: () => void): void {
            if (!isVoid(onInitFunc))
                onInitFunc();
        }

        public get sessionCookieKey(): string {
            return this.appName.slice(0, 1).toLowerCase() + this.appName.slice(1) + '-session-id';
        }
        public get subsCodeCookieKey(): string {
            return this.appName.slice(0, 1).toLowerCase() + this.appName.slice(1) + '-subs-code';
        }

        public get ssoCookieKey(): string { return 'sso-value'; }

        private _onReloadAppLocation: string = '';
        public get onReloadAppLocation(): string { return this._onReloadAppLocation }
        public set onReloadAppLocation(vl: string) { this._onReloadAppLocation = vl; }

        public requestAuthorizationOptions: RequestAuthorizationOptions = new RequestAuthorizationOptions();

        public hasPrivilege(privile: string): boolean {
            return this.privileges[privile] === true;
        }

        public get isCurrentTransactionDirty() {
            return this.transactionLevels[this.transactionLevels.length - 1];
        }
        public set isCurrentTransactionDirty(v:boolean) {
            this.transactionLevels[this.transactionLevels.length - 1] = v;
        }

        public createNewTransactionLevel() {
            this.transactionLevels.push(false);
        }
        public dropLastTransactionLevel() {
            this.transactionLevels.pop();
        }

        public onMenuClick(menuId: string) {
            if (menuId !== undefined) {
                //console.log("on menu click", menuId);
            }
        }

        transactionLevels: Array<boolean> = [false];

        private _appName: UIStringModel = new UIStringModel(undefined);
        public get appName(): string { return this._appName.value }
        public set appName(vl: string) { this._appName.value = vl;}

        private _appTitle: UIStringModel = new UIStringModel(undefined);
        public get appTitle(): string { return this._appTitle.value }
        public set appTitle(vl: string) { this._appTitle.value = vl; }

        private _sessionClientIp: UIStringModel = new UIStringModel(undefined);
        public get sessionClientIp(): string { return this._sessionClientIp.value }
        public set sessionClientIp(vl: string) { this._sessionClientIp.value = vl; }

        private _appWsResponeInfoEnabled: UIBoolModel = new UIBoolModel(undefined);
        public get appWsResponeInfoEnabled(): boolean { return this._appWsResponeInfoEnabled.value }
        public set appWsResponeInfoEnabled(vl: boolean) {
            this._appWsResponeInfoEnabled.value = vl;
            if (!isVoid(vl) && !vl) this.wsResponseInfoList.splice(0);
        }

        private _globalUserEmail: UIStringModel = new UIStringModel(undefined);
        public get globalUserEmail(): string { return this._globalUserEmail.value }
        public set globalUserEmail(vl: string) { this._globalUserEmail.value = vl; }

        private _globalUserActiveEmail: UIStringModel = new UIStringModel(undefined);
        public get globalUserActiveEmail(): string { return this._globalUserActiveEmail.value }
        public set globalUserActiveEmail(vl: string) { this._globalUserActiveEmail.value = vl; }

        private _globalUserLoginName: UIStringModel = new UIStringModel(undefined);
        public get globalUserLoginName(): string { return this._globalUserLoginName.value }
        public set globalUserLoginName(vl: string) { this._globalUserLoginName.value = vl; }

        private _globalUserVat: UIStringModel = new UIStringModel(undefined);
        public get globalUserVat(): string { return this._globalUserVat.value }
        public set globalUserVat(vl: string) { this._globalUserVat.value = vl; }

        private _globalUserId: UINumberModel = new UINumberModel(undefined);
        public get globalUserId(): number { return this._globalUserId.value }
        public set globalUserId(vl: number) { this._globalUserId.value = vl; }

        private _globalSubsId: UINumberModel = new UINumberModel(undefined);
        public get globalSubsId(): number { return this._globalSubsId.value }
        public set globalSubsId(vl: number) { this._globalSubsId.value = vl; }

        private _globalSubsDescription: UIStringModel = new UIStringModel(undefined);
        public get globalSubsDescription(): string { return this._globalSubsDescription.value }
        public set globalSubsDescription(vl: string) { this._globalSubsDescription.value = vl; }

        private _globalSubsCode: UIStringModel = new UIStringModel(undefined);
        public get globalSubsCode(): string { return this._globalSubsCode.value }
        public set globalSubsCode(vl: string) { this._globalSubsCode.value = vl; }

        private _globalSubsSecClasses: NpTypes.UIModel<number[]> = new NpTypes.UIModel<number[]>(undefined);
        public get globalSubsSecClasses(): number[] { return this._globalSubsSecClasses.value }
        public set globalSubsSecClasses(vl: number[]) { this._globalSubsSecClasses.value = vl; }

        private _globalLang: UIStringModel = new UIStringModel(undefined);
        public get globalLang(): string { return this._globalLang.value }
        public set globalLang(vl: string) { this._globalLang.value = vl; }

        nInFlightRequests: number;
        bDigestHackEnabled: boolean = true;
        nPKSequence: number;
        timeoutInMS: number = 60000;
        privileges: { [id: string]: boolean; } = {};

        /*private _initialDialogHasBeenDisplay: boolean = false;
        public get initialDialogHasBeenDisplay(): boolean { return this._initialDialogHasBeenDisplay}
        public set initialDialogHasBeenDisplay(vl: boolean) {
            console.log("initialDialogHasBeenDisplay set, old:" + this.initialDialogHasBeenDisplay + ", new:" + vl);
            this._initialDialogHasBeenDisplay = vl;
        }*/


        public addPrivileges(privs: Array<string>) {
            var self = this;
            if (privs !== undefined) {
                _.forEach(privs, priv => { self.privileges[priv] = true; });
            }
        }

        public getNextSequenceValue(): string {
            return 'TEMP_ID_' + (this.nPKSequence++).toString();
        }
        constructor() {
            this.version = 'TIMESTAMP';
            this.nInFlightRequests = 0;
            this.nPKSequence = 0;
            this.bLoggingOut = false;
        }
        public initialize($scope: IApplicationScope, $timeout: ng.ITimeoutService, $http: ng.IHttpService, Plato: Services.INpDialogMaker) {
        }
        public initializeServerSideGlobals(response: any) {
        }
        public showInitialDialog($scope: IApplicationScope, $timeout: ng.ITimeoutService, $http: ng.IHttpService, Plato: Services.INpDialogMaker) {
        }

        public addDialogOption(dlgOption: DialogOptions) {
            this.dialogOptionsList.push(dlgOption);
        }
        public findAndRemoveDialogOptionByClassName(clsName: string):DialogOptions {
            return this.dialogOptionsList.removeFirstElement(x => x.className === clsName);
        }
        public addWsResponseInfo(path: string, startTs: number, endTs: number, success: boolean, status?: number) {
            if (isVoid(this.appWsResponeInfoEnabled) || this.appWsResponeInfoEnabled)
                this.wsResponseInfoList.push({
                    path: path,
                    startTs: startTs,
                    endTs: endTs,
                    success: success,
                    errorCode: isVoid(status) ? "" : status.toString()
                });
        }
        public getAndResetWsResponseInfoList() {
            if (isVoid(this.appWsResponeInfoEnabled) || this.appWsResponeInfoEnabled) {
                return this.wsResponseInfoList.splice(0);
            } else {
                return [];
            }
        }

        public onGlobalLangChange(ctrl: IPage) {
            
        }

        public isGlobalLangControlDisabled():boolean {
            return false;
        }

        public isLoginSubscriberInvisible(): boolean {
            return false;
        }

        public getDynamicMessage(msgKey: string): string {
            console.log("getDynamicMessage function is not defined in globals");
            return msgKey;
        }

        public getLoggedInUserDescription() {
            return !isVoid(this.globalUserLoginName) ? this.globalUserLoginName : this.globalUserEmail;
        }
    };

    export interface IApplicationScope extends ng.IScope {
        globals: RTLGlobals;
        //NavigationService properties and methods
        modelNavigation: Services.ModelNavigation;
        navigateForward(url: string, label: string, pageToGoModel: NpTypes.IBreadcrumbStepModel): void;
        navigateBackward(): void;
        navigateToRoute(url: string, askForGlobals?: boolean): void;
        getCurrentPageModel(): NpTypes.IBreadcrumbStepModel;
        setCurrentPageModel(mdl: NpTypes.IBreadcrumbStepModel);
        getPageModelByURL(url: string): NpTypes.IBreadcrumbStepModel;
        setPageModelByURL(url: string, mdl: NpTypes.IBreadcrumbStepModel);
        hasURL(url: string): boolean;
        showF1(): void;
        getALString(key: string, asHtml?: boolean): string;
        setActiveLangByGlobalLang(): void;
    };

    export function getReportExtensionByFormat(formatValue: number): string {
        if (isVoid(formatValue))
            return null;
        if (formatValue == 0) return "xls";
        if (formatValue == 1) return "pdf";
        if (formatValue == 2) return "docx";
        if (formatValue == 3) return "txt";
    }
}


interface BannerMessage {
    message: string;
    type: number;     // using an enum didn't work :-( 
                      // enum BannerMessageType { INFO:3, WARNING:2, DANGER:1 };
    location: number; // 1: LoginWindow, 2:MainApp, 3:Both
};

// From: http://stackoverflow.com/questions/13897659/extending-functionality-in-typescript
// (inject endswith in all strings)
//
// We add it here, because npTypes is the very first .js being included (right after version.ts)
//
interface String {
    startsWith(suffix: string): boolean;
    endsWith(suffix: string): boolean;
    U1():string
}

String.prototype.startsWith = function (str: string) {
    return this.indexOf(str) == 0;
};

String.prototype.endsWith = function (suffix: string) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

String.prototype.U1 = function ():string {
    var self: string = this;
    return self.substr(0,1).toUpperCase() + self.substr(1);
}

interface Array<T> {
    first(predicate: (x: T) => boolean): T;
    firstOrNull(predicate: (x: T) => boolean): T;
    removeFirstElement(predicate: (x: T) => boolean): T;
    sumBy(f: (x: T) => number): number;
    maxBy(f: (x: T) => number, initialValue?: number): number;

    groupBy<U>(projection: (a: T) => U, toString?:(u:U)=>string): Tuple2<U, T[]>[] ;

}


Array.prototype.groupBy = function groupBy<T, U>(projection: (a: T) => U, toString?: (u: U) => string): Tuple2<U, T[]>[]{
    var self: Array<T> = this;
    var tmpMap: { [id: string]: Tuple2<U, T[]>; } = { }
    var ret: Tuple2<U, T[]>[] = [];
    for (var i = 0; i < self.length; i++) {
        var curElem = self[i];
        var key = projection(curElem);
        var keyStr = toString === undefined ? key.toString() : toString(key);
        if (tmpMap[keyStr] === undefined) {
            tmpMap[keyStr] = new Tuple2(key, [curElem]);
        } else {
            tmpMap[keyStr].b.push(curElem);
        }
    }
    var keys = Object.keys(tmpMap);
    for (var j = 0; j < keys.length; j++) {
        ret.push(tmpMap[keys[j]]);
    }

    return ret;
}

Array.prototype.first = function (predicate: (x: any) => boolean) {
    var self: Array<any> = this;
    var ret = self.filter(predicate);
    if (ret.length === 0) {
        throw "No element found (Array.prototype.first)";
    }
    return ret[0];

}

Array.prototype.firstOrNull = function (predicate: (x: any) => boolean) {
    var self: Array<any> = this;
    var ret = self.filter(predicate);
    return ret.length === 0 ? null : ret[0];
}


Array.prototype.removeFirstElement = function (predicate: (x: any) => boolean): any {
    var indexToRemove = -1;
    for (var i = 0; i < this.length; i++) {
        var curElement = this[i];
        if (predicate(curElement)) {
            indexToRemove = i;
            break;
        }
    }
    if (indexToRemove >= 0) {
        var itemToRemove = this[indexToRemove];
        this.splice(indexToRemove, 1);
        return itemToRemove;
    }
    return null;
}

Array.prototype.sumBy = function (f: (x: any) => number): number {
    var ret = 0;
    for (var i = 0; i < this.length; i++) {
        var curElement = this[i];
        ret = ret + f(curElement);
    }
    return ret;
}

Array.prototype.maxBy = function (f: (x: any) => number, initialValue: number= 0): number {
    var ret = initialValue;
    for (var i = 0; i < this.length; i++) {
        var curElement = this[i];
        var curValue = f(curElement);
        if (isVoid(curValue)) {
            continue;
        }
        if (curValue > ret) {
            ret = curValue;
        }
    }
    return ret;
}

interface Date {
    withoutTime(): Date;
}

Date.prototype.withoutTime = function (): Date {
    var self: Date = this;
    self.setHours(0, 0, 0, 0);
    return self;
}
interface Number {
    toStringFormatted(decSep?: string, thSep?: string, decimals?: number): string;
}

// moved here from NpNumber directive (function numberToViewText)
Number.prototype.toStringFormatted = function (decSep?: string, thSep?: string, decimals?: number): string {
    var n: number = this;
    decimals = isVoid(decimals) ? NpTypes.defaultDecimals : decimals;
    decSep = isVoid(decSep) ? NpTypes.defaultDecimalSeparator : decSep;
    thSep = isVoid(thSep) ? NpTypes.defaultThousandSeparator : thSep;

    if (isVoid(n)) return ''; // the model may be undefined/null by the user

    var parts = n.toString().split(NpTypes.localeDecimalSeparator);
    // for the integer part, add thousands separators:
    // From http://stackoverflow.com/questions/17294959/how-does-b-d3-d-g-work-for-adding-comma-on-numbers
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thSep);
    // for the fractional part, if it doesn't exist, add it:
    if (!isVoid(decimals) && decimals !== 0) {
        if (parts.length === 1)
            parts.push('')
        // ...and then make sure it is lengthy up to the required decimals
        while (parts[1].length < decimals)
            parts[1] += "0";
    }

    return parts.join(decSep);
}
