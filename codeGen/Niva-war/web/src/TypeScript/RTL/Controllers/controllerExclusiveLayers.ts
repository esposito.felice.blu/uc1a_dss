/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />

module Controllers {

    export class LayerExclusionState {
        constructor(
            public name:string,
            public enabled:boolean,
            public sqlLayer: NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>)
        {}
    }

    export class LayerExclusionModel {
        layers: Array<LayerExclusionState> = [];
    }

    export interface ILayerExclusionScope extends NpTypes.IApplicationScope {
        model: LayerExclusionModel;
        nevermind: ()=>void;
        change(foo:any);
    }

    export class controllerExclusiveLayers {

        constructor(public $scope: ILayerExclusionScope, $timeout:ng.ITimeoutService) {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("ChooseExclusiveLayersDialog");

            var self = this;
            var npMapThis:NpGeo.NpMap = (<any>dialogOptions).npMapThis;
            $scope.model = new LayerExclusionModel();
            $scope.model.layers = 
                npMapThis.layers.
                filter(l => l instanceof NpGeoLayers.SqlVectorLayer).
                filter((l: NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>) => {
                    if (l.exclusive === true || l.exclusive === undefined) return true;
                   // return  l.exclusive === undefined;
                }).
                map((l: NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>) => {
                    var isUsedForExclusions = 
                        NpGeoLayers.getSetting(l.layerId + l.label, "isUsedForExclusions", true);
                    l.setUsedForExclusions(isUsedForExclusions);
                    return new LayerExclusionState(
                        l.label, isUsedForExclusions, l);
                });

            $scope.nevermind = () => {
                dialogOptions.jquiDialog.dialog("close");
            }

            $scope.change = (l:LayerExclusionState) => {
                l.sqlLayer.setUsedForExclusions(l.enabled); // Update sessionStorage info
            }
        }
    }
}
