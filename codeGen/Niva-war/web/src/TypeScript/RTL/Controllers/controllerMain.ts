﻿/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../services.ts" />
/// <reference path="../npTypes.ts" />

module Controllers {

    export interface IMainScope extends NpTypes.IApplicationScope {
        getNpHeaderTemplate(): string;
        getNpBreadcrumbTemplate(): string;
        showNpMenu(): boolean;
        showNpFooter(): boolean;
        showVersion(): boolean;
    }


    export class controllerMain {

        constructor(
            public $scope: IMainScope,
            private $cookies: ng.cookies.ICookiesService,
            private $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public $window: ng.IWindowService,
            private LoginService: Services.ILoginService,
            public NavigationService: Services.NavigationService)
        {

            //new
            $scope.getNpHeaderTemplate = () => {
                return $scope.globals.mainAppTemplate.showNpHeader ? $scope.globals.mainAppTemplate.npHeaderPartialUrl : undefined;
            }
            $scope.getNpBreadcrumbTemplate = () => {
                return LoginService.isLoggedIn() && $scope.globals.mainAppTemplate.showNpBreadcrumb ? $scope.globals.mainAppTemplate.npBreadcrumbUrl : undefined;
            }
            $scope.showNpMenu = () => {
                return LoginService.isLoggedIn() && $scope.globals.mainAppTemplate.showNpMenu;
            }
            $scope.showNpFooter = () => {
                return $scope.globals.mainAppTemplate.showNpFooter;
            }
            $scope.showVersion = () => {
                return $scope.globals.mainAppTemplate.showVersion;
            }

            // set globalLang and add watchers
            var globalLangCookiekey:string = 'allApps' + '-globalLang';
            if ($scope.globals.languages.length > 1) {
                if (supports_html5_storage() && isVoid($cookies[globalLangCookiekey])) {
                    $cookies[globalLangCookiekey] = localStorage[globalLangCookiekey];
                }

                if (isVoid($cookies[globalLangCookiekey])) {
                    $cookies[globalLangCookiekey] = $scope.globals.languages[0];
                } else {
                    // check if lang exists in current project
                    var bLangExists: boolean = !isVoid($scope.globals.languages.firstOrNull((l: string) => l === $cookies[globalLangCookiekey]))
                    // if not exists set default language
                    if (!bLangExists)
                        $cookies[globalLangCookiekey] = $scope.globals.languages[0];
                }

                $scope.globals.globalLang = $cookies[globalLangCookiekey];
                $scope.setActiveLangByGlobalLang();
                $scope.$watch(
                    () => { return $scope.globals.globalLang },
                    (newVal, oldVal) => {
                        if (isVoid(newVal)) {
                            return;
                        }
                        if (oldVal !== newVal) {
                            $cookies[globalLangCookiekey] = newVal;
                        }
                    }, false);
                $scope.$watch(
                    () => { return $cookies[globalLangCookiekey] },
                    (newVal, oldVal) => {
                        if (isVoid(newVal)) {
                            return;
                        }
                        if (oldVal !== newVal) {
                            $scope.globals.globalLang = newVal;
                            $scope.setActiveLangByGlobalLang();
                            $.datepicker.setDefaults($.datepicker.regional[$scope.globals.globalLang]);
                        }
                    }, false);
            } else {
                $scope.globals.globalLang = $scope.globals.languages[0];
                $scope.setActiveLangByGlobalLang();
            }
            $.datepicker.setDefaults($.datepicker.regional[$scope.globals.globalLang]);

            // add some watchers
            //From ControllerLogin
            $scope.$watch('globals.nInFlightRequests', (newValue: number, oldValue: number) => {
                if (newValue === 0) {
                    $("html").removeClass("wait");
                } else {
                    $("html").addClass("wait");
                }
            });

            //new watchers for login
            $scope.$watch(() => { return $cookies[LoginService.sessionCookieKey] },
                (newValue: string, oldValue: string) => {
                if (newValue !== oldValue) {

                    if (!isVoid(newValue) && isVoid(oldValue)) {
                        // Just LoggedIn
                        LoginService.ssoDeleted = false;
                        LoginService.afterLoginInit();
                        if (NavigationService.$location.path() === '/login') {
                            $timeout(() => {
                                NavigationService.goToAfterLoginPage();
                            }, 0);
                        }
                    }

                    if (isVoid(newValue) && !isVoid(oldValue)) {
                        // Session Cookie deleted
                        LoginService.deleteSubsCodeCookie();
                        $timeout(() => {
                            NavigationService.goToNotLoggedInPage();
                            // reload (to clear up Javascript completely - globals, etc)
                            LoginService.finalReload();
                        }, 0);
                    }

                    if (!isVoid(newValue) && !isVoid(oldValue)) {
                        LoginService.onSessionChanged();
                    }
                }
                });
            $scope.$watch(() => { return $cookies[LoginService.ssoCookieKey] },
                (newValue: string, oldValue: string) => {
                    if (newValue !== oldValue) {

                        if (!isVoid(newValue)) {
                            // A New SsoSession created. Delete previous app Session if exists (with logout)
                            // On reload controllerMain will make login via Sso
                            if (LoginService.isLoggedIn()) {
                                LoginService.sessionBelongToSso().
                                    then(
                                        // if sessionId not belong to ssoSession logout
                                        (belongToSso) => {
                                                if (isVoid(belongToSso) || !belongToSso) {
                                                    LoginService.logout(true)
                                                };
                                    },
                                        // if error
                                        (reason) => { LoginService.logout(true); }
                                );
                            } else {
                                LoginService.loginSso(() => {});
                            }
                        }

                        if (isVoid(newValue) && !isVoid(oldValue)) {
                            // SsoSession terminated
                            LoginService.ssoDeleted = true; // we need this in finalreload() which called after logout (because of sessionCoockie deletion)
                            // terminate app Session
                            if (LoginService.isLoggedIn()) {
                                LoginService.logout(true);
                            }
                        }
                    }
                });


            // From ControllerNavigation
            var resizableOptions = {
                handles: 'e',
                minWidth: 150,
                maxWidth: 350
            }
            resizableOptions["resize"] = function () {
                Utils.UpdateMainWindowWidthAndMainButtonPlacement($scope);
            };
            $(".resizable1").resizable(resizableOptions);

            $(window).resize(
                function () {
                    Utils.UpdateMainWindowWidthAndMainButtonPlacement($scope);
                });

            $window.onbeforeunload = () => {
                if ($scope.globals.bLoggingOut === true)
                    return undefined;

                if (LoginService.isLoggedIn()) {
                    var msg = $scope.globals.getDynamicMessage("OnbeforeunloadMsg");
                    return msg;
                } else
                    return undefined;
            };

            $window.addEventListener('unload', function () {
                if (LoginService.isLoggedIn()) {
                    if (supports_html5_storage()) {
                        sessionStorage[NavigationService.strResetVar] = true;
                    }
                    if (LoginService.logoutOnUnload) {
                        LoginService.logout(true);
                    }
                }
                if (!isVoid($cookies[globalLangCookiekey]) && supports_html5_storage()) {
                    localStorage[globalLangCookiekey] = $cookies[globalLangCookiekey];
                }
            });

            $scope.globals.initApp($scope, NavigationService, LoginService, $http, $timeout, () => {

                // Check if LoggedIn and if so Initialize app
                if (LoginService.isLoggedIn()) {
                    LoginService.afterLoginInit();
                } else if (LoginService.existsSso()) {
                    if (supports_html5_storage() && sessionStorage[NavigationService.strResetVar] && sessionStorage[NavigationService.strResetVar] === "true") {
                        sessionStorage[NavigationService.strResetVar] = false;
                        NavigationService.assignLocation("partials/logout.html");
                    } else {
                        LoginService.loginSso(() => {});
                    }
                } else {
                    if (supports_html5_storage() && sessionStorage[NavigationService.strResetVar] && sessionStorage[NavigationService.strResetVar] === "true") {
                        sessionStorage[NavigationService.strResetVar] = false;
                        NavigationService.go(NavigationService.modelNavigation.notLoggedInPagePath);
                    }
                }

            });
            


            var checkForNewBannerMain = () => {
                if (!LoginService.isLoggedIn())
                    return;

                var url = "/" + $scope.globals.appName + "/rest/Menu/getBannerMessages";
                $http.post(
                    url, { "wsResponseInfoList": $scope.globals.getAndResetWsResponseInfoList() }, { withCredentials: true, timeout: $scope.globals.timeoutInMS, cache: false }
                ).success(function (response, status, header, config) {
                    $scope.globals.appWsResponeInfoEnabled = response.isWsResponseInfoEnabled;
                    var bannerMessages = isVoid(response.result) ? [] : response.result;
                    if (bannerMessages.length === 0) {
                        NavigationService.modelNavigation.banner = '';
                    } else {
                        var sortedMessagesByImportance = _.sortBy(
                            bannerMessages,
                            (msg: BannerMessage) => {
                                return msg.type;
                            });
                        NavigationService.modelNavigation.banner = sortedMessagesByImportance[0].message;
                        switch (sortedMessagesByImportance[0].type) {
                            case 1:
                                NavigationService.modelNavigation.bannerClass = 'bannerMainDanger';
                                break;
                            case 2:
                                NavigationService.modelNavigation.bannerClass = 'bannerMainWarning';
                                break;
                            case 3:
                                NavigationService.modelNavigation.bannerClass = 'bannerMainInfo';
                                break;
                            default:
                                NavigationService.modelNavigation.bannerClass = 'bannerMainInfo';
                        }
                    }
                }).error(function (response, status, header, config) {
                    NavigationService.modelNavigation.banner = '';
                });
            }
            window.setInterval(checkForNewBannerMain, 60000);
            

        }
    }

}
