/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />

module Controllers {

    export class GeometryLineRow {
        constructor(
            public length: string,
            public lengthVal: number,
            public lineFeats: ol.Feature,
            public origIdx: number)
        { }
    }

    export class ChooseSplitLineModel {
        rows: Array<GeometryLineRow> = [];
        dataSetIdx: number = -1;
    }

    export interface IChooseSplitLineScope extends NpTypes.IApplicationScope {
        model: ChooseSplitLineModel;
        nevermind: () => void;
        preview: (idx: number) => void;
        select: (idx: number) => void;
    }
                 
    export class controllerChooseSplitLine {

        constructor(
            public $scope: IChooseSplitLineScope,
            public Plato: Services.INpDialogMaker,
            $timeout: ng.ITimeoutService) {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("ChooseSplitLineDialog");
            var npMapThis: NpGeo.NpMap = (<any>dialogOptions).npMapThis;
            var self = this;
            $scope.model = new ChooseSplitLineModel();

            var linesExist: any = (<any>dialogOptions).linesExist;
           
            linesExist.forEach((x, origIdx) => {
               // console.log("x=====", x);
                var geom = x.getGeometry();
                var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:2100');
                var transLine = geom.clone().transform(
                    ol.proj.get('EPSG:3857'),
                    ol.proj.get(coordinateSystem)
                );
                var lengthVal = transLine.getLength();
                var length;
                if (lengthVal > 10000) {
                    length = (Math.round((lengthVal / 1000) * 100) / 100) + '&nbsp;' + 'km';
                } else {
                    length = (Math.round(lengthVal * 100) / 100) + '&nbsp;' + 'm';
                }
                $scope.model.rows.push(new GeometryLineRow(length, lengthVal, x, origIdx));
            });

            $scope.model.rows.sort((a: GeometryLineRow, b: GeometryLineRow) => {
                return b.lengthVal - a.lengthVal;
            });

            $scope.nevermind = function () {
                npMapThis.featureOverlayCrop.getSource().clear();
                npMapThis.splitInteraction.overlayLayer_.getSource().clear();
                NpGeoGlobals.splitLayer.getSource().clear();
                npMapThis.unregitserSplitListeners();
                npMapThis.splitInteraction.isFirstPoint = true;
                dialogOptions.jquiDialog.dialog("close");
            }

            $scope.preview = function (idx: number) {
               // console.log("idx=====", idx);
               // console.log("linesExist=====", linesExist[idx]);
                $scope.model.dataSetIdx = idx;
                npMapThis.featureOverlayCrop.getSource().clear();
                npMapThis.featureOverlayCrop.getSource().addFeature(linesExist[idx]);
            };

            $scope.select = function (idx) {
              //  console.log("select  $scope.model.dataSetIdx=====", $scope.model.dataSetIdx); 
                var geo = NpGeoGlobals.geoJSON_to_OL3feature({
                    obj: NpGeoGlobals.OL3feature_to_geoJSON(linesExist[$scope.model.dataSetIdx], npMapThis.coordinateSystem),
                    coordinateSystem: npMapThis.coordinateSystem,
                    name: npMapThis.getEntityLabel(npMapThis.Current)
                });
                npMapThis.setFeature(geo);
                if (typeof npMapThis.entityHistories[npMapThis.currentEntityKey] == 'undefined') {
                    npMapThis.entityHistories[npMapThis.currentEntityKey] = [];
                }
                npMapThis.entityHistories[npMapThis.currentEntityKey].push(NpGeoGlobals.safeClone(geo));
                var geoJSON = NpGeoGlobals.OL3feature_to_geoJSON(geo, npMapThis.coordinateSystem);
                npMapThis.saveDataToEntity(npMapThis.Current, geoJSON);
                npMapThis.splitInteraction.overlayLayer_.getSource().clear();
                NpGeoGlobals.splitLayer.getSource().clear();
                npMapThis.unregitserSplitListeners();
                npMapThis.splitInteraction.isFirstPoint = true;
                npMapThis.featureOverlayCrop.getSource().clear();
                dialogOptions.jquiDialog.dialog("close");
            };
        }
    }
}
