/// <reference path="../npTypes.ts" />
/// <reference path="controllerLogin.ts" />

module Controllers {

    export class ModelChangePass {
        message: string = "";
        publicUserName: string = "";
        oldPassword: string = "";
        newPassword1: string = "";
        newPassword2: string = "";
    }

    export interface IChangePassScope extends Controllers.ILoginScope {
        closeChangePass():void;
        setNewPassword():void;
        modelChangePass: ModelChangePass;
    }

    export class controllerChangePass {

        constructor(
            public $scope: IChangePassScope,
            private $http: ng.IHttpService,
            private $timeout: ng.ITimeoutService,
            private Plato: Services.INpDialogMaker,
            private LoginService: Services.LoginService
        ) {
            var scope: IChangePassScope = $scope;
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>scope.globals.findAndRemoveDialogOptionByClassName("ChangePassClass");

            scope.modelChangePass = new ModelChangePass();

            scope.closeChangePass = function () {
                dialogOptions.jquiDialog.dialog("close");
            }
            scope.setNewPassword = function () {

                scope.modelChangePass.message = "";

                LoginService.setNewPassword(scope.modelChangePass.publicUserName,
                    scope.modelChangePass.oldPassword,
                    scope.modelChangePass.newPassword1,
                    scope.modelChangePass.newPassword2,
                    () => {
                        scope.modelChangePass.message = scope.globals.getDynamicMessage("ChangePasswdDialog_SuccessMsg");
                        $timeout(function () {
                            scope.modelChangePass.message = "";
                            dialogOptions.jquiDialog.dialog("close");
                        }, 3000);
                    }, (errMsg) => {
                        scope.modelChangePass.message = "<strong>" + scope.getALString('ChangePasswdDialog_ErrorLabel') + ":</strong>&nbsp;" + errMsg;
                        $timeout(function () {
                            scope.modelChangePass.message = "";
                        }, 14000);
                    });
            }

        }
    }
}
