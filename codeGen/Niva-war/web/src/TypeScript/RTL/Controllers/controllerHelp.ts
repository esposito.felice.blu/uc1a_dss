/// <reference path="../npTypes.ts" />

module Controllers {

    export interface IHelpScope extends NpTypes.IApplicationScope {
        closeHelp():void;
    }

    export class controllerHelp {

        constructor(public $scope: IHelpScope) {
            var dialogOptions: NpTypes.DialogOptions = <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("HelpClass");

            $scope.closeHelp = function() {
                dialogOptions.jquiDialog.dialog("close");
            }
        }
    }
}
