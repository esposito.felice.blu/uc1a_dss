/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    var FeatureRow = (function () {
        function FeatureRow(area, areaValue, origIdx) {
            this.area = area;
            this.areaValue = areaValue;
            this.origIdx = origIdx;
        }
        return FeatureRow;
    })();
    Controllers.FeatureRow = FeatureRow;
    var ChooseFeatureModel = (function () {
        function ChooseFeatureModel() {
            this.rows = [];
            this.dataSetIdx = -1;
        }
        return ChooseFeatureModel;
    })();
    Controllers.ChooseFeatureModel = ChooseFeatureModel;
    var controllerChooseFeature = (function () {
        function controllerChooseFeature($scope, Plato, $timeout) {
            this.$scope = $scope;
            this.Plato = Plato;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("ChooseFeatureDialog");
            var npMapThis = dialogOptions.npMapThis;
            var featuresRemaining = dialogOptions.featuresRemaining;
            var self = this;
            $scope.model = new ChooseFeatureModel();
            featuresRemaining.forEach(function (x, origIdx) {
                //console.log("x", x);
                var area;
                var geometry = x.a.getGeometry();
                if (geometry instanceof ol.geom.Polygon) {
                    var poly = geometry;
                    area = npMapThis.formatArea(poly, true).replace(/ /g, "&nbsp;");
                    var label = "Digitized Feature"; //if null. means unmanaged layer. the temp digitised geometry should be the one
                    if (!isVoid(x.b))
                        label = x.b.npName;
                    area += "&nbsp;(" + label + ")";
                    //area += "&nbsp;(" + npMapThis.getLayerLabelById(x.a.get('layerId')) + ")";
                    //if (!isVoid(x.a.get('layerId'))) {
                    //    area += "&nbsp;(" + npMapThis.getLayerLabelById(x.a.get('layerId')) + ")";
                    //}
                    $scope.model.rows.push(new FeatureRow(area, area, origIdx));
                }
                else if (geometry instanceof ol.geom.MultiPolygon) {
                    var poly = geometry;
                    area = npMapThis.formatArea(poly, true).replace(/ /g, "&nbsp;");
                    var label = "Digitized Feature"; //if null. means unmanaged layer. the temp digitised geometry should be the one
                    if (!isVoid(x.b))
                        label = x.b.npName;
                    area += "&nbsp;(" + label + ")";
                    $scope.model.rows.push(new FeatureRow(area, area, origIdx));
                }
                else if (geometry instanceof ol.geom.LineString) {
                    var line = geometry;
                    area = npMapThis.formatLength(line).replace(/ /g, "&nbsp;");
                    var label = "Digitized Feature"; //if null. means unmanaged layer. the temp digitised geometry should be the one
                    if (!isVoid(x.b))
                        label = x.b.npName;
                    area += "&nbsp;(" + label + ")";
                    $scope.model.rows.push(new FeatureRow(area, area, origIdx));
                }
                else if (geometry instanceof ol.geom.Point) {
                    var poly = geometry;
                    area = 0 + 'm<sup>2</sup>'; //there is no fucking area when points,lines
                    var label = "Digitized Feature"; //if null. means unmanaged layer. the temp digitised geometry should be the one
                    if (!isVoid(x.b))
                        label = x.b.npName;
                    area += "&nbsp;(" + label + ")";
                    //area += "&nbsp;(" + npMapThis.getLayerLabelById(x.a.get('layerId')) + ")";
                    //if (!isVoid(x.a.get('layerId'))) {
                    //    area += "&nbsp;(" + npMapThis.getLayerLabelById(x.a.get('layerId')) + ")";
                    //}
                    $scope.model.rows.push(new FeatureRow(area, area, origIdx));
                }
            });
            $scope.model.rows.sort(function (a, b) {
                return b.areaValue - a.areaValue;
            });
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
            function commonWork(f, idx) {
                var g = featuresRemaining[idx].a.getGeometry();
                var newFeature = new ol.Feature();
                newFeature.setGeometryName("");
                newFeature.setGeometry(g);
                f.clear();
                //console.log("newFeature", newFeature);
                f.push(newFeature);
            }
            $scope.preview = function (idx) {
                npMapThis.featureOverlayCrop.getSource().clear();
                commonWork(npMapThis.featureOverlayCrop.getSource().getFeaturesCollection(), idx);
                $scope.model.dataSetIdx = idx;
            };
            $scope.select = function () {
                npMapThis.featureOverlayCrop.getSource().clear();
                npMapThis.selectInteraction.getFeatures().clear();
                commonWork(npMapThis.selectInteraction.getFeatures(), $scope.model.dataSetIdx);
                npMapThis.clearInteractions();
                npMapThis.addSelectInteraction();
                var selectedFeatures = npMapThis.selectInteraction.getFeatures();
                var feature = featuresRemaining[$scope.model.dataSetIdx].a;
                var layer = featuresRemaining[$scope.model.dataSetIdx].b;
                selectedFeatures.push(feature);
                if (!isVoid(layer) && !isVoid(layer.getSqlVectorLayer)) {
                    layer.getSqlVectorLayer().onSelectFeature(feature, layer);
                }
                else {
                    var html = npMapThis.getFeatureLabel(feature, false);
                    if (!isVoid(html)) {
                        messageBox($scope, Plato, "Info", html, IconKind.INFO, [
                            new Tuple2("OK", function () { })
                        ], 0, 0, 'auto');
                    }
                }
                dialogOptions.jquiDialog.dialog("close");
            };
        }
        return controllerChooseFeature;
    })();
    Controllers.controllerChooseFeature = controllerChooseFeature;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerChooseFeature.js.map