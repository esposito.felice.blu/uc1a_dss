/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />

module Controllers {

    export class LayerSearchState {
        static cntOptions:number = 0;
        public id:number;
        public label:string;
        constructor(
            public sqlLayer: NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>)
        {
            this.id = LayerSearchState.cntOptions++;
            this.label = sqlLayer.label;
        }
    }

    export class LayerSearchModel {
        inputs: Array<LayerSearchState> = [];
        outputs: Array<LayerSearchState> = [];
        searchLayer: LayerSearchState;
        resultLayer: LayerSearchState;
    }

    export interface ILayerSearchScope extends NpTypes.IApplicationScope {
        modelSearch: LayerSearchModel;
        doTheWork: ()=>void;
        change(foo:any);
    }

    export class controllerSearchLayers {

        constructor(public $scope: ILayerSearchScope, $timeout:ng.ITimeoutService, public Plato: Services.INpDialogMaker) {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("ChooseSearchLayersDialog");

            var self = this;
            var npMapThis:NpGeo.NpMap = (<any>dialogOptions).npMapThis;
            $scope.modelSearch = new LayerSearchModel();
            var getVectorLayers = (isSearch:boolean) => {
                return npMapThis.layers.
                    filter(l => l instanceof NpGeoLayers.SqlVectorLayer).
                    filter((l: NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>) => {
                        if (isSearch === false) {
                            return l.isSearch === isSearch && !isVoid(l.searchControllerClassName);
                        } else {
                            return l.isSearch === isSearch;
                        }
                    }).
                    map((l: NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>) => {
                        return new LayerSearchState(l);
                    });
            };

            $scope.modelSearch.inputs = getVectorLayers(false);
            $scope.modelSearch.outputs = getVectorLayers(true);

            if ($scope.modelSearch.inputs.length === 0 || $scope.modelSearch.outputs.length === 0) {
                var msg = ($scope.modelSearch.inputs.length === 0)?"προς αναζήτηση.":"αποτελεσμάτων.";
                messageBox(
                    $scope,
                    self.Plato,
                    "Δεν είναι δυνατή η αναζήτηση...",
                    "Δεν υπάρχουν θεματικά επίπεδα " + msg,
                    IconKind.ERROR, 
                    [
                        new Tuple2("OK", () => {
                        })
                    ], 0, 0, '22em');
                $timeout( () => {
                    dialogOptions.jquiDialog.dialog("close");
                }, 10);
                return;
            }

            $scope.modelSearch.searchLayer = $scope.modelSearch.inputs[0];
            $scope.modelSearch.resultLayer = $scope.modelSearch.outputs[0];

            $scope.doTheWork = () => {
                dialogOptions.jquiDialog.dialog("close");
                var onOk: (searchLayer: NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>, otherLayer: NpGeoLayers.SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>) => void;
                onOk = (<any>dialogOptions).onOk;
                onOk($scope.modelSearch.resultLayer.sqlLayer, $scope.modelSearch.searchLayer.sqlLayer);
            }
        }
    }
}
