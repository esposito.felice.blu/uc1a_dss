﻿/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../services.ts" />

module Controllers {

    export interface INpBreadcrumbScope extends NpTypes.IApplicationScope {
        toggleMenu(): void;
        logout(): void;
    }

    export class controllerNpBreadcrumb {

        constructor(
            public $scope: INpBreadcrumbScope,
            public $timeout: ng.ITimeoutService,
            private Plato: Services.INpDialogMaker,
            private LoginService: Services.ILoginService,
            private NavigationService: Services.NavigationService
        )
        {
            var self = this;
            $scope.modelNavigation = NavigationService.modelNavigation;
            $scope.toggleMenu = () => { self.toggleMenu(); };
            $scope.logout = () => {
                LoginService.logout();
            }
        }

        public toggleMenu(): void {
            var self = this;
            function resize(target, new_width) { 
                // http://stackoverflow.com/questions/7506104/jquery-ui-resizable-set-size-programmatically
                var $wrapper = $(target).resizable('widget'),
                    $element = $wrapper.find('.ui-resizable'),
                    dx = $element.width() - new_width;
                $element.width(new_width);
                $wrapper.width($wrapper.width() - dx);
                $('.TopFormArea').css('width', 'calc( 100% - ' + (new_width + 17) + 'px )');
                $('.fixedContainer').css('width',
                    (parseInt($('.TopFormArea').css('width')) - 52) + 'px'); // subtract 52px for scrollbar buttons
            }
            if (self.$scope.modelNavigation.menuLabel === 'Hide menu') {
                self.$scope.modelNavigation.menuLabel = 'Show menu';
                self.$scope.modelNavigation.oldMenuAreaWidth = parseInt($('.resizable1').css('width'));
                self.$scope.modelNavigation.oldWorkAreWidth = parseInt($('.resizable2').css('width'));
                resize('resizable1', 0);
                $('.resizable2').css('left', '0%');
                $('.resizable2').css('width', '100%');
            } else {
                self.$scope.modelNavigation.menuLabel = 'Hide menu';
                resize('resizable1', self.$scope.modelNavigation.oldMenuAreaWidth);
                $('.resizable2').css('left', self.$scope.modelNavigation.oldMenuAreaWidth);
                $('.resizable2').css('width', self.$scope.modelNavigation.oldWorkAreWidth);
            }
            // Trigger resizes of all grids
            // We don't need this trigger since we added watcher of vissibility of 'resizable1' in rootscope (in controllerMain)
            //self.$scope.modelNavigation.epochOfLastResize = Utils.getTimeInMS();
        }

    }
}
