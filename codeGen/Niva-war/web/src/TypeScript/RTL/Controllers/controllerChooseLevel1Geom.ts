/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />

module Controllers {

    export class Lvl1GeometryRow {
        constructor(
            public area:string,
            public areaValue: number,
            public ilot_no: string,
            public valid: string,
            public cover_id: string,
            public gtype: string,  
            public origIdx: number)
        {}
    }

    export class ChooseLvl1GeomModel {
        rows: Array<Lvl1GeometryRow> = [];
        dataSetIdx: number = -1;
    }

    export interface IChooseLvl1GeomScope extends NpTypes.IApplicationScope {
        model: ChooseLvl1GeomModel;
        nevermind: () => void;
        init: () => void;
        preview: (idx:number)=>void;
        select: (idx:number)=>void;
    }
    
    export class controllerChooseLevel1Geom {
        constructor(
            public $scope: IChooseLvl1GeomScope, 
            $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker
            
            
        )
        {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("ChooseLevel1GeomDialog");
            var npMapThis:NpGeo.NpMap = (<any>dialogOptions).npMapThis;
            var polygonsRemaining: any = (<any>dialogOptions).polygonsRemaining;

            var labelForGeoms: any = [];
            var coverIds: any = [];
            var gtype: any = [];
            var valid: any = [];

            var imgPathDown: string = "/" + $scope.globals.appName + "/img/arrow_show.png";
            var imgPathUp: string = "/" + $scope.globals.appName + "/img/arrow_hide.png";

            var self = this;
            $scope.model = new ChooseLvl1GeomModel();
            polygonsRemaining.forEach((x, origIdx) => {
                labelForGeoms.push(x.entity.data['ilot_no']);
                coverIds.push(x.entity.data['cover_id']);
                gtype.push(x.entity.data['gtype']);
                valid.push(x.entity.data['valid']);

                var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:2100');
                var transGeom = x.getGeometry().clone().transform(
                    ol.proj.get('EPSG:3857'),
                    ol.proj.get(coordinateSystem)
                );
                var area = transGeom.getArea();

                var output;
                var textColor = 'black'
                //if invalid or not polygon , mark the text as red
                if (valid[origIdx] !== 'TRUE' || gtype[origIdx] !== 2003) {
                    textColor = 'red';
                }
                
                if (area > 10000) {
                    output = (Math.round(area / 1000000 * 100) / 100) + '&nbsp;' + 'km<sup>2</sup>' +
                        '<img align="right" width= "16" height= "16" src= "' + imgPathDown + '"  id= "btninfodiv_' + origIdx + '" >' +
                        '<p style="display: none;color:' + textColor+'" id="infodiv_' + origIdx + '">' +
                        ' (<b>ilot_no:</b>' + labelForGeoms[origIdx] + ',<br/><b>valid:</b>' + valid[origIdx] +
                        ',<b>cover_id:</b>' + coverIds[origIdx] + ',<br/><b>gtype:</b>' + gtype[origIdx] + ')' +
                        '</p>';
                } else {
                    output = (Math.round(area * 100) / 100) + '&nbsp;' + 'm<sup>2</sup>' +
                        '<img align="right" width= "16" height= "16" src= "' + imgPathDown + '"  id= "btninfodiv_' + origIdx + '" >' +
                        '<p style="display: none;color:' + textColor + '" id="infodiv_' + origIdx + '">' +
                        ' (<b>ilot_no:</b>' + labelForGeoms[origIdx] + ',<br/><b>valid:</b>' + valid[origIdx] +
                        ',<b>cover_id:</b>' + coverIds[origIdx] + ',<br/><b>gtype:</b>' + gtype[origIdx] + ')' +
                        '</p>';
                }
                $scope.model.rows.push(new Lvl1GeometryRow(output, area, labelForGeoms, valid, coverIds, gtype, origIdx));
            });

            $scope.model.rows.sort((a: Lvl1GeometryRow, b: Lvl1GeometryRow) => {
                return b.areaValue - a.areaValue;
            });

    

            $timeout(function () {
                polygonsRemaining.forEach((x, origIdx) => {
                    //toggle the info paragraph
                    $('#btninfodiv_' + origIdx).click(function () {
                        $('#infodiv_' + origIdx).toggle("slow", function () {
                            var divId = "infodiv_" + origIdx;
                            var btnDivId = "btninfodiv_" + origIdx;
                           //toggle the img button
                            if ($('#' + divId).is(":hidden")) {
                                $('#' + btnDivId).attr("src", imgPathDown);
                            }
                            if ($('#' + divId).is(":visible")) {
                                $('#' + btnDivId).attr("src", imgPathUp);
                            }
                        });
                    });
                })

            });

            $scope.nevermind = function() {
                dialogOptions.jquiDialog.dialog("close");
            }

            function commonWork(f: ol.Collection<ol.Feature>, idx:number) {
                //var croppedFeature = NpGeoGlobals.coordinates_to_OL3feature(polygonsRemaining[idx]);
                var croppedFeature = polygonsRemaining[idx];
                var g = croppedFeature.getGeometry();
                var newFeature = new ol.Feature();
                newFeature.setGeometryName("");
                newFeature.setGeometry(g);
                f.clear();
                f.push(newFeature);
                npMapThis.enterDrawMode();
            }

            $scope.preview = function(idx:number) {
                npMapThis.featureOverlayCrop.getSource().clear();
                var features = npMapThis.featureOverlayCrop.getSource().getFeaturesCollection();
                if (features === null) {
                    features = new ol.Collection<ol.Feature>();
                } 
                commonWork(features, idx);
                //console.log("polygonsRemaining[idx]========",polygonsRemaining[idx]);
                //var directionsClockwise = polygonsRemaining[idx].map(x => NpGeoGlobals.isClockWise(x)).filter(x => x === false);
                //console.log("directionsClockwise", directionsClockwise);
                var isValid = isGeoJstsValid(polygonsRemaining[idx].getGeometry());
                //console.log("isValid", isValid);
                var geomType = polygonsRemaining[idx].getGeometry().getType();
                //console.log("geomType", geomType);
                if (isValid !== true || geomType !== 'Polygon') {
                    messageBox(
                        $scope,
                        Plato,
                        "Μη επιτρεπτή γεωμετρία",
                        "Προκύπτει μη αποδεκτή γεωμετρία. (τύπος:" + geomType + ", εγκυρότητα:" + isValid+")",
                        IconKind.ERROR, 
                        [
                            new Tuple2("OK", () => {})
                        ], 0, 0, '19em');
                    $scope.model.dataSetIdx = -1;
                } else
                    $scope.model.dataSetIdx = idx;
            };

            $scope.select = function() {
                npMapThis.featureOverlayCrop.getSource().clear();
                commonWork(npMapThis.featureOverlayDraw.getSource().getFeaturesCollection(), $scope.model.dataSetIdx);
                dialogOptions.jquiDialog.dialog("close");
            };
        }
    }
}
