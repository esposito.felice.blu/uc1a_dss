/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../utils.ts" />


module Entities {

    export class RTL_Subscriber extends NpTypes.BaseEntity {

        constructor(
            id: string,
            shortName: string,
            vat: string,
            legalName: string,
            taxOffice: string,
            occupation: string,
            phone: string,
            fax: string,
            address: string,
            addressNumber: string,
            postalCode: string,
            city: string,
            prefecture: string,
            country: string,
            email: string,
            webSite: string,
            database: string,
            applicationUrl: string,
            invoicingMethod: number,
            suvcId: number,
            companyVat: string,
            billingCompId: number,
            rowVersion: number,
            userAutoInactivate: boolean,
            loginPolicy: number,
            legalNameShort: string) 
        {
            super();
            var self = this;
            this._id = new NpTypes.UIStringModel(id, this);
            this._shortName = new NpTypes.UIStringModel(shortName, this);
            this._vat = new NpTypes.UIStringModel(vat, this);
            this._legalName = new NpTypes.UIStringModel(legalName, this);
            this._taxOffice = new NpTypes.UIStringModel(taxOffice, this);
            this._occupation = new NpTypes.UIStringModel(occupation, this);
            this._phone = new NpTypes.UIStringModel(phone, this);
            this._fax = new NpTypes.UIStringModel(fax, this);
            this._address = new NpTypes.UIStringModel(address, this);
            this._addressNumber = new NpTypes.UIStringModel(addressNumber, this);
            this._postalCode = new NpTypes.UIStringModel(postalCode, this);
            this._city = new NpTypes.UIStringModel(city, this);
            this._prefecture = new NpTypes.UIStringModel(prefecture, this);
            this._country = new NpTypes.UIStringModel(country, this);
            this._email = new NpTypes.UIStringModel(email, this);
            this._webSite = new NpTypes.UIStringModel(webSite, this);
            this._database = new NpTypes.UIStringModel(database, this);
            this._applicationUrl = new NpTypes.UIStringModel(applicationUrl, this);
            this._invoicingMethod = new NpTypes.UINumberModel(invoicingMethod, this);
            this._suvcId = new NpTypes.UINumberModel(suvcId, this);
            this._companyVat = new NpTypes.UIStringModel(companyVat, this);
            this._billingCompId = new NpTypes.UINumberModel(billingCompId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._userAutoInactivate = new NpTypes.UIBoolModel(userAutoInactivate, this);
            this._loginPolicy = new NpTypes.UINumberModel(loginPolicy, this);
            this._legalNameShort = new NpTypes.UIStringModel(legalNameShort, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.id; 
        }
        
        public getKeyName(): string { 
            return "id"; 
        }

        public getEntityName(): string { 
            return 'RTL_Subscriber'; 
        }
        
        public fromJSON(data: any): RTL_Subscriber[] { 
            return RTL_Subscriber.fromJSONComplete(data);
        }

        //id property
        _id: NpTypes.UIStringModel;
        public get id():string {
            return this._id.value;
        }
        public set id(id:string) {
            var self = this;
            self._id.value = id;
        //    self.id_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public id_listeners: Array<(c:Subscriber) => void> = [];
        //shortName property
        _shortName: NpTypes.UIStringModel;
        public get shortName():string {
            return this._shortName.value;
        }
        public set shortName(shortName:string) {
            var self = this;
            self._shortName.value = shortName;
        //    self.shortName_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public shortName_listeners: Array<(c:Subscriber) => void> = [];
        //vat property
        _vat: NpTypes.UIStringModel;
        public get vat():string {
            return this._vat.value;
        }
        public set vat(vat:string) {
            var self = this;
            self._vat.value = vat;
        //    self.vat_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public vat_listeners: Array<(c:Subscriber) => void> = [];
        //legalName property
        _legalName: NpTypes.UIStringModel;
        public get legalName():string {
            return this._legalName.value;
        }
        public set legalName(legalName:string) {
            var self = this;
            self._legalName.value = legalName;
        //    self.legalName_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public legalName_listeners: Array<(c:Subscriber) => void> = [];
        //taxOffice property
        _taxOffice: NpTypes.UIStringModel;
        public get taxOffice():string {
            return this._taxOffice.value;
        }
        public set taxOffice(taxOffice:string) {
            var self = this;
            self._taxOffice.value = taxOffice;
        //    self.taxOffice_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public taxOffice_listeners: Array<(c:Subscriber) => void> = [];
        //occupation property
        _occupation: NpTypes.UIStringModel;
        public get occupation():string {
            return this._occupation.value;
        }
        public set occupation(occupation:string) {
            var self = this;
            self._occupation.value = occupation;
        //    self.occupation_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public occupation_listeners: Array<(c:Subscriber) => void> = [];
        //phone property
        _phone: NpTypes.UIStringModel;
        public get phone():string {
            return this._phone.value;
        }
        public set phone(phone:string) {
            var self = this;
            self._phone.value = phone;
        //    self.phone_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public phone_listeners: Array<(c:Subscriber) => void> = [];
        //fax property
        _fax: NpTypes.UIStringModel;
        public get fax():string {
            return this._fax.value;
        }
        public set fax(fax:string) {
            var self = this;
            self._fax.value = fax;
        //    self.fax_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public fax_listeners: Array<(c:Subscriber) => void> = [];
        //address property
        _address: NpTypes.UIStringModel;
        public get address():string {
            return this._address.value;
        }
        public set address(address:string) {
            var self = this;
            self._address.value = address;
        //    self.address_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public address_listeners: Array<(c:Subscriber) => void> = [];
        //addressNumber property
        _addressNumber: NpTypes.UIStringModel;
        public get addressNumber():string {
            return this._addressNumber.value;
        }
        public set addressNumber(addressNumber:string) {
            var self = this;
            self._addressNumber.value = addressNumber;
        //    self.addressNumber_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public addressNumber_listeners: Array<(c:Subscriber) => void> = [];
        //postalCode property
        _postalCode: NpTypes.UIStringModel;
        public get postalCode():string {
            return this._postalCode.value;
        }
        public set postalCode(postalCode:string) {
            var self = this;
            self._postalCode.value = postalCode;
        //    self.postalCode_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public postalCode_listeners: Array<(c:Subscriber) => void> = [];
        //city property
        _city: NpTypes.UIStringModel;
        public get city():string {
            return this._city.value;
        }
        public set city(city:string) {
            var self = this;
            self._city.value = city;
        //    self.city_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public city_listeners: Array<(c:Subscriber) => void> = [];
        //prefecture property
        _prefecture: NpTypes.UIStringModel;
        public get prefecture():string {
            return this._prefecture.value;
        }
        public set prefecture(prefecture:string) {
            var self = this;
            self._prefecture.value = prefecture;
        //    self.prefecture_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public prefecture_listeners: Array<(c:Subscriber) => void> = [];
        //country property
        _country: NpTypes.UIStringModel;
        public get country():string {
            return this._country.value;
        }
        public set country(country:string) {
            var self = this;
            self._country.value = country;
        //    self.country_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public country_listeners: Array<(c:Subscriber) => void> = [];
        //email property
        _email: NpTypes.UIStringModel;
        public get email():string {
            return this._email.value;
        }
        public set email(email:string) {
            var self = this;
            self._email.value = email;
        //    self.email_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public email_listeners: Array<(c:Subscriber) => void> = [];
        //webSite property
        _webSite: NpTypes.UIStringModel;
        public get webSite():string {
            return this._webSite.value;
        }
        public set webSite(webSite:string) {
            var self = this;
            self._webSite.value = webSite;
        //    self.webSite_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public webSite_listeners: Array<(c:Subscriber) => void> = [];
        //database property
        _database: NpTypes.UIStringModel;
        public get database():string {
            return this._database.value;
        }
        public set database(database:string) {
            var self = this;
            self._database.value = database;
        //    self.database_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public database_listeners: Array<(c:Subscriber) => void> = [];
        //applicationUrl property
        _applicationUrl: NpTypes.UIStringModel;
        public get applicationUrl():string {
            return this._applicationUrl.value;
        }
        public set applicationUrl(applicationUrl:string) {
            var self = this;
            self._applicationUrl.value = applicationUrl;
        //    self.applicationUrl_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public applicationUrl_listeners: Array<(c:Subscriber) => void> = [];
        //invoicingMethod property
        _invoicingMethod: NpTypes.UINumberModel;
        public get invoicingMethod():number {
            return this._invoicingMethod.value;
        }
        public set invoicingMethod(invoicingMethod:number) {
            var self = this;
            self._invoicingMethod.value = invoicingMethod;
        //    self.invoicingMethod_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public invoicingMethod_listeners: Array<(c:Subscriber) => void> = [];
        //suvcId property
        _suvcId: NpTypes.UINumberModel;
        public get suvcId():number {
            return this._suvcId.value;
        }
        public set suvcId(suvcId:number) {
            var self = this;
            self._suvcId.value = suvcId;
        //    self.suvcId_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public suvcId_listeners: Array<(c:Subscriber) => void> = [];
        //companyVat property
        _companyVat: NpTypes.UIStringModel;
        public get companyVat():string {
            return this._companyVat.value;
        }
        public set companyVat(companyVat:string) {
            var self = this;
            self._companyVat.value = companyVat;
        //    self.companyVat_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public companyVat_listeners: Array<(c:Subscriber) => void> = [];
        //billingCompId property
        _billingCompId: NpTypes.UINumberModel;
        public get billingCompId():number {
            return this._billingCompId.value;
        }
        public set billingCompId(billingCompId:number) {
            var self = this;
            self._billingCompId.value = billingCompId;
        //    self.billingCompId_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public billingCompId_listeners: Array<(c:Subscriber) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public rowVersion_listeners: Array<(c:Subscriber) => void> = [];
        //userAutoInactivate property
        _userAutoInactivate: NpTypes.UIBoolModel;
        public get userAutoInactivate():boolean {
            return this._userAutoInactivate.value;
        }
        public set userAutoInactivate(userAutoInactivate:boolean) {
            var self = this;
            self._userAutoInactivate.value = userAutoInactivate;
        //    self.userAutoInactivate_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public userAutoInactivate_listeners: Array<(c:Subscriber) => void> = [];
        //loginPolicy property
        _loginPolicy: NpTypes.UINumberModel;
        public get loginPolicy():number {
            return this._loginPolicy.value;
        }
        public set loginPolicy(loginPolicy:number) {
            var self = this;
            self._loginPolicy.value = loginPolicy;
        //    self.loginPolicy_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public loginPolicy_listeners: Array<(c:Subscriber) => void> = [];
        //legalNameShort property
        _legalNameShort: NpTypes.UIStringModel;
        public get legalNameShort():string {
            return this._legalNameShort.value;
        }
        public set legalNameShort(legalNameShort:string) {
            var self = this;
            self._legalNameShort.value = legalNameShort;
        //    self.legalNameShort_listeners.forEach(cb => { cb(<Subscriber>self); });
        }
        //public legalNameShort_listeners: Array<(c:Subscriber) => void> = [];

        public static fromJSONPartial_actualdecode_private(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): RTL_Subscriber {
            var key = "RTL_Subscriber:"+x.id;
            var ret = new RTL_Subscriber(
                x.id,
                x.shortName,
                x.vat,
                x.legalName,
                x.taxOffice,
                x.occupation,
                x.phone,
                x.fax,
                x.address,
                x.addressNumber,
                x.postalCode,
                x.city,
                x.prefecture,
                x.country,
                x.email,
                x.webSite,
                x.database,
                x.applicationUrl,
                x.invoicingMethod,
                x.suvcId,
                x.companyVat,
                x.billingCompId,
                x.rowVersion,
                x.userAutoInactivate,
                x.loginPolicy,
                x.legalNameShort
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): RTL_Subscriber {
            
            return <RTL_Subscriber>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind): RTL_Subscriber {
            var self = this;
            var key="";
            var ret: RTL_Subscriber = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "RTL_Subscriber:"+x.$refId;
                ret = <RTL_Subscriber>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "RTL_Subscriber::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                ret = RTL_Subscriber.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}): Array<RTL_Subscriber> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret: Array<RTL_Subscriber> = _.map(data, (x: any): RTL_Subscriber => {
                return RTL_Subscriber.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl: NpTypes.IAbstractController, ids: string[], func: (list: RTL_Subscriber[])=>void)  {
            var self = this;
            var url = "/" + ctrl.$scope.globals.appName + "/rest/RTL_Subscriber/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = RTL_Subscriber.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.id = this.id;
                ret.shortName = this.shortName;
                ret.vat = this.vat;
                ret.legalName = this.legalName;
                ret.taxOffice = this.taxOffice;
                ret.occupation = this.occupation;
                ret.phone = this.phone;
                ret.fax = this.fax;
                ret.address = this.address;
                ret.addressNumber = this.addressNumber;
                ret.postalCode = this.postalCode;
                ret.city = this.city;
                ret.prefecture = this.prefecture;
                ret.country = this.country;
                ret.email = this.email;
                ret.webSite = this.webSite;
                ret.database = this.database;
                ret.applicationUrl = this.applicationUrl;
                ret.invoicingMethod = this.invoicingMethod;
                ret.suvcId = this.suvcId;
                ret.companyVat = this.companyVat;
                ret.billingCompId = this.billingCompId;
                ret.rowVersion = this.rowVersion;
                ret.userAutoInactivate = this.userAutoInactivate;
                ret.loginPolicy = this.loginPolicy;
                ret.legalNameShort = this.legalNameShort;
            return ret;
        }
        

        public static cashedEntities_findByCode_Login_subscriber: { [id: string]: RTL_Subscriber; } = {};
        public static findByCode_Login_subscriber(name: any, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, onSuccess: (selectedEntity: RTL_Subscriber) => void, errFunc: (msg: string) => void) {
            var self = this;
            var key = "#" + name;
            if (RTL_Subscriber.cashedEntities_findByCode_Login_subscriber[key] !== undefined) {
                onSuccess(Entities.RTL_Subscriber.cashedEntities_findByCode_Login_subscriber[key]);
                return;
            }

            var wsPath = "Login/findByCode_Login_subscriber";
            var url = "/" + $scope.globals.appName + "/rest/" + wsPath + "?";
            var paramData = {};
            if (name !== undefined && name !== null) { paramData['shortName'] = name }
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities = RTL_Subscriber.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        var ret = dbEntities[0];
                        Entities.RTL_Subscriber.cashedEntities_findByCode_Login_subscriber[key] = ret;
                        onSuccess(ret);
                    } else if (dbEntities.length > 1) {
                        errFunc('TooManyRows');
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityByCode_NotFoundMsg()"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });

        }

        public getRowVersion(): number {
            return this.rowVersion;
        }

    }

    NpTypes.BaseEntity.entitiesFactory['RTL_Subscriber'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.RTL_Subscriber.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.RTL_Subscriber.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}