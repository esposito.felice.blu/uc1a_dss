/// <reference path="../DefinitelyTyped/jquery/jquery.d.ts" />

// From MSDN:

// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).

var rv = -1; // Return value assumes failure.
if (navigator.appName == 'Microsoft Internet Explorer') {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
        rv = parseFloat( RegExp.$1 );
}
if (rv != -1 && rv <= 9) {
    alert(
        "Η εφαρμογή χρησιμοποιεί προηγμένα χαρακτηριστικά που " +
        "διαθέτουν μόνο μοντέρνοι πλοηγοί διαδικτύου (browsers).\n\n" + 
        "Παρακαλώ κατεβάστε και εγκαταστήστε (δωρεάν) έναν από τους:\n\n" +
        "  - Firefox\n\t(http://www.getfirefox.com)\n\n" +
        "  - Internet Explorer 11\n\t(http://windows.microsoft.com/el-gr/internet-explorer/download-ie)\n\n");
    window.close();
}

//if (navigator.userAgent.search("Chrome") == -1) {
//    alert("Η εφαρμογή λειτουργεί σημαντικά πιο γρήγορα\n" +
//          "αν χρησιμοποιήσετε ως πλοηγό διαδικτύου τον Google Chrome.\n\n" +
//        "Ο εν λόγω browser διατίθεται δωρεάν από τον ιστότοπο:\n\n    https://www.google.com/chrome/\n\n")
//}

$.fn.getCursorPosition = function() {
    var el = $(this).get(0);
    var pos = 0;
    if('selectionStart' in el) {
        pos = el.selectionStart;
    } else if('selection' in document) {
        el.focus();
        var Sel = (<any>document).selection.createRange();
        var SelLength = (<any>document).selection.createRange().text.length;
        Sel.moveStart('character', -el.value.length);
        pos = Sel.text.length - SelLength;
    }
    return pos;
};

// Bootstrap popover is clipped inside ng-grid's table unless we do this:
$.fn.popover.defaults.container = 'body';

var BACKSPACE_KEY_CODE = 8;
function stupidWindowsBackspaceNavigation(event) {
    if (event.keyCode == BACKSPACE_KEY_CODE) {
        var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers
        var isInputTextLike = $(element).is('input') || $(element).is('textarea');
        if (element.readOnly === true || element.disabled === true)
            event.preventDefault();
        return isInputTextLike;
    }
    return navigateWithArrows(event);
}
$(document).keydown (stupidWindowsBackspaceNavigation);

// Scrollable jQuery tabs, from...
// http://stackoverflow.com/questions/6660370/how-to-make-jquery-ui-tabs-scroll-horizontally-if-there-are-too-many-tabs

var sbarSettings = {
    barheight: 31
};

// Scrollable jQuery tabs, from...
// http://stackoverflow.com/questions/6660370/how-to-make-jquery-ui-tabs-scroll-horizontally-if-there-are-too-many-tabs
$.fn.scrollabletab = function (options) {
    var ops = $.extend(sbarSettings, options);

    var ul = this.children('ul').first();
    var ulHtmlOld = ul.html();
    var tabBarWidth = $(this).width() - 52;
    ul.wrapInner('<div class="fixedContainer" style="height: ' + ops.barheight + 'px; width: ' + tabBarWidth + 'px; overflow: hidden; float: left;"><div class="moveableContainer" style="height: ' + ops.barheight + 'px; width: 5000px; position: relative; left: 0px;"></div></div>');
	

    ul.append('<div class="NextBtn" style="width: 20px; height: ' + (ops.barheight - 2) + 'px;"></div></div>');
    var rightArrow = ul.children().last();
    rightArrow.button({ icons: { secondary: "ui-icon ui-icon-carat-1-e" } });
    rightArrow.children('.ui-icon-carat-1-e').first().css('left', '2px');
	
    ul.append('<div class="PreviousBtn" style="width: 20px; height: ' + (ops.barheight - 2) + 'px;"></div>');
    var leftArrow = ul.children().last();
    leftArrow.button({ icons: { secondary: "ui-icon ui-icon-carat-1-w" } });
    leftArrow.children('.ui-icon-carat-1-w').first().css('left', '2px');

    var moveable = ul.find('.moveableContainer').first();
    leftArrow.click(function () {
        var offset = parseInt($('.TopFormArea').css('width')) - 52;
        var currentPosition = moveable.css('left').replace('px', '') / 1;

        if (currentPosition + offset >= 0) {
            moveable.stop().animate({ left: '0' }, 'fast');
        } else {
            moveable.stop().animate({ left: currentPosition + offset/2 + 'px' }, 'fast');
        }
    });

    rightArrow.click(function () {
        var offset = parseInt($('.TopFormArea').css('width')) - 52;
        var currentPosition = moveable.css('left').replace('px', '') / 1;
        var tabsRealWidth = 0;
        ul.find('li').each(function (index, element) {
            tabsRealWidth += $(element).width();
            tabsRealWidth += (parseInt($(element).css('margin-right').replace('px', '')) / 1);
            tabsRealWidth += 4; // from george.css, line 610
        });

        tabsRealWidth *= -1;

        if (currentPosition - offset > tabsRealWidth) {
            moveable.stop().animate({ left: currentPosition - offset/2 + 'px' }, 'fast');
        }
    });

    return this;
};

// Keyboard handling, based on the original JSF implementation by George.

$.fn.selectRange = function(start, end) {
    return this.each(function() {
        if (this.setSelectionRange) {
            //this.focus(); // This was causing round-about focus issues in Chrome
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

$.fn.selectAll = function() {
    var endSelection = 0;
    var text = $(this).val();
    if (text)
        endSelection = text.length;
    $(this).selectRange(0,endSelection);
}

$.fn.exists = function () {
    return this.length !== 0;
};

var ARROWLEFT_KEY_CODE =  37;
var ARROWUP_KEY_CODE   =  38;
var ARROWRIGHT_KEY_CODE = 39;
var ARROWDOWN_KEY_CODE =  40;
var ENTER_KEY_CODE = 13;
var TAB_KEY_CODE    = 9;
var ESC_KEY_CODE = 27;
var SPACE_KEY_CODE = 32;
var PGUP_KEY_CODE = 33;
var PGDN_KEY_CODE = 34;
var S_KEY_CODE  = 83;
var F1_KEY_CODE = 112;
var F2_KEY_CODE = 113;
var F08_KEY_CODE = 119;
var F09_KEY_CODE = 120;
var F10_KEY_CODE = 121;
var F12_KEY_CODE = 123;
var BACKSPACE_KEY_CODE = 8;
var ARROWKEY_CODES = [SPACE_KEY_CODE, ARROWLEFT_KEY_CODE, ARROWRIGHT_KEY_CODE, ARROWUP_KEY_CODE, ARROWDOWN_KEY_CODE, ENTER_KEY_CODE, ESC_KEY_CODE, PGUP_KEY_CODE, PGDN_KEY_CODE, S_KEY_CODE, TAB_KEY_CODE, F1_KEY_CODE, F2_KEY_CODE, F08_KEY_CODE, F12_KEY_CODE, BACKSPACE_KEY_CODE, F10_KEY_CODE, F09_KEY_CODE];

function navigateWithinForm(event) {
    var singleEnterPressed = (event.keyCode==ENTER_KEY_CODE || event.keyCode==TAB_KEY_CODE) && !event.shiftKey && !event.ctrlKey;
    var shiftEnterPressed = (event.keyCode==ENTER_KEY_CODE || event.keyCode==TAB_KEY_CODE) && event.shiftKey && !event.ctrlKey;
    var singleF10Pressed = (event.keyCode==F10_KEY_CODE ) && !event.shiftKey && !event.ctrlKey;
    var shiftF10Pressed = (event.keyCode==F10_KEY_CODE ) && event.shiftKey && !event.ctrlKey;

    var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers

    //if (singleF10Pressed || shiftF10Pressed) {
    //    onF10(event);
    //    return false;
    //}

    if (singleEnterPressed || shiftEnterPressed) {

        // see also http://stackoverflow.com/questions/4328381/jquery-set-focus-on-the-first-enabled-input-or-select-or-textarea-on-the-page
        //  $(element).closest('form').find(':input:enabled:visible:first').focus();
        var formElements = $(element).closest('.GridSearchArea, .FormArea').find("input:enabled:not([readonly]):not(':file'), select:enabled, button:enabled");

        var curIndex = formElements.index(element);
        var nextIndex = singleEnterPressed ? curIndex+1 : curIndex-1;
        var nextElement = formElements.slice(nextIndex,nextIndex+1);
        if (nextElement.parents('.GridSearchArea, .FormArea').exists()) {
            nextElement.focus();
            var txtElement = nextElement.filter('input:not([readonly])').not(':checkbox');
            var text = $(txtElement).attr('value');
            var endSelection = 0;
            if (text)
                endSelection = text.length;
            $(txtElement).selectRange(0,endSelection);
        }
        return false;
    }
    return true;
}

function navigateWithinTable(event) {
    var nextRow;
    var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers

    var singleEnterPressed = (event.keyCode==ENTER_KEY_CODE || event.keyCode==TAB_KEY_CODE) && !event.shiftKey && !event.ctrlKey;
    var shiftEnterPressed = (event.keyCode==ENTER_KEY_CODE || event.keyCode==TAB_KEY_CODE) && event.shiftKey && !event.ctrlKey;
    var ctrlEnterPressed = event.keyCode==ENTER_KEY_CODE && !event.shiftKey && event.ctrlKey;
    var ctrlShiftEnterPressed = event.keyCode==ENTER_KEY_CODE && event.shiftKey && event.ctrlKey;
    var singleF10Pressed = (event.keyCode==F10_KEY_CODE ) && !event.shiftKey && !event.ctrlKey;
    var shiftF10Pressed = (event.keyCode==F10_KEY_CODE ) && event.shiftKey && !event.ctrlKey;

    var addButton = $(element).parents('.NpTable').find('button > i.icon.new').parent();

    // var cloneButton = $(element).parents('.KeyboardNavigableTable').find('tr.ui-widget-content').filter('.ui-state-highlight').find('[id$=clone_id]');
    // On Shift + Ctrl+Enter clone row
    //if (ctrlShiftEnterPressed) {
    //    cloneButton.focus();
    //    cloneButton.trigger('click');
    //    return false;
    //}
    //if (singleF10Pressed || shiftF10Pressed) {
    //    onF10(event);
    //    return false;
    //}

    // On Ctrl+Enter create new table row
    if (ctrlEnterPressed || (event.keyCode == F09_KEY_CODE)) {
        addButton.focus();
        addButton.trigger('click');
        return false;
    }

    // var currentCell = $(element).closest('td');
    // if ($(element).hasClass('lovItem') ) 
    //      currentCell =  $(element).closest('tbody').closest('td');//$($(element).closest('tbody')[0]).closest('td');
    // var curCellIndex = currentCell.index();
    // var currentRow = currentCell.closest('tr');

    // var hasCursor = ($(element).hasClass('textBoxWithinTable') || $(element).hasClass('dateBoxWithinTable') || $(element).parents().first().hasClass('dateBoxWithinTable') || $(element).parent().hasClass('numberBoxWithinTable'));
    // var cellToMove=$([]);

    // if (event.keyCode==ARROWRIGHT_KEY_CODE || singleEnterPressed) {
    //     if ( hasCursor && (event.keyCode==ARROWRIGHT_KEY_CODE) &&              
    //          ($(element).getCursorPosition() != element.value.length)) 
    //         return true; //caret  is not at the end so let the default behavior to occure.

    //     cellToMove = currentCell.next('td');
    //     while(cellToMove.exists() &&  cellIsReadOnlyOrDisabled(cellToMove)) {
    //         cellToMove = cellToMove.next('td');
    //     }
    // } else if (event.keyCode==ARROWLEFT_KEY_CODE || shiftEnterPressed) {
    //     if ( hasCursor && (event.keyCode==ARROWLEFT_KEY_CODE) && ($(element).getCursorPosition() !== 0)) 
    //         return true; //caret  is not at the start so let the default behavior to occure.

    //     cellToMove = currentCell.prev('td');
    //     while(cellToMove.exists() &&  cellIsReadOnlyOrDisabled(cellToMove)) {
    //         cellToMove = cellToMove.prev('td');
    //     }
    //     //cellToMove is the column contaning the delete and duplicate button
    //     if (cellToMove.find('span.ui-icon-closethick').exists())
    //         return false;

    // } else if (event.keyCode==ARROWDOWN_KEY_CODE) {
    //     nextRow = currentRow.next('tr');
    //     cellToMove = nextRow.children('td').slice(curCellIndex,curCellIndex+1);
    // } else if (event.keyCode==ARROWUP_KEY_CODE) {
    //     nextRow = currentRow.prev('tr');
    //     cellToMove = nextRow.children('td').slice(curCellIndex,curCellIndex+1);
    // }
    // if (cellToMove.exists() && cellToMove.find('*').not('div.ui-dt-c').exists()) {
    //     if ($(element).parents().first().hasClass('dateBoxWithinTable')) {
    //         // close callendar div
    //         $("div.ui-datepicker").hide();
    //     }
    //     setFocus(cellToMove);
    // } else {
    //     if (event.keyCode==ARROWRIGHT_KEY_CODE || singleEnterPressed) {
    //         var groupId = $(element).parents('div.KeyboardNavigableTable').first().attr('id').split(':').slice(-1)[0].replace('_tableId','');
    //         var itemRegion = $(element).parents('form').find('.childof'+groupId);
    //         if (itemRegion.exists()) {
    //             focusFirstChild(itemRegion);
    //         }
    //     }
    // }
    return true;
}

function navigateWithArrows(event) {
    var lovItemText;
    if (ARROWKEY_CODES.indexOf(event.keyCode) <0 )
        return true;

    // The HTML element that received the key down event
    var element = $(event.target || event.srcElement); // srcElement in Internet Explorer, target in other browsers

    if (element.is('textarea'))
        return true;

    if (event.keyCode==SPACE_KEY_CODE) {
        if (element.hasClass('closedLovItem')) {
            element.trigger('click');
            return false;
        }
        lovItemText = element.closest('input.closedLovItem').closest('tbody').find('a.closedLovItem');
        if (lovItemText.exists()) {
            lovItemText.click();
            return false;
        }
        return true;
    }

    if (event.keyCode==F12_KEY_CODE) {
        if (element.hasClass('ui-commandlink')) {
            element.trigger('click');
            return false;
        }
        lovItemText = element.closest('input.lovItem').closest('tbody').find('a.lovItem');
        if (lovItemText.exists()) {
            lovItemText.click();
            return false;
        }
    }

    if (event.keyCode==S_KEY_CODE) {
        if (event.ctrlKey) {
            var saveButton = element.parents('body').find("[id$='saveBtn_id']").first();
            saveButton.focus();
            saveButton.trigger('click');
            element.focus();
            return false;
        } else {
            return true;
        }
    }

    if (event.keyCode == F08_KEY_CODE) {
        var saveButton = element.parents('form').last().find("[id$='commitBtn']").first();
        saveButton.focus();
        saveButton.trigger('click');
    }

    if (element.is('select')) {
        if (    (event.keyCode==ARROWDOWN_KEY_CODE) ||
                (event.keyCode==ARROWUP_KEY_CODE)   ||
                (event.keyCode==ARROWLEFT_KEY_CODE) ||
                (event.keyCode==ARROWRIGHT_KEY_CODE))
            return true;
    }

    if (element.closest('.GridSearchArea, .FormArea').exists())
        return navigateWithinForm(event);

    if (element.closest('.NpTable').exists())
        return navigateWithinTable(event);

    if (event.keyCode==F12_KEY_CODE) {
        return false;
    }

    return true;
}
