declare module goog {
    export class array<T> {
        forEach(action:(e:T)=>void);
    }

    module events {

        var EventType : {
            CLICK:any;
            CHANGE:any;
            DBLCLICK:any;
            MOUSEUP:any;
            MOUSEMOVE:any;
        };
    }
}

declare module ol {

    function inherits(arg1: any, arg2: any);

    export class Observable {
        constructor();
        static unByKey(key: any): void;
    }


    export class Collection<T> {
        constructor(
            arg?: Array<T>   
            );
        push(arg: T): void;
        remove(arg: T): void;
        getLength(): number;
        getArray(): Array<T>;
        item(arg:number):T;
        forEach(callback:(arg1:T, ...rest:T[])=>void):void;
        extend(bases:any[]):ol.Collection<T>;
        clear(): void;
        removeAt(arg: number): T;
        insertAt(arg1: number, arg2: ol.layer.Layer): T;
        setAt(arg1: number, arg2: ol.layer.Layer): T;
        on(s:string, f:(evt:any)=>void);
    }

    export class Overlay {
        constructor(
            config: {
                element: any
            });
        setPosition(pos:ol.Coordinate);
        getElement():any;
    }



    export class Map {
        constructor(
            config: {
                target: string;
                layers: ol.layer.Tile[];
                view: ol.View;
                interactions: ol.interaction.Interaction;
                controls: ol.Collection<ol.control.Control>;
            });
        addInteraction(arg: ol.interaction.Interaction): void;
        addLayer(arg: ol.layer.Layer): void;
        removeLayer(arg: ol.layer.Layer): ol.layer.Layer;
        removeInteraction(arg:ol.interaction.Interaction):void;
        on(evtName: string, callback: (evt: any) => void): void;
        once(evtName: string, callback: (evt: any) => void): void;
        forEachFeatureAtPixel(
            pixel:number[],
            callback:(feature:ol.Feature, layer:ol.layer.Layer)=>any,
            arg:any,
            filterLayer:(layer:ol.layer.Layer)=>boolean):any;
        getViewport():any;
        getEventPixel(evt:any):any;
        getCoordinateFromPixel(p:ol.Pixel):ol.Coordinate;
        getPixelFromCoordinate(c:ol.Coordinate):ol.Pixel;
        getView(): ol.View;
        getSize(): ol.Size;
        setSize(arg: ol.Size);
        renderSync();
        addOverlay(ov: ol.Overlay);
        getLayers(): ol.Collection<ol.layer.Layer>;
        getInteractions(): ol.Collection<any>;
        updateSize();
    }

    export class Pixel {}
    export class Coordinate {}
    export class Size {}
    export class Extent {
        extend(anotherExtent: ol.Extent): ol.Extent;
        getCenter(): ol.Coordinate;
    } 

    export class View {
        constructor(
            config: {
                center: number[];
                zoom: number;
                maxZoom?: number;
                resolutions?: number[];
                maxResolution?: number;
                minResolution?: number;
                projection?: ol.proj.Projection;
            });
        setCenter(center:ol.Coordinate);
        setZoom(zoomLevel: number);
        //fit(extent: ol.geom.Geometry | ol.Extent, size: ol.Size);
        fit(extent: any, size: ol.Size);
        getResolution(): number;
        calculateExtent(arg: ol.Size): ol.Extent;
        on(evtName:string, callback:(evt:any)=>void):void;
    }

    export class Feature {
        constructor(
            config?: {
                geometry?: ol.geom.Geometry;
                name?: string;
            });
        setGeometryName(arg: string): void;
        set(arg: string, any);
        get(arg: string);
        getGeometryName():string;
        setGeometry(arg:ol.geom.Geometry):void;
        getGeometry(): ol.geom.Geometry;
        setStyle(arg:any): void;
        clone():ol.Feature;
        entity:any;
    }

    module style {
        export class Style {
            constructor(
                config: {
                    stroke?: ol.style.Stroke;
                    fill?: ol.style.Fill;
                    image?: ol.style.Circle;
                    geometry?: (f) => any;
                    text?: ol.style.Text
                });
        }
        export class Stroke {
            constructor(
                config: {
                    color: string;
                    lineDash?: number[];
                    width: number;
                });
        }
        export class Fill {
            constructor(
                config: {
                    color: string;
                });
        }
        export class Circle {
            constructor(
                config: {
                    radius: number;
                    fill: ol.style.Fill;
                    stroke?: ol.style.Stroke;
                });
        }
        export class Icon {
            constructor(
                config: {
                    anchor: number[];
                    anchorXUnits:string;
                    anchorYUnits:string;
                    opacity: number;
                    src:string;
                });
        }
        export class Text {
            constructor(
                config: {
                    textAlign?: string;
                    textBaseline?: string;
                    font?: string;
                    text?: string;
                    fill?: ol.style.Fill;
                    stroke?: ol.style.Stroke;
                    offsetX?: number;
                    offsetY?: number;
                    rotation?: number;
                });
        }
        export class RegularShape {
            constructor(
                config: {
                    fill?: ol.style.Fill;
                    points: number;
                    radius?: number;
                    radius1?: number;
                    radius2?: number; 
                    angle?: number;
                    snapToPixel?: number;
                    stroke?: ol.style.Stroke;
                    rotation?: number;
                });
        }
    }

    module layer {
        export class Layer {
            getVisible(): boolean;
            setVisible(boolean): void;
            getSource(): ol.source.Vector;
            getOpacity(): number;
            setOpacity(n: number): void;
            setMap(arg: ol.Map): void;
            setZIndex(n: number): void;
            get(arg: string): any;
        }
        export class Tile extends Layer {
            constructor(source: ol.source.MapQuest);
        }
        export class Vector extends Layer {
            constructor(
                config: {
                    source: any;
                    style?: ol.style.Style;
                    visible?: boolean;
                    layerId?: string;
                });
            setStyle(styleFunc: (feature: any, resolution: any) => ol.style.Style[]);
            getStyle() : ol.style.Style;
            set(arg: string, any);
        }
    }

    module source {
        export class MapQuest {
            constructor(
                config: {
                    layer: string;
                }
            );
        }
        export class OSM {
            constructor(
                config: {
                    crossOrigin?: string;
                    url?: string;
                }
            );
        }
        export class Stamen {
            constructor(
                config: {
                    layer: string;
                    maxZoom?: number;
                    minZoom?: number;
                    crossOrigin?: string;
                    url?: string;
                }
            );
        }
        export class BingMaps {
            constructor(
                config: {
                    imagerySet: string;
                    crossOrigin: string;
                    key: string;
                    maxZoom: number;
                }
            );
        }
        export class XYZ {
            constructor(
                config: {
                    url: string;
                    crossOrigin?: string;
                    title?: string;
                    type?: string;
                    maxZoom?: number;
                }
            );
        }
        export class TileWMS {
            constructor(
                config: {
                    url: string;
                    params: any;
                    serverType: string;
                    extent?: any;
                    projection: any;
                    tileGrid: ol.tilegrid.WMTS;
                    crossOrigin?: string;
                    tileLoadFunction?: any;
                });
        }
        export class Vector {
            constructor(
                config?: {
                    features?: any;
                    useSpatialIndex?: boolean;
                }
            );
            addFeatures(arg: goog.array<ol.Feature>) : void;
            addFeature(arg: ol.Feature) : void;
            readFeatures(geojson: any) : goog.array<ol.Feature> ;
            removeFeature(arg:ol.Feature):void;
            getFeatures(): ol.Feature[];
            getFeaturesCollection(): ol.Collection<ol.Feature>
            clear();
            getExtent(): ol.Extent;
            getFeaturesInExtent(arg: ol.Extent): ol.Feature[];
            on(event: string, f: (evt: any) => void);
            unByKey(event);
        }
        export class GeoJSON {
        }
        export class FormatVector extends Vector {
        }
        export class ServerVector extends FormatVector {
            constructor(
                config: {
                    format: any;
                    loader: (extent, resolution, projection) => any;
                    strategy: any;
                    projection?: string;
                }
            );
        }
    }

    module control {
        function defaults(opt_options): ol.Collection<ol.control.Control>;
        export class Control { }
        export class ZoomSlider {}
        export class Zoom {}
        
        export class FullScreen {
            constructor(
                config?: {
                    source?: Element
                    target?: Element;
                }
            );
        }
        export class ScaleLine {}
    }
    module proj {
        function transform(vec:number[], s1:string, s2:string): number[];
        function get(s:string): Projection;
        export class Projection {
            constructor(
                config?: {
                    code?: string
                    units?: string;
                }
            );
        }
    }
    module interaction {
        function defaults(opt_options): ol.Collection<ol.interaction.Interaction>;
        class Interaction {}
        class DragZoom extends Interaction {
            constructor(config: {
                 condition?: any;
            });
            setActive(arg: boolean);
            getActive(): boolean;
            on(event: string, f: (evt: any) => void);
        }
        class DragAndDrop extends Interaction {
            constructor(config: {
                formatConstructors: Array<any>
            });
            on(event:string, f:(evt:any)=>void);
        }
        class Select extends Interaction {
            constructor(
                config: {
                    condition: any;
                    style: any;//TODO here should be ol.style.Style | ol.style.Style[] | ol.style.StyleFunction  with the latest verion of typescript it should be valid
                });
            getFeatures(): ol.Collection<ol.Feature>;
            getLayer(arg: ol.Feature): ol.layer.Vector;
            featureOverlay_: ol.layer.Vector;
        }
        class Modify {
            constructor(
                config: {
                    features: ol.Collection<ol.Feature>;
                    deleteCondition: (event:any)=>boolean;
                });
            setProperties(values, opt_silent?): any; boolean;
            setActive(arg: boolean);
            getActive(): boolean;
            on(evtName: string, f: (evt: any) => void);
        }
        class Draw {
            constructor(
                config: {
                    features?: ol.Collection<ol.Feature>;
                    type: any;
                    source?: ol.source.Vector;
                    style?: any;
                    geometryFunction?: (f, g) => any;
                    condition?: any;
                    freehandCondition?: any;
                });
            sketchFeature_:ol.Feature;
            updateSketchFeatures_();
            mode_:string;
            sketchPolygonCoords_: any;
            sketchCoords_: any;
            on(evtName:string, callback:(a:any, b:any, c:any) => void);
            abortDrawing_(): any;
            setActive(arg: boolean);
            getActive(): boolean;
        }
        class Snap {
            constructor(
                config: {
                    features?: ol.Collection<ol.Feature>;
                    source?: ol.source.Vector;
                    pixelTolerance?: number;
                });
            features_: ol.Collection<any>;
            setActive(arg: boolean);
            getActive(): boolean;
            addFeature(arg: ol.Feature);
        }
        //this is a custom interaction not icluded in official ol3.js 
        //use to split linear rings in the point clicked
        //sources for interaction included within geomtery.js file.
        class Split {
            constructor(
                config: {
                    sources?: ol.source.Vector;
                    features?: ol.Collection<ol.Feature>;
                    snapDistance?: number;
                    cursor?: string;
                    featureStyle?: ol.style.Style;
                    sketchStyle?: ol.style.Style[];
                    tolerance?: number;
                });
            isFirstPoint: boolean;
            listenerBefore: any;
            listenerAfter: any;
            overlayLayer_: ol.layer.Vector;
            setActive(arg: boolean);
            getActive(): boolean;
            on(evtName: string, f: (evt: any) => void);
        }

        var DrawMode: {
            POINT: string;
            LINE_STRING: string;
            LINEAR_RING: string;
            POLYGON: string;
            MULTI_POINT: string;
            MULTI_LINE_STRING: string;
            MULTI_POLYGON: string;
            GEOMETRY_COLLECTION: string;
            CIRCLE: string;
        };
    }
    module format {
        class GeoJSON {
            constructor(config?: {
                defaultDataProjection: ol.proj.Projection;
            });
            writeGeometry(arg:ol.geom.Geometry):any;
            readGeometry(geojson:any):any;
            writeFeature(arg:ol.Feature):string;
        }
        class WKT {
            readGeometry(arg:string):ol.geom.Geometry;
            readFeature(arg:string):ol.Feature;
        }
        class GPX {
            readFeature(arg: string): ol.Feature;
            readFeatures(arg: string): ol.Feature[];
            writeFeature(arg: ol.Feature): string;
        }
        class KML {
            readFeature(arg: string): ol.Feature;
            readFeatures(arg: string): ol.Feature[];
            writeFeature(arg: ol.Feature): string;
        }
        class GML {
            constructor(config?: {
                featureNS: string;
            });
            readFeatures(arg: string): ol.Feature[];
            writeFeatures(arg: ol.Feature[], arg2: any): string;
            writeFeaturesNode(arg: ol.Feature[], arg2: any): string;
        }
        class GML3 {
            readFeatures(arg: string): ol.Feature[];
            writeFeatures(arg: ol.Feature[]): string;
            writeFeaturesNode(arg: ol.Feature[]): string;
            writeGeometryNode(arg1: ol.geom.Geometry, arg2: any): string;
        }
    }
    module tilegrid {
        class XYZ {
            constructor(
                config: {
                    maxZoom: number
                }
            );
        }
        class WMTS {
            constructor(
                config: {
                    origin: number[];
                    tileSize: number[];
                    resolutions: number[];
                    matrixIds: number[];
                    maxExtent: number[];
                }
            )
        }
    }

    module loadingstrategy {
        function createTile(arg: ol.tilegrid.XYZ);
    }

    module events {
        module condition {
            function never(e:any):boolean;
            function shiftKeyOnly(e: any): boolean;
            function altKeyOnly(e: any): boolean;
            function singleClick(e:any):boolean;
        }
    }

    module geom {
        var GeometryType: {
            POINT: string;
            LINE_STRING: string;
            LINEAR_RING: string;
            POLYGON: string;
            MULTI_POINT: string;
            MULTI_LINE_STRING: string;
            MULTI_POLYGON: string;
            GEOMETRY_COLLECTION: string;
            CIRCLE: string;
            TWOPOINTSPLIT: string;
        };


        class Geometry {
            transform(arg1: string, arg2: string): void;
            transform(arg1: ol.proj.Projection, arg2: ol.proj.Projection): ol.geom.Geometry;
            flatCoordinates:number[];
            getCoordinates():number[];
            setCoordinates(coords:ol.Coordinate);
            getExtent():ol.Extent;
            clone(): ol.geom.Geometry;
            appendLinearRing(linearRing: ol.geom.LinearRing);
            getType(): string;
            getLinearRing(index: number): ol.geom.LinearRing;
            getLinearRings(): ol.geom.LinearRing[];
            getClosestPoint(point: ol.Coordinate, opt_closestPoint?: ol.Coordinate): ol.Coordinate;
            getArea(): number;
            getInteriorPoint(): ol.geom.Point;//for polygons
            getInteriorPoints(): ol.geom.MultiPoint;//for multipolygons
            getPolygons(): ol.geom.Polygon[];
            
        }
        class LinearRing extends Geometry {
            constructor(coordinates: ol.Coordinate[]);
            getArea(): number;
        }
        class LineString extends Geometry {
            constructor(coordinates: ol.Coordinate[]);
            getLength(): number;
        }
        class Polygon extends Geometry {
            constructor(coordinates: ol.Coordinate[]);
            getArea(): number;
            appendLinearRing(linearRing: ol.geom.LinearRing);
            getCoordinates(): number[];
            getType(): string;
            getInteriorPoint(): ol.geom.Point;
        }
        class Point extends Geometry {
            constructor(coordinates:ol.Coordinate);
        }
        class MultiPoint extends Geometry {
            constructor(coordinates:ol.Coordinate);
        }
        class MultiPolygon extends Geometry {
            constructor(coordinates: ol.Coordinate);
            getPolygons(): ol.geom.Polygon[];
        }

       
    }

    module coordinate {
        function squaredDistanceToSegment(a:any, b:any):number;
        function closestOnSegment(a:any, b:any):ol.Coordinate;
        function squaredDistance(a:ol.Coordinate, b:ol.Coordinate):number;
    }

    module extent {
        function boundingExtent(a: any);
        function getCenter(a: any);
    }
}

//declare module jsts {
//    module io {
//        class OL3Parser {
//            read(arg: ol.geom.Geometry);
//        }
//    }
//    module geom {
//        class Geometry {
//            IsSimple(): boolean;
//        }
//    }

//    module operation {
//        //IsSimpleOp(Geometry): boolean;
//        export function IsSimpleOp(Geometry): any;
//    }
//}

declare function proj4(fromProj: string, toProj: string, input: number[]): number[];
declare function proj4(fromProj:string, toProj:string, input:ol.Coordinate):number[];
