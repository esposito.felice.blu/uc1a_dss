﻿
/// <reference path="NpMapLayers.ts"/>

module NpGeoCustControls {

    // ==================================================================
    //   ____  _      ____
    //  / __ \| |    |___ \
    // | |  | | |      __) |
    // | |  | | |     |__ <
    // | |__| | |____ ___) |
    //  \____/|______|____/
    //
    //
    //   _____                                             _
    //  / ____|                                           | |
    // | |     ___  _ __ ___  _ __   ___  _ __   ___ _ __ | |_ ___
    // | |    / _ \| '_ ` _ \| '_ \ / _ \| '_ \ / _ \ '_ \| __/ __|
    // | |___| (_) | | | | | | |_) | (_) | | | |  __/ | | | |_\__ \
    //  \_____\___/|_| |_| |_| .__/ \___/|_| |_|\___|_| |_|\__|___/
    //                       | |
    //                       |_|
    //
    // ===================================================================

    ////////////////////////////////////////////////////////////////////////////////
    // Javascript classes, re-used amongst map instances to create custom components
    // These are all custom ol3 controls. For the time being each button is an ol.control
    // But may create toolbar controls and populate the toolbars with buttons
    // In that way only a few ol.controls might be instatitated and then attach 
    // the buttons to them.
    ////////////////////////////////////////////////////////////////////////////////

    // Common function for top-right and bottom-right dropdown lists

    var makeSelectElement = (layers: NpGeoLayers.BaseLayer[], isVector: boolean) => {
        var selectElem = document.createElement('select');
        if (isVector) {
            selectElem.multiple = true;
        } else {
            selectElem.className = "ui-widget ui-corner-all";
        }
        var idx = 0;
        layers.forEach(function (l) {
            if (l.isVector === isVector) {
                var option = document.createElement('option');
                option.value = idx.toString();
                option.text = l.label;
                option.selected = l.isVisible;
                selectElem.appendChild(option);
                idx++;
            }
        });
        if (!isVector) {
            var option = document.createElement('option');
            option.value = idx.toString();
            option.text = "Κανένα";
            option.selected = false;
            selectElem.appendChild(option);
        }
        return selectElem;
    }

    // Choosing tile layer

    export var TileSelectorControl = function (opt_options) {
        var options = opt_options || { layers: [] };
        var layers: NpGeoLayers.BaseLayer[] = options.layers;
        var selectElem = makeSelectElement(layers, false);
        selectElem.id = 'selectTile';

        var this_ = this;
        var handleTileChange = function (e) {
            //e.preventDefault(); // prevent #tile-toggle from appending to url
            var selectedLayer = layers[selectElem.selectedIndex];
            layers.forEach(function (l: NpGeoLayers.BaseLayer) {
                if (!l.isVector) {
                    var layerLabel = l.label;
                    var layerVar = l.layer;
                    // For tile layers, clear the rest, and setVisible the clicked one
                    if (layerLabel === selectedLayer.label) {
                        layerVar.setVisible(true);
                        l.isVisible = true;
                        //layerVar.setOpacity(0.5);
                    } else {
                        layerVar.setVisible(false);
                        l.isVisible = false;
                    }
                }
            });
        };

        this.getActiveTileLayer = function () {
            var activeLayer: ol.layer.Layer;
            layers.forEach(function (l: NpGeoLayers.BaseLayer) {
                if (!l.isVector && l.isVisible) {
                    activeLayer = l.layer;
                }
            });
            return activeLayer;
        }

        selectElem.addEventListener('change', handleTileChange, false);

        var element = document.createElement('div');
        element.className = 'tile-toggle ol-unselectable';
        element.appendChild(selectElem);

        (<any>ol).control.Control.call(this, {
            element: element,
            target: options.target
        });
    };
    ol.inherits(TileSelectorControl, ol.control.Control);

    // Choosing vector layer - the dropup checkboxes approach

    var LayerToggleControl = function (opt_options) {
        var options = opt_options || {};
        var layers: NpGeoLayers.BaseLayer[] = options.layers;
        var selectElem = makeSelectElement(layers, true);
        selectElem.id = 'selectLayer';
        var vectorLayers: NpGeoLayers.BaseLayer[] = [];
        layers.forEach(function (l) {
            if (l.isVector) {
                vectorLayers.push(l);
            }
        });

        var this_ = this;
        var handleLayerToggle = function (e) {
            //e.preventDefault(); // prevent #tile-toggle from appending to url
            var selectedLayer = layers[selectElem.selectedIndex];
            layers.forEach(function (l) {
                if (l.isVector) {
                    var layerLabel = l.label;
                    var layerVar = l.layer;
                    // For tile layers, clear the rest, and setVisible the clicked one
                    if (layerLabel === selectedLayer.label) {
                        // For vector layers, toggle!
                        layerVar.setVisible(!l.isVisible);
                        l.isVisible = !l.isVisible;
                        console.log("setting layer visibility");
                    }
                }
            });
            //this_.getMap()...
        };

        selectElem.addEventListener('change', handleLayerToggle, false);

        var element = document.createElement('div');
        element.className = 'layer-toggle ol-unselectable';
        element.appendChild(selectElem);

        ol.control.Control.call(this, {
            element: element,
            target: options.target
        });

        var waitForCanvasAndThenCreateMultiselect = () => {
            var elem = $('canvas');
            if (elem.length) {
                var parId = '#' + elem.parent().parent()[0].id;
                (<any>$(selectElem)).multiselect({
                    header: false,
                    minWidth: options.layerSelectorWidthInPx,
                    height: 'auto',
                    position: {
                        my: 'left bottom',
                        at: 'left top'
                    },
                    appendTo: parId,
                    click: function (event, ui) {
                        var idx = parseInt(ui.value, 10);
                        vectorLayers[idx].layer.setVisible(ui.checked);
                        vectorLayers[idx].isVisible = ui.checked;
                    }
                });
            } else {
                setTimeout(waitForCanvasAndThenCreateMultiselect, 500);
            }
        }
        setTimeout(waitForCanvasAndThenCreateMultiselect, 500);
    };
    ol.inherits(LayerToggleControl, ol.control.Control);

    // Common Javascript class for all buttons

    export var CommonBtnControl = function (opt_options, this_) {
        var options = opt_options || {
            state: {
                bottomOffset: 5,
                leftSide: false
            }
        };
        var state = options.state;
        var bottomOffset = state.bottomOffset;
        state.bottomOffset += 30;

        var anchor = document.createElement('a');
        anchor.id = options.id;
        anchor.href = '#' + options.id;
        if (!isVoid(options.imgPath) || (!isVoid(options.letter) && options.letter.slice(0, 5) === 'data:')) {
            var imgPath;
            if ((!isVoid(options.letter) && options.letter.slice(0, 5) === 'data:'))
                imgPath = options.letter;
            else {
                imgPath = "/" + options.grp.$scope.globals.appName + "/img/";
                imgPath += options.imgPath;
            }
            var inside = '<img id="ctl00_XXX" src="' + imgPath;
            inside += '" width="24" height="24" style="border: 0; margin: 0px 0px" /> ';
            inside += '<span id="XXX"></span>';
            anchor.innerHTML = inside;
        }
        else if (!isVoid(options.letter)) {
            anchor.innerHTML = '&nbsp;' + options.letter + '&nbsp;';
        }
        else
            alert("No imgPath and no letter passed to custom button class...");
        anchor.title = options.title;

        var handleClickBtn = function (e) {
            e.preventDefault();
            if (this.disabled)
                return;
            options.onclick(this_, e);
        };

        this_.enable = (justHide: boolean = true) => {
            $(anchor).parent().show();
            $(anchor).prop("disabled", false);
            $(anchor).removeClass("NpMapDisabledBtn");
            if (!isVoid(options.imgPath)) {
                var img = $(anchor).find('img');
                var src = img.attr('src');
                img.attr('src', src.replace('-disabled.png', '.png'));
            }
        };

        this_.disable = (justHide: boolean = true) => {
            if (justHide)
                $(anchor).parent().hide();
            $(anchor).prop("disabled", true);
            $(anchor).addClass("NpMapDisabledBtn");
            if (!isVoid(options.imgPath)) {
                var img = $(anchor).find('img');
                var src = img.attr('src');
                if (src.indexOf('-disabled.png') === -1)
                    img.attr('src', src.replace('.png', '-disabled.png'));
            }
        };

        this_.allowClicks = function () {
            // Used by the toggle buttons - they want the disable() functionality,
            // (to change the image bitmap, etc), but need clicks to go through,
            // so they do:
            //
            //    self.disable();
            //    self.allowClicks();
            //
            $(anchor).prop("disabled", false);
        };

        this_.setHoverText = function (text: string) {
            anchor.title = text;
        };

        this_.updateState = function () {
            //
            // OBSOLETE: I tried disabling the buttons to indicate absence of
            // focus.  It worked, but then again, people mostly use the mouse.
            // When Stamatis asks for why F2 or 'S' doesn't work, I will tell
            // him to grep for his name :-)
            //
            // if ($('#' + options.npMapThis.divId).is(':focus'))
            //     options.updateState(this_);
            // else
            //     this_.disable();
            options.updateState(this_);
        };

        anchor.addEventListener('click', handleClickBtn, false);
        anchor.addEventListener('touchstart', handleClickBtn, false);
        anchor.className = 'NpMapBtnAnchor';

        var element = document.createElement('div');
        var elementjq = $(element);
        elementjq.css('bottom', bottomOffset + 'px');
        element.className = 'NpMapBtn ol-unselectable';

        if (state.leftSide)
            elementjq.css('right', '8px');
        else
            elementjq.css('left', '6px');

        element.appendChild(anchor);

        ol.control.Control.call(this_, {
            element: element,
            target: options.target
        });

    };

    // The Javascript classes used by the buttons.
    // They all delegate to map-provided constructors,
    // which eventually call CommonBtnControl.

   export var VectorLayerSelectorControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructVectorLayerButton(options);
    };
    ol.inherits(VectorLayerSelectorControl, ol.control.Control);

   export var EditGeometryControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructEditButton(options);
    };
    ol.inherits(EditGeometryControl, ol.control.Control);

    class MyGenericBtn extends ol.control.Control {
        private anchor: HTMLAnchorElement;
        constructor(
            public id: string,
            public appName: string,
            public imagePath: string,
            public letter: string,
            public title: string,
            public onClick: () => void,
            public updateState: (btn: MyGenericBtn) => void) {
            super();
            var state = {
                bottomOffset: 5,
                leftSide: false
            }
            var bottomOffset = state.bottomOffset;
            state.bottomOffset += 30;

            this.anchor = document.createElement('a');
            this.anchor.id = id;
            this.anchor.href = '#' + id;
            if (!isVoid(imagePath) || (!isVoid(letter) && letter.slice(0, 5) === 'data:')) {
                var imgPath;
                if ((!isVoid(letter) && letter.slice(0, 5) === 'data:'))
                    imgPath = letter;
                else {
                    imgPath = "/" + appName + "/img/";
                    imgPath += imagePath;
                }
                var inside = '<img id="ctl00_XXX" src="' + imgPath;
                inside += '" width="24" height="24" style="border: 0; margin: 0px 0px" /> ';
                inside += '<span id="XXX"></span>';
                this.anchor.innerHTML = inside;
            }
            else if (!isVoid(letter)) {
                this.anchor.innerHTML = '&nbsp;' + letter + '&nbsp;';
            }
            else
                alert("No imgPath and no letter passed to custom button class...");
            this.anchor.title = title;

            var handleClickBtn = function (e) {
                e.preventDefault();
                if (this.disabled)
                    return;
                onClick();
            };


            this.anchor.addEventListener('click', handleClickBtn, false);
            this.anchor.addEventListener('touchstart', handleClickBtn, false);
            this.anchor.className = 'NpMapBtnAnchor';

            var element = document.createElement('div');
            var elementjq = $(element);
            elementjq.css('bottom', bottomOffset + 'px');
            element.className = 'NpMapBtn ol-unselectable';

            if (state.leftSide)
                elementjq.css('right', '8px');
            else
                elementjq.css('left', '6px');

            element.appendChild(this.anchor);

            ol.control.Control.call(this, {
                element: element,
                target: undefined // options.target
            });

        }



        public enable(justHide: boolean = true) {
            $(this.anchor).parent().show();
            $(this.anchor).prop("disabled", false);
            $(this.anchor).removeClass("NpMapDisabledBtn");
            if (!isVoid(this.imagePath)) {
                var img = $(this.anchor).find('img');
                var src = img.attr('src');
                img.attr('src', src.replace('-disabled.png', '.png'));
            }
        }

        public disable(justHide: boolean = true) {
            if (justHide)
                $(this.anchor).parent().hide();
            $(this.anchor).prop("disabled", true);
            $(this.anchor).addClass("NpMapDisabledBtn");
            if (!isVoid(this.imagePath)) {
                var img = $(this.anchor).find('img');
                var src = img.attr('src');
                if (src.indexOf('-disabled.png') === -1)
                    img.attr('src', src.replace('.png', '-disabled.png'));
            }
        }

        public allowClicks() {
            // Used by the toggle buttons - they want the disable() functionality,
            // (to change the image bitmap, etc), but need clicks to go through,
            // so they do:
            //
            //    self.disable();
            //    self.allowClicks();
            //
            $(this.anchor).prop("disabled", false);
        }

        public setHoverText(text: string) {
            this.anchor.title = text;
        }



    }



    export var RevertGeometryControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructRevertButton(options);
    };
    ol.inherits(RevertGeometryControl, ol.control.Control);

    export var SavePDFControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructSaveButton(options);
    };
    ol.inherits(SavePDFControl, ol.control.Control);

    export var MeasureDistanceControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructMsreButton(options);
    }
    ol.inherits(MeasureDistanceControl, ol.control.Control);

    export var SelectGeometryControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructSlctButton(options);
    }
    ol.inherits(SelectGeometryControl, ol.control.Control);

    export var DrilHoleControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructDrilHoleButton(options);
    }
    ol.inherits(DrilHoleControl, ol.control.Control);

    export var RemoveGeometryControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructRmveButton(options);
    }
    ol.inherits(RemoveGeometryControl, ol.control.Control);

    export var ZoomToPolyControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructZoomButton(options);
    }
    ol.inherits(ZoomToPolyControl, ol.control.Control);

    export var JumpToCoordControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructJumpButton(options);
    }
    ol.inherits(JumpToCoordControl, ol.control.Control);

    export var SnapControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructSnapButton(options);
    }
    ol.inherits(SnapControl, ol.control.Control);
    //@p.tsagkis snap interaction start
    //export var snapCntrlInt = function (opt_options) {
    //    var options = opt_options || {};
    //    options.controlThis = this;
    //    var npMapThis: NpGeo.NpMap = options.npMapThis;
    //    npMapThis.constructSnapIntButton(options);
    //}
   // ol.inherits(snapCntrlInt, ol.control.Control);
    //@p.tsagkis snap interaction end


    // var InfoToggleControl = function(opt_options) {
    //     var options = opt_options || {};
    //     options.controlThis = this;
    //     var npMapThis:NpMap = options.npMapThis;
    //     npMapThis.constructInfoToggleButton(options);
    // }
    // ol.inherits(InfoToggleControl, ol.control.Control);

    // var PolyHoleMakerControl = function(opt_options) {
    //     var options = opt_options || {};
    //     options.controlThis = this;
    //     var npMapThis:NpMap = options.npMapThis;
    //     npMapThis.constructPolyHoleMakerButton(options);
    // }
    // ol.inherits(PolyHoleMakerControl, ol.control.Control);

    // var CropControl = function(opt_options) {
    //     var options = opt_options || {};
    //     options.controlThis = this;
    //     var npMapThis:NpMap = options.npMapThis;
    //     npMapThis.constructCropButton(options);
    // }
    // ol.inherits(CropControl, ol.control.Control);

    export var HoleRemoverControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructHoleRemoverButton(options);
    }
    ol.inherits(HoleRemoverControl, ol.control.Control);

    export var IntersectControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructIntersectButton(options);
    }
    ol.inherits(IntersectControl, ol.control.Control);

    export var MergeControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructMergeButton(options);
    }
    ol.inherits(MergeControl, ol.control.Control);

    export var CopyControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructCopyButton(options);
    }
    ol.inherits(CopyControl, ol.control.Control);

    export var PasteControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructPasteButton(options);
    }
    ol.inherits(PasteControl, ol.control.Control);

    export var RefreshControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructRefreshButton(options);
    }
    ol.inherits(RefreshControl, ol.control.Control);

    export var SearchControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructSearchButton(options);
    }
    ol.inherits(SearchControl, ol.control.Control);

    export var CoordSpanControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructSpanControl(options);
    }
    ol.inherits(CoordSpanControl, ol.control.Control);

    export var DigiInfoControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructDigiInfoControl(options);
    }
    ol.inherits(DigiInfoControl, ol.control.Control);

    export var legendControl = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        var npMapThis: NpGeo.NpMap = options.npMapThis;
        npMapThis.constructLegendControl(options);
    }
    ol.inherits(legendControl, ol.control.Control);

    export var CustomBtnControl  = function (opt_options) {
        var options = opt_options || {};
        options.controlThis = this;
        CommonBtnControl(options, this);
    }
    ol.inherits(CustomBtnControl, ol.control.Control);
}