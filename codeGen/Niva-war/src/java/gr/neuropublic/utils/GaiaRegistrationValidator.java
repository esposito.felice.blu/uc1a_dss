package gr.neuropublic.utils;


import gr.neuropublic.base.Property;
import gr.neuropublic.jsf.util.JsfUtil;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class GaiaRegistrationValidator {
    
    /**
     * Εκτελούμε τα validations με τα registrations web services
     *
     * @param tin
     * @return
     * @throws java.io.IOException
     * @throws org.json.simple.parser.ParseException
     */
    public static boolean validateUserRegistration(String tin){

        Long userId;//Ελέγχει στο web service των ss_users αν υπάρχει καταχωρημένο το ΑΦΜ

        if( ( userId = consumeSSUsersWs(tin) ) == null ) {
            JsfUtil.addErrorMessage((JsfUtil.getMsgString("producer.tinNotRegisteredInGaiaErrorMessage")));
            return false;
        }
        //Ελέγχει στο web service των ss_user_modules αν υπάρχει καταχωρημένο το userId
        else if( !consumeSSUserModulesWs(userId) ) {
            JsfUtil.addErrorMessage((JsfUtil.getMsgString("producer.userIdNotRegisteredInGaiaErrorMessage")));
            return false;
        }

        return true;
    }

    /**
     * Εκτελούμε τα validations με τα registrations web services
     *
     * @param tin
     * @return
     * @throws java.io.IOException
     * @throws org.json.simple.parser.ParseException
     */
    public static boolean validateUserRegistration(String tin, String moduleId){

        Long userId;//Ελέγχει στο web service των ss_users αν υπάρχει καταχωρημένο το ΑΦΜ

        if( ( userId = consumeSSUsersWs(tin) ) == null ) {
            JsfUtil.addErrorMessage((JsfUtil.getMsgString("producer.tinNotRegisteredInGaiaErrorMessage")));
            return false;
        }
        //Ελέγχει στο web service των ss_user_modules αν υπάρχει καταχωρημένο το userId
        else if( !consumeSSUserModulesWs(userId, moduleId) ) {
            JsfUtil.addErrorMessage((JsfUtil.getMsgString("producer.userIdNotRegisteredInGaiaErrorMessage")));
            return false;
        }

        return true;
    }
    
    /**
     * Ελέγχει στο web service των ss_users αν υπάρχει καταχωρημένο το ΑΦΜ
     * αν ναι επιστρέφει το user_id εάν δεν υπάρχει επιστρέφει null
     *
     * @return
     * @throws java.io.IOException
     * @throws org.json.simple.parser.ParseException
     */
    private static Long consumeSSUsersWs(String tin) {

        String urlString = getSSUsersWebServiceURL();

        if(urlString == null) {
            throw new IllegalStateException("Δεν υπάρχει ορισμένο το ssUserURL στις παραμέτρους της εφαρμογής");
        }

        try {

            String urlStr = String.format(urlString+"?vat=%s", tin);

            URL url = new URL(urlStr);
            URLConnection urlc = url.openConnection();

            BufferedReader bfr = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
            String line;
            if ((line = bfr.readLine()) != null) {

                JSONObject json = (JSONObject)new JSONParser().parse(line);
                JSONObject b = (JSONObject)json.get("b");

                if( (b == null) || (b.isEmpty())) {

                    return null;
                }
                else {

                    return (Long)b.get("user_id");
                }
            }

        } catch (IOException e) {

            e.printStackTrace();
            throw new IllegalStateException("IOException in consumeSSUsersWs() :"+e.getMessage());

        } catch (ParseException e) {

            e.printStackTrace();
            throw new IllegalStateException("ParseException in consumeSSUsersWs() :"+e.getMessage());
        }

        return null;
    }
   

    private static boolean consumeSSUserModulesWs(Long userId) {

        String urlString = getSSUserModulesWebServiceURL();
        String moduleId =  getVatRefundModuleId();

        if(urlString == null) {
            throw new IllegalStateException("Δεν υπάρχει ορισμένο το ssUserModulesURL στις παραμέτρους της εφαρμογής");
        }
        if(moduleId == null) {
            throw new IllegalStateException("Δεν υπάρχει ορισμένο το moduleId στις παραμέτρους της εφαρμογής");
        }
        if(userId == null) {
            return false;
        }

        try {
            String urlStr = String.format(urlString+"?user_id=%s&modu_id=%s", userId,moduleId.toString());

            URL url = new URL(urlStr);
            URLConnection urlc = url.openConnection();

            BufferedReader bfr = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
            String line;
            if ((line = bfr.readLine()) != null) {

                JSONObject json = (JSONObject)new JSONParser().parse(line);
                Boolean status = (Boolean)json.get("status");

                if( (status == null) || (!status)) {

                    return false;
                }
                else {

                    return true;
                }
            }

        } catch (IOException e) {

            e.printStackTrace();
            throw new IllegalStateException("IOException in consumeSSUserModulesWs() :"+e.getMessage());

        } catch (ParseException e) {

            e.printStackTrace();
            throw new IllegalStateException("ParseException in consumeSSUserModulesWs() :"+e.getMessage());
        }

        return false;
    }
    
    private static boolean consumeSSUserModulesWs(Long userId, String moduleId) {

        String urlString = getSSUserModulesWebServiceURL();

        if(urlString == null) {
            throw new IllegalStateException("Δεν υπάρχει ορισμένο το ssUserModulesURL στις παραμέτρους της εφαρμογής");
        }
        if(moduleId == null) {
            throw new IllegalStateException("Δεν υπάρχει ορισμένο το moduleId στις παραμέτρους της εφαρμογής");
        }
        if(userId == null) {
            return false;
        }

        try {
            String urlStr = String.format(urlString+"?user_id=%s&modu_id=%s", userId,moduleId.toString());

            URL url = new URL(urlStr);
            URLConnection urlc = url.openConnection();

            BufferedReader bfr = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
            String line;
            if ((line = bfr.readLine()) != null) {

                JSONObject json = (JSONObject)new JSONParser().parse(line);
                Boolean status = (Boolean)json.get("status");

                if( (status == null) || (!status)) {

                    return false;
                }
                else {

                    return true;
                }
            }

        } catch (IOException e) {

            e.printStackTrace();
            throw new IllegalStateException("IOException in consumeSSUserModulesWs() :"+e.getMessage());

        } catch (ParseException e) {

            e.printStackTrace();
            throw new IllegalStateException("ParseException in consumeSSUserModulesWs() :"+e.getMessage());
        }

        return false;
    }

    /**
     * Επιστρέφει το URL με τα web services για το ss_users
     *
     * @return
     */
    private static String getSSUsersWebServiceURL() {

        return Property.getPropertyValue("web-services.properties", "ssUserURL", null);
    }

    /**
     * Επιστρέφει το URL με τα web services για το ss_user_modules
     *
     * @return
     */
    private static String getSSUserModulesWebServiceURL() {

        return Property.getPropertyValue("web-services.properties","ssUserModulesURL",null);
    }

    /**
     * Επιστρέφει το URL με τα web services για το ss_user_modules
     *
     * @return
     */
    private static String getVatRefundModuleId() {

        return Property.getPropertyValue("vat-refund.properties","moduleId",null);
    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
