package gr.neuropublic.Niva.wsRestFacades;

import gr.neuropublic.Niva.services.IMainService;
import gr.neuropublic.base.SqlQueryRow;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.functional.Action1Ex;
import gr.neuropublic.mutil.base.Pair;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.ws.WebServiceContext;

import gr.neuropublic.Niva.*;


@Stateless
@WebService
@HandlerChain(file="/SoapHandlers.xml")

public class WebServices {
    
    
    
    @EJB
    private IMainService.ILocal mainService;
    
    private IMainService.ILocal getMainService() {
    return mainService;
    }
       
    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }
   
    @Resource
    WebServiceContext wsCtx;
    
//    public String getUserName() {
//        MessageContext ctx = wsCtx.getMessageContext();
//        Object o = ctx.get("userName");
//        return o == null ? "WEB_SERVICE" : o.toString();
//    }
    
    /* v_parcel_decision_ws_export */
    @WebMethod
    public List<ParcelDecisionWsExport> getParcelDecision(@WebParam(name="businessRuleEngine") String businessRuleEngine,
                                       @WebParam(name="issueDate") java.util.Date issueDate)  {

        List<ParcelDecisionWsExport> results = new ArrayList<>();
        
        ArrayList<Pair<String, Object>> params = new ArrayList<>();
        
        //    <businessRuleEngine xmlns="">crop-to-land</businessRuleEngine>
        //    <issueDate xmlns="">2020-11-18</issueDate>        
        
        String sqlQuery = "select * from niva.v_parcel_decision_ws_export  where ecgr_name = :businessRuleEngine " +
                          " and dema_date = :issueDate ";
                   
         params.add(gr.neuropublic.mutil.base.Pair.create("businessRuleEngine", (Object) businessRuleEngine));
         params.add(gr.neuropublic.mutil.base.Pair.create("issueDate", (Object) issueDate));
        
        List<SqlQueryRow> entities = getMainService().executeSqlQuery2(sqlQuery, params);
                                 
        for (SqlQueryRow queryRow : entities) {

            ParcelDecisionWsExport pdWsExport = new ParcelDecisionWsExport();
            
            pdWsExport.DECISION_LIGHT =  queryRow.get("decision_light") != null ? ((String)queryRow.get("decision_light")) : null;
            pdWsExport.FARMER_CODE =  queryRow.get("farmer_code") != null ?   BigInteger.valueOf((Integer)queryRow.get("farmer_code"))       : null;
            pdWsExport.PARCEL_CODE =  queryRow.get("parcel_code") != null ?  BigInteger.valueOf((Integer)queryRow.get("parcel_code")) : null;
            pdWsExport.IDENTIFIER =  queryRow.get("identifier") != null ? ((String)queryRow.get("identifier")) : null;
            pdWsExport.CULTIVATION_DECLARED_CODE =  queryRow.get("cultivation_declared_code") != null ?  BigInteger.valueOf((Integer)queryRow.get("cultivation_declared_code")) : null;
            pdWsExport.CULTIVATION_DECLARED_NAME =  queryRow.get("cultivation_declared_name") != null ? ((String)queryRow.get("cultivation_declared_name")) : null;
                        
            pdWsExport.CULTIVATION_1ST_PREDICTION_CODE =  queryRow.get("cultivation_1st_prediction_code") != null ?  BigInteger.valueOf((Integer)queryRow.get("cultivation_1st_prediction_code"))  : null;
            pdWsExport.CULTIVATION_1ST_PREDICTION_NAME =  queryRow.get("cultivation_1st_prediction_name") != null ? ((String)queryRow.get("cultivation_1st_prediction_name")) : null;
            pdWsExport.PROBABILITY_1ST_PREDICTION =  queryRow.get("probability_1st_prediction") != null ? ((BigDecimal)  queryRow.get("probability_1st_prediction")) : null;
             
            pdWsExport.CULTIVATION_2ND_PREDICTION_CODE =  queryRow.get("cultivation_2nd_prediction_code") != null ? BigInteger.valueOf((Integer)queryRow.get("cultivation_2nd_prediction_code"))   : null;
            pdWsExport.CULTIVATION_2ND_PREDICTION_NAME =  queryRow.get("cultivation_2nd_prediction_name") != null ? ((String)queryRow.get("cultivation_2nd_prediction_name")) : null;
            pdWsExport.PROBABILITY_2ND_PREDICTION =  queryRow.get("probability_2nd_prediction") != null ? ((BigDecimal) queryRow.get("probability_2nd_prediction")) : null;

            results.add(pdWsExport);
        }
        
        return results;
    } 
    
}