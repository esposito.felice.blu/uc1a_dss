package gr.neuropublic.Niva.wsRestFacadesBase;

import gr.neuropublic.wsrestutils.AbstractWebServiceHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gr.neuropublic.Niva.services.ISessionsCacheService;
import gr.neuropublic.base.UserSession;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.CookieParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.NewCookie;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.apache.commons.dbutils.DbUtils;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.base.UserBannerMessage;
import gr.neuropublic.base.WsResponseInfo;
import gr.neuropublic.functional.Func;
import gr.neuropublic.Niva.services.IUserManagementService;
import gr.neuropublic.base.JsonHelper;
import gr.neuropublic.base.SqlQueryRow;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.mutil.base.Pair;


public class MenuServiceBase extends AbstractWebServiceHandler {

    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    @Override
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }


    @Override
    public void initializeDatabaseSession(UserSession usrSession) {
    }


    @EJB
    private ISessionsCacheService.ILocal sessionsCache;
    @Override
    public ISessionsCacheService.ILocal getSessionsCache() {
        return sessionsCache;
    }
    public void setSessionsCache(ISessionsCacheService.ILocal sessionsCache) {
        this.sessionsCache = sessionsCache;
    }

    
    @POST
    @Path("/getMenu")
    public Response getMenu(
        @CookieParam("niva-session-id") String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        final MenuServiceBase pThis = this;
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            MenuResponce rsp = new MenuResponce();
            rsp.result = MenuItem.getMenu(usrSession.privileges, pThis, usrSession);
            rsp.privileges = usrSession.privileges.toArray(new String[0]);
            rsp.subsId = usrSession.subsId;
            rsp.subsCode = usrSession.subsCode;
            rsp.subsDescription = usrSession.subsDescription;
            rsp.userId = usrSession.userId;
            rsp.userVat = usrSession.userVat;
            rsp.usrEmail = usrSession.usrEmail;
            rsp.usrActiveEmail = usrSession.getUsrActiveEmail();
            rsp.userLoginName = usrSession.getUserLoginName();
            rsp.subSecClasses = usrSession.subSecClasses;
            rsp.isWsResponseInfoEnabled = getUserManagementService().getAppWsResponeInfoEnabled(usrSession.appName);
            rsp.sessionClientIp = usrSession.getClientIp0();

            // ServerSide globals

            String jsonStr = rsp.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public static class GetBannerMessages_req{
        public List<WsResponseInfo> wsResponseInfoList;
    }

    @POST
    @Path("/getBannerMessages")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getBannerMessages(
            final GetBannerMessages_req r,
            @CookieParam("niva-session-id") final String sessionId) 
    {
        return handleWebRequest(sessionId, false, new Func1<gr.neuropublic.base.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.base.UserSession usrSession) {
            BannerMessagesResponce rsp = new BannerMessagesResponce();
            rsp.isWsResponseInfoEnabled = getUserManagementService().getAppWsResponeInfoEnabled(usrSession.appName);
            if (rsp.isWsResponseInfoEnabled){
                getUserManagementService().insertWsResponseInfo(sessionId, usrSession.appName, usrSession.subsId, r.wsResponseInfoList);
            }
            rsp.result = getUserManagementService().getUserBannerMessages(sessionId, usrSession.userId, usrSession.subsId, usrSession.appName);
            String jsonStr = rsp.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/getLoginPageMessages")
    public Response getLoginPageMessages() 
    {
        return super.handleWebRequest(new Func<Response>() {@Override public Response  lambda() {
            List<UserBannerMessage> usrMessages = getUserManagementService().getUserBannerMessages(null, null, null, "niva");
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            return Response.ok(gson.toJson(usrMessages)).type("application/json;charset=UTF-8").build();
        }});
    }


    @POST
    @Path("/getLoginPageMessages")
    public Response getLoginPageMessages(@CookieParam("niva-session-id") final String sessionId) 
    {
        return handleWebRequest(sessionId, new Func1<gr.neuropublic.base.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.base.UserSession usrSession) {
            List<UserBannerMessage> usrMessages = getUserManagementService().getUserBannerMessages(sessionId, usrSession.userId, usrSession.subsId, usrSession.appName);
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            return Response.ok(gson.toJson(usrMessages)).type("application/json;charset=UTF-8").build();
        }});
    }

    public static class GetAuditInfoUsers_req {
        public String userInsert;
        public String userUpdate;
    }

    @POST
    @Path("/getAuditInfoUsers")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getAuditInfoUsers(
            final GetAuditInfoUsers_req r,
            @CookieParam("niva-session-id") final String sessionId) 
    {
        return handleWebRequest(sessionId, false, new Func1<gr.neuropublic.base.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.base.UserSession usrSession) {
            if (!usrSession.privileges.contains("Niva_ShowAuditInfo")) {
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").
                        entity("NO_AUDIT_INFO_PRIVILEGE").build();
            }

            SqlQueryRow rsp = new SqlQueryRow();
            rsp = getUserManagementService().getAuditInfoUsers(usrSession.DecryptString(r.userInsert), usrSession.DecryptString(r.userUpdate), false, false);
            String jsonStr = rsp.toJson();
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }


    public  Response handleWebRequest(String sessionId, Func1<gr.neuropublic.base.UserSession, Response> javaClosure) {
        return super.handleWebRequest(sessionId, "niva-session-id", "/Niva/", javaClosure);
    }

    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subscriberCookie, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        Func1<gr.neuropublic.base.UserSession, Response> javaClosureCasted = new Func1<gr.neuropublic.base.UserSession, Response>() {
            @Override
            public Response lambda(gr.neuropublic.base.UserSession t1) {
                return javaClosure.lambda((gr.neuropublic.Niva.services.UserSession)t1);
            }
        };
        return super.handleWebRequest(sessionId, ssoSessionId, subscriberCookie, "niva-session-id", "/Niva/", javaClosureCasted);
    }

    public  Response handleWebRequest(String sessionId, boolean resetMemCacheTimer, Func1<gr.neuropublic.base.UserSession, Response> javaClosure) {
        return super.handleWebRequest(sessionId, resetMemCacheTimer, "niva-session-id", "/Niva/", javaClosure);
    }

    public boolean isMenuItemVisible(MenuItem mnItem, gr.neuropublic.Niva.services.UserSession usrSession) {
        return true;
    }
    public String getMenuItemLabel(MenuItem mnItem, gr.neuropublic.Niva.services.UserSession usrSession) {
        return mnItem.label;
    }

    public static class MenuResponce {
        public MenuItem[] result;
        public String[] privileges;
        public Boolean isWsResponseInfoEnabled;
        public Integer userId;
        public String usrEmail;
        public String usrActiveEmail;
        public String userLoginName;
        public Integer subsId;
        public String subsCode;
        public String subsDescription;
        public String userVat;
        public ArrayList subSecClasses;
        public String sessionClientIp;

        public String toJson(gr.neuropublic.base.CryptoUtils crypto) {
            java.util.Set tmp = new java.util.HashSet();
            StringBuilder sb = new StringBuilder();
            
            sb.append("{");
            
            sb.append("\"result\":"); 
            sb.append("[");
           
            int length = result.length;
            if (length>0) {
                sb.append(result[0].toJson());
                for(int i=1;i<length; i++) {
                    sb.append(',');
                    sb.append(result[i].toJson());
                }
            }
            sb.append("],"); // end of result
            
            sb.append("\"privileges\": [");
            length = privileges.length;
            if (length>0) {
                sb.append("\""); sb.append(JsonHelper.escapeString(privileges[0]));sb.append("\"");
                for(int i=1;i<length; i++) {
                    sb.append(',');
                    sb.append("\"");sb.append(JsonHelper.escapeString(privileges[i]));sb.append("\"");
                }
            }
            sb.append("],"); // end of privileges

            if (isWsResponseInfoEnabled != null){
                sb.append("\"isWsResponseInfoEnabled\": "); sb.append(isWsResponseInfoEnabled); sb.append(", ");
            }
            sb.append("\"userId\": "); sb.append(userId); sb.append(", ");
            sb.append("\"usrEmail\": \""); sb.append(JsonHelper.escapeString(usrEmail)); sb.append("\", ");
            if (usrActiveEmail != null) {
                sb.append("\"usrActiveEmail\": \""); sb.append(JsonHelper.escapeString(usrActiveEmail)); sb.append("\", ");
            } else {
                sb.append("\"usrActiveEmail\":"); sb.append("null"); sb.append(", ");
            }
            if (userLoginName != null) {
                sb.append("\"userLoginName\": \""); sb.append(JsonHelper.escapeString(userLoginName)); sb.append("\", ");
            } else {
                sb.append("\"userLoginName\":"); sb.append("null"); sb.append(", ");
            }
            sb.append("\"userVat\": \""); sb.append(JsonHelper.escapeString(userVat)); sb.append("\", ");
            sb.append("\"subsId\": "); sb.append(subsId); sb.append(", ");
            sb.append("\"subsCode\": \""); sb.append(JsonHelper.escapeString(subsCode)); sb.append("\", ");
            sb.append("\"subsDescription\": \""); sb.append(JsonHelper.escapeString(subsDescription)); sb.append("\", ");
            sb.append("\"sessionClientIp\": \""); sb.append(JsonHelper.escapeString(sessionClientIp)); sb.append("\", ");

            sb.append("\"subSecClasses\": [");
            length = subSecClasses.size();
            if (length>0) {
                sb.append(subSecClasses.get(0));
                for(int i=1;i<length; i++) {
                    sb.append(',');
                    sb.append(subSecClasses.get(i));
                }
            }
            sb.append("]"); // end of subSecClasses
            
            // ServerSide globals
            // end of ServerSide globals
            
            sb.append("}");
            return sb.toString();
        }
    }


    public static class MenuItem {
        public final String label; 
        public final String url; 
        public final String menuId;
        public final Boolean btnState;
        public final String invisibleFunc;
        public final MenuItem[] items;
        public final String privilege;
        public String getPrivilege() {
            return privilege;
        }
        public MenuItem(String label, String menuId, String url, Boolean btnState, String invisibleFunc, String privilege, MenuItem[] items) {
            this.label = label;
            this.menuId = menuId;
            this.url = url;
            this.btnState = btnState;
            this.invisibleFunc = invisibleFunc;
            this.items =  items;
            this.privilege = privilege;
        }
        public MenuItem(String label, String menuId, String url, Boolean btnState, String invisibleFunc, ArrayList<MenuItem> items) {
            this.label = label;
            this.menuId = menuId;
            this.url = url;
            this.btnState = btnState;
            this.invisibleFunc = invisibleFunc;
            this.items = items.toArray(new MenuItem[0]);
            this.privilege = null;
        }

        public String toJson() {
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            
            sb.append("\"label\":"); sb.append("\""); sb.append(JsonHelper.escapeString(label)); sb.append("\"");
            if (url != null){
                sb.append(",");sb.append("\"url\":"); sb.append("\""); sb.append(JsonHelper.escapeString(url)); sb.append("\"");
            }
            if (btnState != null){
                sb.append(",");sb.append("\"btnState\":"); sb.append(btnState); 
            }
            if (invisibleFunc != null){
                sb.append(",");sb.append("\"invisibleFunc\":"); sb.append("\""); sb.append(JsonHelper.escapeString(invisibleFunc)); sb.append("\"");
            }
            if (menuId != null) {
                sb.append(",");sb.append("\"menuId\":"); sb.append("\""); sb.append(JsonHelper.escapeString(menuId)); sb.append("\"");
            }
            sb.append(",");
            sb.append("\"items\":"); sb.append("[");
            int length = items.length;
            if (length>0) {
                sb.append(items[0].toJson());
                for(int i=1;i<length; i++) {
                    sb.append(',');
                    sb.append(items[i].toJson());
                }
            }
            sb.append("]");
            
            sb.append("}"); 
            return sb.toString();
        }
        
        public MenuItem getMenuWithPriveleges(HashSet<String> userPrivileges, MenuServiceBase pService, gr.neuropublic.Niva.services.UserSession usrSession) {
            MenuItem ret = null;
            String _label = this.label;
            if ((privilege == null || userPrivileges.contains(privilege)) && pService.isMenuItemVisible(this, usrSession)) {
                ArrayList subItems = new ArrayList();
                for(MenuItem sb:items) {
                    MenuItem psb = sb.getMenuWithPriveleges(userPrivileges, pService, usrSession);
                    if (psb != null)
                        subItems.add(psb);
                }
                
                _label = pService.getMenuItemLabel(this, usrSession);
                ret =  items.length == 0 ?
                        new MenuItem(_label, menuId, url, btnState, invisibleFunc, subItems):
                        subItems.isEmpty() ? null : new MenuItem(_label, menuId, url, btnState, invisibleFunc, subItems);
            }
            
            return ret;
            
        }

        public static MenuItem[] getMenu(HashSet<String> userPrivileges, MenuServiceBase pService, gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<MenuItem> topLevelMenuItems = new ArrayList<MenuItem>();
            for(MenuItem mn:menuItems) {
                MenuItem itm = mn.getMenuWithPriveleges(userPrivileges, pService, usrSession);
                if (itm != null)
                    topLevelMenuItems.add(itm);
            }

            return topLevelMenuItems.toArray(new MenuItem[0]);
        }

        
        public static MenuItem[] menuItems = new MenuItem[] {
                new MenuItem(
                    "Decision Support Engine", 
                    null,
                    null, 
                    true,
                    null,
                    null,
                    new MenuItem[]{ 
                        new MenuItem(
                            "Data Import", 
                            "Classification",
                            "/ClassificationSearch", 
                            null,
                            null,
                            "Niva_Classification_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Decision Making", 
                            "DecisionMaking",
                            "/DecisionMakingSearch", 
                            null,
                            null,
                            "Niva_DecisionMaking_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Field Map", 
                            "DecisionMakingRO",
                            "/DecisionMakingROSearch", 
                            null,
                            null,
                            "Niva_DecisionMakingRO_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Geotagged", 
                            "ParcelGP",
                            "/ParcelGPSearch", 
                            null,
                            null,
                            "Niva_ParcelGP_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "FMIS Calendars", 
                            "ParcelFMIS",
                            "/ParcelFMISSearch", 
                            null,
                            null,
                            "Niva_ParcelFMIS_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Dashboard", 
                            "Dashboard",
                            "/DashboardSearch", 
                            null,
                            null,
                            "Niva_Dashboard_R",
                            new MenuItem[]{}) 
                    }),
                new MenuItem(
                    "Administration", 
                    null,
                    null, 
                    true,
                    null,
                    null,
                    new MenuItem[]{ 
                        new MenuItem(
                            "Business Rules Engine", 
                            "EcGroup",
                            "/EcGroupSearch", 
                            null,
                            null,
                            "Niva_EcGroup_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Data Import Template", 
                            "FileTemplate",
                            "/FileTemplateSearch", 
                            null,
                            null,
                            "Niva_FileTemplate_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Producers", 
                            "Producers",
                            "/ProducersSearch", 
                            null,
                            null,
                            "Niva_Producers_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Geotagged Apps Users", 
                            "AgrisnapUsers",
                            "/AgrisnapUsersSearch", 
                            null,
                            null,
                            "Niva_AgrisnapUsers_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "FMIS Users", 
                            "FmisUser",
                            "/FmisUserSearch", 
                            null,
                            null,
                            "Niva_FmisUser_R",
                            new MenuItem[]{}) 
                    }),
                new MenuItem(
                    "System Parameters", 
                    null,
                    null, 
                    true,
                    null,
                    null,
                    new MenuItem[]{ 
                        new MenuItem(
                            "Paying Agency", 
                            "Agency",
                            "/Agency", 
                            null,
                            null,
                            "Niva_Agency_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Classification Engine", 
                            "Classifier",
                            "/Classifier", 
                            null,
                            null,
                            "Niva_Classifier_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Land Cover", 
                            "CoverType",
                            "/CoverType", 
                            null,
                            null,
                            "Niva_CoverType_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Crop", 
                            "Cultivation",
                            "/Cultivation", 
                            null,
                            null,
                            "Niva_Cultivation_R",
                            new MenuItem[]{}),
                        new MenuItem(
                            "Temporary Files Directory", 
                            "FileDirPath",
                            "/FileDirPath", 
                            null,
                            null,
                            "Niva_FileDirPath_R",
                            new MenuItem[]{}) 
                    }),
                new MenuItem(
                    "Help", 
                    null,
                    null, 
                    true,
                    null,
                    null,
                    new MenuItem[]{ 
                        new MenuItem(
                            "Documents", 
                            "Document",
                            "/Document", 
                            null,
                            null,
                            "Niva_Document_R",
                            new MenuItem[]{}) 
                    })
            };        

    }

    public static class BannerMessagesResponce {
        public List<UserBannerMessage> result;
        public Boolean isWsResponseInfoEnabled;

        public String toJson(gr.neuropublic.base.CryptoUtils crypto) {
            java.util.Set tmp = new java.util.HashSet();
            StringBuilder sb = new StringBuilder();
            
            sb.append("{");
            
            sb.append("\"result\":"); 
            sb.append("[");
           
            int length = result.size();
            if (length>0) {
                sb.append(result.get(0).toJson());
                for(int i=1; i<length; i++) {
                    sb.append(',');
                    sb.append(result.get(i).toJson());
                }
            }
            sb.append("]"); // end of result

            if (isWsResponseInfoEnabled != null){
                sb.append(",");sb.append("\"isWsResponseInfoEnabled\": "); sb.append(isWsResponseInfoEnabled); 
            }
            
            sb.append("}");
            return sb.toString();
        }
    }

}