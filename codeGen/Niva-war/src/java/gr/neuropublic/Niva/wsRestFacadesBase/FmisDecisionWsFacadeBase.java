package gr.neuropublic.Niva.wsRestFacadesBase;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import gr.neuropublic.shiro.ILoginController;
import java.util.List;
import java.util.ArrayList;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.servlet.ServletContext;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.wsrestutils.AbstractWebServiceHandler;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.Niva.services.IUserManagementService;
import gr.neuropublic.mutil.base.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.Niva.facades.IFmisDecisionFacade;
import gr.neuropublic.Niva.entities.FmisDecision;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import gr.neuropublic.Niva.facades.ITmpBlobFacade;
import gr.neuropublic.Niva.entities.TmpBlob;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.base.DateFormat;
import javax.ws.rs.CookieParam;
import javax.ws.rs.core.NewCookie;
import gr.neuropublic.Niva.services.ISessionsCacheService;
import gr.neuropublic.base.JsonBlob;
import gr.neuropublic.base.JsonSingleValue;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.core.MultivaluedMap;
import java.io.InputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import gr.neuropublic.base.GridColDefinition;

public  class FmisDecisionWsFacadeBase  extends AbstractWebServiceHandler {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(gr.neuropublic.base.IAbstractService.class);

    @EJB
    private IFmisDecisionFacade.ILocal fmisDecisionFacade;
    public IFmisDecisionFacade.ILocal getFmisDecisionFacade() {
        return fmisDecisionFacade;
    }
    public void setFmisDecisionFacade(IFmisDecisionFacade.ILocal val) {
        fmisDecisionFacade = val;
    }

    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    @Override
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }

    @EJB
    private ISessionsCacheService.ILocal sessionsCache;
    @Override
    public ISessionsCacheService.ILocal getSessionsCache() {
        return sessionsCache;
    }
    public void setSessionsCache(ISessionsCacheService.ILocal sessionsCache) {
        this.sessionsCache = sessionsCache;
    }


    @EJB
    private ITmpBlobFacade.ILocal tmpBlobFacade;
    public ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }

    @EJB
    private gr.neuropublic.Niva.services.IMainService.ILocal service;

    public gr.neuropublic.Niva.services.IMainService.ILocal getService()
    {
        return service;
    }
    public void setService(gr.neuropublic.Niva.services.IMainService.ILocal service)
    {
        this.service = service;
    }

    @Override
    public void initializeDatabaseSession(gr.neuropublic.base.UserSession usrSession) {
    }




    public static class FindAllByIds_request {
        public String[] ids; 
    }

    @POST
    @Path("/findAllByIds")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByIds(
        final FindAllByIds_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findAllByIds_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findAllByIds_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByIds_request r) {
            
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(String enc_id: r.ids) {
            Integer id = (enc_id != null ? usrSession.DecryptInteger(enc_id) : null);
            if (id != null)
                ids.add(id);
        } 
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        List<FmisDecision> entities = getFmisDecisionFacade().findAllByIds(ids);
        ret.data = entities;
        ret.count = entities.size();
        return ret;
    }


    public static class FindByFmisDecisionsId_request {
        public String fmisDecisionsId; 
    }

    @POST
    @Path("/findByFmisDecisionsId")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findByFmisDecisionsId(
        final FindByFmisDecisionsId_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findByFmisDecisionsId_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findByFmisDecisionsId_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindByFmisDecisionsId_request r) {
            
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        java.util.ArrayList<FmisDecision> list = new java.util.ArrayList<FmisDecision>();
        FmisDecision c = getFmisDecisionFacade().findByFmisDecisionsId((r.fmisDecisionsId != null ? usrSession.DecryptInteger(r.fmisDecisionsId) : null));
        if (c!=null)
            list.add(c); 
        ret.data = list;
        ret.count = list.size();
        return ret;
    }


    public static class FindAllByCriteriaRange_DashboardGrpFmisDecision_request {
        public String parcelsIssuesId_parcelsIssuesId;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_DashboardGrpFmisDecision")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_DashboardGrpFmisDecision(
        final FindAllByCriteriaRange_DashboardGrpFmisDecision_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_DashboardGrpFmisDecision_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_DashboardGrpFmisDecision_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_DashboardGrpFmisDecision_count(
        final FindAllByCriteriaRange_DashboardGrpFmisDecision_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_DashboardGrpFmisDecision_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_DashboardGrpFmisDecision_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_DashboardGrpFmisDecision_request r) {
        if (!usrSession.privileges.contains("Niva_Dashboard_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<FmisDecision> retEntities= getFmisDecisionFacade().findAllByCriteriaRange_DashboardGrpFmisDecision((r.parcelsIssuesId_parcelsIssuesId != null && !r.parcelsIssuesId_parcelsIssuesId.startsWith("TEMP_ID_") ? (r.parcelsIssuesId_parcelsIssuesId != null ? usrSession.DecryptInteger(r.parcelsIssuesId_parcelsIssuesId) : null) : null), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getFmisDecisionFacade().findAllByCriteriaRange_DashboardGrpFmisDecision_count((r.parcelsIssuesId_parcelsIssuesId != null && !r.parcelsIssuesId_parcelsIssuesId.startsWith("TEMP_ID_") ? (r.parcelsIssuesId_parcelsIssuesId != null ? usrSession.DecryptInteger(r.parcelsIssuesId_parcelsIssuesId) : null) : null), excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    public static class FindAllByCriteriaRange_ParcelFMISGrpFmisDecision_request {
        public String parcelsIssuesId_parcelsIssuesId;
        public String fsch_cultId_cultId;
        public String fsch_cotyId_cotyId;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_ParcelFMISGrpFmisDecision")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_ParcelFMISGrpFmisDecision(
        final FindAllByCriteriaRange_ParcelFMISGrpFmisDecision_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_ParcelFMISGrpFmisDecision_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_ParcelFMISGrpFmisDecision_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_ParcelFMISGrpFmisDecision_count(
        final FindAllByCriteriaRange_ParcelFMISGrpFmisDecision_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_ParcelFMISGrpFmisDecision_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_ParcelFMISGrpFmisDecision_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_ParcelFMISGrpFmisDecision_request r) {
        if (!usrSession.privileges.contains("Niva_ParcelFMIS_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<FmisDecision> retEntities= getFmisDecisionFacade().findAllByCriteriaRange_ParcelFMISGrpFmisDecision((r.parcelsIssuesId_parcelsIssuesId != null && !r.parcelsIssuesId_parcelsIssuesId.startsWith("TEMP_ID_") ? (r.parcelsIssuesId_parcelsIssuesId != null ? usrSession.DecryptInteger(r.parcelsIssuesId_parcelsIssuesId) : null) : null), (r.fsch_cultId_cultId != null && !r.fsch_cultId_cultId.startsWith("TEMP_ID_") ? (r.fsch_cultId_cultId != null ? usrSession.DecryptInteger(r.fsch_cultId_cultId) : null) : null), (r.fsch_cotyId_cotyId != null && !r.fsch_cotyId_cotyId.startsWith("TEMP_ID_") ? (r.fsch_cotyId_cotyId != null ? usrSession.DecryptInteger(r.fsch_cotyId_cotyId) : null) : null), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getFmisDecisionFacade().findAllByCriteriaRange_ParcelFMISGrpFmisDecision_count((r.parcelsIssuesId_parcelsIssuesId != null && !r.parcelsIssuesId_parcelsIssuesId.startsWith("TEMP_ID_") ? (r.parcelsIssuesId_parcelsIssuesId != null ? usrSession.DecryptInteger(r.parcelsIssuesId_parcelsIssuesId) : null) : null), (r.fsch_cultId_cultId != null && !r.fsch_cultId_cultId.startsWith("TEMP_ID_") ? (r.fsch_cultId_cultId != null ? usrSession.DecryptInteger(r.fsch_cultId_cultId) : null) : null), (r.fsch_cotyId_cotyId != null && !r.fsch_cotyId_cotyId.startsWith("TEMP_ID_") ? (r.fsch_cotyId_cotyId != null ? usrSession.DecryptInteger(r.fsch_cotyId_cotyId) : null) : null), excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    @DELETE
    @Path("/delByFmisDecisionsId")
    public Response delByFmisDecisionsId(
        @QueryParam("fmisDecisionsId") final String fmisDecisionsId, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            FmisDecision fmisDecision = getFmisDecisionFacade().findByFmisDecisionsId((fmisDecisionsId != null ? usrSession.DecryptInteger(fmisDecisionsId) : null));
            getService().removeEntity(usrSession.usrEmail, fmisDecision);
        //    Integer c = getFmisDecisionFacade().findByFmisDecisionsId((fmisDecisionsId != null ? usrSession.DecryptInteger(fmisDecisionsId) : null));
            return Response.status(200).type("application/json;charset=UTF-8").entity("").build();
        }});
    }

    public  Response handleWebRequest(String sessionId, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        return this.handleWebRequest(sessionId, null, new Pair<String,String>(null, null), javaClosure);
    }
    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subscriberCookie, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        Func1<gr.neuropublic.base.UserSession, Response> javaClosureCasted = new Func1<gr.neuropublic.base.UserSession, Response>() {
            @Override
            public Response lambda(gr.neuropublic.base.UserSession t1) {
                return javaClosure.lambda((gr.neuropublic.Niva.services.UserSession)t1);
            }
            
        };
    
        return super.handleWebRequest(sessionId, ssoSessionId, subscriberCookie, "niva-session-id", "/Niva/", javaClosureCasted);
    }
}