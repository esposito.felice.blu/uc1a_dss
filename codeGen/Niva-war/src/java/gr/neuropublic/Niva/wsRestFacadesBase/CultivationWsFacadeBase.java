package gr.neuropublic.Niva.wsRestFacadesBase;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import gr.neuropublic.shiro.ILoginController;
import java.util.List;
import java.util.ArrayList;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.servlet.ServletContext;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.wsrestutils.AbstractWebServiceHandler;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.Niva.services.IUserManagementService;
import gr.neuropublic.mutil.base.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.Niva.facades.ICultivationFacade;
import gr.neuropublic.Niva.entities.Cultivation;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import gr.neuropublic.Niva.facades.ITmpBlobFacade;
import gr.neuropublic.Niva.entities.TmpBlob;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.base.DateFormat;
import javax.ws.rs.CookieParam;
import javax.ws.rs.core.NewCookie;
import gr.neuropublic.Niva.services.ISessionsCacheService;
import gr.neuropublic.base.JsonBlob;
import gr.neuropublic.base.JsonSingleValue;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.core.MultivaluedMap;
import java.io.InputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import gr.neuropublic.base.GridColDefinition;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.DataFormat;

public  class CultivationWsFacadeBase  extends AbstractWebServiceHandler {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(gr.neuropublic.base.IAbstractService.class);

    @EJB
    private ICultivationFacade.ILocal cultivationFacade;
    public ICultivationFacade.ILocal getCultivationFacade() {
        return cultivationFacade;
    }
    public void setCultivationFacade(ICultivationFacade.ILocal val) {
        cultivationFacade = val;
    }

    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    @Override
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }

    @EJB
    private ISessionsCacheService.ILocal sessionsCache;
    @Override
    public ISessionsCacheService.ILocal getSessionsCache() {
        return sessionsCache;
    }
    public void setSessionsCache(ISessionsCacheService.ILocal sessionsCache) {
        this.sessionsCache = sessionsCache;
    }


    @EJB
    private ITmpBlobFacade.ILocal tmpBlobFacade;
    public ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }

    @EJB
    private gr.neuropublic.Niva.services.IMainService.ILocal service;

    public gr.neuropublic.Niva.services.IMainService.ILocal getService()
    {
        return service;
    }
    public void setService(gr.neuropublic.Niva.services.IMainService.ILocal service)
    {
        this.service = service;
    }

    @Override
    public void initializeDatabaseSession(gr.neuropublic.base.UserSession usrSession) {
    }




    public static class FindAllByIds_request {
        public String[] ids; 
    }

    @POST
    @Path("/findAllByIds")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByIds(
        final FindAllByIds_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findAllByIds_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findAllByIds_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByIds_request r) {
            
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(String enc_id: r.ids) {
            Integer id = (enc_id != null ? usrSession.DecryptInteger(enc_id) : null);
            if (id != null)
                ids.add(id);
        } 
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        List<Cultivation> entities = getCultivationFacade().findAllByIds(ids);
        ret.data = entities;
        ret.count = entities.size();
        return ret;
    }


    public static class FindByCultId_request {
        public String cultId; 
    }

    @POST
    @Path("/findByCultId")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findByCultId(
        final FindByCultId_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findByCultId_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findByCultId_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindByCultId_request r) {
            
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        java.util.ArrayList<Cultivation> list = new java.util.ArrayList<Cultivation>();
        Cultivation c = getCultivationFacade().findByCultId((r.cultId != null ? usrSession.DecryptInteger(r.cultId) : null));
        if (c!=null)
            list.add(c); 
        ret.data = list;
        ret.count = list.size();
        return ret;
    }


    public static class FindAllByCriteriaRange_forLov_request {
        public String fsch_CultivationLov_name;
        public Integer fsch_CultivationLov_code;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_forLov")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_forLov(
        final FindAllByCriteriaRange_forLov_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_forLov_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_forLov_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_forLov_count(
        final FindAllByCriteriaRange_forLov_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_forLov_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_forLov_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_forLov_request r) {
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<Cultivation> retEntities= getCultivationFacade().findAllByCriteriaRange_forLov(r.fsch_CultivationLov_name, r.fsch_CultivationLov_code, range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getCultivationFacade().findAllByCriteriaRange_forLov_count(r.fsch_CultivationLov_name, r.fsch_CultivationLov_code, excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    public static class FindAllByCriteriaRange_CultivationGrpCultivation_request {
        public String fsch_name;
        public Integer fsch_code;
        public String fsch_cotyId_cotyId;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_CultivationGrpCultivation")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_CultivationGrpCultivation(
        final FindAllByCriteriaRange_CultivationGrpCultivation_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_CultivationGrpCultivation_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_CultivationGrpCultivation_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_CultivationGrpCultivation_count(
        final FindAllByCriteriaRange_CultivationGrpCultivation_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_CultivationGrpCultivation_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_CultivationGrpCultivation_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_CultivationGrpCultivation_request r) {
        if (!usrSession.privileges.contains("Niva_Cultivation_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<Cultivation> retEntities= getCultivationFacade().findAllByCriteriaRange_CultivationGrpCultivation(r.fsch_name, r.fsch_code, (r.fsch_cotyId_cotyId != null && !r.fsch_cotyId_cotyId.startsWith("TEMP_ID_") ? (r.fsch_cotyId_cotyId != null ? usrSession.DecryptInteger(r.fsch_cotyId_cotyId) : null) : null), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getCultivationFacade().findAllByCriteriaRange_CultivationGrpCultivation_count(r.fsch_name, r.fsch_code, (r.fsch_cotyId_cotyId != null && !r.fsch_cotyId_cotyId.startsWith("TEMP_ID_") ? (r.fsch_cotyId_cotyId != null ? usrSession.DecryptInteger(r.fsch_cotyId_cotyId) : null) : null), excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }

    @POST
    @Path("/findAllByCriteriaRange_CultivationGrpCultivation_getIds")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_CultivationGrpCultivation_getIds(
        final FindAllByCriteriaRange_CultivationGrpCultivation_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            List<String> excludedIds = new ArrayList<String>();
            if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
                for(String encrypted_id:r.exc_Id) {
                    if (encrypted_id == null)
                        continue;
                    String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                    excludedIds.add(descryptedId);
                }
            }
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
            List<Integer> IdsList = getCultivationFacade().findAllByCriteriaRange_CultivationGrpCultivation_getIds(r.fsch_name, r.fsch_code, (r.fsch_cotyId_cotyId != null && !r.fsch_cotyId_cotyId.startsWith("TEMP_ID_") ? (r.fsch_cotyId_cotyId != null ? usrSession.DecryptInteger(r.fsch_cotyId_cotyId) : null) : null), excludedIds);
            ArrayList<String> encryptedIds = new ArrayList<String>();
            for(Integer id:IdsList) {
                encryptedIds.add(usrSession.EncryptNumber(id));
            }
            ret.data = encryptedIds;
            ret.count = IdsList.size();
            return Response.status(200).type("application/json;charset=UTF-8").entity(ret).build();
        }});
    }


    public boolean findAllByCriteriaRange_CultivationGrpCultivation_columnFilter(String field, gr.neuropublic.Niva.services.UserSession usrSession){
        return true;
    }

    @POST
    @Path("/findAllByCriteriaRange_CultivationGrpCultivation_toExcel")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_CultivationGrpCultivation_toExcel(
        final FindAllByCriteriaRange_CultivationGrpCultivation_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            if (!usrSession.privileges.contains("Niva_Cultivation_R"))
                throw new GenericApplicationException("NO_READ_ACCESS");
            //Thread.sleep(1500); // debugging waiting dialogs
            int maxRetEntities = 10000;
            int[] range = new int[] {0, maxRetEntities + 1};
            int[] recordCount = new int [] {0};
            List<String> excludedIds = new ArrayList<String>();
            if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
                for(String encrypted_id:r.exc_Id) {
                    if (encrypted_id == null)
                        continue;
                    String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                    excludedIds.add(descryptedId);
                }
            }

            //Determine Excel Columns
            String[] availableFields = {"name", "code", "cotyId"};
            HashSet<String> visibleFields = new HashSet<>();
            for (String field : availableFields){
                if ( findAllByCriteriaRange_CultivationGrpCultivation_columnFilter(field, usrSession))
                    visibleFields.add(field);
            }
                        
            HashMap<String,String> visibleColumns = new HashMap<>();
            if (r != null && r.__fields != null) {
                for (GridColDefinition def : r.__fields) {
                    if (def.field != null && visibleFields.contains(def.field))
                        visibleColumns.put(def.field, def.displayName);
                }
            }
            if (r != null && r.__fields == null) {
                for (String field : visibleFields) {
                    visibleColumns.put(field, null);
                }
            }

            if (visibleColumns.isEmpty())
                throw new GenericApplicationException("EXCEL HAS NO COLUMNS"); 

            //Count Entities
            int entCount = getCultivationFacade().findAllByCriteriaRange_CultivationGrpCultivation_count(r.fsch_name, r.fsch_code, (r.fsch_cotyId_cotyId != null && !r.fsch_cotyId_cotyId.startsWith("TEMP_ID_") ? (r.fsch_cotyId_cotyId != null ? usrSession.DecryptInteger(r.fsch_cotyId_cotyId) : null) : null), excludedIds);
            if (entCount > maxRetEntities) {
                throw new GenericApplicationException("TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(" + maxRetEntities + ")"); 
            }
            //Get Entities
            List<Cultivation> retEntities= getCultivationFacade().findAllByCriteriaRange_CultivationGrpCultivation(true, r.fsch_name, r.fsch_code, (r.fsch_cotyId_cotyId != null && !r.fsch_cotyId_cotyId.startsWith("TEMP_ID_") ? (r.fsch_cotyId_cotyId != null ? usrSession.DecryptInteger(r.fsch_cotyId_cotyId) : null) : null), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            if (retEntities.size() > maxRetEntities) {
                throw new GenericApplicationException("TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(" + maxRetEntities + ")"); 
            }

            //create excel
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet worksheet = workbook.createSheet("Sheet1");

            // add headers
            HSSFRow headersRow = worksheet.createRow(0);
            HSSFCellStyle cellStyleHeader = workbook.createCellStyle();
            cellStyleHeader.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            cellStyleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

            HSSFCell cell;
            int cellIndex = 0;
            if (visibleColumns.containsKey("name")){
                cell = headersRow.createCell(cellIndex);
                cell.setCellValue(visibleColumns.get("name") != null ? visibleColumns.get("name") : "Crop Name");
                cell.setCellStyle(cellStyleHeader);
                worksheet.autoSizeColumn(cellIndex);
                cellIndex++;
            }

            if (visibleColumns.containsKey("code")){
                cell = headersRow.createCell(cellIndex);
                cell.setCellValue(visibleColumns.get("code") != null ? visibleColumns.get("code") : "Code");
                cell.setCellStyle(cellStyleHeader);
                worksheet.autoSizeColumn(cellIndex);
                cellIndex++;
            }

            if (visibleColumns.containsKey("cotyId")){
                cell = headersRow.createCell(cellIndex);
                cell.setCellValue(visibleColumns.get("cotyId") != null ? visibleColumns.get("cotyId") : "Land Cover Name");
                cell.setCellStyle(cellStyleHeader);
                worksheet.autoSizeColumn(cellIndex);
                cellIndex++;
            }


            // create styles
            HSSFCellStyle cellStyle = workbook.createCellStyle();
            DataFormat dataFormat = workbook.createDataFormat();
            
            HSSFCellStyle cellStyleDate = workbook.createCellStyle();
            cellStyleDate.setDataFormat(dataFormat.getFormat("dd/mm/yyyy"));

            HSSFCellStyle GrpCultivation_itm__code_cellStyle = workbook.createCellStyle();
            GrpCultivation_itm__code_cellStyle.setDataFormat(dataFormat.getFormat("0"));

            // add entities
            int length = retEntities.size();
            if (length>0) {
                for(int i=0;i<length; i++) {
                    HSSFRow entityRow = worksheet.createRow(i+1);

                    cellIndex = 0;
                    if (visibleColumns.containsKey("name")) {
                        cell = entityRow.createCell(cellIndex);
                        if (retEntities.get(i).getName()!=null)
                            cell.setCellValue(retEntities.get(i).getName());
                        cell.setCellStyle(cellStyle);

                        cellIndex++;
                    }

                    if (visibleColumns.containsKey("code")) {
                        cell = entityRow.createCell(cellIndex);
                        if (retEntities.get(i).getCode()!=null)
                            cell.setCellValue(retEntities.get(i).getCode());
                        cell.setCellStyle(GrpCultivation_itm__code_cellStyle);

                        cellIndex++;
                    }

                    if (visibleColumns.containsKey("cotyId")) {
                        cell = entityRow.createCell(cellIndex);
                        if (retEntities.get(i).isCotyIdPresent() && retEntities.get(i).getCotyId().getName()!=null)
                            cell.setCellValue(retEntities.get(i).getCotyId().getName());
                        cell.setCellStyle(cellStyle);

                        cellIndex++;
                    }
                        
                }
            }

            // ByteArrayOutput
            java.io.ByteArrayOutputStream strm = new java.io.ByteArrayOutputStream();
            try {
                workbook.write(strm);
            } catch (IOException e) {
                logger.error("IOException", e);
                throw new RuntimeException(e);
            }
                        
            JsonBlob rsp = new JsonBlob();
            rsp.data = strm.toByteArray();
                
            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }



    @DELETE
    @Path("/delByCultId")
    public Response delByCultId(
        @QueryParam("cultId") final String cultId, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Cultivation cultivation = getCultivationFacade().findByCultId((cultId != null ? usrSession.DecryptInteger(cultId) : null));
            getService().removeEntity(usrSession.usrEmail, cultivation);
        //    Integer c = getCultivationFacade().findByCultId((cultId != null ? usrSession.DecryptInteger(cultId) : null));
            return Response.status(200).type("application/json;charset=UTF-8").entity("").build();
        }});
    }

    public  Response handleWebRequest(String sessionId, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        return this.handleWebRequest(sessionId, null, new Pair<String,String>(null, null), javaClosure);
    }
    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subscriberCookie, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        Func1<gr.neuropublic.base.UserSession, Response> javaClosureCasted = new Func1<gr.neuropublic.base.UserSession, Response>() {
            @Override
            public Response lambda(gr.neuropublic.base.UserSession t1) {
                return javaClosure.lambda((gr.neuropublic.Niva.services.UserSession)t1);
            }
            
        };
    
        return super.handleWebRequest(sessionId, ssoSessionId, subscriberCookie, "niva-session-id", "/Niva/", javaClosureCasted);
    }
}