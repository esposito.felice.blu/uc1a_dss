package gr.neuropublic.Niva.wsRestFacadesBase;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import gr.neuropublic.shiro.ILoginController;
import java.util.List;
import java.util.ArrayList;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.servlet.ServletContext;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.wsrestutils.AbstractWebServiceHandler;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.Niva.services.IUserManagementService;
import gr.neuropublic.mutil.base.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.Niva.facades.IFileTemplateFacade;
import gr.neuropublic.Niva.entities.FileTemplate;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import gr.neuropublic.Niva.facades.ITmpBlobFacade;
import gr.neuropublic.Niva.entities.TmpBlob;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.base.DateFormat;
import javax.ws.rs.CookieParam;
import javax.ws.rs.core.NewCookie;
import gr.neuropublic.Niva.services.ISessionsCacheService;
import gr.neuropublic.base.JsonBlob;
import gr.neuropublic.base.JsonSingleValue;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.core.MultivaluedMap;
import java.io.InputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import gr.neuropublic.base.GridColDefinition;

public  class FileTemplateWsFacadeBase  extends AbstractWebServiceHandler {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(gr.neuropublic.base.IAbstractService.class);

    @EJB
    private IFileTemplateFacade.ILocal fileTemplateFacade;
    public IFileTemplateFacade.ILocal getFileTemplateFacade() {
        return fileTemplateFacade;
    }
    public void setFileTemplateFacade(IFileTemplateFacade.ILocal val) {
        fileTemplateFacade = val;
    }

    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    @Override
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }

    @EJB
    private ISessionsCacheService.ILocal sessionsCache;
    @Override
    public ISessionsCacheService.ILocal getSessionsCache() {
        return sessionsCache;
    }
    public void setSessionsCache(ISessionsCacheService.ILocal sessionsCache) {
        this.sessionsCache = sessionsCache;
    }


    @EJB
    private ITmpBlobFacade.ILocal tmpBlobFacade;
    public ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }

    @EJB
    private gr.neuropublic.Niva.services.IMainService.ILocal service;

    public gr.neuropublic.Niva.services.IMainService.ILocal getService()
    {
        return service;
    }
    public void setService(gr.neuropublic.Niva.services.IMainService.ILocal service)
    {
        this.service = service;
    }

    @Override
    public void initializeDatabaseSession(gr.neuropublic.base.UserSession usrSession) {
    }




    public static class FindAllByIds_request {
        public String[] ids; 
    }

    @POST
    @Path("/findAllByIds")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByIds(
        final FindAllByIds_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findAllByIds_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findAllByIds_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByIds_request r) {
            
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(String enc_id: r.ids) {
            Integer id = (enc_id != null ? usrSession.DecryptInteger(enc_id) : null);
            if (id != null)
                ids.add(id);
        } 
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        List<FileTemplate> entities = getFileTemplateFacade().findAllByIds(ids);
        ret.data = entities;
        ret.count = entities.size();
        return ret;
    }


    public static class FindByFiteId_request {
        public String fiteId; 
    }

    @POST
    @Path("/findByFiteId")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findByFiteId(
        final FindByFiteId_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findByFiteId_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findByFiteId_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindByFiteId_request r) {
            
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        java.util.ArrayList<FileTemplate> list = new java.util.ArrayList<FileTemplate>();
        FileTemplate c = getFileTemplateFacade().findByFiteId((r.fiteId != null ? usrSession.DecryptInteger(r.fiteId) : null));
        if (c!=null)
            list.add(c); 
        ret.data = list;
        ret.count = list.size();
        return ret;
    }


    public static class FindLazyFileTemplate_request {
        public String name;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findLazyFileTemplate")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findLazyFileTemplate(
        final FindLazyFileTemplate_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findLazyFileTemplate_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findLazyFileTemplate_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findLazyFileTemplate_count(
        final FindLazyFileTemplate_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findLazyFileTemplate_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findLazyFileTemplate_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindLazyFileTemplate_request r) {
        if (!usrSession.privileges.contains("Niva_FileTemplate_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<FileTemplate> retEntities= getFileTemplateFacade().findLazyFileTemplate(r.name, range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getFileTemplateFacade().findLazyFileTemplate_count(r.name, excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    public static class FindAllByCriteriaRange_forLov_request {
        public String fsch_FileTemplateLov_name;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_forLov")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_forLov(
        final FindAllByCriteriaRange_forLov_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_forLov_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_forLov_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_forLov_count(
        final FindAllByCriteriaRange_forLov_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_forLov_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_forLov_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_forLov_request r) {
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<FileTemplate> retEntities= getFileTemplateFacade().findAllByCriteriaRange_forLov(r.fsch_FileTemplateLov_name, range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getFileTemplateFacade().findAllByCriteriaRange_forLov_count(r.fsch_FileTemplateLov_name, excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    @DELETE
    @Path("/delByFiteId")
    public Response delByFiteId(
        @QueryParam("fiteId") final String fiteId, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            FileTemplate fileTemplate = getFileTemplateFacade().findByFiteId((fiteId != null ? usrSession.DecryptInteger(fiteId) : null));
            getService().removeEntity(usrSession.usrEmail, fileTemplate);
        //    Integer c = getFileTemplateFacade().findByFiteId((fiteId != null ? usrSession.DecryptInteger(fiteId) : null));
            return Response.status(200).type("application/json;charset=UTF-8").entity("").build();
        }});
    }

    public  Response handleWebRequest(String sessionId, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        return this.handleWebRequest(sessionId, null, new Pair<String,String>(null, null), javaClosure);
    }
    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subscriberCookie, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        Func1<gr.neuropublic.base.UserSession, Response> javaClosureCasted = new Func1<gr.neuropublic.base.UserSession, Response>() {
            @Override
            public Response lambda(gr.neuropublic.base.UserSession t1) {
                return javaClosure.lambda((gr.neuropublic.Niva.services.UserSession)t1);
            }
            
        };
    
        return super.handleWebRequest(sessionId, ssoSessionId, subscriberCookie, "niva-session-id", "/Niva/", javaClosureCasted);
    }
}