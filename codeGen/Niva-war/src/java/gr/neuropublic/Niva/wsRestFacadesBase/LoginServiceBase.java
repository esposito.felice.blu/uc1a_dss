package gr.neuropublic.Niva.wsRestFacadesBase;

import gr.neuropublic.wsrestutils.AbstractWebServiceHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gr.neuropublic.Niva.services.ISessionsCacheService;
import gr.neuropublic.base.JsonSingleValue;
import gr.neuropublic.base.AuthenticationStatus;
import gr.neuropublic.Niva.services.IUserManagementService;
import gr.neuropublic.base.CryptoUtils;
import gr.neuropublic.base.UserSession;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.CookieParam;
import javax.ws.rs.Consumes;
import gr.neuropublic.exceptions.GenericApplicationException;


import javax.ws.rs.core.Response;
import javax.ws.rs.core.NewCookie;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.apache.commons.dbutils.DbUtils;
import javax.ws.rs.core.Context;
import javax.servlet.http.HttpServletRequest;
import gr.neuropublic.mutil.base.Pair;

import org.jboss.resteasy.annotations.GZIP;
import java.util.ArrayList;
import gr.neuropublic.rtlEntities.RTL_Subscriber;

public class LoginServiceBase extends AbstractWebServiceHandler {

    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    @Override
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }


    @Override
    public void initializeDatabaseSession(UserSession usrSession) {
    }


    @EJB
    private ISessionsCacheService.ILocal sessionsCache;
    @Override
    public ISessionsCacheService.ILocal getSessionsCache() {
        return sessionsCache;
    }
    public void setSessionsCache(ISessionsCacheService.ILocal sessionsCache) {
        this.sessionsCache = sessionsCache;
    }

    @EJB
    private gr.neuropublic.base.IMemCacheService.ILocal memCacheService;
    public gr.neuropublic.base.IMemCacheService.ILocal getMemCacheService() {
        return memCacheService;
    }
    public void setMemCacheService(gr.neuropublic.base.IMemCacheService.ILocal memCacheService) {
        this.memCacheService = memCacheService;
    }

    protected String getClientIp(HttpServletRequest request) {
        String clientIP = request.getHeader("X-Forwarded-For");

        if(clientIP != null && !clientIP.isEmpty()) {
            clientIP = clientIP.substring(0, Math.min(clientIP.length(), 120));
        }
        else {
            clientIP = request.getRemoteAddr();
        }
        return clientIP;
    }

    protected String getSubscriberCookie(UserSession userSession) {
        try {
            if (userSession == null)
                return null;

            gr.neuropublic.base.CryptoUtils crypto = new gr.neuropublic.base.CryptoUtils(UserSession.class.getName());
            return crypto.EncryptString(userSession.subsCode);
        } catch(Exception ex) {
            return null;
        }
    }
    
    public static class LoginUser_Request {
        public String login;
        public String password;
        public String loginSubsCode;
    }

    @POST
    @Path("/loginUser")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response loginUser(
        LoginUser_Request wsReq,
        @Context HttpServletRequest req
        ) {
        try {
            String appName = "Niva";
            Pair<AuthenticationStatus,Integer> authStatus;
            JsonSingleValue ret;
            String loginName = getUserManagementService().getUserLoginNameByPublicEmail(wsReq.login);
            if (loginName == null) {
                authStatus = Pair.create(AuthenticationStatus.ACCOUNT_DOESNOT_EXIST,0);
                ret = new JsonSingleValue(authStatus.a);
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").entity(ret).build();
            }

            authStatus = getUserManagementService().getUserAuthenticationStatus(loginName, wsReq.password, appName, true, false, new Pair<>(false, wsReq.loginSubsCode));
            ret = new JsonSingleValue(authStatus.a);
            if (authStatus.a == AuthenticationStatus.SUCCESS) {
                String sessionId = UUID.randomUUID().toString();
                String ssoSessionId = UUID.randomUUID().toString();
                getUserManagementService().makeNoteOfSuccessfulLogin(loginName, getClientIp(req), sessionId, ssoSessionId, appName);
                //Add userSession in the local Cache
                getMemCacheService().setValue(sessionId, "1");
                UserSession userSession = getSessionsCache().getEntry(sessionId);
                NewCookie sessionCookie = new NewCookie("niva-session-id", sessionId, "/Niva/", null, NewCookie.DEFAULT_VERSION, null, NewCookie.DEFAULT_MAX_AGE, false);    
                NewCookie subscriberCookie = new NewCookie("niva-subs-code", getSubscriberCookie(userSession), "/Niva/", null, NewCookie.DEFAULT_VERSION, null, NewCookie.DEFAULT_MAX_AGE, false);    
                String rspData = authStatus.b != null && authStatus.b < 100 ? authStatus.b.toString() : "\"" + authStatus.a.toString() + "\"";
                String rsp = "{\"data\":" + rspData + ", \"isSsoCreator\":false}";
                return Response.ok(rsp).type("application/json;charset=UTF-8").cookie(sessionCookie).cookie(subscriberCookie).build();
            } else {
                boolean statusLocks = false;
                if (authStatus.a == AuthenticationStatus.WRONG_PASSWORD)
                    statusLocks = true;
                getUserManagementService().makeNoteOfUnSuccessfulLogin(loginName, getClientIp(req), appName, authStatus.a.toString() , statusLocks);
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").entity(ret).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(e.getMessage()).build();
        }
    
    }

    @POST
    @Path("/loginSsoUser")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response loginSsoUser(
        @CookieParam("sso-value") String ssoSessionId,
        @Context HttpServletRequest req
        ) {
        try {
            String appName = "Niva";
            Pair<Pair<AuthenticationStatus,Integer>,String> authStatus = getUserManagementService().getUserAuthenticationStatusBasedOnSso(ssoSessionId, appName);
            JsonSingleValue ret = new JsonSingleValue(authStatus.a.a);
            if (authStatus.a.a == AuthenticationStatus.SUCCESS) {
                String sessionId = UUID.randomUUID().toString();
                getUserManagementService().makeNoteOfSuccessfulLogin(authStatus.b, getClientIp(req), sessionId, ssoSessionId, appName);
                //Add userSession in the local Cache
                getMemCacheService().setValue(sessionId, "1");
                UserSession userSession = getSessionsCache().getEntry(sessionId, ssoSessionId, true);
                NewCookie sessionCookie = new NewCookie("niva-session-id", sessionId, "/Niva/", null, NewCookie.DEFAULT_VERSION, null, NewCookie.DEFAULT_MAX_AGE, false);    
                NewCookie subscriberCookie = new NewCookie("niva-subs-code", getSubscriberCookie(userSession), "/Niva/", null, NewCookie.DEFAULT_VERSION, null, NewCookie.DEFAULT_MAX_AGE, false);    
                ret.data = authStatus.a.b != null && authStatus.a.b < 100 ? authStatus.a.b : authStatus.a.a;
                return Response.ok(ret).type("application/json;charset=UTF-8").cookie(sessionCookie).cookie(subscriberCookie).build();
            } else {
                boolean statusLocks = false;
                if (authStatus.a.a == AuthenticationStatus.WRONG_PASSWORD)
                    statusLocks = true;
                getUserManagementService().makeNoteOfUnSuccessfulLogin(authStatus.b, getClientIp(req), appName, authStatus.a.a.toString() , statusLocks);
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").entity(ret).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(e.getMessage()).build();
        }
    
    }

    @POST
    @Path("/sessionBelongToSso")
    public Response sessionBelongToSso(
            @CookieParam("niva-session-id") String sessionId,
            @CookieParam("sso-value") String ssoSessionId) 
    {
        try {
            return Response.status(200).type("application/json;charset=UTF-8").entity(getSessionsCache().sessionBelongToSso(sessionId, ssoSessionId)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/logoutUser")
    public Response logoutUser(
            @CookieParam("niva-session-id") String sessionId,
            @CookieParam("sso-value") String ssoSessionId) 
    {
        try {
            getSessionsCache().removeEntryAndLogoutFromDataBase(sessionId, ssoSessionId, false);
            //NewCookie sessionCookie = new NewCookie("niva-session-id", sessionId, "/Niva/", null, NewCookie.DEFAULT_VERSION, null, 0, false);    
            //return Response.ok().cookie(sessionCookie).build();
            return Response.ok().header(
                "Set-Cookie",
                "niva-session-id="+sessionId+"; "+
                "Version=1; Max-Age=0; Path=/Niva/; " +
                "Expires=Thu, 01 Jan 1970 00:00:00 GMT").header(
                "Set-Cookie",
                "niva-subs-code=; "+
                "Version=1; Max-Age=0; Path=/Niva/; " +
                "Expires=Thu, 01 Jan 1970 00:00:00 GMT").build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(e.getMessage()).build();
        }
    }

    public static class SetNewPassword_Request {
        public String publicUserName;
        public String oldPassword;
        public String newPassword;
    }

    @POST
    @Path("/setNewPassword")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response setNewPassword(
        SetNewPassword_Request wsReq,
        @Context HttpServletRequest req
        ) {
        try {
            String appName = "Niva";
            Pair<AuthenticationStatus,Integer> authStatus;
            JsonSingleValue ret;
            String loginName = getUserManagementService().getUserLoginNameByPublicEmail(wsReq.publicUserName);
            if (loginName == null) {
                authStatus = Pair.create(AuthenticationStatus.ACCOUNT_DOESNOT_EXIST,0);
                ret = new JsonSingleValue(authStatus.a);
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").entity(ret).build();
            }
            Integer userId = usrMngSrv.getUserIdByUserName(loginName);
            if (userId == null){
                authStatus = Pair.create(AuthenticationStatus.ACCOUNT_DOESNOT_EXIST,0);
                ret = new JsonSingleValue(authStatus.a);
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").entity(ret).build();
            }

            authStatus = getUserManagementService().getUserAuthenticationStatus(loginName, wsReq.oldPassword, appName, false, false, new Pair<Boolean, String>(false, null));
            ret = new JsonSingleValue(authStatus.a);
            if (authStatus.a == AuthenticationStatus.SUCCESS) {
                String clientIp = getClientIp(req);
                usrMngSrv.setUserPassword(userId, wsReq.publicUserName, wsReq.newPassword, loginName, false, clientIp);
                return Response.status(200).type("application/json;charset=UTF-8").entity("NEW_PASSWORD_SET").build();
            } else {
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").entity(ret).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(e.getMessage()).build();
        }
    }

    /**************** SSO LOGIN VIA TIN FROM EXTERNAL APPLICATION ************************************/
    public static class LoginExternalUserViaTin_Request {
        public String userSubsCode;
        public String userRole;
        public String userTin;
        public String userInsert;
        public String userEmail;
        public String userFirstName;
        public String userLastName;
        public String userFatherName;
        public String userPhone;
        public String userPhoneCell;
        public String userIdentityNumber;
        public String userAddress;
        public String userAddressNumber;
        public String userPostalCode;
        public String userCity;
        public String userPrefecture;
        public String userCountry;
    }

    @POST
    @Path("/loginSsoExternalUserViaTin")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response loginExternalUserViaTin(
        LoginExternalUserViaTin_Request wsReq,
        @Context HttpServletRequest req
        ) {
        try {
            String appName = "Niva";
            Pair<AuthenticationStatus,Integer> authStatus;
            JsonSingleValue ret;

            CryptoUtils crypto = new CryptoUtils(getMemCacheService().getExternalLogin_reqKey());
            
            String loginName = getUserManagementService().createExternalUserBy_Tin_SubsCode(appName, 
                    wsReq.userSubsCode != null ? crypto.DecryptString(wsReq.userSubsCode) : null, 
                    wsReq.userRole != null ?  crypto.DecryptString(wsReq.userRole) : null, 
                    wsReq.userTin != null ?  crypto.DecryptString(wsReq.userTin) : null, 
                    wsReq.userInsert != null ?  crypto.DecryptString(wsReq.userInsert) : null, 
                    wsReq.userEmail != null ?  crypto.DecryptString(wsReq.userEmail) : null, 
                    wsReq.userFirstName != null ?  crypto.DecryptString(wsReq.userFirstName) : null,  
                    wsReq.userLastName != null ?  crypto.DecryptString(wsReq.userLastName) : null, 
                    wsReq.userFatherName != null ?  crypto.DecryptString(wsReq.userFatherName) : null, 
                    wsReq.userPhone != null ?  crypto.DecryptString(wsReq.userPhone) : null, 
                    wsReq.userPhoneCell != null ?  crypto.DecryptString(wsReq.userPhoneCell) : null,  
                    wsReq.userIdentityNumber != null ?  crypto.DecryptString(wsReq.userIdentityNumber) : null,  
                    wsReq.userAddress != null ?  crypto.DecryptString(wsReq.userAddress) : null, 
                    wsReq.userAddressNumber != null ?  crypto.DecryptString(wsReq.userAddressNumber) : null, 
                    wsReq.userPostalCode != null ?  crypto.DecryptString(wsReq.userPostalCode) : null, 
                    wsReq.userCity != null ?  crypto.DecryptString(wsReq.userCity) : null, 
                    wsReq.userPrefecture != null ?  crypto.DecryptString(wsReq.userPrefecture) : null,  
                    wsReq.userCountry != null ?  crypto.DecryptString(wsReq.userCountry) : null);
            
            if (loginName == null) {
                authStatus = Pair.create(AuthenticationStatus.ACCOUNT_DOESNOT_EXIST,0);
                ret = new JsonSingleValue(authStatus.a);
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").entity(ret).build();
            }

            authStatus = getUserManagementService().getExternalUserAuthenticationStatus(loginName, appName);
            ret = new JsonSingleValue(authStatus.a);
            if (authStatus.a == AuthenticationStatus.SUCCESS) {
                //String sessionId = UUID.randomUUID().toString();
                String ssoSessionId = UUID.randomUUID().toString();
                getUserManagementService().makeNoteOfSuccessfulLogin(loginName, getClientIp(req), "-", ssoSessionId, appName);
                //Add userSession in the local Cache
                //getMemCacheService().setValue(sessionId, "1");
                //getSessionsCache().getEntry(sessionId);
                NewCookie ssoSessionCookie = new NewCookie("sso-value", ssoSessionId, "/", null, NewCookie.DEFAULT_VERSION, null, NewCookie.DEFAULT_MAX_AGE, false);    
                ret.data = authStatus.b != null && authStatus.b < 100 ? authStatus.b : authStatus.a;
                return Response.ok(ret).type("application/json;charset=UTF-8").cookie(ssoSessionCookie).build();
            } else {
                boolean statusLocks = false;
                if (authStatus.a == AuthenticationStatus.WRONG_PASSWORD)
                    statusLocks = true;
                getUserManagementService().makeNoteOfUnSuccessfulLogin(loginName, getClientIp(req), appName, authStatus.a.toString() , statusLocks);
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").entity(ret).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(e.getMessage()).build();
        }
    
    }
    /*********************************************************************************************/

    public static class IsAuthorizedUser_Request {
        public String privilege;
        public String password;
        public String username;
        public String userSubsCode;
    }
    
    @POST
    @Path("/getUserAuthorizationStatus")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response getUserAuthorizationStatus(
        final IsAuthorizedUser_Request req, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            if (!usrSession.privileges.contains(req.privilege)) {
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").
                        entity(AuthenticationStatus.AUTH_ERROR.toString()).build();
            }

            AuthenticationStatus authStatus = getUserManagementService().getUserAuthorizationStatus ((req.userSubsCode == null ? usrSession.subsCode : req.userSubsCode), (req.username == null ? usrSession.usrEmail : req.username), req.password, usrSession.appName, req.privilege);
            if (authStatus == AuthenticationStatus.SUCCESS) {
                JsonSingleValue ret = new JsonSingleValue(authStatus);
                return Response.ok().type("application/json;charset=UTF-8").entity(ret).build();
            } else {
                return Response.status(Response.Status.UNAUTHORIZED).type("application/json;charset=UTF-8").entity(authStatus.toString()).build();
            }
        }});
    }    

    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subscriberCookie, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        Func1<gr.neuropublic.base.UserSession, Response> javaClosureCasted = new Func1<gr.neuropublic.base.UserSession, Response>() {
            @Override
            public Response lambda(gr.neuropublic.base.UserSession t1) {
                return javaClosure.lambda((gr.neuropublic.Niva.services.UserSession)t1);
            }
            
        };
    
        return super.handleWebRequest(sessionId, ssoSessionId, subscriberCookie, "niva-session-id", "/Niva/", javaClosureCasted);
    }

/************* RTL_Subscriber services START **********************************/
    public static class FindByCode_Login_subscriber_request {
        public String shortName;
    }

    @POST
    @Path("/findByCode_Login_subscriber")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findByCode_Login_subscriber(
        final FindByCode_Login_subscriber_request r) 
    {
        try {
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
            List<RTL_Subscriber> retEntities= getUserManagementService().findByCode_Login_subscriber(r.shortName);
            ret.data = retEntities;
            ret.count = retEntities.size();
            String jsonStr = ret.toJson(new CryptoUtils(LoginServiceBase.class.getName()));
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(e.getMessage()).build();
        }        
    }
    
    public static class FindAllByCriteriaRange_Login_subscriber_request {
        public String srch_subs_short_name;
        public String srch_subs_vat;
        public String srch_subs_legal_name;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<String> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_Login_subscriber")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_Login_subscriber(final FindAllByCriteriaRange_Login_subscriber_request r) 
    {
        try {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_Login_subscriber_naked(r);
            String jsonStr = ret.toJson(new CryptoUtils(LoginServiceBase.class.getName()));
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(e.getMessage()).build();
        }        
    }

    @POST
    @Path("/findAllByCriteriaRange_Login_subscriber_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_Login_subscriber_count(final FindAllByCriteriaRange_Login_subscriber_request r) 
    {
        try {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_Login_subscriber_naked(r);
            String jsonStr = ret.toJson(new CryptoUtils(LoginServiceBase.class.getName()));
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(e.getMessage()).build();
        } 
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_Login_subscriber_naked(final FindAllByCriteriaRange_Login_subscriber_request r) {
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String exc_Id:r.exc_Id) {
                if (exc_Id == null)
                    continue;
                excludedIds.add(exc_Id);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<RTL_Subscriber> retEntities = getUserManagementService().findAllByCriteriaRange_Login_subscriber(r.srch_subs_short_name, r.srch_subs_vat, r.srch_subs_legal_name, range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getUserManagementService().findAllByCriteriaRange_Login_subscriber_count(r.srch_subs_short_name, r.srch_subs_vat, r.srch_subs_legal_name, excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }
    
/************* RTL_Subscriber services END **********************************/
    
}
