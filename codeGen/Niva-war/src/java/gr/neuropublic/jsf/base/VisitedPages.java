package gr.neuropublic.jsf.base;

import gr.neuropublic.jsf.util.JsfUtil;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Stack;

public  class  VisitedPages {


    Stack<Page> pages = new Stack<Page>();
    
    

    public void navigateForward(ControllerState currentPageState, String newPageUrl, String newPageLabel) {
        Page currentPage = pages.peek();
        currentPage.setState(currentPageState);
        pages.push(new Page(newPageUrl, newPageLabel, null));
    }

    public Page navigateBackward() {
        pages.pop(); // remove current page; crrent page has no state
        Page pageToReturn = pages.pop();
        pages.push(new Page(pageToReturn.getPageUrl(), pageToReturn.getPageLabel(),  null));
        return pageToReturn;
    }

    public Page navigateBackwardToUrl(String targetPageUrl) {
        Page lastPage;
        while (true) {
            lastPage = navigateBackward();
            if (lastPage.getPageUrl().equals(targetPageUrl))
                return  lastPage;
        }
    }
    
    public Stack<Page> getPages() {
        return pages;
    }
    
    
    public void reset() {
        pages.clear();
        pages.push(new Page("/index?faces-redirect=true", "", null));
    }
            
    

    public static  class Page {

        public Page(String url, String pageLabel, ControllerState state) {
            this.pageUrl = url;
            this.state = state;
            this.pageLabel = pageLabel;
        }

        private String pageUrl;
        public String getPageUrl() {return pageUrl;}
        public void setPageUrl(String val) {pageUrl = val;}

        private String pageLabel;
        public String getPageLabel() {return pageLabel;}
        public void setPageLabel(String val) {pageLabel = val;}
        
        
        private ControllerState state;
        public ControllerState getState() {return state;}
        public void setState(ControllerState val) {state = val;}
    }
    
    public abstract static class ControllerState implements Serializable {
        
        public abstract String getClassSignatureId();

        public void removeStateFromSession() {
            JsfUtil.getAndRemoveFromSession(getClassSignatureId());
        }
        public void storeOnSession() {
            JsfUtil.storeOnSession(getClassSignatureId(), this);
        }

    }
    

}

