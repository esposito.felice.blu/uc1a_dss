/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.jsf.base;

/**
 *
 * @author gmamais
 */
public enum LockKind {
    DisabledLock, ReadOnlyLock, InvisibleLock, DeleteLock, UpdateLock
}
