package gr.neuropublic.jsf.base;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

public abstract class LovHelper<T> {
    
    public abstract void saveEnity(T ent);

    public abstract List<T> filterData(Map filters, int[] range, int[] rowCount, String sortField, boolean sortOrder);
    
    public abstract boolean isEditPageVisible(String sEditPageId);
	
}
