/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.jsf.base;

import gr.neuropublic.jsf.util.JsfUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import gr.neuropublic.base.ChangeToCommit;
import java.util.Collections;
import org.primefaces.component.datatable.DataTable;

public abstract class TableLazyDataModel<T extends gr.neuropublic.validators.ICheckedEntity> extends org.primefaces.model.LazyDataModel {

    private static Logger logger = LoggerFactory.getLogger(TableLazyDataModel.class);
    
    
    private int first=0;
    private String sortField;
    private String sortOrder;
    private Map<String, String> filters;


	
    private DataTable dataTable;
    public DataTable getDataTable() {
            return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
            this.dataTable = dataTable;
    }
    
    private boolean showFilters = false;
    public void toggleFilters() {
        showFilters = !showFilters;
    }
    public void hideFilters() {
        showFilters = false;
    }
    
	
    Map<String, javax.el.ValueExpression> filters_CustomerTableCustomer;
    public void preRenderComponent() {
        List<org.primefaces.component.api.UIColumn> cols = getDataTable().getColumns();
        if (filters_CustomerTableCustomer == null) {
            filters_CustomerTableCustomer = new HashMap<String, javax.el.ValueExpression>();
            for(org.primefaces.component.api.UIColumn c : cols) {
                filters_CustomerTableCustomer.put(c.getClientId(), c.getValueExpression("filterBy"));
            }
        }
    
        for(org.primefaces.component.api.UIColumn c : cols) {
            org.primefaces.component.column.Column cl = (org.primefaces.component.column.Column)c;
            if (showFilters) {
                cl.setValueExpression("filterBy", filters_CustomerTableCustomer.get(cl.getClientId()));
            } else {
                cl.setValueExpression("filterBy", null);
                getDataTable().setFilters(new HashMap<String,String>());
            }
        }
    }
    
    
	
	
    public int getFirst() {
            return first;
    }

    public String getSortField() {
            return sortField;
    }

    public String getSortOrder() {
            return sortOrder;
    }

    public Map<String, String> getFilters() {
            return filters;
    }
    
    
    private ArrayList<ChangeToCommit<T>> changesToCommit = new ArrayList<ChangeToCommit<T>>();
    private ArrayList<ChangeToCommit<T>> prevChangesToCommit = new ArrayList<ChangeToCommit<T>>();
    public ArrayList<ChangeToCommit<gr.neuropublic.validators.ICheckedEntity>> getChangesToCommit()
    {
		ArrayList<ChangeToCommit<gr.neuropublic.validators.ICheckedEntity>> ret = new ArrayList<ChangeToCommit<gr.neuropublic.validators.ICheckedEntity>>();
		for(ChangeToCommit<T> cur:changesToCommit)
			ret.add(new ChangeToCommit<gr.neuropublic.validators.ICheckedEntity>(cur.getEntity(), cur.getWhen(), cur.getStatus()));
	
        return ret;
    }
	
    public void clearChanges() {
	for(ChangeToCommit<T> cur:changesToCommit) {
            if (cur.getStatus() == ChangeToCommit.ChangeStatus.NEW)
                prevChangesToCommit.add(cur);
        }
            
        changesToCommit.clear();
    }
	

    protected abstract ArrayList<T> filterData(Map filters, int[] range, int[] rowCount, String sortField, boolean sortOrder);
    public abstract IPendingChanges getPendingChangesInterface();
    public abstract T newEntity();
    public abstract boolean belongsToCurrentParrent(T entity);
    public abstract T refreshEntity(T entity);

    /*
     * Called by button's listener 
     * return the newlly created  entity
     */
    public abstract T onAdd();

    /*
     * Listener called by Prime Faces when pressing the
     * delete button
     */
    public ChangeToCommit<T> onDelete(T entity) {
        ChangeToCommit<T> cur = getChangedEntity(entity);
        ChangeToCommit<T> updatedChange = null;

        if (cur != null && cur.getStatus() == ChangeToCommit.ChangeStatus.NEW) {
            changesToCommit.remove(cur);
        } else {
            updatedChange = removeFromUpdate(entity);
            ChangeToCommit<T> newChange = new ChangeToCommit<T>(entity, ChangeToCommit.ChangeStatus.DELETE);
            getPendingChangesInterface().setPendingChanges(true);
            changesToCommit.add(newChange);
        }
        removeIfPresentFromHistory(entity);
        return updatedChange;
    }
    
    private void removeIfPresentFromHistory(T entity) {
        if (entity == null)
            return;
        ChangeToCommit<T> changeToRemove=null;
        for(ChangeToCommit<T> cur:prevChangesToCommit) {
            if (cur.getEntity().equals(entity)) {
                changeToRemove = cur;
                break;
            }
        }
        if (changeToRemove!=null)
            prevChangesToCommit.remove(changeToRemove);
    }

    public void removeFromDelete(T entity) {
        if (entity == null)
            return;
        
        ChangeToCommit<T> changeForDeletion = null;
        for (ChangeToCommit<T> change: changesToCommit) {
            if (change.getStatus() == ChangeToCommit.ChangeStatus.DELETE && 
                    change.getEntity().equals(entity)) {
                changeForDeletion = change;
                break;
            }
        }
        if (changeForDeletion!=null)
            changesToCommit.remove(changeForDeletion);
        
    }
	
    public ChangeToCommit<T> removeFromUpdate(T entity) {
        if (entity == null)
            return null;
        
        ChangeToCommit<T> changeForUpdate = null;
        for (ChangeToCommit<T> change: changesToCommit) {
            if (change.getStatus() == ChangeToCommit.ChangeStatus.UPDATE && 
                    change.getEntity().equals(entity)) {
                changeForUpdate = change;
                break;
            }
        }
        if (changeForUpdate!=null)
            changesToCommit.remove(changeForUpdate);
        return changeForUpdate;
    }
    
    public void restoreChangeForUpdate(ChangeToCommit<T> change) {
        if (change!=null)
            changesToCommit.add(change);
    }
	
    /*
     * Listener called by Prime Faces whenever
     * a control withing the datatable is changed
     */
    public void on_value_change(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && event.getNewValue().equals("") && (event.getOldValue() == null)) {
            return;
        }
        if ((event.getOldValue() != null) && event.getOldValue().equals("") && (event.getNewValue() == null)) {
            return;
        }

        /**
         * For pe:inputNumber we cast the old and new values to BigDecimal and
         * check for equality due to pe:inputNumber converter which triggers the
         * valueChangeListener. The converter changes the object type from Float
         * to Double.
         */
        if (org.primefaces.extensions.component.inputnumber.InputNumber.class.equals(event.getSource().getClass())) {
            BigDecimal oldValueBd = event.getOldValue() != null ? new BigDecimal(event.getOldValue().toString()) : null;
            BigDecimal newValueBd = event.getNewValue() != null ? new BigDecimal(event.getNewValue().toString()) : null;

            //Never use equals with BigDecimals. Use compareTo. equals cares for scale
            if ((oldValueBd != null && newValueBd != null && (oldValueBd.compareTo(newValueBd) == 0))
                    || (oldValueBd == null && newValueBd == null)) {
                //Do not  setPendingChanges(true)
                return;
            }
        }


        T changedEntity = (T)this.getRowData();
        if (changedEntity != null) {
            addToListAsUpdated(changedEntity);
        }
        
        //Notify GlobalsBean that pending changes exist
        getPendingChangesInterface().setPendingChanges(true);
    }
    
    public List<T> getMergedItems() {

        ArrayList<T> dbData = filterData(new HashMap(), null, null, null,false);
        return applyPendingChanges(dbData, null);
    }
    
    
    protected T current;
    public T getCurrent() {
        return current;
    }
    public void setCurrent(T current) {
        this.current = current;
    }

    @Override
    public List load(int first, int pageSize, String sortField,
            SortOrder sortOrder, Map filters) {
        
        this.first = first;
        this.sortField = sortField;
        this.sortOrder = (sortOrder == SortOrder.ASCENDING) ? "ascending" : "descending";
        this.filters = filters;

        int[] estimatedRowCount = {0};

        boolean ascendingOrder =
                (!sortOrder.equals(SortOrder.DESCENDING));

        ArrayList<T> dbData = filterData(filters, new int[]{first, first
                    + pageSize}, estimatedRowCount, sortField,
                ascendingOrder);

        ArrayList<T> finalData = applyPendingChanges(dbData, estimatedRowCount);

        this.setRowCount(estimatedRowCount[0]);

        if (!finalData.isEmpty()) {
            if (!finalData.contains(current)) {
                setCurrent(finalData.get(0));
            } else {
                ArrayList<T> list = (ArrayList<T>) finalData;
                int index = list.indexOf(current);
                setCurrent(list.get(index));
            }
        } else {
            setCurrent(null);
        }
        return finalData;
    }
    
    

    private ChangeToCommit<T> getChangedEntity(T entity) {
        for (ChangeToCommit<T> cur : changesToCommit) {
            if (cur.getEntity().equals(entity)) {
                return cur;
            }
        }
        return null;
    }
    
    public void markAsUpdated(T entity) {
        addToListAsUpdated(entity);
    }    

    public void addToListAsUpdated(T entity) {
        ChangeToCommit<T> cur = getChangedEntity(entity);

        if (cur == null) {
            ChangeToCommit<T> newChange = new ChangeToCommit<T>(entity, ChangeToCommit.ChangeStatus.UPDATE);
            changesToCommit.add(newChange);
        }
    }

    protected void addToListAsNew(T entity) {
        ChangeToCommit<T> newChange = new ChangeToCommit<T>(entity, ChangeToCommit.ChangeStatus.NEW);
        changesToCommit.add(newChange);

    }


    public ArrayList<T> applyPendingChanges(ArrayList<T> dbData, int[] estimatedRowCount) {
        ArrayList<T> finalData = new ArrayList<T>();
        populateInsertedRows(finalData, estimatedRowCount);
        Collections.reverse(finalData);
        for (T dbItem : dbData) {
            if (finalData.indexOf(dbItem) == -1)
                finalData.add(dbItem);
        }
//        finalData.addAll(dbData);
        populateUpdatedRows(finalData, estimatedRowCount);
        populateDeletedRows(finalData, estimatedRowCount);
        return finalData;
    }
    /*
     private void populateInsertedRows(List<T> data)
     {
     populateInsertedRows(data, null);
     }
     */

    private static <T> ChangeToCommit<T> getChangeFromList(List<ChangeToCommit<T>> lst, T entity) {
        
        for(ChangeToCommit<T> cur: lst) {
            if (cur.getEntity().equals(entity))
                return cur;
        }        
        
        return null;
    }
    
    private void refreshPreviouslySavedEntities() {
        ArrayList<ChangeToCommit<T>> copyList = new ArrayList<ChangeToCommit<T>>();
        copyList.addAll(prevChangesToCommit);

        for(ChangeToCommit<T> cur:copyList) {
            if ((cur.getStatus() == ChangeToCommit.ChangeStatus.NEW) && (belongsToCurrentParrent(cur.getEntity()))) {
                T refreshedEntity = this.refreshEntity(cur.getEntity());
                if (refreshedEntity != null)
                    cur.setEntity(refreshedEntity);
                else
                    prevChangesToCommit.remove(cur);
            }
        }
        
    }
    
    private void populateInsertedRows(List<T> data, int[] estimatedRowCount) {
        refreshPreviouslySavedEntities();
        for(ChangeToCommit<T> cur:prevChangesToCommit) {
            if ((cur.getStatus() == ChangeToCommit.ChangeStatus.NEW) && (belongsToCurrentParrent(cur.getEntity()))) {
                ChangeToCommit<T> curCurrentChanges = getChangeFromList(changesToCommit, cur.getEntity());
                
                if (curCurrentChanges!=null)
                    data.add(curCurrentChanges.getEntity());
                else
                    data.add(cur.getEntity());
                if (estimatedRowCount != null) {
                    estimatedRowCount[0] += 1;
                }
            }
        }
        

        for (ChangeToCommit<T> cur : changesToCommit) {
            if ((cur.getStatus() == ChangeToCommit.ChangeStatus.NEW) && (belongsToCurrentParrent(cur.getEntity()))) {
                data.add(cur.getEntity());
                if (estimatedRowCount != null) {
                    estimatedRowCount[0] += 1;
                }
            }
        }
    }

    private void populateDeletedRows(List data, int[] estimatedRowCount) {
        for (ChangeToCommit<T> cur : changesToCommit) {
            if (cur.getStatus() == ChangeToCommit.ChangeStatus.DELETE) {
                boolean deleted = data.remove(cur.getEntity());
                if (deleted && (estimatedRowCount != null)) {
                    estimatedRowCount[0] -= 1;
                }
            }
        }
    }

    private void populateUpdatedRows(ArrayList<T> data, int[] estimatedRowCount) {

        for (ChangeToCommit<T> cur : changesToCommit) {
            if (cur.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) {
                T curEntity = cur.getEntity();
                int idx = data.indexOf(curEntity);
                if (idx != -1) {
                    data.set(idx, curEntity);
                }
            }
        }
    }

    @Override
    public void setRowIndex(int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }
}