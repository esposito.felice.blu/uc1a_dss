package gr.neuropublic.neuropublic.jsf.validators;

import gr.neuropublic.jsf.util.JsfUtil;
import gr.neuropublic.shiro.ILoginController;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * EXAMPLE USE in xhtml: Enclose the following snippet inside an input jsf component to use validator
 * 
                <f:validator validatorId="tinValidator"/>
                <f:attribute name="checkGaia" value="TRUE" />
                <f:attribute name="moduleName" value="RegisterFarm" />
                <f:attribute name="subsId" value="1" />
 */
@FacesValidator("tinValidator")
public class TinValidator implements Validator {

    private Logger logger = LoggerFactory.getLogger(TinValidator.class);

    
    private ILoginController loginController;
    public ILoginController getLoginController() {
        if (loginController==null)
            loginController = (ILoginController)JsfUtil.accessBeanFromFacesContext("loginController");
        return loginController;
    }
    
    @Override
    public final void validate(final FacesContext context, final UIComponent component, final Object value)
            throws ValidatorException {
/*        if (value==null)
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, JsfUtil.getMsgString("tinValidator.tinErrorMessage", new Object[] {""}), ""));
        String afm = value.toString();
        if (!gr.neuropublic.validators.impl.TinValidator.validateTin(afm)) 
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, JsfUtil.getMsgString("tinValidator.tinErrorMessage", new Object[] {afm}), ""));
        try {
            if (component.getAttributes().get("checkGaia") != null && component.getAttributes().get("checkGaia").toString().toLowerCase().equals("true"))  {
                if (!getLoginController().vatIsRegisteredInGaia(afm))
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, JsfUtil.getMsgString("tinValidator.VatNotRegisteredInGAIA", new Object[] {afm}), ""));

                if (component.getAttributes().get("moduleName")!=null) {
                    String APP_NAME = component.getAttributes().get("moduleName").toString();
                    if (!getLoginController().vatIsRegisteredInModule(afm, APP_NAME)) 
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, JsfUtil.getMsgString("tinValidator.VatNotRegisteredInModule", new Object[] {afm, APP_NAME}), ""));
                    if (component.getAttributes().get("subsId") !=null) {
                        Integer subsId = Integer.parseInt(component.getAttributes().get("subsId").toString());
                        if (!getLoginController().isUserRegistedInModuleBySpecificSubscriber(afm, APP_NAME, subsId)) 
                            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, JsfUtil.getMsgString("tinValidator.UserRegistedInModuleBySpecificSubscriber", new Object[] {APP_NAME, afm}), ""));
                    }
                }
            }
        } catch (SQLException ex) {
            logger.error("Error in TinValidator", ex);
        }*/
        
        String afm = value == null ? null : value.toString();
        Boolean checkGaia = component.getAttributes().get("checkGaia") == null ? null : Boolean.parseBoolean(component.getAttributes().get("checkGaia").toString());
        String moduleName =  component.getAttributes().get("moduleName")==null ? null : component.getAttributes().get("moduleName").toString();
        Integer subsId    = component.getAttributes().get("subsId") == null ? null : Integer.parseInt(component.getAttributes().get("subsId").toString());
        
        validateTin(getLoginController(), afm, checkGaia, moduleName, subsId);
    }
    
    
    public static void validateTin(ILoginController loginController, String afm, Boolean checkGaia, 
            String moduleName, Integer subsId) throws ValidatorException {
        
        if (afm==null)
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, JsfUtil.getMsgString("tinValidator.tinErrorMessage", new Object[] {""}), ""));
        if (!gr.neuropublic.validators.impl.TinValidator.validateTin(afm)) 
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, JsfUtil.getMsgString("tinValidator.tinErrorMessage", new Object[] {afm}), ""));
        try {
            if (checkGaia != null && checkGaia)  {
                if (!loginController.vatIsRegisteredInGaia(afm))
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, JsfUtil.getMsgString("tinValidator.VatNotRegisteredInGAIA", new Object[] {afm}), ""));

                if (moduleName!=null) {
                    if (!loginController.vatIsRegisteredInModule(afm, moduleName)) 
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, JsfUtil.getMsgString("tinValidator.VatNotRegisteredInModule", new Object[] {afm, moduleName}), ""));
                    if (subsId !=null) {
                        if (!loginController.isUserRegistedInModuleBySpecificSubscriber(afm, moduleName, subsId)) 
                            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, JsfUtil.getMsgString("tinValidator.UserRegistedInModuleBySpecificSubscriber", new Object[] {moduleName, afm}), ""));
                    }
                }
            }
        } catch (SQLException ex) {
            LoggerFactory.getLogger(TinValidator.class).error("Error in TinValidator", ex);
        }
    }
}
