package gr.neuropublic.jsf.beans;

import java.io.Serializable;
import java.util.Map;

/**
 * Manages facelet templates.
 *
 * @author g_dimitriadis
 */
public class UIManager implements Serializable {
    
    private String DEFAULT_LAYOUT_KEY = "page";
    
    private Map<String, String> layoutList;

    public Map<String, String> getlayoutList() {
        return layoutList;
    }

    public void setlayoutList(Map<String, String> layoutList) {
        this.layoutList = layoutList;
    }

    /**
     * Returns appropriate template page according to layout argument
     *
     * @param layout
     * @return
     */
    public String templatePage(String layout) {
        return layoutList.get(layoutKey(layout));
    }

    /**
     * If layout is not the default layout do not render button
     *
     * @param layout
     * @return
     */
    public boolean btnRendered(String layout) {
        if (!layoutKey(layout).equals(DEFAULT_LAYOUT_KEY)) {
            return false;
        }

        return true; //default behaviour
    }
    
    /**
     * If layout argument is empty return default layout
     * 
     * @param layout
     * @return 
     */
    private String layoutKey(final String layout) {
        if (layout == null || layout.trim().equals("")) {
            return DEFAULT_LAYOUT_KEY;
        } else {
            return layout;
        }
    }
    
    
}