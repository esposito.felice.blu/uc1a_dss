package gr.neuropublic.jsf.util;

import java.io.Serializable;

/**
 *
 * @author g_dimitriadis
 */
public class NavigationHandler implements Serializable {
    
    private boolean redirectionEnabled;

    public boolean isRedirectionEnabled() {
        return redirectionEnabled;
    }

    public void setRedirectionEnabled(boolean redirectionEnabled) {
        this.redirectionEnabled = redirectionEnabled;
    }
    
}
