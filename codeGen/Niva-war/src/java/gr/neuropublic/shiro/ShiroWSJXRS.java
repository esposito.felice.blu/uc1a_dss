package gr.neuropublic.shiro;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;


@Path("shiro")
public class ShiroWSJXRS {

    @GET
    @Produces("text/plain")
    @Path("am-I-authenticated")
    public String amIAuthenticated() {
        Subject subject = SecurityUtils.getSubject();
        return internalIsAuthenticated(subject);
    }

    @GET
    @Produces("text/plain")
    @Path("is-authenticated")
    public String isAuthenticated(@QueryParam("session-id") String sessionId) {
        Subject subject = (new Subject.Builder()).sessionId(sessionId).buildSubject();
        return internalIsAuthenticated(subject);
    }

    private String internalIsAuthenticated(Subject subject) {
        return String.valueOf(subject.isAuthenticated());
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
