package gr.neuropublic.shiro;

import org.apache.shiro.web.filter.PathMatchingFilter;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.ExecutionException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.mgt.FilterChainResolver;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.apache.shiro.web.subject.WebSubject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.Cookie;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.Context;
import javax.naming.NamingException;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.mutil.base.ExceptionAdapter;
import gr.neuropublic.mutil.jdbc.JdbcUtils;

public class SSOffShiroFilter extends PathMatchingFilter {

    private static final Logger l = LoggerFactory.getLogger(SSOffShiroFilter.class);

    private String getSSOSessionId(ServletRequest request) {
        Cookie[] cookies = ((HttpServletRequest)request).getCookies();
        if (cookies != null) {
            for(int i=0; i<cookies.length; i++) {
                if (cookies[i].getName().equalsIgnoreCase("sso-value")) return cookies[i].getValue(); 
            }
        }
        return null;
    }

    private String appName ;
    private void ensureAppName() throws NamingException {
        if (this.appName == null) {
            InitialContext ic = new InitialContext();
            Context env = (Context) ic.lookup("java:comp/env");
            appName = (String) env.lookup("appName");
        }
    }

    DataSource dataSource;
    private void ensureDataSource() throws NamingException {
        if (dataSource == null) {
            InitialContext ic = new InitialContext();
            String dataSourceURI = "java:/usermgmnt"; // userMngmtDataSource.getDataSourceURI();
            l.info("looking for the '{}' datasource", dataSourceURI);
            dataSource = (DataSource) ic.lookup(dataSourceURI);
            l.info("data source is: "+dataSource);
        }
    }

    private boolean sessionSSOff(String ssoSessionId) throws NamingException, SQLException {
        if (ssoSessionId==null) throw new RuntimeException();
        ensureDataSource();
        Connection conn = dataSource.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("SELECT COUNT(*) FROM subscription.ss_user_sessions WHERE usrs_ssosession=? AND usrs_logoutsse IS NOT NULL");
            ps.setString(1, ssoSessionId);
            rs = ps.executeQuery();
            boolean haveBeenHereBefore = false;
            int signedOffCount = 0;
            while (rs.next()) {
                if (haveBeenHereBefore) throw new RuntimeException(ssoSessionId);
                signedOffCount = rs.getInt(1);
                haveBeenHereBefore = true;
            }
            boolean retValue = signedOffCount>0;
            return retValue;
        } finally {
            JdbcUtils.closeResultSet(rs);
            JdbcUtils.closeStatement(ps);
            JdbcUtils.closeConnection(conn);
        }
    }

     private boolean zombieSession(String ssoSessionId) throws NamingException, SQLException {
        if (ssoSessionId==null) throw new RuntimeException();
        ensureDataSource();
        Connection conn = dataSource.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("SELECT COUNT(*) FROM subscription.ss_user_sessions WHERE usrs_ssosession=? AND usrs_app=? AND usrs_logoutsse IS NOT NULL");
            ensureAppName();
            ps.setString(1, ssoSessionId);
            ps.setString(2, appName);
            rs = ps.executeQuery();
            boolean haveBeenHereBefore = false;
            int signedOffCount = 0;
            while (rs.next()) {
                if (haveBeenHereBefore) throw new RuntimeException(ssoSessionId);
                signedOffCount = rs.getInt(1);
                haveBeenHereBefore = true;
            }
            boolean retValue = signedOffCount==0;
            return retValue;
        } finally {
            JdbcUtils.closeResultSet(rs);
            JdbcUtils.closeStatement(ps);
            JdbcUtils.closeConnection(conn);
        }
    }

    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) {
        /*if (System.currentTimeMillis() % 100 != 0) {
            l.info("not entering onPreHandle");
            return true;
            }*/
        String ssoSessionId = getSSOSessionId(request);
        l.info("SSOffShiroFilter :: onPreHandle(ssoSessionId = {}", ssoSessionId);
        try {
            Subject subject = SecurityUtils.getSubject();
            if ((ssoSessionId!=null) && (zombieSession (ssoSessionId))) {
                if (subject.isAuthenticated()) {
                    l.info("loging out zombie subject: {}", subject.getPrincipal().toString());
                    subject.logout();
                }
            }
            else if ((ssoSessionId!=null) && (sessionSSOff (ssoSessionId))) {
                if (subject.isAuthenticated()) {
                    l.info("loging out subject: {}", subject.getPrincipal().toString());
                    subject.logout();
                }
            }
            return true;
        } catch (NamingException e) {
            throw new ExceptionAdapter(e);
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
