package gr.neuropublic.shiro;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.Context;
import javax.naming.NamingException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.config.ConfigurationException;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.apache.shiro.util.JdbcUtils;
import org.apache.shiro.SecurityUtils;

import org.apache.commons.dbutils.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import gr.neuropublic.mutil.base.Util;

import gr.neuropublic.mutil.base.ExceptionAdapter;


import gr.neuropublic.jsf.util.JsfUtil;
import gr.neuropublic.jsf.util.FacesUtil;
import gr.neuropublic.gaia.usermngmt.util.UserMngmtUtil;
import gr.neuropublic.gaia.usermngmt.util.LogoutCode;


public class GaiaSSOJdbcRealm extends JdbcRealm {

    private static Logger l = LoggerFactory.getLogger(GaiaSSOJdbcRealm.class);

    private String getAppName() {
        String[] components = FacesUtil.getContextPath().split("/");
        return components[components.length - 1];
    }



    // we're using a salted realm so we can no longer be using the default ( DEFAULT_AUTHENTICATION_QUERY )

    protected static final String OVERRIDEN_SALTED_AUTHENTICATION_QUERY =
        "SELECT a.user_password, a.user_password_salt FROM subscription.ss_users a WHERE a.user_email = ?"; 

    protected static final String OVERRIDEN_USER_ROLES_QUERY = 
        "SELECT a.syro_id FROM subscription.ssv_user_system_roles  a WHERE a.user_email = ? AND a.rost_id = 0";


    protected static final String OVERRIDEN_PERMISSIONS_QUERY =
        "SELECT a.sypr_name FROM subscription.ssv_system_roles_privileges a WHERE a.syro_id = ? AND a.modu_name=?";


    private void ensureDataSource() {
        if (this.dataSource ==null) {
            // UserMngmtDataSource userMngmtDataSource = (UserMngmtDataSource) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("userMngmtDataSource");
            try {
                InitialContext ic = new InitialContext();
                String dataSourceURI = JsfUtil.getFromEnv(LoginController.USER_MNGMNT_URI_ENV_KEY); // userMngmtDataSource.getDataSourceURI();
                l.debug("looking for the '{}' datasource", dataSourceURI);
                DataSource dataSource = (DataSource) ic.lookup(dataSourceURI); // was: ic.lookup("jdbc/auth");
                l.debug("data source is: "+dataSource);
                this.setDataSource(dataSource);
                this.setSaltStyle(JdbcRealm.SaltStyle.COLUMN);
            } catch (NamingException e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage()); // if we can't find the source, we should panic
            }
        }
    }


    public GaiaSSOJdbcRealm() {
        super();
        setAuthenticationQuery(OVERRIDEN_SALTED_AUTHENTICATION_QUERY);
        setUserRolesQuery     (OVERRIDEN_USER_ROLES_QUERY           );
        setPermissionsQuery   (OVERRIDEN_PERMISSIONS_QUERY          );
        //        ensureDataSource(userMngmtDataSource.getDataSourceURI()); // we can't set the data source here as we aren't in FacesContext, hence the need to 
                                                                            // redefine the JdbcRealm methods.
    }

  private void makeNoteOfLogout(String userName, String userIp, String userSessionId) throws SQLException, NamingException {
        Connection        conn = null;
        PreparedStatement pstm = null;
        try {
            ensureDataSource();
            conn = dataSource.getConnection();
            String stmtStr = "UPDATE subscription.ss_user_sessions SET usrs_logoutsse=?, uslc_id="+LogoutCode.USER.getCode()+" "+
                             "WHERE usrs_email=? AND usrs_ip=? AND usrs_session=? AND usrs_app=?";

            pstm  = conn.prepareStatement(stmtStr);

            pstm.setInt        (1, Util.secondsSinceTheEpoch());
            pstm.setString     (2, userName);
            pstm.setString     (3, userIp);
            pstm.setString     (4, userSessionId);
            pstm.setString     (5, getAppName());
            pstm.executeUpdate();
        } finally {
            DbUtils.closeQuietly(conn, pstm, null);
        }        
    }

    /****************************************************************/

    public void onLogout(PrincipalCollection principals) {
        try {
            {
                l.info("onLogout called");
                List principalsL = principals != null ? principals.asList(): new ArrayList();
                for (Object principal : principalsL) {
                    l.info("**************** principal : {}", principal.toString());
                }
                l.info("**************** host: {}", (SecurityUtils.getSubject() != null) && (SecurityUtils.getSubject().getSession() != null) ?
                                                     SecurityUtils.getSubject().getSession().getHost() : null);
                l.info("**************** session: {}", (SecurityUtils.getSubject() != null) && (SecurityUtils.getSubject().getSession() != null) &&
                                                        (SecurityUtils.getSubject().getSession().getId() != null) ?  SecurityUtils.getSubject().getSession().getId().toString() : null);
            }
            super.onLogout(principals);


            makeNoteOfLogout(principals != null ? (String) principals.getPrimaryPrincipal() : null,
                    (SecurityUtils.getSubject() != null) && (SecurityUtils.getSubject().getSession() != null) ? SecurityUtils.getSubject().getSession().getHost() : null,
                    (SecurityUtils.getSubject() != null) && (SecurityUtils.getSubject().getSession() != null) &&
                    (SecurityUtils.getSubject().getSession().getId() != null) ? SecurityUtils.getSubject().getSession().getId().toString() : null);
                         

        } catch (Exception e) {
            throw new ExceptionAdapter(e);
        }
    }


    public String getUsernameOfLoggedInSession(String sessionId) {
        // int i=0; if (i==0) return "foo";
        l.debug("getUsernameOfLoggedInSession({})", sessionId);
        Connection conn = null;
        String retValue = null;
        try {
            ensureDataSource();
            conn = dataSource.getConnection();
            retValue = UserMngmtUtil.getUsernameOfLoggedInSession(conn, sessionId);
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);            
        } finally {
            DbUtils.closeQuietly(conn);
            l.debug("username of logged-in session is : {}", retValue);
            return retValue;
        }  
    }

    /****************************************************************/
    @Override
    protected GaiaAuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        ensureDataSource();

        //UsernamePasswordToken upToken = (UsernamePasswordToken) token;
        GaiaAuthenticationToken upToken = (GaiaAuthenticationToken) token;
        if (upToken.isTypeOpportunisticSSO()) {
            String username = getUsernameOfLoggedInSession(upToken.sessionId);
            if (username != null) {
                try { assertNoConcurrentLogin(username); } catch (NamingException ne) { throw new ExceptionAdapter(ne); }
                return GaiaAuthenticationInfo.ssoSuccessfulInfo(username, getName());
            }
            else return null; 
        } else {
            String username = upToken.getUsername();
            // Null username is invalid
            if (username == null) {
                throw new AccountException("Null usernames are not allowed by this realm.");
            }
    
            Connection conn = null;
            GaiaAuthenticationInfo info = null;
            try {
                conn = dataSource.getConnection();
    
                String password = null;
                String salt = null;
                switch (saltStyle) {
                case NO_SALT:
                    password = getPasswordForUser(conn, username)[0];
                    break;
                case CRYPT:
                    // TODO: separate password and hash from getPasswordForUser[0]
                    throw new ConfigurationException("Not implemented yet");
                    //break;
                case COLUMN:
                    String[] queryResults = getPasswordForUser(conn, username);
                    password = queryResults[0];
                    salt = queryResults[1];
                    break;
                case EXTERNAL:
                    password = getPasswordForUser(conn, username)[0];
                    salt = getSaltForUser(username);
                }
    
                if (password == null) {
                    throw new UnknownAccountException("No account found for user [" + username + "]");
                }
    
                info = new GaiaAuthenticationInfo(username, password.toCharArray(), getName(), false);
                
                if (salt != null) {
                    info.setCredentialsSalt(ByteSource.Util.bytes(salt));
                }
    
            } catch (SQLException e) {
                final String message = "There was a SQL error while authenticating user [" + username + "]";
                if (l.isErrorEnabled()) {
                    l.error(message, e);
                }
    
                // Rethrow any SQL errors as an authentication exception
                throw new AuthenticationException(message, e);
            } finally {
                DbUtils.closeQuietly(conn);
            }
            // try { assertNoConcurrentLogin(username); } catch (NamingException ne) { throw new ExceptionAdapter(ne); }
            return info;
        }
    }

    private void assertNoConcurrentLogin(String username) throws ConcurrentAccessException, NamingException {
        Connection conn = null;
        try {
            ensureDataSource();
            conn = dataSource.getConnection();
            // if (UserMngmtUtil.existsConcurrentLogin(conn, username, appName))
            if (UserMngmtUtil.existsConcurrentLoginWtSideEffects(conn, username, getAppName()))
                throw new ConcurrentAccessException();
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
    }

    private String[] getPasswordForUser(Connection conn, String username) throws SQLException {
        ensureDataSource();
        String[] result;
        boolean returningSeparatedSalt = false;
        switch (saltStyle) {
        case NO_SALT:
        case CRYPT:
        case EXTERNAL:
            result = new String[1];
            break;
        default:
            result = new String[2];
            returningSeparatedSalt = true;
        }
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(authenticationQuery);
            ps.setString(1, username);

            // Execute query
            rs = ps.executeQuery();

            // Loop over results - although we are only expecting one result, since usernames should be unique
            boolean foundResult = false;
            while (rs.next()) {

                // Check to ensure only one row is processed
                if (foundResult) {
                    throw new AuthenticationException("More than one user row found for user [" + username + "]. Usernames must be unique.");
                }

                result[0] = rs.getString(1);
                if (returningSeparatedSalt) {
                    result[1] = rs.getString(2);
                }

                foundResult = true;
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }

        return result;
    }



    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        l.info("**************** context is: "+FacesUtil.getContextPath()+" **** appName is: "+getAppName());
        ensureDataSource();
        //null usernames are invalid
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }

        String username = (String) getAvailablePrincipal(principals);

        Connection conn = null;
        Set<String> roleNames = null;
        Set<String> permissions = null;
        try {
            conn = dataSource.getConnection();

            // Retrieve roles and permissions from database
            roleNames = getRoleNamesForUser(conn, username);
            if (permissionsLookupEnabled) {
                permissions = getPermissions(conn, username, roleNames);
            }

        } catch (SQLException e) {
            final String message = "There was an SQL error while authorizing user [" + username + "]";
            if (l.isErrorEnabled()) {
                l.error(message, e);
            }

            // Rethrow any SQL errors as an authorization exception
            throw new AuthorizationException(message, e);
        } finally {
            DbUtils.closeQuietly(conn);
        }

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roleNames);
        info.setStringPermissions(permissions);
        return info;

    }

    @Override
    protected Set<String> getRoleNamesForUser(Connection conn, String username) throws SQLException {
        ensureDataSource();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Set<String> roleNames = new LinkedHashSet<String>();
        try {
            ps = conn.prepareStatement(userRolesQuery);
            ps.setString(1, username);

            // Execute query
            rs = ps.executeQuery();

            // Loop over results and add each returned role to a set
            while (rs.next()) {

                String roleName = rs.getString(1);

                // Add the role to the list of names if it isn't null
                if (roleName != null) {
                    roleNames.add(roleName);
                } else {
                    if (l.isWarnEnabled()) {
                        l.warn("Null role name found while retrieving role names for user [" + username + "]");
                    }
                }
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
        return roleNames;
    }

    @Override
    protected Set<String> getPermissions(Connection conn, String username, Collection<String> roleNames) throws SQLException {
        ensureDataSource();
        PreparedStatement ps = null;
        Set<String> permissions = new LinkedHashSet<String>();
        try {
            ps = conn.prepareStatement(permissionsQuery);
            for (String roleName : roleNames) {

                ps.setInt   (1, Integer.valueOf(roleName));
                ps.setString(2, getAppName());

                ResultSet rs = null;

                try {
                    // Execute query
                    rs = ps.executeQuery();

                    // Loop over results and add each returned role to a set
                    while (rs.next()) {

                        String permissionString = rs.getString(1);

                        // Add the permission to the set of permissions
                        permissions.add(permissionString);
                    }
                } finally {
                    DbUtils.closeQuietly(rs);
                }

            }
        } finally {
            DbUtils.closeQuietly(ps);
        }

        return permissions;
    }

}

 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
