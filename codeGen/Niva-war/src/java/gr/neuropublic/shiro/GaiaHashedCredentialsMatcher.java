package gr.neuropublic.shiro;

import org.apache.shiro.authc.credential.HashedCredentialsMatcher;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.Context;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.shiro.authc.ConcurrentAccessException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.List;

import org.apache.shiro.SecurityUtils;

import gr.neuropublic.mutil.base.ExceptionAdapter;
import gr.neuropublic.mutil.jdbc.JdbcUtils;
import gr.neuropublic.jsf.util.JsfUtil;
import gr.neuropublic.jsf.util.FacesUtil;
import gr.neuropublic.gaia.usermngmt.util.UserMngmtUtil;

public class GaiaHashedCredentialsMatcher extends HashedCredentialsMatcher {

    private static Logger l = LoggerFactory.getLogger(GaiaHashedCredentialsMatcher.class);

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        boolean ssoHit = ((GaiaAuthenticationInfo) info).sessionExistsInSSOStore;
        if (ssoHit) return true;
        else {
            boolean credentialsMatch = super.doCredentialsMatch(token, info);
            if (!credentialsMatch) return false;
            else {
                assertNoConcurrentLogin( ((GaiaAuthenticationToken) token).getUsername() );
                return true;
            }
        }
    }

    private DataSource dataSource;
    private void setDataSource(DataSource dataSource) { this.dataSource = dataSource; }
    private void ensureDataSource() {
        if (this.dataSource ==null) {
            // UserMngmtDataSource userMngmtDataSource = (UserMngmtDataSource) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("userMngmtDataSource");
            try {
                InitialContext ic = new InitialContext();
                String dataSourceURI = JsfUtil.getFromEnv(LoginController.USER_MNGMNT_URI_ENV_KEY); // userMngmtDataSource.getDataSourceURI();
                l.debug("looking for the '{}' datasource", dataSourceURI);
                DataSource dataSource = (DataSource) ic.lookup(dataSourceURI); // was: ic.lookup("jdbc/auth");
                l.debug("data source is: "+dataSource);
                this.setDataSource(dataSource);
            } catch (NamingException e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage()); // if we can't find the source, we should panic
            }
        }
    }


    private String getAppName() {
        String[] components = FacesUtil.getContextPath().split("/");
        return components[components.length - 1];
    }


    private void assertNoConcurrentLogin(String username) throws ConcurrentAccessException {
        Connection conn = null;
        try {
            ensureDataSource();
            conn = dataSource.getConnection();
            if (UserMngmtUtil.existsConcurrentLoginWtSideEffects(conn, username, getAppName()))
                throw new ConcurrentAccessException();
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);
        } finally {
            JdbcUtils.closeConnection(conn);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
