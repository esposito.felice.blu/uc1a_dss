/// <reference path="../DefinitelyTyped/jquery/jquery.d.ts"/>
/// <reference path="../DefinitelyTyped/angularjs/angular.d.ts"/>
/// <reference path="openlayers3.d.ts"/>
/// <reference path="FileSaver.ts"/>
/// <reference path="utils.ts"/>
/// <reference path="sprintf.d.ts"/>
/// <reference path="Controllers/BaseController.ts"/>
/// <reference path="base64Utils.ts"/>
/// <reference path="geoLib.ts"/>
/// <reference path="NpMapLayers.ts"/>
/// <reference path="NpMapCustomControls.ts"/>
;
//@p.tsagkis addition end
var NpGeoGlobals;
(function (NpGeoGlobals) {
    NpGeoGlobals.DEFAULT_OPACITY = 0.8;
    NpGeoGlobals.fullScreenState = false;
    /**
     * @p.tsagkis
     * this is the gps/kml vector layer to place any drop features regarding kml and gpx
     * file drops
     * should be declared somewhere but didnt have time to dig it so
     * set it here gloabally
     */
    NpGeoGlobals.importsLayer = new ol.layer.Vector({
        source: new ol.source.Vector({}),
        style: function (feature) {
            var textStyle = new ol.style.Text({
                textAlign: "center",
                textBaseline: "middle",
                font: "bold 10px Verdana",
                text: feature.getGeometryName(),
                fill: new ol.style.Fill({
                    color: 'rgba(0,0,0,1)'
                }),
                stroke: new ol.style.Stroke({
                    color: 'rgba(255,255,255,1)',
                    width: 3
                }),
                offsetX: 0,
                offsetY: 0,
                rotation: 0
            });
            var strokeStyle = new ol.style.Stroke({
                color: '#FF0000',
                width: 3
            });
            var fillStyle = new ol.style.Fill({
                color: 'rgba(255,0,255,0.5)'
            });
            var styleToRet = {
                'Point': new ol.style.Style({
                    image: new ol.style.Circle({
                        fill: fillStyle,
                        radius: 6,
                        stroke: strokeStyle
                    }),
                    text: textStyle
                }),
                'LineString': new ol.style.Style({
                    stroke: strokeStyle,
                    text: textStyle
                }),
                'Polygon': new ol.style.Style({
                    fill: fillStyle,
                    stroke: strokeStyle
                }),
                'MultiPoint': new ol.style.Style({
                    image: new ol.style.Circle({
                        fill: fillStyle,
                        radius: 6,
                        stroke: strokeStyle
                    })
                }),
                'MultiLineString': new ol.style.Style({
                    stroke: strokeStyle,
                    text: textStyle
                }),
                'MultiPolygon': new ol.style.Style({
                    fill: fillStyle,
                    stroke: strokeStyle
                })
            };
            return styleToRet[feature.getGeometry().getType()];
        }
    });
    //set the layerId for identification
    NpGeoGlobals.importsLayer.set("layerId", "importsLayer");
    //and this is the split line layer
    NpGeoGlobals.splitLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            useSpatialIndex: true //important for split interaction to work. Though , true should be the default
        }),
        style: function (f) {
            return [
                new ol.style.Style({
                    stroke: new ol.style.Stroke({ color: '#ffcc33', width: 4 }),
                    fill: new ol.style.Fill({
                        color: 'rgba(0, 0, 255, 0.3)'
                    })
                }),
                new ol.style.Style({
                    image: new ol.style.RegularShape({ radius: 8, points: 8, fill: new ol.style.Fill({ color: 'rgba(0, 255, 0, 0.8)' }) }),
                    geometry: function (f) {
                        return new ol.geom.MultiPoint([f.getGeometry().getFirstCoordinate(), f.getGeometry().getLastCoordinate()]);
                    }
                })
            ];
        }
    });
    //set the layerId for identification
    NpGeoGlobals.splitLayer.set("layerId", "splitLayer");
    //and this is the split helper polygon layer
    NpGeoGlobals.splitPolyLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            useSpatialIndex: true //important for split interaction to work. Though , true should be the default
        }),
        style: function (f) {
            return [
                new ol.style.Style({
                    stroke: new ol.style.Stroke({ color: 'rgba(0, 96, 28, 0.5)', width: 2 }),
                    fill: new ol.style.Fill({
                        color: 'rgba(0, 0, 255, 0.5)'
                    })
                }),
                new ol.style.Style({
                    image: new ol.style.RegularShape({ radius: 8, points: 8, fill: new ol.style.Fill({ color: 'rgba(255, 0, 0, 0.3)' }) }),
                    geometry: function (f) {
                        return new ol.geom.MultiPoint(f.getGeometry().getLinearRing(0).getCoordinates());
                    }
                })
            ];
        }
    });
    //set the npName for display purposes
    NpGeoGlobals.splitPolyLayer.npName = "Ψηφ. Πολύγωνο";
    //set the layerId for identification
    NpGeoGlobals.splitPolyLayer.set("layerId", "splitPolyLayer");
    // Module-global utility functions
    function geoJSON_to_OL3feature(config) {
        var o = config.obj;
        if (isVoid(o))
            return null;
        var coordinateSystem = fallback(config.coordinateSystem, 'EPSG:3857');
        var format = new ol.format.GeoJSON();
        var geom = format.readGeometry(o);
        geom.transform(ol.proj.get(coordinateSystem), ol.proj.get('EPSG:3857'));
        var nf = new ol.Feature();
        if (!isVoid(config.name))
            nf.setGeometryName(config.name);
        nf.setGeometry(geom);
        return nf;
    }
    NpGeoGlobals.geoJSON_to_OL3feature = geoJSON_to_OL3feature;
    function geoJSON_to_OL3Geom(geoJson, coordinateSystem) {
        if (isVoid(geoJson))
            return null;
        var format = new ol.format.GeoJSON();
        var geom = format.readGeometry(geoJson);
        geom.transform(ol.proj.get(coordinateSystem), ol.proj.get('EPSG:3857'));
        return geom;
    }
    NpGeoGlobals.geoJSON_to_OL3Geom = geoJSON_to_OL3Geom;
    function geoJSONTurf_to_OL3Geom(geoJson, coordinateSystem) {
        if (isVoid(geoJson))
            return null;
        var format = new ol.format.GeoJSON();
        var geom = format.readGeometry(geoJson.geometry);
        geom.transform(ol.proj.get(coordinateSystem), ol.proj.get('EPSG:3857'));
        return geom;
    }
    NpGeoGlobals.geoJSONTurf_to_OL3Geom = geoJSONTurf_to_OL3Geom;
    function OL3feature_to_geoJSON_Turf(f, coordinateSystem) {
        //console.log("OL3feature_to_geoJSON f value", f);
        if (isVoid(f))
            return null;
        var format = new ol.format.GeoJSON();
        var g = f.getGeometry();
        if (isVoid(g))
            return null;
        g = g.clone();
        g.transform(ol.proj.get('EPSG:3857'), ol.proj.get(coordinateSystem));
        var geojson = JSON.parse(format.writeGeometry(g));
        var turfObj = {
            "type": "Feature",
            "properties": {
                "fill": "#00f"
            },
            "geometry": {
                "type": geojson.type,
                "coordinates": geojson.coordinates
            }
        };
        return turfObj;
    }
    NpGeoGlobals.OL3feature_to_geoJSON_Turf = OL3feature_to_geoJSON_Turf;
    function OL3feature_to_geoJSON(f, coordinateSystem) {
        //console.log("OL3feature_to_geoJSON f value", f);
        if (isVoid(f))
            return null;
        var format = new ol.format.GeoJSON();
        var g = f.getGeometry();
        if (isVoid(g))
            return null;
        g = g.clone();
        g.transform(ol.proj.get('EPSG:3857'), ol.proj.get(coordinateSystem));
        return JSON.parse(format.writeGeometry(g));
    }
    NpGeoGlobals.OL3feature_to_geoJSON = OL3feature_to_geoJSON;
    function OL3feature_to_coordinates(f) {
        //console.log("OL3feature_to_coordinates f value", f);
        var polygonData = [];
        var g = f.getGeometry();
        if (g instanceof ol.geom.Polygon) {
            // We are already in 3857 - but just in case, pass it on...
            var geoJSON = OL3feature_to_geoJSON(f, 'EPSG:3857');
            geoJSON.coordinates.forEach(function (arr) {
                var pointsOfPolygonPart = [];
                arr.forEach(function (point) {
                    pointsOfPolygonPart.push(point[0]);
                    pointsOfPolygonPart.push(point[1]);
                });
                polygonData.push(pointsOfPolygonPart);
            });
        }
        return polygonData;
    }
    NpGeoGlobals.OL3feature_to_coordinates = OL3feature_to_coordinates;
    function areaCalculation(arrCoords, failResponse, successResponse) {
        // http://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
        var sum = 0.0;
        if (arrCoords.length < 5)
            // shouldn't happen, but better safe than sorry
            return failResponse();
        var x1 = arrCoords[0];
        var y1 = arrCoords[1];
        for (var i = 2; i < arrCoords.length; i += 2) {
            var x2 = arrCoords[i];
            var y2 = arrCoords[i + 1];
            sum += (x2 - x1) * (y2 + y1);
            x1 = x2;
            y1 = y2;
        }
        return successResponse(sum);
    }
    NpGeoGlobals.areaCalculation = areaCalculation;
    function polyArea(arrCoords) {
        return areaCalculation(arrCoords, function () { return 0.0; }, function (sum) { return sum / 2.0; });
    }
    NpGeoGlobals.polyArea = polyArea;
    function isClockWise(arrCoords) {
        return areaCalculation(arrCoords, function () { return false; }, function (sum) { return sum > 0; });
    }
    NpGeoGlobals.isClockWise = isClockWise;
    function isMultiPolygon(arrArrPolys) {
        var directions = arrArrPolys.map(function (arrCoords) { return isClockWise(arrCoords); });
        var clockWise = directions.filter(function (x) { return x; });
        var counterClockWise = directions.filter(function (x) { return x == false; });
        if (counterClockWise.length > 1)
            return true;
    }
    NpGeoGlobals.isMultiPolygon = isMultiPolygon;
    function calculate_area_of_coordinates(arrArrays) {
        var area = null;
        if (!isVoid(arrArrays)) {
            var feature = coordinates_to_OL3feature(arrArrays);
            var geom = feature.getGeometry();
            area = feature.getGeometry().getArea();
        }
        return area;
    }
    NpGeoGlobals.calculate_area_of_coordinates = calculate_area_of_coordinates;
    /**
     * This is originaly written by t.tsiodras but I have some doubts about it.
     * @TODO p.tsagkis comment
     * This is forcing to return POLYGON
     * in case of multipolygon or any other geometry type, this would create errors
     * It might needs recustruction.
     * For the time being is not crucial
     * Just keep in mind
     * @param arrArrays
     */
    function coordinates_to_OL3feature(arrArrays) {
        var feature = new ol.Feature();
        if (!isVoid(arrArrays)) {
            var format = new ol.format.WKT();
            var textParts = [];
            arrArrays.forEach(function (arr) {
                var parts = [];
                for (var i = 0; i < arr.length; i += 2) {
                    parts.push(arr[i] + ' ' + arr[i + 1]);
                }
                var lastIdx = arr.length - 2;
                if (arr[0] !== arr[lastIdx] || arr[1] !== arr[lastIdx + 1])
                    parts.push(arr[0] + ' ' + arr[1]);
                textParts.push("(" + parts.join(",") + ")");
            });
            if (textParts.length > 0) {
                var text = 'POLYGON(' + textParts.join(",") + ")";
                var geometryTmp = format.readGeometry(text);
                feature.setGeometryName('');
                feature.setGeometry(geometryTmp);
            }
        }
        return feature;
    }
    NpGeoGlobals.coordinates_to_OL3feature = coordinates_to_OL3feature;
    // http://stackoverflow.com/questions/14484787/wrap-text-in-javascript
    //
    // Splits a string on '\n' and ' ' so that it's at most 'width' chars wide.
    function stringDivider(str, width, spaceReplacer) {
        if (str.length > width) {
            var p = width;
            for (; p > 0 && (str[p] != ' ' && str[p] != '-'); p--) {
            }
            if (p > 0) {
                var left;
                if (str.substring(p, p + 1) == '-') {
                    left = str.substring(0, p + 1);
                }
                else {
                    left = str.substring(0, p);
                }
                var right = str.substring(p + 1);
                return left + spaceReplacer + stringDivider(right, width, spaceReplacer);
            }
        }
        return str;
    }
    function getTextOfFeatureForResolution(feature, resolution) {
        var maxResolution = 5;
        var text = feature.getGeometryName();
        var verticalExtentAtThisResolution = 400 * resolution; // 400 comes from nrl, it's the map height
        if (!isVoid(feature)) {
            var geom = feature.getGeometry();
            //console.log("geom.getType()", geom.getType());
            if (!isVoid(geom)) {
                if (geom.getType() !== 'Point') {
                    var extent = geom.getExtent();
                    var polygonHeight = extent[3] - extent[1];
                    if (polygonHeight < verticalExtentAtThisResolution / 10)
                        text = '';
                }
            }
        }
        return text;
    }
    function quicklyFindRotation(feature) {
        var geom = feature.getGeometry();
        if (!(geom instanceof ol.geom.Polygon))
            return 0.0;
        var coords = geom.getCoordinates();
        var centerx = 0.0, centery = 0.0;
        var points = 0;
        // Much slower lambdas than for-loops...
        //
        // coords.forEach((arr:Array<number[]>) => {
        //     arr.forEach((xy:number[]) => {
        //         centerx += xy[0];
        //         centery += xy[1];
        //         points++;
        //     });
        // });
        for (var idx0 = 0; idx0 < coords.length; idx0++) {
            var poly = coords[idx0];
            for (var idx1 = 0; idx1 < poly.length; idx1++) {
                var point = poly[idx1];
                centerx += point[0];
                centery += point[1];
                points++;
            }
        }
        centerx /= points;
        centery /= points;
        var bestAngle = 0, minYSpan = 1e20;
        for (var angle = -90; angle < 91; angle++) {
            var cosTheta = Math.cos(Math.PI * angle / 180.);
            var sinTheta = Math.sin(Math.PI * angle / 180.);
            var miny = 1e20, maxy = -1e20;
            for (var idx0 = 0; idx0 < coords.length; idx0++) {
                var poly = coords[idx0];
                for (var idx1 = 0; idx1 < poly.length; idx1++) {
                    var px = poly[idx1][0];
                    var py = poly[idx1][1];
                    px -= centerx;
                    py -= centery;
                    // var npx = px*cosTheta - py*sinTheta;
                    var npy = px * sinTheta + py * cosTheta;
                    miny = Math.min(miny, npy);
                    maxy = Math.max(maxy, npy);
                }
            }
            var spanY = maxy - miny;
            if (spanY < minYSpan) {
                minYSpan = spanY;
                bestAngle = angle;
            }
        }
        return Math.PI * bestAngle / 180.;
    }
    function createTextStyle(feature, resolution, labelEnabled, labelColorMapped) {
        var align = "center";
        var baseline = "middle";
        var size = "10px";
        var offsetX = 0;
        var offsetY = 0;
        if (feature.getGeometry().getType() === "Point") {
            offsetY = -10; //if point give an offset of 10px so point will not be hidden below label
        }
        var weight = "bold";
        var rotation = quicklyFindRotation(feature);
        var font = "bold 10px Verdana";
        // var fillColor = fallback(labelColorMapped, 'blue');
        var fillColor = "#000000";
        var outlineColor = "#FFFFFF";
        var outlineWidth = 3;
        return new ol.style.Text({
            textAlign: align,
            textBaseline: baseline,
            font: font,
            text: labelEnabled ? getTextOfFeatureForResolution(feature, resolution) : '',
            fill: new ol.style.Fill({ color: fillColor }),
            stroke: new ol.style.Stroke({ color: outlineColor, width: outlineWidth }),
            offsetX: offsetX,
            offsetY: offsetY,
            rotation: rotation
        });
    }
    NpGeoGlobals.createTextStyle = createTextStyle;
    function createPolygonStyleFunction(fillColor, borderColor, labelColor, width, getIcon, labelEnabled) {
        if (labelEnabled === void 0) { labelEnabled = true; }
        return function (feature, resolution) {
            var fillColorMapped = (fillColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[fillColor] : fillColor;
            var borderColorMapped = (borderColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[borderColor] : borderColor;
            var labelColorMapped = (labelColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[labelColor] : labelColor;
            var config = {};
            if (feature.getGeometry().getType() === 'Point') {
                config = {
                    image: new ol.style.Circle({
                        radius: 7,
                        fill: new ol.style.Fill({
                            color: 'rgba(255, 165, 0, 0.8)'
                        }),
                        stroke: new ol.style.Stroke({
                            color: 'red',
                            width: 2
                        })
                    })
                };
            }
            else if (feature.getGeometry().getType() === 'LineString') {
                config = {
                    stroke: new ol.style.Stroke({
                        color: 'red',
                        width: 7
                    })
                };
            }
            else {
                config = {
                    stroke: new ol.style.Stroke({
                        color: borderColorMapped,
                        width: width
                    }),
                    fill: new ol.style.Fill({
                        color: fillColorMapped
                    })
                };
            }
            if (!isVoid(getIcon)) {
                config.image = getIcon();
            }
            var style = new ol.style.Style(config);
            var textStyleConfig = {
                text: createTextStyle(feature, resolution, labelEnabled, labelColorMapped),
                geometry: function (feature) {
                    var geomToIntersect = feature.getGeometry();
                    var retPoint = null;
                    var forceInt = true;
                    if (feature.getGeometry().getType() === 'MultiPolygon') {
                        geomToIntersect = NpGeoGlobals.getMaxPoly(feature.getGeometry().getPolygons());
                    }
                    else if (feature.getGeometry().getType() === 'Polygon') {
                        geomToIntersect = feature.getGeometry();
                    }
                    else if (feature.getGeometry().getType() === 'Point') {
                        return feature.getGeometry();
                    }
                    else {
                        forceInt = false;
                    }
                    var mapExtent = NpGeoGlobals.globalMapThis.map.getView().calculateExtent(NpGeoGlobals.globalMapThis.map.getSize());
                    var extentGeom = new ol.geom.Polygon([[
                            [mapExtent[0], mapExtent[1]],
                            [mapExtent[0], mapExtent[3]],
                            [mapExtent[2], mapExtent[3]],
                            [mapExtent[2], mapExtent[1]],
                            [mapExtent[0], mapExtent[1]]
                        ]]);
                    if (forceInt === true) {
                        var intGeom = getJSTSIntersection(geomToIntersect, extentGeom);
                        if (intGeom.getType() === "MultiPolygon") {
                            //handle the case of multipolygon if needed
                            retPoint = getMaxPoly(intGeom.getPolygons()).getInteriorPoint();
                        }
                        if (intGeom.getType() === "Polygon") {
                            retPoint = intGeom.getInteriorPoint();
                        }
                    }
                    else {
                        retPoint = new ol.geom.Point(NpGeoGlobals.lineMidpoint(feature.getGeometry().getCoordinates()));
                    }
                    //console.log("globals retPoint", retPoint);
                    return retPoint;
                }
            };
            var textStyle = new ol.style.Style(textStyleConfig);
            return [style, textStyle];
        };
    }
    NpGeoGlobals.createPolygonStyleFunction = createPolygonStyleFunction;
    /**
     * This is NOT USED - DEPRECATED
     * sets a proper style depending on the geometry of feature supplied
     * @param feat
     */
    function getDefaultStyleUsingGeomType(feat) {
        //console.log("feature passed====", feat);
        var defaultStyle = {
            'Point': new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 165, 0, 0.8)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'red',
                        width: 2
                    })
                })
            }),
            'LineString': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'blue',
                    width: 2
                })
            }),
            'Polygon': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'blue',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255, 0.1)'
                })
            }),
            'MultiPoint': new ol.style.Style({
                image: new ol.style.Circle({
                    fill: new ol.style.Fill({
                        color: 'rgba(255,0,255,0.5)'
                    }),
                    radius: 5,
                    stroke: new ol.style.Stroke({
                        color: '#f0f',
                        width: 1
                    })
                })
            }),
            'MultiLineString': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: '#0f0',
                    width: 3
                })
            }),
            'MultiPolygon': new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(0,0,255,0.5)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#00f',
                    width: 1
                })
            })
        };
        return defaultStyle[feat.getGeometry().getType()];
    }
    NpGeoGlobals.getDefaultStyleUsingGeomType = getDefaultStyleUsingGeomType;
    /////////////////***********************************************************/////////////////////////////////////////////
    ////////////////******** set of functions for the calculation of polyline middle point ***********************///////////
    /**
     * calculate the distnace between two coordinates
     * @param coordA
     * @param coordB
     */
    function calcDistance(coordA, coordB) {
        var dx = coordA[0] - coordB[0];
        var dy = coordA[1] - coordB[1];
        return Math.sqrt(dx * dx + dy * dy);
    }
    NpGeoGlobals.calcDistance = calcDistance;
    /**
     * Given a line between point1 and point2 return a point that
     * is distance away from point1
     * @param coordA
     * @param coordB
     * @param distance
     */
    function lineInterpolate(coordA, coordB, distance) {
        var xabs = Math.abs(coordA[0] - coordB[0]);
        var yabs = Math.abs(coordA[1] - coordB[1]);
        var xdiff = coordB[0] - coordA[0];
        var ydiff = coordB[1] - coordA[1];
        var length = Math.sqrt((Math.pow(xabs, 2) + Math.pow(yabs, 2)));
        var steps = length / distance;
        var xstep = xdiff / steps;
        var ystep = ydiff / steps;
        return [coordA[0] + xstep, coordA[1] + ystep];
    }
    NpGeoGlobals.lineInterpolate = lineInterpolate;
    /**
     * Return the point that is the midpoint for the line
     * @param pointCoords
     */
    function lineMidpoint(pointCoords) {
        // Sum up the total distance of the line
        var TotalDistance = 0;
        for (var i = 0; i < pointCoords.length - 1; i += 1) {
            TotalDistance += calcDistance(pointCoords[i], pointCoords[i + 1]);
        }
        // Find the middle segemnt of the line
        var DistanceSoFar = 0;
        for (var i = 0; i < pointCoords.length - 1; i += 1) {
            // If this linesegment puts us past the middle then this
            // is the segment in which the midpoint appears
            if (DistanceSoFar + calcDistance(pointCoords[i], pointCoords[i + 1]) > TotalDistance / 2) {
                // Figure out how far to the midpoint
                var DistanceToMidpoint = TotalDistance / 2 - DistanceSoFar;
                // Given the start/end of a line and a distance return the point
                // on the line the specified distance away
                return lineInterpolate(pointCoords[i], pointCoords[i + 1], DistanceToMidpoint);
            }
            DistanceSoFar += calcDistance(pointCoords[i], pointCoords[i + 1]);
        }
        // Can happen when the line is of zero length... so just return the first segment
        return pointCoords[0];
    }
    NpGeoGlobals.lineMidpoint = lineMidpoint;
    function getAreaAndPerimeter(geom) {
        var npMapThis = NpGeoGlobals.globalMapThis;
        var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:2100');
        var transGeom = geom.transform(ol.proj.get('EPSG:3857'), ol.proj.get(coordinateSystem));
        var area = 0;
        var peremiter = 0;
        if (transGeom.getType() === "Polygon") {
            area = transGeom.getArea();
            peremiter = parseFloat(new ol.geom.LineString(transGeom.getLinearRing(0).getCoordinates()).getLength().toFixed(2));
        }
        else if (transGeom.getType() === "MultiPolygon") {
            area = transGeom.getArea();
            var polys = transGeom.getPolygons();
            var interLinesSum = 0;
            for (var s = 0; s < polys.length; s++) {
                interLinesSum = interLinesSum + (new ol.geom.LineString(polys[s].getLinearRing(0).getCoordinates())).getLength();
            }
            peremiter = parseFloat(interLinesSum.toFixed(2));
        }
        else {
            area = 0; //just dummy it
            peremiter = 0; //just dummy it//just dummy it
        }
        return {
            area: area, perimeter: peremiter
        };
    }
    NpGeoGlobals.getAreaAndPerimeter = getAreaAndPerimeter;
    /**
     * supply an array of polygons
     * get back the larger one
     * @param polys {array of ol.geom.Polygon}
     * @returns ol.geom.Polygon
     */
    function getMaxPoly(polys) {
        var polyObj = [];
        //now need to find which one is the greater and so label only this
        for (var b = 0; b < polys.length; b++) {
            polyObj.push({ poly: polys[b], area: polys[b].getArea() });
        }
        polyObj.sort(function (a, b) { return a.area - b.area; });
        //console.log("maxPolySort", maxPolySort);
        return polyObj[polyObj.length - 1].poly;
    }
    NpGeoGlobals.getMaxPoly = getMaxPoly;
    /**
     * supply an array of polygons
     * get back the larger one
     * @param polys {array of ol.geom.Polygon}
     * @returns ol.geom.Polygon
     */
    function getMaxLine(lines) {
        var lineObj = [];
        //now need to find which one is the greater and so label only this
        for (var b = 0; b < lines.length; b++) {
            lineObj.push({ line: lines[b], length: lines[b].getLength() });
        }
        lineObj.sort(function (a, b) { return a.length - b.length; });
        //console.log("maxPolySort", maxPolySort);
        return lineObj[lineObj.length - 1].line;
    }
    NpGeoGlobals.getMaxLine = getMaxLine;
    function createPolygonWithVerticesStyleFunction(npMapThis, styleFunc, fillColor, borderColor, labelColor, width, getIcon, verticesCircles, labelEnabled) {
        if (labelEnabled === void 0) { labelEnabled = true; }
        var simpleStyleFunction = fallback(styleFunc, createPolygonStyleFunction(fillColor, borderColor, labelColor, width, getIcon, labelEnabled));
        if (!verticesCircles || npMapThis.currentMode !== NpGeo.NpMapMode.NPMAP_MODE_DRAW) {
            return simpleStyleFunction;
        }
        else {
            return function (feature, resolution) {
                var simpleStyle = simpleStyleFunction(feature, resolution);
                simpleStyle.unshift(new ol.style.Style({
                    image: new ol.style.RegularShape({
                        fill: new ol.style.Fill({ color: 'rgba(255, 255, 255, 0.0)' }),
                        stroke: new ol.style.Stroke({ color: 'rgba(255, 0, 0, 0.4)', width: 2 }),
                        points: 4,
                        radius: 6,
                        radius2: 0,
                        angle: Math.PI / 4
                    }),
                    geometry: function (feature) {
                        var coordsToRet = new Array();
                        //Case of simple Polygons
                        if (feature.getGeometry().getType() === 'Polygon') {
                            var linerings = feature.getGeometry().getLinearRings();
                            for (var b = 0; b < linerings.length; b++) {
                                var coordsToAdd = linerings[b].getCoordinates();
                                for (var p = 0; p < coordsToAdd.length; p++) {
                                    coordsToRet.push(coordsToAdd[p]);
                                }
                            }
                            return new ol.geom.MultiPoint(coordsToRet);
                        }
                        else if (feature.getGeometry().getType() === 'MultiPolygon') {
                            var pols = feature.getGeometry().getPolygons();
                            for (var q = 0; q < pols.length; q++) {
                                var linerings = pols[q].getLinearRings();
                                for (var b = 0; b < linerings.length; b++) {
                                    var coordsToAdd = linerings[b].getCoordinates();
                                    for (var p = 0; p < coordsToAdd.length; p++) {
                                        coordsToRet.push(coordsToAdd[p]);
                                    }
                                }
                            }
                            return new ol.geom.MultiPoint(coordsToRet);
                        }
                        else if (feature.getGeometry().getType() === 'LineString') {
                            return new ol.geom.MultiPoint(feature.getGeometry().getCoordinates());
                        }
                        else {
                            return new ol.geom.Point(feature.getGeometry().getCoordinates());
                        }
                        // return new ol.geom.MultiPoint(coordsToRet);
                    }
                }));
                return simpleStyle;
            };
        }
        ;
    }
    NpGeoGlobals.createPolygonWithVerticesStyleFunction = createPolygonWithVerticesStyleFunction;
    function commonCoordinateSystems(system) {
        // Info from here: http://spatialreference.org
        if (system.indexOf('EPSG:') == 0) {
            system = system.slice(5);
        }
        switch (system) {
            case "2100":
                return "+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9996 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=-199.87,74.79,246.62,0,0,0,0 +units=m +no_defs";
            case "3857":
                return "+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6378137 +b=6378137 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs";
            case "4326":
                return "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
            default:
                return undefined;
        }
    }
    NpGeoGlobals.commonCoordinateSystems = commonCoordinateSystems;
    function cleanNamesOfFeatures(features) {
        features.forEach(function (f) {
            var oldName = f.getGeometryName();
            if (oldName === "geometry") {
                var anarchy = f;
                var oldValues = anarchy.values_[oldName];
                anarchy.geometryName_ = "";
                delete anarchy.values_[oldName];
                anarchy.values_[""] = oldValues;
            }
        });
        return features;
    }
    NpGeoGlobals.cleanNamesOfFeatures = cleanNamesOfFeatures;
    function safeClone(data) {
        if (isVoid(data))
            return data;
        else
            return data.clone();
    }
    NpGeoGlobals.safeClone = safeClone;
    //@p.tsagkis start
    function getTileResolutions() {
        return [979.9994708002862, 419.99977320012255, 279.99984880008174, 209.99988660006127, 139.99992440004087, 97.99994708002859, 83.99995464002451, 69.99996220002043, 55.999969760016334, 27.999984880008167, 13.999992440004084, 8.39999546400245, 5.599996976001633, 2.7999984880008166, 2.2399987904006537, 1.3999992440004083, 0.6999996220002042, 0.41999977320012255, 0.2799998488000817];
    }
    NpGeoGlobals.getTileResolutions = getTileResolutions;
    function getResolutions() {
        return [30000, 19552, 9776, 4888, 2444, 1222, 979.9994708002862, 419.99977320012255, 279.99984880008174, 209.99988660006127, 139.99992440004087, 97.99994708002859, 83.99995464002451, 69.99996220002043, 55.999969760016334, 27.999984880008167, 13.999992440004084, 8.39999546400245, 5.599996976001633, 2.7999984880008166, 2.2399987904006537, 1.3999992440004083, 0.6999996220002042, 0.41999977320012255, 0.2799998488000817, 0.13999992440004086, 0.08399995464002451, 0.05599996976001634, 0.02799998488000817];
    }
    NpGeoGlobals.getResolutions = getResolutions;
    ;
    function getTileMatrixIds() {
        var res = getTileResolutions();
        var retArr = new Array(res.length);
        for (var z = 0; z < res.length; z++) {
            retArr[z] = 'NP_GRID:' + z;
        }
        return retArr;
    }
    NpGeoGlobals.getTileMatrixIds = getTileMatrixIds;
    ;
    function getTextWidth(text, font) {
        var f = font || '12px arial', o = $('<div>' + text + '</div>')
            .css({ 'position': 'absolute', 'float': 'left', 'white-space': 'nowrap', 'visibility': 'hidden', 'font': f })
            .appendTo($('body')), w = o.width();
        o.remove();
        return w;
    }
    NpGeoGlobals.getTextWidth = getTextWidth;
    //set of functions to solve the full screen buggy attitude
    function changeFullScreenState(e) {
        var topBarEl = $(".TopFormArea")[0];
        var appContainerElement = document.getElementById('NpMainContent');
        if (NpGeoGlobals.fullScreenState === true) {
            if (typeof appContainerElement !== 'undefined') {
                removeClass(appContainerElement, "custOl-fullScreen");
                addClass(appContainerElement, "resizable2");
            }
            if (typeof topBarEl !== 'undefined') {
                removeClass(topBarEl, "custTopBar-fullScreen");
            }
            NpGeoGlobals.fullScreenState = false;
        }
        else {
            if (typeof appContainerElement !== 'undefined') {
                removeClass(appContainerElement, "resizable2");
                addClass(appContainerElement, "custOl-fullScreen");
            }
            if (typeof topBarEl !== 'undefined') {
                addClass(topBarEl, "custTopBar-fullScreen");
            }
            NpGeoGlobals.fullScreenState = true;
        }
    }
    NpGeoGlobals.changeFullScreenState = changeFullScreenState;
    //helper functions to add/remove css classes on elemetns
    function hasClass(el, className) {
        if (el.classList)
            return el.classList.contains(className);
        else
            return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
    }
    NpGeoGlobals.hasClass = hasClass;
    function addClass(el, className) {
        if (el.classList)
            el.classList.add(className);
        else if (!hasClass(el, className))
            el.className += " " + className;
    }
    NpGeoGlobals.addClass = addClass;
    function removeClass(el, className) {
        if (el.classList)
            el.classList.remove(className);
        else if (hasClass(el, className)) {
            var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
    }
    NpGeoGlobals.removeClass = removeClass;
    /**
     * convert rgb color to hex color code
     *
     * @param rgb
     */
    function rgb2hex(rgb) {
        rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
        return (rgb && rgb.length === 4) ? "#" +
            ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
            ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
            ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
    }
    NpGeoGlobals.rgb2hex = rgb2hex;
    /**
     * convert rgba to
     * hex color code and opacity
     *
     * @param rgba
     * @returns JSON onject {hex:....,opacity:....}
     */
    function rgba2hexAndOpacity(rgba) {
        var retVal = []; //object to return
        if (typeof rgba === 'undefined') {
            retVal = [{
                    hex: "#000000",
                    opacity: "1" //1px width
                }];
        }
        else {
            var rgbaStringAr = rgba.split(",");
            var opacity = rgbaStringAr[3].substring(0, rgbaStringAr[3].indexOf(")"));
            rgba = rgba.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
            var retHex = (rgba && rgba.length === 4) ? "#" +
                ("0" + parseInt(rgba[1], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgba[2], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgba[3], 10).toString(16)).slice(-2) : '';
            retVal = [{
                    hex: retHex,
                    opacity: opacity
                }];
        }
        return retVal;
    }
    NpGeoGlobals.rgba2hexAndOpacity = rgba2hexAndOpacity;
    /**
     * Default function to use in order to build the legend data.
     * If no function is specified this should be used
     * Should get every availble vector layer and dispaly a legened for each entry using the alias name and fill color
     */
    function defaultLegendFnc() {
        var npMapThis = this;
        var legendEntries = new Array();
        var lyrsOnMap = npMapThis.map.getLayers().getArray();
        for (var i = 0; i < lyrsOnMap.length; i++) {
            var lyr = lyrsOnMap[i];
            var lyrAlias = npMapThis.getLayerLabelById(lyr.get("layerId"));
            //console.log("layername for legend==", npMapThis.getLayerLabelById(<any>lyr.get("layerId")));
            if (lyr instanceof ol.layer.Vector) {
                var npLyr = npMapThis.getNPLayerById(lyr.get("layerId"));
                //console.log("npLyr", npLyr)
                var fillCol = (npLyr.fillColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[npLyr.fillColor] : npLyr.fillColor;
                //console.log("fillCol", fillCol)
                legendEntries.push({
                    title: lyrAlias,
                    childs: [
                        {
                            title: lyrAlias,
                            color: fillCol
                        }
                    ]
                });
            }
        }
        var data = [{
                legendTitle: "Υπόμνημα Χάρτη",
                entries: legendEntries
            }];
        return data;
    }
    NpGeoGlobals.defaultLegendFnc = defaultLegendFnc;
    /**
     * Reomove the inner ring
     * at the supplied index of the supplied polygon
     * @param geom
     * @param index
     */
    function removeHoleAtIndex(geom, index) {
        var coordElemntLength = geom.getCoordinates().length;
        var coordsExt = geom.getCoordinates()[0]; //exterior ring
        var coordsInter = []; //interior rings 
        for (var i = 1; i < coordElemntLength; i++) {
            if (i !== index) {
                coordsInter.push(geom.getCoordinates()[i]);
            }
        }
        var setCoords = [];
        setCoords.push(coordsExt);
        for (var z = 0; z < coordsInter.length; z++) {
            setCoords.push(coordsInter[z]);
        }
        geom.setCoordinates(setCoords);
    }
    NpGeoGlobals.removeHoleAtIndex = removeHoleAtIndex;
    //@p.tsagkis end
    function getColorMap() {
        return NpGeoGlobals.colorMap;
    }
    NpGeoGlobals.getColorMap = getColorMap;
    NpGeoGlobals.colorMap = {
        'aliceBlue': 'rgba(240, 248, 255, 0.1)',
        'antiqueWhite': 'rgba(250, 235, 215, 0.1)',
        'aqua': 'rgba(0, 255, 255, 0.1)',
        'aquamarine': 'rgba(127, 255, 212, 0.1)',
        'azure': 'rgba(240, 255, 255, 0.1)',
        'beige': 'rgba(245, 245, 220, 0.1)',
        'bisque': 'rgba(255, 228, 196, 0.1)',
        'black': 'rgba(0, 0, 0, 0.1)',
        'blanchedAlmond': 'rgba(255, 235, 205, 0.1)',
        'blue': 'rgba(0, 0, 255, 0.8)',
        'blueViolet': 'rgba(138, 43, 226, 0.1)',
        'brown': 'rgba(165, 42, 42, 0.1)',
        'burlyWood': 'rgba(222, 184, 135, 0.1)',
        'cadetBlue': 'rgba(95, 158, 160, 0.1)',
        'chartreuse': 'rgba(127, 255, 0, 0.1)',
        'chocolate': 'rgba(210, 105, 30, 0.1)',
        'coral': 'rgba(255, 127, 80, 0.1)',
        'cornflowerBlue': 'rgba(100, 149, 237, 0.1)',
        'cornsilk': 'rgba(255, 248, 220, 0.1)',
        'crimson': 'rgba(220, 20, 60, 0.1)',
        'cyan': 'rgba(0, 255, 255, 0.1)',
        'darkBlue': 'rgba(0, 0, 139, 0.1)',
        'darkCyan': 'rgba(0, 139, 139, 0.1)',
        'darkGoldenRod': 'rgba(184, 134, 11, 0.1)',
        'darkGray': 'rgba(169, 169, 169, 0.1)',
        'darkGreen': 'rgba(0, 100, 0, 0.1)',
        'darkKhaki': 'rgba(189, 183, 107, 0.1)',
        'darkMagenta': 'rgba(139, 0, 139, 0.1)',
        'darkOliveGreen': 'rgba(85, 107, 47, 0.1)',
        'darkOrange': 'rgba(255, 140, 0, 0.1)',
        'darkOrchid': 'rgba(153, 50, 204, 0.1)',
        'darkRed': 'rgba(139, 0, 0, 0.1)',
        'darkSalmon': 'rgba(233, 150, 122, 0.1)',
        'darkSeaGreen': 'rgba(143, 188, 143, 0.1)',
        'darkSlateBlue': 'rgba(72, 61, 139, 0.1)',
        'darkSlateGray': 'rgba(47, 79, 79, 0.1)',
        'darkTurquoise': 'rgba(0, 206, 209, 0.1)',
        'darkViolet': 'rgba(148, 0, 211, 0.1)',
        'deepPink': 'rgba(255, 20, 147, 0.1)',
        'deepSkyBlue': 'rgba(0, 191, 255, 0.1)',
        'dimGray': 'rgba(105, 105, 105, 0.1)',
        'dodgerBlue': 'rgba(30, 144, 255, 0.1)',
        'fireBrick': 'rgba(178, 34, 34, 0.1)',
        'floralWhite': 'rgba(255, 250, 240, 0.1)',
        'forestGreen': 'rgba(34, 139, 34, 0.1)',
        'fuchsia': 'rgba(255, 0, 255, 0.1)',
        'gainsboro': 'rgba(220, 220, 220, 0.1)',
        'ghostWhite': 'rgba(248, 248, 255, 0.1)',
        'gold': 'rgba(255, 215, 0, 0.1)',
        'goldenRod': 'rgba(218, 165, 32, 0.1)',
        'gray': 'rgba(128, 128, 128, 0.1)',
        'green': 'rgba(0, 128, 0, 0.8)',
        'greenYellow': 'rgba(173, 255, 47, 0.1)',
        'honeyDew': 'rgba(240, 255, 240, 0.1)',
        'hotPink': 'rgba(255, 105, 180, 0.1)',
        'indianRed': 'rgba(205, 92, 92, 0.1)',
        'indigo': 'rgba(75, 0, 130, 0.1)',
        'ivory': 'rgba(255, 255, 240, 0.1)',
        'khaki': 'rgba(240, 230, 140, 0.1)',
        'lavender': 'rgba(230, 230, 250, 0.1)',
        'lavenderBlush': 'rgba(255, 240, 245, 0.1)',
        'lawnGreen': 'rgba(124, 252, 0, 0.1)',
        'lemonChiffon': 'rgba(255, 250, 205, 0.1)',
        'lightBlue': 'rgba(173, 216, 230, 0.1)',
        'lightCoral': 'rgba(240, 128, 128, 0.1)',
        'lightCyan': 'rgba(224, 255, 255, 0.1)',
        'lightGoldenRodYellow': 'rgba(250, 250, 210, 0.1)',
        'lightGray': 'rgba(211, 211, 211, 0.1)',
        'lightGreen': 'rgba(144, 238, 144, 0.1)',
        'lightPink': 'rgba(255, 182, 193, 0.1)',
        'lightSalmon': 'rgba(255, 160, 122, 0.1)',
        'lightSeaGreen': 'rgba(32, 178, 170, 0.1)',
        'lightSkyBlue': 'rgba(135, 206, 250, 0.1)',
        'lightSlateGray': 'rgba(119, 136, 153, 0.1)',
        'lightSteelBlue': 'rgba(176, 196, 222, 0.1)',
        'lightYellow': 'rgba(255, 255, 224, 0.1)',
        'lime': 'rgba(0, 255, 0, 0.1)',
        'limeGreen': 'rgba(50, 205, 50, 0.1)',
        'linen': 'rgba(250, 240, 230, 0.1)',
        'magenta': 'rgba(255, 0, 255, 0.1)',
        'maroon': 'rgba(128, 0, 0, 0.1)',
        'mediumAquaMarine': 'rgba(102, 205, 170, 0.1)',
        'mediumBlue': 'rgba(0, 0, 205, 0.1)',
        'mediumOrchid': 'rgba(186, 85, 211, 0.1)',
        'mediumPurple': 'rgba(147, 112, 219, 0.1)',
        'mediumSeaGreen': 'rgba(60, 179, 113, 0.1)',
        'mediumSlateBlue': 'rgba(123, 104, 238, 0.1)',
        'mediumSpringGreen': 'rgba(0, 250, 154, 0.1)',
        'mediumTurquoise': 'rgba(72, 209, 204, 0.1)',
        'mediumVioletRed': 'rgba(199, 21, 133, 0.1)',
        'midnightBlue': 'rgba(25, 25, 112, 0.1)',
        'mintCream': 'rgba(245, 255, 250, 0.1)',
        'mistyRose': 'rgba(255, 228, 225, 0.1)',
        'moccasin': 'rgba(255, 228, 181, 0.1)',
        'navajoWhite': 'rgba(255, 222, 173, 0.1)',
        'navy': 'rgba(0, 0, 128, 0.1)',
        'oldLace': 'rgba(253, 245, 230, 0.1)',
        'olive': 'rgba(128, 128, 0, 0.1)',
        'oliveDrab': 'rgba(107, 142, 35, 0.1)',
        'orange': 'rgba(255, 165, 0, 0.8)',
        'orangeRed': 'rgba(255, 69, 0, 0.1)',
        'orchid': 'rgba(218, 112, 214, 0.1)',
        'paleGoldenRod': 'rgba(238, 232, 170, 0.1)',
        'paleGreen': 'rgba(152, 251, 152, 0.1)',
        'paleTurquoise': 'rgba(175, 238, 238, 0.1)',
        'paleVioletRed': 'rgba(219, 112, 147, 0.1)',
        'papayaWhip': 'rgba(255, 239, 213, 0.1)',
        'peachPuff': 'rgba(255, 218, 185, 0.1)',
        'peru': 'rgba(205, 133, 63, 0.1)',
        'pink': 'rgba(255, 192, 203, 0.1)',
        'plum': 'rgba(221, 160, 221, 0.1)',
        'powderBlue': 'rgba(176, 224, 230, 0.1)',
        'purple': 'rgba(128, 0, 128, 0.1)',
        'red': 'rgba(255, 0, 0, 0.8)',
        'rosyBrown': 'rgba(188, 143, 143, 0.1)',
        'royalBlue': 'rgba(65, 105, 225, 0.1)',
        'saddleBrown': 'rgba(139, 69, 19, 0.1)',
        'salmon': 'rgba(250, 128, 114, 0.1)',
        'sandyBrown': 'rgba(244, 164, 96, 0.1)',
        'seaGreen': 'rgba(46, 139, 87, 0.1)',
        'seaShell': 'rgba(255, 245, 238, 0.1)',
        'sienna': 'rgba(160, 82, 45, 0.1)',
        'silver': 'rgba(192, 192, 192, 0.1)',
        'skyBlue': 'rgba(135, 206, 235, 0.1)',
        'slateBlue': 'rgba(106, 90, 205, 0.1)',
        'slateGray': 'rgba(112, 128, 144, 0.1)',
        'snow': 'rgba(255, 250, 250, 0.1)',
        'springGreen': 'rgba(0, 255, 127, 0.1)',
        'steelBlue': 'rgba(70, 130, 180, 0.1)',
        'tan': 'rgba(210, 180, 140, 0.1)',
        'teal': 'rgba(0, 128, 128, 0.1)',
        'thistle': 'rgba(216, 191, 216, 0.1)',
        'tomato': 'rgba(255, 99, 71, 0.1)',
        'turquoise': 'rgba(64, 224, 208, 0.1)',
        'violet': 'rgba(238, 130, 238, 0.1)',
        'wheat': 'rgba(245, 222, 179, 0.1)',
        'white': 'rgba(255, 255, 255, 0.8)',
        'whiteSmoke': 'rgba(245, 245, 245, 0.1)',
        'yellow': 'rgba(255, 255, 0, 0.5)',
        'yellowGreen': 'rgba(154, 205, 50, 0.1)',
        'whiteSelected': 'rgba(255, 255, 255, 0.5)'
    };
})(NpGeoGlobals || (NpGeoGlobals = {}));
//# sourceMappingURL=NpMapGlobals.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
