/// <reference path="../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="validators.ts" />
/// <reference path="utils.ts" />
/// <reference path="openlayers3.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var NpTypes;
(function (NpTypes) {
    (function (ExceptionSeverity) {
        ExceptionSeverity[ExceptionSeverity["generatorError"] = 0] = "generatorError";
        ExceptionSeverity[ExceptionSeverity["programmerError"] = 1] = "programmerError";
        ExceptionSeverity[ExceptionSeverity["userError"] = 2] = "userError";
    })(NpTypes.ExceptionSeverity || (NpTypes.ExceptionSeverity = {}));
    var ExceptionSeverity = NpTypes.ExceptionSeverity;
    var NpException = (function () {
        function NpException(severity, message) {
            this.severity = severity;
            this.message = message;
        }
        return NpException;
    })();
    NpTypes.NpException = NpException;
    var EntityAuditInfo = (function () {
        function EntityAuditInfo(userInsert, dateInsert, userUpdate, dateUpdate) {
            this.userInsert = userInsert;
            this.dateInsert = dateInsert;
            this.userUpdate = userUpdate;
            this.dateUpdate = dateUpdate;
        }
        return EntityAuditInfo;
    })();
    NpTypes.EntityAuditInfo = EntityAuditInfo;
    var DynamicSqlVectorInfoField = (function () {
        function DynamicSqlVectorInfoField(label, field, width) {
            this.label = label;
            this.field = field;
            this.width = width;
        }
        return DynamicSqlVectorInfoField;
    })();
    NpTypes.DynamicSqlVectorInfoField = DynamicSqlVectorInfoField;
    var DynamicSqlVectorInfo = (function () {
        function DynamicSqlVectorInfo(code, label, fillColor, borderColor, penWidth, orderIndex, opacity, fields) {
            this.code = code;
            this.label = label;
            this.fillColor = fillColor;
            this.borderColor = borderColor;
            this.penWidth = penWidth;
            this.orderIndex = orderIndex;
            this.opacity = opacity;
            this.fields = fields;
        }
        return DynamicSqlVectorInfo;
    })();
    NpTypes.DynamicSqlVectorInfo = DynamicSqlVectorInfo;
    var entitiesFactoryRecord = (function () {
        function entitiesFactoryRecord() {
        }
        return entitiesFactoryRecord;
    })();
    NpTypes.entitiesFactoryRecord = entitiesFactoryRecord;
    var BaseEntity = (function () {
        function BaseEntity() {
            this.markAsDirty = function (x, itemId) { };
        }
        Object.defineProperty(BaseEntity.prototype, "$scope", {
            get: function () {
                if (!isVoid(BaseEntity.activePageController))
                    return BaseEntity.activePageController.$scope;
                return undefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BaseEntity.prototype, "$http", {
            get: function () {
                if (!isVoid(BaseEntity.activePageController))
                    return BaseEntity.activePageController.$http;
                return undefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BaseEntity.prototype, "$timeout", {
            get: function () {
                if (!isVoid(BaseEntity.activePageController))
                    return BaseEntity.activePageController.$timeout;
                return undefined;
            },
            enumerable: true,
            configurable: true
        });
        BaseEntity.prototype.getKey = function () { throw "Not Impemented function"; };
        BaseEntity.prototype.getRowVersion = function () { throw "Not Impemented function"; };
        BaseEntity.prototype.getKeyCombinedWithRowVersion = function () {
            var rowVersion = !isVoid(this.getRowVersion()) ? this.getRowVersion() : 0;
            return this.getKey() + "#" + rowVersion.toString();
        };
        BaseEntity.prototype.toJSON = function () { throw "Not Impemented function"; };
        BaseEntity.prototype.getEntityName = function () { throw "Not Impemented function"; };
        BaseEntity.prototype.updateInstance = function (other) { throw "Not Impemented function"; };
        BaseEntity.prototype.getKeyName = function () { throw "Not Impemented function"; };
        BaseEntity.prototype.fromJSON = function (data) { throw "Not Impemented function"; };
        BaseEntity.prototype.postConstruct = function () { };
        BaseEntity.prototype.isNew = function () {
            var key = this.getKey();
            if (isVoid(key))
                return true;
            if (this.getKey().indexOf('TEMP_ID') === 0)
                return true;
            return false;
        };
        BaseEntity.prototype.isEqual = function (other) {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey() && this.getEntityName() === other.getEntityName();
        };
        BaseEntity.prototype.refresh = function (ctrl, afterRefreshFunc) {
            var _this = this;
            if (afterRefreshFunc === void 0) { afterRefreshFunc = function () { }; }
            if (!this.isNew()) {
                var url = "/" + ctrl.AppName + "/rest/" + this.getEntityName() + "/findBy" + this.getKeyName().U1() + "?";
                var requestData = {};
                requestData[this.getKeyName()] = this.getKey();
                ctrl.httpPost(url, requestData, function (rsp) {
                    var refresedEntities = _this.fromJSON(rsp.data);
                    _this.updateInstance(refresedEntities[0]);
                    afterRefreshFunc();
                });
            }
        };
        BaseEntity.prototype.getAuditInfo = function () {
            return new EntityAuditInfo('', undefined, '', undefined);
        };
        BaseEntity.entitiesFactory = {};
        return BaseEntity;
    })();
    NpTypes.BaseEntity = BaseEntity;
    function checkProducerStatus(ctrl, producerVat, func, errFunc) {
        AlertMessage.clearAlerts(ctrl.PageModel);
        if (!Utils.checkGreekVAT(producerVat)) {
            var errKey = Sprintf.sprintf("INVALID_VAT('%s')", producerVat);
            if (errFunc === undefined) {
                AlertMessage.addDanger(ctrl.PageModel, ctrl.dynamicMessage(errKey));
            }
            else {
                errFunc(ctrl.dynamicMessage(errKey));
            }
        }
        else {
            var url = "/" + ctrl.AppName + "/rest/MainService/checkProducerStatus";
            ctrl.httpPost(url, { producerVat: producerVat }, function (response) {
                func();
            }, errFunc);
        }
    }
    NpTypes.checkProducerStatus = checkProducerStatus;
    function getProducerStatus(ctrl, producerVat, func) {
        var url = "/" + ctrl.AppName + "/rest/MainService/getProducerStatus";
        ctrl.httpPost(url, { producerVat: producerVat }, function (response) {
            func(response);
        });
    }
    NpTypes.getProducerStatus = getProducerStatus;
    var EntityManager = (function () {
        function EntityManager() {
        }
        EntityManager.prototype.clearCache = function () {
            this.entitiesCache = {};
        };
        EntityManager.prototype.findById = function (entityClassName, id) {
            return this.entitiesCache[entityClassName + '#' + id];
        };
        return EntityManager;
    })();
    NpTypes.EntityManager = EntityManager;
    (function (EntityCachedKind) {
        EntityCachedKind[EntityCachedKind["MASTER"] = 0] = "MASTER";
        EntityCachedKind[EntityCachedKind["MANY_TO_ONE"] = 1] = "MANY_TO_ONE";
    })(NpTypes.EntityCachedKind || (NpTypes.EntityCachedKind = {}));
    var EntityCachedKind = NpTypes.EntityCachedKind;
    ;
    var EntityCacheEntry = (function () {
        function EntityCacheEntry(entity, kind) {
            this.entity = entity;
            this.kind = kind;
        }
        return EntityCacheEntry;
    })();
    NpTypes.EntityCacheEntry = EntityCacheEntry;
    ;
    // Kept per UIModel (would have been an inner class if TypeScript supported it)
    // the error message of the validation, and whether we hover it or not on mousemoves.
    // We don't colorize/hover for selects and checkboxes, since these can't be kept
    // showing an invalid value - they can only show their model contents. In these
    // cases, the popover is shown for 3 seconds, and then this ValidationStateEntry
    // is removed.
    var ValidationStateEntry = (function () {
        function ValidationStateEntry(msg, noHover) {
            if (noHover === void 0) { noHover = false; }
            this.msg = msg;
            this.noHover = noHover;
        }
        return ValidationStateEntry;
    })();
    NpTypes.ValidationStateEntry = ValidationStateEntry;
    ;
    // The per-entity state, kept separately from the component, because of grid paging.
    // (i.e. the same component (e.g. an <input>) is used to show multiple entities' fields
    // (different in page1, page2, etc) and we therefore need to keep state OUTSIDE the component.
    var UIModel = (function () {
        function UIModel(value, entity) {
            this.entity = entity;
            // Array with tuples of (strValidationMessage,bNoHover) (when empty, the associated component has valid data)
            this.validationMsgs = [];
            this._value = value;
        }
        Object.defineProperty(UIModel.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (vl) {
                this._value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(UIModel.prototype, "isValid", {
            // State of validity. 
            // At the beginning of all $parser in all directives, it is set to true (via clearAllErrors())
            // Subsequent checks, on failure, call addNewErrorMessage, which sets it (implicitely) to false
            // and triggers the showing of a popover. 
            //
            // This is also checked in showError, to avoid showing another popover if one is already visible.
            get: function () {
                return this.validationMsgs.length === 0;
            },
            enumerable: true,
            configurable: true
        });
        // To avoid adding a message we already know about, we need to be able to search for it.
        UIModel.prototype.indexOfValidationMsg = function (msg) {
            for (var i = 0; i < this.validationMsgs.length; i++) {
                if (this.validationMsgs[i].msg === msg)
                    return i;
            }
            return -1;
        };
        // Bark an error... An implicit popover will be shown due to the $watch-er in the component.
        UIModel.prototype.addNewErrorMessage = function (msg, dontColorizeOrHover) {
            if (dontColorizeOrHover === void 0) { dontColorizeOrHover = false; }
            var self = this;
            if (self.indexOfValidationMsg(msg) === -1) {
                self.validationMsgs.push(new ValidationStateEntry(msg, dontColorizeOrHover));
                if (dontColorizeOrHover) {
                    // For checkboxes and selects, we need to clear the error state ;
                    // there is no 'memory' of the bad value kept, since these can't be kept
                    // showing an invalid value - they can only show their model contents. In these
                    // cases, the popover is shown for 3 seconds, and then this ValidationStateEntry
                    // is removed.
                    setTimeout(function () {
                        // In the meantime, indexes may have shifted - search again:
                        var idx = self.indexOfValidationMsg(msg);
                        self.validationMsgs.splice(idx, 1);
                    }, 3000);
                }
            }
        };
        // Silence them all (implicitely hides all popovers)
        UIModel.prototype.clearAllErrors = function () {
            this.validationMsgs.splice(0);
        };
        UIModel.prototype.getErrors = function () {
            return this.validationMsgs.map(function (x) { return x.msg; });
        };
        return UIModel;
    })();
    NpTypes.UIModel = UIModel;
    // Instances of the UIModel<T> base, one per type of directive content:
    var UIStringModel = (function (_super) {
        __extends(UIStringModel, _super);
        function UIStringModel() {
            _super.apply(this, arguments);
        }
        return UIStringModel;
    })(UIModel);
    NpTypes.UIStringModel = UIStringModel;
    var UIReadOnlyStringModel = (function (_super) {
        __extends(UIReadOnlyStringModel, _super);
        function UIReadOnlyStringModel(getValue, entity) {
            _super.call(this, undefined, entity);
            this.getValue = getValue;
            this.entity = entity;
        }
        Object.defineProperty(UIReadOnlyStringModel.prototype, "value", {
            get: function () {
                return this.getValue();
            },
            enumerable: true,
            configurable: true
        });
        return UIReadOnlyStringModel;
    })(UIStringModel);
    NpTypes.UIReadOnlyStringModel = UIReadOnlyStringModel;
    var UINumberModel = (function (_super) {
        __extends(UINumberModel, _super);
        function UINumberModel() {
            _super.apply(this, arguments);
        }
        return UINumberModel;
    })(UIModel);
    NpTypes.UINumberModel = UINumberModel;
    var UIReadOnlyNumberModel = (function (_super) {
        __extends(UIReadOnlyNumberModel, _super);
        function UIReadOnlyNumberModel(getValue, entity) {
            _super.call(this, undefined, entity);
            this.getValue = getValue;
            this.entity = entity;
        }
        Object.defineProperty(UIReadOnlyNumberModel.prototype, "value", {
            get: function () {
                return this.getValue();
            },
            enumerable: true,
            configurable: true
        });
        return UIReadOnlyNumberModel;
    })(UINumberModel);
    NpTypes.UIReadOnlyNumberModel = UIReadOnlyNumberModel;
    var UIDateModel = (function (_super) {
        __extends(UIDateModel, _super);
        function UIDateModel() {
            _super.apply(this, arguments);
        }
        return UIDateModel;
    })(UIModel);
    NpTypes.UIDateModel = UIDateModel;
    var UIReadOnlyDateModel = (function (_super) {
        __extends(UIReadOnlyDateModel, _super);
        function UIReadOnlyDateModel(getValue, entity) {
            _super.call(this, undefined, entity);
            this.getValue = getValue;
            this.entity = entity;
        }
        Object.defineProperty(UIReadOnlyDateModel.prototype, "value", {
            get: function () {
                return this.getValue();
            },
            enumerable: true,
            configurable: true
        });
        return UIReadOnlyDateModel;
    })(UIDateModel);
    NpTypes.UIReadOnlyDateModel = UIReadOnlyDateModel;
    var UIBoolModel = (function (_super) {
        __extends(UIBoolModel, _super);
        function UIBoolModel() {
            _super.apply(this, arguments);
        }
        return UIBoolModel;
    })(UIModel);
    NpTypes.UIBoolModel = UIBoolModel;
    var UIGeoModel = (function (_super) {
        __extends(UIGeoModel, _super);
        function UIGeoModel() {
            _super.apply(this, arguments);
        }
        return UIGeoModel;
    })(UIModel);
    NpTypes.UIGeoModel = UIGeoModel;
    var UIReadOnlyBoolModel = (function (_super) {
        __extends(UIReadOnlyBoolModel, _super);
        function UIReadOnlyBoolModel(getValue, entity) {
            _super.call(this, undefined, entity);
            this.getValue = getValue;
            this.entity = entity;
        }
        Object.defineProperty(UIReadOnlyBoolModel.prototype, "value", {
            get: function () {
                return this.getValue();
            },
            enumerable: true,
            configurable: true
        });
        return UIReadOnlyBoolModel;
    })(UIBoolModel);
    NpTypes.UIReadOnlyBoolModel = UIReadOnlyBoolModel;
    var UIManyToOneModel = (function (_super) {
        __extends(UIManyToOneModel, _super);
        function UIManyToOneModel() {
            _super.apply(this, arguments);
        }
        return UIManyToOneModel;
    })(UIModel);
    NpTypes.UIManyToOneModel = UIManyToOneModel;
    var UIReadOnlyManyToOneModel = (function (_super) {
        __extends(UIReadOnlyManyToOneModel, _super);
        function UIReadOnlyManyToOneModel(getValue, entity) {
            _super.call(this, undefined, entity);
            this.getValue = getValue;
            this.entity = entity;
        }
        Object.defineProperty(UIReadOnlyManyToOneModel.prototype, "value", {
            get: function () {
                return this.getValue();
            },
            enumerable: true,
            configurable: true
        });
        return UIReadOnlyManyToOneModel;
    })(UIManyToOneModel);
    NpTypes.UIReadOnlyManyToOneModel = UIReadOnlyManyToOneModel;
    var UIBlobModel = (function (_super) {
        __extends(UIBlobModel, _super);
        function UIBlobModel(value, fileNameGetter, fileNameSetter, entity) {
            _super.call(this, value, entity);
            this.value = value;
            this.fileNameGetter = fileNameGetter;
            this.fileNameSetter = fileNameSetter;
            this.entity = entity;
            this.progress = 0;
            this.progressVisible = false;
        }
        Object.defineProperty(UIBlobModel.prototype, "filename", {
            get: function () {
                var f = this.fileNameGetter();
                if (f === null || f === undefined)
                    return "No files selected";
                return f;
            },
            set: function (vl) {
                this.fileNameSetter(vl);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(UIBlobModel.prototype, "fileid", {
            get: function () {
                return this.value;
            },
            set: function (vl) {
                this.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        return UIBlobModel;
    })(UIModel);
    NpTypes.UIBlobModel = UIBlobModel;
    // Store the info necessary per breadcrumb step (in the top bar)
    var BreadcrumbStep = (function () {
        // George needed the associated model, too (not just the url and the label)
        function BreadcrumbStep(url, menuId, label, model) {
            this.url = url;
            this.menuId = menuId;
            this.label = label;
            this.model = model;
            this.isAuthorized = false;
        }
        return BreadcrumbStep;
    })();
    NpTypes.BreadcrumbStep = BreadcrumbStep;
    // When spawning a dialog, this is what we pass in:
    var DialogOptions = (function () {
        function DialogOptions() {
            // The width of the dialog generated
            this.width = '45em';
            // Whether the dialog should autoshow itself immediately
            this.autoOpen = true;
            // Whether the dialog should be modal or not
            this.modal = true;
            // What CSS class to assign to the dialog's root <div> element
            this.className = undefined;
            // Should the dialog show a close button?
            this.showCloseButton = true;
        }
        return DialogOptions;
    })();
    NpTypes.DialogOptions = DialogOptions;
    var SFLDialogOptions = (function (_super) {
        __extends(SFLDialogOptions, _super);
        function SFLDialogOptions() {
            _super.call(this);
            this.className = Controllers.SqlLayerFilterController._ControllerClassName;
        }
        return SFLDialogOptions;
    })(DialogOptions);
    NpTypes.SFLDialogOptions = SFLDialogOptions;
    var LovDialogOptions = (function (_super) {
        __extends(LovDialogOptions, _super);
        function LovDialogOptions() {
            _super.apply(this, arguments);
            this.hasNewButton = false;
            this.showTotalItems = true;
            this.updateTotalOnDemand = false;
            // Dictionary of columns shown in this LOV
            this.shownCols = {};
            this.bMultiSelect = false;
        }
        return LovDialogOptions;
    })(DialogOptions);
    NpTypes.LovDialogOptions = LovDialogOptions;
    var useAsserts = false;
    ///////////////////////////////////////////////////////////
    // Base class for all components inside directives.ts
    var NpComponent = (function () {
        function NpComponent(element, attrs, $timeout, scope, bDebug) {
            // Is this component supposed to be filled-in, as in, mandatory?
            // This used to be a flag...
            //
            //if (attrs.npRequired !== undefined)
            //    this.required = scope.$eval(attrs.npRequired);
            //
            // ...but we now do it via $eval-ed property (see above),
            // so we don't need to store it.
            this.element = element;
            this.attrs = attrs;
            this.$timeout = $timeout;
            this.scope = scope;
            this.bDebug = bDebug;
            // The timeout promise that destroys the error popovers after X milliseconds
            this.promise = undefined;
            // Whenever $parse is called, it will also check whether there is a
            // user-provided validation. If there is, it will $eval it,
            // and check its return value (ValidationResult)
            this.validationCallback = undefined;
            // But we do store any npValidate expression.
            if (attrs.npValidate !== undefined)
                this.validationCallback = attrs.npValidate;
            if (attrs.npUserDefinedFormatter !== undefined) {
                this.userDefinedFormatter = function (mv, e) {
                    return scope[attrs.npUserDefinedFormatter](mv, e);
                };
            }
            if (attrs.npUserDefinedParser !== undefined) {
                this.userDefinedParser = function (vv, e) {
                    return scope[attrs.npUserDefinedParser](vv, e);
                };
            }
            // The uiModel may be unset - but the moment it is set for the first time,
            // we will $watch its validationMsgs.
            var self = this;
            // ...and setup the real one that will watch the uiModel's validationMsgs!
            scope.$watch(function () {
                var uim = self.uiModel;
                return (uim !== undefined && uim !== null) ? uim.validationMsgs : [];
            }, function (newVal2, oldVal2) {
                if (self.bDebug)
                    console.log("$watch-er on validationMsgs just triggered...");
                self.markAsGood();
                if (newVal2.length != 0) {
                    self.showError(self.uiModel.validationMsgs[0].msg, self.uiModel.validationMsgs[0].noHover);
                }
            }, true);
            scope.$watch(function () {
                var entity = self.entity;
                return (!isVoid(entity)) ? entity.getKey() : '';
            }, function (newEntityKey, oldEntityKey) {
                var entity = self.entity;
                if (isVoid(entity)) {
                    return;
                }
                if (isVoid(self.formatter)) {
                    return;
                }
                var value = scope.$eval(attrs.ngModel);
                value = self.formatter(value);
                self.element.val(value);
            });
        }
        NpComponent.prototype.checkUImodelAndAssertOrRunStatement = function (func) {
            var self = this;
            var condition = self.uiModel !== undefined && self.uiModel !== null;
            if (useAsserts) {
                // In debug mode
                if (!condition) {
                    console.error("ASSERTION FAILED! uiModel is void...");
                }
                else
                    func();
            }
            else {
                // In production mode - silence the error that would happen otherwise...
                if (condition)
                    func();
            }
        };
        NpComponent.prototype.checkUImodelAndAssertOrReturnExpression = function (func, defaultValue) {
            var self = this;
            var condition = self.uiModel !== undefined && self.uiModel !== null;
            if (useAsserts) {
                // In debug mode
                if (!condition) {
                    console.error("ASSERTION FAILED! uiModel is void...");
                    return defaultValue;
                }
                else
                    return func();
            }
            else {
                // In production mode - silence the error that would happen otherwise...
                if (condition)
                    return func();
                else
                    return defaultValue;
            }
        };
        NpComponent.prototype.safelyGetOldValue = function (defaultValueInCaseOfFailure) {
            var self = this;
            return self.checkUImodelAndAssertOrReturnExpression(function () {
                return self.uiModel.oldModelValue;
            }, defaultValueInCaseOfFailure);
        };
        NpComponent.prototype.safelySetOldValue = function (newValue) {
            var self = this;
            return self.checkUImodelAndAssertOrRunStatement(function () {
                self.uiModel.oldModelValue = newValue;
            });
        };
        NpComponent.prototype.safelyGetLastViewValue = function () {
            var self = this;
            return self.checkUImodelAndAssertOrReturnExpression(function () {
                return self.uiModel.lastViewValue;
            }, '');
        };
        NpComponent.prototype.safelySetLastViewValue = function (newViewValue) {
            var self = this;
            return self.checkUImodelAndAssertOrRunStatement(function () {
                self.uiModel.lastViewValue = newViewValue;
            });
        };
        NpComponent.prototype.clearAllErrors = function () {
            var self = this;
            return self.checkUImodelAndAssertOrRunStatement(function () {
                self.uiModel.clearAllErrors();
            });
        };
        NpComponent.prototype.addNewErrorMessage = function (msg, dontColorizeOrHover) {
            if (dontColorizeOrHover === void 0) { dontColorizeOrHover = false; }
            var self = this;
            return self.checkUImodelAndAssertOrRunStatement(function () {
                self.uiModel.addNewErrorMessage(msg, dontColorizeOrHover);
            });
        };
        Object.defineProperty(NpComponent.prototype, "uiModel", {
            // To work with 'row.entity' based directives that live inside ng-grids,
            // we need to evaluate the NeuroCode-provided np-ui-model variable.
            get: function () {
                return (this.scope.$eval(this.attrs.npUiModel));
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NpComponent.prototype, "entity", {
            // Similarly, entities are accessible via the corresponding UIModels,
            // but to avoid exceptions, have this check and return undefined 
            // when the UIModel is stillborn.
            get: function () {
                var uim = this.uiModel;
                if (isVoid(uim))
                    return undefined;
                else
                    return uim.entity;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NpComponent.prototype, "required", {
            // Is this component supposed to be filled-in, as in, mandatory?
            // This used to be a flag...
            //public required:boolean = false;
            // ...but it turns out we need to evaluate this at runtime, 
            // so it was turned to a property that $evals the input string.
            // And of course, that meant we have to store the $scope inside ourselves!
            // (see the constructor)
            get: function () {
                return this.scope.$eval(this.attrs.npRequired);
            },
            enumerable: true,
            configurable: true
        });
        // Some validation failed - show a bootstrap popover with 'msg':
        NpComponent.prototype.showPopover = function (msg) {
            var self = this;
            // Is there an already visible popover?
            if (self.promise === undefined) {
                // No. Is the the element still visible?
                if (self.element.is(":visible")) {
                    // Yes, so go ahead and show this new popover
                    self.element.popover({
                        animation: true, html: false, placement: 'bottom', trigger: 'manual', content: msg
                    }).popover('show');
                    // Schedule a popover destroying after 3000ms, and store it in .promise
                    self.promise = self.$timeout(function () {
                        self.element.popover('destroy');
                        // Unfortunately, the element we 'hang' the popover on, may have been destroyed
                        // (e.g. a dialog closing will kill its DOM elements)
                        // When this happens, the .popover('destroy') above will NOT work
                        // We therefore 'sweep' all .popover, just in case
                        $('.popover').remove();
                        self.promise = undefined;
                    }, 3000);
                }
            }
        };
        // Close the popover before the timer expires...
        NpComponent.prototype.killPopover = function () {
            var self = this;
            // Is there a currently visible popover? If so, there's an active promise to destroy it...
            if (self.promise !== undefined) {
                // Cancel the timer, destroy the popover:
                self.$timeout.cancel(self.promise);
                self.element.popover('destroy');
                // Unfortunately, the element we 'hang' the popover on, may have been destroyed
                // (e.g. a dialog closing will kill its DOM elements)
                // When this happens, the .popover('destroy') above will NOT work
                // We therefore 'sweep' all .popover, just in case
                $('.popover').remove();
                self.promise = undefined;
            }
        };
        NpComponent.prototype.markAsGood = function () {
            var self = this;
            // Close the popover if the timer hasn't expired yet.
            self.killPopover();
            // Stop hover in/out triggering the showing of popovers
            $(self.element).off("mouseenter mouseleave");
            // Stop highlighting the element with error colors
            $(self.element).removeClass('alert-error');
        };
        // Use a Bootstrap popover to notify the user of erroneous data
        NpComponent.prototype.showError = function (msg, dontColorizeOrHover) {
            if (dontColorizeOrHover === void 0) { dontColorizeOrHover = false; }
            var self = this;
            // If this message doesn't already exist in the uimodel state, add it.
            if (self.uiModel !== undefined)
                self.uiModel.addNewErrorMessage(msg, dontColorizeOrHover);
            // Destroy any old error popover
            self.killPopover();
            // Highlight the control with Bootstrap's error color
            if (!dontColorizeOrHover)
                $(self.element).addClass('alert-error');
            // Show the error message via a Bootstrap popover
            self.showPopover(msg);
            // The first time showError is called, when isValid is not set to false yet,
            // setup the hover/unhover to show/hide the popover
            if ((!self.uiModel.isValid) && (!dontColorizeOrHover)) {
                self.element.hover(
                // On hover in...
                function () {
                    // Only show the popover if it is not already there
                    if (self.promise === undefined) {
                        self.showPopover(msg);
                    }
                }, 
                // On hover out...
                function () {
                    self.killPopover();
                });
            }
        };
        NpComponent.prototype.getDynamicMessage = function (msgKey) {
            var ret = this.scope.$parent.globals.getDynamicMessage(msgKey);
            return !isVoid(ret) ? ret : msgKey;
        };
        return NpComponent;
    })();
    NpTypes.NpComponent = NpComponent;
    var NpBoolean = (function (_super) {
        __extends(NpBoolean, _super);
        function NpBoolean() {
            _super.apply(this, arguments);
        }
        return NpBoolean;
    })(NpComponent);
    NpTypes.NpBoolean = NpBoolean;
    var NpText = (function (_super) {
        __extends(NpText, _super);
        function NpText() {
            _super.apply(this, arguments);
        }
        return NpText;
    })(NpComponent);
    NpTypes.NpText = NpText;
    NpTypes.localeDecimalSeparator = '.';
    NpTypes.defaultDecimals = 0;
    NpTypes.defaultDecimalSeparator = ',';
    NpTypes.defaultThousandSeparator = '.';
    var NpNumber = (function (_super) {
        __extends(NpNumber, _super);
        function NpNumber(element, attrs, $timeout, scope, bDebug) {
            _super.call(this, element, attrs, $timeout, scope, bDebug);
            this.element = element;
            this.attrs = attrs;
            this.$timeout = $timeout;
            this.scope = scope;
            this.decimals = NpTypes.defaultDecimals;
            this.decSep = NpTypes.defaultDecimalSeparator;
            this.thSep = NpTypes.defaultThousandSeparator;
            this.min = Number.NEGATIVE_INFINITY;
            this.max = Number.POSITIVE_INFINITY;
            // This is how the decimal point is represented in a native floating point number's string representation:
            this.localeDecSep = NpTypes.localeDecimalSeparator;
            // Parse input params: min, max, number of decimals, decimal separator and thousands separator:
            if (attrs.npMin !== undefined)
                this.min = parseFloat(attrs.npMin);
            if (attrs.npMax !== undefined)
                this.max = parseFloat(attrs.npMax);
            if (attrs.npDecimals !== undefined)
                this.decimals = parseInt(attrs.npDecimals);
            if (attrs.npDecsep !== undefined)
                this.decSep = attrs.npDecsep;
            if (attrs.npThsep !== undefined)
                this.thSep = attrs.npThsep;
            // cache the validation and cleaning regexps inside ourselves
            // (i.e. don't compile them all the time)
            //
            // Validation regexp:
            var regex = "^(-)?[0-9]*(" + this.decSep + "([0-9]{0," + this.decimals + "}))?$";
            this.compiledRegExValidate = new RegExp(regex);
            // Clean regexp (used to remove the thousands separator char:
            this.compiledRegExClean = new RegExp(this.thSep.replace(/\./g, "\\."), 'g');
        }
        NpNumber.prototype.viewTextToNumber = function (t) {
            if (!this.required && t == '')
                return null;
            // Use the clean regexp to remove the thousands separators,
            // and then replace the decimal separator with the locale-specific one.
            return parseFloat(t.replace(this.compiledRegExClean, '').replace(this.decSep, this.localeDecSep));
        };
        return NpNumber;
    })(NpComponent);
    NpTypes.NpNumber = NpNumber;
    var NpDate = (function (_super) {
        __extends(NpDate, _super);
        function NpDate(element, attrs, $timeout, scope, bDebug) {
            var _this = this;
            _super.call(this, element, attrs, $timeout, scope, bDebug);
            this.element = element;
            this.attrs = attrs;
            this.$timeout = $timeout;
            this.scope = scope;
            this.format = 'dd/MM/yyyy';
            this.minDate = new Date(-3000000000000);
            this.maxDate = new Date(4000000000000);
            this.sanitizedDatePartLengths = [];
            this.datePickerYearRange = 'c-10:c+10';
            // Parse input params: format...
            if (attrs.npFormat !== undefined)
                this.format = attrs.npFormat;
            // one without the dashes and the slashes (numbers only):
            this.formatNumbersOnly = this.format.replace(/\//g, '').replace(/-/g, '');
            if (attrs.npDatePickerYearRange !== undefined)
                this.datePickerYearRange = attrs.npDatePickerYearRange;
            var viewTextToDate = function (t) {
                return Date.parseString(t, _this.format);
            };
            // min and max allowed dates:
            if (attrs.npMinDate !== undefined) {
                this.minDate = viewTextToDate(scope.$eval(attrs.npMinDate));
                if (this.minDate === null)
                    console.error("Invalid specification for np-min-date (" + attrs.npMinDate + ")" +
                        " with format " + attrs.npFormat);
            }
            if (attrs.npMaxDate !== undefined) {
                this.maxDate = viewTextToDate(scope.$eval(attrs.npMaxDate));
                if (this.maxDate === null)
                    console.error("Invalid specification for np-min-date (" + attrs.npMaxDate + ")" +
                        " with format " + attrs.npFormat);
            }
            // cache the validation regexp inside ourselves (don't compile it all the time)
            var regex = this.format.slice(0);
            var regex = regex.replace(/d/ig, '\\d');
            var regex = regex.replace(/m/ig, '\\d');
            var regex = regex.replace(/y/ig, '\\d');
            var regex = regex.replace(/\//g, '/?');
            var regex = regex.replace(/-/g, '/?');
            this.compiledRegExValidate = new RegExp('^' + regex + '$');
            this.sanitizedDatePartLengths = _.map(this.format.replace(/[\/-]/g, '@').split('@'), function (part) { return part.length; });
        }
        return NpDate;
    })(NpComponent);
    NpTypes.NpDate = NpDate;
    var NpRichtext = (function (_super) {
        __extends(NpRichtext, _super);
        function NpRichtext() {
            _super.apply(this, arguments);
        }
        return NpRichtext;
    })(NpComponent);
    NpTypes.NpRichtext = NpRichtext;
    var NpSelect = (function (_super) {
        __extends(NpSelect, _super);
        function NpSelect() {
            _super.apply(this, arguments);
            this.choices = 'x[1] as x[0] for x in [["yes", 1], ["no", 0]]';
        }
        return NpSelect;
    })(NpComponent);
    NpTypes.NpSelect = NpSelect;
    var NpLookup = (function (_super) {
        __extends(NpLookup, _super);
        function NpLookup(element, attrs, $timeout, scope, bDebug) {
            _super.call(this, element, attrs, $timeout, scope, bDebug);
            this.element = element;
            this.attrs = attrs;
            this.$timeout = $timeout;
            this.scope = scope;
            if (attrs.npLookup === undefined) {
                console.error("npLookup: you forgot to pass data-np-lookup");
                return;
            }
            this.npLookup = attrs.npLookup;
            if (attrs.npSource === undefined) {
                console.error("npLookup: you forgot to pass data-np-source");
                return;
            }
            this.npSource = attrs.npSource;
            this.npContext = attrs.npContext;
        }
        return NpLookup;
    })(NpComponent);
    NpTypes.NpLookup = NpLookup;
    var NpBlob = (function () {
        function NpBlob(blobChangedCallback, getURL, isDisabled, isInvisible, postURL, isPlusEnabled, isMinusEnabled, extensions, getMaximumSizeInKB) {
            if (extensions === void 0) { extensions = ''; }
            if (getMaximumSizeInKB === void 0) { getMaximumSizeInKB = function (entity) { return 1024; }; }
            this.blobChangedCallback = blobChangedCallback;
            this.getURL = getURL;
            this.isDisabled = isDisabled;
            this.isInvisible = isInvisible;
            this.postURL = postURL;
            this.isPlusEnabled = isPlusEnabled;
            this.isMinusEnabled = isMinusEnabled;
            this.extensions = extensions;
            this.getMaximumSizeInKB = getMaximumSizeInKB;
        }
        return NpBlob;
    })();
    NpTypes.NpBlob = NpBlob;
    var NpFileUpload = (function () {
        function NpFileUpload(formDataAppendCallback, fileUpLoadedCallback, getLabel, isDisabled, isInvisible, postURL, helpButtonAction, extensions, maximumSizeInKB) {
            if (extensions === void 0) { extensions = ''; }
            if (maximumSizeInKB === void 0) { maximumSizeInKB = 1024; }
            this.formDataAppendCallback = formDataAppendCallback;
            this.fileUpLoadedCallback = fileUpLoadedCallback;
            this.getLabel = getLabel;
            this.isDisabled = isDisabled;
            this.isInvisible = isInvisible;
            this.postURL = postURL;
            this.helpButtonAction = helpButtonAction;
            this.extensions = extensions;
            this.maximumSizeInKB = maximumSizeInKB;
        }
        return NpFileUpload;
    })();
    NpTypes.NpFileUpload = NpFileUpload;
    var ValidationResult = (function () {
        function ValidationResult(isValid, errorMessage) {
            this.isValid = isValid;
            this.errorMessage = errorMessage;
        }
        return ValidationResult;
    })();
    NpTypes.ValidationResult = ValidationResult;
    // For the Bootstrap alert messages
    (function (AlertType) {
        AlertType[AlertType["SUCCESS"] = 0] = "SUCCESS";
        AlertType[AlertType["INFO"] = 1] = "INFO";
        AlertType[AlertType["WARNING"] = 2] = "WARNING";
        AlertType[AlertType["DANGER"] = 3] = "DANGER";
    })(NpTypes.AlertType || (NpTypes.AlertType = {}));
    var AlertType = NpTypes.AlertType;
    var AlertData = (function () {
        function AlertData(kind, msg) {
            this.kind = kind;
            this.msg = msg;
        }
        return AlertData;
    })();
    NpTypes.AlertData = AlertData;
    var AlertMessage = (function () {
        function AlertMessage(type, msg) {
            this.type = type;
            this.msg = msg;
            if (type !== "success" && type !== "info" && type !== "warning" && type !== "danger")
                console.error("AlertMessage: " + type + " is an unsupported option");
        }
        AlertMessage.addSuccess = function (model, msg) {
            model.alertData.push(new AlertData(AlertType.SUCCESS, msg));
            AlertMessage.updateAlerts(model);
        };
        AlertMessage.addInfo = function (model, msg) {
            model.alertData.push(new AlertData(AlertType.INFO, msg));
            AlertMessage.updateAlerts(model);
        };
        AlertMessage.addWarning = function (model, msg) {
            model.alertData.push(new AlertData(AlertType.WARNING, msg));
            AlertMessage.updateAlerts(model);
        };
        AlertMessage.addDanger = function (model, msg) {
            model.alertData.push(new AlertData(AlertType.DANGER, msg));
            AlertMessage.updateAlerts(model);
        };
        AlertMessage.updateAlerts = function (model) {
            var success = _.map(_.filter(model.alertData, function (d) { return d.kind == AlertType.SUCCESS; }), function (d) { return d.msg; }).join("</div><div>");
            var info = _.map(_.filter(model.alertData, function (d) { return d.kind == AlertType.INFO; }), function (d) { return d.msg; }).join("</div><div>");
            var warning = _.map(_.filter(model.alertData, function (d) { return d.kind == AlertType.WARNING; }), function (d) { return d.msg; }).join("</div><div>");
            var danger = _.map(_.filter(model.alertData, function (d) { return d.kind == AlertType.DANGER; }), function (d) { return d.msg; }).join("</div><div>");
            var newAlerts = [];
            if (success != "")
                newAlerts.push(new AlertMessage('success', success));
            if (info != "")
                newAlerts.push(new AlertMessage('info', info));
            if (warning != "")
                newAlerts.push(new AlertMessage('warning', warning));
            if (danger != "")
                newAlerts.push(new AlertMessage('danger', danger));
            model.alerts = newAlerts;
        };
        AlertMessage.clearAlerts = function (model) {
            model.alertData = [];
            AlertMessage.updateAlerts(model);
        };
        return AlertMessage;
    })();
    NpTypes.AlertMessage = AlertMessage;
    var MainAppTemplate = (function () {
        function MainAppTemplate() {
            var _this = this;
            this.showNpHeader = false;
            this.showNpBreadcrumb = false;
            this.showNpMenu = false;
            this.showNpFooter = false;
            this.showVersion = false;
            this.showAll = function (bShow) {
                _this.showNpHeader = bShow;
                _this.showNpBreadcrumb = bShow;
                _this.showNpMenu = bShow;
                _this.showNpFooter = bShow;
                _this.showVersion = bShow;
            };
        }
        return MainAppTemplate;
    })();
    NpTypes.MainAppTemplate = MainAppTemplate;
    var RequestAuthorizationOptions = (function () {
        function RequestAuthorizationOptions() {
        }
        return RequestAuthorizationOptions;
    })();
    NpTypes.RequestAuthorizationOptions = RequestAuthorizationOptions;
    var RTLGlobals = (function () {
        function RTLGlobals() {
            this.dialogOptionsList = [];
            this.wsResponseInfoList = [];
            // For future use: dynamic loading of controllers
            //
            // controllerProvider: ng.IControllerProvider;
            this.mainAppTemplate = new MainAppTemplate();
            this._onReloadAppLocation = '';
            this.requestAuthorizationOptions = new RequestAuthorizationOptions();
            this.transactionLevels = [false];
            this._appName = new UIStringModel(undefined);
            this._appTitle = new UIStringModel(undefined);
            this._sessionClientIp = new UIStringModel(undefined);
            this._appWsResponeInfoEnabled = new UIBoolModel(undefined);
            this._globalUserEmail = new UIStringModel(undefined);
            this._globalUserActiveEmail = new UIStringModel(undefined);
            this._globalUserLoginName = new UIStringModel(undefined);
            this._globalUserVat = new UIStringModel(undefined);
            this._globalUserId = new UINumberModel(undefined);
            this._globalSubsId = new UINumberModel(undefined);
            this._globalSubsDescription = new UIStringModel(undefined);
            this._globalSubsCode = new UIStringModel(undefined);
            this._globalSubsSecClasses = new NpTypes.UIModel(undefined);
            this._globalLang = new UIStringModel(undefined);
            this.bDigestHackEnabled = true;
            this.timeoutInMS = 60000;
            this.privileges = {};
            this.version = 'TIMESTAMP';
            this.nInFlightRequests = 0;
            this.nPKSequence = 0;
            this.bLoggingOut = false;
        }
        // called by controllerMain
        RTLGlobals.prototype.initApp = function ($scope, NavigationService, loginService, $http, $timeout, onInitFunc) {
            if (!isVoid(onInitFunc))
                onInitFunc();
        };
        Object.defineProperty(RTLGlobals.prototype, "sessionCookieKey", {
            get: function () {
                return this.appName.slice(0, 1).toLowerCase() + this.appName.slice(1) + '-session-id';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "subsCodeCookieKey", {
            get: function () {
                return this.appName.slice(0, 1).toLowerCase() + this.appName.slice(1) + '-subs-code';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "ssoCookieKey", {
            get: function () { return 'sso-value'; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "onReloadAppLocation", {
            get: function () { return this._onReloadAppLocation; },
            set: function (vl) { this._onReloadAppLocation = vl; },
            enumerable: true,
            configurable: true
        });
        RTLGlobals.prototype.hasPrivilege = function (privile) {
            return this.privileges[privile] === true;
        };
        Object.defineProperty(RTLGlobals.prototype, "isCurrentTransactionDirty", {
            get: function () {
                return this.transactionLevels[this.transactionLevels.length - 1];
            },
            set: function (v) {
                this.transactionLevels[this.transactionLevels.length - 1] = v;
            },
            enumerable: true,
            configurable: true
        });
        RTLGlobals.prototype.createNewTransactionLevel = function () {
            this.transactionLevels.push(false);
        };
        RTLGlobals.prototype.dropLastTransactionLevel = function () {
            this.transactionLevels.pop();
        };
        RTLGlobals.prototype.onMenuClick = function (menuId) {
            if (menuId !== undefined) {
            }
        };
        Object.defineProperty(RTLGlobals.prototype, "appName", {
            get: function () { return this._appName.value; },
            set: function (vl) { this._appName.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "appTitle", {
            get: function () { return this._appTitle.value; },
            set: function (vl) { this._appTitle.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "sessionClientIp", {
            get: function () { return this._sessionClientIp.value; },
            set: function (vl) { this._sessionClientIp.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "appWsResponeInfoEnabled", {
            get: function () { return this._appWsResponeInfoEnabled.value; },
            set: function (vl) {
                this._appWsResponeInfoEnabled.value = vl;
                if (!isVoid(vl) && !vl)
                    this.wsResponseInfoList.splice(0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "globalUserEmail", {
            get: function () { return this._globalUserEmail.value; },
            set: function (vl) { this._globalUserEmail.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "globalUserActiveEmail", {
            get: function () { return this._globalUserActiveEmail.value; },
            set: function (vl) { this._globalUserActiveEmail.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "globalUserLoginName", {
            get: function () { return this._globalUserLoginName.value; },
            set: function (vl) { this._globalUserLoginName.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "globalUserVat", {
            get: function () { return this._globalUserVat.value; },
            set: function (vl) { this._globalUserVat.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "globalUserId", {
            get: function () { return this._globalUserId.value; },
            set: function (vl) { this._globalUserId.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "globalSubsId", {
            get: function () { return this._globalSubsId.value; },
            set: function (vl) { this._globalSubsId.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "globalSubsDescription", {
            get: function () { return this._globalSubsDescription.value; },
            set: function (vl) { this._globalSubsDescription.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "globalSubsCode", {
            get: function () { return this._globalSubsCode.value; },
            set: function (vl) { this._globalSubsCode.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "globalSubsSecClasses", {
            get: function () { return this._globalSubsSecClasses.value; },
            set: function (vl) { this._globalSubsSecClasses.value = vl; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RTLGlobals.prototype, "globalLang", {
            get: function () { return this._globalLang.value; },
            set: function (vl) { this._globalLang.value = vl; },
            enumerable: true,
            configurable: true
        });
        /*private _initialDialogHasBeenDisplay: boolean = false;
        public get initialDialogHasBeenDisplay(): boolean { return this._initialDialogHasBeenDisplay}
        public set initialDialogHasBeenDisplay(vl: boolean) {
            console.log("initialDialogHasBeenDisplay set, old:" + this.initialDialogHasBeenDisplay + ", new:" + vl);
            this._initialDialogHasBeenDisplay = vl;
        }*/
        RTLGlobals.prototype.addPrivileges = function (privs) {
            var self = this;
            if (privs !== undefined) {
                _.forEach(privs, function (priv) { self.privileges[priv] = true; });
            }
        };
        RTLGlobals.prototype.getNextSequenceValue = function () {
            return 'TEMP_ID_' + (this.nPKSequence++).toString();
        };
        RTLGlobals.prototype.initialize = function ($scope, $timeout, $http, Plato) {
        };
        RTLGlobals.prototype.initializeServerSideGlobals = function (response) {
        };
        RTLGlobals.prototype.showInitialDialog = function ($scope, $timeout, $http, Plato) {
        };
        RTLGlobals.prototype.addDialogOption = function (dlgOption) {
            this.dialogOptionsList.push(dlgOption);
        };
        RTLGlobals.prototype.findAndRemoveDialogOptionByClassName = function (clsName) {
            return this.dialogOptionsList.removeFirstElement(function (x) { return x.className === clsName; });
        };
        RTLGlobals.prototype.addWsResponseInfo = function (path, startTs, endTs, success, status) {
            if (isVoid(this.appWsResponeInfoEnabled) || this.appWsResponeInfoEnabled)
                this.wsResponseInfoList.push({
                    path: path,
                    startTs: startTs,
                    endTs: endTs,
                    success: success,
                    errorCode: isVoid(status) ? "" : status.toString()
                });
        };
        RTLGlobals.prototype.getAndResetWsResponseInfoList = function () {
            if (isVoid(this.appWsResponeInfoEnabled) || this.appWsResponeInfoEnabled) {
                return this.wsResponseInfoList.splice(0);
            }
            else {
                return [];
            }
        };
        RTLGlobals.prototype.onGlobalLangChange = function (ctrl) {
        };
        RTLGlobals.prototype.isGlobalLangControlDisabled = function () {
            return false;
        };
        RTLGlobals.prototype.isLoginSubscriberInvisible = function () {
            return false;
        };
        RTLGlobals.prototype.getDynamicMessage = function (msgKey) {
            console.log("getDynamicMessage function is not defined in globals");
            return msgKey;
        };
        RTLGlobals.prototype.getLoggedInUserDescription = function () {
            return !isVoid(this.globalUserLoginName) ? this.globalUserLoginName : this.globalUserEmail;
        };
        return RTLGlobals;
    })();
    NpTypes.RTLGlobals = RTLGlobals;
    ;
    ;
    function getReportExtensionByFormat(formatValue) {
        if (isVoid(formatValue))
            return null;
        if (formatValue == 0)
            return "xls";
        if (formatValue == 1)
            return "pdf";
        if (formatValue == 2)
            return "docx";
        if (formatValue == 3)
            return "txt";
    }
    NpTypes.getReportExtensionByFormat = getReportExtensionByFormat;
})(NpTypes || (NpTypes = {}));
;
String.prototype.startsWith = function (str) {
    return this.indexOf(str) == 0;
};
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
String.prototype.U1 = function () {
    var self = this;
    return self.substr(0, 1).toUpperCase() + self.substr(1);
};
Array.prototype.groupBy = function groupBy(projection, toString) {
    var self = this;
    var tmpMap = {};
    var ret = [];
    for (var i = 0; i < self.length; i++) {
        var curElem = self[i];
        var key = projection(curElem);
        var keyStr = toString === undefined ? key.toString() : toString(key);
        if (tmpMap[keyStr] === undefined) {
            tmpMap[keyStr] = new Tuple2(key, [curElem]);
        }
        else {
            tmpMap[keyStr].b.push(curElem);
        }
    }
    var keys = Object.keys(tmpMap);
    for (var j = 0; j < keys.length; j++) {
        ret.push(tmpMap[keys[j]]);
    }
    return ret;
};
Array.prototype.first = function (predicate) {
    var self = this;
    var ret = self.filter(predicate);
    if (ret.length === 0) {
        throw "No element found (Array.prototype.first)";
    }
    return ret[0];
};
Array.prototype.firstOrNull = function (predicate) {
    var self = this;
    var ret = self.filter(predicate);
    return ret.length === 0 ? null : ret[0];
};
Array.prototype.removeFirstElement = function (predicate) {
    var indexToRemove = -1;
    for (var i = 0; i < this.length; i++) {
        var curElement = this[i];
        if (predicate(curElement)) {
            indexToRemove = i;
            break;
        }
    }
    if (indexToRemove >= 0) {
        var itemToRemove = this[indexToRemove];
        this.splice(indexToRemove, 1);
        return itemToRemove;
    }
    return null;
};
Array.prototype.sumBy = function (f) {
    var ret = 0;
    for (var i = 0; i < this.length; i++) {
        var curElement = this[i];
        ret = ret + f(curElement);
    }
    return ret;
};
Array.prototype.maxBy = function (f, initialValue) {
    if (initialValue === void 0) { initialValue = 0; }
    var ret = initialValue;
    for (var i = 0; i < this.length; i++) {
        var curElement = this[i];
        var curValue = f(curElement);
        if (isVoid(curValue)) {
            continue;
        }
        if (curValue > ret) {
            ret = curValue;
        }
    }
    return ret;
};
Date.prototype.withoutTime = function () {
    var self = this;
    self.setHours(0, 0, 0, 0);
    return self;
};
// moved here from NpNumber directive (function numberToViewText)
Number.prototype.toStringFormatted = function (decSep, thSep, decimals) {
    var n = this;
    decimals = isVoid(decimals) ? NpTypes.defaultDecimals : decimals;
    decSep = isVoid(decSep) ? NpTypes.defaultDecimalSeparator : decSep;
    thSep = isVoid(thSep) ? NpTypes.defaultThousandSeparator : thSep;
    if (isVoid(n))
        return ''; // the model may be undefined/null by the user
    var parts = n.toString().split(NpTypes.localeDecimalSeparator);
    // for the integer part, add thousands separators:
    // From http://stackoverflow.com/questions/17294959/how-does-b-d3-d-g-work-for-adding-comma-on-numbers
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thSep);
    // for the fractional part, if it doesn't exist, add it:
    if (!isVoid(decimals) && decimals !== 0) {
        if (parts.length === 1)
            parts.push('');
        // ...and then make sure it is lengthy up to the required decimals
        while (parts[1].length < decimals)
            parts[1] += "0";
    }
    return parts.join(decSep);
};
//# sourceMappingURL=npTypes.js.map 
**/
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
/**
