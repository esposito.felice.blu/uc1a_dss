﻿/// <reference path="BaseController.ts" />

module Controllers.ExcelFileImportResultsDlg {

    export interface IExcelFile extends NpTypes.IBaseEntity {
        id: string;
        filename: string;
        data: string;
        totalRows: number;
        totalErrorRows: number;
        totalImportedRows: number;
        _data: NpTypes.UIBlobModel
    }

    interface IExcelError extends NpTypes.IBaseEntity {
        id: string;
        excelRowNum: number;
        errMessage: string;
        exfiId: IExcelFile
    }

    function fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): NpTypes.IBaseEntity {
        return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
    }

    function fromJSONPartial(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind): NpTypes.IBaseEntity {
        var self = this;
        var key = "";
        var ret: NpTypes.IBaseEntity = undefined;

        if (x.$refId !== undefined && x.$refId !== null) {
            key = x.$entityName + ":" + x.$refId;
            ret = deserializedEntities[key];
            if (ret === undefined) {
                throw new NpTypes.NpException(NpTypes.ExceptionSeverity.programmerError, x.$entityName + "::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
            }

        } else {
            ret = fromJSONPartial_actualdecode(x, deserializedEntities);
        }

        return ret;
    }
    function fromJSONComplete(data: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}): Array<NpTypes.IBaseEntity> {
        var self = this;
        var ret: Array<NpTypes.IBaseEntity> = _.map(data, (x: any): NpTypes.IBaseEntity => {
            return fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
        });

        return ret;
    }


    /**
     * CONTROLLERS
     */

    export class PageModel extends AbstractPageModel {
        modelGrpExcelFile: ModelGrpExcelFile;
        modelGrpExcelError: ModelGrpExcelError;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }

    export interface IPageScope extends IAbstractPageScope {
        globals: NpTypes.RTLGlobals;
        pageModel: PageModel;
    }

    export class PageController extends AbstractPageController {

        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
            var self: PageController = this;
            var model: PageModel = <PageModel>self.model;
            model.controller = <PageController>self;
            $scope.pageModel = model;

            $timeout(function () {
                $('#ExcelFile').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }

        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "ExcelFileImportResultsDlg";
        }

        public _getPageTitle(): string {
            return "Αποτελέσματα Εισαγωγής Αρχείου Excel";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public get model(): PageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get pageModel(): PageModel {
            var self = this;
            return self.model;
        }


        public update(): void {
            var self = this;
            self.model.modelGrpExcelFile.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpExcelFile.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpExcelError.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpExcelFile.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpExcelError.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController> = undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpExcelFile.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController> = undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpExcelFile.controller,
                    self.model.modelGrpExcelError.controller
                ];
            }
            return this._allGroupControllers;
        }

        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            actualNavigation(self.$scope);
        }

    }


    export class ModelGrpExcelFile extends Controllers.AbstractGroupFormModel {
        modelGrpExcelError: ModelGrpExcelError;
        controller: ControllerGrpExcelFile;
        public get id(): string {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<IExcelFile>this.selectedEntities[0]).id;
        }

        public set id(id_newVal: string) {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<IExcelFile>this.selectedEntities[0]).id = id_newVal;
        }

        public get _id(): NpTypes.UIStringModel {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._id;
        }

        public get filename(): string {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<IExcelFile>this.selectedEntities[0]).filename;
        }

        public set filename(filename_newVal: string) {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<IExcelFile>this.selectedEntities[0]).filename = filename_newVal;
        }

        public get _filename(): NpTypes.UIStringModel {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._filename;
        }

        public get data(): string {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<IExcelFile>this.selectedEntities[0]).data;
        }

        public set data(data_newVal: string) {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<IExcelFile>this.selectedEntities[0]).data = data_newVal;
        }

        public get _data(): NpTypes.UIBlobModel {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._data;
        }

        public get totalRows(): number {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<IExcelFile>this.selectedEntities[0]).totalRows;
        }

        public set totalRows(totalRows_newVal: number) {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<IExcelFile>this.selectedEntities[0]).totalRows = totalRows_newVal;
        }

        public get _totalRows(): NpTypes.UINumberModel {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._totalRows;
        }

        public get totalErrorRows(): number {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<IExcelFile>this.selectedEntities[0]).totalErrorRows;
        }

        public set totalErrorRows(totalErrorRows_newVal: number) {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<IExcelFile>this.selectedEntities[0]).totalErrorRows = totalErrorRows_newVal;
        }

        public get _totalErrorRows(): NpTypes.UINumberModel {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._totalErrorRows;
        }

        public get totalImportedRows(): number {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return ((<IExcelFile>this.selectedEntities[0]).totalImportedRows != undefined ? (<IExcelFile>this.selectedEntities[0]).totalImportedRows : 0);
        }

        public set totalImportedRows(totalImportedRows_newVal: number) {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<IExcelFile>this.selectedEntities[0]).totalImportedRows = totalImportedRows_newVal;
        }

        public get _totalImportedRows(): NpTypes.UINumberModel {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._totalImportedRows;
        }

        ExcelFileEntityName: string;
        ExcelErrorEntityName: string;

        constructor(public $scope: IScopeGrpExcelFile) { super($scope); }
    }


    export interface IScopeGrpExcelFile extends Controllers.IAbstractFormGroupScope, IPageScope {
        globals: NpTypes.RTLGlobals;
        modelGrpExcelFile: ModelGrpExcelFile;
        createBlobModel_GrpExcelFile_itm__data(): NpTypes.NpBlob;
    }

    export class ControllerGrpExcelFile extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpExcelFile,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpExcelFile($scope));

            var self: ControllerGrpExcelFile = this;
            var model: ModelGrpExcelFile = self.model;
            model.controller = <ControllerGrpExcelFile>self;
            $scope.modelGrpExcelFile = model;

            $scope.createBlobModel_GrpExcelFile_itm__data =
                (): NpTypes.NpBlob => {
                    var tmp = self.createBlobModel_GrpExcelFile_itm__data();
                    return tmp;
                };

            $scope.pageModel.modelGrpExcelFile = $scope.modelGrpExcelFile;

            model.ExcelFileEntityName = $scope.pageModel.dialogOptions.customParams.ExcelFileEntityName;
            model.ExcelErrorEntityName = $scope.pageModel.dialogOptions.customParams.ExcelErrorEntityName;

            self.getExcelFileById($scope.pageModel.dialogOptions.customParams.excelFileId, (excelFile) => {
                excelFile._data.fileNameGetter = () => { return excelFile.filename; }
                $scope.modelGrpExcelFile.visibleEntities[0] = excelFile;
                $scope.modelGrpExcelFile.selectedEntities[0] = excelFile;
            });

        }

        public getExcelFileById(excelFileId: string, afterCallFunc: (excelFile: IExcelFile) => void): void {
            var self = this;
            if (isVoid(self.getEntityName()) || self.model.selectedEntities.length !== 0)
                return;
            var url = "/" + self.AppName + "/rest/" + self.getEntityName() + "/findById?";
            self.httpPost(url, { "id": excelFileId }, rsp => {
                var excelFiles = this.getEntitiesFromJSON(rsp);
                if (excelFiles.length === 0)
                    return;
                if (afterCallFunc !== undefined)
                    afterCallFunc(<IExcelFile>(excelFiles[0]));
            });

        }


        public get ControllerClassName(): string {
            return "ControllerGrpExcelFile";
        }
        public get HtmlDivId(): string {
            return "ExcelFileImportResultsDlg_ControllerGrpExcelFile";
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get model(): ModelGrpExcelFile {
            var self = this;
            return self.$scope.model;
        }

        public get modelGrpExcelFile(): ModelGrpExcelFile {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            var self = this;
            return self.model.ExcelFileEntityName;
        }


        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var self = this;
            var entlist: Array<IExcelFile> = <IExcelFile[]>fromJSONComplete(webResponse.data);

            return entlist;
        }

        public get Current(): IExcelFile {
            var self = this;
            return <IExcelFile>self.$scope.modelGrpExcelFile.selectedEntities[0];
        }


        _firstLevelChildGroups: Array<AbstractGroupController> = undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpExcelFile.modelGrpExcelError.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public createBlobModel_GrpExcelFile_itm__data(): NpTypes.NpBlob {
            var self = this;
            var ret =
                new NpTypes.NpBlob(
                    (e: IExcelFile) => { }, // on value change
                    (e: IExcelFile) => { return self.getDownloadUrl_createBlobModel_GrpExcelFile_itm__data(e); },            // download url
                    (e: IExcelFile) => true,                                                           // is disabled
                    (e: IExcelFile) => false,                                                                 // is invisible
                    "",                                     // post url
                    false, // add is enabled
                    false, // del is enabled
                    "",    // valid extensions
                    (e: IExcelFile) => 0    //size in KB
                );
            return ret;
        }
        public getDownloadUrl_createBlobModel_GrpExcelFile_itm__data(excelFile: IExcelFile): string {
            var self = this;
            if (isVoid(self.getEntityName()))
                return 'javascript:void(0)'
            if (isVoid(excelFile))
                return 'javascript:void(0)'
            if (isVoid(excelFile.id))
                return 'javascript:void(0)';
            var url = "/" + self.AppName + "/rest/" + self.getEntityName() + "/getExcelFileImportResultsGrpExcelFile_Data?";
            url += "&id=" + encodeURIComponent(excelFile.id);
            return url;
        }

    }


    export class ModelGrpExcelError extends AbstractGroupTableModel {
        controller: ControllerGrpExcelError;
        public get id(): string {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<IExcelError>this.selectedEntities[0]).id;
        }

        public set id(id_newVal: string) {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<IExcelError>this.selectedEntities[0]).id = id_newVal;
        }

        public get _id(): NpTypes.UIStringModel {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._id;
        }

        public get excelRowNum(): number {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<IExcelError>this.selectedEntities[0]).excelRowNum;
        }

        public set excelRowNum(excelRowNum_newVal: number) {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<IExcelError>this.selectedEntities[0]).excelRowNum = excelRowNum_newVal;
        }

        public get _excelRowNum(): NpTypes.UINumberModel {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._excelRowNum;
        }

        public get errMessage(): string {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<IExcelError>this.selectedEntities[0]).errMessage;
        }

        public set errMessage(errMessage_newVal: string) {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<IExcelError>this.selectedEntities[0]).errMessage = errMessage_newVal;
        }

        public get _errMessage(): NpTypes.UIStringModel {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._errMessage;
        }

        public get exfiId(): IExcelFile {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<IExcelError>this.selectedEntities[0]).exfiId;
        }

        public set exfiId(exfiId_newVal: IExcelFile) {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<IExcelError>this.selectedEntities[0]).exfiId = exfiId_newVal;
        }

        public get _exfiId(): NpTypes.UIManyToOneModel<IExcelFile> {
            if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._exfiId;
        }

        _fsch_row: NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_row(): number {
            return this._fsch_row.value;
        }
        public set fsch_row(vl: number) {
            this._fsch_row.value = vl;
        }
        _fsch_errMessage: NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_errMessage(): string {
            return this._fsch_errMessage.value;
        }
        public set fsch_errMessage(vl: string) {
            this._fsch_errMessage.value = vl;
        }

        constructor(public $scope: IScopeGrpExcelError) { super($scope); }
    }


    export interface IScopeGrpExcelError extends Controllers.IAbstractTableGroupScope, IScopeGrpExcelFile {
        globals: NpTypes.RTLGlobals;
        modelGrpExcelError: ModelGrpExcelError;
    }

    export class ControllerGrpExcelError extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpExcelError,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpExcelError($scope), { pageSize: 10, maxLinesInHeader: 2 });

            var self: ControllerGrpExcelError = this;
            var model: ModelGrpExcelError = self.model;
            model.controller = <ControllerGrpExcelError>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpExcelError = self.model;

            $scope.pageModel.modelGrpExcelError = $scope.modelGrpExcelError;

            $scope.clearBtnAction = () => {
                self.modelGrpExcelError.fsch_row = undefined;
                self.modelGrpExcelError.fsch_errMessage = undefined;
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpExcelFile.modelGrpExcelError = $scope.modelGrpExcelError;
            self.$timeout(
                () => {
                    $scope.$on('$destroy', <(evt: ng.IAngularEvent, ...cols: any[]) => any>(
                        $scope.$watch('modelGrpExcelFile.selectedEntities[0]', () => { self.onParentChange(); }, false)));
                },
                50);

            $scope.$on('$destroy', (evt: ng.IAngularEvent, ...cols: any[]): void => {
            });
        }

        public get model(): ModelGrpExcelError {
            var self = this;
            return self.$scope.model;
        }

        public get ControllerClassName(): string {
            return "ControllerGrpExcelError";
        }
        public get HtmlDivId(): string {
            return "ExcelFile_ControllerGrpExcelError";
        }

        public gridTitle(): string {
            return "ImportExvel_ResultsDlg_GridTitle";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                /*{ width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },*/
                {
                    cellClass: 'cellToolTip', field: 'excelRowNum', displayName: 'getALString("Row\nNum", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '10%', cellTemplate: "<div data-ng-class='col.colIndex()' > \
                    <input name='GrpExcelError_itm__excelRowNum' data-ng-model='row.entity.excelRowNum' data-np-ui-model='row.entity._excelRowNum' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                {
                    cellClass: 'cellToolTip', field: 'errMessage', displayName: 'getALString("Error\nMessage", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '80%', cellTemplate: "<div data-ng-class='col.colIndex()' > \
                    <input name='GrpExcelError_itm__errMessage' data-ng-model='row.entity.errMessage' data-np-ui-model='row.entity._errMessage' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpExcelError(): ModelGrpExcelError {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            var self = this;
            return self.$scope.modelGrpExcelFile.ExcelErrorEntityName;
        }


        public getEntitiesFromJSON(webResponse: any, parent?: IExcelFile): Array<NpTypes.IBaseEntity> {
            var self = this;
            var entlist: Array<IExcelError> = <IExcelError[]>fromJSONComplete(webResponse.data);

            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.exfiId = this.Parent;
                } else {
                    x.exfiId = parent;
                }
            });

            return entlist;
        }

        public get Current(): IExcelError {
            var self = this;
            return <IExcelError>self.$scope.modelGrpExcelError.selectedEntities[0];
        }

        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string> = []): string {
            var self = this;
            var paramData = {};

            var model = self.model;
            if (model.sortField !== undefined) {
                paramData['sortField'] = model.sortField;
                paramData['sortOrder'] = model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.id)) {
                paramData['exfiId_id'] = self.Parent.id;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_row)) {
                paramData['fsch_row'] = self.modelGrpExcelError.fsch_row;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_errMessage)) {
                paramData['fsch_errMessage'] = self.modelGrpExcelError.fsch_errMessage;
            }
            var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
            return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }

        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string> = []): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = self.getEntityName() + "/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError";
            var url = "/" + self.AppName + "/rest/" + wsPath + "?";
            var paramData = {};


            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;

            paramData['exc_Id'] = excludedIds;
            var model = self.model;


            if (model.sortField !== undefined) {
                paramData['sortField'] = model.sortField;
                paramData['sortOrder'] = model.sortOrder == 'asc';
            }

            if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.id)) {
                paramData['exfiId_id'] = self.Parent.id;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_row)) {
                paramData['fsch_row'] = self.modelGrpExcelError.fsch_row;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_errMessage)) {
                paramData['fsch_errMessage'] = self.modelGrpExcelError.fsch_errMessage;
            }

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

            var promise = self.$http.post(
                url,
                paramData,
                { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });

        }
        public makeWebRequest_count(excludedIds: Array<string> = []): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = self.getEntityName() + "/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_count";
            var url = "/" + self.AppName + "/rest/" + wsPath + "?";
            var paramData = {};

            paramData['exc_Id'] = excludedIds;
            var model = self.model;

            if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.id)) {
                paramData['exfiId_id'] = self.Parent.id;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_row)) {
                paramData['fsch_row'] = self.modelGrpExcelError.fsch_row;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_errMessage)) {
                paramData['fsch_errMessage'] = self.modelGrpExcelError.fsch_errMessage;
            }

            var promise = self.$http.post(
                url,
                paramData,
                { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });

        }




        private getMergedItems_cache: { [parentId: string]: Tuple2<boolean, IExcelError[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: IExcelError[]) => void> = [];


        public getMergedItems(func: (items: IExcelError[]) => void, bContinuousCaller: boolean = true, parent?: IExcelFile, bForceUpdate: boolean = false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: IExcelError[]): void {
                var mergedEntities = <IExcelError[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<IExcelError>e.a).exfiId)).
                        map(e => <IExcelError>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest: boolean = bForceUpdate || self.getMergedItems_cache[parent.id] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.id] !== undefined &&
                        self.getMergedItems_cache[parent.id].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.id] = new Tuple2(true, undefined);

                    var wsPath = self.getEntityName() + "/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError";
                    var url = "/" + self.AppName + "/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['exfiId_id'] = parent.id;



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <IExcelError[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.id].a = false;
                            self.getMergedItems_cache[parent.id].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.id].a = false;
                            self.getMergedItems_cache[parent.id].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, data);
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.id].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.id].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }

        public expToExcelBtnAction_fileName() {
            var self = this;
            var excelFileName = self.$scope.modelGrpExcelFile.filename;
            var parts = excelFileName.split('.');
            parts.pop();
            var excelFileNameWithoutExt = parts.join('.');
            return excelFileNameWithoutExt + "_ImportErrors_" + Utils.convertDateToString(new Date(), "yyyy_MM_dd_HHmm") + ".xls";
        }
        public exportToExcelBtnAction() {
            var self = this;
            var wsPath = self.getEntityName() + "/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_toExcel";
            var url = "/" + self.AppName + "/rest/" + wsPath + "?";
            var paramData = {};
            paramData['__fields'] = self.getExcelFields();
            if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.id)) {
                paramData['exfiId_id'] = self.Parent.id;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_row)) {
                paramData['fsch_row'] = self.modelGrpExcelError.fsch_row;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_errMessage)) {
                paramData['fsch_errMessage'] = self.modelGrpExcelError.fsch_errMessage;
            }

            NpTypes.AlertMessage.clearAlerts(self.PageModel);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                    var data = base64DecToArr(response.data);
                    var blob = new Blob([data], { type: "application/octet-stream" });
                    saveAs(blob, self.expToExcelBtnAction_fileName());
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.PageModel, data);
                });

        }

        public belongsToCurrentParent(x: IExcelError): boolean {
            if (this.Parent === undefined || x.exfiId === undefined)
                return false;
            return x.exfiId.id === this.Parent.id;

        }

        public onParentChange(): void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew())
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent(): IExcelFile {
            var self = this;
            return self.exfiId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpExcelFile.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.id.indexOf('TEMP_ID') === 0);
        }


        public get exfiId(): IExcelFile {
            var self = this;
            return <IExcelFile>self.$scope.modelGrpExcelFile.selectedEntities[0];
        }


        _firstLevelChildGroups: Array<AbstractGroupController> = undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }


    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
