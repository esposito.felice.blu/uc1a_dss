/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    var GeometryRow = (function () {
        function GeometryRow(area, areaValue, origIdx) {
            this.area = area;
            this.areaValue = areaValue;
            this.origIdx = origIdx;
        }
        return GeometryRow;
    })();
    Controllers.GeometryRow = GeometryRow;
    var ChooseGeomModel = (function () {
        function ChooseGeomModel() {
            this.rows = [];
            this.dataSetIdx = -1;
        }
        return ChooseGeomModel;
    })();
    Controllers.ChooseGeomModel = ChooseGeomModel;
    var controllerChooseGeom = (function () {
        function controllerChooseGeom($scope, Plato, $timeout) {
            this.$scope = $scope;
            this.Plato = Plato;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("ChooseGeomDialog");
            var npMapThis = dialogOptions.npMapThis;
            var polygonsRemaining = dialogOptions.polygonsRemaining;
            var self = this;
            $scope.model = new ChooseGeomModel();
            polygonsRemaining.forEach(function (x, origIdx) {
                if (x.length > 0 && x[0].length > 0) {
                    var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:2100');
                    var transGeom = NpGeoGlobals.coordinates_to_OL3feature(x).getGeometry().clone().transform(ol.proj.get('EPSG:3857'), ol.proj.get(coordinateSystem));
                    var area = transGeom.getArea();
                    var output;
                    //output = (Math.round(area / 1000000 * 100) / 100) + '&nbsp;' + 'km<sup>2</sup>';
                    //@p.tsagkis venizelou & nantin asked to get all areas as HA and not m2. 
                    // Stupid???? Maybe...... it is not my desicion anyway.
                    if (area > 10000) {
                        output = (Math.round(area / 1000000 * 100) / 100) + '&nbsp;' + 'km<sup>2</sup>';
                    }
                    else {
                        output = (Math.round(area * 100) / 100) + '&nbsp;' + 'm<sup>2</sup>';
                    }
                    $scope.model.rows.push(new GeometryRow(output, area, origIdx));
                }
            });
            $scope.model.rows.sort(function (a, b) {
                return b.areaValue - a.areaValue;
            });
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
            function commonWork(f, idx) {
                var croppedFeature = NpGeoGlobals.coordinates_to_OL3feature(polygonsRemaining[idx]);
                var g = croppedFeature.getGeometry();
                var newFeature = new ol.Feature();
                newFeature.setGeometryName("");
                newFeature.setGeometry(g);
                f.clear();
                f.push(newFeature);
                npMapThis.enterDrawMode();
            }
            $scope.preview = function (idx) {
                npMapThis.featureOverlayCrop.getSource().clear();
                var features = npMapThis.featureOverlayCrop.getSource().getFeaturesCollection();
                if (features === null) {
                    features = new ol.Collection();
                }
                commonWork(features, idx);
                console.log("polygonsRemaining[idx]========", polygonsRemaining[idx]);
                var directionsClockwise = polygonsRemaining[idx].map(function (x) { return NpGeoGlobals.isClockWise(x); }).filter(function (x) { return x === false; });
                console.log("directionsClockwise", directionsClockwise);
                if (directionsClockwise.length > 1) {
                    messageBox($scope, Plato, "Μη επιτρεπτή γεωμετρία", "Τα πολύγωνα δεν μπορούν να είναι σπασμένα σε κομμάτια.", IconKind.ERROR, [
                        new Tuple2("OK", function () { })
                    ], 0, 0, '19em');
                    $scope.model.dataSetIdx = -1;
                }
                else
                    $scope.model.dataSetIdx = idx;
            };
            $scope.select = function () {
                npMapThis.featureOverlayCrop.getSource().clear();
                commonWork(npMapThis.featureOverlayDraw.getSource().getFeaturesCollection(), $scope.model.dataSetIdx);
                dialogOptions.jquiDialog.dialog("close");
            };
        }
        return controllerChooseGeom;
    })();
    Controllers.controllerChooseGeom = controllerChooseGeom;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerChooseGeom.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
