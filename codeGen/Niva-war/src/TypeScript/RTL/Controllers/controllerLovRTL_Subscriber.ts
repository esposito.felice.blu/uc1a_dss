/// <reference path="../services.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../base64Utils.ts" />
/// <reference path="../FileSaver.ts" />
/// <reference path="../Controllers/BaseController.ts" />
/// <reference path="../Entities/RTL_Subscriber.ts" />

module Controllers {
    export class ModelLovRTL_Subscriber extends AbstractLovModel {
        _srch_subs_short_name:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get srch_subs_short_name():string {
            return this._srch_subs_short_name.value;
        }
        public set srch_subs_short_name(vl:string) {
            this._srch_subs_short_name.value = vl;
        }
        _srch_subs_vat:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get srch_subs_vat():string {
            return this._srch_subs_vat.value;
        }
        public set srch_subs_vat(vl:string) {
            this._srch_subs_vat.value = vl;
        }
        _srch_subs_legal_name:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get srch_subs_legal_name():string {
            return this._srch_subs_legal_name.value;
        }
        public set srch_subs_legal_name(vl:string) {
            this._srch_subs_legal_name.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }

        controller: ControllerLovRTL_Subscriber;
        constructor(public $scope: IScopeLovRTL_Subscriber) { super($scope); }
    }


    export interface IScopeLovRTL_Subscriber extends IAbstractLovScope {
        globals: NpTypes.RTLGlobals;
        modelLovRTL_Subscriber : ModelLovRTL_Subscriber;
        _disabled():boolean; 
        _invisible():boolean; 
        LovSubscriber_srch_subs_short_name_disabled():boolean; 
        LovSubscriber_srch_subs_vat_disabled():boolean; 
        LovSubscriber_srch_subs_legal_name_disabled():boolean; 
    }

    export class ControllerLovRTL_SubscriberBase extends LovController {
        constructor(
            public $scope: IScopeLovRTL_Subscriber,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker,
            public model: ModelLovRTL_Subscriber)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 2})
            var self: ControllerLovRTL_Subscriber = this;
            model.controller = <ControllerLovRTL_Subscriber>self;
            $scope.clearBtnAction = () => { 
                model.srch_subs_short_name = undefined;
                model.srch_subs_vat = undefined;
                model.srch_subs_legal_name = undefined;
                self.updateGrid();
            };
            $scope.LovSubscriber_srch_subs_short_name_disabled = 
                () => {
                    return self.LovSubscriber_srch_subs_short_name_disabled();
                };
            $scope.LovSubscriber_srch_subs_vat_disabled = 
                () => {
                    return self.LovSubscriber_srch_subs_vat_disabled();
                };
            $scope.LovSubscriber_srch_subs_legal_name_disabled = 
                () => {
                    return self.LovSubscriber_srch_subs_legal_name_disabled();
                };
            $scope.modelLovRTL_Subscriber = model;
            self.updateUI();
        }

        public dynamicMessage(sMsg: string): string {
            return this.$scope.globals.getDynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerLovRTL_Subscriber";
        }
        public get HtmlDivId(): string {
            return "Subscriber_Id";
        }
        
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions =   <NpTypes.LovDialogOptions>self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName); 
            }
            var ret = [
                { cellClass:'cellToolTip', field:'shortName', displayName:'"Κωδικός"', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'20%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.shortName' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'vat', displayName:'"Α.Φ.Μ.&#160;<br/>Εταιρείας"', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.vat' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'legalName', displayName:'"Επωνυμία"', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'33%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.legalName' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter((cl => cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true));
            if (self.isMultiSelect()) {
                ret.splice(0,0,{ width:'22', cellTemplate:"<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined});
            }
            return ret;
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            return Entities.RTL_Subscriber.fromJSONComplete(webResponse.data);
        }

        public get modelLovRTL_Subscriber():ModelLovRTL_Subscriber {
            var self = this;
            return self.model;
        }

        public initializeModelWithPreviousValues() {
            var self = this;
            var prevModel:ModelLovRTL_Subscriber = <ModelLovRTL_Subscriber>self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovRTL_Subscriber.srch_subs_short_name = prevModel.srch_subs_short_name
                self.modelLovRTL_Subscriber.srch_subs_vat = prevModel.srch_subs_vat
                self.modelLovRTL_Subscriber.srch_subs_legal_name = prevModel.srch_subs_legal_name
            }
        }

        public LovSubscriber_srch_subs_short_name_disabled():boolean {
            var self = this;


            return false;
        }
        public LovSubscriber_srch_subs_vat_disabled():boolean {
            var self = this;


            return false;
        }
        public LovSubscriber_srch_subs_legal_name_disabled():boolean {
            var self = this;


            return false;
        }
        public LovSubscriber_srch_subs_sucaId_disabled():boolean {
            var self = this;


            return false;
        }
    }

    export class ControllerLovRTL_Subscriber extends ControllerLovRTL_SubscriberBase {
        constructor(
            public $scope: IScopeLovRTL_Subscriber,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelLovRTL_Subscriber($scope))
        }
    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
