﻿/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
/**
 * p.tsagkis 6/2016
 * display a window containing legend data
 * of the map
 */
module Controllers {
    export class LegendRow {
        constructor(
            public title: string,
            public color: Array<any>)
        {}
    }

    export class LegendModel {
        rows: Array<LegendRow> = [];
    }

    export interface LegendScope extends NpTypes.IApplicationScope {
        model: LegendModel;
        nevermind: () => void;
    }

    

    export class controllerLegend {
        
        constructor(public $scope: LegendScope, $timeout: ng.ITimeoutService) {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("LegendDialog");
            

            var npMapThis: NpGeo.NpMap = (<any>dialogOptions).npMapThis;
            var self = this;
            var legendData: any = (<any>dialogOptions).legendData;
            //console.log("legendData3", legendData);

            var controllerEntries = legendData[0].entries;
            var legendHtmlTbl = "<table>";
            for (var i = 0; i < controllerEntries.length; i++) {
                legendHtmlTbl = legendHtmlTbl + "<tr><td colspan='2'><b>" + controllerEntries[i].title + "</b></td></tr>";
                var childEntries = controllerEntries[i].childs;
                for (var f = 0; f < childEntries.length; f++) {
                    //console.log("childEntries[f].color=====", NpGeoGlobals.rgba2hexAndOpacity(childEntries[f].color));
                    legendHtmlTbl = legendHtmlTbl +
                        "<tr>" +
                        "<td>" + childEntries[f].title + "</td>" +
                        //"<td style=\"background-color:" + NpGeoGlobals.rgba2hexAndOpacity(childEntries[f].color)[0].hex + "; border: 1px solid #000000; ; text-align:center;  height: 50px; width: 50px;\"></td>" +
                        "<td style=\"background-color:" + childEntries[f].color + "; border: 10px solid #eee; text-align:center;  height: 40px; width: 40px;\"></td>" +
                        "</tr>"; 
                }

            }
            legendHtmlTbl = legendHtmlTbl + "</table>";
           
            document.getElementById("legenddivid").innerHTML = legendHtmlTbl;



            $scope.model = new LegendModel();

            $scope.model.rows = legendData[0].entries;
        
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            }
            
        }
       
       
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
