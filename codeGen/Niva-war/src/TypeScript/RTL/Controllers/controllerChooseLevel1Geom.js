/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    var Lvl1GeometryRow = (function () {
        function Lvl1GeometryRow(area, areaValue, ilot_no, valid, cover_id, gtype, origIdx) {
            this.area = area;
            this.areaValue = areaValue;
            this.ilot_no = ilot_no;
            this.valid = valid;
            this.cover_id = cover_id;
            this.gtype = gtype;
            this.origIdx = origIdx;
        }
        return Lvl1GeometryRow;
    })();
    Controllers.Lvl1GeometryRow = Lvl1GeometryRow;
    var ChooseLvl1GeomModel = (function () {
        function ChooseLvl1GeomModel() {
            this.rows = [];
            this.dataSetIdx = -1;
        }
        return ChooseLvl1GeomModel;
    })();
    Controllers.ChooseLvl1GeomModel = ChooseLvl1GeomModel;
    var controllerChooseLevel1Geom = (function () {
        function controllerChooseLevel1Geom($scope, $timeout, Plato) {
            this.$scope = $scope;
            this.Plato = Plato;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("ChooseLevel1GeomDialog");
            var npMapThis = dialogOptions.npMapThis;
            var polygonsRemaining = dialogOptions.polygonsRemaining;
            var labelForGeoms = [];
            var coverIds = [];
            var gtype = [];
            var valid = [];
            var imgPathDown = "/" + $scope.globals.appName + "/img/arrow_show.png";
            var imgPathUp = "/" + $scope.globals.appName + "/img/arrow_hide.png";
            var self = this;
            $scope.model = new ChooseLvl1GeomModel();
            polygonsRemaining.forEach(function (x, origIdx) {
                labelForGeoms.push(x.entity.data['ilot_no']);
                coverIds.push(x.entity.data['cover_id']);
                gtype.push(x.entity.data['gtype']);
                valid.push(x.entity.data['valid']);
                var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:2100');
                var transGeom = x.getGeometry().clone().transform(ol.proj.get('EPSG:3857'), ol.proj.get(coordinateSystem));
                var area = transGeom.getArea();
                var output;
                var textColor = 'black';
                //if invalid or not polygon , mark the text as red
                if (valid[origIdx] !== 'TRUE' || gtype[origIdx] !== 2003) {
                    textColor = 'red';
                }
                if (area > 10000) {
                    output = (Math.round(area / 1000000 * 100) / 100) + '&nbsp;' + 'km<sup>2</sup>' +
                        '<img align="right" width= "16" height= "16" src= "' + imgPathDown + '"  id= "btninfodiv_' + origIdx + '" >' +
                        '<p style="display: none;color:' + textColor + '" id="infodiv_' + origIdx + '">' +
                        ' (<b>ilot_no:</b>' + labelForGeoms[origIdx] + ',<br/><b>valid:</b>' + valid[origIdx] +
                        ',<b>cover_id:</b>' + coverIds[origIdx] + ',<br/><b>gtype:</b>' + gtype[origIdx] + ')' +
                        '</p>';
                }
                else {
                    output = (Math.round(area * 100) / 100) + '&nbsp;' + 'm<sup>2</sup>' +
                        '<img align="right" width= "16" height= "16" src= "' + imgPathDown + '"  id= "btninfodiv_' + origIdx + '" >' +
                        '<p style="display: none;color:' + textColor + '" id="infodiv_' + origIdx + '">' +
                        ' (<b>ilot_no:</b>' + labelForGeoms[origIdx] + ',<br/><b>valid:</b>' + valid[origIdx] +
                        ',<b>cover_id:</b>' + coverIds[origIdx] + ',<br/><b>gtype:</b>' + gtype[origIdx] + ')' +
                        '</p>';
                }
                $scope.model.rows.push(new Lvl1GeometryRow(output, area, labelForGeoms, valid, coverIds, gtype, origIdx));
            });
            $scope.model.rows.sort(function (a, b) {
                return b.areaValue - a.areaValue;
            });
            $timeout(function () {
                polygonsRemaining.forEach(function (x, origIdx) {
                    //toggle the info paragraph
                    $('#btninfodiv_' + origIdx).click(function () {
                        $('#infodiv_' + origIdx).toggle("slow", function () {
                            var divId = "infodiv_" + origIdx;
                            var btnDivId = "btninfodiv_" + origIdx;
                            //toggle the img button
                            if ($('#' + divId).is(":hidden")) {
                                $('#' + btnDivId).attr("src", imgPathDown);
                            }
                            if ($('#' + divId).is(":visible")) {
                                $('#' + btnDivId).attr("src", imgPathUp);
                            }
                        });
                    });
                });
            });
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
            function commonWork(f, idx) {
                //var croppedFeature = NpGeoGlobals.coordinates_to_OL3feature(polygonsRemaining[idx]);
                var croppedFeature = polygonsRemaining[idx];
                var g = croppedFeature.getGeometry();
                var newFeature = new ol.Feature();
                newFeature.setGeometryName("");
                newFeature.setGeometry(g);
                f.clear();
                f.push(newFeature);
                npMapThis.enterDrawMode();
            }
            $scope.preview = function (idx) {
                npMapThis.featureOverlayCrop.getSource().clear();
                var features = npMapThis.featureOverlayCrop.getSource().getFeaturesCollection();
                if (features === null) {
                    features = new ol.Collection();
                }
                commonWork(features, idx);
                //console.log("polygonsRemaining[idx]========",polygonsRemaining[idx]);
                //var directionsClockwise = polygonsRemaining[idx].map(x => NpGeoGlobals.isClockWise(x)).filter(x => x === false);
                //console.log("directionsClockwise", directionsClockwise);
                var isValid = isGeoJstsValid(polygonsRemaining[idx].getGeometry());
                //console.log("isValid", isValid);
                var geomType = polygonsRemaining[idx].getGeometry().getType();
                //console.log("geomType", geomType);
                if (isValid !== true || geomType !== 'Polygon') {
                    messageBox($scope, Plato, "Μη επιτρεπτή γεωμετρία", "Προκύπτει μη αποδεκτή γεωμετρία. (τύπος:" + geomType + ", εγκυρότητα:" + isValid + ")", IconKind.ERROR, [
                        new Tuple2("OK", function () { })
                    ], 0, 0, '19em');
                    $scope.model.dataSetIdx = -1;
                }
                else
                    $scope.model.dataSetIdx = idx;
            };
            $scope.select = function () {
                npMapThis.featureOverlayCrop.getSource().clear();
                commonWork(npMapThis.featureOverlayDraw.getSource().getFeaturesCollection(), $scope.model.dataSetIdx);
                dialogOptions.jquiDialog.dialog("close");
            };
        }
        return controllerChooseLevel1Geom;
    })();
    Controllers.controllerChooseLevel1Geom = controllerChooseLevel1Geom;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerChooseLevel1Geom.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
