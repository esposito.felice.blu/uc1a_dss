/// <reference path="../npTypes.ts" />
/// <reference path="controllerLogin.ts" />
var Controllers;
(function (Controllers) {
    var ModelChangePass = (function () {
        function ModelChangePass() {
            this.message = "";
            this.publicUserName = "";
            this.oldPassword = "";
            this.newPassword1 = "";
            this.newPassword2 = "";
        }
        return ModelChangePass;
    })();
    Controllers.ModelChangePass = ModelChangePass;
    var controllerChangePass = (function () {
        function controllerChangePass($scope, $http, $timeout, Plato, LoginService) {
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.LoginService = LoginService;
            var scope = $scope;
            var dialogOptions = scope.globals.findAndRemoveDialogOptionByClassName("ChangePassClass");
            scope.modelChangePass = new ModelChangePass();
            scope.closeChangePass = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
            scope.setNewPassword = function () {
                scope.modelChangePass.message = "";
                LoginService.setNewPassword(scope.modelChangePass.publicUserName, scope.modelChangePass.oldPassword, scope.modelChangePass.newPassword1, scope.modelChangePass.newPassword2, function () {
                    scope.modelChangePass.message = scope.globals.getDynamicMessage("ChangePasswdDialog_SuccessMsg");
                    $timeout(function () {
                        scope.modelChangePass.message = "";
                        dialogOptions.jquiDialog.dialog("close");
                    }, 3000);
                }, function (errMsg) {
                    scope.modelChangePass.message = "<strong>" + scope.getALString('ChangePasswdDialog_ErrorLabel') + ":</strong>&nbsp;" + errMsg;
                    $timeout(function () {
                        scope.modelChangePass.message = "";
                    }, 14000);
                });
            };
        }
        return controllerChangePass;
    })();
    Controllers.controllerChangePass = controllerChangePass;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerChangePass.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
