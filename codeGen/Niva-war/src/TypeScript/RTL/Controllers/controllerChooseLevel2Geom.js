/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    var Lvl2GeometryRow = (function () {
        function Lvl2GeometryRow(area, areaValue, label, valid, intGeoms, gtype, origIdx) {
            this.area = area;
            this.areaValue = areaValue;
            this.label = label;
            this.valid = valid;
            this.intGeoms = intGeoms;
            this.gtype = gtype;
            this.origIdx = origIdx;
        }
        return Lvl2GeometryRow;
    })();
    Controllers.Lvl2GeometryRow = Lvl2GeometryRow;
    var ChooseLvl2GeomModel = (function () {
        function ChooseLvl2GeomModel() {
            this.rows = [];
            this.dataSetIdx = -1;
        }
        return ChooseLvl2GeomModel;
    })();
    Controllers.ChooseLvl2GeomModel = ChooseLvl2GeomModel;
    var controllerChooseLevel2Geom = (function () {
        function controllerChooseLevel2Geom($scope, $timeout, Plato) {
            this.$scope = $scope;
            this.Plato = Plato;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("ChooseLevel2GeomDialog");
            var npMapThis = dialogOptions.npMapThis;
            var polygonsRemaining = dialogOptions.polygonsRemaining;
            var labelForGeoms = [];
            var intGeoms = [];
            var gtype = [];
            var valid = [];
            var imgPathDown = "/" + $scope.globals.appName + "/img/arrow_show.png";
            var imgPathUp = "/" + $scope.globals.appName + "/img/arrow_hide.png";
            var self = this;
            $scope.model = new ChooseLvl2GeomModel();
            polygonsRemaining.forEach(function (x, origIdx) {
                labelForGeoms.push(x.entity.data['shortdesc']);
                intGeoms.push(x.entity.data['intgeom']);
                gtype.push(x.entity.data['gtype']);
                valid.push(x.entity.data['valid']);
                var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:2100');
                var transGeom = x.getGeometry().clone().transform(ol.proj.get('EPSG:3857'), ol.proj.get(coordinateSystem));
                var area = transGeom.getArea();
                var output;
                var textColor = 'black';
                //if invalid or not polygon , mark the text as red
                if (valid[origIdx] !== 'TRUE' || gtype[origIdx] !== 2003) {
                    textColor = 'red';
                }
                if (area > 10000) {
                    output = (Math.round(area / 1000000 * 100) / 100) + '&nbsp;' + 'km<sup>2</sup> (' + x.entity.data['shortdesc'] + ' )'; //+
                }
                else {
                    output = (Math.round(area * 100) / 100) + '&nbsp;' + 'm<sup>2</sup> (' + x.entity.data['shortdesc'] + ' )'; //+
                }
                $scope.model.rows.push(new Lvl2GeometryRow(output, area, labelForGeoms, intGeoms, valid, gtype, origIdx));
            });
            $scope.model.rows.sort(function (a, b) {
                return b.areaValue - a.areaValue;
            });
            //$timeout(function () {
            //    polygonsRemaining.forEach((x, origIdx) => {
            //        //toggle the info paragraph
            //        $('#btninfodiv_' + origIdx).click(function () {
            //            $('#infodiv_' + origIdx).toggle("slow", function () {
            //                var divId = "infodiv_" + origIdx;
            //                var btnDivId = "btninfodiv_" + origIdx;
            //               //toggle the img button
            //                if ($('#' + divId).is(":hidden")) {
            //                    $('#' + btnDivId).attr("src", imgPathDown);
            //                }
            //                if ($('#' + divId).is(":visible")) {
            //                    $('#' + btnDivId).attr("src", imgPathUp);
            //                }
            //            });
            //        });
            //    })
            // });
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
            function commonWork(f, idx) {
                //var croppedFeature = NpGeoGlobals.coordinates_to_OL3feature(polygonsRemaining[idx]);
                var croppedFeature = polygonsRemaining[idx];
                var g = croppedFeature.getGeometry();
                var newFeature = new ol.Feature();
                newFeature.setGeometryName("");
                newFeature.setGeometry(g);
                f.clear();
                f.push(newFeature);
                npMapThis.enterDrawMode();
            }
            $scope.previewInt = function (idx) {
                npMapThis.featureOverlayCrop.getSource().clear();
                //need to loop through the checkboxes and find those checked
                polygonsRemaining.forEach(function (x, origIdx) {
                    var isChecked = $('#lvl2CheckGeom_' + origIdx).is(':checked');
                    if (isChecked === true) {
                        var geom = new ol.geom[intGeoms[origIdx].type](intGeoms[origIdx].coordinates);
                        var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:3857');
                        geom.transform(ol.proj.get(coordinateSystem), ol.proj.get('EPSG:3857'));
                        var newFeature = new ol.Feature();
                        newFeature.setGeometryName("");
                        newFeature.setGeometry(geom);
                        npMapThis.featureOverlayCrop.getSource().addFeature(newFeature);
                        var isValid = isGeoJstsValid(polygonsRemaining[origIdx].getGeometry());
                        var geomType = polygonsRemaining[origIdx].getGeometry().getType();
                        if (isValid !== true || geomType !== 'Polygon') {
                            messageBox($scope, Plato, "Μη επιτρεπτή γεωμετρία", "Προκύπτει μη αποδεκτή γεωμετρία. (τύπος:" + geomType + ", εγκυρότητα:" + isValid + ")", IconKind.ERROR, [
                                new Tuple2("OK", function () { })
                            ], 0, 0, '19em');
                            $('#lvl2Geom_' + origIdx).prop('disabled', true);
                        }
                        else
                            $('#lvl2Geom_' + origIdx).prop('disabled', false);
                    }
                    else {
                        $('#lvl2Geom_' + origIdx).prop('disabled', true);
                    }
                });
            };
            $scope.select = function (id) {
                npMapThis.featureOverlayCrop.getSource().clear();
                commonWork(npMapThis.featureOverlayDraw.getSource().getFeaturesCollection(), id);
                dialogOptions.jquiDialog.dialog("close");
                npMapThis.intersectOverlapsCustom();
            };
        }
        return controllerChooseLevel2Geom;
    })();
    Controllers.controllerChooseLevel2Geom = controllerChooseLevel2Geom;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerChooseLevel2Geom.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
