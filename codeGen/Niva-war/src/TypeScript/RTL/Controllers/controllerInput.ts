/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />

module Controllers {

    export class InputModel {
        allOK: boolean = true;
    }

    export interface IInputScope extends NpTypes.IApplicationScope {
        model: InputModel;
        triggerCancel():void;
    }

    export class controllerInput {

        constructor(public $scope: IInputScope) {
            var dialogOptions: InputDialogOptions =
                <InputDialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("GenericInputDialog");

            var self = this;
            $scope.model = new InputModel();

            var variables = dialogOptions.variables;
            variables.forEach((v:InputDialogVariable) => {
                var uiVarName = "_" + v.varName;
                if (v instanceof InputDialogText) {
                    $scope.model[uiVarName] = new NpTypes.UIStringModel((<InputDialogText>v).initial);
                } else if (v instanceof InputDialogNumber) {
                    $scope.model[uiVarName] = new NpTypes.UINumberModel((<InputDialogNumber>v).initial);
                } else if (v instanceof InputDialogDate) {
                    $scope.model[uiVarName] = new NpTypes.UIDateModel((<InputDialogDate>v).initial);
                }
                Object.defineProperty($scope.model, v.varName, {
                    get: () => {
                        return $scope.model[uiVarName]._value;
                    },
                    set: (newValue) => {
                        $scope.model[uiVarName]._value = newValue;
                    },
                    enumerable: true,
                    configurable: true
                });
                if (isVoid(v.validateFunc)) 
                    $scope['validate_' + v.varName] = () => { return new NpTypes.ValidationResult(true, ""); };
                else
                    $scope['validate_' + v.varName] = v.validateFunc;
            });

            $scope.$watch(
                () => {
                    var allOK = true;
                    variables.forEach((v:InputDialogVariable) => {
                        if (!allOK)
                            return;
                        var uiVarName = "_" + v.varName;
                        allOK = allOK && $scope.model[uiVarName].isValid;
                    });
                    return allOK;
                },
                (newValue) => {
                    $scope.model.allOK = newValue;
                });

            $scope.triggerCancel = function() {
                variables.forEach((v:InputDialogVariable) => {
                    var uiVarName = "_" + v.varName;
                    $scope.model[uiVarName].clearAllErrors();
                });
                dialogOptions.jquiDialog.dialog("close");
                setTimeout(() => {
                    $('.popover').remove();
                }, 750);
                if (dialogOptions.indexCancel !== undefined)
                    dialogOptions.buttons[dialogOptions.indexCancel].b($scope.model);
            }

            var buttons = dialogOptions.buttons;

            var count = 0;
            buttons.forEach((v:Tuple2<string, (model:any)=>void>) => {
                var suffix = Sprintf.sprintf("_%d_%d", dialogOptions.countDialogs, count);
                $scope[<string>(Sprintf.sprintf("click_%s", suffix))] = () => {
                    dialogOptions.jquiDialog.dialog("close");
                    v.b($scope.model);
                };
                count += 1;
            });

            if (!isVoid(dialogOptions.indexSelected)) {
                var suffix = Sprintf.sprintf("_%d_%d", dialogOptions.countDialogs, dialogOptions.indexSelected);
                $('#autoBtn_' + suffix).focus();
            }
        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
