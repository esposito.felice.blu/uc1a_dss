/// <reference path="../npTypes.ts" />

module Controllers {

    export class ModelRequestAuthorization {
        userNameLabel: string;
        userName: string;
        passwordLabel: string;
        password: string;
    }

    export interface IRequestAuthorizationScope extends NpTypes.IApplicationScope {
        requestAuthorization():void;
        closeRequestAuthorization():void;
        modelRequestAuthorization: ModelRequestAuthorization;
        useCustomMethod(): boolean;
    }

    export class controllerRequestAuthorization {

        constructor(
            public $scope: IRequestAuthorizationScope,
            private $http: ng.IHttpService,
            private $timeout: ng.ITimeoutService,
            private Plato: Services.INpDialogMaker,
            private LoginService: Services.LoginService
        ) {
            var scope: IRequestAuthorizationScope = $scope;
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>scope.globals.findAndRemoveDialogOptionByClassName("RequestAuthorizationClass");

            scope.modelRequestAuthorization = new ModelRequestAuthorization();
            scope.modelRequestAuthorization.passwordLabel = scope.globals.requestAuthorizationOptions.getPasswordLabel(LoginService.routeAuthor.url);
            scope.modelRequestAuthorization.userNameLabel = scope.globals.requestAuthorizationOptions.getUserNameLabel(LoginService.routeAuthor.url);

            scope.useCustomMethod = function () {
                return LoginService.routeAuthor.useCustomMethod;
            }

            scope.closeRequestAuthorization = function () {
                LoginService.cancelRequestAuthorization();
                dialogOptions.jquiDialog.dialog("close");
            }
            scope.requestAuthorization = function () {
                //When "enter" pressed then input button does not loose focus and model is not updated, 
                // so we programmatically focus on submit button
                document.getElementById('submitButton').focus(); 

                if (isVoid(scope.modelRequestAuthorization.password) || scope.modelRequestAuthorization.password === "")
                    return;
                LoginService.requestAuthorization(scope.modelRequestAuthorization.password, scope.modelRequestAuthorization.userName, scope.globals.globalSubsCode);
                dialogOptions.jquiDialog.dialog("close");
            }

        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
