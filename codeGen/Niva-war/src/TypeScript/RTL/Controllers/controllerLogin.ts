﻿/// <reference path="../services.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../utils.ts" />
/// <reference path="controllerLovRTL_Subscriber.ts" />
/// <reference path="../Entities/RTL_Subscriber.ts" />

module Controllers {

    export class ModelLogin {
        email: string;
        password: string;
        message: string;
        _subscriber: NpTypes.UIManyToOneModel<Entities.RTL_Subscriber> = new NpTypes.UIManyToOneModel<Entities.RTL_Subscriber>(undefined);
        public get subscriber(): Entities.RTL_Subscriber {
            return this._subscriber.value;
        }
        public set subscriber(vl: Entities.RTL_Subscriber) {
            this._subscriber.value = vl;
        }
        loginSubsCodeCookiekey: string = 'allApps' + '-loginSubsCode';
    }

    export interface ILoginScope extends NpTypes.IApplicationScope {
        modelLogin: ModelLogin;
        checkCredentials(): void;
        isLoggedIn(): boolean;
        logout(): void;
        changePassword(): void;
        isLoginSubscriberInvisible(): boolean;
        showLov_subscriber(): void; 
        findByCode_Login_subscriber(dummyContext: any, newCodeValue: string, component: NpTypes.NpLookup): void;
    }

    export class controllerLogin {

        constructor(
            public $scope: ILoginScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            private Plato: Services.INpDialogMaker,
            private LoginService: Services.LoginService,
            private NavigationService: Services.NavigationService
        )
        {
            var self = this;
            var scope = $scope;

            scope.globals.mainAppTemplate.showAll(false);
            Utils.UpdateMainWindowWidthAndMainButtonPlacement(scope);

            scope.modelLogin = new ModelLogin();
            scope.modelLogin.email = "";
            scope.modelLogin.password = "";
            scope.modelLogin.message = "";
            scope.modelLogin.subscriber = null;

            scope.isLoggedIn = (): boolean => {
                return LoginService.isLoggedIn();
            }
            scope.logout = () => {
                LoginService.logout();
            }
            scope.changePassword = () => {
                LoginService.changePassword();
            }

            scope.checkCredentials = (): void => {
                // Unfortunately, only Firefox works properly with autocompletion.
                // Chrome / webkit don't autocomplete at all - and when sometimes they do,
                // they DONT trigger an update to the bound ng-models ....

                // (sigh)
                //
                // We therefore use jQuery to read these two:
                scope.modelLogin.email = $('#emailForLogin').val();
                scope.modelLogin.password = $('#passwordForLogin').val();

                var loginSubsCode: string = null;
                if (!self.isLoginSubscriberInvisible()) {
                    loginSubsCode = isVoid(scope.modelLogin.subscriber) ? null : scope.modelLogin.subscriber.shortName;

                    if (supports_html5_storage()) {
                        if (isVoid(loginSubsCode)) {
                            localStorage.removeItem(this.modelLogin.loginSubsCodeCookiekey);
                        } else {
                            localStorage[this.modelLogin.loginSubsCodeCookiekey] = loginSubsCode;
                        }
                    }
                }

                LoginService.login(scope.modelLogin.email, scope.modelLogin.password, loginSubsCode, () => {},
                    (errMsg) => {
                        scope.modelLogin.message = errMsg;
                        $timeout(function () {
                            scope.modelLogin.message = "";
                        }, 4000);
                });

            }

            scope.isLoginSubscriberInvisible = () => self.isLoginSubscriberInvisible();

            scope.showLov_subscriber =
                () => {
                    $timeout(() => {
                        self.showLov_subscriber();
                    }, 0);
                };
            scope.findByCode_Login_subscriber =
                (dummyContext: any, newCodeValue: string, component: NpTypes.NpLookup) => {
                    self.findByCode_Login_subscriber(newCodeValue, component);
                };

            if (!self.isLoginSubscriberInvisible()) {
                if (supports_html5_storage()) {
                    var storedLoginSubsCode: string = localStorage[this.modelLogin.loginSubsCodeCookiekey];
                    if (!isVoid(storedLoginSubsCode))
                        self.findByCode_Login_subscriber(storedLoginSubsCode, null);
                }
            }

        }
        public get modelLogin(): ModelLogin {
            return <ModelLogin>this.$scope.modelLogin;
        }

        public isLoginSubscriberInvisible(): boolean {
            var self = this;
            if (isVoid(self.$scope.globals))
                return false;
            return self.$scope.globals.isLoginSubscriberInvisible();
        }

        cachedLovModel_subscriber: ModelLovRTL_Subscriber;
        public showLov_subscriber() {
            var self = this;
            var uimodel: NpTypes.IUIModel = self.modelLogin._subscriber;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Φορείς';
            dialogOptions.previousModel = self.cachedLovModel_subscriber;
            dialogOptions.className = "ControllerLovRTL_Subscriber";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var subscriber1: Entities.RTL_Subscriber = <Entities.RTL_Subscriber>selectedEntity;
                uimodel.clearAllErrors();
                if (!isVoid(subscriber1) && subscriber1.isEqual(self.modelLogin.subscriber))
                    return;
                self.modelLogin.subscriber = subscriber1;
            };
            dialogOptions.openNewEntityDialog = () => {
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel: ModelLovRTL_Subscriber, excludedIds: Array<string> = []) => {
                self.cachedLovModel_subscriber = lovModel;
                var wsPath = "Login/findAllByCriteriaRange_Login_subscriber";
                var url = "/" + self.$scope.globals.appName +  "/rest/" + wsPath + "?";
                var paramData = {};


                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;

                paramData['exc_Id'] = excludedIds;
                var model = lovModel;


                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_short_name)) {
                    paramData['srch_subs_short_name'] = lovModel.srch_subs_short_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_vat)) {
                    paramData['srch_subs_vat'] = lovModel.srch_subs_vat;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_legal_name)) {
                    paramData['srch_subs_legal_name'] = lovModel.srch_subs_legal_name;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel: ModelLovRTL_Subscriber, excludedIds: Array<string> = []) => {
                var wsPath = "Login/findAllByCriteriaRange_Login_subscriber_count";
                var url = "/" + self.$scope.globals.appName + "/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = lovModel;

                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_short_name)) {
                    paramData['srch_subs_short_name'] = lovModel.srch_subs_short_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_vat)) {
                    paramData['srch_subs_vat'] = lovModel.srch_subs_vat;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_legal_name)) {
                    paramData['srch_subs_legal_name'] = lovModel.srch_subs_legal_name;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: ModelLovRTL_Subscriber, excludedIds: Array<string>): string => {
                var paramData = {};

                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_short_name)) {
                    paramData['srch_subs_short_name'] = lovModel.srch_subs_short_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_vat)) {
                    paramData['srch_subs_vat'] = lovModel.srch_subs_vat;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_legal_name)) {
                    paramData['srch_subs_legal_name'] = lovModel.srch_subs_legal_name;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return res;

            };


            dialogOptions.shownCols['shortName'] = true;
            dialogOptions.shownCols['legalName'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/" + self.$scope.globals.appName + '/partials/RTL_Subscriber.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }


        public findByCode_Login_subscriber(newCodeValue: string, component: NpTypes.NpLookup) {
            var self = this;
            var uimodel: NpTypes.IUIModel = self.modelLogin._subscriber;
            var setSubscriber = (subscriber1: Entities.RTL_Subscriber) => {
                if (!isVoid(subscriber1) && subscriber1.isEqual(self.modelLogin.subscriber)) {
                    return;
                }

                self.modelLogin.subscriber = subscriber1;

            }
            var onSuccess = (subscriber1: Entities.RTL_Subscriber) => {
                uimodel.clearAllErrors();

                setSubscriber(subscriber1);
                if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
            };

            var onError = (err: string) => {
                var err_msg: string
                if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                if (err === 'TooManyRows') {
                    err_msg = self.$scope.globals.getDynamicMessage("EntityByCode_TooManyRowsForCodeMsg(\"" + newCodeValue + "\")");

                } else {
                    err_msg = self.$scope.globals.getDynamicMessage("EntityByCode_NoRecordForCodeMsg(\"" + newCodeValue + "\")");
                }
                uimodel.addNewErrorMessage(err_msg, false);
                if (!isVoid(component)) {
                    if (!isVoid(self.modelLogin.subscriber)) {
                        component.element.val(self.modelLogin.subscriber.shortName);
                    } else {
                        component.element.val(null);
                    }
                }
                self.$timeout(() => {
                    uimodel.clearAllErrors();
                }, 3000);
            }

            if (isVoid(newCodeValue) || (newCodeValue === "")) {
                onSuccess(null);
            } else {
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout, true);
                Entities.RTL_Subscriber.findByCode_Login_subscriber(newCodeValue, self.$scope, self.$http, onSuccess, onError);
            }
        }
    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
