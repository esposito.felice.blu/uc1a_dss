/// <reference path="../services.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../base64Utils.ts" />
/// <reference path="../FileSaver.ts" />
/// <reference path="../Controllers/BaseController.ts" />
/// <reference path="../Entities/RTL_Subscriber.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovRTL_Subscriber = (function (_super) {
        __extends(ModelLovRTL_Subscriber, _super);
        function ModelLovRTL_Subscriber($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._srch_subs_short_name = new NpTypes.UIStringModel(undefined);
            this._srch_subs_vat = new NpTypes.UIStringModel(undefined);
            this._srch_subs_legal_name = new NpTypes.UIStringModel(undefined);
        }
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "srch_subs_short_name", {
            get: function () {
                return this._srch_subs_short_name.value;
            },
            set: function (vl) {
                this._srch_subs_short_name.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "srch_subs_vat", {
            get: function () {
                return this._srch_subs_vat.value;
            },
            set: function (vl) {
                this._srch_subs_vat.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "srch_subs_legal_name", {
            get: function () {
                return this._srch_subs_legal_name.value;
            },
            set: function (vl) {
                this._srch_subs_legal_name.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovRTL_Subscriber.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        return ModelLovRTL_Subscriber;
    })(Controllers.AbstractLovModel);
    Controllers.ModelLovRTL_Subscriber = ModelLovRTL_Subscriber;
    var ControllerLovRTL_SubscriberBase = (function (_super) {
        __extends(ControllerLovRTL_SubscriberBase, _super);
        function ControllerLovRTL_SubscriberBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 2 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            $scope.clearBtnAction = function () {
                model.srch_subs_short_name = undefined;
                model.srch_subs_vat = undefined;
                model.srch_subs_legal_name = undefined;
                self.updateGrid();
            };
            $scope.LovSubscriber_srch_subs_short_name_disabled =
                function () {
                    return self.LovSubscriber_srch_subs_short_name_disabled();
                };
            $scope.LovSubscriber_srch_subs_vat_disabled =
                function () {
                    return self.LovSubscriber_srch_subs_vat_disabled();
                };
            $scope.LovSubscriber_srch_subs_legal_name_disabled =
                function () {
                    return self.LovSubscriber_srch_subs_legal_name_disabled();
                };
            $scope.modelLovRTL_Subscriber = model;
            self.updateUI();
        }
        ControllerLovRTL_SubscriberBase.prototype.dynamicMessage = function (sMsg) {
            return this.$scope.globals.getDynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerLovRTL_SubscriberBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerLovRTL_Subscriber";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerLovRTL_SubscriberBase.prototype, "HtmlDivId", {
            get: function () {
                return "Subscriber_Id";
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovRTL_SubscriberBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName);
            }
            var ret = [
                { cellClass: 'cellToolTip', field: 'shortName', displayName: '"Κωδικός"', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '20%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.shortName' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'vat', displayName: '"Α.Φ.Μ.&#160;<br/>Εταιρείας"', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.vat' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'legalName', displayName: '"Επωνυμία"', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '33%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.legalName' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter((function (cl) { return cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true; }));
            if (self.isMultiSelect()) {
                ret.splice(0, 0, { width: '22', cellTemplate: "<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined });
            }
            return ret;
        };
        ControllerLovRTL_SubscriberBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.RTL_Subscriber.fromJSONComplete(webResponse.data);
        };
        Object.defineProperty(ControllerLovRTL_SubscriberBase.prototype, "modelLovRTL_Subscriber", {
            get: function () {
                var self = this;
                return self.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovRTL_SubscriberBase.prototype.initializeModelWithPreviousValues = function () {
            var self = this;
            var prevModel = self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovRTL_Subscriber.srch_subs_short_name = prevModel.srch_subs_short_name;
                self.modelLovRTL_Subscriber.srch_subs_vat = prevModel.srch_subs_vat;
                self.modelLovRTL_Subscriber.srch_subs_legal_name = prevModel.srch_subs_legal_name;
            }
        };
        ControllerLovRTL_SubscriberBase.prototype.LovSubscriber_srch_subs_short_name_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovRTL_SubscriberBase.prototype.LovSubscriber_srch_subs_vat_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovRTL_SubscriberBase.prototype.LovSubscriber_srch_subs_legal_name_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovRTL_SubscriberBase.prototype.LovSubscriber_srch_subs_sucaId_disabled = function () {
            var self = this;
            return false;
        };
        return ControllerLovRTL_SubscriberBase;
    })(Controllers.LovController);
    Controllers.ControllerLovRTL_SubscriberBase = ControllerLovRTL_SubscriberBase;
    var ControllerLovRTL_Subscriber = (function (_super) {
        __extends(ControllerLovRTL_Subscriber, _super);
        function ControllerLovRTL_Subscriber($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovRTL_Subscriber($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovRTL_Subscriber;
    })(ControllerLovRTL_SubscriberBase);
    Controllers.ControllerLovRTL_Subscriber = ControllerLovRTL_Subscriber;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerLovRTL_Subscriber.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
