/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
/**
 * p.tsagkis 4/2016
 * options related to the export format
 * of uor map. Possible values pdf,kml,jpeg,png,geotiff
 */
var Controllers;
(function (Controllers) {
    var MapExportModel = (function () {
        function MapExportModel() {
        }
        return MapExportModel;
    })();
    Controllers.MapExportModel = MapExportModel;
    var controllerExportMap = (function () {
        function controllerExportMap($scope, $timeout) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("MapExportDialog");
            var npMapThis = dialogOptions.npMapThis;
            var self = this;
            $scope.model = new MapExportModel();
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
            $scope.exportFunc = function () {
                var elm = document.getElementById("mapoutputformattype");
                var fileExt = "";
                var format = elm.value;
                if (format === 'kml') {
                    fileExt = format;
                }
                else {
                    fileExt = format.split("/")[1];
                }
                npMapThis.executeInlineWms(format, fileExt);
                dialogOptions.jquiDialog.dialog("close");
            };
        }
        return controllerExportMap;
    })();
    Controllers.controllerExportMap = controllerExportMap;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerExportMap.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
