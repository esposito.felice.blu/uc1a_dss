/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    var HoleModel = (function () {
        function HoleModel() {
            this._holes = new NpTypes.UINumberModel(1);
        }
        Object.defineProperty(HoleModel.prototype, "holes", {
            get: function () { return this._holes.value; },
            set: function (h) { this._holes.value = h; },
            enumerable: true,
            configurable: true
        });
        return HoleModel;
    })();
    Controllers.HoleModel = HoleModel;
    var controllerHole = (function () {
        function controllerHole($scope, $timeout) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("CreateHoleDialog");
            var self = this;
            $scope.model = new HoleModel();
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
            $scope.createPolygon = function () {
                if (!$scope.model._holes.isValid)
                    return;
                var npMapThis = dialogOptions.npMapThis;
                var width = npMapThis.map.getViewport().clientWidth;
                var wstep = width / 5;
                var height = npMapThis.map.getViewport().clientHeight;
                var hstep = height / 5;
                var coord = function (x, y) {
                    var coords = npMapThis.map.getCoordinateFromPixel([x, y]);
                    return Sprintf.sprintf("%10.2f %10.2f", coords[0], coords[1]);
                };
                var outerSquare = Sprintf.sprintf("POLYGON((%s, %s, %s, %s, %s)", coord(wstep, hstep), coord(wstep, 4. * hstep), coord(4. * wstep, 4. * hstep), coord(4. * wstep, hstep), coord(wstep, hstep));
                var divisor = $scope.model.holes * 2 + 1;
                var holeStep = 3 * wstep / divisor;
                var baseX = wstep + holeStep;
                var baseY = 2. * hstep;
                for (var i = 0; i < $scope.model.holes; i++) {
                    var lx1 = baseX + (i * 2 * holeStep);
                    var lx2 = baseX + (i * 2 * holeStep) + holeStep;
                    outerSquare += Sprintf.sprintf(", (%s, %s, %s, %s, %s)", coord(lx1, 2 * hstep), coord(lx2, 2 * hstep), coord(lx2, 3. * hstep), coord(lx1, 3. * hstep), coord(lx1, 2 * hstep));
                }
                outerSquare += ")";
                var format = new ol.format.WKT();
                var geometryTmp = format.readGeometry(outerSquare);
                var featureTmp = new ol.Feature();
                featureTmp.setGeometry(geometryTmp);
                $timeout(function () {
                    npMapThis.featureOverlayDraw.getSource().clear();
                    npMapThis.featureOverlayDraw.getSource().addFeature(featureTmp);
                    npMapThis.enterDrawMode();
                    npMapThis.entityHistories[npMapThis.currentEntityKey].push(featureTmp.clone());
                }, 250);
                dialogOptions.jquiDialog.dialog("close");
            };
        }
        return controllerHole;
    })();
    Controllers.controllerHole = controllerHole;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerHoles.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
