/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    var InputModel = (function () {
        function InputModel() {
            this.allOK = true;
        }
        return InputModel;
    })();
    Controllers.InputModel = InputModel;
    var controllerInput = (function () {
        function controllerInput($scope) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("GenericInputDialog");
            var self = this;
            $scope.model = new InputModel();
            var variables = dialogOptions.variables;
            variables.forEach(function (v) {
                var uiVarName = "_" + v.varName;
                if (v instanceof InputDialogText) {
                    $scope.model[uiVarName] = new NpTypes.UIStringModel(v.initial);
                }
                else if (v instanceof InputDialogNumber) {
                    $scope.model[uiVarName] = new NpTypes.UINumberModel(v.initial);
                }
                else if (v instanceof InputDialogDate) {
                    $scope.model[uiVarName] = new NpTypes.UIDateModel(v.initial);
                }
                Object.defineProperty($scope.model, v.varName, {
                    get: function () {
                        return $scope.model[uiVarName]._value;
                    },
                    set: function (newValue) {
                        $scope.model[uiVarName]._value = newValue;
                    },
                    enumerable: true,
                    configurable: true
                });
                if (isVoid(v.validateFunc))
                    $scope['validate_' + v.varName] = function () { return new NpTypes.ValidationResult(true, ""); };
                else
                    $scope['validate_' + v.varName] = v.validateFunc;
            });
            $scope.$watch(function () {
                var allOK = true;
                variables.forEach(function (v) {
                    if (!allOK)
                        return;
                    var uiVarName = "_" + v.varName;
                    allOK = allOK && $scope.model[uiVarName].isValid;
                });
                return allOK;
            }, function (newValue) {
                $scope.model.allOK = newValue;
            });
            $scope.triggerCancel = function () {
                variables.forEach(function (v) {
                    var uiVarName = "_" + v.varName;
                    $scope.model[uiVarName].clearAllErrors();
                });
                dialogOptions.jquiDialog.dialog("close");
                setTimeout(function () {
                    $('.popover').remove();
                }, 750);
                if (dialogOptions.indexCancel !== undefined)
                    dialogOptions.buttons[dialogOptions.indexCancel].b($scope.model);
            };
            var buttons = dialogOptions.buttons;
            var count = 0;
            buttons.forEach(function (v) {
                var suffix = Sprintf.sprintf("_%d_%d", dialogOptions.countDialogs, count);
                $scope[(Sprintf.sprintf("click_%s", suffix))] = function () {
                    dialogOptions.jquiDialog.dialog("close");
                    v.b($scope.model);
                };
                count += 1;
            });
            if (!isVoid(dialogOptions.indexSelected)) {
                var suffix = Sprintf.sprintf("_%d_%d", dialogOptions.countDialogs, dialogOptions.indexSelected);
                $('#autoBtn_' + suffix).focus();
            }
        }
        return controllerInput;
    })();
    Controllers.controllerInput = controllerInput;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerInput.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
