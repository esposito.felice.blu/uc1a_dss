/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    var LayerState = (function () {
        function LayerState(name, layerId, enabled, labelEnabled, vertexEnabled, selectable, lSnapEnable, orderIndex, opacity, sqlLayer) {
            this.name = name;
            this.layerId = layerId;
            this.enabled = enabled;
            this.labelEnabled = labelEnabled;
            this.vertexEnabled = vertexEnabled;
            this.selectable = selectable;
            this.lSnapEnable = lSnapEnable;
            this.orderIndex = orderIndex;
            this.opacity = opacity;
            this.sqlLayer = sqlLayer;
        }
        return LayerState;
    })();
    Controllers.LayerState = LayerState;
    var LayerModel = (function () {
        function LayerModel() {
            this.layers = [];
        }
        return LayerModel;
    })();
    Controllers.LayerModel = LayerModel;
    var controllerLayers = (function () {
        function controllerLayers($scope, $timeout) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("MapLayersDialog");
            var self = this;
            //@p.tsagkis
            $("#layersorter").sortable({
                start: function (event, ui) {
                    $(ui.item).animate({
                        'background-color': '#b6b5b5'
                    }, 400);
                },
                stop: function (event, ui) {
                    $(ui.item).animate({
                        'background-color': 'transparent'
                    }, 400);
                    var counter = 1;
                    //$($("#layersorter").children().get().reverse()).each(function () {
                    $('#layersorter').children().each(function () {
                        var npLyr = npMapThis.getNPLayerById(this.id);
                        npLyr.layer.setZIndex(counter);
                        npLyr.setOrderIndex(counter);
                        counter = counter + 1;
                    });
                }
            });
            var npMapThis = dialogOptions.npMapThis;
            $scope.model = new LayerModel();
            var indexLayers = npMapThis.layers.filter(function (l) { return l instanceof NpGeoLayers.SqlVectorLayer; });
            if (isVoid(indexLayers[0].orderIndex)) {
                for (var i = 0; i < indexLayers.length; i++) {
                    indexLayers[i].orderIndex = i + 1;
                    indexLayers[i].layer.setZIndex(i + 1);
                }
            }
            /**
            * @p.tsagkis
            * make sure elements ready on dom
            * before using jquery init functions
            * for spinner as well as cell color style representing layer legend
            * this is the angular timout fn. Thanks god
            */
            $timeout(function () {
                for (var z = 0; z < indexLayers.length; z++) {
                    var currentOpacity = indexLayers[z].layer.getOpacity();
                    //init the spinners
                    $('#layerOpacity_' + indexLayers[z].layerId).spinner({
                        min: 0,
                        max: 10,
                        step: 1,
                        spin: function (event, ui) {
                            var npLyr = npMapThis.getNPLayerById(this.id.split(/_(.+)?/)[1]);
                            npLyr.layer.setOpacity(ui.value / 10);
                            npLyr.setOpacity(ui.value / 10);
                        }
                    });
                    $('#layerOpacity_' + indexLayers[z].layerId).spinner("value", currentOpacity * 10);
                    //here handle the style preview of each layer next to its name
                    var fillColor = indexLayers[z].fillColor;
                    var borderColor = indexLayers[z].borderColor;
                    var penWidth = indexLayers[z].penWidth;
                    var fillColorMapped = (fillColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[fillColor] : fillColor;
                    var borderColorMapped = (borderColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[borderColor] : borderColor;
                    if (typeof fillColorMapped !== 'undefined') {
                        $('#layerstyletd_' + indexLayers[z].layerId).css('background-color', fillColorMapped);
                    }
                    if (typeof borderColorMapped !== 'undefined') {
                        $('#layerstyletd_' + indexLayers[z].layerId).css('border-color', borderColorMapped);
                    }
                    if (typeof borderColorMapped === 'undefined') {
                        $('#layerstyletd_' + indexLayers[z].layerId).css('border-color', 'black');
                    }
                    if (typeof penWidth !== 'undefined') {
                        $('#layerstyletd_' + indexLayers[z].layerId).css('border-width', penWidth + "px");
                        $('#layerstyletd_' + indexLayers[z].layerId).css('border-style', "solid");
                    }
                    if (typeof penWidth === 'undefined') {
                        $('#layerstyletd_' + indexLayers[z].layerId).css('border-width', "1px");
                        $('#layerstyletd_' + indexLayers[z].layerId).css('border-style', "solid");
                    }
                }
                //asign the hover over row effect here
                $('#layersorter').children().each(function () {
                    $(this).mousedown(function () {
                        $(this).css('cursor', 'move');
                        $(this).animate({
                            'background-color': '#dbdbdb'
                        }, 400);
                    }).mouseup(function () {
                        $(this).css('cursor', 'default');
                        $(this).animate({
                            'background-color': 'transparent'
                        }, 400);
                    });
                    //here is the right click context menu functionality on layer tree rows
                    //$(this).on("contextmenu", function (event) {
                    //    alert("open menu for layerId=" + this.id);
                    //    $(document.body).append(
                    //        '<ul style="z-index:9999;width:150px" id="lyrmenu">' +
                    //        '<li>Ιδιότητες</li>' +
                    //        '</ul>'
                    //    );
                    //    $("#lyrmenu").menu();
                    //    $("#lyrmenu").position({
                    //        my: "left top",
                    //        of: event
                    //    });
                    //    $("#lyrmenu").show();
                    //    return false;
                    //    });
                });
            });
            $scope.model.layers =
                npMapThis.layers.
                    filter(function (l) { return l instanceof NpGeoLayers.SqlVectorLayer; }).
                    sort(function (a, b) {
                    if (a.orderIndex > b.orderIndex) {
                        return 1;
                    }
                    else if (a.orderIndex < b.orderIndex) {
                        return -1;
                    }
                    else {
                        return 0;
                    }
                }).map(function (l) { return new LayerState(l.label, l.layerId, l.isVisible, l.getLabelVisible(), l.getVerticesVisible(), l.getSelectable(), l.getlSnapEnable(), l.orderIndex, l.getOpacity(), l); });
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
            $scope.change = function (l) {
                l.sqlLayer.setIsVisible(l.enabled); // Update sessionStorage info
                l.sqlLayer.layer.setVisible(l.sqlLayer.isVisible);
                l.sqlLayer.setSelectable(l.selectable); // sessionStorage info
                l.sqlLayer.setLabelVisible(l.labelEnabled); // sessionStorage info
                l.sqlLayer.setVerticesVisible(l.vertexEnabled); // sessionStorage info
                l.sqlLayer.setLSnapEnable(l.lSnapEnable); // sessionStorage info
                //@p.tsagkis setting snapping features depending on the visibility
                if ((l.sqlLayer.isVisible === false || l.sqlLayer.lSnapEnable === false) && npMapThis.snapEnabled === true) {
                    //need to remove snapping features of this layer
                    var featsArray = npMapThis.snapFeats.getArray();
                    var clonedFeats = $.extend(true, [], featsArray); //deep clone it. Necessary step to hanldle collection array.
                    for (var f = 0; f < clonedFeats.length; f++) {
                        console.log("clonedFeats[f].get('layerId')", clonedFeats[f].get('layerId'));
                        if (clonedFeats[f].get('layerId') === l.layerId) {
                            npMapThis.snapFeats.remove(clonedFeats[f]);
                        }
                    }
                    clonedFeats = []; //release it
                }
                if (l.sqlLayer.isVisible === true && l.sqlLayer.lSnapEnable === true && npMapThis.snapEnabled === true) {
                    //need to add the features of the clicked layer to snap feats
                    var featsToAdd = l.sqlLayer.layer.getSource().getFeatures();
                    for (var t = 0; t < featsToAdd.length; t++) {
                        featsToAdd[t].set('layerId', l.layerId);
                        npMapThis.snapFeats.push(featsToAdd[t]);
                    }
                }
                l.sqlLayer.setOrderIndex(l.orderIndex); // sessionStorage info
                l.sqlLayer.setOpacity(l.opacity); // sessionStorage info
                (l.sqlLayer.layer).setStyle(NpGeoGlobals.createPolygonWithVerticesStyleFunction(npMapThis, l.sqlLayer.styleFunc, l.sqlLayer.fillColor, l.sqlLayer.borderColor, l.sqlLayer.labelColor, l.sqlLayer.penWidth, undefined, l.sqlLayer.getVerticesVisible(), l.sqlLayer.getLabelVisible()));
            };
        }
        return controllerLayers;
    })();
    Controllers.controllerLayers = controllerLayers;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerLayers.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
