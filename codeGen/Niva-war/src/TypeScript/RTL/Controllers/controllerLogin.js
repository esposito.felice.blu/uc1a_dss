/// <reference path="../services.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../utils.ts" />
/// <reference path="controllerLovRTL_Subscriber.ts" />
/// <reference path="../Entities/RTL_Subscriber.ts" />
var Controllers;
(function (Controllers) {
    var ModelLogin = (function () {
        function ModelLogin() {
            this._subscriber = new NpTypes.UIManyToOneModel(undefined);
            this.loginSubsCodeCookiekey = 'allApps' + '-loginSubsCode';
        }
        Object.defineProperty(ModelLogin.prototype, "subscriber", {
            get: function () {
                return this._subscriber.value;
            },
            set: function (vl) {
                this._subscriber.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        return ModelLogin;
    })();
    Controllers.ModelLogin = ModelLogin;
    var controllerLogin = (function () {
        function controllerLogin($scope, $http, $timeout, Plato, LoginService, NavigationService) {
            var _this = this;
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.LoginService = LoginService;
            this.NavigationService = NavigationService;
            var self = this;
            var scope = $scope;
            scope.globals.mainAppTemplate.showAll(false);
            Utils.UpdateMainWindowWidthAndMainButtonPlacement(scope);
            scope.modelLogin = new ModelLogin();
            scope.modelLogin.email = "";
            scope.modelLogin.password = "";
            scope.modelLogin.message = "";
            scope.modelLogin.subscriber = null;
            scope.isLoggedIn = function () {
                return LoginService.isLoggedIn();
            };
            scope.logout = function () {
                LoginService.logout();
            };
            scope.changePassword = function () {
                LoginService.changePassword();
            };
            scope.checkCredentials = function () {
                // Unfortunately, only Firefox works properly with autocompletion.
                // Chrome / webkit don't autocomplete at all - and when sometimes they do,
                // they DONT trigger an update to the bound ng-models ....
                // (sigh)
                //
                // We therefore use jQuery to read these two:
                scope.modelLogin.email = $('#emailForLogin').val();
                scope.modelLogin.password = $('#passwordForLogin').val();
                var loginSubsCode = null;
                if (!self.isLoginSubscriberInvisible()) {
                    loginSubsCode = isVoid(scope.modelLogin.subscriber) ? null : scope.modelLogin.subscriber.shortName;
                    if (supports_html5_storage()) {
                        if (isVoid(loginSubsCode)) {
                            localStorage.removeItem(_this.modelLogin.loginSubsCodeCookiekey);
                        }
                        else {
                            localStorage[_this.modelLogin.loginSubsCodeCookiekey] = loginSubsCode;
                        }
                    }
                }
                LoginService.login(scope.modelLogin.email, scope.modelLogin.password, loginSubsCode, function () { }, function (errMsg) {
                    scope.modelLogin.message = errMsg;
                    $timeout(function () {
                        scope.modelLogin.message = "";
                    }, 4000);
                });
            };
            scope.isLoginSubscriberInvisible = function () { return self.isLoginSubscriberInvisible(); };
            scope.showLov_subscriber =
                function () {
                    $timeout(function () {
                        self.showLov_subscriber();
                    }, 0);
                };
            scope.findByCode_Login_subscriber =
                function (dummyContext, newCodeValue, component) {
                    self.findByCode_Login_subscriber(newCodeValue, component);
                };
            if (!self.isLoginSubscriberInvisible()) {
                if (supports_html5_storage()) {
                    var storedLoginSubsCode = localStorage[this.modelLogin.loginSubsCodeCookiekey];
                    if (!isVoid(storedLoginSubsCode))
                        self.findByCode_Login_subscriber(storedLoginSubsCode, null);
                }
            }
        }
        Object.defineProperty(controllerLogin.prototype, "modelLogin", {
            get: function () {
                return this.$scope.modelLogin;
            },
            enumerable: true,
            configurable: true
        });
        controllerLogin.prototype.isLoginSubscriberInvisible = function () {
            var self = this;
            if (isVoid(self.$scope.globals))
                return false;
            return self.$scope.globals.isLoginSubscriberInvisible();
        };
        controllerLogin.prototype.showLov_subscriber = function () {
            var self = this;
            var uimodel = self.modelLogin._subscriber;
            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Φορείς';
            dialogOptions.previousModel = self.cachedLovModel_subscriber;
            dialogOptions.className = "ControllerLovRTL_Subscriber";
            dialogOptions.onSelect = function (selectedEntity) {
                var subscriber1 = selectedEntity;
                uimodel.clearAllErrors();
                if (!isVoid(subscriber1) && subscriber1.isEqual(self.modelLogin.subscriber))
                    return;
                self.modelLogin.subscriber = subscriber1;
            };
            dialogOptions.openNewEntityDialog = function () {
            };
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                self.cachedLovModel_subscriber = lovModel;
                var wsPath = "Login/findAllByCriteriaRange_Login_subscriber";
                var url = "/" + self.$scope.globals.appName + "/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_short_name)) {
                    paramData['srch_subs_short_name'] = lovModel.srch_subs_short_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_vat)) {
                    paramData['srch_subs_vat'] = lovModel.srch_subs_vat;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_legal_name)) {
                    paramData['srch_subs_legal_name'] = lovModel.srch_subs_legal_name;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var wsPath = "Login/findAllByCriteriaRange_Login_subscriber_count";
                var url = "/" + self.$scope.globals.appName + "/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_short_name)) {
                    paramData['srch_subs_short_name'] = lovModel.srch_subs_short_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_vat)) {
                    paramData['srch_subs_vat'] = lovModel.srch_subs_vat;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_legal_name)) {
                    paramData['srch_subs_legal_name'] = lovModel.srch_subs_legal_name;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                var paramData = {};
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_short_name)) {
                    paramData['srch_subs_short_name'] = lovModel.srch_subs_short_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_vat)) {
                    paramData['srch_subs_vat'] = lovModel.srch_subs_vat;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.srch_subs_legal_name)) {
                    paramData['srch_subs_legal_name'] = lovModel.srch_subs_legal_name;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            dialogOptions.shownCols['shortName'] = true;
            dialogOptions.shownCols['legalName'] = true;
            self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/RTL_Subscriber.html?rev=' + self.$scope.globals.version, dialogOptions);
        };
        controllerLogin.prototype.findByCode_Login_subscriber = function (newCodeValue, component) {
            var self = this;
            var uimodel = self.modelLogin._subscriber;
            var setSubscriber = function (subscriber1) {
                if (!isVoid(subscriber1) && subscriber1.isEqual(self.modelLogin.subscriber)) {
                    return;
                }
                self.modelLogin.subscriber = subscriber1;
            };
            var onSuccess = function (subscriber1) {
                uimodel.clearAllErrors();
                setSubscriber(subscriber1);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
            };
            var onError = function (err) {
                var err_msg;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                if (err === 'TooManyRows') {
                    err_msg = self.$scope.globals.getDynamicMessage("EntityByCode_TooManyRowsForCodeMsg(\"" + newCodeValue + "\")");
                }
                else {
                    err_msg = self.$scope.globals.getDynamicMessage("EntityByCode_NoRecordForCodeMsg(\"" + newCodeValue + "\")");
                }
                uimodel.addNewErrorMessage(err_msg, false);
                if (!isVoid(component)) {
                    if (!isVoid(self.modelLogin.subscriber)) {
                        component.element.val(self.modelLogin.subscriber.shortName);
                    }
                    else {
                        component.element.val(null);
                    }
                }
                self.$timeout(function () {
                    uimodel.clearAllErrors();
                }, 3000);
            };
            if (isVoid(newCodeValue) || (newCodeValue === "")) {
                onSuccess(null);
            }
            else {
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout, true);
                Entities.RTL_Subscriber.findByCode_Login_subscriber(newCodeValue, self.$scope, self.$http, onSuccess, onError);
            }
        };
        return controllerLogin;
    })();
    Controllers.controllerLogin = controllerLogin;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerLogin.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
