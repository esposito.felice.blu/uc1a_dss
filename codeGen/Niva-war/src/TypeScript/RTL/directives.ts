/// <reference path="../DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="npTypes.ts" />
/// <reference path="utils.ts" />
/// <reference path="base64Utils.ts" />

module Directives {

    // Common framework for per-directive logging 
    function infoGeneric(bDebug:boolean) {
        if (bDebug) {
            return (msg:string) => { console.log(msg); };
        } else {
            return (msg:string) => {};
        }
    }
    function warn(msg:string)  { console.log("WARNING: " + msg); }
    function error(msg:string) { console.error("ERROR: " + msg); }
    function identity(x)       { return x;                       }

    function logWrapGeneric(bDebug:boolean) {
        if (bDebug) {
            return (funFoo, filters={inpFilter:identity, outFilter:identity}) => {
                var info = infoGeneric(true);
                if (filters.inpFilter === undefined) filters.inpFilter = identity;
                if (filters.outFilter === undefined) filters.outFilter = identity;
                return function () {
                    var args = arguments;
                    if (arguments.length>=1) {
                        if (filters.inpFilter)
                            info(funFoo.name + "(" + filters.inpFilter(args[0]) + ")");
                        else
                            info(funFoo.name + "(" + args[0] + "...)");
                    } else {
                        info(funFoo.name + "()");
                    }
                    var ret = funFoo.apply(this, args);
                    if (filters.outFilter)
                        info(funFoo.name.toString() + "() returns " + filters.outFilter(ret));
                    else
                        info(funFoo.name.toString() + "() returns " + ret);
                    return ret;
                };
            }
        } else {
            // Release builds -> don't log anything
            return (funFoo, filters={inpFilter:identity, outFilter:identity}) => {
                return funFoo;
            }
        }
    }

    // Custom directives for components:
    // 
    //   npCheckbox, npText, npNumber, npDate, npRichtext, npLookup, npBlob and npSelect
    //
    // Individual mini help is on top of each directive (as an HTML syntax example).
    //
    // How these directives work: first of all, something to know about $formatters and $parsers:
    //
    //   change in $modelValue ->
    //       triggers $formatters ->
    //           results in new $viewValue ->
    //               $render is called -> update the DOM using $viewValue
    //
    //   we call $setViewValue ->
    //       $viewValue changes ->
    //           $parsers are triggered ->
    //               $modelValue changes
    //
    // So $parsers perform the validation, but we only want to do that when we are out of focus ;
    // (that is, when we leave the control). If any of the validations fail,  we return the last known
    // good value (stored in options.oldModelValue) and show a Bootstrap popover with the error in question.

    /////////////////////////////////////////////////////////////////////////////////////////////
    // 3-state checkboxes - used like this:
    //
    //   <input name="employee" type="checkbox"
    //          data-ng-model="modelCustomers.isEmployee"    (pointing to the 'value' property of np-ui-model)
    //          data-np-ui-model="modelCustomers._isEmployee"    (of type NpTypes.UIBoolModel)
    //          data-np-checkbox
    //          data-np-required="false"      (set to false triggers 3-state behaviour,
    //                                         that is, false => undefined => true)
    //          data-np-validate="someFunc">  (someFunc takes a (boolean,entity) and returns NpTypes.ValidationResult)
    // (the model is a boolean)

    interface ICheckBoxScope extends ng.IScope {}

    function npCheckbox($timeout) {
        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return {
            // We will change the model via this directive
            require: '?ngModel',

            link: (scope:ICheckBoxScope, element:JQuery, attrs, ngModel) => {
                if (!ngModel) return; // do nothing if no ng-model was passed in.

                // Read the options passed in the directive
                var component:NpTypes.NpBoolean = new NpTypes.NpBoolean(element, attrs, $timeout, scope, debugDirective);

                // Unbelievable: in some browsers, change will be called BEFORE $parser,
                // and in others, AFTER!  Web development: "kill me now" :-)
                // We use an empty function for change listener and call function change from parser.

                function m2v(state:boolean) {
                    var b1:boolean, b2:boolean;
                    if      (state === false)     { b1 = false; b2 = false; }
                    else if (state === undefined || state === null) { b1 = false; b2 = true;  }
                    else if (state === true)      { b1 = true;  b2 = false; }
                    $(element).prop('checked', b1);
                    $(element).prop('indeterminate', b2 && !component.required);
                }

                function nextState(state:boolean) {
                    if (!component.required) {
                        if (state === false)          return undefined;
                        else if (state === undefined || state === null) return true;
                        else if (state === true)      return false;
                    } else {
                        return !state;
                    }
                }

                function change() {
                    var state = ngModel.$modelValue;
                    var oldState = state;
                    //info('change() called with ' + state);
                    state = nextState(state);
                    component.clearAllErrors();
                    if (component.validationCallback !== undefined) {
                        var validationResult:NpTypes.ValidationResult =
                            scope[component.validationCallback](state, component.entity);
                        while (!validationResult.isValid) {
                            component.addNewErrorMessage(validationResult.errorMessage, true);
                            state = nextState(state);
                            var validationResult:NpTypes.ValidationResult =
                                scope[component.validationCallback](state, component.entity);
                        }
                    }
                    m2v(state);
                    ngModel.$modelValue = state;
                }
                element.on('change', (event => { }));

                // when Angular detects the model changed, it will call the formatters...
                function formatter(modelValue:boolean) {
                    // ...and we store the model value
                    // so that we can reset back to it if the validations fail.
                    if (isVoid(component.uiModel))
                        return false;

                    component.safelySetOldValue(modelValue);
                    m2v(modelValue);
                    return modelValue;
                }
                ngModel.$formatters.push(logWrap(formatter));
                component.formatter = formatter;

                // Angular calls this when the model -> formatters -> $viewValue call chain completes.
                function render() {
                    var state:boolean = ngModel.$modelValue;
                    m2v(state);
                }
                ngModel.$render = render;

                // Whenever the control content changes, Angular will call this:
                // check that the value is valid, and if so, return the updated model value
                // otherwise show a popover, and return the last known good value
                function parser(viewValue:boolean) {
                    change();
                    viewValue = ngModel.$modelValue;
                    //info("parser(): Actual value:" + viewValue);
                    //info("parser() called with oldModelValue:"+component.oldModelValue+" and new value:"+viewValue);
                    ngModel.$modelValue = null; // Force model update
                                                // (angular.js, line 13435)
                    component.safelySetOldValue(viewValue);
                    return viewValue;
                }
                ngModel.$parsers.push(logWrap(parser));
            }
        }
    }

    function npCheckboxNumber($timeout) {
        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return {
            // We will change the model via this directive
            require: '?ngModel',

            link: (scope: ICheckBoxScope, element: JQuery, attrs, ngModel) => {
                if (!ngModel) return; // do nothing if no ng-model was passed in.

                // Read the options passed in the directive
                var component: NpTypes.NpNumber = new NpTypes.NpNumber(element, attrs, $timeout, scope, debugDirective);

                // Unbelievable: in some browsers, change will be called BEFORE $parser,
                // and in others, AFTER!  Web development: "kill me now" :-)
                // We use an empty function for change listener and call function change from parser.

                function m2v(state: number) {
                    var b1: boolean, b2: boolean;
                    if (state === 0) { b1 = false; b2 = false; }
                    else if (state === undefined || state === null) { b1 = false; b2 = true; }
                    else { b1 = true; b2 = false; }
                    $(element).prop('checked', b1);
                    $(element).prop('indeterminate', b2 && !component.required);
                }

                function nextState(state: number) {
                    if (!component.required) {
                        if (state === 0) return undefined;
                        else if (state === undefined || state === null) return 1;
                        else return 0;
                    } else {
                        if (state === 0 || state === undefined || state === null) return 1;
                        else return 0;
                    }
                }

                function change() {
                    var state = ngModel.$modelValue;
                    var oldState = state;
                    //info('change() called with ' + state);
                    state = nextState(state);
                    component.clearAllErrors();
                    if (component.validationCallback !== undefined) {
                        var validationResult: NpTypes.ValidationResult =
                            scope[component.validationCallback](state, component.entity);
                        while (!validationResult.isValid) {
                            component.addNewErrorMessage(validationResult.errorMessage, true);
                            state = nextState(state);
                            var validationResult: NpTypes.ValidationResult =
                                scope[component.validationCallback](state, component.entity);
                        }
                    }
                    m2v(state);
                    ngModel.$modelValue = state;
                }
                element.on('change', (event => { }));

                // when Angular detects the model changed, it will call the formatters...
                function formatter(modelValue: number) {
                    // ...and we store the model value
                    // so that we can reset back to it if the validations fail.
                    if (isVoid(component.uiModel))
                        return 0;

                    component.safelySetOldValue(modelValue);
                    m2v(modelValue);
                    return modelValue;
                }
                ngModel.$formatters.push(logWrap(formatter));
                component.formatter = formatter;

                // Angular calls this when the model -> formatters -> $viewValue call chain completes.
                function render() {
                    var state: number = ngModel.$modelValue;
                    m2v(state);
                }
                ngModel.$render = render;

                // Whenever the control content changes, Angular will call this:
                // check that the value is valid, and if so, return the updated model value
                // otherwise show a popover, and return the last known good value
                function parser(viewValue: number) {
                    change();
                    viewValue = ngModel.$modelValue;
                    //info("parser(): Actual value:" + viewValue);
                    //info("parser() called with oldModelValue:"+component.oldModelValue+" and new value:"+viewValue);
                    ngModel.$modelValue = null; // Force model update
                    // (angular.js, line 13435)
                    component.safelySetOldValue(viewValue);
                    return viewValue;
                }
                ngModel.$parsers.push(logWrap(parser));
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    // HTML side for npText:
    //   <input name="someText"
    //          data-ng-model="modelCustomers.someText"    (pointing to the 'value' property of np-ui-model)
    //          data-np-ui-model="modelCustomers._someText"    (of type NpTypes.UIStringModel)
    //          data-np-text
    //          data-np-required="false"      (set to true means this cant be empty)
    //          data-np-validate="someFunc">  (someFunc takes a (string,entity) and returns NpTypes.ValidationResult)
    // (the model is a string)

    interface ITextScope extends ng.IScope {}

    function npText($timeout) {
        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return {
            // We will change the model via this directive
            require: '?ngModel',

            link: (scope:ITextScope, element, attrs, ngModel) => {
                if(!ngModel) return; // do nothing if no ng-model

                // Read the options passed in the directive
                var component:NpTypes.NpText = new NpTypes.NpText(element, attrs, $timeout, scope, debugDirective);
                var passwordNoAutoComplete: boolean = attrs.npText === 'passwordNoAutoComplete';
                
                // Auto-select everything on focus
                function focus(e) {
                    $timeout( () => { element.selectAll(); }, 0);
                }
                element.focus(logWrap(focus));

                function blur(e) {
                    // Don't change the model if we are readOnly or disabled
                    if (element[0].readOnly === true || element[0].disabled === true)
                        return;
                    // Update the date model
                    ngModel.$setViewValue(element.val().replace(/\s*$/, '')); // right trim
                    // trigger a digest
                    $timeout(() => { scope.$apply(); }, 0);
                }
                element.blur(logWrap(blur));

                // when Angular detects the model changed, it will call the formatters...
                function formatter(modelValue:string) {
                    if (isVoid(component.uiModel))
                        return '';

                    // ...and the first time this is called, we store the model value
                    // so that we can reset back to it if the validations fail.
                    component.safelySetOldValue(modelValue);
                    if (isVoid(modelValue))
                        return '';

                    // When we use page next/previous, we want the control to show
                    // the content and the validity state of the uimodel
                    var lastViewValue = component.safelyGetLastViewValue();
                    if (lastViewValue !== undefined) {
                        if (!component.uiModel.isValid) {
                            component.addNewErrorMessage(
                                component.uiModel.validationMsgs[0].msg,
                                component.uiModel.validationMsgs[0].noHover);
                            return lastViewValue;
                        }
                        else
                            component.clearAllErrors();
                    } else
                        component.clearAllErrors();

                    var scrollToBottom:boolean = scope.$eval(attrs.npScrollToBottom);
                    // Also, undefined/null should render as an empty string:
                    if (isVoid(modelValue)) {
                        return '';
                    } else {
                        if (scrollToBottom)
                            $timeout(() => { element.scrollTop(element[0].scrollHeight); });
                        return isVoid(component.userDefinedFormatter) ? modelValue : component.userDefinedFormatter(modelValue, component.entity);
                    }
                }
                ngModel.$formatters.push(logWrap(formatter));
                component.formatter = formatter;

                // Angular calls this when the model -> formatters -> $viewValue call chain completes,
                // so, just update the DOM:
                function render() {
                    element.val(ngModel.$viewValue);
                }
                ngModel.$render = logWrap(render);

                // Whenever the control content changes, Angular will call this.
                // Check that the value is valid, and if so, return the updated model value.
                // Otherwise show a popover, and return the last known good value.
                function parser(viewValue:string) {

                    // Browsers ignore attribute 'autoComplete' when type is 'password'.
                    // This result to auto-fill the password fields and previous input field even if 'autocomplete' is set to off.
                    // To avoid this behaviour we make type = 'password' dynamically only when the field is not blank.
                    // When the field is not blank the browser do not autocomplete fields (or preview list of passwords)
                    if (passwordNoAutoComplete) {
                        if (isVoid(viewValue) || viewValue === '') {
                            $(element).attr('type', 'text');
                        } else {
                            $(element).attr('type', 'password');
                            $(element).selectRange(viewValue.length, viewValue.length);
                        }
                    }

                    // Don't propagate the changes while we still have the focus.
                    if ($(element).is(":focus"))
                        return component.safelyGetOldValue('');

                    // Set the uimodel, so we can revert to this new viewvalue in page next/previous
                    component.safelySetLastViewValue(viewValue);

                    // ...and since validations will re-run, start with a clean slate:
                    component.clearAllErrors();

                    var requiredMsg = ' ' + component.getDynamicMessage("FieldValidation_RequiredMsg2");
                    if (viewValue === '') {
                        if (component.required) {
                            if (isVoid(ngModel.$modelValue) || (ngModel.$modelValue === '')) {
                                component.addNewErrorMessage(requiredMsg);
                                return component.safelyGetOldValue('');
                            } else {
                                component.addNewErrorMessage(requiredMsg, true);
                                element.val(ngModel.$modelValue);
                                return ngModel.$modelValue;
                            }
                        } else
                            // empty content corresponds to null (targeting: DB NULLs)
                            return null;
                    }
                    var modelValue = isVoid(component.userDefinedParser) ? viewValue : component.userDefinedParser(viewValue, component.entity);

                    if (component.validationCallback !== undefined) {
                        var validationResult:NpTypes.ValidationResult =
                            scope[component.validationCallback](modelValue, component.entity);
                        if (!validationResult.isValid) {
                            component.addNewErrorMessage(validationResult.errorMessage);
                            return component.safelyGetOldValue('');
                        }
                    }
                    // The new input was solid - first, clear any error popovers...
                    component.clearAllErrors();

                    // ... and return the new value so the model will be updated.
                    var correctedViewValue = isVoid(component.userDefinedFormatter) ? modelValue : component.userDefinedFormatter(modelValue, component.entity)
                    element.val(correctedViewValue);
                    component.safelySetOldValue(modelValue);
                    return modelValue;
                }
                ngModel.$parsers.push(logWrap(parser));
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    // HTML side for npNumber:
    //   <input name="someNumber"
    //          data-ng-model="modelCustomers.someNumber"    (pointing to the 'value' property of np-ui-model)
    //          data-np-ui-model="modelCustomers._someNumber"    (of type NpTypes.UIStringModel)
    //          data-np-number
    //          data-np-required="false"      (set to true means this can't be empty)
    //          data-np-validate="someFunc"   (someFunc takes a (number,entity) and returns NpTypes.ValidationResult)
    //          data-np-min="0.123"
    //          data-np-max="1000.234"
    //          data-np-decimals="2"          (int)
    //          data-np-decSep=","
    //          data-np-thSep="."
    // (the model is a number)

    interface INumberScope extends ng.IScope {}

    function npNumber($timeout) {
        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return {
            // We will change the model via this directive
            require: '?ngModel',

            link: (scope:INumberScope, element, attrs, ngModel) => {
                if(!ngModel) return; // do nothing if no ng-model was passed-in.

                // Read the options passed in the directive
                var component:NpTypes.NpNumber = new NpTypes.NpNumber(element, attrs, $timeout, scope, debugDirective);

                // Converters to and from between the model (number) and the two state strings (edit/view)

                function numberToEditText(n:number):string {
                    if (isVoid(n)) return ''; // the model may be undefined/null by the user
                    return n.toString().split(component.localeDecSep).join(component.decSep);
                }

                function numberToViewText(n:number):string {
                    /*if (isVoid(n)) return ''; // the model may be undefined/null by the user
                    var parts = n.toString().split(component.localeDecSep);
                    // for the integer part, add thousands separators:
                    // From http://stackoverflow.com/questions/17294959/how-does-b-d3-d-g-work-for-adding-comma-on-numbers
                    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, component.thSep);
                    // for the fractional part, if it doesn't exist, add it:
                    if (!isVoid(component.decimals) && component.decimals !== 0) {
                        if (parts.length === 1)
                            parts.push('')
                        // ...and then make sure it is lengthy up to the required decimals
                        while(parts[1].length < component.decimals)
                            parts[1] += "0";
                    }
                    return parts.join(component.decSep);*/
                    if (typeof n !== 'number') return n.toString();
                    return n.toStringFormatted(component.decSep, component.thSep, component.decimals);
                }

                function editTextToNumber(t:string):number {
                    if (!component.required && t == '')
                        return null;
                    return parseFloat(t.replace(component.decSep, component.localeDecSep));
                }

                function focus(e) {
                    // If showError has been called (and the control has been marked with Bootstrap error colors)
                    // then don't re-read from the model - the user has already edited, keep his changes
                    //assert(!isVoid(component.uiModel);
                    if ((!isVoid(component.uiModel)) && component.uiModel.isValid && !element.is("[readonly]"))
                        element.val(numberToEditText(ngModel.$modelValue));
                    // Auto-select everything on focus
                    $timeout( () => { element.selectAll(); }, 0);
                }
                element.focus(logWrap(focus));

                function blur(e) {
                    // Don't change the model if we are readOnly or disabled
                    if (element[0].readOnly === true || element[0].disabled === true)
                        return;
                    // Update the number model (via the $parsers)
                    var newValue = element.val().replace(/\s*$/, '').replace(/^\s*/, ''); // left right trim
                    if (!isVoid(component.thSep) && component.thSep !== ''
                        && newValue.indexOf(component.decSep) === -1
                        && (newValue.split(component.thSep).length - 1) === 1
                        && component.decimals > 0
                        ) {
                        newValue = newValue.replace(new RegExp(component.thSep.replace(".","\\."), "g"), component.decSep);
                    }
                    ngModel.$setViewValue(newValue);
                    // Trigger a digest
                    $timeout(() => { scope.$apply(); }, 0);
                }
                element.blur(logWrap(blur));

                // when Angular detects the model changed, it will call the formatters...
                function formatter(modelValue:number) {
                    if (isVoid(component.uiModel)) 
                        return '';

                    // ...and the first time this is called, we store the model value
                    // so that we can reset back to it if the validations fail.
                    component.safelySetOldValue(modelValue);
                    if (isVoid(modelValue))
                        return '';

                    // When we use page next/previous, we want the control to show
                    // the content and the validity state of the uimodel
                    var lastViewValue = component.safelyGetLastViewValue();
                    if (lastViewValue !== undefined) {
                        if (!component.uiModel.isValid) {
                            component.addNewErrorMessage(
                                component.uiModel.validationMsgs[0].msg,
                                component.uiModel.validationMsgs[0].noHover);
                            return lastViewValue;
                        } else
                            component.clearAllErrors();
                    } else
                        component.clearAllErrors();

                    // Also, 'undefined' should render as an empty string:
                    if (modelValue === undefined) {
                        return '';
                    } else
                        return numberToViewText(modelValue);
                }
                ngModel.$formatters.push(logWrap(formatter));
                component.formatter = formatter;

                // Angular calls this when the model -> formatters -> $viewValue call chain completes,
                // so, just update the DOM:
                function render() {
                    element.val(ngModel.$viewValue);
                }
                ngModel.$render = logWrap(render);

                // Whenever the control's content changes, Angular will call this.
                // Check that the value is valid, and if so, return the updated model value.
                // Otherwise show a popover and return the last known good value.
                function parser(viewValue:string) {
                    // Don't propagate the changes while we still have the focus.
                    if ($(element).is(":focus"))
                        return component.safelyGetOldValue(null);

                    // Set the uimodel, so we can revert to this new viewvalue in page next/previous
                    component.safelySetLastViewValue(viewValue);

                    // viewValue = viewValue.replace(component.compiledRegExClean, '');
                    // On second thought, forbid thousand separator character altogether:
                    /*if (component.thSep !== null &&
                            component.thSep !== undefined &&
                            component.thSep !== '' &&
                            viewValue.indexOf(component.thSep) != -1) {
                         component.addNewErrorMessage(
                             ' Επιτρέπονται μόνο ' + (component.decimals === 0 ? 'ακέραιοι ' : '') + 'αριθμοί' + ((component.decimals>0)?
                                ', μία υποδιαστολή ' /*(' + component.decSep + ') '*\/ +
                                "και μέχρι " + component.decimals + ' δεκαδικά ψηφία'
                                :''));
                        return component.safelyGetOldValue(null);
                    }*/

                    component.clearAllErrors();
                    var requiredMsg = ' ' + component.getDynamicMessage("FieldValidation_RequiredMsg2");
                    if (viewValue === '') {
                        if (component.required) {
                            if (isVoid(ngModel.$modelValue)) {
                                component.addNewErrorMessage(requiredMsg);
                                return component.safelyGetOldValue(null);
                            } else {
                                component.addNewErrorMessage(requiredMsg, true);
                                element.val(numberToViewText(ngModel.$modelValue));
                                return ngModel.$modelValue;
                            }
                        } else
                            // empty content corresponds to null (targeting: DB NULLs)
                            return null;
                    } else {
                        var errMsg: string = " " + component.getDynamicMessage("FieldValidation_NumbersOnlyMsg");
                        if (component.decimals === 0)
                            errMsg = " " + component.getDynamicMessage("FieldValidation_IntegersOnlyMsg");
                        if (component.decimals > 0)
                            errMsg = " " + component.getDynamicMessage("FieldValidation_DecimalsMsg(" + component.decimals + ")");

                        if (component.thSep !== null && component.thSep !== undefined && component.thSep !== '' && viewValue.indexOf(component.thSep) !== -1) {
                            var parts = viewValue.split(component.decSep);
                            var intParts = parts[0].split(component.thSep);
                            intParts.splice(0,1);
                            if (intParts.some((i) => { return i.length !== 3 })) {
                                component.addNewErrorMessage(errMsg);
                                return component.safelyGetOldValue(null);
                            } else {
                                viewValue = viewValue.replace(new RegExp(component.thSep.replace(".", "\\."), "g"), "");
                            }
                        }
                        if (!component.compiledRegExValidate.test(viewValue) || viewValue === component.decSep) {
                            // it fails the regex, it's not valid
                            info('This (' + viewValue + ') is invalid due to regex: ' + component.compiledRegExValidate);
                            component.addNewErrorMessage(errMsg);
                            return component.safelyGetOldValue(null);
                        }

                        var newValueNumber:number = editTextToNumber(viewValue);
                        if (newValueNumber>component.max || newValueNumber<component.min) {
                            // it fails the range check
                            info(viewValue + ' is invalid due to range check');
                            if (component.min === Number.NEGATIVE_INFINITY) {
                                component.addNewErrorMessage(" " + component.getDynamicMessage("FieldValidation_Numbers_Allowed_MaxMsg(\"" + component.max.toString().replace(".", component.decSep) + "\")"));
                            } else if (component.max === Number.POSITIVE_INFINITY) {
                                component.addNewErrorMessage(" " + component.getDynamicMessage("FieldValidation_Numbers_Allowed_MinMsg(\"" + component.min.toString().replace(".", component.decSep) + "\")"));
                            } else {
                                component.addNewErrorMessage(" " + component.getDynamicMessage("FieldValidation_Numbers_Allowed_MinMaxMsg(\"" + component.min.toString().replace(".", component.decSep) + "\", \"" + component.max.toString().replace(".", component.decSep) + "\")"));
                            }
                            return component.safelyGetOldValue(null);
                        }
                        if (component.validationCallback !== undefined) {
                            var validationResult:NpTypes.ValidationResult =
                                scope[component.validationCallback](newValueNumber, component.entity);
                            if (!validationResult.isValid) {
                                // it fails the user check
                                info(newValueNumber + ' is invalid due to custom check');
                                component.addNewErrorMessage(validationResult.errorMessage);
                                return component.safelyGetOldValue(null);
                            }
                        }

                        // The input was solid, return the updated value for the model.
                        // (but first, clear any error popovers)
                        component.clearAllErrors();
                        element.val(numberToViewText(newValueNumber));
                        component.safelySetOldValue(newValueNumber);
                        return newValueNumber;
                    }
                }
                ngModel.$parsers.push(logWrap(parser));
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    // HTML side for npDate:
    //      <input 
    //          data-ng-model="modelCustomers.someDate"    (pointing to the 'value' property of np-ui-model)
    //          data-np-ui-model="modelCustomers._someDate"    (of type NpTypes.UIDateModel)
    //          data-np-date
    //          data-np-required="false"      (set to true means this cant be empty)
    //          data-np-validate="someFunc"   (someFunc takes a (Date,entity) and returns NpTypes.ValidationResult)
    //          data-np-format="dd/MM/yyyy"   (format used for displaying)
    //          data-np-min-date="'01/01/1900'" (in same format as np-format)
    //          data-np-max-date="'01/01/9999'" (in same format as np-format)
    //          or
    //          data-np-max-date="someFunc()" (someFunc must return in same format as np-format)
    //          data-np-datePicker-yearRange="c-10:c+10" (the range of year drop down select list. Default is from current -10 to current +10 years.)
    // (the model is a Date)

    interface IDateScope extends ng.IScope {}

    function npDate($timeout) {
        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return {
            // We will change the model via this directive
            require: '?ngModel',

            link: (scope:IDateScope, element, attrs, ngModel) => {
                if(!ngModel) return; // do nothing if no ng-model

                // Read the options passed in the directive
                var component:NpTypes.NpDate = new NpTypes.NpDate(element, attrs, $timeout, scope, debugDirective);

                // Converters to and from between the model (number) and the two state strings (edit/view)
                // To use the javascript toolbox's 'format' and 'parseString' we need to cast...

                function dateToEditText(d:Date):string {
                    if (isVoid(d)) return ''; // the model may be undefined/null by the user
                    return (<any>d).format(component.formatNumbersOnly);
                }

                function dateToViewText(d:Date):string {
                    if (isVoid(d)) return ''; // the model may be undefined/null by the user
                    return (<any>d).format(component.format);
                }

                function editTextToSanitizedText(t:string):string {
                    if (t.indexOf('-') !== -1 || t.indexOf('/') !== -1) {
                        var sanitizedDateString = '';
                        _.each(
                            t.replace(/[\/-]/g, '@').split('@'),
                            (part, index) => {
                                if (index < component.sanitizedDatePartLengths.length) {
                                    while(part.length < component.sanitizedDatePartLengths[index]) {
                                        part = '0' + part;
                                    }
                                }
                                sanitizedDateString += part;
                            });
                        return sanitizedDateString;
                    } else {
                        return t;
                    }
                }

                function editTextToDate(t:string):Date {
                    //return (<any>Date).parseString(t, component.formatNumbersOnly);
                    var sanitizedDateString = editTextToSanitizedText(t);
                    return (<any>Date).parseString(sanitizedDateString, component.formatNumbersOnly);
                }

                // for logging during debugging (used as filter)
                function easyDate(d:Date) {
                    if (!d)
                        return '**';
                    else
                        return '*' + (<any>d).format('yyyy/MM/dd') + '*';
                }

                function focus(e) {
                    // If showError has been called (and the control has been marked with Bootstrap error colors)
                    // then don't re-read from the model - the user has already edited, keep his changes
                    if ((!isVoid(component.uiModel)) && component.uiModel.isValid && !element.is("[readonly]"))
                        element.val(element.val().replace(/[\/-]/g, ''));
                    // Auto-select everything on focus
                    $timeout( () => { element.selectAll(); }, 0);
                }
                element.focus(logWrap(focus));

                function blur(e) {
                    // Don't change the model if we are readOnly or disabled
                    if (element[0].readOnly === true || element[0].disabled === true)
                        return;
                    // Update the date model
                    ngModel.$setViewValue(element.val().replace(/\s*$/, ''));
                    // Trigger a digest
                    $timeout(() => { scope.$apply(); }, 0);
                }
                element.blur(logWrap(blur));

                // when Angular detects the model changed, it will call the formatters...
                function formatter(modelValue:Date) {
                    if (isVoid(component.uiModel))
                        return '';

                    // ...and the first time this is called, we store the model value
                    // so that we can reset back to it if the validations fail.
                    component.safelySetOldValue(modelValue);
                    if (isVoid(modelValue))
                        return '';

                    // When we use page next/previous, we want the control to show
                    // the content and the validity state of the uimodel
                    var lastViewValue = component.safelyGetLastViewValue();
                    if (lastViewValue !== undefined) {
                        if (!component.uiModel.isValid) {
                            component.addNewErrorMessage(
                                component.uiModel.validationMsgs[0].msg,
                                component.uiModel.validationMsgs[0].noHover);
                            return lastViewValue;
                        } else
                            component.clearAllErrors();
                    } else
                        component.clearAllErrors();

                    // Also, 'undefined' should render as an empty string:
                    if (modelValue === undefined) {
                        return '';
                    } else
                        return dateToViewText(modelValue);
                }
                ngModel.$formatters.push(logWrap(formatter, {inpFilter:easyDate, outFilter:identity}));
                component.formatter = formatter;

                // Angular calls this when the model -> formatters -> $viewValue call chain completes,
                // so, just update the DOM:
                function render() {
                    element.val(ngModel.$viewValue);
                }
                ngModel.$render = logWrap(render);

                // Whenever the control's content changes, Angular will call this.
                // Check that the value is valid, and if so, return the updated model value.
                // Otherwise show a popover and return the last known good value.
                function parser(viewValue:string) {
                    // Don't propagate the changes while we still have the focus.
                    if ($(element).is(":focus"))
                        return component.safelyGetOldValue(null);

                    // Set the uimodel, so we can revert to this new viewvalue in page next/previous
                    component.safelySetLastViewValue(viewValue);

                    var requiredMsg = ' ' + component.getDynamicMessage("FieldValidation_RequiredMsg2");
                    component.clearAllErrors();
                    if (viewValue === '') {
                        if (component.required) {
                            if (isVoid(ngModel.$modelValue)) {
                                component.addNewErrorMessage(requiredMsg);
                                return component.safelyGetOldValue(null);
                            } else {
                                component.addNewErrorMessage(requiredMsg, true);
                                element.val(dateToViewText(ngModel.$modelValue));
                                return ngModel.$modelValue;
                            }
                        } else
                            return null;
                    } else {
                        viewValue = editTextToSanitizedText(viewValue);
                        if (!component.compiledRegExValidate.test(viewValue)) {
                            component.addNewErrorMessage(" " + component.getDynamicMessage("FieldValidation_DateFormatMsg(\"" + component.formatNumbersOnly + "\")"));
                            return component.safelyGetOldValue(null);
                        }
                        var r = editTextToDate(viewValue.replace(/[\/-]/g, ''));
                        if (r === null) {
                            component.addNewErrorMessage(" " + component.getDynamicMessage("FieldValidation_DateFormatMsg(\"" + component.formatNumbersOnly + "\")"));
                            return component.safelyGetOldValue(null);
                        }
                        if (r<component.minDate || r>component.maxDate) {
                            component.addNewErrorMessage(
                                " " + component.getDynamicMessage("FieldValidation_DateFromMsg(\"" + (<any>component.minDate).format(component.format) + "\")") +
                                " " + component.getDynamicMessage("FieldValidation_DateToMsg(\"" + (<any>component.maxDate).format(component.format) + "\")") );
                            return component.safelyGetOldValue(null);
                        }
                        if (component.validationCallback !== undefined) {

                            var validationResult:NpTypes.ValidationResult =
                                scope[component.validationCallback](r, component.entity);
                            if (!validationResult.isValid) {
                                // it fails the user check
                                info(viewValue + ' is invalid due to custom check');
                                component.addNewErrorMessage(validationResult.errorMessage);
                                return component.safelyGetOldValue(null);
                            }
                        }

                        // The input was solid, return the updated value for the model.
                        // (but first, clear any error popovers)
                        component.clearAllErrors();
                        element.val(dateToViewText(r));
                        component.safelySetOldValue(r);
                        return r;
                    }
                }
                ngModel.$parsers.push(logWrap(parser, {inpFilter:identity, outFilter:easyDate}));

                function doubleClick() {
                    if (!element.is("[readonly]")) {
                        var jqFormatDate = component.format.replace(/M/g, 'm');
                        jqFormatDate = jqFormatDate.replace(/[\/-]/g, '');
                        if (/yyyy/.test(jqFormatDate)) {
                            jqFormatDate = jqFormatDate.replace(/yyyy/, 'yy');
                        } else {
                            jqFormatDate = jqFormatDate.replace(/yy/, 'y');
                        }
                        element.datepicker({
                            dateFormat: jqFormatDate,
                            changeYear: true,
                            yearRange: component.datePickerYearRange,
                            onSelect: (newStringDate: string, dpInst: any) => {
                                // when date is selected remove onClick event from data handlers 
                                // to avoid exception of null instance in double click  (e.g we want to do nothing at second click)
                                // The exception occures when double click  since we destroy the datepicker on close.
                                dpInst.dpDiv.find("[data-event]").find("a").on("click", () => { return false; });
                            },
                            onClose: (newStringDate:string) => {
                                element.datepicker("destroy");
                                // Update the date model
                                ngModel.$setViewValue(newStringDate);
                                // Trigger a digest
                                $timeout(() => { scope.$apply(); }, 0);
                            }
                        });
                        element.datepicker("show");
                    }
                }
                element.dblclick(doubleClick);
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    // HTML side for npRichtext:
    //
    //      Must exist as a cell inside a table.
    //      All the attributes below in the td/div/textarea are MANDATORY.
    //
    //      <td style="height:100%; padding:0px">
    //          <div style="height:100% !important">
    //              <textarea style="display:block; margin:0; resize:none; height:100%; width:100%"
    //                        data-ng-model="modelLogin.variablelHtmlText"
    //                        data-np-richtext
    //                        data-np-disabled="someExpression"
    //                        (currently, the following two are not implemented: )
    //                        data-np-required='true'
    //                        data-np-validate="someFunc">  (someFunc takes a (string,entity) and returns NpTypes.ValidationResult)
    //              </textarea>
    //          </div>
    //      </td>
    // (the model is a string)

    interface IRichtextScope extends ng.IScope {}

    function npRichtext($timeout) {
        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return {
            // We will change the model via this directive
            require: '?ngModel',

            link: (scope:IRichtextScope, element, attrs, ngModel) => {
                if(!ngModel) return; // do nothing if no ng-model

                var component:NpTypes.NpRichtext = new NpTypes.NpRichtext(element, attrs, $timeout, scope, debugDirective);

                var disableExpression = attrs.npDisabled;

                // Instantiate the CLEditor magic thing
                var options = {
                    controls:     // controls to add to the toolbar
                      "bold italic underline strikethrough subscript superscript | font size " +
                      "style | color highlight removeformat | bullets numbering | outdent " +
                      "indent | alignleft center alignright justify | undo redo | " +
                      "rule image link unlink | cut copy paste pastetext",
                    width:'auto',
                    height:'100%',
                    editor:{change:null, unbind:null}
                };
                options.editor = element.cleditor(options)[0];

                function change(e) {
                    // Don't change the model if we are readOnly or disabled
                    if (element[0].readOnly === true || element[0].disabled === true)
                        return;
                    ngModel.$setViewValue(element.val().replace(/\s*$/, ''));
                    $timeout(() => { scope.$apply(); }, 0);
                }
                options.editor.change(logWrap(change));

                // Angular calls this when the model -> formatters -> $viewValue call chain completes,
                // so, just update the DOM:
                function render() {
                    element.val(ngModel.$viewValue).blur();
                    //options.editor.updateFrame();
                }
                ngModel.$render = logWrap(render);

                // when Angular detects the model changed, it will call the formatters...
                function formatter(modelValue:string) {
                    if (isVoid(component.uiModel) || isVoid(modelValue))
                        return '';
                    // ...and the first time this is called, we store the model value
                    // so that we can reset back to it if the validations fail.
                    component.safelySetOldValue(modelValue);
                    return modelValue;
                }
                ngModel.$formatters.push(logWrap(formatter));
                component.formatter = formatter;

                // Whenever the control's content changes, Angular will call this.
                // Check that the value is valid, and if so, return the updated model value.
                // In this case, we are always right (no validation):
                function parser(viewValue:string) {
                    component.clearAllErrors();
                    component.safelySetOldValue(viewValue);
                    return viewValue;
                }
                ngModel.$parsers.push(logWrap(parser));

                if (attrs.npDisabled !== undefined) {
                    scope.$watch(attrs.npDisabled, (isDisabled:boolean, oldVal:boolean) => {
                        (<any>options.editor).disable(isDisabled);
                        if (isDisabled) {
                            (<any>options.editor).$toolbar.hide();
                            (<any>options.editor).$frame.height(
                                (<any>options.editor).$frame.height() +
                                (<any>options.editor).$toolbar.height());
                        } else {
                            (<any>options.editor).$toolbar.show();
                            (<any>options.editor).refresh();
                        }
                    });
                }

                scope.$on('$destroy', () => {
                    component.formatter = null;
                    component.scope = null;
                    options.editor.unbind();
                    options.editor = null;
                    element.empty();
                });
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // HTML side for npBlob:
    //
    //      <span data-np-blob
    //          data-ng-model="row.entity._someBlob",   (of type NpTypes.UIBlobModel)
    //          data-np-factory="blobModelFactory" 
    //               (variable that is $eval-ed to obtain the NpTypes.NpBlob model
    //          data-np-blob
    //      />
    //
    // The model stores the selected 'filename' (string) and 'fileid' (number,
    // returned by the 'postURL'). The NpBlob provides the 
    // 'postURL', as well as a number of other dependencies,
    // e.g. the blobChangedCallback, which will be called
    // whenever the user selects a new file or clears the selection
    // via the minus button.
    //
    // Example instance of the factory that creates the model:
    //
    //    $scope.blobModelFactory = () => {
    //        return new NpTypes.NpBlob(
    //            // blobChangedCallback
    //            () => {                                  (the callback called when the blob is changed)
    //                console.log("blob changed!");
    //            },
    //            // downloadCallback
    //            ()=> {                                   (the callback that downloads the file via the fileid)
    //                alert("Will download via GET this: " + entity.fileid);
    //            },
    //            // isDisabled
    //            ()=>{                                    (callback returning whether the blob is disabled)
    //                return false;
    //            },
    //            // isInvisible
    //            ()=>{                                    (callback returning whether the blob is invisible)
    //                return false;
    //            },
    //            // postURL
    //            "/Customers/rest/upload",                (postURL that is sent the binary data)
    //            // isPlusEnabled                         (has plus button)
    //            true,
    //            // isMinusEnabled                        (has minus button)
    //            true,
    //            '(\.jpg|\.pdf)$',                        (regex for filename)
    //            1024);                                   (maximumSizeInKB)
    //    }

    interface IBlobScope extends ng.IScope {
        component: NpTypes.NpBlob;
        model: NpTypes.UIBlobModel;

        cbDisabled(): void;
        cbInvisible(): void;
        selectFile(): void;
        getURL():string;
        clearFile():void;
        handleFiles(evt: any);

        readFileProgress(evt: any);
        readFileFailed(evt: any);
        readFileCanceled(evt: any);

        uploadProgress(evt: any);
        uploadComplete(fileName: string, fileId:string);
        uploadFailed(evt:any);
        uploadCanceled(evt:any);
    }

    function npBlob($timeout, Plato:Services.INpDialogMaker) {
        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return {
            restrict: 'A',
            require: '?ngModel',
            replace: true,
            template: '<div class="npBlob" data-ng-hide="cbInvisible()">'  +
                          '<input style="display:none; text-overflow:ellipsis" type="file" />'                         +
                          '<div style="overflow: hidden" data-ng-show="!model.progressVisible">'                       +
                          '  <table style="white-space:nowrap; table-layout:fixed; width:100%">'                       +
                          '  <tr><td class="npBlobContent" style="overflow: hidden; text-overflow: ellipsis; padding:0 0 0 4px">' +
                          '   <a style="white-space:nowrap; cursor:pointer; overflow:hidden; '                         +
                          '             text-decoration:underline; text-overflow:ellipsis" '                           +
                          '      data-ng-href="{{getURL()}}" target="_blank" >{{model.filename}}</a>'                  +
                          '   </td><td style="width:30px; overflow: hidden; text-overflow: ellipsis; padding:0 0 0">'  +
                          '   <button data-ng-show="component.isPlusEnabled && !cbDisabled()" '                        +
                          '           style="width:30px; float:right" data-ng-click="selectFile()">+</button>'         +
                          '   </td><td style="width:30px; overflow: hidden; text-overflow: ellipsis; padding:0 0 0">'  +
                          '   <button data-ng-show="component.isMinusEnabled && !cbDisabled()" '                       +
                          '           style="width:30px; float:right" data-ng-click="clearFile()">-</button>&nbsp;'    +
                          '  </td></tr></table>'                                                                       +
                          '</div>'                                                                                     +
                          '<div style="overflow:hidden; text-overflow:ellipsis" data-ng-show="model.progressVisible">' +
                              '<div style="overflow: hidden" class="percent">Uploading...</div>'                       +
                              '<div class="progress-bar">'                                                             +
                                  '<div class="uploaded" ng-style="{\'width\': model.progress+\'%\'}"></div>'          +
                              '</div>'                                                                                 +
                          '</div>'                                                                                     +
                          '<div style="clear:both"></div>'                                                             +
                      '</div>',

            // For the file reader, we don't care about showing some ngModel fields in some binding.
            // If we did (e.g. the npSelect directive below needs to auto-select the current model's value)
            // we would then need to pass 'scope=true' below, to maintain access to our parent scope's model variables
            // (that is, the modelLogin.something stuff).
            //
            // However, for the blob, we will be using local model stuff only - i.e. we will have
            // a model (of type NpTypes.NpBlob) and we will work with that.

            scope: { model:'=ngModel' },

            link: (scope:IBlobScope, element, attrs, ngModel) => {
                if(!ngModel) return;

                var component:NpTypes.NpBlob = scope.$parent.$eval(attrs.npFactory);
                scope.component = component;

                scope.cbDisabled = ()=>{
                    if (!isVoid(scope.model))
                        return component.isDisabled(scope.model.entity);
                    else
                        return true;
		}
                scope.cbInvisible = ()=>{
                    if (!isVoid(scope.model))
                        return component.isInvisible(scope.model.entity);
                    else
                        return false;
		}

                var compiledRegExValidate;
                if (component.extensions !== '') {
                    var allowedExtensions = component.extensions.split('|');
                    var regExpExtensions = [];
                    allowedExtensions.forEach((ext:string) => {
                        regExpExtensions.push('(.*' + ext.replace(".", "\.") + ")");
                    });
                    compiledRegExValidate = new RegExp("^(" + regExpExtensions.join('|') + ")$", 'i'); // Ignore case
                } else {
                    compiledRegExValidate = new RegExp('^.*$');
                }

                $timeout(() => { scope.$apply(); }, 0);

                // Delegate the click on the '+' button to a click on the HTML input control
                scope.selectFile = () => {
                    $timeout(() => {
                        // From http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
                        var e = element.find('input');
                        e.wrap('<form>').closest('form').get(0).reset();
                        e.unwrap();
                        element.find('input').click();
                    },0);
                }

                function propagateModelChanges() {
                    scope.model.clearAllErrors();
                    component.blobChangedCallback(scope.model.entity);
                    // This is a blob - there are no parsers.
                    //ngModel.$setViewValue(scope.model);
                    $timeout(() => { scope.$apply(); }, 0);
                }

                scope.getURL = ():string => {
		    if (!isVoid(scope.model))
			return component.getURL(scope.model.entity);
		    else
			return "";
                }

                // A click on the '-' button clears the model
                scope.clearFile = () => {
                    scope.model.filename = null;
                    scope.model.fileid = null;
                    // From http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
                    var e = element.find('input');
                    e.wrap('<form>').closest('form').get(0).reset();
                    e.unwrap();
                    propagateModelChanges();
                }

                // Angular calls this when the model -> formatters -> $viewValue call chain completes.
                // In this case, we do nothing (no DOM manipulation necessary)
                ngModel.$render = () => {};

                scope.handleFiles = (evt) => {
                    var files = evt.target.files;

                    if (files.length>1) {
                        // For now, multiple files are disabled.
                        scope.$apply(() => {
                            scope.model.filename = null;
                            scope.model.value = null;
                            // From http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
                            var e = element.find('input');
                            e.wrap('<form>').closest('form').get(0).reset();
                            e.unwrap();
                            propagateModelChanges();
                        });
                    } else {
                        var file = files[0];
                        scope.model.progress = 0;
                        if (!compiledRegExValidate.test(file.name)) {
                            scope.$apply(() => {
                                messageBox(
                                    <NpTypes.IApplicationScope>scope.$parent,
                                    Plato,
                                    "MessageBox_Blob_FileType_Title",
                                    (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("FieldValidation_Blob_FileTypeMsg") + " " + component.extensions ,
                                    IconKind.ERROR, 
                                    [
                                        new Tuple2("OK", () => {})
                                    ], 0, 0);
                            });
                            return;
                        }
                        var maximumSizeInKB: number = component.getMaximumSizeInKB(scope.model.entity);
                        if (file.size / 1024.0 > maximumSizeInKB) {
                            scope.$apply(() => {
                                messageBox(
                                    <NpTypes.IApplicationScope>scope.$parent,
                                    Plato,
                                    "MessageBox_Blob_FileSize_Title",
                                    (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("FieldValidation_Blob_FileSizeMsg(\"" + maximumSizeInKB + "\")"),
                                    IconKind.ERROR, 
                                    [
                                        new Tuple2("OK", () => {})
                                    ], 0, 0);
                            });
                            return;
                        }

                        var fr = new FileReader();
                        fr.addEventListener("progress", scope.readFileProgress, false);
                        fr.addEventListener("load", readFileComplete, false);
                        fr.addEventListener("error", scope.readFileFailed, false);
                        fr.addEventListener("abort", scope.readFileCanceled, false);

                        fr.readAsArrayBuffer(file);
                        scope.$apply(() => {
                            scope.model.progressVisible = true;
                        });

                        function readFileComplete(evt: any) {
                            var fd = new FormData();
                            var blob = new Blob([fr.result], { type: "application/octet-stream" });
                            fd.append("uploadedFile", blob);

                            var xhr = new XMLHttpRequest();
                            xhr.upload.addEventListener("progress", scope.uploadProgress, false);
                            xhr.addEventListener("error", scope.uploadFailed, false);
                            xhr.addEventListener("abort", scope.uploadCanceled, false);
                            xhr.onreadystatechange = function () {
                                if (this.readyState == 4) {
                                    if (this.status == 200) {
                                        scope.uploadComplete(file.name, this.responseText);
                                    } else {
                                        messageBox(
                                            <NpTypes.IApplicationScope>scope.$parent,
                                            Plato,
                                            "MessageBox_Error_Title",
                                            (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("BlobUploadErrorMsg") + " '" + file.name + "'. (" +
                                            (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage(this.responseText) + ")",
                                            IconKind.ERROR,
                                            [
                                                new Tuple2("OK", () => { })
                                            ], 0, 0);
                                        scope.$apply(() => {
                                            scope.model.progressVisible = false;
                                        })
                                    }
                                } 
                            };
                            xhr.open("POST", component.postURL);
                            xhr.send(fd);
                        }

                    }
                }
                $(element).find('input').bind('change', scope.handleFiles);

                function jobProgress(evt:any, startValue: number, maxValue: number): void {
                    if (evt.lengthComputable) {
                        scope.model.progress = Math.round(startValue * 1.0 + (evt.loaded * (maxValue - startValue)*1.0 / evt.total));
                    } else {
                        scope.model.progress = 0;
                    }
                    scope.$digest();
                }
                scope.readFileProgress = function (evt) {
                    jobProgress(evt, 0, 10);
                }
                scope.readFileFailed = function (evt) {
                    error("readFileFailed called...");
                    console.log(evt);
                    scope.$apply(() => {
                        scope.model.progressVisible = false;
                    })
                    messageBox(
                        <NpTypes.IApplicationScope>scope.$parent,
                        Plato,
                        "MessageBox_Error_Title",
                        (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("BlobReadFileErrorMsg"),
                        IconKind.ERROR,
                        [
                            new Tuple2("OK", () => { })
                        ], 0, 0);

                }

                scope.readFileCanceled = function (evt) {
                    error("readFileCanceled called...");
                    console.log(evt);
                    scope.$apply(() => {
                        scope.model.progressVisible = false;
                    })
                    messageBox(
                        <NpTypes.IApplicationScope>scope.$parent,
                        Plato,
                        "MessageBox_Error_Title",
                        (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("BlobReadFileCanceledMsg"),
                        IconKind.ERROR,
                        [
                            new Tuple2("OK", () => { })
                        ], 0, 0);
                }


                scope.uploadProgress = function (evt) {
                    jobProgress(evt, 10, 95);
                }
                scope.uploadComplete = function (fileName, fileId) {
                    scope.model.filename = fileName;
                    scope.model.progress = 100;
                    scope.$digest();
                    scope.model.progressVisible = false;
                    scope.$digest();
                    scope.model.fileid = fileId;//evt.target.responseText;
                    propagateModelChanges();
                }

                scope.uploadFailed = function(evt) {
                    error("uploadFailed called...");
                    console.log(evt);
                    scope.$apply(() => {
                        scope.model.progressVisible = false;
                    })
                    messageBox(
                        <NpTypes.IApplicationScope>scope.$parent,
                        Plato,
                        "MessageBox_Error_Title",
                        (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("BlobUploadFailedMsg"),
                        IconKind.ERROR, 
                        [
                            new Tuple2("OK", () => {})
                        ], 0, 0);

                }

                scope.uploadCanceled = function(evt) {
                    error("uploadCanceled called...");
                    console.log(evt);
                    scope.$apply(() => {
                        scope.model.progressVisible = false;
                    })
                    messageBox(
                        <NpTypes.IApplicationScope>scope.$parent,
                        Plato,
                        "MessageBox_Error_Title",
                        (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("BlobUploadFailedMsg"),
                        IconKind.ERROR, 
                    [
                        new Tuple2("OK", () => {})
                    ], 0, 0);
                }
/*
                Old version of the directive was storing the actual file bytes.
                I am keeping it here, in case I need to access the uploaded bytes
                from the Javascript side (I hope I won't).

                // Whenever the input control tells us the data changed, update the model
                $(element).find('input').bind('change', function(e) {
                    var element = e.target;
                    var options = {promise: undefined};

                    // If one file at least is selected...
                    if (element.files.length > 0) {
                        // Take its name, and...
                        var filename = element.files[0].name.slice(0);
                        // ... read its contents.
                        var reader = new FileReader();
                        reader.onload = function(e){
                            // The reading completed - update the model
                            // and clear any error popovers.
                            component.markAsGood();
                            var values = e.target.result;
                            // ...and update the model.
                            model.filename = filename;
                            model.fileid = values;
                            ngModel.$setViewValue(model);
                            // Trigger a digest.
                            scope.callback.call();
                            $timeout(function() { scope.$apply(); }, 0);
                        }
                        reader.onerror = function(e) {
                            component.showError(
                                ' Σφάλμα κατά την ανάγνωση του αρχείου "'+filename+'"');
                        }
                        reader.readAsDataURL(element.files[0]);
                    } else {
                        // No file was selected - emulate what the '-' button does.
                        model.filename = undefined;
                        model.fileid = undefined;
                        ngModel.$setViewValue(model);
                        scope.callback.call();
                        $timeout(function() { scope.$apply(); }, 0);
                    }
                });
*/
                scope.$on('$destroy', () => {
                    // Amazingly, this is called for all blobs that are members of
                    // grids, on every updateFlexibleLayout!
                    //
                    // I therefore can't clear the pointed entity (scope.model.entity)

                    //scope.component.isDisabled = null;
                    //scope.component.isInvisible = null;
                    //scope.component.getURL = null;
                    //scope.component.blobChangedCallback = null;
                    //scope.component = null;
                    //scope.uploadCanceled = null;
                    //scope.uploadFailed = null;
                    //scope.uploadComplete = null;
                    //scope.uploadProgress = null;
                    //scope.handleFiles = null;
                    //scope.clearFile = null;
                    //scope.getURL = null;
                    //scope.selectFile = null;
                    //scope.cbDisabled = null;
                    //scope.cbInvisible = null;
                    //scope.model = null;
                });
            }
        };
    }

    interface IFileUploadScope extends NpTypes.IApplicationScope {
        component: NpTypes.NpFileUpload;
        entity: NpTypes.IBaseEntity;

        getBtnLabel(): string;
        cbDisabled(): void;
        cbInvisible(): void;
        selectFile(): void;
        handleFiles(evt: any);
        showHelpButton(): boolean;
        helpButtonAction(): void;
    }

    function npFileUpload($timeout, Plato: Services.INpDialogMaker) {
        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return {
            restrict: 'A',
            replace: true,
            template: '<div data-ng-hide="cbInvisible()">' +
            '  <table class="buttonContainer">' +
            '  <tr><td style="padding:0px">' +
            '   <input style="display:none" type="file" />' +
            '   <button data-ng-disabled="cbDisabled()" ' +
            '           data-ng-click="selectFile()">{{getBtnLabel()}}</button>' +
            '   </td><td style="width:24px; padding:0 0 0 1px" data-ng-show="showHelpButton()"> ' +
            '   <button data-ng-click="helpButtonAction()">?</button>' +
            '  </td></tr></table>' +
            '</div>',
            // we want to isolate the scope of our component so as it can be reusable on app
            scope: { entity: '='},

            link: (scope: IFileUploadScope, element, attrs) => {
                var test = true;
                var component: NpTypes.NpFileUpload = scope.$parent.$eval(attrs.npFactory);
                scope.component = component;

                scope.getBtnLabel = () => {
                    if (!isVoid(scope.entity))
                        return component.getLabel(scope.entity);
                    else
                        return component.getLabel();
                }

                scope.cbDisabled = () => {
                    if (!isVoid(scope.entity))
                        return component.isDisabled(scope.entity);
                    else
                        return component.isDisabled();
                }
                scope.cbInvisible = () => {
                    if (!isVoid(scope.entity))
                        return component.isInvisible(scope.entity);
                    else
                        return component.isInvisible();
                }

                scope.showHelpButton = () => {
                    return !isVoid(component.helpButtonAction);
                }

                scope.helpButtonAction = () => {
                    component.helpButtonAction();
                }

                var compiledRegExValidate;
                if (component.extensions !== '') {
                    var allowedExtensions = component.extensions.split('|');
                    var regExpExtensions = [];
                    allowedExtensions.forEach((ext: string) => {
                        regExpExtensions.push('(.*' + ext.replace(".", "\.") + ")");
                    });
                    compiledRegExValidate = new RegExp("^(" + regExpExtensions.join('|') + ")$", 'i'); // Ignore case
                } else {
                    compiledRegExValidate = new RegExp('^.*$');
                }

                $timeout(() => { scope.$apply(); }, 0);

                // Delegate the click on the button to a click on the HTML input control
                scope.selectFile = () => {
                    if (scope.cbDisabled() || scope.cbInvisible()) {
                        return;
                    }
                    $timeout(() => {
                        // From http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
                        var e = element.find('input');
                        e.wrap('<form>').closest('form').get(0).reset();
                        e.unwrap();
                        element.find('input').click();
                    }, 0);
                }

                scope.handleFiles = (evt) => {
                    var files = evt.target.files;

                    if (files.length > 1) {
                        // For now, multiple files are disabled.
                        scope.$apply(() => {
                            // From http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
                            var e = element.find('input');
                            e.wrap('<form>').closest('form').get(0).reset();
                            e.unwrap();
                        });
                    } else {
                        var file = files[0];
                        if (!compiledRegExValidate.test(file.name)) {
                            scope.$apply(() => {
                                showErrorMessage(
                                    "MessageBox_Blob_FileType_Title",
                                    (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("FieldValidation_Blob_FileTypeMsg") + " " + component.extensions
                                );
                            });
                            return;
                        }
                        if (file.size / 1024.0 > component.maximumSizeInKB) {
                            scope.$apply(() => {
                                showErrorMessage(
                                    "MessageBox_Blob_FileSize_Title",
                                    (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("FieldValidation_Blob_FileSizeMsg(\"" + component.maximumSizeInKB + "\")"));
                            });
                            return;
                        }

                        var fr = new FileReader();
                        fr.addEventListener("load", readFileComplete, false);
                        fr.addEventListener("error", readFileFailed, false);
                        fr.addEventListener("abort", readFileCanceled, false);

                        fr.readAsArrayBuffer(file);

                        function readFileComplete(evt: any) {
                            var fd = new FormData();
                            var blob = new Blob([fr.result], { type: "application/octet-stream" });
                            var filename: string = file.name;
                            fd.append("uploadedFileData", blob);
                            fd.append("uploadedFileName", b64EncodeUnicode(filename));
                            if (!isVoid(scope.entity)) {
                                component.formDataAppendCallback(fd, scope.entity);
                            } else {
                                component.formDataAppendCallback(fd);
                            }

                            var xhr = new XMLHttpRequest();
                            xhr.addEventListener("error", uploadFailed, false);
                            xhr.addEventListener("abort", uploadCanceled, false);
                            xhr.onreadystatechange = function () {
                                if (this.readyState == 4) {
                                    if ((<NpTypes.IApplicationScope>scope.$parent).globals.nInFlightRequests > 0) (<NpTypes.IApplicationScope>scope.$parent).globals.nInFlightRequests--;
                                    if (this.status == 200) {
                                        uploadComplete(file.name, this.responseText);
                                    } else {
                                        showErrorMessage(
                                            "MessageBox_Error_Title",
                                            (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("BlobUploadErrorMsg") + " '" + file.name + "'. (" +
                                            (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage(this.responseText) + ")");
                                    }
                                }
                            };
                            Utils.showWaitWindow((<NpTypes.IApplicationScope>scope.$parent), Plato, $timeout, true);
                            xhr.open("POST", component.postURL);
                            xhr.send(fd);
                        }

                    }
                }
                $(element).find('input').bind('change', scope.handleFiles);

                function uploadComplete(fileName, uploadResponseText) {
                    if (!isVoid(scope.entity)) {
                        component.fileUpLoadedCallback(fileName, uploadResponseText, scope.entity);
                    } else {
                        component.fileUpLoadedCallback(fileName, uploadResponseText);
                    }
                }


                function readFileFailed (evt) {
                    error("readFileFailed called...");
                    console.log(evt);
                    showErrorMessage(
                        "MessageBox_Error_Title",
                        (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("BlobReadFileErrorMsg"));

                }

                function readFileCanceled (evt) {
                    error("readFileCanceled called...");
                    console.log(evt);
                    showErrorMessage(
                        "MessageBox_Error_Title",
                        (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("BlobReadFileCanceledMsg"));
                }

                function uploadFailed (evt) {
                    error("uploadFailed called...");
                    console.log(evt);
                    showErrorMessage(
                        "MessageBox_Error_Title",
                        (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("BlobUploadFailedMsg"));
                }

                function uploadCanceled (evt) {
                    error("uploadCanceled called...");
                    console.log(evt);
                    showErrorMessage(
                        "MessageBox_Error_Title",
                        (<NpTypes.IApplicationScope>scope.$parent).globals.getDynamicMessage("BlobUploadCanceledMsg"));
                }

                scope.$on('$destroy', () => {});

                function showErrorMessage(msgTitle: string, msgTxt: string) {
                    messageBox(<NpTypes.IApplicationScope>scope.$parent, Plato, msgTitle, msgTxt, IconKind.ERROR,
                        [
                            new Tuple2("OK", () => { })
                        ], 0, 0, "40em");
                }

            }
        };
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    // HTML side for npSelect:
    //
    //  <select class="mySelect" data-ng-model="modelLogin.variableAny"
    //          data-np-select
    //          data-np-required='true'
    //          data-np-validate="someFunc"   (someFunc takes a (any,entity) and returns NpTypes.ValidationResult)
    //          and
    //          data-ng-options:'x[1] as x[0] for x in [[&quot;no&quot;, 0],[&quot;yes&quot;, 1]]',
    //          or
    //          data-ng-options:'x as x.name for x in getOptions() track by x.id'>
    //
    //                (the 'any' in the npValidate 'someFunc' applies to the second member
    //                 of the ng-options tuples, or the type of 'x.id' in the second example)
    //  </select>
    //
    // (the model is whatever the values are - x[1] or x in the two examples above)

    interface ISelectScope extends NpTypes.IApplicationScope {}

    function npSelect($timeout) {

        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return {
            require: '?ngModel',

            link: (scope:ISelectScope, element, attrs, ngModel) => {
                if(!ngModel) return;

                var component:NpTypes.NpSelect = new NpTypes.NpSelect(element, attrs, $timeout, scope, debugDirective);

                // Angular calls this when the model -> formatters -> $viewValue call chain completes.
                function render() {
                    info("render(): $viewValue = " + ngModel.$viewValue);
                    //element.val(ngModel.$viewValue);
                }
                ngModel.$render = logWrap(render);

                // when Angular detects the model changed, it will call the formatters...
                function formatter(modelValue:any) {
                    // ...and the first time this is called, we store the model value
                    // so that we can reset back to it if the validations fail.
                    if (isVoid(modelValue)) {
                        modelValue = null;
                    }
                    component.safelySetOldValue(modelValue);
                    return modelValue;
                }
                ngModel.$formatters.push(logWrap(formatter));
                component.formatter = formatter;

                // Whenever the control's content changes, Angular will call this.
                // Check that the value is valid, and if so, return the updated model value.
                // Otherwise show a popover and return the last known good value.
                function parser(viewValue:any):any {
                    component.clearAllErrors();
                    if (isVoid(viewValue)) {
                        if (component.required) {
                            var requiredMsg = ' ' + component.getDynamicMessage("FieldValidation_RequiredMsg2");
                            component.addNewErrorMessage(requiredMsg, true);
                            return component.safelyGetOldValue(null);
                        } else
                            return null;
                    } else {
                        if (component.validationCallback !== undefined) {

                            var validationResult:NpTypes.ValidationResult =
                                scope[component.validationCallback](viewValue, component.entity);
                            if (!validationResult.isValid) {
                                // it fails the user check
                                info(viewValue + ' is invalid due to custom check');
                                component.addNewErrorMessage(validationResult.errorMessage, true);
                                return component.safelyGetOldValue(null);
                            }
                        }

                        // The input was solid, return the updated value for the model.
                        // (but first, clear any error popovers)
                        component.clearAllErrors();
                        component.safelySetOldValue(viewValue);
                        return viewValue;
                    }
                }
                ngModel.$parsers.push(logWrap(parser));
                element.focus(() => {
                    scope.globals.bDigestHackEnabled = false;
                    //console.log("Digest hack disabled");
                });
                element.blur(() => {
                    scope.globals.bDigestHackEnabled = true;
                    //console.log("Digest hack enabled");
                });
            }
        };
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    // HTML side for npLookup:
    //   <input name="someName"
    //          data-np-lookup="someFunc"    (someFunc is passed the new text value)
    //          data-np-source="modelCustomers.someTextVariable" 
    //          data-np-context="someContextVariable">
    // (the model - someTextVariable in the example above - is a string)

    interface ILookupScope extends ng.IScope {}

    function npLookup($timeout) {
        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return {
            link: (scope:ILookupScope, element, attrs) => {
                // Read the options passed in the directive
                var component:NpTypes.NpLookup = new NpTypes.NpLookup(element, attrs, $timeout, scope, debugDirective);

                scope.$watch(component.npSource, (newVal, oldVal) => {
                    var newViewValue = isVoid(component.userDefinedFormatter) ? newVal : component.userDefinedFormatter(newVal, scope.$eval(component.npContext));
                    element.val(newViewValue);
                });

                function blur(e) {
                    if (!component.attrs.readonly) {
                        var newText = isVoid(component.userDefinedParser) ? element.val() : component.userDefinedParser(element.val(), component.entity);
                        component.clearAllErrors();
                        $timeout(() => {
                            var source = scope.$eval(component.npSource);
                            if (typeof source === 'string')
                                source = source.replace(/\s*$/, '').replace(/^\s*/, '');
                            if (newText != source /*The source is not always string, but newText is always string, so we must compare only the value, not the type*/
                                    && (!isVoid(component.npLookup)) && component.npLookup !== '')
                                scope[component.npLookup](scope.$eval(component.npContext), newText, component);
                        }, 0);
                    }
                }
                element.blur(logWrap(blur));
            }
        }
    }

    // 
    // Link actions with keycodes in certain DOM scopes - used like this:
    //
    //  <div data-on-key-down="gridPageUp()" data-keys="[33]">
    //      <div data-on-key-down="gridPageDown()" data-keys="[34]">
    //          <div id="npCustomersGrid0" data-ng-grid="modelCustomers.customersGrid" class="gridStyle">
    //          </div>
    //      </div>
    //  </div>

    function onCustomKeyDown($cookies) {
        var debugDirective = false;
        var info = infoGeneric(debugDirective);
        var logWrap = logWrapGeneric(debugDirective);

        return function(scope, elm, attrs) {
            function applyKeyDown() {
                scope.$apply(attrs.onKeyDown);
            };

            var allowedKeys = scope.$eval(attrs.keys);
            elm.bind('keydown', (evt) => {
                /*
                var isLoggedIn = $cookies[
                    globals.appName.slice(0,1).toLowerCase() +
                    globals.appName.slice(1) +
                    '-session-id'] !== undefined;
                if (isLoggedIn)
                    return true;
                */
                if (!allowedKeys || allowedKeys.length == 0) {
                    applyKeyDown();
                    return false;
                } else {
                    var found = false;
                    _.each(allowedKeys, (key) => {
                        if (key == evt.which) {
                            applyKeyDown();
                            found = true;
                        }
                    });
                    if (found) {
                        return false;
                    }
                }
                return true;
            });
        };
    }

    function npFocusMe($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, model) {
                if (scope.$eval(attrs.npFocusMe)) {
                    $timeout(() => { element[0].focus(); });
                }
            }
        };
    }

    function npTabs($timeout) {
        return {
            restrict: 'A',
            link: function (scope:ng.IScope, element, attrs) {
                $timeout(() => {
                    var tabs = (<any>$(element)).tabs({
                        activate: function (event, ui) {
                            scope.$parent.$digest();
                        }
                        /*
                           DEPRECATED - this was too slow,
                           we now use other mechanisms to update only
                           the grids that matter.

                        activate: function(event, ui) {
                            // jQuery UI tabs in combination with ng-grids,
                            // fail to resize the grid cell elements in hidden tabs (sometimes).
                            // Trigger a repaint in grids when any tab gets activated
                            scope.modelNavigation.epochOfLastResize++;
                        }
                        */
                    }).scrollabletab(); // To enable autoscrolled tabs

                    // For draggable tabs
                    //tabs.find( ".ui-tabs-nav" ).sortable({
                    //    axis: "x",
                    //    stop: function() {
                    //            tabs.tabs( "refresh" );
                    //    }
                    //});
                });

                //scope.$on('$destroy', function () {
                //    $(element).tabs('destroy');
                //});
            }
        };
    }

    function menuCompile($compile) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                scope.$watch(
                    (scope) => {
                        // watch the 'compile' expression for changes
                        return scope.$eval(attrs.menuCompile);
                    },
                    (value) => {
                        // when the 'compile' expression changes
                        // assign it into the current DOM
                        element.html(value);

                        // compile the new DOM and link it to the current scope.
                        $compile(element.contents())(scope);
                    }
                );
            }
        };
    }

    function npClick($timeout, $parse) {
        return {
            restrict: 'A',
            link: (scope:ng.IScope, element, attrs) => {
                var action = $parse(attrs['npClick']);
                var lastClickTimestamp:number;
                var delayedReactionHandler = (evt) => {
                    var currentTimestamp = (new Date()).getTime();
                    if (isVoid(lastClickTimestamp) ||
                        // When deleting rows in grid we don't want delay
                        element.hasClass("npGridDelete") ||
                        (lastClickTimestamp + Utils.timeoutForWaitDialogsAndInterclickDelaysInMilliseconds < currentTimestamp)
                    ) {
                        lastClickTimestamp = currentTimestamp;
                        $timeout(() => { action(scope, {$event:evt}); }, 0);
                    }
                };
                element.click(delayedReactionHandler);
            }
        }
    }

    export function addNewDirectives(application:ng.IModule)
    {
        application.directive('npTabs',       npTabs);
        application.directive('onKeyDown',    onCustomKeyDown);
        application.directive('menuCompile',  menuCompile);
        application.directive('npCheckbox', npCheckbox);
        application.directive('npCheckboxNumber', npCheckboxNumber);
        application.directive('npText',       npText);
        application.directive('npNumber',     npNumber);
        application.directive('npDate',       npDate);
        application.directive('npRichtext',   npRichtext);
        application.directive('npBlob',       npBlob);
        application.directive('npFileUpload', npFileUpload);
        application.directive('npSelect',     npSelect);
        application.directive('npLookup',     npLookup);
        application.directive('npFocusMe',    npFocusMe);
        application.directive('npClick',      npClick);

        var extraDirectives = [ 'focus', 'blur' ];
        _.each(extraDirectives, (name) => {
            var directiveName = 'ng' + name[ 0 ].toUpperCase( ) + name.substr( 1 );

            application.directive(
                directiveName,
                ($parse, $timeout) => {
                    return (scope:ng.IScope, element, attr) => {
                        var fn = $parse(attr[directiveName]);
                        element.bind(name, (evt) => {
                            $timeout(
                                () => {
                                    fn(scope, {$event:evt});
                                }, 0);
                        });
                    };
                });
        });

    }

}
