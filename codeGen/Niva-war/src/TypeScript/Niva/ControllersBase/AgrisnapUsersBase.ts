/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/AgrisnapUsersBase.ts" />
/// <reference path="../EntitiesBase/AgrisnapUsersProducersBase.ts" />
/// <reference path="../EntitiesBase/ProducersBase.ts" />
/// <reference path="LovProducersBase.ts" />
/// <reference path="../Controllers/AgrisnapUsers.ts" />
module Controllers.AgrisnapUsers {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpAgrisnapUsers:ModelGrpAgrisnapUsers;
        modelGrpAgrisnapUsersProducers:ModelGrpAgrisnapUsersProducers;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
        onNewBtnAction(): void;
        onDeleteBtnAction():void;

    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            

            $scope.onNewBtnAction = () => { 
                self.onNewBtnAction();
            };
            
            $scope.onDeleteBtnAction = () => { 
                self.onDeleteBtnAction();
            };


            $timeout(function() {
                $('#AgrisnapUsers').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "AgrisnapUsers";
        }
    
        public _getPageTitle(): string {
            return "Agrisnap Users";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpAgrisnapUsers.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpAgrisnapUsers.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpAgrisnapUsersProducers.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpAgrisnapUsers.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpAgrisnapUsersProducers.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpAgrisnapUsers.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpAgrisnapUsers.controller,
                    self.model.modelGrpAgrisnapUsersProducers.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
            if (bForDelete) {
                pageChanges = pageChanges.concat(self.model.modelGrpAgrisnapUsers.controller.getChangesToCommitForDeletion());
                pageChanges = pageChanges.concat(self.model.modelGrpAgrisnapUsers.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpAgrisnapUsersProducers.controller.getDeleteChangesToCommit());
            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpAgrisnapUsers.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpAgrisnapUsersProducers.controller.getChangesToCommit());
            }
            
            var hasAgrisnapUsers = pageChanges.some(change => change.entityName === "AgrisnapUsers")
            if (!hasAgrisnapUsers) {
                var validateEntity = new ChangeToCommit(ChangeStatus.UPDATE, 0, "AgrisnapUsers", self.model.modelGrpAgrisnapUsers.controller.Current);
                pageChanges = pageChanges.concat(validateEntity);
            }
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_AgrisnapUsers"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpAgrisnapUsers.controller.cleanUpAfterSave();
            self.model.modelGrpAgrisnapUsersProducers.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;

                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.AgrisnapUsers>>self.$scope.getCurrentPageModel();
                        var newAgrisnapUsersId = response.newEntitiesIds.firstOrNull(x => x.entityName === 'AgrisnapUsers');
                        if (!isVoid(newAgrisnapUsersId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.AgrisnapUsers.CreateById(newAgrisnapUsersId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        } else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }

                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }

        public getInitialEntity(): Entities.AgrisnapUsers {
            if (this.shownAsDialog()) {
                return <Entities.AgrisnapUsers>this.dialogSelectedEntity();
            } else {
                var breadCrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.AgrisnapUsers>>this.$scope.getPageModelByURL('/AgrisnapUsers');
                if (isVoid(breadCrumbStepModel)) {
                    return null;
                } else {
                    return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                }
            }
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


        public onNewBtnAction(): void {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => { 
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                self.addNewRecord();
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {self.addNewRecord();}),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                self.addNewRecord();
            }
        }

        public addNewRecord(): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            self.model.modelGrpAgrisnapUsers.controller.cleanUpAfterSave();
            self.model.modelGrpAgrisnapUsersProducers.controller.cleanUpAfterSave();
            self.model.modelGrpAgrisnapUsers.controller.createNewEntityAndAddToUI();
        }

        public onDeleteBtnAction():void {
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, 
                [
                    new Tuple2("MessageBox_Button_Yes", () => {self.deleteRecord();}),
                    new Tuple2("MessageBox_Button_No", () => {})
                ], 1,1);
        }

        public deleteRecord(): void {
            var self = this;
            if (self.Mode === EditMode.NEW) {
                console.log("Delete pressed in a form while being in new mode!");
                return;
            }
            NpTypes.AlertMessage.clearAlerts(self.model);
            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData(true);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
            });
        }
        
        public get Mode(): EditMode {
            var self = this;
            if (isVoid(self.model.modelGrpAgrisnapUsers) || isVoid(self.model.modelGrpAgrisnapUsers.controller))
                return EditMode.NEW;
            return self.model.modelGrpAgrisnapUsers.controller.Mode;
        }

        public _newIsDisabled(): boolean {
            var self = this;
            if (isVoid(self.model.modelGrpAgrisnapUsers) || isVoid(self.model.modelGrpAgrisnapUsers.controller))
                return true;
            return self.model.modelGrpAgrisnapUsers.controller._newIsDisabled();
        }
        public _deleteIsDisabled(): boolean {
            var self = this;
            if (self.Mode === EditMode.NEW)
                return true;
            if (isVoid(self.model.modelGrpAgrisnapUsers) || isVoid(self.model.modelGrpAgrisnapUsers.controller))
                return true;
            return self.model.modelGrpAgrisnapUsers.controller._deleteIsDisabled(self.model.modelGrpAgrisnapUsers.controller.Current);
        }


    }


    

    // GROUP GrpAgrisnapUsers

    export class ModelGrpAgrisnapUsersBase extends Controllers.AbstractGroupFormModel {
        modelGrpAgrisnapUsersProducers:ModelGrpAgrisnapUsersProducers;
        controller: ControllerGrpAgrisnapUsers;
        public get agrisnapName():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.AgrisnapUsers>this.selectedEntities[0]).agrisnapName;
        }

        public set agrisnapName(agrisnapName_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.AgrisnapUsers>this.selectedEntities[0]).agrisnapName = agrisnapName_newVal;
        }

        public get _agrisnapName():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agrisnapName;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.AgrisnapUsers>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.AgrisnapUsers>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get agrisnapUsersId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.AgrisnapUsers>this.selectedEntities[0]).agrisnapUsersId;
        }

        public set agrisnapUsersId(agrisnapUsersId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.AgrisnapUsers>this.selectedEntities[0]).agrisnapUsersId = agrisnapUsersId_newVal;
        }

        public get _agrisnapUsersId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agrisnapUsersId;
        }

        public get agrisnapUid():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.AgrisnapUsers>this.selectedEntities[0]).agrisnapUid;
        }

        public set agrisnapUid(agrisnapUid_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.AgrisnapUsers>this.selectedEntities[0]).agrisnapUid = agrisnapUid_newVal;
        }

        public get _agrisnapUid():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agrisnapUid;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpAgrisnapUsers) { super($scope); }
    }



    export interface IScopeGrpAgrisnapUsersBase extends Controllers.IAbstractFormGroupScope, IPageScope{
        globals: Globals;
        modelGrpAgrisnapUsers : ModelGrpAgrisnapUsers;
        GrpAgrisnapUsers_itm__agrisnapName_disabled(agrisnapUsers:Entities.AgrisnapUsers):boolean; 
        GrpAgrisnapUsers_itm__agrisnapUid_disabled(agrisnapUsers:Entities.AgrisnapUsers):boolean; 
        GrpAgrisnapUsers_0_disabled():boolean; 
        GrpAgrisnapUsers_0_invisible():boolean; 
        child_group_GrpAgrisnapUsersProducers_isInvisible():boolean; 

    }



    export class ControllerGrpAgrisnapUsersBase extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpAgrisnapUsers,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpAgrisnapUsers)
        {
            super($scope, $http, $timeout, Plato, model);
            var self:ControllerGrpAgrisnapUsersBase = this;
            model.controller = <ControllerGrpAgrisnapUsers>self;
            $scope.modelGrpAgrisnapUsers = self.model;

            var selectedEntity: Entities.AgrisnapUsers = this.$scope.pageModel.controller.getInitialEntity();
            if (isVoid(selectedEntity)) {
                self.createNewEntityAndAddToUI();
            } else {
                var clonedEntity = Entities.AgrisnapUsers.Create();
                clonedEntity.updateInstance(selectedEntity);
                $scope.modelGrpAgrisnapUsers.visibleEntities[0] = clonedEntity;
                $scope.modelGrpAgrisnapUsers.selectedEntities[0] = clonedEntity;
            } 
            $scope.GrpAgrisnapUsers_itm__agrisnapName_disabled = 
                (agrisnapUsers:Entities.AgrisnapUsers) => {
                    return self.GrpAgrisnapUsers_itm__agrisnapName_disabled(agrisnapUsers);
                };
            $scope.GrpAgrisnapUsers_itm__agrisnapUid_disabled = 
                (agrisnapUsers:Entities.AgrisnapUsers) => {
                    return self.GrpAgrisnapUsers_itm__agrisnapUid_disabled(agrisnapUsers);
                };
            $scope.GrpAgrisnapUsers_0_disabled = 
                () => {
                    return self.GrpAgrisnapUsers_0_disabled();
                };
            $scope.GrpAgrisnapUsers_0_invisible = 
                () => {
                    return self.GrpAgrisnapUsers_0_invisible();
                };
            $scope.child_group_GrpAgrisnapUsersProducers_isInvisible = 
                () => {
                    return self.child_group_GrpAgrisnapUsersProducers_isInvisible();
                };


            $scope.pageModel.modelGrpAgrisnapUsers = $scope.modelGrpAgrisnapUsers;


    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.AgrisnapUsers[] , oldVisible:Entities.AgrisnapUsers[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.AgrisnapUsers.agrisnapUsersProducersCollection = (agrisnapUsers, func) => {
                this.model.modelGrpAgrisnapUsersProducers.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, agrisnapUsers);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.AgrisnapUsers.agrisnapUsersProducersCollection = null;//(agrisnapUsers, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpAgrisnapUsers";
        }
        public get HtmlDivId(): string {
            return "AgrisnapUsers_ControllerGrpAgrisnapUsers";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: Entities.AgrisnapUsers) => 'Full Name',
                        false ,
                        (ent?: Entities.AgrisnapUsers) => ent.agrisnapName,  
                        (ent?: Entities.AgrisnapUsers) => ent._agrisnapName,  
                        (ent?: Entities.AgrisnapUsers) => true, 
                        (vl: string, ent?: Entities.AgrisnapUsers) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.AgrisnapUsers) => 'AgriSnap UserId',
                        false ,
                        (ent?: Entities.AgrisnapUsers) => ent.agrisnapUid,  
                        (ent?: Entities.AgrisnapUsers) => ent._agrisnapUid,  
                        (ent?: Entities.AgrisnapUsers) => true, 
                        (vl: string, ent?: Entities.AgrisnapUsers) => new NpTypes.ValidationResult(true, ""), 
                        undefined)
                ];
            }
            return this._items;
        }

        

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpAgrisnapUsers():ModelGrpAgrisnapUsers {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "AgrisnapUsers";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
        }

        public setCurrentFormPageBreadcrumpStepModel() {
            var self = this;
            if (self.$scope.pageModel.controller.shownAsDialog())
                return;

            var breadCrumbStepModel: IFormPageBreadcrumbStepModel<Entities.AgrisnapUsers> = { selectedEntity: null };
            if (!isVoid(self.Current)) {
                var selectedEntity: Entities.AgrisnapUsers = Entities.AgrisnapUsers.Create();
                selectedEntity.updateInstance(self.Current);
                breadCrumbStepModel.selectedEntity = selectedEntity;
            } else {
                breadCrumbStepModel.selectedEntity = null;
            }
            self.$scope.setCurrentPageModel(breadCrumbStepModel);
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.AgrisnapUsers.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.AgrisnapUsers {
            var self = this;
            return <Entities.AgrisnapUsers>self.$scope.modelGrpAgrisnapUsers.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.AgrisnapUsers, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }







        public constructEntity(): Entities.AgrisnapUsers {
            var self = this;
            var ret = new Entities.AgrisnapUsers(
                /*agrisnapName:string*/ null,
                /*rowVersion:number*/ null,
                /*agrisnapUsersId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*agrisnapUid:string*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.AgrisnapUsers = <Entities.AgrisnapUsers>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.$scope.modelGrpAgrisnapUsers.visibleEntities[0] = newEnt;
            self.$scope.modelGrpAgrisnapUsers.selectedEntities[0] = newEnt;
            return newEnt;

        }

        public cloneEntity(src: Entities.AgrisnapUsers): Entities.AgrisnapUsers {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.AgrisnapUsers, calledByParent: boolean= false): Entities.AgrisnapUsers {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.AgrisnapUsers.Create();
            ret.updateInstance(src);
            ret.agrisnapUsersId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.visibleEntities[0] = ret;
            this.model.selectedEntities[0] = ret;

            this.$timeout( () => {
                this.modelGrpAgrisnapUsers.modelGrpAgrisnapUsersProducers.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpAgrisnapUsers.modelGrpAgrisnapUsersProducers.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.AgrisnapUsers, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpAgrisnapUsers.modelGrpAgrisnapUsersProducers.controller.deleteAllEntitiesUnderParent(ent, () => {
                    super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpAgrisnapUsers.modelGrpAgrisnapUsersProducers.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public GrpAgrisnapUsers_itm__agrisnapName_disabled(agrisnapUsers:Entities.AgrisnapUsers):boolean {
            var self = this;
            if (agrisnapUsers === undefined || agrisnapUsers === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(agrisnapUsers, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpAgrisnapUsers_itm__agrisnapUid_disabled(agrisnapUsers:Entities.AgrisnapUsers):boolean {
            var self = this;
            if (agrisnapUsers === undefined || agrisnapUsers === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(agrisnapUsers, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpAgrisnapUsers_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpAgrisnapUsers_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = false;
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(agrisnapUsers:Entities.AgrisnapUsers):boolean  {
            var self = this;
            if (agrisnapUsers === null || agrisnapUsers === undefined || agrisnapUsers.getEntityName() !== "AgrisnapUsers")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(agrisnapUsers, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public child_group_GrpAgrisnapUsersProducers_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpAgrisnapUsersProducers === undefined) || (self.model.modelGrpAgrisnapUsersProducers.controller === undefined))
                return false;
            return self.model.modelGrpAgrisnapUsersProducers.controller._isInvisible()
        }

    }


    // GROUP GrpAgrisnapUsersProducers

    export class ModelGrpAgrisnapUsersProducersBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpAgrisnapUsersProducers;
        public get dteRelCreated():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).dteRelCreated;
        }

        public set dteRelCreated(dteRelCreated_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).dteRelCreated = dteRelCreated_newVal;
        }

        public get _dteRelCreated():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteRelCreated;
        }

        public get dteRelCanceled():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).dteRelCanceled;
        }

        public set dteRelCanceled(dteRelCanceled_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).dteRelCanceled = dteRelCanceled_newVal;
        }

        public get _dteRelCanceled():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteRelCanceled;
        }

        public get agrisnapUsersProducersId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).agrisnapUsersProducersId;
        }

        public set agrisnapUsersProducersId(agrisnapUsersProducersId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).agrisnapUsersProducersId = agrisnapUsersProducersId_newVal;
        }

        public get _agrisnapUsersProducersId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agrisnapUsersProducersId;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get agrisnapUsersId():Entities.AgrisnapUsers {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).agrisnapUsersId;
        }

        public set agrisnapUsersId(agrisnapUsersId_newVal:Entities.AgrisnapUsers) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).agrisnapUsersId = agrisnapUsersId_newVal;
        }

        public get _agrisnapUsersId():NpTypes.UIManyToOneModel<Entities.AgrisnapUsers> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agrisnapUsersId;
        }

        public get producersId():Entities.Producers {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).producersId;
        }

        public set producersId(producersId_newVal:Entities.Producers) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.AgrisnapUsersProducers>this.selectedEntities[0]).producersId = producersId_newVal;
        }

        public get _producersId():NpTypes.UIManyToOneModel<Entities.Producers> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._producersId;
        }

        _fsch_producersId:NpTypes.UIManyToOneModel<Entities.Producers> = new NpTypes.UIManyToOneModel<Entities.Producers>(undefined);
        public get fsch_producersId():Entities.Producers {
            return this._fsch_producersId.value;
        }
        public set fsch_producersId(vl:Entities.Producers) {
            this._fsch_producersId.value = vl;
        }
        _fsch_dteRelCreated:NpTypes.UIDateModel = new NpTypes.UIDateModel(undefined);
        public get fsch_dteRelCreated():Date {
            return this._fsch_dteRelCreated.value;
        }
        public set fsch_dteRelCreated(vl:Date) {
            this._fsch_dteRelCreated.value = vl;
        }
        _fsch_dteRelCanceled:NpTypes.UIDateModel = new NpTypes.UIDateModel(undefined);
        public get fsch_dteRelCanceled():Date {
            return this._fsch_dteRelCanceled.value;
        }
        public set fsch_dteRelCanceled(vl:Date) {
            this._fsch_dteRelCanceled.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpAgrisnapUsersProducers) { super($scope); }
    }



    export interface IScopeGrpAgrisnapUsersProducersBase extends Controllers.IAbstractTableGroupScope, IScopeGrpAgrisnapUsersBase{
        globals: Globals;
        modelGrpAgrisnapUsersProducers : ModelGrpAgrisnapUsersProducers;
        _disabled():boolean; 
        _invisible():boolean; 
        GrpAgrisnapUsersProducers_srchr__fsch_producersId_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean; 
        showLov_GrpAgrisnapUsersProducers_srchr__fsch_producersId():void; 
        GrpAgrisnapUsersProducers_srchr__fsch_dteRelCreated_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean; 
        GrpAgrisnapUsersProducers_srchr__fsch_dteRelCanceled_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean; 
        GrpAgrisnapUsersProducers_itm__producersId_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean; 
        showLov_GrpAgrisnapUsersProducers_itm__producersId(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):void; 
        GrpAgrisnapUsersProducers_itm__dteRelCreated_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean; 
        GrpAgrisnapUsersProducers_itm__dteRelCanceled_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean; 

    }



    export class ControllerGrpAgrisnapUsersProducersBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpAgrisnapUsersProducers,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpAgrisnapUsersProducers)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpAgrisnapUsersProducersBase = this;
            model.controller = <ControllerGrpAgrisnapUsersProducers>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpAgrisnapUsersProducers = self.model;

            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.GrpAgrisnapUsersProducers_srchr__fsch_producersId_disabled = 
                (agrisnapUsersProducers:Entities.AgrisnapUsersProducers) => {
                    return self.GrpAgrisnapUsersProducers_srchr__fsch_producersId_disabled(agrisnapUsersProducers);
                };
            $scope.showLov_GrpAgrisnapUsersProducers_srchr__fsch_producersId = 
                () => {
                    $timeout( () => {        
                        self.showLov_GrpAgrisnapUsersProducers_srchr__fsch_producersId();
                    }, 0);
                };
            $scope.GrpAgrisnapUsersProducers_srchr__fsch_dteRelCreated_disabled = 
                (agrisnapUsersProducers:Entities.AgrisnapUsersProducers) => {
                    return self.GrpAgrisnapUsersProducers_srchr__fsch_dteRelCreated_disabled(agrisnapUsersProducers);
                };
            $scope.GrpAgrisnapUsersProducers_srchr__fsch_dteRelCanceled_disabled = 
                (agrisnapUsersProducers:Entities.AgrisnapUsersProducers) => {
                    return self.GrpAgrisnapUsersProducers_srchr__fsch_dteRelCanceled_disabled(agrisnapUsersProducers);
                };
            $scope.GrpAgrisnapUsersProducers_itm__producersId_disabled = 
                (agrisnapUsersProducers:Entities.AgrisnapUsersProducers) => {
                    return self.GrpAgrisnapUsersProducers_itm__producersId_disabled(agrisnapUsersProducers);
                };
            $scope.showLov_GrpAgrisnapUsersProducers_itm__producersId = 
                (agrisnapUsersProducers:Entities.AgrisnapUsersProducers) => {
                    $timeout( () => {        
                        self.showLov_GrpAgrisnapUsersProducers_itm__producersId(agrisnapUsersProducers);
                    }, 0);
                };
            $scope.GrpAgrisnapUsersProducers_itm__dteRelCreated_disabled = 
                (agrisnapUsersProducers:Entities.AgrisnapUsersProducers) => {
                    return self.GrpAgrisnapUsersProducers_itm__dteRelCreated_disabled(agrisnapUsersProducers);
                };
            $scope.GrpAgrisnapUsersProducers_itm__dteRelCanceled_disabled = 
                (agrisnapUsersProducers:Entities.AgrisnapUsersProducers) => {
                    return self.GrpAgrisnapUsersProducers_itm__dteRelCanceled_disabled(agrisnapUsersProducers);
                };


            $scope.pageModel.modelGrpAgrisnapUsersProducers = $scope.modelGrpAgrisnapUsersProducers;


            $scope.clearBtnAction = () => { 
                self.modelGrpAgrisnapUsersProducers.fsch_producersId = undefined;
                self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated = undefined;
                self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled = undefined;
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpAgrisnapUsers.modelGrpAgrisnapUsersProducers = $scope.modelGrpAgrisnapUsersProducers;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpAgrisnapUsers.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.AgrisnapUsersProducers[] , oldVisible:Entities.AgrisnapUsersProducers[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpAgrisnapUsersProducers";
        }
        public get HtmlDivId(): string {
            return "AgrisnapUsers_ControllerGrpAgrisnapUsersProducers";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new LovItem (
                        (ent?: NpTypes.IBaseEntity) => 'Producer',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_producersId,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_producersId,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: Entities.Producers, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, "")),
                    new DateItem (
                        (ent?: NpTypes.IBaseEntity) => 'Relation Created',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_dteRelCreated,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_dteRelCreated,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: Date, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new DateItem (
                        (ent?: NpTypes.IBaseEntity) => 'Relation Ended',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_dteRelCanceled,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_dteRelCanceled,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: Date, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new LovItem (
                        (ent?: Entities.AgrisnapUsersProducers) => 'Producer Code',
                        false ,
                        (ent?: Entities.AgrisnapUsersProducers) => ent.producersId,  
                        (ent?: Entities.AgrisnapUsersProducers) => ent._producersId,  
                        (ent?: Entities.AgrisnapUsersProducers) => true, //isRequired
                        (vl: Entities.Producers, ent?: Entities.AgrisnapUsersProducers) => new NpTypes.ValidationResult(true, "")),
                    new DateItem (
                        (ent?: Entities.AgrisnapUsersProducers) => 'Relation Created',
                        false ,
                        (ent?: Entities.AgrisnapUsersProducers) => ent.dteRelCreated,  
                        (ent?: Entities.AgrisnapUsersProducers) => ent._dteRelCreated,  
                        (ent?: Entities.AgrisnapUsersProducers) => false, //isRequired
                        (vl: Date, ent?: Entities.AgrisnapUsersProducers) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new DateItem (
                        (ent?: Entities.AgrisnapUsersProducers) => 'Relation Ended',
                        false ,
                        (ent?: Entities.AgrisnapUsersProducers) => ent.dteRelCanceled,  
                        (ent?: Entities.AgrisnapUsersProducers) => ent._dteRelCanceled,  
                        (ent?: Entities.AgrisnapUsersProducers) => false, //isRequired
                        (vl: Date, ent?: Entities.AgrisnapUsersProducers) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined)
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Agrisnap Users and Producers";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'producersId.prodName', displayName:'getALString("Producer Code", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"), resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpAgrisnapUsersProducers_itm__producersId_disabled(row.entity) && showLov_GrpAgrisnapUsersProducers_itm__producersId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.producersId.prodName' data-np-ui-model='row.entity._producersId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpAgrisnapUsersProducers_itm__producersId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpAgrisnapUsersProducers_itm__producersId(row.entity)'   data-ng-disabled=\"GrpAgrisnapUsersProducers_itm__producersId_disabled(row.entity)\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteRelCreated', displayName:'getALString("Relation Created", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpAgrisnapUsersProducers_itm__dteRelCreated' data-ng-model='row.entity.dteRelCreated' data-np-ui-model='row.entity._dteRelCreated' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteRelCreated&quot;)' data-ng-readonly='GrpAgrisnapUsersProducers_itm__dteRelCreated_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteRelCanceled', displayName:'getALString("Relation Ended", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpAgrisnapUsersProducers_itm__dteRelCanceled' data-ng-model='row.entity.dteRelCanceled' data-np-ui-model='row.entity._dteRelCanceled' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteRelCanceled&quot;)' data-ng-readonly='GrpAgrisnapUsersProducers_itm__dteRelCanceled_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpAgrisnapUsersProducers():ModelGrpAgrisnapUsersProducers {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "AgrisnapUsersProducers";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.AgrisnapUsers): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.AgrisnapUsersProducers.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.agrisnapUsersId = this.Parent;
                } else {
                    x.agrisnapUsersId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.AgrisnapUsersProducers {
            var self = this;
            return <Entities.AgrisnapUsersProducers>self.$scope.modelGrpAgrisnapUsersProducers.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.AgrisnapUsersProducers, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpAgrisnapUsers.controller.isEntityLocked(<Entities.AgrisnapUsers>cur.agrisnapUsersId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.agrisnapUsersId)) {
                    paramData['agrisnapUsersId_agrisnapUsersId'] = self.Parent.agrisnapUsersId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated)) {
                    paramData['fsch_dteRelCreated'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled)) {
                    paramData['fsch_dteRelCanceled'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId)) {
                    paramData['fsch_producersId_producersId'] = self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "AgrisnapUsersProducers/findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.agrisnapUsersId)) {
                    paramData['agrisnapUsersId_agrisnapUsersId'] = self.Parent.agrisnapUsersId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated)) {
                    paramData['fsch_dteRelCreated'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled)) {
                    paramData['fsch_dteRelCanceled'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId)) {
                    paramData['fsch_producersId_producersId'] = self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "AgrisnapUsersProducers/findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.agrisnapUsersId)) {
                    paramData['agrisnapUsersId_agrisnapUsersId'] = self.Parent.agrisnapUsersId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated)) {
                    paramData['fsch_dteRelCreated'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled)) {
                    paramData['fsch_dteRelCanceled'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId)) {
                    paramData['fsch_producersId_producersId'] = self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "AgrisnapUsersProducers/findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.agrisnapUsersId)) {
                    paramData['agrisnapUsersId_agrisnapUsersId'] = self.Parent.agrisnapUsersId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated)) {
                    paramData['fsch_dteRelCreated'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled)) {
                    paramData['fsch_dteRelCanceled'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId)) {
                    paramData['fsch_producersId_producersId'] = self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.AgrisnapUsersProducers[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.AgrisnapUsersProducers[]) => void> = [];


        public getMergedItems(func: (items: Entities.AgrisnapUsersProducers[]) => void, bContinuousCaller:boolean=true, parent?: Entities.AgrisnapUsers, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.AgrisnapUsersProducers[]):void {
                var mergedEntities = <Entities.AgrisnapUsersProducers[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.AgrisnapUsersProducers>e.a).agrisnapUsersId)).
                        map(e => <Entities.AgrisnapUsersProducers>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "AgrisnapUsersProducers/findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['agrisnapUsersId_agrisnapUsersId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.AgrisnapUsersProducers[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.AgrisnapUsersProducers):boolean {
            if (this.Parent === undefined || x.agrisnapUsersId === undefined)
                return false;
            return x.agrisnapUsersId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.AgrisnapUsers {
            var self = this;
            return self.agrisnapUsersId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpAgrisnapUsers.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get agrisnapUsersId():Entities.AgrisnapUsers {
            var self = this;
            return <Entities.AgrisnapUsers>self.$scope.modelGrpAgrisnapUsers.selectedEntities[0];
        }









        public constructEntity(): Entities.AgrisnapUsersProducers {
            var self = this;
            var ret = new Entities.AgrisnapUsersProducers(
                /*dteRelCreated:Date*/ null,
                /*dteRelCanceled:Date*/ null,
                /*agrisnapUsersProducersId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*rowVersion:number*/ null,
                /*agrisnapUsersId:Entities.AgrisnapUsers*/ null,
                /*producersId:Entities.Producers*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.AgrisnapUsersProducers = <Entities.AgrisnapUsersProducers>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.agrisnapUsersId = self.agrisnapUsersId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.AgrisnapUsersProducers): Entities.AgrisnapUsersProducers {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.AgrisnapUsersProducers, calledByParent: boolean= false): Entities.AgrisnapUsersProducers {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.AgrisnapUsersProducers.Create();
            ret.updateInstance(src);
            ret.agrisnapUsersProducersId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.AgrisnapUsers, newParent:Entities.AgrisnapUsers) {
        
            this.model.totalItems = 0;

            this.getMergedItems(agrisnapUsersProducersList => {
                agrisnapUsersProducersList.forEach(agrisnapUsersProducers => {
                    var agrisnapUsersProducersCloned = this.cloneEntity_internal(agrisnapUsersProducers, true);
                    agrisnapUsersProducersCloned.agrisnapUsersId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(agrisnapUsers: Entities.AgrisnapUsers) {
        

            var self = this;
            var toBeDeleted:Array<Entities.AgrisnapUsersProducers> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var agrisnapUsersProducers = <Entities.AgrisnapUsersProducers>change.a
                if (agrisnapUsers.getKey() === agrisnapUsersProducers.agrisnapUsersId.getKey()) {
                    toBeDeleted.push(agrisnapUsersProducers);
                }
            }

            _.each(toBeDeleted, (x:Entities.AgrisnapUsersProducers) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(agrisnapUsers: Entities.AgrisnapUsers, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteAgrisnapUsersProducersList(agrisnapUsersProducersList: Entities.AgrisnapUsersProducers[]) {
                if (agrisnapUsersProducersList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = agrisnapUsersProducersList[0];
                    var tail = agrisnapUsersProducersList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteAgrisnapUsersProducersList(tail);
                    });
                }
            }


            this.getMergedItems(agrisnapUsersProducersList => {
                deleteAgrisnapUsersProducersList(agrisnapUsersProducersList);
            }, false, agrisnapUsers);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.AgrisnapUsersProducers, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpAgrisnapUsersProducers_srchr__fsch_producersId_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean {
            var self = this;


            return false;
        }
        cachedLovModel_GrpAgrisnapUsersProducers_srchr__fsch_producersId:ModelLovProducersBase;
        public showLov_GrpAgrisnapUsersProducers_srchr__fsch_producersId() {
            var self = this;
            var uimodel:NpTypes.IUIModel = self.modelGrpAgrisnapUsersProducers._fsch_producersId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Producers';
            dialogOptions.previousModel= self.cachedLovModel_GrpAgrisnapUsersProducers_srchr__fsch_producersId;
            dialogOptions.className = "ControllerLovProducers";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var producers1:Entities.Producers =   <Entities.Producers>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(producers1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(producers1) && producers1.isEqual(self.modelGrpAgrisnapUsersProducers.fsch_producersId))
                    return;
                self.modelGrpAgrisnapUsersProducers.fsch_producersId = producers1;
            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovProducersBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpAgrisnapUsersProducers_srchr__fsch_producersId = lovModel;
                    var wsPath = "Producers/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovProducersBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Producers/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['prodName'] = true;
            dialogOptions.shownCols['prodCode'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Producers.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpAgrisnapUsersProducers_srchr__fsch_dteRelCreated_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean {
            var self = this;


            return false;
        }
        public GrpAgrisnapUsersProducers_srchr__fsch_dteRelCanceled_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean {
            var self = this;


            return false;
        }
        public GrpAgrisnapUsersProducers_itm__producersId_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean {
            var self = this;
            if (agrisnapUsersProducers === undefined || agrisnapUsersProducers === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(agrisnapUsersProducers, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpAgrisnapUsersProducers_itm__producersId:ModelLovProducersBase;
        public showLov_GrpAgrisnapUsersProducers_itm__producersId(agrisnapUsersProducers:Entities.AgrisnapUsersProducers) {
            var self = this;
            if (agrisnapUsersProducers === undefined)
                return;
            var uimodel:NpTypes.IUIModel = agrisnapUsersProducers._producersId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Producers';
            dialogOptions.previousModel= self.cachedLovModel_GrpAgrisnapUsersProducers_itm__producersId;
            dialogOptions.className = "ControllerLovProducers";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var producers1:Entities.Producers =   <Entities.Producers>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(producers1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(producers1) && producers1.isEqual(agrisnapUsersProducers.producersId))
                    return;
                agrisnapUsersProducers.producersId = producers1;
                self.markEntityAsUpdated(agrisnapUsersProducers, 'GrpAgrisnapUsersProducers_itm__producersId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovProducersBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpAgrisnapUsersProducers_itm__producersId = lovModel;
                    var wsPath = "Producers/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovProducersBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Producers/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['prodName'] = true;
            dialogOptions.shownCols['prodCode'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Producers.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpAgrisnapUsersProducers_itm__dteRelCreated_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean {
            var self = this;
            if (agrisnapUsersProducers === undefined || agrisnapUsersProducers === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(agrisnapUsersProducers, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpAgrisnapUsersProducers_itm__dteRelCanceled_disabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean {
            var self = this;
            if (agrisnapUsersProducers === undefined || agrisnapUsersProducers === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(agrisnapUsersProducers, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpAgrisnapUsers_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpAgrisnapUsers_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(agrisnapUsersProducers:Entities.AgrisnapUsersProducers):boolean  {
            var self = this;
            if (agrisnapUsersProducers === null || agrisnapUsersProducers === undefined || agrisnapUsersProducers.getEntityName() !== "AgrisnapUsersProducers")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(agrisnapUsersProducers, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
