/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />
/// <reference path="../Controllers/LovParcelsIssues.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovParcelsIssuesBase = (function (_super) {
        __extends(ModelLovParcelsIssuesBase, _super);
        function ModelLovParcelsIssuesBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._fsch_ParcelsIssuesLov_dteCreated = new NpTypes.UIDateModel(undefined);
            this._fsch_ParcelsIssuesLov_status = new NpTypes.UINumberModel(undefined);
            this._fsch_ParcelsIssuesLov_dteStatusUpdate = new NpTypes.UIDateModel(undefined);
        }
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "fsch_ParcelsIssuesLov_dteCreated", {
            get: function () {
                return this._fsch_ParcelsIssuesLov_dteCreated.value;
            },
            set: function (vl) {
                this._fsch_ParcelsIssuesLov_dteCreated.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "fsch_ParcelsIssuesLov_status", {
            get: function () {
                return this._fsch_ParcelsIssuesLov_status.value;
            },
            set: function (vl) {
                this._fsch_ParcelsIssuesLov_status.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "fsch_ParcelsIssuesLov_dteStatusUpdate", {
            get: function () {
                return this._fsch_ParcelsIssuesLov_dteStatusUpdate.value;
            },
            set: function (vl) {
                this._fsch_ParcelsIssuesLov_dteStatusUpdate.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovParcelsIssuesBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelLovParcelsIssuesBase;
    })(Controllers.AbstractLovModel);
    Controllers.ModelLovParcelsIssuesBase = ModelLovParcelsIssuesBase;
    var ControllerLovParcelsIssuesBase = (function (_super) {
        __extends(ControllerLovParcelsIssuesBase, _super);
        function ControllerLovParcelsIssuesBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 5, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            $scope.clearBtnAction = function () {
                self.model.fsch_ParcelsIssuesLov_dteCreated = undefined;
                self.model.fsch_ParcelsIssuesLov_status = undefined;
                self.model.fsch_ParcelsIssuesLov_dteStatusUpdate = undefined;
                self.updateGrid();
            };
            $scope._disabled =
                function () {
                    return self._disabled();
                };
            $scope._invisible =
                function () {
                    return self._invisible();
                };
            $scope.LovParcelsIssues_fsch_ParcelsIssuesLov_dteCreated_disabled =
                function () {
                    return self.LovParcelsIssues_fsch_ParcelsIssuesLov_dteCreated_disabled();
                };
            $scope.LovParcelsIssues_fsch_ParcelsIssuesLov_status_disabled =
                function () {
                    return self.LovParcelsIssues_fsch_ParcelsIssuesLov_status_disabled();
                };
            $scope.LovParcelsIssues_fsch_ParcelsIssuesLov_dteStatusUpdate_disabled =
                function () {
                    return self.LovParcelsIssues_fsch_ParcelsIssuesLov_dteStatusUpdate_disabled();
                };
            $scope.modelLovParcelsIssues = model;
            self.updateUI();
        }
        ControllerLovParcelsIssuesBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerLovParcelsIssuesBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerLovParcelsIssues";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerLovParcelsIssuesBase.prototype, "HtmlDivId", {
            get: function () {
                return "ParcelsIssues_Id";
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovParcelsIssuesBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName);
            }
            var ret = [
                { cellClass: 'cellToolTip', field: 'dteCreated', displayName: 'getALString("dteCreated", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '40%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <input data-ng-model='row.entity.dteCreated' data-np-date='dummy' data-ng-readonly='true' data-np-format='dd-MM-yyyy' data-np-ui-model='row.entity._dteCreated' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'status', displayName: 'getALString("Κατάσταση", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '14%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.status' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'dteStatusUpdate', displayName: 'getALString("Ημερομηνία", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '40%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <input data-ng-model='row.entity.dteStatusUpdate' data-np-date='dummy' data-ng-readonly='true' data-np-format='dd-MM-yyyy' data-np-ui-model='row.entity._dteStatusUpdate' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter((function (cl) { return cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true; }));
            if (self.isMultiSelect()) {
                ret.splice(0, 0, { width: '22', cellTemplate: "<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined });
            }
            return ret;
        };
        ControllerLovParcelsIssuesBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.ParcelsIssues.fromJSONComplete(webResponse.data);
        };
        Object.defineProperty(ControllerLovParcelsIssuesBase.prototype, "modelLovParcelsIssues", {
            get: function () {
                var self = this;
                return self.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovParcelsIssuesBase.prototype.initializeModelWithPreviousValues = function () {
            var self = this;
            var prevModel = self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovParcelsIssues.fsch_ParcelsIssuesLov_dteCreated = prevModel.fsch_ParcelsIssuesLov_dteCreated;
                self.modelLovParcelsIssues.fsch_ParcelsIssuesLov_status = prevModel.fsch_ParcelsIssuesLov_status;
                self.modelLovParcelsIssues.fsch_ParcelsIssuesLov_dteStatusUpdate = prevModel.fsch_ParcelsIssuesLov_dteStatusUpdate;
            }
        };
        ControllerLovParcelsIssuesBase.prototype._disabled = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovParcelsIssuesBase.prototype._invisible = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovParcelsIssuesBase.prototype.LovParcelsIssues_fsch_ParcelsIssuesLov_dteCreated_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovParcelsIssuesBase.prototype.LovParcelsIssues_fsch_ParcelsIssuesLov_status_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovParcelsIssuesBase.prototype.LovParcelsIssues_fsch_ParcelsIssuesLov_dteStatusUpdate_disabled = function () {
            var self = this;
            return false;
        };
        return ControllerLovParcelsIssuesBase;
    })(Controllers.LovController);
    Controllers.ControllerLovParcelsIssuesBase = ControllerLovParcelsIssuesBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovParcelsIssuesBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
