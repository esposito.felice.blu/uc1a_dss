/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />
/// <reference path="../EntitiesBase/GpDecisionBase.ts" />
/// <reference path="../EntitiesBase/GpRequestsContextsBase.ts" />
/// <reference path="../EntitiesBase/GpUploadBase.ts" />
/// <reference path="../EntitiesBase/ParcelsIssuesActivitiesBase.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="LovParcelClasBase.ts" />
/// <reference path="../EntitiesBase/GpRequestsProducersBase.ts" />
/// <reference path="LovGpRequestsProducersBase.ts" />
/// <reference path="../Controllers/ParcelsIssues.ts" />
module Controllers.ParcelsIssues {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpParcelsIssues:ModelGrpParcelsIssues;
        modelGrpGpDecision:ModelGrpGpDecision;
        modelGrpGpRequestsContexts:ModelGrpGpRequestsContexts;
        modelGrpGpUpload:ModelGrpGpUpload;
        modelGrpParcelsIssuesActivities:ModelGrpParcelsIssuesActivities;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
        onNewBtnAction(): void;
        onDeleteBtnAction():void;

    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            

            $scope.onNewBtnAction = () => { 
                self.onNewBtnAction();
            };
            
            $scope.onDeleteBtnAction = () => { 
                self.onDeleteBtnAction();
            };


            $timeout(function() {
                $('#ParcelsIssues').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "ParcelsIssues";
        }
    
        public _getPageTitle(): string {
            return "ParcelsIssues";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpParcelsIssues.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpParcelsIssues.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpGpDecision.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpGpRequestsContexts.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpGpUpload.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpParcelsIssuesActivities.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpParcelsIssues.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpGpDecision.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpGpRequestsContexts.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpGpUpload.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpParcelsIssuesActivities.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpParcelsIssues.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpParcelsIssues.controller,
                    self.model.modelGrpGpDecision.controller,
                    self.model.modelGrpGpRequestsContexts.controller,
                    self.model.modelGrpGpUpload.controller,
                    self.model.modelGrpParcelsIssuesActivities.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
            if (bForDelete) {
                pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getChangesToCommitForDeletion());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpGpRequestsContexts.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpGpUpload.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssuesActivities.controller.getDeleteChangesToCommit());
            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpGpRequestsContexts.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpGpUpload.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssuesActivities.controller.getChangesToCommit());
            }
            
            var hasParcelsIssues = pageChanges.some(change => change.entityName === "ParcelsIssues")
            if (!hasParcelsIssues) {
                var validateEntity = new ChangeToCommit(ChangeStatus.UPDATE, 0, "ParcelsIssues", self.model.modelGrpParcelsIssues.controller.Current);
                pageChanges = pageChanges.concat(validateEntity);
            }
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_ParcelsIssues"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpParcelsIssues.controller.cleanUpAfterSave();
            self.model.modelGrpGpDecision.controller.cleanUpAfterSave();
            self.model.modelGrpGpRequestsContexts.controller.cleanUpAfterSave();
            self.model.modelGrpGpUpload.controller.cleanUpAfterSave();
            self.model.modelGrpParcelsIssuesActivities.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;

                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.ParcelsIssues>>self.$scope.getCurrentPageModel();
                        var newParcelsIssuesId = response.newEntitiesIds.firstOrNull(x => x.entityName === 'ParcelsIssues');
                        if (!isVoid(newParcelsIssuesId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.ParcelsIssues.CreateById(newParcelsIssuesId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        } else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }

                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }

        public getInitialEntity(): Entities.ParcelsIssues {
            if (this.shownAsDialog()) {
                return <Entities.ParcelsIssues>this.dialogSelectedEntity();
            } else {
                var breadCrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.ParcelsIssues>>this.$scope.getPageModelByURL('/ParcelsIssues');
                if (isVoid(breadCrumbStepModel)) {
                    return null;
                } else {
                    return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                }
            }
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


        public onNewBtnAction(): void {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => { 
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                self.addNewRecord();
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {self.addNewRecord();}),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                self.addNewRecord();
            }
        }

        public addNewRecord(): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            self.model.modelGrpParcelsIssues.controller.cleanUpAfterSave();
            self.model.modelGrpGpDecision.controller.cleanUpAfterSave();
            self.model.modelGrpGpRequestsContexts.controller.cleanUpAfterSave();
            self.model.modelGrpGpUpload.controller.cleanUpAfterSave();
            self.model.modelGrpParcelsIssuesActivities.controller.cleanUpAfterSave();
            self.model.modelGrpParcelsIssues.controller.createNewEntityAndAddToUI();
        }

        public onDeleteBtnAction():void {
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, 
                [
                    new Tuple2("MessageBox_Button_Yes", () => {self.deleteRecord();}),
                    new Tuple2("MessageBox_Button_No", () => {})
                ], 1,1);
        }

        public deleteRecord(): void {
            var self = this;
            if (self.Mode === EditMode.NEW) {
                console.log("Delete pressed in a form while being in new mode!");
                return;
            }
            NpTypes.AlertMessage.clearAlerts(self.model);
            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData(true);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
            });
        }
        
        public get Mode(): EditMode {
            var self = this;
            if (isVoid(self.model.modelGrpParcelsIssues) || isVoid(self.model.modelGrpParcelsIssues.controller))
                return EditMode.NEW;
            return self.model.modelGrpParcelsIssues.controller.Mode;
        }

        public _newIsDisabled(): boolean {
            var self = this;
            if (isVoid(self.model.modelGrpParcelsIssues) || isVoid(self.model.modelGrpParcelsIssues.controller))
                return true;
            return self.model.modelGrpParcelsIssues.controller._newIsDisabled();
        }
        public _deleteIsDisabled(): boolean {
            var self = this;
            if (self.Mode === EditMode.NEW)
                return true;
            if (isVoid(self.model.modelGrpParcelsIssues) || isVoid(self.model.modelGrpParcelsIssues.controller))
                return true;
            return self.model.modelGrpParcelsIssues.controller._deleteIsDisabled(self.model.modelGrpParcelsIssues.controller.Current);
        }


    }


    

    // GROUP GrpParcelsIssues

    export class ModelGrpParcelsIssuesBase extends Controllers.AbstractGroupFormModel {
        modelGrpGpDecision:ModelGrpGpDecision;
        modelGrpGpRequestsContexts:ModelGrpGpRequestsContexts;
        modelGrpParcelsIssuesActivities:ModelGrpParcelsIssuesActivities;
        controller: ControllerGrpParcelsIssues;
        public get parcelsIssuesId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).parcelsIssuesId;
        }

        public set parcelsIssuesId(parcelsIssuesId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).parcelsIssuesId = parcelsIssuesId_newVal;
        }

        public get _parcelsIssuesId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcelsIssuesId;
        }

        public get dteCreated():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).dteCreated;
        }

        public set dteCreated(dteCreated_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).dteCreated = dteCreated_newVal;
        }

        public get _dteCreated():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteCreated;
        }

        public get status():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).status;
        }

        public set status(status_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).status = status_newVal;
        }

        public get _status():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._status;
        }

        public get dteStatusUpdate():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).dteStatusUpdate;
        }

        public set dteStatusUpdate(dteStatusUpdate_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).dteStatusUpdate = dteStatusUpdate_newVal;
        }

        public get _dteStatusUpdate():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteStatusUpdate;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get typeOfIssue():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).typeOfIssue;
        }

        public set typeOfIssue(typeOfIssue_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).typeOfIssue = typeOfIssue_newVal;
        }

        public get _typeOfIssue():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._typeOfIssue;
        }

        public get active():boolean {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).active;
        }

        public set active(active_newVal:boolean) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).active = active_newVal;
        }

        public get _active():NpTypes.UIBoolModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._active;
        }

        public get pclaId():Entities.ParcelClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssues>this.selectedEntities[0]).pclaId;
        }

        public set pclaId(pclaId_newVal:Entities.ParcelClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssues>this.selectedEntities[0]).pclaId = pclaId_newVal;
        }

        public get _pclaId():NpTypes.UIManyToOneModel<Entities.ParcelClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._pclaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpParcelsIssues) { super($scope); }
    }



    export interface IScopeGrpParcelsIssuesBase extends Controllers.IAbstractFormGroupScope, IPageScope{
        globals: Globals;
        modelGrpParcelsIssues : ModelGrpParcelsIssues;
        GrpParcelsIssues_itm__dteCreated_disabled(parcelsIssues:Entities.ParcelsIssues):boolean; 
        GrpParcelsIssues_itm__status_disabled(parcelsIssues:Entities.ParcelsIssues):boolean; 
        GrpParcelsIssues_itm__dteStatusUpdate_disabled(parcelsIssues:Entities.ParcelsIssues):boolean; 
        GrpParcelsIssues_itm__pclaId_disabled(parcelsIssues:Entities.ParcelsIssues):boolean; 
        showLov_GrpParcelsIssues_itm__pclaId(parcelsIssues:Entities.ParcelsIssues):void; 
        GrpParcelsIssues_0_disabled():boolean; 
        GrpParcelsIssues_0_invisible():boolean; 
        child_group_GrpGpDecision_isInvisible():boolean; 
        child_group_GrpGpRequestsContexts_isInvisible():boolean; 
        child_group_GrpParcelsIssuesActivities_isInvisible():boolean; 

    }



    export class ControllerGrpParcelsIssuesBase extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpParcelsIssues,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpParcelsIssues)
        {
            super($scope, $http, $timeout, Plato, model);
            var self:ControllerGrpParcelsIssuesBase = this;
            model.controller = <ControllerGrpParcelsIssues>self;
            $scope.modelGrpParcelsIssues = self.model;

            var selectedEntity: Entities.ParcelsIssues = this.$scope.pageModel.controller.getInitialEntity();
            if (isVoid(selectedEntity)) {
                self.createNewEntityAndAddToUI();
            } else {
                var clonedEntity = Entities.ParcelsIssues.Create();
                clonedEntity.updateInstance(selectedEntity);
                $scope.modelGrpParcelsIssues.visibleEntities[0] = clonedEntity;
                $scope.modelGrpParcelsIssues.selectedEntities[0] = clonedEntity;
            } 
            $scope.GrpParcelsIssues_itm__dteCreated_disabled = 
                (parcelsIssues:Entities.ParcelsIssues) => {
                    return self.GrpParcelsIssues_itm__dteCreated_disabled(parcelsIssues);
                };
            $scope.GrpParcelsIssues_itm__status_disabled = 
                (parcelsIssues:Entities.ParcelsIssues) => {
                    return self.GrpParcelsIssues_itm__status_disabled(parcelsIssues);
                };
            $scope.GrpParcelsIssues_itm__dteStatusUpdate_disabled = 
                (parcelsIssues:Entities.ParcelsIssues) => {
                    return self.GrpParcelsIssues_itm__dteStatusUpdate_disabled(parcelsIssues);
                };
            $scope.GrpParcelsIssues_itm__pclaId_disabled = 
                (parcelsIssues:Entities.ParcelsIssues) => {
                    return self.GrpParcelsIssues_itm__pclaId_disabled(parcelsIssues);
                };
            $scope.showLov_GrpParcelsIssues_itm__pclaId = 
                (parcelsIssues:Entities.ParcelsIssues) => {
                    $timeout( () => {        
                        self.showLov_GrpParcelsIssues_itm__pclaId(parcelsIssues);
                    }, 0);
                };
            $scope.GrpParcelsIssues_0_disabled = 
                () => {
                    return self.GrpParcelsIssues_0_disabled();
                };
            $scope.GrpParcelsIssues_0_invisible = 
                () => {
                    return self.GrpParcelsIssues_0_invisible();
                };
            $scope.child_group_GrpGpDecision_isInvisible = 
                () => {
                    return self.child_group_GrpGpDecision_isInvisible();
                };
            $scope.child_group_GrpGpRequestsContexts_isInvisible = 
                () => {
                    return self.child_group_GrpGpRequestsContexts_isInvisible();
                };
            $scope.child_group_GrpParcelsIssuesActivities_isInvisible = 
                () => {
                    return self.child_group_GrpParcelsIssuesActivities_isInvisible();
                };


            $scope.pageModel.modelGrpParcelsIssues = $scope.modelGrpParcelsIssues;


    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelsIssues[] , oldVisible:Entities.ParcelsIssues[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.ParcelsIssues.gpDecisionCollection = (parcelsIssues, func) => {
                this.model.modelGrpGpDecision.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, parcelsIssues);
            }
            //bind entity child collection with child controller merged items
            Entities.ParcelsIssues.gpRequestsContextsCollection = (parcelsIssues, func) => {
                this.model.modelGrpGpRequestsContexts.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, parcelsIssues);
            }
            //bind entity child collection with child controller merged items
            Entities.ParcelsIssues.parcelsIssuesActivitiesCollection = (parcelsIssues, func) => {
                this.model.modelGrpParcelsIssuesActivities.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, parcelsIssues);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.ParcelsIssues.gpDecisionCollection = null;//(parcelsIssues, func) => { }
                //unbind entity child collection with child controller merged items
                Entities.ParcelsIssues.gpRequestsContextsCollection = null;//(parcelsIssues, func) => { }
                //unbind entity child collection with child controller merged items
                Entities.ParcelsIssues.parcelsIssuesActivitiesCollection = null;//(parcelsIssues, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpParcelsIssues";
        }
        public get HtmlDivId(): string {
            return "ParcelsIssues_ControllerGrpParcelsIssues";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new DateItem (
                        (ent?: Entities.ParcelsIssues) => 'dteCreated',
                        false ,
                        (ent?: Entities.ParcelsIssues) => ent.dteCreated,  
                        (ent?: Entities.ParcelsIssues) => ent._dteCreated,  
                        (ent?: Entities.ParcelsIssues) => false, //isRequired
                        (vl: Date, ent?: Entities.ParcelsIssues) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new NumberItem (
                        (ent?: Entities.ParcelsIssues) => 'Κατάσταση',
                        false ,
                        (ent?: Entities.ParcelsIssues) => ent.status,  
                        (ent?: Entities.ParcelsIssues) => ent._status,  
                        (ent?: Entities.ParcelsIssues) => false, //isRequired
                        (vl: number, ent?: Entities.ParcelsIssues) => new NpTypes.ValidationResult(true, ""), 
                        () => -32768,
                        () => 32767,
                        0),
                    new DateItem (
                        (ent?: Entities.ParcelsIssues) => 'Ημερομηνία',
                        false ,
                        (ent?: Entities.ParcelsIssues) => ent.dteStatusUpdate,  
                        (ent?: Entities.ParcelsIssues) => ent._dteStatusUpdate,  
                        (ent?: Entities.ParcelsIssues) => false, //isRequired
                        (vl: Date, ent?: Entities.ParcelsIssues) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new LovItem (
                        (ent?: Entities.ParcelsIssues) => 'pclaId',
                        false ,
                        (ent?: Entities.ParcelsIssues) => ent.pclaId,  
                        (ent?: Entities.ParcelsIssues) => ent._pclaId,  
                        (ent?: Entities.ParcelsIssues) => true, //isRequired
                        (vl: Entities.ParcelClas, ent?: Entities.ParcelsIssues) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpParcelsIssues():ModelGrpParcelsIssues {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "ParcelsIssues";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
        }

        public setCurrentFormPageBreadcrumpStepModel() {
            var self = this;
            if (self.$scope.pageModel.controller.shownAsDialog())
                return;

            var breadCrumbStepModel: IFormPageBreadcrumbStepModel<Entities.ParcelsIssues> = { selectedEntity: null };
            if (!isVoid(self.Current)) {
                var selectedEntity: Entities.ParcelsIssues = Entities.ParcelsIssues.Create();
                selectedEntity.updateInstance(self.Current);
                breadCrumbStepModel.selectedEntity = selectedEntity;
            } else {
                breadCrumbStepModel.selectedEntity = null;
            }
            self.$scope.setCurrentPageModel(breadCrumbStepModel);
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.ParcelsIssues.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.ParcelsIssues {
            var self = this;
            return <Entities.ParcelsIssues>self.$scope.modelGrpParcelsIssues.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.ParcelsIssues, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }







        public constructEntity(): Entities.ParcelsIssues {
            var self = this;
            var ret = new Entities.ParcelsIssues(
                /*parcelsIssuesId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*dteCreated:Date*/ null,
                /*status:number*/ null,
                /*dteStatusUpdate:Date*/ null,
                /*rowVersion:number*/ null,
                /*typeOfIssue:number*/ null,
                /*active:boolean*/ null,
                /*pclaId:Entities.ParcelClas*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.ParcelsIssues = <Entities.ParcelsIssues>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.$scope.modelGrpParcelsIssues.visibleEntities[0] = newEnt;
            self.$scope.modelGrpParcelsIssues.selectedEntities[0] = newEnt;
            return newEnt;

        }

        public cloneEntity(src: Entities.ParcelsIssues): Entities.ParcelsIssues {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.ParcelsIssues, calledByParent: boolean= false): Entities.ParcelsIssues {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.ParcelsIssues.Create();
            ret.updateInstance(src);
            ret.parcelsIssuesId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.visibleEntities[0] = ret;
            this.model.selectedEntities[0] = ret;

            this.$timeout( () => {
                this.modelGrpParcelsIssues.modelGrpGpDecision.controller.cloneAllEntitiesUnderParent(src, ret);
                this.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller.cloneAllEntitiesUnderParent(src, ret);
                this.modelGrpParcelsIssues.modelGrpParcelsIssuesActivities.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpParcelsIssues.modelGrpGpDecision.controller,
                    self.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller,
                    self.modelGrpParcelsIssues.modelGrpParcelsIssuesActivities.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.ParcelsIssues, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteAllEntitiesUnderParent(ent, () => {
                    self.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller.deleteAllEntitiesUnderParent(ent, () => {
                        self.modelGrpParcelsIssues.modelGrpParcelsIssuesActivities.controller.deleteAllEntitiesUnderParent(ent, () => {
                            super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                        });
                    });
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteNewEntitiesUnderParent(ent);
                    self.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller.deleteNewEntitiesUnderParent(ent);
                    self.modelGrpParcelsIssues.modelGrpParcelsIssuesActivities.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public GrpParcelsIssues_itm__dteCreated_disabled(parcelsIssues:Entities.ParcelsIssues):boolean {
            var self = this;
            if (parcelsIssues === undefined || parcelsIssues === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelsIssues, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpParcelsIssues_itm__status_disabled(parcelsIssues:Entities.ParcelsIssues):boolean {
            var self = this;
            if (parcelsIssues === undefined || parcelsIssues === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelsIssues, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpParcelsIssues_itm__dteStatusUpdate_disabled(parcelsIssues:Entities.ParcelsIssues):boolean {
            var self = this;
            if (parcelsIssues === undefined || parcelsIssues === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelsIssues, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpParcelsIssues_itm__pclaId_disabled(parcelsIssues:Entities.ParcelsIssues):boolean {
            var self = this;
            if (parcelsIssues === undefined || parcelsIssues === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelsIssues, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpParcelsIssues_itm__pclaId:ModelLovParcelClasBase;
        public showLov_GrpParcelsIssues_itm__pclaId(parcelsIssues:Entities.ParcelsIssues) {
            var self = this;
            if (parcelsIssues === undefined)
                return;
            var uimodel:NpTypes.IUIModel = parcelsIssues._pclaId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'ParcelClas';
            dialogOptions.previousModel= self.cachedLovModel_GrpParcelsIssues_itm__pclaId;
            dialogOptions.className = "ControllerLovParcelClas";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var parcelClas1:Entities.ParcelClas =   <Entities.ParcelClas>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(parcelClas1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(parcelClas1) && parcelClas1.isEqual(parcelsIssues.pclaId))
                    return;
                parcelsIssues.pclaId = parcelClas1;
                self.markEntityAsUpdated(parcelsIssues, 'GrpParcelsIssues_itm__pclaId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovParcelClasBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpParcelsIssues_itm__pclaId = lovModel;
                    var wsPath = "ParcelClas/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovParcelClasBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "ParcelClas/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['probPred'] = true;
            dialogOptions.shownCols['probPred2'] = true;
            dialogOptions.shownCols['prodCode'] = true;
            dialogOptions.shownCols['parcIdentifier'] = true;
            dialogOptions.shownCols['parcCode'] = true;
            dialogOptions.shownCols['geom4326'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/ParcelClas.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpParcelsIssues_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelsIssues_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = false;
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(parcelsIssues:Entities.ParcelsIssues):boolean  {
            var self = this;
            if (parcelsIssues === null || parcelsIssues === undefined || parcelsIssues.getEntityName() !== "ParcelsIssues")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(parcelsIssues, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public child_group_GrpGpDecision_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpGpDecision === undefined) || (self.model.modelGrpGpDecision.controller === undefined))
                return false;
            return self.model.modelGrpGpDecision.controller._isInvisible()
        }
        public child_group_GrpGpRequestsContexts_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpGpRequestsContexts === undefined) || (self.model.modelGrpGpRequestsContexts.controller === undefined))
                return false;
            return self.model.modelGrpGpRequestsContexts.controller._isInvisible()
        }
        public child_group_GrpParcelsIssuesActivities_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpParcelsIssuesActivities === undefined) || (self.model.modelGrpParcelsIssuesActivities.controller === undefined))
                return false;
            return self.model.modelGrpParcelsIssuesActivities.controller._isInvisible()
        }

    }


    // GROUP GrpGpDecision

    export class ModelGrpGpDecisionBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpGpDecision;
        public get gpDecisionsId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).gpDecisionsId;
        }

        public set gpDecisionsId(gpDecisionsId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).gpDecisionsId = gpDecisionsId_newVal;
        }

        public get _gpDecisionsId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._gpDecisionsId;
        }

        public get cropOk():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).cropOk;
        }

        public set cropOk(cropOk_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).cropOk = cropOk_newVal;
        }

        public get _cropOk():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cropOk;
        }

        public get landcoverOk():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).landcoverOk;
        }

        public set landcoverOk(landcoverOk_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).landcoverOk = landcoverOk_newVal;
        }

        public get _landcoverOk():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._landcoverOk;
        }

        public get dteInsert():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).dteInsert;
        }

        public set dteInsert(dteInsert_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).dteInsert = dteInsert_newVal;
        }

        public get _dteInsert():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteInsert;
        }

        public get usrInsert():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).usrInsert;
        }

        public set usrInsert(usrInsert_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).usrInsert = usrInsert_newVal;
        }

        public get _usrInsert():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._usrInsert;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get parcelsIssuesId():Entities.ParcelsIssues {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).parcelsIssuesId;
        }

        public set parcelsIssuesId(parcelsIssuesId_newVal:Entities.ParcelsIssues) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).parcelsIssuesId = parcelsIssuesId_newVal;
        }

        public get _parcelsIssuesId():NpTypes.UIManyToOneModel<Entities.ParcelsIssues> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcelsIssuesId;
        }

        public get cultId():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).cultId;
        }

        public set cultId(cultId_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).cultId = cultId_newVal;
        }

        public get _cultId():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultId;
        }

        public get cotyId():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpDecision>this.selectedEntities[0]).cotyId;
        }

        public set cotyId(cotyId_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpDecision>this.selectedEntities[0]).cotyId = cotyId_newVal;
        }

        public get _cotyId():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyId;
        }

        _fsch_cropOk:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_cropOk():number {
            return this._fsch_cropOk.value;
        }
        public set fsch_cropOk(vl:number) {
            this._fsch_cropOk.value = vl;
        }
        _fsch_landcoverOk:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_landcoverOk():number {
            return this._fsch_landcoverOk.value;
        }
        public set fsch_landcoverOk(vl:number) {
            this._fsch_landcoverOk.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpGpDecision) { super($scope); }
    }



    export interface IScopeGrpGpDecisionBase extends Controllers.IAbstractTableGroupScope, IScopeGrpParcelsIssuesBase{
        globals: Globals;
        modelGrpGpDecision : ModelGrpGpDecision;
        _disabled():boolean; 
        _invisible():boolean; 
        GrpGpDecision_srchr__fsch_cropOk_disabled(gpDecision:Entities.GpDecision):boolean; 
        GrpGpDecision_srchr__fsch_landcoverOk_disabled(gpDecision:Entities.GpDecision):boolean; 
        GrpGpDecision_itm__cropOk_disabled(gpDecision:Entities.GpDecision):boolean; 
        GrpGpDecision_itm__landcoverOk_disabled(gpDecision:Entities.GpDecision):boolean; 

    }



    export class ControllerGrpGpDecisionBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpGpDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpGpDecision)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpGpDecisionBase = this;
            model.controller = <ControllerGrpGpDecision>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpGpDecision = self.model;

            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.GrpGpDecision_srchr__fsch_cropOk_disabled = 
                (gpDecision:Entities.GpDecision) => {
                    return self.GrpGpDecision_srchr__fsch_cropOk_disabled(gpDecision);
                };
            $scope.GrpGpDecision_srchr__fsch_landcoverOk_disabled = 
                (gpDecision:Entities.GpDecision) => {
                    return self.GrpGpDecision_srchr__fsch_landcoverOk_disabled(gpDecision);
                };
            $scope.GrpGpDecision_itm__cropOk_disabled = 
                (gpDecision:Entities.GpDecision) => {
                    return self.GrpGpDecision_itm__cropOk_disabled(gpDecision);
                };
            $scope.GrpGpDecision_itm__landcoverOk_disabled = 
                (gpDecision:Entities.GpDecision) => {
                    return self.GrpGpDecision_itm__landcoverOk_disabled(gpDecision);
                };


            $scope.pageModel.modelGrpGpDecision = $scope.modelGrpGpDecision;


            $scope.clearBtnAction = () => { 
                self.modelGrpGpDecision.fsch_cropOk = undefined;
                self.modelGrpGpDecision.fsch_landcoverOk = undefined;
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpParcelsIssues.modelGrpGpDecision = $scope.modelGrpGpDecision;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpDecision[] , oldVisible:Entities.GpDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpGpDecision";
        }
        public get HtmlDivId(): string {
            return "ParcelsIssues_ControllerGrpGpDecision";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new NumberItem (
                        (ent?: NpTypes.IBaseEntity) => 'cropOk',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_cropOk,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_cropOk,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: number, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        () => -32768,
                        () => 32767,
                        0),
                    new NumberItem (
                        (ent?: NpTypes.IBaseEntity) => 'landcoverOk',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_landcoverOk,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_landcoverOk,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: number, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        () => -32768,
                        () => 32767,
                        0),
                    new NumberItem (
                        (ent?: Entities.GpDecision) => 'cropOk',
                        false ,
                        (ent?: Entities.GpDecision) => ent.cropOk,  
                        (ent?: Entities.GpDecision) => ent._cropOk,  
                        (ent?: Entities.GpDecision) => false, //isRequired
                        (vl: number, ent?: Entities.GpDecision) => new NpTypes.ValidationResult(true, ""), 
                        () => -32768,
                        () => 32767,
                        0),
                    new NumberItem (
                        (ent?: Entities.GpDecision) => 'landcoverOk',
                        false ,
                        (ent?: Entities.GpDecision) => ent.landcoverOk,  
                        (ent?: Entities.GpDecision) => ent._landcoverOk,  
                        (ent?: Entities.GpDecision) => false, //isRequired
                        (vl: number, ent?: Entities.GpDecision) => new NpTypes.ValidationResult(true, ""), 
                        () => -32768,
                        () => 32767,
                        0)
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "GpDecision";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'cropOk', displayName:'getALString("cropOk", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpDecision_itm__cropOk' data-ng-model='row.entity.cropOk' data-np-ui-model='row.entity._cropOk' data-ng-change='markEntityAsUpdated(row.entity,&quot;cropOk&quot;)' data-ng-readonly='GrpGpDecision_itm__cropOk_disabled(row.entity)' data-np-number='dummy' data-np-min='-32768' data-np-max='32767' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'landcoverOk', displayName:'getALString("landcoverOk", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpDecision_itm__landcoverOk' data-ng-model='row.entity.landcoverOk' data-np-ui-model='row.entity._landcoverOk' data-ng-change='markEntityAsUpdated(row.entity,&quot;landcoverOk&quot;)' data-ng-readonly='GrpGpDecision_itm__landcoverOk_disabled(row.entity)' data-np-number='dummy' data-np-min='-32768' data-np-max='32767' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpGpDecision():ModelGrpGpDecision {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "GpDecision";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.ParcelsIssues): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.GpDecision.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.parcelsIssuesId = this.Parent;
                } else {
                    x.parcelsIssuesId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.GpDecision {
            var self = this;
            return <Entities.GpDecision>self.$scope.modelGrpGpDecision.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.GpDecision, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(<Entities.ParcelsIssues>cur.parcelsIssuesId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cropOk)) {
                    paramData['fsch_cropOk'] = self.modelGrpGpDecision.fsch_cropOk;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_landcoverOk)) {
                    paramData['fsch_landcoverOk'] = self.modelGrpGpDecision.fsch_landcoverOk;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_ParcelsIssuesGrpGpDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cropOk)) {
                    paramData['fsch_cropOk'] = self.modelGrpGpDecision.fsch_cropOk;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_landcoverOk)) {
                    paramData['fsch_landcoverOk'] = self.modelGrpGpDecision.fsch_landcoverOk;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_ParcelsIssuesGrpGpDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cropOk)) {
                    paramData['fsch_cropOk'] = self.modelGrpGpDecision.fsch_cropOk;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_landcoverOk)) {
                    paramData['fsch_landcoverOk'] = self.modelGrpGpDecision.fsch_landcoverOk;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_ParcelsIssuesGrpGpDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cropOk)) {
                    paramData['fsch_cropOk'] = self.modelGrpGpDecision.fsch_cropOk;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_landcoverOk)) {
                    paramData['fsch_landcoverOk'] = self.modelGrpGpDecision.fsch_landcoverOk;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.GpDecision[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.GpDecision[]) => void> = [];


        public getMergedItems(func: (items: Entities.GpDecision[]) => void, bContinuousCaller:boolean=true, parent?: Entities.ParcelsIssues, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.GpDecision[]):void {
                var mergedEntities = <Entities.GpDecision[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.GpDecision>e.a).parcelsIssuesId)).
                        map(e => <Entities.GpDecision>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "GpDecision/findAllByCriteriaRange_ParcelsIssuesGrpGpDecision";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.GpDecision[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.GpDecision):boolean {
            if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                return false;
            return x.parcelsIssuesId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.ParcelsIssues {
            var self = this;
            return self.parcelsIssuesId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpParcelsIssues.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get parcelsIssuesId():Entities.ParcelsIssues {
            var self = this;
            return <Entities.ParcelsIssues>self.$scope.modelGrpParcelsIssues.selectedEntities[0];
        }









        public constructEntity(): Entities.GpDecision {
            var self = this;
            var ret = new Entities.GpDecision(
                /*gpDecisionsId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*cropOk:number*/ null,
                /*landcoverOk:number*/ null,
                /*dteInsert:Date*/ null,
                /*usrInsert:string*/ null,
                /*rowVersion:number*/ null,
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null,
                /*cultId:Entities.Cultivation*/ null,
                /*cotyId:Entities.CoverType*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.GpDecision = <Entities.GpDecision>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.parcelsIssuesId = self.parcelsIssuesId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.GpDecision): Entities.GpDecision {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.GpDecision, calledByParent: boolean= false): Entities.GpDecision {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.GpDecision.Create();
            ret.updateInstance(src);
            ret.gpDecisionsId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.ParcelsIssues, newParent:Entities.ParcelsIssues) {
        
            this.model.totalItems = 0;

            this.getMergedItems(gpDecisionList => {
                gpDecisionList.forEach(gpDecision => {
                    var gpDecisionCloned = this.cloneEntity_internal(gpDecision, true);
                    gpDecisionCloned.parcelsIssuesId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(parcelsIssues: Entities.ParcelsIssues) {
        

            var self = this;
            var toBeDeleted:Array<Entities.GpDecision> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var gpDecision = <Entities.GpDecision>change.a
                if (parcelsIssues.getKey() === gpDecision.parcelsIssuesId.getKey()) {
                    toBeDeleted.push(gpDecision);
                }
            }

            _.each(toBeDeleted, (x:Entities.GpDecision) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(parcelsIssues: Entities.ParcelsIssues, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteGpDecisionList(gpDecisionList: Entities.GpDecision[]) {
                if (gpDecisionList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = gpDecisionList[0];
                    var tail = gpDecisionList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteGpDecisionList(tail);
                    });
                }
            }


            this.getMergedItems(gpDecisionList => {
                deleteGpDecisionList(gpDecisionList);
            }, false, parcelsIssues);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.GpDecision, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpGpDecision_srchr__fsch_cropOk_disabled(gpDecision:Entities.GpDecision):boolean {
            var self = this;


            return false;
        }
        public GrpGpDecision_srchr__fsch_landcoverOk_disabled(gpDecision:Entities.GpDecision):boolean {
            var self = this;


            return false;
        }
        public GrpGpDecision_itm__cropOk_disabled(gpDecision:Entities.GpDecision):boolean {
            var self = this;
            if (gpDecision === undefined || gpDecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpDecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpDecision_itm__landcoverOk_disabled(gpDecision:Entities.GpDecision):boolean {
            var self = this;
            if (gpDecision === undefined || gpDecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpDecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelsIssues_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelsIssues_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(gpDecision:Entities.GpDecision):boolean  {
            var self = this;
            if (gpDecision === null || gpDecision === undefined || gpDecision.getEntityName() !== "GpDecision")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(gpDecision, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }


    // GROUP GrpGpRequestsContexts

    export class ModelGrpGpRequestsContextsBase extends Controllers.AbstractGroupTableModel {
        modelGrpGpUpload:ModelGrpGpUpload;
        controller: ControllerGrpGpRequestsContexts;
        public get gpRequestsContextsId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpRequestsContexts>this.selectedEntities[0]).gpRequestsContextsId;
        }

        public set gpRequestsContextsId(gpRequestsContextsId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpRequestsContexts>this.selectedEntities[0]).gpRequestsContextsId = gpRequestsContextsId_newVal;
        }

        public get _gpRequestsContextsId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._gpRequestsContextsId;
        }

        public get type():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpRequestsContexts>this.selectedEntities[0]).type;
        }

        public set type(type_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpRequestsContexts>this.selectedEntities[0]).type = type_newVal;
        }

        public get _type():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._type;
        }

        public get label():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpRequestsContexts>this.selectedEntities[0]).label;
        }

        public set label(label_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpRequestsContexts>this.selectedEntities[0]).label = label_newVal;
        }

        public get _label():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._label;
        }

        public get comment():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpRequestsContexts>this.selectedEntities[0]).comment;
        }

        public set comment(comment_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpRequestsContexts>this.selectedEntities[0]).comment = comment_newVal;
        }

        public get _comment():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comment;
        }

        public get geomHexewkb():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpRequestsContexts>this.selectedEntities[0]).geomHexewkb;
        }

        public set geomHexewkb(geomHexewkb_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpRequestsContexts>this.selectedEntities[0]).geomHexewkb = geomHexewkb_newVal;
        }

        public get _geomHexewkb():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._geomHexewkb;
        }

        public get referencepoint():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpRequestsContexts>this.selectedEntities[0]).referencepoint;
        }

        public set referencepoint(referencepoint_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpRequestsContexts>this.selectedEntities[0]).referencepoint = referencepoint_newVal;
        }

        public get _referencepoint():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._referencepoint;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpRequestsContexts>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpRequestsContexts>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get hash():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpRequestsContexts>this.selectedEntities[0]).hash;
        }

        public set hash(hash_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpRequestsContexts>this.selectedEntities[0]).hash = hash_newVal;
        }

        public get _hash():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._hash;
        }

        public get gpRequestsProducersId():Entities.GpRequestsProducers {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpRequestsContexts>this.selectedEntities[0]).gpRequestsProducersId;
        }

        public set gpRequestsProducersId(gpRequestsProducersId_newVal:Entities.GpRequestsProducers) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpRequestsContexts>this.selectedEntities[0]).gpRequestsProducersId = gpRequestsProducersId_newVal;
        }

        public get _gpRequestsProducersId():NpTypes.UIManyToOneModel<Entities.GpRequestsProducers> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._gpRequestsProducersId;
        }

        public get parcelsIssuesId():Entities.ParcelsIssues {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpRequestsContexts>this.selectedEntities[0]).parcelsIssuesId;
        }

        public set parcelsIssuesId(parcelsIssuesId_newVal:Entities.ParcelsIssues) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpRequestsContexts>this.selectedEntities[0]).parcelsIssuesId = parcelsIssuesId_newVal;
        }

        public get _parcelsIssuesId():NpTypes.UIManyToOneModel<Entities.ParcelsIssues> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcelsIssuesId;
        }

        _fsch_type:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_type():string {
            return this._fsch_type.value;
        }
        public set fsch_type(vl:string) {
            this._fsch_type.value = vl;
        }
        _fsch_label:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_label():string {
            return this._fsch_label.value;
        }
        public set fsch_label(vl:string) {
            this._fsch_label.value = vl;
        }
        _fsch_comment:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_comment():string {
            return this._fsch_comment.value;
        }
        public set fsch_comment(vl:string) {
            this._fsch_comment.value = vl;
        }
        _fsch_geomHexewkb:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_geomHexewkb():string {
            return this._fsch_geomHexewkb.value;
        }
        public set fsch_geomHexewkb(vl:string) {
            this._fsch_geomHexewkb.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpGpRequestsContexts) { super($scope); }
    }



    export interface IScopeGrpGpRequestsContextsBase extends Controllers.IAbstractTableGroupScope, IScopeGrpParcelsIssuesBase{
        globals: Globals;
        modelGrpGpRequestsContexts : ModelGrpGpRequestsContexts;
        _disabled():boolean; 
        _invisible():boolean; 
        GrpGpRequestsContexts_srchr__fsch_type_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        GrpGpRequestsContexts_srchr__fsch_label_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        GrpGpRequestsContexts_srchr__fsch_comment_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        GrpGpRequestsContexts_srchr__fsch_geomHexewkb_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        GrpGpRequestsContexts_itm__type_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        GrpGpRequestsContexts_itm__label_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        GrpGpRequestsContexts_itm__comment_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        GrpGpRequestsContexts_itm__geomHexewkb_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        GrpGpRequestsContexts_itm__referencepoint_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        GrpGpRequestsContexts_itm__hash_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean; 
        showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId(gpRequestsContexts:Entities.GpRequestsContexts):void; 
        child_group_GrpGpUpload_isInvisible():boolean; 

    }



    export class ControllerGrpGpRequestsContextsBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpGpRequestsContexts,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpGpRequestsContexts)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpGpRequestsContextsBase = this;
            model.controller = <ControllerGrpGpRequestsContexts>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpGpRequestsContexts = self.model;

            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.GrpGpRequestsContexts_srchr__fsch_type_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_srchr__fsch_type_disabled(gpRequestsContexts);
                };
            $scope.GrpGpRequestsContexts_srchr__fsch_label_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_srchr__fsch_label_disabled(gpRequestsContexts);
                };
            $scope.GrpGpRequestsContexts_srchr__fsch_comment_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_srchr__fsch_comment_disabled(gpRequestsContexts);
                };
            $scope.GrpGpRequestsContexts_srchr__fsch_geomHexewkb_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_srchr__fsch_geomHexewkb_disabled(gpRequestsContexts);
                };
            $scope.GrpGpRequestsContexts_itm__type_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_itm__type_disabled(gpRequestsContexts);
                };
            $scope.GrpGpRequestsContexts_itm__label_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_itm__label_disabled(gpRequestsContexts);
                };
            $scope.GrpGpRequestsContexts_itm__comment_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_itm__comment_disabled(gpRequestsContexts);
                };
            $scope.GrpGpRequestsContexts_itm__geomHexewkb_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_itm__geomHexewkb_disabled(gpRequestsContexts);
                };
            $scope.GrpGpRequestsContexts_itm__referencepoint_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_itm__referencepoint_disabled(gpRequestsContexts);
                };
            $scope.GrpGpRequestsContexts_itm__hash_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_itm__hash_disabled(gpRequestsContexts);
                };
            $scope.GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    return self.GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled(gpRequestsContexts);
                };
            $scope.showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId = 
                (gpRequestsContexts:Entities.GpRequestsContexts) => {
                    $timeout( () => {        
                        self.showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId(gpRequestsContexts);
                    }, 0);
                };
            $scope.child_group_GrpGpUpload_isInvisible = 
                () => {
                    return self.child_group_GrpGpUpload_isInvisible();
                };


            $scope.pageModel.modelGrpGpRequestsContexts = $scope.modelGrpGpRequestsContexts;


            $scope.clearBtnAction = () => { 
                self.modelGrpGpRequestsContexts.fsch_type = undefined;
                self.modelGrpGpRequestsContexts.fsch_label = undefined;
                self.modelGrpGpRequestsContexts.fsch_comment = undefined;
                self.modelGrpGpRequestsContexts.fsch_geomHexewkb = undefined;
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpParcelsIssues.modelGrpGpRequestsContexts = $scope.modelGrpGpRequestsContexts;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpRequestsContexts[] , oldVisible:Entities.GpRequestsContexts[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.GpRequestsContexts.gpUploadCollection = (gpRequestsContexts, func) => {
                this.model.modelGrpGpUpload.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, gpRequestsContexts);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.GpRequestsContexts.gpUploadCollection = null;//(gpRequestsContexts, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpGpRequestsContexts";
        }
        public get HtmlDivId(): string {
            return "ParcelsIssues_ControllerGrpGpRequestsContexts";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'type',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_type,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_type,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'label',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_label,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_label,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'comment',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_comment,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_comment,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'geomHexewkb',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_geomHexewkb,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_geomHexewkb,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.GpRequestsContexts) => 'type',
                        false ,
                        (ent?: Entities.GpRequestsContexts) => ent.type,  
                        (ent?: Entities.GpRequestsContexts) => ent._type,  
                        (ent?: Entities.GpRequestsContexts) => false, 
                        (vl: string, ent?: Entities.GpRequestsContexts) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.GpRequestsContexts) => 'label',
                        false ,
                        (ent?: Entities.GpRequestsContexts) => ent.label,  
                        (ent?: Entities.GpRequestsContexts) => ent._label,  
                        (ent?: Entities.GpRequestsContexts) => false, 
                        (vl: string, ent?: Entities.GpRequestsContexts) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.GpRequestsContexts) => 'comment',
                        false ,
                        (ent?: Entities.GpRequestsContexts) => ent.comment,  
                        (ent?: Entities.GpRequestsContexts) => ent._comment,  
                        (ent?: Entities.GpRequestsContexts) => false, 
                        (vl: string, ent?: Entities.GpRequestsContexts) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.GpRequestsContexts) => 'geomHexewkb',
                        false ,
                        (ent?: Entities.GpRequestsContexts) => ent.geomHexewkb,  
                        (ent?: Entities.GpRequestsContexts) => ent._geomHexewkb,  
                        (ent?: Entities.GpRequestsContexts) => false, 
                        (vl: string, ent?: Entities.GpRequestsContexts) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.GpRequestsContexts) => 'referencepoint',
                        false ,
                        (ent?: Entities.GpRequestsContexts) => ent.referencepoint,  
                        (ent?: Entities.GpRequestsContexts) => ent._referencepoint,  
                        (ent?: Entities.GpRequestsContexts) => false, 
                        (vl: string, ent?: Entities.GpRequestsContexts) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.GpRequestsContexts) => 'hash',
                        false ,
                        (ent?: Entities.GpRequestsContexts) => ent.hash,  
                        (ent?: Entities.GpRequestsContexts) => ent._hash,  
                        (ent?: Entities.GpRequestsContexts) => true, 
                        (vl: string, ent?: Entities.GpRequestsContexts) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new LovItem (
                        (ent?: Entities.GpRequestsContexts) => 'gpRequestsProducersId',
                        false ,
                        (ent?: Entities.GpRequestsContexts) => ent.gpRequestsProducersId,  
                        (ent?: Entities.GpRequestsContexts) => ent._gpRequestsProducersId,  
                        (ent?: Entities.GpRequestsContexts) => true, //isRequired
                        (vl: Entities.GpRequestsProducers, ent?: Entities.GpRequestsContexts) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "GpRequestsContexts";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'type', displayName:'getALString("type", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__type' data-ng-model='row.entity.type' data-np-ui-model='row.entity._type' data-ng-change='markEntityAsUpdated(row.entity,&quot;type&quot;)' data-ng-readonly='GrpGpRequestsContexts_itm__type_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'label', displayName:'getALString("label", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__label' data-ng-model='row.entity.label' data-np-ui-model='row.entity._label' data-ng-change='markEntityAsUpdated(row.entity,&quot;label&quot;)' data-ng-readonly='GrpGpRequestsContexts_itm__label_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comment', displayName:'getALString("comment", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__comment' data-ng-model='row.entity.comment' data-np-ui-model='row.entity._comment' data-ng-change='markEntityAsUpdated(row.entity,&quot;comment&quot;)' data-ng-readonly='GrpGpRequestsContexts_itm__comment_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'geomHexewkb', displayName:'getALString("geomHexewkb", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__geomHexewkb' data-ng-model='row.entity.geomHexewkb' data-np-ui-model='row.entity._geomHexewkb' data-ng-change='markEntityAsUpdated(row.entity,&quot;geomHexewkb&quot;)' data-ng-readonly='GrpGpRequestsContexts_itm__geomHexewkb_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'referencepoint', displayName:'getALString("referencepoint", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__referencepoint' data-ng-model='row.entity.referencepoint' data-np-ui-model='row.entity._referencepoint' data-ng-change='markEntityAsUpdated(row.entity,&quot;referencepoint&quot;)' data-ng-readonly='GrpGpRequestsContexts_itm__referencepoint_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'hash', displayName:'getALString("hash", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__hash' data-ng-model='row.entity.hash' data-np-ui-model='row.entity._hash' data-ng-change='markEntityAsUpdated(row.entity,&quot;hash&quot;)' data-np-required='true' data-ng-readonly='GrpGpRequestsContexts_itm__hash_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'gpRequestsProducersId.notes', displayName:'getALString("gpRequestsProducersId", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled(row.entity) && showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.gpRequestsProducersId.notes' data-np-ui-model='row.entity._gpRequestsProducersId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpGpRequestsContexts_itm__gpRequestsProducersId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId(row.entity)'   data-ng-disabled=\"GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled(row.entity)\"   />  \
                    </div> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpGpRequestsContexts():ModelGrpGpRequestsContexts {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "GpRequestsContexts";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.ParcelsIssues): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.GpRequestsContexts.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.parcelsIssuesId = this.Parent;
                } else {
                    x.parcelsIssuesId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.GpRequestsContexts {
            var self = this;
            return <Entities.GpRequestsContexts>self.$scope.modelGrpGpRequestsContexts.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.GpRequestsContexts, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(<Entities.ParcelsIssues>cur.parcelsIssuesId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpGpRequestsContexts.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_label)) {
                    paramData['fsch_label'] = self.modelGrpGpRequestsContexts.fsch_label;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_comment)) {
                    paramData['fsch_comment'] = self.modelGrpGpRequestsContexts.fsch_comment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_geomHexewkb)) {
                    paramData['fsch_geomHexewkb'] = self.modelGrpGpRequestsContexts.fsch_geomHexewkb;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpGpRequestsContexts.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_label)) {
                    paramData['fsch_label'] = self.modelGrpGpRequestsContexts.fsch_label;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_comment)) {
                    paramData['fsch_comment'] = self.modelGrpGpRequestsContexts.fsch_comment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_geomHexewkb)) {
                    paramData['fsch_geomHexewkb'] = self.modelGrpGpRequestsContexts.fsch_geomHexewkb;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpGpRequestsContexts.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_label)) {
                    paramData['fsch_label'] = self.modelGrpGpRequestsContexts.fsch_label;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_comment)) {
                    paramData['fsch_comment'] = self.modelGrpGpRequestsContexts.fsch_comment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_geomHexewkb)) {
                    paramData['fsch_geomHexewkb'] = self.modelGrpGpRequestsContexts.fsch_geomHexewkb;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpGpRequestsContexts.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_label)) {
                    paramData['fsch_label'] = self.modelGrpGpRequestsContexts.fsch_label;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_comment)) {
                    paramData['fsch_comment'] = self.modelGrpGpRequestsContexts.fsch_comment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_geomHexewkb)) {
                    paramData['fsch_geomHexewkb'] = self.modelGrpGpRequestsContexts.fsch_geomHexewkb;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.GpRequestsContexts[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.GpRequestsContexts[]) => void> = [];


        public getMergedItems(func: (items: Entities.GpRequestsContexts[]) => void, bContinuousCaller:boolean=true, parent?: Entities.ParcelsIssues, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.GpRequestsContexts[]):void {
                var mergedEntities = <Entities.GpRequestsContexts[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.GpRequestsContexts>e.a).parcelsIssuesId)).
                        map(e => <Entities.GpRequestsContexts>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.GpRequestsContexts[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.GpRequestsContexts):boolean {
            if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                return false;
            return x.parcelsIssuesId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.ParcelsIssues {
            var self = this;
            return self.parcelsIssuesId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpParcelsIssues.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get parcelsIssuesId():Entities.ParcelsIssues {
            var self = this;
            return <Entities.ParcelsIssues>self.$scope.modelGrpParcelsIssues.selectedEntities[0];
        }









        public constructEntity(): Entities.GpRequestsContexts {
            var self = this;
            var ret = new Entities.GpRequestsContexts(
                /*gpRequestsContextsId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*type:string*/ null,
                /*label:string*/ null,
                /*comment:string*/ null,
                /*geomHexewkb:string*/ null,
                /*referencepoint:string*/ null,
                /*rowVersion:number*/ null,
                /*hash:string*/ null,
                /*gpRequestsProducersId:Entities.GpRequestsProducers*/ null,
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.GpRequestsContexts = <Entities.GpRequestsContexts>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.parcelsIssuesId = self.parcelsIssuesId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.GpRequestsContexts): Entities.GpRequestsContexts {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.GpRequestsContexts, calledByParent: boolean= false): Entities.GpRequestsContexts {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.GpRequestsContexts.Create();
            ret.updateInstance(src);
            ret.gpRequestsContextsId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
                this.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.ParcelsIssues, newParent:Entities.ParcelsIssues) {
        
            this.model.totalItems = 0;

            this.getMergedItems(gpRequestsContextsList => {
                gpRequestsContextsList.forEach(gpRequestsContexts => {
                    var gpRequestsContextsCloned = this.cloneEntity_internal(gpRequestsContexts, true);
                    gpRequestsContextsCloned.parcelsIssuesId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(parcelsIssues: Entities.ParcelsIssues) {
        

            var self = this;
            var toBeDeleted:Array<Entities.GpRequestsContexts> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var gpRequestsContexts = <Entities.GpRequestsContexts>change.a
                if (parcelsIssues.getKey() === gpRequestsContexts.parcelsIssuesId.getKey()) {
                    toBeDeleted.push(gpRequestsContexts);
                }
            }

            _.each(toBeDeleted, (x:Entities.GpRequestsContexts) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
                self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.deleteNewEntitiesUnderParent(x);
            });

        }

        public deleteAllEntitiesUnderParent(parcelsIssues: Entities.ParcelsIssues, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteGpRequestsContextsList(gpRequestsContextsList: Entities.GpRequestsContexts[]) {
                if (gpRequestsContextsList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = gpRequestsContextsList[0];
                    var tail = gpRequestsContextsList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteGpRequestsContextsList(tail);
                    });
                }
            }


            this.getMergedItems(gpRequestsContextsList => {
                deleteGpRequestsContextsList(gpRequestsContextsList);
            }, false, parcelsIssues);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.GpRequestsContexts, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.deleteAllEntitiesUnderParent(ent, () => {
                    super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpGpRequestsContexts_srchr__fsch_type_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;


            return false;
        }
        public GrpGpRequestsContexts_srchr__fsch_label_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;


            return false;
        }
        public GrpGpRequestsContexts_srchr__fsch_comment_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;


            return false;
        }
        public GrpGpRequestsContexts_srchr__fsch_geomHexewkb_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;


            return false;
        }
        public GrpGpRequestsContexts_itm__type_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;
            if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpRequestsContexts, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpRequestsContexts_itm__label_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;
            if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpRequestsContexts, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpRequestsContexts_itm__comment_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;
            if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpRequestsContexts, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpRequestsContexts_itm__geomHexewkb_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;
            if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpRequestsContexts, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpRequestsContexts_itm__referencepoint_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;
            if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpRequestsContexts, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpRequestsContexts_itm__hash_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;
            if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpRequestsContexts, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean {
            var self = this;
            if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpRequestsContexts, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpGpRequestsContexts_itm__gpRequestsProducersId:ModelLovGpRequestsProducersBase;
        public showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId(gpRequestsContexts:Entities.GpRequestsContexts) {
            var self = this;
            if (gpRequestsContexts === undefined)
                return;
            var uimodel:NpTypes.IUIModel = gpRequestsContexts._gpRequestsProducersId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'GpRequestsProducers';
            dialogOptions.previousModel= self.cachedLovModel_GrpGpRequestsContexts_itm__gpRequestsProducersId;
            dialogOptions.className = "ControllerLovGpRequestsProducers";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var gpRequestsProducers1:Entities.GpRequestsProducers =   <Entities.GpRequestsProducers>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(gpRequestsProducers1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(gpRequestsProducers1) && gpRequestsProducers1.isEqual(gpRequestsContexts.gpRequestsProducersId))
                    return;
                gpRequestsContexts.gpRequestsProducersId = gpRequestsProducers1;
                self.markEntityAsUpdated(gpRequestsContexts, 'GrpGpRequestsContexts_itm__gpRequestsProducersId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovGpRequestsProducersBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpGpRequestsContexts_itm__gpRequestsProducersId = lovModel;
                    var wsPath = "GpRequestsProducers/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_GpRequestsProducersLov_notes)) {
                        paramData['fsch_GpRequestsProducersLov_notes'] = lovModel.fsch_GpRequestsProducersLov_notes;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovGpRequestsProducersBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "GpRequestsProducers/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_GpRequestsProducersLov_notes)) {
                        paramData['fsch_GpRequestsProducersLov_notes'] = lovModel.fsch_GpRequestsProducersLov_notes;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_GpRequestsProducersLov_notes)) {
                        paramData['fsch_GpRequestsProducersLov_notes'] = lovModel.fsch_GpRequestsProducersLov_notes;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['notes'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/GpRequestsProducers.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelsIssues_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelsIssues_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(gpRequestsContexts:Entities.GpRequestsContexts):boolean  {
            var self = this;
            if (gpRequestsContexts === null || gpRequestsContexts === undefined || gpRequestsContexts.getEntityName() !== "GpRequestsContexts")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(gpRequestsContexts, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public child_group_GrpGpUpload_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpGpUpload === undefined) || (self.model.modelGrpGpUpload.controller === undefined))
                return false;
            return self.model.modelGrpGpUpload.controller._isInvisible()
        }

    }


    // GROUP GrpGpUpload

    export class ModelGrpGpUploadBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpGpUpload;
        public get gpUploadsId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpUpload>this.selectedEntities[0]).gpUploadsId;
        }

        public set gpUploadsId(gpUploadsId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpUpload>this.selectedEntities[0]).gpUploadsId = gpUploadsId_newVal;
        }

        public get _gpUploadsId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._gpUploadsId;
        }

        public get data():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpUpload>this.selectedEntities[0]).data;
        }

        public set data(data_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpUpload>this.selectedEntities[0]).data = data_newVal;
        }

        public get _data():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._data;
        }

        public get environment():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpUpload>this.selectedEntities[0]).environment;
        }

        public set environment(environment_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpUpload>this.selectedEntities[0]).environment = environment_newVal;
        }

        public get _environment():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._environment;
        }

        public get dteUpload():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpUpload>this.selectedEntities[0]).dteUpload;
        }

        public set dteUpload(dteUpload_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpUpload>this.selectedEntities[0]).dteUpload = dteUpload_newVal;
        }

        public get _dteUpload():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteUpload;
        }

        public get hash():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpUpload>this.selectedEntities[0]).hash;
        }

        public set hash(hash_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpUpload>this.selectedEntities[0]).hash = hash_newVal;
        }

        public get _hash():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._hash;
        }

        public get image():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpUpload>this.selectedEntities[0]).image;
        }

        public set image(image_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpUpload>this.selectedEntities[0]).image = image_newVal;
        }

        public get _image():NpTypes.UIBlobModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._image;
        }

        public get gpRequestsContextsId():Entities.GpRequestsContexts {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.GpUpload>this.selectedEntities[0]).gpRequestsContextsId;
        }

        public set gpRequestsContextsId(gpRequestsContextsId_newVal:Entities.GpRequestsContexts) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.GpUpload>this.selectedEntities[0]).gpRequestsContextsId = gpRequestsContextsId_newVal;
        }

        public get _gpRequestsContextsId():NpTypes.UIManyToOneModel<Entities.GpRequestsContexts> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._gpRequestsContextsId;
        }

        _fsch_data:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_data():string {
            return this._fsch_data.value;
        }
        public set fsch_data(vl:string) {
            this._fsch_data.value = vl;
        }
        _fsch_environment:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_environment():string {
            return this._fsch_environment.value;
        }
        public set fsch_environment(vl:string) {
            this._fsch_environment.value = vl;
        }
        _fsch_dteUpload:NpTypes.UIDateModel = new NpTypes.UIDateModel(undefined);
        public get fsch_dteUpload():Date {
            return this._fsch_dteUpload.value;
        }
        public set fsch_dteUpload(vl:Date) {
            this._fsch_dteUpload.value = vl;
        }
        _fsch_hash:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_hash():string {
            return this._fsch_hash.value;
        }
        public set fsch_hash(vl:string) {
            this._fsch_hash.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpGpUpload) { super($scope); }
    }



    export interface IScopeGrpGpUploadBase extends Controllers.IAbstractTableGroupScope, IScopeGrpGpRequestsContextsBase{
        globals: Globals;
        modelGrpGpUpload : ModelGrpGpUpload;
        _disabled():boolean; 
        _invisible():boolean; 
        GrpGpUpload_srchr__fsch_data_disabled(gpUpload:Entities.GpUpload):boolean; 
        GrpGpUpload_srchr__fsch_environment_disabled(gpUpload:Entities.GpUpload):boolean; 
        GrpGpUpload_srchr__fsch_dteUpload_disabled(gpUpload:Entities.GpUpload):boolean; 
        GrpGpUpload_srchr__fsch_hash_disabled(gpUpload:Entities.GpUpload):boolean; 
        GrpGpUpload_itm__data_disabled(gpUpload:Entities.GpUpload):boolean; 
        GrpGpUpload_itm__environment_disabled(gpUpload:Entities.GpUpload):boolean; 
        GrpGpUpload_itm__dteUpload_disabled(gpUpload:Entities.GpUpload):boolean; 
        GrpGpUpload_itm__hash_disabled(gpUpload:Entities.GpUpload):boolean; 
        GrpGpUpload_itm__image_disabled(gpUpload:Entities.GpUpload):boolean; 
        createBlobModel_GrpGpUpload_itm__image(): NpTypes.NpBlob; 

    }



    export class ControllerGrpGpUploadBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpGpUpload,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpGpUpload)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpGpUploadBase = this;
            model.controller = <ControllerGrpGpUpload>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpGpUpload = self.model;

            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.GrpGpUpload_srchr__fsch_data_disabled = 
                (gpUpload:Entities.GpUpload) => {
                    return self.GrpGpUpload_srchr__fsch_data_disabled(gpUpload);
                };
            $scope.GrpGpUpload_srchr__fsch_environment_disabled = 
                (gpUpload:Entities.GpUpload) => {
                    return self.GrpGpUpload_srchr__fsch_environment_disabled(gpUpload);
                };
            $scope.GrpGpUpload_srchr__fsch_dteUpload_disabled = 
                (gpUpload:Entities.GpUpload) => {
                    return self.GrpGpUpload_srchr__fsch_dteUpload_disabled(gpUpload);
                };
            $scope.GrpGpUpload_srchr__fsch_hash_disabled = 
                (gpUpload:Entities.GpUpload) => {
                    return self.GrpGpUpload_srchr__fsch_hash_disabled(gpUpload);
                };
            $scope.GrpGpUpload_itm__data_disabled = 
                (gpUpload:Entities.GpUpload) => {
                    return self.GrpGpUpload_itm__data_disabled(gpUpload);
                };
            $scope.GrpGpUpload_itm__environment_disabled = 
                (gpUpload:Entities.GpUpload) => {
                    return self.GrpGpUpload_itm__environment_disabled(gpUpload);
                };
            $scope.GrpGpUpload_itm__dteUpload_disabled = 
                (gpUpload:Entities.GpUpload) => {
                    return self.GrpGpUpload_itm__dteUpload_disabled(gpUpload);
                };
            $scope.GrpGpUpload_itm__hash_disabled = 
                (gpUpload:Entities.GpUpload) => {
                    return self.GrpGpUpload_itm__hash_disabled(gpUpload);
                };
            $scope.GrpGpUpload_itm__image_disabled = 
                (gpUpload:Entities.GpUpload) => {
                    return self.GrpGpUpload_itm__image_disabled(gpUpload);
                };
            $scope.createBlobModel_GrpGpUpload_itm__image = 
                (): NpTypes.NpBlob => {
                    var tmp = self.createBlobModel_GrpGpUpload_itm__image();
                    return tmp;
                };


            $scope.pageModel.modelGrpGpUpload = $scope.modelGrpGpUpload;


            $scope.clearBtnAction = () => { 
                self.modelGrpGpUpload.fsch_data = undefined;
                self.modelGrpGpUpload.fsch_environment = undefined;
                self.modelGrpGpUpload.fsch_dteUpload = undefined;
                self.modelGrpGpUpload.fsch_hash = undefined;
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpGpRequestsContexts.modelGrpGpUpload = $scope.modelGrpGpUpload;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpGpRequestsContexts.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpUpload[] , oldVisible:Entities.GpUpload[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpGpUpload";
        }
        public get HtmlDivId(): string {
            return "ParcelsIssues_ControllerGrpGpUpload";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'data',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_data,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_data,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'environment',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_environment,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_environment,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new DateItem (
                        (ent?: NpTypes.IBaseEntity) => 'dteUpload',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_dteUpload,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_dteUpload,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: Date, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'hash',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_hash,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_hash,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.GpUpload) => 'data',
                        false ,
                        (ent?: Entities.GpUpload) => ent.data,  
                        (ent?: Entities.GpUpload) => ent._data,  
                        (ent?: Entities.GpUpload) => false, 
                        (vl: string, ent?: Entities.GpUpload) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.GpUpload) => 'environment',
                        false ,
                        (ent?: Entities.GpUpload) => ent.environment,  
                        (ent?: Entities.GpUpload) => ent._environment,  
                        (ent?: Entities.GpUpload) => false, 
                        (vl: string, ent?: Entities.GpUpload) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new DateItem (
                        (ent?: Entities.GpUpload) => 'dteUpload',
                        false ,
                        (ent?: Entities.GpUpload) => ent.dteUpload,  
                        (ent?: Entities.GpUpload) => ent._dteUpload,  
                        (ent?: Entities.GpUpload) => true, //isRequired
                        (vl: Date, ent?: Entities.GpUpload) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new TextItem (
                        (ent?: Entities.GpUpload) => 'hash',
                        false ,
                        (ent?: Entities.GpUpload) => ent.hash,  
                        (ent?: Entities.GpUpload) => ent._hash,  
                        (ent?: Entities.GpUpload) => true, 
                        (vl: string, ent?: Entities.GpUpload) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new BlobItem (
                        (ent?: Entities.GpUpload) => 'image',
                        false ,
                        (ent?: Entities.GpUpload) => ent.image,  
                        (ent?: Entities.GpUpload) => ent._image,  
                        (ent?: Entities.GpUpload) => false, //isRequired
                        (vl: string, ent?: Entities.GpUpload) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "GpUpload";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'data', displayName:'getALString("data", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpUpload_itm__data' data-ng-model='row.entity.data' data-np-ui-model='row.entity._data' data-ng-change='markEntityAsUpdated(row.entity,&quot;data&quot;)' data-ng-readonly='GrpGpUpload_itm__data_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'environment', displayName:'getALString("environment", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpUpload_itm__environment' data-ng-model='row.entity.environment' data-np-ui-model='row.entity._environment' data-ng-change='markEntityAsUpdated(row.entity,&quot;environment&quot;)' data-ng-readonly='GrpGpUpload_itm__environment_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteUpload', displayName:'getALString("dteUpload", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpUpload_itm__dteUpload' data-ng-model='row.entity.dteUpload' data-np-ui-model='row.entity._dteUpload' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteUpload&quot;)' data-np-required='true' data-ng-readonly='GrpGpUpload_itm__dteUpload_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'hash', displayName:'getALString("hash", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpUpload_itm__hash' data-ng-model='row.entity.hash' data-np-ui-model='row.entity._hash' data-ng-change='markEntityAsUpdated(row.entity,&quot;hash&quot;)' data-np-required='true' data-ng-readonly='GrpGpUpload_itm__hash_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'image', displayName:'getALString("image", true)', requiredAsterisk:false, resizable:true, sortable:false, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <span data-ng-model='row.entity._image' data-np-factory='createBlobModel_GrpGpUpload_itm__image()' data-np-blob='dummy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpGpUpload():ModelGrpGpUpload {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "GpUpload";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.GpRequestsContexts): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.GpUpload.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.gpRequestsContextsId = this.Parent;
                } else {
                    x.gpRequestsContextsId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.GpUpload {
            var self = this;
            return <Entities.GpUpload>self.$scope.modelGrpGpUpload.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.GpUpload, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpGpRequestsContexts.controller.isEntityLocked(<Entities.GpRequestsContexts>cur.gpRequestsContextsId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_data)) {
                    paramData['fsch_data'] = self.modelGrpGpUpload.fsch_data;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_environment)) {
                    paramData['fsch_environment'] = self.modelGrpGpUpload.fsch_environment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpUpload.fsch_hash;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpUpload/findAllByCriteriaRange_ParcelsIssuesGrpGpUpload_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_data)) {
                    paramData['fsch_data'] = self.modelGrpGpUpload.fsch_data;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_environment)) {
                    paramData['fsch_environment'] = self.modelGrpGpUpload.fsch_environment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpUpload.fsch_hash;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpUpload/findAllByCriteriaRange_ParcelsIssuesGrpGpUpload";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_data)) {
                    paramData['fsch_data'] = self.modelGrpGpUpload.fsch_data;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_environment)) {
                    paramData['fsch_environment'] = self.modelGrpGpUpload.fsch_environment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpUpload.fsch_hash;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "GpUpload/findAllByCriteriaRange_ParcelsIssuesGrpGpUpload_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_data)) {
                    paramData['fsch_data'] = self.modelGrpGpUpload.fsch_data;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_environment)) {
                    paramData['fsch_environment'] = self.modelGrpGpUpload.fsch_environment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpUpload.fsch_hash;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.GpUpload[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.GpUpload[]) => void> = [];


        public getMergedItems(func: (items: Entities.GpUpload[]) => void, bContinuousCaller:boolean=true, parent?: Entities.GpRequestsContexts, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.GpUpload[]):void {
                var mergedEntities = <Entities.GpUpload[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.GpUpload>e.a).gpRequestsContextsId)).
                        map(e => <Entities.GpUpload>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "GpUpload/findAllByCriteriaRange_ParcelsIssuesGrpGpUpload";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.GpUpload[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.GpUpload):boolean {
            if (this.Parent === undefined || x.gpRequestsContextsId === undefined)
                return false;
            return x.gpRequestsContextsId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.GpRequestsContexts {
            var self = this;
            return self.gpRequestsContextsId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpGpRequestsContexts.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get gpRequestsContextsId():Entities.GpRequestsContexts {
            var self = this;
            return <Entities.GpRequestsContexts>self.$scope.modelGrpGpRequestsContexts.selectedEntities[0];
        }









        public constructEntity(): Entities.GpUpload {
            var self = this;
            var ret = new Entities.GpUpload(
                /*gpUploadsId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*data:string*/ null,
                /*environment:string*/ null,
                /*dteUpload:Date*/ null,
                /*hash:string*/ null,
                /*image:string*/ null,
                /*gpRequestsContextsId:Entities.GpRequestsContexts*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.GpUpload = <Entities.GpUpload>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.gpRequestsContextsId = self.gpRequestsContextsId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.GpUpload): Entities.GpUpload {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.GpUpload, calledByParent: boolean= false): Entities.GpUpload {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.GpUpload.Create();
            ret.updateInstance(src);
            ret.gpUploadsId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.GpRequestsContexts, newParent:Entities.GpRequestsContexts) {
        
            this.model.totalItems = 0;

            this.getMergedItems(gpUploadList => {
                gpUploadList.forEach(gpUpload => {
                    var gpUploadCloned = this.cloneEntity_internal(gpUpload, true);
                    gpUploadCloned.gpRequestsContextsId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(gpRequestsContexts: Entities.GpRequestsContexts) {
        

            var self = this;
            var toBeDeleted:Array<Entities.GpUpload> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var gpUpload = <Entities.GpUpload>change.a
                if (gpRequestsContexts.getKey() === gpUpload.gpRequestsContextsId.getKey()) {
                    toBeDeleted.push(gpUpload);
                }
            }

            _.each(toBeDeleted, (x:Entities.GpUpload) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(gpRequestsContexts: Entities.GpRequestsContexts, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteGpUploadList(gpUploadList: Entities.GpUpload[]) {
                if (gpUploadList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = gpUploadList[0];
                    var tail = gpUploadList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteGpUploadList(tail);
                    });
                }
            }


            this.getMergedItems(gpUploadList => {
                deleteGpUploadList(gpUploadList);
            }, false, gpRequestsContexts);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.GpUpload, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpGpUpload_srchr__fsch_data_disabled(gpUpload:Entities.GpUpload):boolean {
            var self = this;


            return false;
        }
        public GrpGpUpload_srchr__fsch_environment_disabled(gpUpload:Entities.GpUpload):boolean {
            var self = this;


            return false;
        }
        public GrpGpUpload_srchr__fsch_dteUpload_disabled(gpUpload:Entities.GpUpload):boolean {
            var self = this;


            return false;
        }
        public GrpGpUpload_srchr__fsch_hash_disabled(gpUpload:Entities.GpUpload):boolean {
            var self = this;


            return false;
        }
        public GrpGpUpload_itm__data_disabled(gpUpload:Entities.GpUpload):boolean {
            var self = this;
            if (gpUpload === undefined || gpUpload === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpUpload, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpUpload_itm__environment_disabled(gpUpload:Entities.GpUpload):boolean {
            var self = this;
            if (gpUpload === undefined || gpUpload === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpUpload, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpUpload_itm__dteUpload_disabled(gpUpload:Entities.GpUpload):boolean {
            var self = this;
            if (gpUpload === undefined || gpUpload === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpUpload, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpUpload_itm__hash_disabled(gpUpload:Entities.GpUpload):boolean {
            var self = this;
            if (gpUpload === undefined || gpUpload === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpUpload, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpGpUpload_itm__image_disabled(gpUpload:Entities.GpUpload):boolean {
            var self = this;
            if (gpUpload === undefined || gpUpload === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(gpUpload, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public createBlobModel_GrpGpUpload_itm__image() : NpTypes.NpBlob {
            var self = this;
            var ret =
                new NpTypes.NpBlob(
                    (e:Entities.GpUpload) =>  {self.markEntityAsUpdated(e,"image"); },                                                                       // on value change
                    (e:Entities.GpUpload) =>  { return self.getDownloadUrl_createBlobModel_GrpGpUpload_itm__image(e);},            // download url
                    (e:Entities.GpUpload) => self.GrpGpUpload_itm__image_disabled(e),                                                           // is disabled
                    (e:Entities.GpUpload) => false,                                                                 // is invisible
                    "/Niva/rest/GpUpload/setImage",                                     // post url
                    true ,                                                           // add is enabled
                    true ,                                                          // del is enabled
                    "",                                              // valid extensions
                    (e:Entities.GpUpload) => 195                                          //size in KB
                );
            return ret;
        }
        public getDownloadUrl_createBlobModel_GrpGpUpload_itm__image(gpUpload:Entities.GpUpload):string {
            var self = this;
            if (isVoid(gpUpload))
                return 'javascript:void(0)'
            if (gpUpload.isNew() && isVoid(gpUpload.image))
                return 'javascript:void(0)';
            var url = "/Niva/rest/GpUpload/getImage?";
            if (!gpUpload.isNew()) {
                url += "&id="+encodeURIComponent(gpUpload.gpUploadsId);
            }
            if (!isVoid(gpUpload.image)) {
                url += "&temp_id="+encodeURIComponent(gpUpload.image);
            }
            return url;
        }

        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(gpUpload:Entities.GpUpload):boolean  {
            var self = this;
            if (gpUpload === null || gpUpload === undefined || gpUpload.getEntityName() !== "GpUpload")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(gpUpload, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }


    // GROUP GrpParcelsIssuesActivities

    export class ModelGrpParcelsIssuesActivitiesBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpParcelsIssuesActivities;
        public get parcelsIssuesActivitiesId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).parcelsIssuesActivitiesId;
        }

        public set parcelsIssuesActivitiesId(parcelsIssuesActivitiesId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).parcelsIssuesActivitiesId = parcelsIssuesActivitiesId_newVal;
        }

        public get _parcelsIssuesActivitiesId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcelsIssuesActivitiesId;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get type():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).type;
        }

        public set type(type_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).type = type_newVal;
        }

        public get _type():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._type;
        }

        public get dteTimestamp():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).dteTimestamp;
        }

        public set dteTimestamp(dteTimestamp_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).dteTimestamp = dteTimestamp_newVal;
        }

        public get _dteTimestamp():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteTimestamp;
        }

        public get typeExtrainfo():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).typeExtrainfo;
        }

        public set typeExtrainfo(typeExtrainfo_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).typeExtrainfo = typeExtrainfo_newVal;
        }

        public get _typeExtrainfo():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._typeExtrainfo;
        }

        public get parcelsIssuesId():Entities.ParcelsIssues {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).parcelsIssuesId;
        }

        public set parcelsIssuesId(parcelsIssuesId_newVal:Entities.ParcelsIssues) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelsIssuesActivities>this.selectedEntities[0]).parcelsIssuesId = parcelsIssuesId_newVal;
        }

        public get _parcelsIssuesId():NpTypes.UIManyToOneModel<Entities.ParcelsIssues> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcelsIssuesId;
        }

        _fsch_type:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_type():number {
            return this._fsch_type.value;
        }
        public set fsch_type(vl:number) {
            this._fsch_type.value = vl;
        }
        _fsch_dteTimestamp:NpTypes.UIDateModel = new NpTypes.UIDateModel(undefined);
        public get fsch_dteTimestamp():Date {
            return this._fsch_dteTimestamp.value;
        }
        public set fsch_dteTimestamp(vl:Date) {
            this._fsch_dteTimestamp.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpParcelsIssuesActivities) { super($scope); }
    }



    export interface IScopeGrpParcelsIssuesActivitiesBase extends Controllers.IAbstractTableGroupScope, IScopeGrpParcelsIssuesBase{
        globals: Globals;
        modelGrpParcelsIssuesActivities : ModelGrpParcelsIssuesActivities;
        _disabled():boolean; 
        _invisible():boolean; 
        GrpParcelsIssuesActivities_srchr__fsch_type_disabled(parcelsIssuesActivities:Entities.ParcelsIssuesActivities):boolean; 
        GrpParcelsIssuesActivities_srchr__fsch_dteTimestamp_disabled(parcelsIssuesActivities:Entities.ParcelsIssuesActivities):boolean; 
        GrpParcelsIssuesActivities_itm__type_disabled(parcelsIssuesActivities:Entities.ParcelsIssuesActivities):boolean; 
        GrpParcelsIssuesActivities_itm__dteTimestamp_disabled(parcelsIssuesActivities:Entities.ParcelsIssuesActivities):boolean; 

    }



    export class ControllerGrpParcelsIssuesActivitiesBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpParcelsIssuesActivities,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpParcelsIssuesActivities)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpParcelsIssuesActivitiesBase = this;
            model.controller = <ControllerGrpParcelsIssuesActivities>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpParcelsIssuesActivities = self.model;

            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.GrpParcelsIssuesActivities_srchr__fsch_type_disabled = 
                (parcelsIssuesActivities:Entities.ParcelsIssuesActivities) => {
                    return self.GrpParcelsIssuesActivities_srchr__fsch_type_disabled(parcelsIssuesActivities);
                };
            $scope.GrpParcelsIssuesActivities_srchr__fsch_dteTimestamp_disabled = 
                (parcelsIssuesActivities:Entities.ParcelsIssuesActivities) => {
                    return self.GrpParcelsIssuesActivities_srchr__fsch_dteTimestamp_disabled(parcelsIssuesActivities);
                };
            $scope.GrpParcelsIssuesActivities_itm__type_disabled = 
                (parcelsIssuesActivities:Entities.ParcelsIssuesActivities) => {
                    return self.GrpParcelsIssuesActivities_itm__type_disabled(parcelsIssuesActivities);
                };
            $scope.GrpParcelsIssuesActivities_itm__dteTimestamp_disabled = 
                (parcelsIssuesActivities:Entities.ParcelsIssuesActivities) => {
                    return self.GrpParcelsIssuesActivities_itm__dteTimestamp_disabled(parcelsIssuesActivities);
                };


            $scope.pageModel.modelGrpParcelsIssuesActivities = $scope.modelGrpParcelsIssuesActivities;


            $scope.clearBtnAction = () => { 
                self.modelGrpParcelsIssuesActivities.fsch_type = undefined;
                self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp = undefined;
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpParcelsIssues.modelGrpParcelsIssuesActivities = $scope.modelGrpParcelsIssuesActivities;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelsIssuesActivities[] , oldVisible:Entities.ParcelsIssuesActivities[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpParcelsIssuesActivities";
        }
        public get HtmlDivId(): string {
            return "ParcelsIssues_ControllerGrpParcelsIssuesActivities";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new NumberItem (
                        (ent?: NpTypes.IBaseEntity) => 'type',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_type,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_type,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: number, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        () => -32768,
                        () => 32767,
                        0),
                    new DateItem (
                        (ent?: NpTypes.IBaseEntity) => 'dteTimestamp',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_dteTimestamp,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_dteTimestamp,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: Date, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new NumberItem (
                        (ent?: Entities.ParcelsIssuesActivities) => 'type',
                        false ,
                        (ent?: Entities.ParcelsIssuesActivities) => ent.type,  
                        (ent?: Entities.ParcelsIssuesActivities) => ent._type,  
                        (ent?: Entities.ParcelsIssuesActivities) => true, //isRequired
                        (vl: number, ent?: Entities.ParcelsIssuesActivities) => new NpTypes.ValidationResult(true, ""), 
                        () => -32768,
                        () => 32767,
                        0),
                    new DateItem (
                        (ent?: Entities.ParcelsIssuesActivities) => 'dteTimestamp',
                        false ,
                        (ent?: Entities.ParcelsIssuesActivities) => ent.dteTimestamp,  
                        (ent?: Entities.ParcelsIssuesActivities) => ent._dteTimestamp,  
                        (ent?: Entities.ParcelsIssuesActivities) => true, //isRequired
                        (vl: Date, ent?: Entities.ParcelsIssuesActivities) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined)
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "ParcelsIssuesActivities";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'type', displayName:'getALString("type", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssuesActivities_itm__type' data-ng-model='row.entity.type' data-np-ui-model='row.entity._type' data-ng-change='markEntityAsUpdated(row.entity,&quot;type&quot;)' data-np-required='true' data-ng-readonly='GrpParcelsIssuesActivities_itm__type_disabled(row.entity)' data-np-number='dummy' data-np-min='-32768' data-np-max='32767' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteTimestamp', displayName:'getALString("dteTimestamp", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssuesActivities_itm__dteTimestamp' data-ng-model='row.entity.dteTimestamp' data-np-ui-model='row.entity._dteTimestamp' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteTimestamp&quot;)' data-np-required='true' data-ng-readonly='GrpParcelsIssuesActivities_itm__dteTimestamp_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpParcelsIssuesActivities():ModelGrpParcelsIssuesActivities {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "ParcelsIssuesActivities";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.ParcelsIssues): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.ParcelsIssuesActivities.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.parcelsIssuesId = this.Parent;
                } else {
                    x.parcelsIssuesId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.ParcelsIssuesActivities {
            var self = this;
            return <Entities.ParcelsIssuesActivities>self.$scope.modelGrpParcelsIssuesActivities.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.ParcelsIssuesActivities, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(<Entities.ParcelsIssues>cur.parcelsIssuesId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpParcelsIssuesActivities.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp)) {
                    paramData['fsch_dteTimestamp'] = self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelsIssuesActivities/findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpParcelsIssuesActivities.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp)) {
                    paramData['fsch_dteTimestamp'] = self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelsIssuesActivities/findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpParcelsIssuesActivities.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp)) {
                    paramData['fsch_dteTimestamp'] = self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelsIssuesActivities/findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpParcelsIssuesActivities.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp)) {
                    paramData['fsch_dteTimestamp'] = self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.ParcelsIssuesActivities[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.ParcelsIssuesActivities[]) => void> = [];


        public getMergedItems(func: (items: Entities.ParcelsIssuesActivities[]) => void, bContinuousCaller:boolean=true, parent?: Entities.ParcelsIssues, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.ParcelsIssuesActivities[]):void {
                var mergedEntities = <Entities.ParcelsIssuesActivities[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.ParcelsIssuesActivities>e.a).parcelsIssuesId)).
                        map(e => <Entities.ParcelsIssuesActivities>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "ParcelsIssuesActivities/findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.ParcelsIssuesActivities[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.ParcelsIssuesActivities):boolean {
            if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                return false;
            return x.parcelsIssuesId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.ParcelsIssues {
            var self = this;
            return self.parcelsIssuesId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpParcelsIssues.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get parcelsIssuesId():Entities.ParcelsIssues {
            var self = this;
            return <Entities.ParcelsIssues>self.$scope.modelGrpParcelsIssues.selectedEntities[0];
        }









        public constructEntity(): Entities.ParcelsIssuesActivities {
            var self = this;
            var ret = new Entities.ParcelsIssuesActivities(
                /*parcelsIssuesActivitiesId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*rowVersion:number*/ null,
                /*type:number*/ null,
                /*dteTimestamp:Date*/ null,
                /*typeExtrainfo:string*/ null,
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.ParcelsIssuesActivities = <Entities.ParcelsIssuesActivities>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.parcelsIssuesId = self.parcelsIssuesId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.ParcelsIssuesActivities): Entities.ParcelsIssuesActivities {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.ParcelsIssuesActivities, calledByParent: boolean= false): Entities.ParcelsIssuesActivities {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.ParcelsIssuesActivities.Create();
            ret.updateInstance(src);
            ret.parcelsIssuesActivitiesId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.ParcelsIssues, newParent:Entities.ParcelsIssues) {
        
            this.model.totalItems = 0;

            this.getMergedItems(parcelsIssuesActivitiesList => {
                parcelsIssuesActivitiesList.forEach(parcelsIssuesActivities => {
                    var parcelsIssuesActivitiesCloned = this.cloneEntity_internal(parcelsIssuesActivities, true);
                    parcelsIssuesActivitiesCloned.parcelsIssuesId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(parcelsIssues: Entities.ParcelsIssues) {
        

            var self = this;
            var toBeDeleted:Array<Entities.ParcelsIssuesActivities> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var parcelsIssuesActivities = <Entities.ParcelsIssuesActivities>change.a
                if (parcelsIssues.getKey() === parcelsIssuesActivities.parcelsIssuesId.getKey()) {
                    toBeDeleted.push(parcelsIssuesActivities);
                }
            }

            _.each(toBeDeleted, (x:Entities.ParcelsIssuesActivities) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(parcelsIssues: Entities.ParcelsIssues, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteParcelsIssuesActivitiesList(parcelsIssuesActivitiesList: Entities.ParcelsIssuesActivities[]) {
                if (parcelsIssuesActivitiesList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = parcelsIssuesActivitiesList[0];
                    var tail = parcelsIssuesActivitiesList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteParcelsIssuesActivitiesList(tail);
                    });
                }
            }


            this.getMergedItems(parcelsIssuesActivitiesList => {
                deleteParcelsIssuesActivitiesList(parcelsIssuesActivitiesList);
            }, false, parcelsIssues);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.ParcelsIssuesActivities, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelsIssuesActivities_srchr__fsch_type_disabled(parcelsIssuesActivities:Entities.ParcelsIssuesActivities):boolean {
            var self = this;


            return false;
        }
        public GrpParcelsIssuesActivities_srchr__fsch_dteTimestamp_disabled(parcelsIssuesActivities:Entities.ParcelsIssuesActivities):boolean {
            var self = this;


            return false;
        }
        public GrpParcelsIssuesActivities_itm__type_disabled(parcelsIssuesActivities:Entities.ParcelsIssuesActivities):boolean {
            var self = this;
            if (parcelsIssuesActivities === undefined || parcelsIssuesActivities === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelsIssuesActivities, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpParcelsIssuesActivities_itm__dteTimestamp_disabled(parcelsIssuesActivities:Entities.ParcelsIssuesActivities):boolean {
            var self = this;
            if (parcelsIssuesActivities === undefined || parcelsIssuesActivities === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelsIssuesActivities, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelsIssues_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpParcelsIssues_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(parcelsIssuesActivities:Entities.ParcelsIssuesActivities):boolean  {
            var self = this;
            if (parcelsIssuesActivities === null || parcelsIssuesActivities === undefined || parcelsIssuesActivities.getEntityName() !== "ParcelsIssuesActivities")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(parcelsIssuesActivities, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
