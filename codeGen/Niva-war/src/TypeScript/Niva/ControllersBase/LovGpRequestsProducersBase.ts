/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/GpRequestsProducersBase.ts" />
/// <reference path="../Controllers/LovGpRequestsProducers.ts" />

module Controllers {
    export class ModelLovGpRequestsProducersBase extends AbstractLovModel {
        _fsch_GpRequestsProducersLov_notes:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_GpRequestsProducersLov_notes():string {
            return this._fsch_GpRequestsProducersLov_notes.value;
        }
        public set fsch_GpRequestsProducersLov_notes(vl:string) {
            this._fsch_GpRequestsProducersLov_notes.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        controller: ControllerLovGpRequestsProducers;
        constructor(public $scope: IScopeLovGpRequestsProducers) { super($scope); }
    }


    export interface IScopeLovGpRequestsProducersBase extends IAbstractLovScope {
        globals: Globals;
        modelLovGpRequestsProducers : ModelLovGpRequestsProducers;
        _disabled():boolean; 
        _invisible():boolean; 
        LovGpRequestsProducers_fsch_GpRequestsProducersLov_notes_disabled():boolean; 
    }

    export class ControllerLovGpRequestsProducersBase extends LovController {
        constructor(
            public $scope: IScopeLovGpRequestsProducers,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:ModelLovGpRequestsProducers)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 5, maxLinesInHeader: 1})
            var self: ControllerLovGpRequestsProducersBase = this;
            model.controller = <ControllerLovGpRequestsProducers>self;
            $scope.clearBtnAction = () => { 
                self.model.fsch_GpRequestsProducersLov_notes = undefined;
                self.updateGrid();
            };
            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.LovGpRequestsProducers_fsch_GpRequestsProducersLov_notes_disabled = 
                () => {
                    return self.LovGpRequestsProducers_fsch_GpRequestsProducersLov_notes_disabled();
                };
            $scope.modelLovGpRequestsProducers = model;
            self.updateUI();
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerLovGpRequestsProducers";
        }
        public get HtmlDivId(): string {
            return "GpRequestsProducers_Id";
        }
        
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions =   <NpTypes.LovDialogOptions>self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName); 
            }
            var ret = [
                { cellClass:'cellToolTip', field:'notes', displayName:'getALString("notes", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'95%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.notes' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter((cl => cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true));
            if (self.isMultiSelect()) {
                ret.splice(0,0,{ width:'22', cellTemplate:"<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined});
            }
            return ret;
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            return Entities.GpRequestsProducers.fromJSONComplete(webResponse.data);
        }

        public get modelLovGpRequestsProducers():ModelLovGpRequestsProducers {
            var self = this;
            return self.model;
        }

        public initializeModelWithPreviousValues() {
            var self = this;
            var prevModel:ModelLovGpRequestsProducers = <ModelLovGpRequestsProducers>self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovGpRequestsProducers.fsch_GpRequestsProducersLov_notes = prevModel.fsch_GpRequestsProducersLov_notes
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public LovGpRequestsProducers_fsch_GpRequestsProducersLov_notes_disabled():boolean {
            var self = this;


            return false;
        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
