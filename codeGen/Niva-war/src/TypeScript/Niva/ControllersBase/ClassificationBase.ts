/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="../EntitiesBase/ClassifierBase.ts" />
/// <reference path="LovClassifierBase.ts" />
/// <reference path="../EntitiesBase/FileTemplateBase.ts" />
/// <reference path="LovFileTemplateBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="LovCoverTypeBase.ts" />
/// <reference path="../Controllers/Classification.ts" />
module Controllers.Classification {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpClas:ModelGrpClas;
        modelGrpParcelClas:ModelGrpParcelClas;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
        onNewBtnAction(): void;
        onDeleteBtnAction():void;

    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            

            $scope.onNewBtnAction = () => { 
                self.onNewBtnAction();
            };
            
            $scope.onDeleteBtnAction = () => { 
                self.onDeleteBtnAction();
            };


            $timeout(function() {
                $('#Classification').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "Classification";
        }
    
        public _getPageTitle(): string {
            return "Classification";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpClas.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpClas.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpParcelClas.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpClas.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpParcelClas.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpClas.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpClas.controller,
                    self.model.modelGrpParcelClas.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
            if (bForDelete) {
                pageChanges = pageChanges.concat(self.model.modelGrpClas.controller.getChangesToCommitForDeletion());
                pageChanges = pageChanges.concat(self.model.modelGrpClas.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getDeleteChangesToCommit());
            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpClas.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getChangesToCommit());
            }
            
            var hasClassification = pageChanges.some(change => change.entityName === "Classification")
            if (!hasClassification) {
                var validateEntity = new ChangeToCommit(ChangeStatus.UPDATE, 0, "Classification", self.model.modelGrpClas.controller.Current);
                pageChanges = pageChanges.concat(validateEntity);
            }
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_Classification"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpClas.controller.cleanUpAfterSave();
            self.model.modelGrpParcelClas.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;

                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.Classification>>self.$scope.getCurrentPageModel();
                        var newClassificationId = response.newEntitiesIds.firstOrNull(x => x.entityName === 'Classification');
                        if (!isVoid(newClassificationId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.Classification.CreateById(newClassificationId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        } else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }

                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }

        public getInitialEntity(): Entities.Classification {
            if (this.shownAsDialog()) {
                return <Entities.Classification>this.dialogSelectedEntity();
            } else {
                var breadCrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.Classification>>this.$scope.getPageModelByURL('/Classification');
                if (isVoid(breadCrumbStepModel)) {
                    return null;
                } else {
                    return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                }
            }
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


        public onNewBtnAction(): void {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => { 
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                self.addNewRecord();
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {self.addNewRecord();}),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                self.addNewRecord();
            }
        }

        public addNewRecord(): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            self.model.modelGrpClas.controller.cleanUpAfterSave();
            self.model.modelGrpParcelClas.controller.cleanUpAfterSave();
            self.model.modelGrpClas.controller.createNewEntityAndAddToUI();
        }

        public onDeleteBtnAction():void {
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, 
                [
                    new Tuple2("MessageBox_Button_Yes", () => {self.deleteRecord();}),
                    new Tuple2("MessageBox_Button_No", () => {})
                ], 1,1);
        }

        public deleteRecord(): void {
            var self = this;
            if (self.Mode === EditMode.NEW) {
                console.log("Delete pressed in a form while being in new mode!");
                return;
            }
            NpTypes.AlertMessage.clearAlerts(self.model);
            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData(true);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
            });
        }
        
        public get Mode(): EditMode {
            var self = this;
            if (isVoid(self.model.modelGrpClas) || isVoid(self.model.modelGrpClas.controller))
                return EditMode.NEW;
            return self.model.modelGrpClas.controller.Mode;
        }

        public _newIsDisabled(): boolean {
            var self = this;
            if (isVoid(self.model.modelGrpClas) || isVoid(self.model.modelGrpClas.controller))
                return true;
            return self.model.modelGrpClas.controller._newIsDisabled();
        }
        public _deleteIsDisabled(): boolean {
            var self = this;
            if (self.Mode === EditMode.NEW)
                return true;
            if (isVoid(self.model.modelGrpClas) || isVoid(self.model.modelGrpClas.controller))
                return true;
            return self.model.modelGrpClas.controller._deleteIsDisabled(self.model.modelGrpClas.controller.Current);
        }


    }


    

    // GROUP GrpClas

    export class ModelGrpClasBase extends Controllers.AbstractGroupFormModel {
        modelGrpParcelClas:ModelGrpParcelClas;
        controller: ControllerGrpClas;
        public get clasId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).clasId;
        }

        public set clasId(clasId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).clasId = clasId_newVal;
        }

        public get _clasId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._clasId;
        }

        public get name():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).name;
        }

        public set name(name_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).name = name_newVal;
        }

        public get _name():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._name;
        }

        public get description():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).description;
        }

        public set description(description_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).description = description_newVal;
        }

        public get _description():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._description;
        }

        public get dateTime():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).dateTime;
        }

        public set dateTime(dateTime_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).dateTime = dateTime_newVal;
        }

        public get _dateTime():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dateTime;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get recordtype():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).recordtype;
        }

        public set recordtype(recordtype_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).recordtype = recordtype_newVal;
        }

        public get _recordtype():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._recordtype;
        }

        public get filePath():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).filePath;
        }

        public set filePath(filePath_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).filePath = filePath_newVal;
        }

        public get _filePath():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._filePath;
        }

        public get attachedFile():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).attachedFile;
        }

        public set attachedFile(attachedFile_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).attachedFile = attachedFile_newVal;
        }

        public get _attachedFile():NpTypes.UIBlobModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._attachedFile;
        }

        public get year():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).year;
        }

        public set year(year_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).year = year_newVal;
        }

        public get _year():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._year;
        }

        public get isImported():boolean {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).isImported;
        }

        public set isImported(isImported_newVal:boolean) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).isImported = isImported_newVal;
        }

        public get _isImported():NpTypes.UIBoolModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._isImported;
        }

        public get clfrId():Entities.Classifier {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).clfrId;
        }

        public set clfrId(clfrId_newVal:Entities.Classifier) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).clfrId = clfrId_newVal;
        }

        public get _clfrId():NpTypes.UIManyToOneModel<Entities.Classifier> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._clfrId;
        }

        public get fiteId():Entities.FileTemplate {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Classification>this.selectedEntities[0]).fiteId;
        }

        public set fiteId(fiteId_newVal:Entities.FileTemplate) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Classification>this.selectedEntities[0]).fiteId = fiteId_newVal;
        }

        public get _fiteId():NpTypes.UIManyToOneModel<Entities.FileTemplate> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._fiteId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpClas) { super($scope); }
    }



    export interface IScopeGrpClasBase extends Controllers.IAbstractFormGroupScope, IPageScope{
        globals: Globals;
        modelGrpClas : ModelGrpClas;
        GrpClas_itm__name_disabled(classification:Entities.Classification):boolean; 
        GrpClas_itm__clfrId_disabled(classification:Entities.Classification):boolean; 
        showLov_GrpClas_itm__clfrId(classification:Entities.Classification):void; 
        GrpClas_itm__year_disabled(classification:Entities.Classification):boolean; 
        GrpClas_itm__fiteId_disabled(classification:Entities.Classification):boolean; 
        showLov_GrpClas_itm__fiteId(classification:Entities.Classification):void; 
        GrpClas_itm__description_disabled(classification:Entities.Classification):boolean; 
        GrpClas_itm__attachedFile_disabled(classification:Entities.Classification):boolean; 
        createBlobModel_GrpClas_itm__attachedFile(): NpTypes.NpBlob; 
        GrpClas_itm__filePath_disabled(classification:Entities.Classification):boolean; 
        importButtonId_disabled(classification:Entities.Classification):boolean; 
        importAction(classification:Entities.Classification):void; 
        GrpClas_0_disabled():boolean; 
        GrpClas_0_invisible():boolean; 
        child_group_GrpParcelClas_isInvisible():boolean; 

    }



    export class ControllerGrpClasBase extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpClas,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpClas)
        {
            super($scope, $http, $timeout, Plato, model);
            var self:ControllerGrpClasBase = this;
            model.controller = <ControllerGrpClas>self;
            $scope.modelGrpClas = self.model;

            var selectedEntity: Entities.Classification = this.$scope.pageModel.controller.getInitialEntity();
            if (isVoid(selectedEntity)) {
                self.createNewEntityAndAddToUI();
            } else {
                var clonedEntity = Entities.Classification.Create();
                clonedEntity.updateInstance(selectedEntity);
                $scope.modelGrpClas.visibleEntities[0] = clonedEntity;
                $scope.modelGrpClas.selectedEntities[0] = clonedEntity;
            } 
            $scope.GrpClas_itm__name_disabled = 
                (classification:Entities.Classification) => {
                    return self.GrpClas_itm__name_disabled(classification);
                };
            $scope.GrpClas_itm__clfrId_disabled = 
                (classification:Entities.Classification) => {
                    return self.GrpClas_itm__clfrId_disabled(classification);
                };
            $scope.showLov_GrpClas_itm__clfrId = 
                (classification:Entities.Classification) => {
                    $timeout( () => {        
                        self.showLov_GrpClas_itm__clfrId(classification);
                    }, 0);
                };
            $scope.GrpClas_itm__year_disabled = 
                (classification:Entities.Classification) => {
                    return self.GrpClas_itm__year_disabled(classification);
                };
            $scope.GrpClas_itm__fiteId_disabled = 
                (classification:Entities.Classification) => {
                    return self.GrpClas_itm__fiteId_disabled(classification);
                };
            $scope.showLov_GrpClas_itm__fiteId = 
                (classification:Entities.Classification) => {
                    $timeout( () => {        
                        self.showLov_GrpClas_itm__fiteId(classification);
                    }, 0);
                };
            $scope.GrpClas_itm__description_disabled = 
                (classification:Entities.Classification) => {
                    return self.GrpClas_itm__description_disabled(classification);
                };
            $scope.GrpClas_itm__attachedFile_disabled = 
                (classification:Entities.Classification) => {
                    return self.GrpClas_itm__attachedFile_disabled(classification);
                };
            $scope.createBlobModel_GrpClas_itm__attachedFile = 
                (): NpTypes.NpBlob => {
                    var tmp = self.createBlobModel_GrpClas_itm__attachedFile();
                    return tmp;
                };
            $scope.GrpClas_itm__filePath_disabled = 
                (classification:Entities.Classification) => {
                    return self.GrpClas_itm__filePath_disabled(classification);
                };
            $scope.importButtonId_disabled = 
                (classification:Entities.Classification) => {
                    return self.importButtonId_disabled(classification);
                };
            $scope.importAction = 
                (classification:Entities.Classification) => {
            self.importAction(classification);
                };
            $scope.GrpClas_0_disabled = 
                () => {
                    return self.GrpClas_0_disabled();
                };
            $scope.GrpClas_0_invisible = 
                () => {
                    return self.GrpClas_0_invisible();
                };
            $scope.child_group_GrpParcelClas_isInvisible = 
                () => {
                    return self.child_group_GrpParcelClas_isInvisible();
                };


            $scope.pageModel.modelGrpClas = $scope.modelGrpClas;


    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Classification[] , oldVisible:Entities.Classification[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.Classification.parcelClasCollection = (classification, func) => {
                this.model.modelGrpParcelClas.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, classification);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.Classification.parcelClasCollection = null;//(classification, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpClas";
        }
        public get HtmlDivId(): string {
            return "Classification_ControllerGrpClas";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: Entities.Classification) => 'Name',
                        false ,
                        (ent?: Entities.Classification) => ent.name,  
                        (ent?: Entities.Classification) => ent._name,  
                        (ent?: Entities.Classification) => true, 
                        (vl: string, ent?: Entities.Classification) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new DateItem (
                        (ent?: Entities.Classification) => 'Classification Date',
                        false ,
                        (ent?: Entities.Classification) => ent.dateTime,  
                        (ent?: Entities.Classification) => ent._dateTime,  
                        (ent?: Entities.Classification) => true, //isRequired
                        (vl: Date, ent?: Entities.Classification) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new LovItem (
                        (ent?: Entities.Classification) => 'Classification Engine',
                        false ,
                        (ent?: Entities.Classification) => ent.clfrId,  
                        (ent?: Entities.Classification) => ent._clfrId,  
                        (ent?: Entities.Classification) => true, //isRequired
                        (vl: Entities.Classifier, ent?: Entities.Classification) => new NpTypes.ValidationResult(true, "")),
                    new StaticListItem<number> (
                        (ent?: Entities.Classification) => 'Year of Declaration',
                        false ,
                        (ent?: Entities.Classification) => ent.year,  
                        (ent?: Entities.Classification) => ent._year,  
                        (ent?: Entities.Classification) => true, //isRequired
                        (vl: number, ent?: Entities.Classification) => new NpTypes.ValidationResult(true, "")),
                    new LovItem (
                        (ent?: Entities.Classification) => 'Data File Columns Template',
                        false ,
                        (ent?: Entities.Classification) => ent.fiteId,  
                        (ent?: Entities.Classification) => ent._fiteId,  
                        (ent?: Entities.Classification) => true, //isRequired
                        (vl: Entities.FileTemplate, ent?: Entities.Classification) => new NpTypes.ValidationResult(true, "")),
                    new TextItem (
                        (ent?: Entities.Classification) => 'Description',
                        false ,
                        (ent?: Entities.Classification) => ent.description,  
                        (ent?: Entities.Classification) => ent._description,  
                        (ent?: Entities.Classification) => false, 
                        (vl: string, ent?: Entities.Classification) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new BlobItem (
                        (ent?: Entities.Classification) => 'File Name',
                        false ,
                        (ent?: Entities.Classification) => ent.filePath,  
                        (ent?: Entities.Classification) => ent._attachedFile,  
                        (ent?: Entities.Classification) => false, //isRequired
                        (vl: string, ent?: Entities.Classification) => new NpTypes.ValidationResult(true, "")),
                    new TextItem (
                        (ent?: Entities.Classification) => ' ',
                        false ,
                        (ent?: Entities.Classification) => ent.filePath,  
                        (ent?: Entities.Classification) => ent._filePath,  
                        (ent?: Entities.Classification) => false, 
                        (vl: string, ent?: Entities.Classification) => new NpTypes.ValidationResult(true, ""), 
                        undefined)
                ];
            }
            return this._items;
        }

        

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpClas():ModelGrpClas {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "Classification";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
        }

        public setCurrentFormPageBreadcrumpStepModel() {
            var self = this;
            if (self.$scope.pageModel.controller.shownAsDialog())
                return;

            var breadCrumbStepModel: IFormPageBreadcrumbStepModel<Entities.Classification> = { selectedEntity: null };
            if (!isVoid(self.Current)) {
                var selectedEntity: Entities.Classification = Entities.Classification.Create();
                selectedEntity.updateInstance(self.Current);
                breadCrumbStepModel.selectedEntity = selectedEntity;
            } else {
                breadCrumbStepModel.selectedEntity = null;
            }
            self.$scope.setCurrentPageModel(breadCrumbStepModel);
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.Classification.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.Classification {
            var self = this;
            return <Entities.Classification>self.$scope.modelGrpClas.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.Classification, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }







        public constructEntity(): Entities.Classification {
            var self = this;
            var ret = new Entities.Classification(
                /*clasId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*name:string*/ null,
                /*description:string*/ null,
                /*dateTime:Date*/ new Date,
                /*rowVersion:number*/ null,
                /*recordtype:number*/ 0,
                /*filePath:string*/ null,
                /*attachedFile:string*/ null,
                /*year:number*/ null,
                /*isImported:boolean*/ null,
                /*clfrId:Entities.Classifier*/ null,
                /*fiteId:Entities.FileTemplate*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.Classification = <Entities.Classification>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.$scope.modelGrpClas.visibleEntities[0] = newEnt;
            self.$scope.modelGrpClas.selectedEntities[0] = newEnt;
            return newEnt;

        }

        public cloneEntity(src: Entities.Classification): Entities.Classification {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.Classification, calledByParent: boolean= false): Entities.Classification {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.Classification.Create();
            ret.updateInstance(src);
            ret.clasId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.visibleEntities[0] = ret;
            this.model.selectedEntities[0] = ret;

            this.$timeout( () => {
                this.modelGrpClas.modelGrpParcelClas.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpClas.modelGrpParcelClas.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.Classification, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpClas.modelGrpParcelClas.controller.deleteAllEntitiesUnderParent(ent, () => {
                    super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpClas.modelGrpParcelClas.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public GrpClas_itm__name_disabled(classification:Entities.Classification):boolean {
            var self = this;
            if (classification === undefined || classification === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(classification, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public isEditClassificationDisabled(classification:Entities.Classification):boolean { 
            console.warn("Unimplemented function isEditClassificationDisabled()");
            return false; 
        }
        public GrpClas_itm__clfrId_disabled(classification:Entities.Classification):boolean {
            var self = this;
            if (classification === undefined || classification === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(classification, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpClas_itm__clfrId:ModelLovClassifierBase;
        public showLov_GrpClas_itm__clfrId(classification:Entities.Classification) {
            var self = this;
            if (classification === undefined)
                return;
            var uimodel:NpTypes.IUIModel = classification._clfrId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Classification Engine';
            dialogOptions.previousModel= self.cachedLovModel_GrpClas_itm__clfrId;
            dialogOptions.className = "ControllerLovClassifier";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var classifier1:Entities.Classifier =   <Entities.Classifier>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(classifier1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(classifier1) && classifier1.isEqual(classification.clfrId))
                    return;
                classification.clfrId = classifier1;
                self.markEntityAsUpdated(classification, 'GrpClas_itm__clfrId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovClassifierBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpClas_itm__clfrId = lovModel;
                    var wsPath = "Classifier/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_name)) {
                        paramData['fsch_ClassifierLov_name'] = lovModel.fsch_ClassifierLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_description)) {
                        paramData['fsch_ClassifierLov_description'] = lovModel.fsch_ClassifierLov_description;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovClassifierBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Classifier/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_name)) {
                        paramData['fsch_ClassifierLov_name'] = lovModel.fsch_ClassifierLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_description)) {
                        paramData['fsch_ClassifierLov_description'] = lovModel.fsch_ClassifierLov_description;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_name)) {
                        paramData['fsch_ClassifierLov_name'] = lovModel.fsch_ClassifierLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_description)) {
                        paramData['fsch_ClassifierLov_description'] = lovModel.fsch_ClassifierLov_description;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Classifier.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpClas_itm__year_disabled(classification:Entities.Classification):boolean {
            var self = this;
            if (classification === undefined || classification === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(classification, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpClas_itm__fiteId_disabled(classification:Entities.Classification):boolean {
            var self = this;
            if (classification === undefined || classification === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(classification, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpClas_itm__fiteId:ModelLovFileTemplateBase;
        public showLov_GrpClas_itm__fiteId(classification:Entities.Classification) {
            var self = this;
            if (classification === undefined)
                return;
            var uimodel:NpTypes.IUIModel = classification._fiteId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Data Import Template';
            dialogOptions.previousModel= self.cachedLovModel_GrpClas_itm__fiteId;
            dialogOptions.className = "ControllerLovFileTemplate";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var fileTemplate1:Entities.FileTemplate =   <Entities.FileTemplate>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(fileTemplate1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(fileTemplate1) && fileTemplate1.isEqual(classification.fiteId))
                    return;
                classification.fiteId = fileTemplate1;
                self.markEntityAsUpdated(classification, 'GrpClas_itm__fiteId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovFileTemplateBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpClas_itm__fiteId = lovModel;
                    var wsPath = "FileTemplate/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_FileTemplateLov_name)) {
                        paramData['fsch_FileTemplateLov_name'] = lovModel.fsch_FileTemplateLov_name;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovFileTemplateBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "FileTemplate/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_FileTemplateLov_name)) {
                        paramData['fsch_FileTemplateLov_name'] = lovModel.fsch_FileTemplateLov_name;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_FileTemplateLov_name)) {
                        paramData['fsch_FileTemplateLov_name'] = lovModel.fsch_FileTemplateLov_name;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/FileTemplate.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpClas_itm__description_disabled(classification:Entities.Classification):boolean {
            var self = this;
            if (classification === undefined || classification === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(classification, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpClas_itm__attachedFile_disabled(classification:Entities.Classification):boolean {
            var self = this;
            if (classification === undefined || classification === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(classification, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public createBlobModel_GrpClas_itm__attachedFile() : NpTypes.NpBlob {
            var self = this;
            var ret =
                new NpTypes.NpBlob(
                    (e:Entities.Classification) =>  {self.markEntityAsUpdated(e,"attachedFile"); },                                                                       // on value change
                    (e:Entities.Classification) =>  { return self.getDownloadUrl_createBlobModel_GrpClas_itm__attachedFile(e);},            // download url
                    (e:Entities.Classification) => self.GrpClas_itm__attachedFile_disabled(e),                                                           // is disabled
                    (e:Entities.Classification) => false,                                                                 // is invisible
                    "/Niva/rest/Classification/setAttachedFile",                                     // post url
                    true ,                                                           // add is enabled
                    true ,                                                          // del is enabled
                    "",                                              // valid extensions
                    (e:Entities.Classification) => 976562                                          //size in KB
                );
            return ret;
        }
        public getDownloadUrl_createBlobModel_GrpClas_itm__attachedFile(classification:Entities.Classification):string {
            var self = this;
            if (isVoid(classification))
                return 'javascript:void(0)'
            if (classification.isNew() && isVoid(classification.attachedFile))
                return 'javascript:void(0)';
            var url = "/Niva/rest/Classification/getAttachedFile?";
            if (!classification.isNew()) {
                url += "&id="+encodeURIComponent(classification.clasId);
            }
            if (!isVoid(classification.attachedFile)) {
                url += "&temp_id="+encodeURIComponent(classification.attachedFile);
            }
            return url;
        }

        public GrpClas_itm__filePath_disabled(classification:Entities.Classification):boolean {
            var self = this;
            if (classification === undefined || classification === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(classification, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public importButtonId_disabled(classification:Entities.Classification):boolean {
            var self = this;
            if (classification === undefined || classification === null)
                return true;


            

            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public importAction(classification:Entities.Classification) {
            var self = this;
            console.log("Method: importAction() called")
            console.log(classification);

        }
        public GrpClas_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpClas_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  this.isEditClassificationNotImported();
            return programmerVal;
        }
        public isEditClassificationNotImported():boolean { 
            console.warn("Unimplemented function isEditClassificationNotImported()");
            return false; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = false;
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(classification:Entities.Classification):boolean  {
            var self = this;
            if (classification === null || classification === undefined || classification.getEntityName() !== "Classification")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_Classification_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(classification, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public child_group_GrpParcelClas_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpParcelClas === undefined) || (self.model.modelGrpParcelClas.controller === undefined))
                return false;
            return self.model.modelGrpParcelClas.controller._isInvisible()
        }

    }


    // GROUP GrpParcelClas

    export class ModelGrpParcelClasBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpParcelClas;
        public get pclaId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).pclaId;
        }

        public set pclaId(pclaId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).pclaId = pclaId_newVal;
        }

        public get _pclaId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._pclaId;
        }

        public get probPred():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).probPred;
        }

        public set probPred(probPred_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).probPred = probPred_newVal;
        }

        public get _probPred():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probPred;
        }

        public get probPred2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).probPred2;
        }

        public set probPred2(probPred2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).probPred2 = probPred2_newVal;
        }

        public get _probPred2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probPred2;
        }

        public get prodCode():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).prodCode;
        }

        public set prodCode(prodCode_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).prodCode = prodCode_newVal;
        }

        public get _prodCode():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._prodCode;
        }

        public get parcIdentifier():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).parcIdentifier;
        }

        public set parcIdentifier(parcIdentifier_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).parcIdentifier = parcIdentifier_newVal;
        }

        public get _parcIdentifier():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcIdentifier;
        }

        public get parcCode():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).parcCode;
        }

        public set parcCode(parcCode_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).parcCode = parcCode_newVal;
        }

        public get _parcCode():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._parcCode;
        }

        public get geom4326():ol.Feature {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).geom4326;
        }

        public set geom4326(geom4326_newVal:ol.Feature) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).geom4326 = geom4326_newVal;
        }

        public get _geom4326():NpTypes.UIGeoModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._geom4326;
        }

        public get clasId():Entities.Classification {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).clasId;
        }

        public set clasId(clasId_newVal:Entities.Classification) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).clasId = clasId_newVal;
        }

        public get _clasId():NpTypes.UIManyToOneModel<Entities.Classification> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._clasId;
        }

        public get cultIdDecl():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).cultIdDecl;
        }

        public set cultIdDecl(cultIdDecl_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).cultIdDecl = cultIdDecl_newVal;
        }

        public get _cultIdDecl():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultIdDecl;
        }

        public get cultIdPred():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).cultIdPred;
        }

        public set cultIdPred(cultIdPred_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).cultIdPred = cultIdPred_newVal;
        }

        public get _cultIdPred():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultIdPred;
        }

        public get cotyIdDecl():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).cotyIdDecl;
        }

        public set cotyIdDecl(cotyIdDecl_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).cotyIdDecl = cotyIdDecl_newVal;
        }

        public get _cotyIdDecl():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyIdDecl;
        }

        public get cotyIdPred():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).cotyIdPred;
        }

        public set cotyIdPred(cotyIdPred_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).cotyIdPred = cotyIdPred_newVal;
        }

        public get _cotyIdPred():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyIdPred;
        }

        public get cultIdPred2():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelClas>this.selectedEntities[0]).cultIdPred2;
        }

        public set cultIdPred2(cultIdPred2_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelClas>this.selectedEntities[0]).cultIdPred2 = cultIdPred2_newVal;
        }

        public get _cultIdPred2():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultIdPred2;
        }

        _fsch_parcIdentifier:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_parcIdentifier():string {
            return this._fsch_parcIdentifier.value;
        }
        public set fsch_parcIdentifier(vl:string) {
            this._fsch_parcIdentifier.value = vl;
        }
        _fsch_parcCode:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_parcCode():string {
            return this._fsch_parcCode.value;
        }
        public set fsch_parcCode(vl:string) {
            this._fsch_parcCode.value = vl;
        }
        _fsch_prodCode:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_prodCode():number {
            return this._fsch_prodCode.value;
        }
        public set fsch_prodCode(vl:number) {
            this._fsch_prodCode.value = vl;
        }
        _fsch_cultIdDecl:NpTypes.UIManyToOneModel<Entities.Cultivation> = new NpTypes.UIManyToOneModel<Entities.Cultivation>(undefined);
        public get fsch_cultIdDecl():Entities.Cultivation {
            return this._fsch_cultIdDecl.value;
        }
        public set fsch_cultIdDecl(vl:Entities.Cultivation) {
            this._fsch_cultIdDecl.value = vl;
        }
        _fsch_cotyIdDecl:NpTypes.UIManyToOneModel<Entities.CoverType> = new NpTypes.UIManyToOneModel<Entities.CoverType>(undefined);
        public get fsch_cotyIdDecl():Entities.CoverType {
            return this._fsch_cotyIdDecl.value;
        }
        public set fsch_cotyIdDecl(vl:Entities.CoverType) {
            this._fsch_cotyIdDecl.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpParcelClas) { super($scope); }
    }



    export interface IScopeGrpParcelClasBase extends Controllers.IAbstractTableGroupScope, IScopeGrpClasBase{
        globals: Globals;
        modelGrpParcelClas : ModelGrpParcelClas;
        _disabled():boolean; 
        _invisible():boolean; 
        GrpParcelClas_srchr__fsch_parcIdentifier_disabled(parcelClas:Entities.ParcelClas):boolean; 
        GrpParcelClas_srchr__fsch_parcCode_disabled(parcelClas:Entities.ParcelClas):boolean; 
        GrpParcelClas_srchr__fsch_prodCode_disabled(parcelClas:Entities.ParcelClas):boolean; 
        GrpParcelClas_srchr__fsch_cultIdDecl_disabled(parcelClas:Entities.ParcelClas):boolean; 
        showLov_GrpParcelClas_srchr__fsch_cultIdDecl():void; 
        GrpParcelClas_srchr__fsch_cotyIdDecl_disabled(parcelClas:Entities.ParcelClas):boolean; 
        showLov_GrpParcelClas_srchr__fsch_cotyIdDecl():void; 
        GrpParcelClas_itm__probPred_disabled(parcelClas:Entities.ParcelClas):boolean; 
        GrpParcelClas_itm__probPred2_disabled(parcelClas:Entities.ParcelClas):boolean; 
        GrpParcelClas_0_disabled():boolean; 
        GrpParcelClas_0_invisible():boolean; 
        XartoReg_disabled():boolean; 
        XartoReg_invisible():boolean; 

    }



    export class ControllerGrpParcelClasBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpParcelClas,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpParcelClas)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 15, maxLinesInHeader: 3});
            var self:ControllerGrpParcelClasBase = this;
            model.controller = <ControllerGrpParcelClas>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpParcelClas = self.model;

            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.GrpParcelClas_srchr__fsch_parcIdentifier_disabled = 
                (parcelClas:Entities.ParcelClas) => {
                    return self.GrpParcelClas_srchr__fsch_parcIdentifier_disabled(parcelClas);
                };
            $scope.GrpParcelClas_srchr__fsch_parcCode_disabled = 
                (parcelClas:Entities.ParcelClas) => {
                    return self.GrpParcelClas_srchr__fsch_parcCode_disabled(parcelClas);
                };
            $scope.GrpParcelClas_srchr__fsch_prodCode_disabled = 
                (parcelClas:Entities.ParcelClas) => {
                    return self.GrpParcelClas_srchr__fsch_prodCode_disabled(parcelClas);
                };
            $scope.GrpParcelClas_srchr__fsch_cultIdDecl_disabled = 
                (parcelClas:Entities.ParcelClas) => {
                    return self.GrpParcelClas_srchr__fsch_cultIdDecl_disabled(parcelClas);
                };
            $scope.showLov_GrpParcelClas_srchr__fsch_cultIdDecl = 
                () => {
                    $timeout( () => {        
                        self.showLov_GrpParcelClas_srchr__fsch_cultIdDecl();
                    }, 0);
                };
            $scope.GrpParcelClas_srchr__fsch_cotyIdDecl_disabled = 
                (parcelClas:Entities.ParcelClas) => {
                    return self.GrpParcelClas_srchr__fsch_cotyIdDecl_disabled(parcelClas);
                };
            $scope.showLov_GrpParcelClas_srchr__fsch_cotyIdDecl = 
                () => {
                    $timeout( () => {        
                        self.showLov_GrpParcelClas_srchr__fsch_cotyIdDecl();
                    }, 0);
                };
            $scope.GrpParcelClas_itm__probPred_disabled = 
                (parcelClas:Entities.ParcelClas) => {
                    return self.GrpParcelClas_itm__probPred_disabled(parcelClas);
                };
            $scope.GrpParcelClas_itm__probPred2_disabled = 
                (parcelClas:Entities.ParcelClas) => {
                    return self.GrpParcelClas_itm__probPred2_disabled(parcelClas);
                };
            $scope.GrpParcelClas_0_disabled = 
                () => {
                    return self.GrpParcelClas_0_disabled();
                };
            $scope.GrpParcelClas_0_invisible = 
                () => {
                    return self.GrpParcelClas_0_invisible();
                };
            $scope.XartoReg_disabled = 
                () => {
                    return self.XartoReg_disabled();
                };
            $scope.XartoReg_invisible = 
                () => {
                    return self.XartoReg_invisible();
                };


            $scope.pageModel.modelGrpParcelClas = $scope.modelGrpParcelClas;


            $scope.clearBtnAction = () => { 
                self.modelGrpParcelClas.fsch_parcIdentifier = undefined;
                self.modelGrpParcelClas.fsch_parcCode = undefined;
                self.modelGrpParcelClas.fsch_prodCode = undefined;
                self.modelGrpParcelClas.fsch_cultIdDecl = undefined;
                self.modelGrpParcelClas.fsch_cotyIdDecl = undefined;
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpClas.modelGrpParcelClas = $scope.modelGrpParcelClas;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpClas.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelClas[] , oldVisible:Entities.ParcelClas[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            this.createGeom4326Map({
                grp:this,
                Plato:Plato,
                divId:"INPMP",
                getDataFromEntity:(x: Entities.ParcelClas) => isVoid(x) ? undefined:x.geom4326,
                saveDataToEntity:(x: Entities.ParcelClas, f: ol.Feature) => { 
                    if (!isVoid(x)) {
                        x.geom4326 = f; 
                        this.markEntityAsUpdated(x,"geom4326");
                    } 
                },
                isDisabled:(x: Entities.ParcelClas) => true,
                layers : [
                            new NpGeoLayers.GoogleTileLayer(true),
                            new NpGeoLayers.DigitalGlobeTileLayer(false),
                            new NpGeoLayers.BingTileLayer(false),
                            new NpGeoLayers.GeoserverTileLayer('ΟΠΕΚΕΠΕ', 'http://tiles.dik.loc/geoserver/gwc/service/wms', 'orthos_17', 'image/jpeg', false, null, null),
                            new NpGeoLayers.OpenStreetTileLayer(false),
                            new NpGeoLayers.QuestTileLayer(false),
                            new NpGeoLayers.SqlVectorLayer<Entities.ParcelClas, AbstractSqlLayerFilterModel>({
                                layerId:'INPMP1',
                                label:'Search1',
                                isVisible:true,
                                isSearch:true
                            }),
                            new NpGeoLayers.SqlVectorLayer<Entities.ParcelClas, AbstractSqlLayerFilterModel>({
                                layerId:'INPMP2',
                                label:'Search2',
                                isVisible:true,
                                isSearch:true
                            }),
                            new NpGeoLayers.SqlVectorLayer<Entities.ParcelClas, AbstractSqlLayerFilterModel>({
                                layerId:'INPMP3',
                                label:'Search3',
                                isVisible:true,
                                isSearch:true
                            })
                         ],
                coordinateSystem : "EPSG:2100"
            });
            
        }

        public geom4326Map : NpGeo.NpMap;

        public createGeom4326Map(defaultOptions: NpGeo.NpMapConstructionOptions) {
            this.geom4326Map = new NpGeo.NpMap(defaultOptions);
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpParcelClas";
        }
        public get HtmlDivId(): string {
            return "Classification_ControllerGrpParcelClas";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'Identifier',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_parcIdentifier,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_parcIdentifier,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'Parcel\'s Code',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_parcCode,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_parcCode,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new NumberItem (
                        (ent?: NpTypes.IBaseEntity) => 'Farmer\'s Code',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_prodCode,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_prodCode,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: number, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        0),
                    new LovItem (
                        (ent?: NpTypes.IBaseEntity) => 'cultIdDecl',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_cultIdDecl,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_cultIdDecl,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: Entities.Cultivation, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, "")),
                    new LovItem (
                        (ent?: NpTypes.IBaseEntity) => 'cotyIdDecl',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_cotyIdDecl,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_cotyIdDecl,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: Entities.CoverType, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, "")),
                    new NumberItem (
                        (ent?: Entities.ParcelClas) => 'Cultivation \n 1st Prediction \n Probability',
                        false ,
                        (ent?: Entities.ParcelClas) => ent.probPred,  
                        (ent?: Entities.ParcelClas) => ent._probPred,  
                        (ent?: Entities.ParcelClas) => false, //isRequired
                        (vl: number, ent?: Entities.ParcelClas) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new NumberItem (
                        (ent?: Entities.ParcelClas) => 'Cultivation \n 2nd Prediction \n Probability',
                        false ,
                        (ent?: Entities.ParcelClas) => ent.probPred2,  
                        (ent?: Entities.ParcelClas) => ent._probPred2,  
                        (ent?: Entities.ParcelClas) => false, //isRequired
                        (vl: number, ent?: Entities.ParcelClas) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new MapItem (
                        (ent?: Entities.ParcelClas) => '',
                        false ,
                        (ent?: Entities.ParcelClas) => ent.geom4326,  
                        (ent?: Entities.ParcelClas) => ent._geom4326,  
                        (ent?: Entities.ParcelClas) => false, //isRequired
                        (vl: ol.Feature, ent?: Entities.ParcelClas) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Parcels";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'parcIdentifier', displayName:'getALString("Identifier", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__parcIdentifier' data-ng-model='row.entity.parcIdentifier' data-np-ui-model='row.entity._parcIdentifier' data-ng-change='markEntityAsUpdated(row.entity,&quot;parcIdentifier&quot;)' data-np-required='true' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'parcCode', displayName:'getALString("Parcel\'s \n Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__parcCode' data-ng-model='row.entity.parcCode' data-np-ui-model='row.entity._parcCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;parcCode&quot;)' data-np-required='true' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'prodCode', displayName:'getALString("Farmer\'s \n Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__prodCode' data-ng-model='row.entity.prodCode' data-np-ui-model='row.entity._prodCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;prodCode&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultIdDecl.code', displayName:'getALString("Cultivation \n Declared \n Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'6%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdDecl_code' data-ng-model='row.entity.cultIdDecl.code' data-np-ui-model='row.entity.cultIdDecl._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdDecl.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultIdDecl.name', displayName:'getALString("Cultivation \n Declared \n Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'10%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdDecl_name' data-ng-model='row.entity.cultIdDecl.name' data-np-ui-model='row.entity.cultIdDecl._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdDecl.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultIdPred.code', displayName:'getALString("Cultivation \n 1st Prediction \n Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'6%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdPred_code' data-ng-model='row.entity.cultIdPred.code' data-np-ui-model='row.entity.cultIdPred._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdPred.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultIdPred.name', displayName:'getALString("Cultivation \n 1st Prediction \n Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'10%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdPred_name' data-ng-model='row.entity.cultIdPred.name' data-np-ui-model='row.entity.cultIdPred._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdPred.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probPred', displayName:'getALString("Cultivation \n 1st Prediction \n Probability", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__probPred' data-ng-model='row.entity.probPred' data-np-ui-model='row.entity._probPred' data-ng-change='markEntityAsUpdated(row.entity,&quot;probPred&quot;)' data-ng-readonly='GrpParcelClas_itm__probPred_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultIdPred2.code', displayName:'getALString("Cultivation \n 2nd Prediction  \nCode", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'6%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdPred2_code' data-ng-model='row.entity.cultIdPred2.code' data-np-ui-model='row.entity.cultIdPred2._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdPred2.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultIdPred2.name', displayName:'getALString("Cultivation \n 2nd Prediction \n Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'10%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdPred2_name' data-ng-model='row.entity.cultIdPred2.name' data-np-ui-model='row.entity.cultIdPred2._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdPred2.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probPred2', displayName:'getALString("Cultivation \n 2nd Prediction \n Probability", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__probPred2' data-ng-model='row.entity.probPred2' data-np-ui-model='row.entity._probPred2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probPred2&quot;)' data-ng-readonly='GrpParcelClas_itm__probPred2_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpParcelClas():ModelGrpParcelClas {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "ParcelClas";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

            this.geom4326Map.cleanUndoBuffers();
        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.Classification): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.ParcelClas.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.clasId = this.Parent;
                } else {
                    x.clasId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.ParcelClas {
            var self = this;
            return <Entities.ParcelClas>self.$scope.modelGrpParcelClas.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.ParcelClas, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpClas.controller.isEntityLocked(<Entities.Classification>cur.clasId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.clasId)) {
                    paramData['clasId_clasId'] = self.Parent.clasId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcIdentifier)) {
                    paramData['fsch_parcIdentifier'] = self.modelGrpParcelClas.fsch_parcIdentifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcCode)) {
                    paramData['fsch_parcCode'] = self.modelGrpParcelClas.fsch_parcCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelClas.fsch_prodCode;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelClas/findAllByCriteriaRange_ClassificationGrpParcelClas_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.clasId)) {
                    paramData['clasId_clasId'] = self.Parent.clasId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcIdentifier)) {
                    paramData['fsch_parcIdentifier'] = self.modelGrpParcelClas.fsch_parcIdentifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcCode)) {
                    paramData['fsch_parcCode'] = self.modelGrpParcelClas.fsch_parcCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelClas.fsch_prodCode;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelClas/findAllByCriteriaRange_ClassificationGrpParcelClas";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.clasId)) {
                    paramData['clasId_clasId'] = self.Parent.clasId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcIdentifier)) {
                    paramData['fsch_parcIdentifier'] = self.modelGrpParcelClas.fsch_parcIdentifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcCode)) {
                    paramData['fsch_parcCode'] = self.modelGrpParcelClas.fsch_parcCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelClas.fsch_prodCode;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelClas/findAllByCriteriaRange_ClassificationGrpParcelClas_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.clasId)) {
                    paramData['clasId_clasId'] = self.Parent.clasId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcIdentifier)) {
                    paramData['fsch_parcIdentifier'] = self.modelGrpParcelClas.fsch_parcIdentifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcCode)) {
                    paramData['fsch_parcCode'] = self.modelGrpParcelClas.fsch_parcCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelClas.fsch_prodCode;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.ParcelClas[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.ParcelClas[]) => void> = [];


        public getMergedItems(func: (items: Entities.ParcelClas[]) => void, bContinuousCaller:boolean=true, parent?: Entities.Classification, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.ParcelClas[]):void {
                var mergedEntities = <Entities.ParcelClas[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.ParcelClas>e.a).clasId)).
                        map(e => <Entities.ParcelClas>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "ParcelClas/findAllByCriteriaRange_ClassificationGrpParcelClas";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['clasId_clasId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.ParcelClas[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.ParcelClas):boolean {
            if (this.Parent === undefined || x.clasId === undefined)
                return false;
            return x.clasId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.Classification {
            var self = this;
            return self.clasId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpClas.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get clasId():Entities.Classification {
            var self = this;
            return <Entities.Classification>self.$scope.modelGrpClas.selectedEntities[0];
        }









        public constructEntity(): Entities.ParcelClas {
            var self = this;
            var ret = new Entities.ParcelClas(
                /*pclaId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*probPred:number*/ null,
                /*probPred2:number*/ null,
                /*prodCode:number*/ null,
                /*parcIdentifier:string*/ null,
                /*parcCode:string*/ null,
                /*geom4326:ol.Feature*/ null,
                /*clasId:Entities.Classification*/ null,
                /*cultIdDecl:Entities.Cultivation*/ null,
                /*cultIdPred:Entities.Cultivation*/ null,
                /*cotyIdDecl:Entities.CoverType*/ null,
                /*cotyIdPred:Entities.CoverType*/ null,
                /*cultIdPred2:Entities.Cultivation*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.ParcelClas = <Entities.ParcelClas>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.clasId = self.clasId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.ParcelClas): Entities.ParcelClas {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.ParcelClas, calledByParent: boolean= false): Entities.ParcelClas {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.ParcelClas.Create();
            ret.updateInstance(src);
            ret.pclaId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.Classification, newParent:Entities.Classification) {
        
            this.model.totalItems = 0;

            this.getMergedItems(parcelClasList => {
                parcelClasList.forEach(parcelClas => {
                    var parcelClasCloned = this.cloneEntity_internal(parcelClas, true);
                    parcelClasCloned.clasId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(classification: Entities.Classification) {
        

            var self = this;
            var toBeDeleted:Array<Entities.ParcelClas> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var parcelClas = <Entities.ParcelClas>change.a
                if (classification.getKey() === parcelClas.clasId.getKey()) {
                    toBeDeleted.push(parcelClas);
                }
            }

            _.each(toBeDeleted, (x:Entities.ParcelClas) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(classification: Entities.Classification, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteParcelClasList(parcelClasList: Entities.ParcelClas[]) {
                if (parcelClasList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = parcelClasList[0];
                    var tail = parcelClasList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteParcelClasList(tail);
                    });
                }
            }


            this.getMergedItems(parcelClasList => {
                deleteParcelClasList(parcelClasList);
            }, false, classification);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.ParcelClas, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelClas_srchr__fsch_parcIdentifier_disabled(parcelClas:Entities.ParcelClas):boolean {
            var self = this;


            return false;
        }
        public GrpParcelClas_srchr__fsch_parcCode_disabled(parcelClas:Entities.ParcelClas):boolean {
            var self = this;


            return false;
        }
        public GrpParcelClas_srchr__fsch_prodCode_disabled(parcelClas:Entities.ParcelClas):boolean {
            var self = this;


            return false;
        }
        public GrpParcelClas_srchr__fsch_cultIdDecl_disabled(parcelClas:Entities.ParcelClas):boolean {
            var self = this;


            return false;
        }
        cachedLovModel_GrpParcelClas_srchr__fsch_cultIdDecl:ModelLovCultivationBase;
        public showLov_GrpParcelClas_srchr__fsch_cultIdDecl() {
            var self = this;
            var uimodel:NpTypes.IUIModel = self.modelGrpParcelClas._fsch_cultIdDecl;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Cultivation';
            dialogOptions.previousModel= self.cachedLovModel_GrpParcelClas_srchr__fsch_cultIdDecl;
            dialogOptions.className = "ControllerLovCultivation";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var cultivation1:Entities.Cultivation =   <Entities.Cultivation>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(cultivation1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(cultivation1) && cultivation1.isEqual(self.modelGrpParcelClas.fsch_cultIdDecl))
                    return;
                self.modelGrpParcelClas.fsch_cultIdDecl = cultivation1;
            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpParcelClas_srchr__fsch_cultIdDecl = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpParcelClas_srchr__fsch_cotyIdDecl_disabled(parcelClas:Entities.ParcelClas):boolean {
            var self = this;


            return false;
        }
        cachedLovModel_GrpParcelClas_srchr__fsch_cotyIdDecl:ModelLovCoverTypeBase;
        public showLov_GrpParcelClas_srchr__fsch_cotyIdDecl() {
            var self = this;
            var uimodel:NpTypes.IUIModel = self.modelGrpParcelClas._fsch_cotyIdDecl;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Land Cover';
            dialogOptions.previousModel= self.cachedLovModel_GrpParcelClas_srchr__fsch_cotyIdDecl;
            dialogOptions.className = "ControllerLovCoverType";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var coverType1:Entities.CoverType =   <Entities.CoverType>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(coverType1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(coverType1) && coverType1.isEqual(self.modelGrpParcelClas.fsch_cotyIdDecl))
                    return;
                self.modelGrpParcelClas.fsch_cotyIdDecl = coverType1;
            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCoverTypeBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpParcelClas_srchr__fsch_cotyIdDecl = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCoverTypeBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpParcelClas_itm__probPred_disabled(parcelClas:Entities.ParcelClas):boolean {
            var self = this;
            if (parcelClas === undefined || parcelClas === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelClas, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpParcelClas_itm__probPred2_disabled(parcelClas:Entities.ParcelClas):boolean {
            var self = this;
            if (parcelClas === undefined || parcelClas === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(parcelClas, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpParcelClas_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelClas_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public XartoReg_disabled():boolean { 
            if (false)
                return true;
            var parControl = this.GrpParcelClas_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public XartoReg_invisible():boolean { 
            if (false)
                return true;
            var parControl = this.GrpParcelClas_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isDisabled():boolean { 
            if (true)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpClas_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(parcelClas:Entities.ParcelClas):boolean  {
            return true;
        }

    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
