/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/GpRequestsContextsBase.ts" />
/// <reference path="../Controllers/LovGpRequestsContexts.ts" />

module Controllers {
    export class ModelLovGpRequestsContextsBase extends AbstractLovModel {
        _fsch_GpRequestsContextsLov_type:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_GpRequestsContextsLov_type():string {
            return this._fsch_GpRequestsContextsLov_type.value;
        }
        public set fsch_GpRequestsContextsLov_type(vl:string) {
            this._fsch_GpRequestsContextsLov_type.value = vl;
        }
        _fsch_GpRequestsContextsLov_label:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_GpRequestsContextsLov_label():string {
            return this._fsch_GpRequestsContextsLov_label.value;
        }
        public set fsch_GpRequestsContextsLov_label(vl:string) {
            this._fsch_GpRequestsContextsLov_label.value = vl;
        }
        _fsch_GpRequestsContextsLov_comment:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_GpRequestsContextsLov_comment():string {
            return this._fsch_GpRequestsContextsLov_comment.value;
        }
        public set fsch_GpRequestsContextsLov_comment(vl:string) {
            this._fsch_GpRequestsContextsLov_comment.value = vl;
        }
        _fsch_GpRequestsContextsLov_geomHexewkb:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_GpRequestsContextsLov_geomHexewkb():string {
            return this._fsch_GpRequestsContextsLov_geomHexewkb.value;
        }
        public set fsch_GpRequestsContextsLov_geomHexewkb(vl:string) {
            this._fsch_GpRequestsContextsLov_geomHexewkb.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        controller: ControllerLovGpRequestsContexts;
        constructor(public $scope: IScopeLovGpRequestsContexts) { super($scope); }
    }


    export interface IScopeLovGpRequestsContextsBase extends IAbstractLovScope {
        globals: Globals;
        modelLovGpRequestsContexts : ModelLovGpRequestsContexts;
        _disabled():boolean; 
        _invisible():boolean; 
        LovGpRequestsContexts_fsch_GpRequestsContextsLov_type_disabled():boolean; 
        LovGpRequestsContexts_fsch_GpRequestsContextsLov_label_disabled():boolean; 
        LovGpRequestsContexts_fsch_GpRequestsContextsLov_comment_disabled():boolean; 
        LovGpRequestsContexts_fsch_GpRequestsContextsLov_geomHexewkb_disabled():boolean; 
    }

    export class ControllerLovGpRequestsContextsBase extends LovController {
        constructor(
            public $scope: IScopeLovGpRequestsContexts,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:ModelLovGpRequestsContexts)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 5, maxLinesInHeader: 1})
            var self: ControllerLovGpRequestsContextsBase = this;
            model.controller = <ControllerLovGpRequestsContexts>self;
            $scope.clearBtnAction = () => { 
                self.model.fsch_GpRequestsContextsLov_type = undefined;
                self.model.fsch_GpRequestsContextsLov_label = undefined;
                self.model.fsch_GpRequestsContextsLov_comment = undefined;
                self.model.fsch_GpRequestsContextsLov_geomHexewkb = undefined;
                self.updateGrid();
            };
            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.LovGpRequestsContexts_fsch_GpRequestsContextsLov_type_disabled = 
                () => {
                    return self.LovGpRequestsContexts_fsch_GpRequestsContextsLov_type_disabled();
                };
            $scope.LovGpRequestsContexts_fsch_GpRequestsContextsLov_label_disabled = 
                () => {
                    return self.LovGpRequestsContexts_fsch_GpRequestsContextsLov_label_disabled();
                };
            $scope.LovGpRequestsContexts_fsch_GpRequestsContextsLov_comment_disabled = 
                () => {
                    return self.LovGpRequestsContexts_fsch_GpRequestsContextsLov_comment_disabled();
                };
            $scope.LovGpRequestsContexts_fsch_GpRequestsContextsLov_geomHexewkb_disabled = 
                () => {
                    return self.LovGpRequestsContexts_fsch_GpRequestsContextsLov_geomHexewkb_disabled();
                };
            $scope.modelLovGpRequestsContexts = model;
            self.updateUI();
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerLovGpRequestsContexts";
        }
        public get HtmlDivId(): string {
            return "GpRequestsContexts_Id";
        }
        
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions =   <NpTypes.LovDialogOptions>self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName); 
            }
            var ret = [
                { cellClass:'cellToolTip', field:'type', displayName:'getALString("type", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.type' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'label', displayName:'getALString("label", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.label' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comment', displayName:'getALString("comment", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.comment' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'geomHexewkb', displayName:'getALString("geomHexewkb", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.geomHexewkb' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'referencepoint', displayName:'getALString("referencepoint", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.referencepoint' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'hash', displayName:'getALString("hash", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'12%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.hash' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter((cl => cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true));
            if (self.isMultiSelect()) {
                ret.splice(0,0,{ width:'22', cellTemplate:"<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined});
            }
            return ret;
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            return Entities.GpRequestsContexts.fromJSONComplete(webResponse.data);
        }

        public get modelLovGpRequestsContexts():ModelLovGpRequestsContexts {
            var self = this;
            return self.model;
        }

        public initializeModelWithPreviousValues() {
            var self = this;
            var prevModel:ModelLovGpRequestsContexts = <ModelLovGpRequestsContexts>self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovGpRequestsContexts.fsch_GpRequestsContextsLov_type = prevModel.fsch_GpRequestsContextsLov_type
                self.modelLovGpRequestsContexts.fsch_GpRequestsContextsLov_label = prevModel.fsch_GpRequestsContextsLov_label
                self.modelLovGpRequestsContexts.fsch_GpRequestsContextsLov_comment = prevModel.fsch_GpRequestsContextsLov_comment
                self.modelLovGpRequestsContexts.fsch_GpRequestsContextsLov_geomHexewkb = prevModel.fsch_GpRequestsContextsLov_geomHexewkb
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public LovGpRequestsContexts_fsch_GpRequestsContextsLov_type_disabled():boolean {
            var self = this;


            return false;
        }
        public LovGpRequestsContexts_fsch_GpRequestsContextsLov_label_disabled():boolean {
            var self = this;


            return false;
        }
        public LovGpRequestsContexts_fsch_GpRequestsContextsLov_comment_disabled():boolean {
            var self = this;


            return false;
        }
        public LovGpRequestsContexts_fsch_GpRequestsContextsLov_geomHexewkb_disabled():boolean {
            var self = this;


            return false;
        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
