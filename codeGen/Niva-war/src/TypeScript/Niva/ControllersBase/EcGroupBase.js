var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
/// <reference path="../EntitiesBase/EcCultBase.ts" />
/// <reference path="../EntitiesBase/EcCultDetailBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="LovCoverTypeBase.ts" />
/// <reference path="../Controllers/EcGroup.ts" />
var Controllers;
(function (Controllers) {
    var EcGroup;
    (function (EcGroup) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        EcGroup.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $scope.onNewBtnAction = function () {
                    self.onNewBtnAction();
                };
                $scope.onDeleteBtnAction = function () {
                    self.onDeleteBtnAction();
                };
                $timeout(function () {
                    $('#EcGroup').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "EcGroup";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Eligibility Criteria";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpEcGroup.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpEcGroup.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpEcCult.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpEcCultDetail.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultCover.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultDetailCover.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultSuper.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultDetailSuper.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpEcGroup.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpEcCult.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpEcCultDetail.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultCover.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultDetailCover.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultSuper.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultDetailSuper.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpEcGroup.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpEcGroup.controller,
                            self.model.modelGrpEcCult.controller,
                            self.model.modelGrpEcCultDetail.controller,
                            self.model.modelEcCultCover.controller,
                            self.model.modelEcCultDetailCover.controller,
                            self.model.modelEcCultSuper.controller,
                            self.model.modelEcCultDetailSuper.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                    pageChanges = pageChanges.concat(self.model.modelGrpEcGroup.controller.getChangesToCommitForDeletion());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcGroup.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcCult.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcCultDetail.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultCover.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultDetailCover.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultSuper.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultDetailSuper.controller.getDeleteChangesToCommit());
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpEcGroup.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcCult.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcCultDetail.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultCover.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultDetailCover.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultSuper.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultDetailSuper.controller.getChangesToCommit());
                }
                var hasEcGroup = pageChanges.some(function (change) { return change.entityName === "EcGroup"; });
                if (!hasEcGroup) {
                    var validateEntity = new Controllers.ChangeToCommit(Controllers.ChangeStatus.UPDATE, 0, "EcGroup", self.model.modelGrpEcGroup.controller.Current);
                    pageChanges = pageChanges.concat(validateEntity);
                }
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_EcGroup";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpEcGroup.controller.cleanUpAfterSave();
                self.model.modelGrpEcCult.controller.cleanUpAfterSave();
                self.model.modelGrpEcCultDetail.controller.cleanUpAfterSave();
                self.model.modelEcCultCover.controller.cleanUpAfterSave();
                self.model.modelEcCultDetailCover.controller.cleanUpAfterSave();
                self.model.modelEcCultSuper.controller.cleanUpAfterSave();
                self.model.modelEcCultDetailSuper.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = self.$scope.getCurrentPageModel();
                        var newEcGroupId = response.newEntitiesIds.firstOrNull(function (x) { return x.entityName === 'EcGroup'; });
                        if (!isVoid(newEcGroupId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.EcGroup.CreateById(newEcGroupId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.getInitialEntity = function () {
                if (this.shownAsDialog()) {
                    return this.dialogSelectedEntity();
                }
                else {
                    var breadCrumbStepModel = this.$scope.getPageModelByURL('/EcGroup');
                    if (isVoid(breadCrumbStepModel)) {
                        return null;
                    }
                    else {
                        return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                    }
                }
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            PageControllerBase.prototype.onNewBtnAction = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                self.addNewRecord();
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () { self.addNewRecord(); }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    self.addNewRecord();
                }
            };
            PageControllerBase.prototype.addNewRecord = function () {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                self.model.modelGrpEcGroup.controller.cleanUpAfterSave();
                self.model.modelGrpEcCult.controller.cleanUpAfterSave();
                self.model.modelGrpEcCultDetail.controller.cleanUpAfterSave();
                self.model.modelEcCultCover.controller.cleanUpAfterSave();
                self.model.modelEcCultDetailCover.controller.cleanUpAfterSave();
                self.model.modelEcCultSuper.controller.cleanUpAfterSave();
                self.model.modelEcCultDetailSuper.controller.cleanUpAfterSave();
                self.model.modelGrpEcGroup.controller.createNewEntityAndAddToUI();
            };
            PageControllerBase.prototype.onDeleteBtnAction = function () {
                var self = this;
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                    new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(); }),
                    new Tuple2("MessageBox_Button_No", function () { })
                ], 1, 1);
            };
            PageControllerBase.prototype.deleteRecord = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW) {
                    console.log("Delete pressed in a form while being in new mode!");
                    return;
                }
                NpTypes.AlertMessage.clearAlerts(self.model);
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData(true);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "Mode", {
                get: function () {
                    var self = this;
                    if (isVoid(self.model.modelGrpEcGroup) || isVoid(self.model.modelGrpEcGroup.controller))
                        return Controllers.EditMode.NEW;
                    return self.model.modelGrpEcGroup.controller.Mode;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._newIsDisabled = function () {
                var self = this;
                if (isVoid(self.model.modelGrpEcGroup) || isVoid(self.model.modelGrpEcGroup.controller))
                    return true;
                return self.model.modelGrpEcGroup.controller._newIsDisabled();
            };
            PageControllerBase.prototype._deleteIsDisabled = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW)
                    return true;
                if (isVoid(self.model.modelGrpEcGroup) || isVoid(self.model.modelGrpEcGroup.controller))
                    return true;
                return self.model.modelGrpEcGroup.controller._deleteIsDisabled(self.model.modelGrpEcGroup.controller.Current);
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        EcGroup.PageControllerBase = PageControllerBase;
        // GROUP GrpEcGroup
        var ModelGrpEcGroupBase = (function (_super) {
            __extends(ModelGrpEcGroupBase, _super);
            function ModelGrpEcGroupBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecgrId;
                },
                set: function (ecgrId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecgrId = ecgrId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecgrId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "description", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].description;
                },
                set: function (description_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].description = description_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_description", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._description;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].name;
                },
                set: function (name_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].name = name_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "recordtype", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].recordtype;
                },
                set: function (recordtype_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].recordtype = recordtype_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_recordtype", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._recordtype;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "cropLevel", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cropLevel;
                },
                set: function (cropLevel_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cropLevel = cropLevel_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_cropLevel", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cropLevel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpEcGroupBase;
        })(Controllers.AbstractGroupFormModel);
        EcGroup.ModelGrpEcGroupBase = ModelGrpEcGroupBase;
        var ControllerGrpEcGroupBase = (function (_super) {
            __extends(ControllerGrpEcGroupBase, _super);
            function ControllerGrpEcGroupBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                $scope.modelGrpEcGroup = self.model;
                var selectedEntity = this.$scope.pageModel.controller.getInitialEntity();
                if (isVoid(selectedEntity)) {
                    self.createNewEntityAndAddToUI();
                }
                else {
                    var clonedEntity = Entities.EcGroup.Create();
                    clonedEntity.updateInstance(selectedEntity);
                    $scope.modelGrpEcGroup.visibleEntities[0] = clonedEntity;
                    $scope.modelGrpEcGroup.selectedEntities[0] = clonedEntity;
                }
                $scope.GrpEcGroup_itm__name_disabled =
                    function (ecGroup) {
                        return self.GrpEcGroup_itm__name_disabled(ecGroup);
                    };
                $scope.GrpEcGroup_itm__description_disabled =
                    function (ecGroup) {
                        return self.GrpEcGroup_itm__description_disabled(ecGroup);
                    };
                $scope.cropLevelChange =
                    function (ecGroup) {
                        self.cropLevelChange(ecGroup);
                    };
                $scope.GrpEcGroup_itm__cropLevel_disabled =
                    function (ecGroup) {
                        return self.GrpEcGroup_itm__cropLevel_disabled(ecGroup);
                    };
                $scope.GrpEcGroup_0_disabled =
                    function () {
                        return self.GrpEcGroup_0_disabled();
                    };
                $scope.GrpEcGroup_0_invisible =
                    function () {
                        return self.GrpEcGroup_0_invisible();
                    };
                $scope.GrpEcGroup_1_disabled =
                    function () {
                        return self.GrpEcGroup_1_disabled();
                    };
                $scope.GrpEcGroup_1_invisible =
                    function () {
                        return self.GrpEcGroup_1_invisible();
                    };
                $scope.GrpEcGroup_2_disabled =
                    function () {
                        return self.GrpEcGroup_2_disabled();
                    };
                $scope.GrpEcGroup_2_invisible =
                    function () {
                        return self.GrpEcGroup_2_invisible();
                    };
                $scope.child_group_GrpEcCult_isInvisible =
                    function () {
                        return self.child_group_GrpEcCult_isInvisible();
                    };
                $scope.child_group_EcCultCover_isInvisible =
                    function () {
                        return self.child_group_EcCultCover_isInvisible();
                    };
                $scope.child_group_EcCultSuper_isInvisible =
                    function () {
                        return self.child_group_EcCultSuper_isInvisible();
                    };
                $scope.pageModel.modelGrpEcGroup = $scope.modelGrpEcGroup;
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcGroup[] , oldVisible:Entities.EcGroup[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                //bind entity child collection with child controller merged items
                Entities.EcGroup.ecCultCollection = function (ecGroup, func) {
                    _this.model.modelGrpEcCult.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecGroup);
                };
                //bind entity child collection with child controller merged items
                Entities.EcGroup.ecCultCollection = function (ecGroup, func) {
                    _this.model.modelEcCultCover.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecGroup);
                };
                //bind entity child collection with child controller merged items
                Entities.EcGroup.ecCultCollection = function (ecGroup, func) {
                    _this.model.modelEcCultSuper.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecGroup);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.EcGroup.ecCultCollection = null; //(ecGroup, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.EcGroup.ecCultCollection = null; //(ecGroup, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.EcGroup.ecCultCollection = null; //(ecGroup, func) => { }
                });
            }
            ControllerGrpEcGroupBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpEcGroupBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpEcGroup";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcGroupBase.prototype, "HtmlDivId", {
                get: function () {
                    return "EcGroup_ControllerGrpEcGroup";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcGroupBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Name'; }, false, function (ent) { return ent.name; }, function (ent) { return ent._name; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'Description'; }, false, function (ent) { return ent.description; }, function (ent) { return ent._description; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.StaticListItem(function (ent) { return 'Crop Level'; }, false, function (ent) { return ent.cropLevel; }, function (ent) { return ent._cropLevel; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcGroupBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcGroupBase.prototype, "modelGrpEcGroup", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcGroupBase.prototype.getEntityName = function () {
                return "EcGroup";
            };
            ControllerGrpEcGroupBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
            };
            ControllerGrpEcGroupBase.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
                var self = this;
                if (self.$scope.pageModel.controller.shownAsDialog())
                    return;
                var breadCrumbStepModel = { selectedEntity: null };
                if (!isVoid(self.Current)) {
                    var selectedEntity = Entities.EcGroup.Create();
                    selectedEntity.updateInstance(self.Current);
                    breadCrumbStepModel.selectedEntity = selectedEntity;
                }
                else {
                    breadCrumbStepModel.selectedEntity = null;
                }
                self.$scope.setCurrentPageModel(breadCrumbStepModel);
            };
            ControllerGrpEcGroupBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.EcGroup.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpEcGroupBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroup.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcGroupBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpEcGroupBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcGroup(
                /*ecgrId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*description:string*/ null, 
                /*name:string*/ null, 
                /*rowVersion:number*/ null, 
                /*recordtype:number*/ 0, 
                /*cropLevel:number*/ null);
                return ret;
            };
            ControllerGrpEcGroupBase.prototype.createNewEntityAndAddToUI = function () {
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.$scope.modelGrpEcGroup.visibleEntities[0] = newEnt;
                self.$scope.modelGrpEcGroup.selectedEntities[0] = newEnt;
                return newEnt;
            };
            ControllerGrpEcGroupBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpEcGroupBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcGroup.Create();
                ret.updateInstance(src);
                ret.ecgrId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.visibleEntities[0] = ret;
                this.model.selectedEntities[0] = ret;
                this.$timeout(function () {
                    _this.modelGrpEcGroup.modelGrpEcCult.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpEcGroup.modelEcCultCover.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpEcGroup.modelEcCultSuper.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpEcGroupBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpEcGroup.modelGrpEcCult.controller,
                            self.modelGrpEcGroup.modelEcCultCover.controller,
                            self.modelGrpEcGroup.modelEcCultSuper.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcGroupBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpEcGroup.modelGrpEcCult.controller.deleteAllEntitiesUnderParent(ent, function () {
                        self.modelGrpEcGroup.modelEcCultCover.controller.deleteAllEntitiesUnderParent(ent, function () {
                            self.modelGrpEcGroup.modelEcCultSuper.controller.deleteAllEntitiesUnderParent(ent, function () {
                                _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                            });
                        });
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpEcGroup.modelGrpEcCult.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpEcGroup.modelEcCultCover.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpEcGroup.modelEcCultSuper.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpEcGroupBase.prototype.GrpEcGroup_itm__name_disabled = function (ecGroup) {
                var self = this;
                if (ecGroup === undefined || ecGroup === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecGroup, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcGroupBase.prototype.GrpEcGroup_itm__description_disabled = function (ecGroup) {
                var self = this;
                if (ecGroup === undefined || ecGroup === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecGroup, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcGroupBase.prototype.cropLevelChange = function (ecGroup) {
                console.log("cropLevelChange called");
                console.dir(ecGroup);
            };
            ControllerGrpEcGroupBase.prototype.isCropLevelDisabled = function (ecGroup) {
                console.warn("Unimplemented function isCropLevelDisabled()");
                return false;
            };
            ControllerGrpEcGroupBase.prototype.GrpEcGroup_itm__cropLevel_disabled = function (ecGroup) {
                var self = this;
                if (ecGroup === undefined || ecGroup === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecGroup, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isCropLevelDisabled(ecGroup);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcGroupBase.prototype.GrpEcGroup_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcGroupBase.prototype.GrpEcGroup_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = this.isEcGroupChildInv();
                return programmerVal;
            };
            ControllerGrpEcGroupBase.prototype.isEcGroupChildInv = function () {
                console.warn("Unimplemented function isEcGroupChildInv()");
                return false;
            };
            ControllerGrpEcGroupBase.prototype.GrpEcGroup_1_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcGroupBase.prototype.GrpEcGroup_1_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = this.isEcGroupCoverChildInv();
                return programmerVal;
            };
            ControllerGrpEcGroupBase.prototype.isEcGroupCoverChildInv = function () {
                console.warn("Unimplemented function isEcGroupCoverChildInv()");
                return false;
            };
            ControllerGrpEcGroupBase.prototype.GrpEcGroup_2_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcGroupBase.prototype.GrpEcGroup_2_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = this.isEcGroupSuperChildInv();
                return programmerVal;
            };
            ControllerGrpEcGroupBase.prototype.isEcGroupSuperChildInv = function () {
                console.warn("Unimplemented function isEcGroupSuperChildInv()");
                return false;
            };
            ControllerGrpEcGroupBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = this.isEcGroupDisabled();
                return programmerVal;
            };
            ControllerGrpEcGroupBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcGroupBase.prototype._newIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var parEntityIsLocked = false;
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpEcGroupBase.prototype._deleteIsDisabled = function (ecGroup) {
                var self = this;
                if (ecGroup === null || ecGroup === undefined || ecGroup.getEntityName() !== "EcGroup")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(ecGroup, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpEcGroupBase.prototype.isEcGroupDisabled = function () {
                console.warn("Unimplemented function isEcGroupDisabled()");
                return false;
            };
            ControllerGrpEcGroupBase.prototype.child_group_GrpEcCult_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpEcCult === undefined) || (self.model.modelGrpEcCult.controller === undefined))
                    return false;
                return self.model.modelGrpEcCult.controller._isInvisible();
            };
            ControllerGrpEcGroupBase.prototype.child_group_EcCultCover_isInvisible = function () {
                var self = this;
                if ((self.model.modelEcCultCover === undefined) || (self.model.modelEcCultCover.controller === undefined))
                    return false;
                return self.model.modelEcCultCover.controller._isInvisible();
            };
            ControllerGrpEcGroupBase.prototype.child_group_EcCultSuper_isInvisible = function () {
                var self = this;
                if ((self.model.modelEcCultSuper === undefined) || (self.model.modelEcCultSuper.controller === undefined))
                    return false;
                return self.model.modelEcCultSuper.controller._isInvisible();
            };
            return ControllerGrpEcGroupBase;
        })(Controllers.AbstractGroupFormController);
        EcGroup.ControllerGrpEcGroupBase = ControllerGrpEcGroupBase;
        // GROUP GrpEcCult
        var ModelGrpEcCultBase = (function (_super) {
            __extends(ModelGrpEcCultBase, _super);
            function ModelGrpEcCultBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpEcCultBase.prototype, "eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].eccuId;
                },
                set: function (eccuId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].eccuId = eccuId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._eccuId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].noneMatchDecision;
                },
                set: function (noneMatchDecision_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].noneMatchDecision = noneMatchDecision_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._noneMatchDecision;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecgrId;
                },
                set: function (ecgrId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecgrId = ecgrId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecgrId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].sucaId;
                },
                set: function (sucaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].sucaId = sucaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._sucaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpEcCultBase;
        })(Controllers.AbstractGroupTableModel);
        EcGroup.ModelGrpEcCultBase = ModelGrpEcCultBase;
        var ControllerGrpEcCultBase = (function (_super) {
            __extends(ControllerGrpEcCultBase, _super);
            function ControllerGrpEcCultBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpEcCult = self.model;
                $scope.GrpEcCult_itm__cultId_disabled =
                    function (ecCult) {
                        return self.GrpEcCult_itm__cultId_disabled(ecCult);
                    };
                $scope.showLov_GrpEcCult_itm__cultId =
                    function (ecCult) {
                        $timeout(function () {
                            self.showLov_GrpEcCult_itm__cultId(ecCult);
                        }, 0);
                    };
                $scope.child_group_GrpEcCultDetail_isInvisible =
                    function () {
                        return self.child_group_GrpEcCultDetail_isInvisible();
                    };
                $scope.pageModel.modelGrpEcCult = $scope.modelGrpEcCult;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpEcGroup.modelGrpEcCult = $scope.modelGrpEcCult;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpEcGroup.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCult[] , oldVisible:Entities.EcCult[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.EcCult.ecCultDetailCollection = function (ecCult, func) {
                    _this.model.modelGrpEcCultDetail.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecCult);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.EcCult.ecCultDetailCollection = null; //(ecCult, func) => { }
                });
            }
            ControllerGrpEcCultBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpEcCult";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "HtmlDivId", {
                get: function () {
                    return "EcGroup_ControllerGrpEcCult";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.LovItem(function (ent) { return 'Crop'; }, false, function (ent) { return ent.cultId; }, function (ent) { return ent._cultId; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultBase.prototype.gridTitle = function () {
                return "Cultivation Criteria";
            };
            ControllerGrpEcCultBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpEcCultBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridClone\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);cloneEntity(row.entity)\" data-ng-disabled=\"newIsDisabled()\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'cultId.name', displayName: 'getALString("Crop", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '14%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpEcCult_itm__cultId_disabled(row.entity) && showLov_GrpEcCult_itm__cultId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpEcCult_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpEcCult_itm__cultId(row.entity)'   data-ng-disabled=\"GrpEcCult_itm__cultId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultId.code', displayName: 'getALString("Crop Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCult_itm__cultId_code' data-ng-model='row.entity.cultId.code' data-np-ui-model='row.entity.cultId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep='.' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultId.cotyId.name', displayName: 'getALString("Land Cover", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCult_itm__cultId_cotyId_name' data-ng-model='row.entity.cultId.cotyId.name' data-np-ui-model='row.entity.cultId.cotyId._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.cotyId.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "modelGrpEcCult", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultBase.prototype.getEntityName = function () {
                return "EcCult";
            };
            ControllerGrpEcCultBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpEcCultBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCult.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.ecgrId = _this.Parent;
                    }
                    else {
                        x.ecgrId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcCult.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpEcGroup.controller.isEntityLocked(cur.ecgrId, lockKind);
            };
            ControllerGrpEcCultBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpEcCultBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupGrpEcCult_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupGrpEcCult";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupGrpEcCult_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.ecgrId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCult/findAllByCriteriaRange_EcGroupGrpEcCult";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['ecgrId_ecgrId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpEcCultBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.ecgrId === undefined)
                    return false;
                return x.ecgrId.getKey() === this.Parent.getKey();
            };
            ControllerGrpEcCultBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.ecgrId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroup.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "ecgrId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroup.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCult(
                /*eccuId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*noneMatchDecision:number*/ null, 
                /*rowVersion:number*/ null, 
                /*cultId:Entities.Cultivation*/ null, 
                /*ecgrId:Entities.EcGroup*/ null, 
                /*cotyId:Entities.CoverType*/ null, 
                /*sucaId:Entities.SuperClas*/ null);
                return ret;
            };
            ControllerGrpEcCultBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.ecgrId = self.ecgrId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpEcCultBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpEcCultBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCult.Create();
                ret.updateInstance(src);
                ret.eccuId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelGrpEcCult.modelGrpEcCultDetail.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerGrpEcCultBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultList) {
                    ecCultList.forEach(function (ecCult) {
                        var ecCultCloned = _this.cloneEntity_internal(ecCult, true);
                        ecCultCloned.ecgrId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpEcCultBase.prototype.deleteNewEntitiesUnderParent = function (ecGroup) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCult = change.a;
                    if (ecGroup.getKey() === ecCult.ecgrId.getKey()) {
                        toBeDeleted.push(ecCult);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelGrpEcCult.modelGrpEcCultDetail.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerGrpEcCultBase.prototype.deleteAllEntitiesUnderParent = function (ecGroup, afterDeleteAction) {
                var self = this;
                function deleteEcCultList(ecCultList) {
                    if (ecCultList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultList[0];
                        var tail = ecCultList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultList) {
                    deleteEcCultList(ecCultList);
                }, false, ecGroup);
            };
            Object.defineProperty(ControllerGrpEcCultBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpEcCult.modelGrpEcCultDetail.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpEcCult.modelGrpEcCultDetail.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpEcCult.modelGrpEcCultDetail.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpEcCultBase.prototype.GrpEcCult_itm__cultId_disabled = function (ecCult) {
                var self = this;
                if (ecCult === undefined || ecCult === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCult, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultBase.prototype.showLov_GrpEcCult_itm__cultId = function (ecCult) {
                var self = this;
                if (ecCult === undefined)
                    return;
                var uimodel = ecCult._cultId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Cultivation';
                dialogOptions.previousModel = self.cachedLovModel_GrpEcCult_itm__cultId;
                dialogOptions.className = "ControllerLovCultivation";
                dialogOptions.onSelect = function (selectedEntity) {
                    var cultivation1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(cultivation1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(cultivation1) && cultivation1.isEqual(ecCult.cultId))
                        return;
                    ecCult.cultId = cultivation1;
                    self.markEntityAsUpdated(ecCult, 'GrpEcCult_itm__cultId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpEcCult_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpEcCultBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroup_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcCultBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroup_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcCultBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpEcCultBase.prototype._deleteIsDisabled = function (ecCult) {
                var self = this;
                if (ecCult === null || ecCult === undefined || ecCult.getEntityName() !== "EcCult")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(ecCult, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpEcCultBase.prototype.child_group_GrpEcCultDetail_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpEcCultDetail === undefined) || (self.model.modelGrpEcCultDetail.controller === undefined))
                    return false;
                return self.model.modelGrpEcCultDetail.controller._isInvisible();
            };
            return ControllerGrpEcCultBase;
        })(Controllers.AbstractGroupTableController);
        EcGroup.ControllerGrpEcCultBase = ControllerGrpEcCultBase;
        // GROUP GrpEcCultDetail
        var ModelGrpEcCultDetailBase = (function (_super) {
            __extends(ModelGrpEcCultDetailBase, _super);
            function ModelGrpEcCultDetailBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "eccdId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].eccdId;
                },
                set: function (eccdId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].eccdId = eccdId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_eccdId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._eccdId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].orderingNumber;
                },
                set: function (orderingNumber_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].orderingNumber = orderingNumber_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._orderingNumber;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar;
                },
                set: function (agreesDeclar_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar = agreesDeclar_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper;
                },
                set: function (comparisonOper_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper = comparisonOper_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres;
                },
                set: function (probabThres_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres = probabThres_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].decisionLight;
                },
                set: function (decisionLight_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].decisionLight = decisionLight_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._decisionLight;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar2;
                },
                set: function (agreesDeclar2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar2 = agreesDeclar2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper2;
                },
                set: function (comparisonOper2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper2 = comparisonOper2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres2;
                },
                set: function (probabThres2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres2 = probabThres2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThresSum;
                },
                set: function (probabThresSum_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThresSum = probabThresSum_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThresSum;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper3;
                },
                set: function (comparisonOper3_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper3 = comparisonOper3_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper3;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].eccuId;
                },
                set: function (eccuId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].eccuId = eccuId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._eccuId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpEcCultDetailBase;
        })(Controllers.AbstractGroupTableModel);
        EcGroup.ModelGrpEcCultDetailBase = ModelGrpEcCultDetailBase;
        var ControllerGrpEcCultDetailBase = (function (_super) {
            __extends(ControllerGrpEcCultDetailBase, _super);
            function ControllerGrpEcCultDetailBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 3 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpEcCultDetail = self.model;
                $scope.GrpEcCultDetail_itm__orderingNumber_disabled =
                    function (ecCultDetail) {
                        return self.GrpEcCultDetail_itm__orderingNumber_disabled(ecCultDetail);
                    };
                $scope.GrpEcCultDetail_itm__agreesDeclar_disabled =
                    function (ecCultDetail) {
                        return self.GrpEcCultDetail_itm__agreesDeclar_disabled(ecCultDetail);
                    };
                $scope.GrpEcCultDetail_itm__comparisonOper_disabled =
                    function (ecCultDetail) {
                        return self.GrpEcCultDetail_itm__comparisonOper_disabled(ecCultDetail);
                    };
                $scope.GrpEcCultDetail_itm__probabThres_disabled =
                    function (ecCultDetail) {
                        return self.GrpEcCultDetail_itm__probabThres_disabled(ecCultDetail);
                    };
                $scope.GrpEcCultDetail_itm__agreesDeclar2_disabled =
                    function (ecCultDetail) {
                        return self.GrpEcCultDetail_itm__agreesDeclar2_disabled(ecCultDetail);
                    };
                $scope.GrpEcCultDetail_itm__comparisonOper2_disabled =
                    function (ecCultDetail) {
                        return self.GrpEcCultDetail_itm__comparisonOper2_disabled(ecCultDetail);
                    };
                $scope.GrpEcCultDetail_itm__probabThres2_disabled =
                    function (ecCultDetail) {
                        return self.GrpEcCultDetail_itm__probabThres2_disabled(ecCultDetail);
                    };
                $scope.GrpEcCultDetail_itm__comparisonOper3_disabled =
                    function (ecCultDetail) {
                        return self.GrpEcCultDetail_itm__comparisonOper3_disabled(ecCultDetail);
                    };
                $scope.GrpEcCultDetail_itm__probabThresSum_disabled =
                    function (ecCultDetail) {
                        return self.GrpEcCultDetail_itm__probabThresSum_disabled(ecCultDetail);
                    };
                $scope.GrpEcCultDetail_itm__decisionLight_disabled =
                    function (ecCultDetail) {
                        return self.GrpEcCultDetail_itm__decisionLight_disabled(ecCultDetail);
                    };
                $scope.pageModel.modelGrpEcCultDetail = $scope.modelGrpEcCultDetail;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpEcCult.modelGrpEcCultDetail = $scope.modelGrpEcCultDetail;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpEcCult.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetail[] , oldVisible:Entities.EcCultDetail[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpEcCultDetailBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpEcCultDetail";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "HtmlDivId", {
                get: function () {
                    return "EcGroup_ControllerGrpEcCultDetail";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.NumberItem(function (ent) { return 'Evaluation \n Order'; }, false, function (ent) { return ent.orderingNumber; }, function (ent) { return ent._orderingNumber; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0),
                            new Controllers.StaticListItem(function (ent) { return '1st \ Prediction '; }, false, function (ent) { return ent.agreesDeclar; }, function (ent) { return ent._agreesDeclar; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.StaticListItem(function (ent) { return 'Comparison \n Operator 1st'; }, false, function (ent) { return ent.comparisonOper; }, function (ent) { return ent._comparisonOper; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Confidence \n Pred. 1st (%)'; }, false, function (ent) { return ent.probabThres; }, function (ent) { return ent._probabThres; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.StaticListItem(function (ent) { return '2nd \ Prediction '; }, false, function (ent) { return ent.agreesDeclar2; }, function (ent) { return ent._agreesDeclar2; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.StaticListItem(function (ent) { return 'Comparison \n Operator 2nd'; }, false, function (ent) { return ent.comparisonOper2; }, function (ent) { return ent._comparisonOper2; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Confidence \n Pred. 2nd (%)'; }, false, function (ent) { return ent.probabThres2; }, function (ent) { return ent._probabThres2; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.StaticListItem(function (ent) { return 'Comparison \n Operator 1st + 2nd'; }, false, function (ent) { return ent.comparisonOper3; }, function (ent) { return ent._comparisonOper3; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Confidence \n Prediction \n 1st + 2nd (%)'; }, false, function (ent) { return ent.probabThresSum; }, function (ent) { return ent._probabThresSum; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.StaticListItem(function (ent) { return 'Traffic \n Light Code'; }, false, function (ent) { return ent.decisionLight; }, function (ent) { return ent._decisionLight; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultDetailBase.prototype.gridTitle = function () {
                return "Criteria";
            };
            ControllerGrpEcCultDetailBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpEcCultDetailBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'orderingNumber', displayName: 'getALString("Evaluation \n Order", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '5%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetail_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='GrpEcCultDetail_itm__orderingNumber_disabled(row.entity)' data-np-number='dummy' data-np-decimals='0' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar', displayName: 'getALString("1st \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='GrpEcCultDetail_itm__agreesDeclar_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper', displayName: 'getALString("Comparison \n Operator 1st", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='GrpEcCultDetail_itm__comparisonOper_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres', displayName: 'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetail_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='GrpEcCultDetail_itm__probabThres_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar2', displayName: 'getALString("2nd \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='GrpEcCultDetail_itm__agreesDeclar2_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper2', displayName: 'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='GrpEcCultDetail_itm__comparisonOper2_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres2', displayName: 'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetail_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='GrpEcCultDetail_itm__probabThres2_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper3', displayName: 'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='GrpEcCultDetail_itm__comparisonOper3_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThresSum', displayName: 'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetail_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='GrpEcCultDetail_itm__probabThresSum_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'decisionLight', displayName: 'getALString("Traffic \n Light Code", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_EcGroup_W"), resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='GrpEcCultDetail_itm__decisionLight_disabled(row.entity)' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "modelGrpEcCultDetail", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultDetailBase.prototype.getEntityName = function () {
                return "EcCultDetail";
            };
            ControllerGrpEcCultDetailBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpEcCultDetailBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCultDetail.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.eccuId = _this.Parent;
                    }
                    else {
                        x.eccuId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcCultDetail.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultDetailBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpEcCult.controller.isEntityLocked(cur.eccuId, lockKind);
            };
            ControllerGrpEcCultDetailBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpEcCultDetailBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupGrpEcCultDetail_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultDetailBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupGrpEcCultDetail";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultDetailBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupGrpEcCultDetail_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultDetailBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.eccuId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupGrpEcCultDetail";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['eccuId_eccuId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpEcCultDetailBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.eccuId === undefined)
                    return false;
                return x.eccuId.getKey() === this.Parent.getKey();
            };
            ControllerGrpEcCultDetailBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.eccuId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcCult.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "eccuId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcCult.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultDetailBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCultDetail(
                /*eccdId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*orderingNumber:number*/ null, 
                /*agreesDeclar:number*/ null, 
                /*comparisonOper:number*/ null, 
                /*probabThres:number*/ null, 
                /*decisionLight:number*/ null, 
                /*rowVersion:number*/ null, 
                /*agreesDeclar2:number*/ null, 
                /*comparisonOper2:number*/ null, 
                /*probabThres2:number*/ null, 
                /*probabThresSum:number*/ null, 
                /*comparisonOper3:number*/ null, 
                /*eccuId:Entities.EcCult*/ null);
                this.getMergedItems(function (items) {
                    ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                }, false);
                return ret;
            };
            ControllerGrpEcCultDetailBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.eccuId = self.eccuId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpEcCultDetailBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpEcCultDetailBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCultDetail.Create();
                ret.updateInstance(src);
                ret.eccdId = tmpId;
                if (!calledByParent) {
                    this.getMergedItems(function (items) {
                        ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                    }, false);
                }
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpEcCultDetailBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultDetailList) {
                    ecCultDetailList.forEach(function (ecCultDetail) {
                        var ecCultDetailCloned = _this.cloneEntity_internal(ecCultDetail, true);
                        ecCultDetailCloned.eccuId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpEcCultDetailBase.prototype.deleteNewEntitiesUnderParent = function (ecCult) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCultDetail = change.a;
                    if (ecCult.getKey() === ecCultDetail.eccuId.getKey()) {
                        toBeDeleted.push(ecCultDetail);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpEcCultDetailBase.prototype.deleteAllEntitiesUnderParent = function (ecCult, afterDeleteAction) {
                var self = this;
                function deleteEcCultDetailList(ecCultDetailList) {
                    if (ecCultDetailList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultDetailList[0];
                        var tail = ecCultDetailList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultDetailList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultDetailList) {
                    deleteEcCultDetailList(ecCultDetailList);
                }, false, ecCult);
            };
            Object.defineProperty(ControllerGrpEcCultDetailBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultDetailBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpEcCultDetailBase.prototype.GrpEcCultDetail_itm__orderingNumber_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultDetailBase.prototype.GrpEcCultDetail_itm__agreesDeclar_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultDetailBase.prototype.GrpEcCultDetail_itm__comparisonOper_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultDetailBase.prototype.GrpEcCultDetail_itm__probabThres_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultDetailBase.prototype.GrpEcCultDetail_itm__agreesDeclar2_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultDetailBase.prototype.GrpEcCultDetail_itm__comparisonOper2_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultDetailBase.prototype.GrpEcCultDetail_itm__probabThres2_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultDetailBase.prototype.GrpEcCultDetail_itm__comparisonOper3_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultDetailBase.prototype.GrpEcCultDetail_itm__probabThresSum_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultDetailBase.prototype.GrpEcCultDetail_itm__decisionLight_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpEcCultDetailBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcCultDetailBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcCultDetailBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpEcCultDetailBase.prototype._deleteIsDisabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === null || ecCultDetail === undefined || ecCultDetail.getEntityName() !== "EcCultDetail")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            return ControllerGrpEcCultDetailBase;
        })(Controllers.AbstractGroupTableController);
        EcGroup.ControllerGrpEcCultDetailBase = ControllerGrpEcCultDetailBase;
        // GROUP EcCultCover
        var ModelEcCultCoverBase = (function (_super) {
            __extends(ModelEcCultCoverBase, _super);
            function ModelEcCultCoverBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelEcCultCoverBase.prototype, "eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].eccuId;
                },
                set: function (eccuId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].eccuId = eccuId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._eccuId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].noneMatchDecision;
                },
                set: function (noneMatchDecision_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].noneMatchDecision = noneMatchDecision_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._noneMatchDecision;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecgrId;
                },
                set: function (ecgrId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecgrId = ecgrId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecgrId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].sucaId;
                },
                set: function (sucaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].sucaId = sucaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._sucaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCoverBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelEcCultCoverBase;
        })(Controllers.AbstractGroupTableModel);
        EcGroup.ModelEcCultCoverBase = ModelEcCultCoverBase;
        var ControllerEcCultCoverBase = (function (_super) {
            __extends(ControllerEcCultCoverBase, _super);
            function ControllerEcCultCoverBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelEcCultCover = self.model;
                $scope.EcCultCover_itm__cotyId_disabled =
                    function (ecCult) {
                        return self.EcCultCover_itm__cotyId_disabled(ecCult);
                    };
                $scope.showLov_EcCultCover_itm__cotyId =
                    function (ecCult) {
                        $timeout(function () {
                            self.showLov_EcCultCover_itm__cotyId(ecCult);
                        }, 0);
                    };
                $scope.child_group_EcCultDetailCover_isInvisible =
                    function () {
                        return self.child_group_EcCultDetailCover_isInvisible();
                    };
                $scope.pageModel.modelEcCultCover = $scope.modelEcCultCover;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpEcGroup.modelEcCultCover = $scope.modelEcCultCover;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpEcGroup.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCult[] , oldVisible:Entities.EcCult[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.EcCult.ecCultDetailCollection = function (ecCult, func) {
                    _this.model.modelEcCultDetailCover.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecCult);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.EcCult.ecCultDetailCollection = null; //(ecCult, func) => { }
                });
            }
            ControllerEcCultCoverBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerEcCultCover";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "HtmlDivId", {
                get: function () {
                    return "EcGroup_ControllerEcCultCover";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.LovItem(function (ent) { return 'Land Cover'; }, false, function (ent) { return ent.cotyId; }, function (ent) { return ent._cotyId; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCoverBase.prototype.gridTitle = function () {
                return "Land Cover Criteria";
            };
            ControllerEcCultCoverBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerEcCultCoverBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridClone\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);cloneEntity(row.entity)\" data-ng-disabled=\"newIsDisabled()\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'cotyId.name', displayName: 'getALString("Land Cover", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '23%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!EcCultCover_itm__cotyId_disabled(row.entity) && showLov_EcCultCover_itm__cotyId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cotyId.name' data-np-ui-model='row.entity._cotyId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='EcCultCover_itm__cotyId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_EcCultCover_itm__cotyId(row.entity)'   data-ng-disabled=\"EcCultCover_itm__cotyId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cotyId.code', displayName: 'getALString("Land Cover Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultCover_itm__cotyId_code' data-ng-model='row.entity.cotyId.code' data-np-ui-model='row.entity.cotyId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cotyId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep='.' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "modelEcCultCover", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCoverBase.prototype.getEntityName = function () {
                return "EcCult";
            };
            ControllerEcCultCoverBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerEcCultCoverBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCult.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.ecgrId = _this.Parent;
                    }
                    else {
                        x.ecgrId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultCover.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCoverBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpEcGroup.controller.isEntityLocked(cur.ecgrId, lockKind);
            };
            ControllerEcCultCoverBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerEcCultCoverBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultCover_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultCoverBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultCover";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultCoverBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultCover_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultCoverBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.ecgrId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultCover";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['ecgrId_ecgrId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerEcCultCoverBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.ecgrId === undefined)
                    return false;
                return x.ecgrId.getKey() === this.Parent.getKey();
            };
            ControllerEcCultCoverBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.ecgrId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroup.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "ecgrId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroup.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCoverBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCult(
                /*eccuId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*noneMatchDecision:number*/ null, 
                /*rowVersion:number*/ null, 
                /*cultId:Entities.Cultivation*/ null, 
                /*ecgrId:Entities.EcGroup*/ null, 
                /*cotyId:Entities.CoverType*/ null, 
                /*sucaId:Entities.SuperClas*/ null);
                return ret;
            };
            ControllerEcCultCoverBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.ecgrId = self.ecgrId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerEcCultCoverBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerEcCultCoverBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCult.Create();
                ret.updateInstance(src);
                ret.eccuId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelEcCultCover.modelEcCultDetailCover.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerEcCultCoverBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultList) {
                    ecCultList.forEach(function (ecCult) {
                        var ecCultCloned = _this.cloneEntity_internal(ecCult, true);
                        ecCultCloned.ecgrId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerEcCultCoverBase.prototype.deleteNewEntitiesUnderParent = function (ecGroup) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCult = change.a;
                    if (ecGroup.getKey() === ecCult.ecgrId.getKey()) {
                        toBeDeleted.push(ecCult);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelEcCultCover.modelEcCultDetailCover.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerEcCultCoverBase.prototype.deleteAllEntitiesUnderParent = function (ecGroup, afterDeleteAction) {
                var self = this;
                function deleteEcCultList(ecCultList) {
                    if (ecCultList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultList[0];
                        var tail = ecCultList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultList) {
                    deleteEcCultList(ecCultList);
                }, false, ecGroup);
            };
            Object.defineProperty(ControllerEcCultCoverBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelEcCultCover.modelEcCultDetailCover.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCoverBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelEcCultCover.modelEcCultDetailCover.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelEcCultCover.modelEcCultDetailCover.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerEcCultCoverBase.prototype.EcCultCover_itm__cotyId_disabled = function (ecCult) {
                var self = this;
                if (ecCult === undefined || ecCult === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCult, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultCoverBase.prototype.showLov_EcCultCover_itm__cotyId = function (ecCult) {
                var self = this;
                if (ecCult === undefined)
                    return;
                var uimodel = ecCult._cotyId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Land Cover';
                dialogOptions.previousModel = self.cachedLovModel_EcCultCover_itm__cotyId;
                dialogOptions.className = "ControllerLovCoverType";
                dialogOptions.onSelect = function (selectedEntity) {
                    var coverType1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(coverType1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(coverType1) && coverType1.isEqual(ecCult.cotyId))
                        return;
                    ecCult.cotyId = coverType1;
                    self.markEntityAsUpdated(ecCult, 'EcCultCover_itm__cotyId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_EcCultCover_itm__cotyId = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerEcCultCoverBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroup_1_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultCoverBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroup_1_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultCoverBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerEcCultCoverBase.prototype._deleteIsDisabled = function (ecCult) {
                var self = this;
                if (ecCult === null || ecCult === undefined || ecCult.getEntityName() !== "EcCult")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(ecCult, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerEcCultCoverBase.prototype.child_group_EcCultDetailCover_isInvisible = function () {
                var self = this;
                if ((self.model.modelEcCultDetailCover === undefined) || (self.model.modelEcCultDetailCover.controller === undefined))
                    return false;
                return self.model.modelEcCultDetailCover.controller._isInvisible();
            };
            return ControllerEcCultCoverBase;
        })(Controllers.AbstractGroupTableController);
        EcGroup.ControllerEcCultCoverBase = ControllerEcCultCoverBase;
        // GROUP EcCultDetailCover
        var ModelEcCultDetailCoverBase = (function (_super) {
            __extends(ModelEcCultDetailCoverBase, _super);
            function ModelEcCultDetailCoverBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "eccdId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].eccdId;
                },
                set: function (eccdId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].eccdId = eccdId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_eccdId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._eccdId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].orderingNumber;
                },
                set: function (orderingNumber_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].orderingNumber = orderingNumber_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._orderingNumber;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar;
                },
                set: function (agreesDeclar_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar = agreesDeclar_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper;
                },
                set: function (comparisonOper_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper = comparisonOper_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres;
                },
                set: function (probabThres_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres = probabThres_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].decisionLight;
                },
                set: function (decisionLight_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].decisionLight = decisionLight_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._decisionLight;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar2;
                },
                set: function (agreesDeclar2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar2 = agreesDeclar2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper2;
                },
                set: function (comparisonOper2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper2 = comparisonOper2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres2;
                },
                set: function (probabThres2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres2 = probabThres2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThresSum;
                },
                set: function (probabThresSum_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThresSum = probabThresSum_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThresSum;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper3;
                },
                set: function (comparisonOper3_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper3 = comparisonOper3_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper3;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].eccuId;
                },
                set: function (eccuId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].eccuId = eccuId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._eccuId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCoverBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelEcCultDetailCoverBase;
        })(Controllers.AbstractGroupTableModel);
        EcGroup.ModelEcCultDetailCoverBase = ModelEcCultDetailCoverBase;
        var ControllerEcCultDetailCoverBase = (function (_super) {
            __extends(ControllerEcCultDetailCoverBase, _super);
            function ControllerEcCultDetailCoverBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 3 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelEcCultDetailCover = self.model;
                $scope.EcCultDetailCover_itm__orderingNumber_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailCover_itm__orderingNumber_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailCover_itm__agreesDeclar_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailCover_itm__agreesDeclar_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailCover_itm__comparisonOper_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailCover_itm__comparisonOper_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailCover_itm__probabThres_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailCover_itm__probabThres_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailCover_itm__agreesDeclar2_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailCover_itm__agreesDeclar2_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailCover_itm__comparisonOper2_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailCover_itm__comparisonOper2_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailCover_itm__probabThres2_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailCover_itm__probabThres2_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailCover_itm__comparisonOper3_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailCover_itm__comparisonOper3_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailCover_itm__probabThresSum_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailCover_itm__probabThresSum_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailCover_itm__decisionLight_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailCover_itm__decisionLight_disabled(ecCultDetail);
                    };
                $scope.pageModel.modelEcCultDetailCover = $scope.modelEcCultDetailCover;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelEcCultCover.modelEcCultDetailCover = $scope.modelEcCultDetailCover;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelEcCultCover.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetail[] , oldVisible:Entities.EcCultDetail[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerEcCultDetailCoverBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerEcCultDetailCover";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "HtmlDivId", {
                get: function () {
                    return "EcGroup_ControllerEcCultDetailCover";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.NumberItem(function (ent) { return 'Evaluation \n Order'; }, false, function (ent) { return ent.orderingNumber; }, function (ent) { return ent._orderingNumber; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0),
                            new Controllers.StaticListItem(function (ent) { return '1st \ Prediction '; }, false, function (ent) { return ent.agreesDeclar; }, function (ent) { return ent._agreesDeclar; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.StaticListItem(function (ent) { return 'Comparison \n Operator 1st'; }, false, function (ent) { return ent.comparisonOper; }, function (ent) { return ent._comparisonOper; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Confidence \n Pred. 1st (%)'; }, false, function (ent) { return ent.probabThres; }, function (ent) { return ent._probabThres; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.StaticListItem(function (ent) { return '2nd \ Prediction '; }, false, function (ent) { return ent.agreesDeclar2; }, function (ent) { return ent._agreesDeclar2; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.StaticListItem(function (ent) { return 'Comparison \n Operator 2nd'; }, false, function (ent) { return ent.comparisonOper2; }, function (ent) { return ent._comparisonOper2; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Confidence \n Pred. 2nd (%)'; }, false, function (ent) { return ent.probabThres2; }, function (ent) { return ent._probabThres2; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.StaticListItem(function (ent) { return 'Comparison \n Operator 1st + 2nd'; }, false, function (ent) { return ent.comparisonOper3; }, function (ent) { return ent._comparisonOper3; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Confidence \n Prediction \n 1st + 2nd (%)'; }, false, function (ent) { return ent.probabThresSum; }, function (ent) { return ent._probabThresSum; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.StaticListItem(function (ent) { return 'Traffic \n Light Code'; }, false, function (ent) { return ent.decisionLight; }, function (ent) { return ent._decisionLight; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCoverBase.prototype.gridTitle = function () {
                return "Criteria";
            };
            ControllerEcCultDetailCoverBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerEcCultDetailCoverBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'orderingNumber', displayName: 'getALString("Evaluation \n Order", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '5%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCover_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='EcCultDetailCover_itm__orderingNumber_disabled(row.entity)' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar', displayName: 'getALString("1st \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='EcCultDetailCover_itm__agreesDeclar_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper', displayName: 'getALString("Comparison \n Operator 1st", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailCover_itm__comparisonOper_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres', displayName: 'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCover_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='EcCultDetailCover_itm__probabThres_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar2', displayName: 'getALString("2nd \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='EcCultDetailCover_itm__agreesDeclar2_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper2', displayName: 'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailCover_itm__comparisonOper2_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres2', displayName: 'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCover_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='EcCultDetailCover_itm__probabThres2_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper3', displayName: 'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailCover_itm__comparisonOper3_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThresSum', displayName: 'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCover_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='EcCultDetailCover_itm__probabThresSum_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'decisionLight', displayName: 'getALString("Traffic \n Light Code", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_EcGroup_W"), resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='EcCultDetailCover_itm__decisionLight_disabled(row.entity)' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "modelEcCultDetailCover", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCoverBase.prototype.getEntityName = function () {
                return "EcCultDetail";
            };
            ControllerEcCultDetailCoverBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerEcCultDetailCoverBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCultDetail.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.eccuId = _this.Parent;
                    }
                    else {
                        x.eccuId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultDetailCover.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCoverBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelEcCultCover.controller.isEntityLocked(cur.eccuId, lockKind);
            };
            ControllerEcCultDetailCoverBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerEcCultDetailCoverBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailCover_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailCoverBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailCover";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailCoverBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailCover_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailCoverBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.eccuId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailCover";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['eccuId_eccuId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerEcCultDetailCoverBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.eccuId === undefined)
                    return false;
                return x.eccuId.getKey() === this.Parent.getKey();
            };
            ControllerEcCultDetailCoverBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.eccuId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultCover.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "eccuId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultCover.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCoverBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCultDetail(
                /*eccdId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*orderingNumber:number*/ null, 
                /*agreesDeclar:number*/ null, 
                /*comparisonOper:number*/ null, 
                /*probabThres:number*/ null, 
                /*decisionLight:number*/ null, 
                /*rowVersion:number*/ null, 
                /*agreesDeclar2:number*/ null, 
                /*comparisonOper2:number*/ null, 
                /*probabThres2:number*/ null, 
                /*probabThresSum:number*/ null, 
                /*comparisonOper3:number*/ null, 
                /*eccuId:Entities.EcCult*/ null);
                this.getMergedItems(function (items) {
                    ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                }, false);
                return ret;
            };
            ControllerEcCultDetailCoverBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.eccuId = self.eccuId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerEcCultDetailCoverBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerEcCultDetailCoverBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCultDetail.Create();
                ret.updateInstance(src);
                ret.eccdId = tmpId;
                if (!calledByParent) {
                    this.getMergedItems(function (items) {
                        ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                    }, false);
                }
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerEcCultDetailCoverBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultDetailList) {
                    ecCultDetailList.forEach(function (ecCultDetail) {
                        var ecCultDetailCloned = _this.cloneEntity_internal(ecCultDetail, true);
                        ecCultDetailCloned.eccuId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerEcCultDetailCoverBase.prototype.deleteNewEntitiesUnderParent = function (ecCult) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCultDetail = change.a;
                    if (ecCult.getKey() === ecCultDetail.eccuId.getKey()) {
                        toBeDeleted.push(ecCultDetail);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerEcCultDetailCoverBase.prototype.deleteAllEntitiesUnderParent = function (ecCult, afterDeleteAction) {
                var self = this;
                function deleteEcCultDetailList(ecCultDetailList) {
                    if (ecCultDetailList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultDetailList[0];
                        var tail = ecCultDetailList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultDetailList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultDetailList) {
                    deleteEcCultDetailList(ecCultDetailList);
                }, false, ecCult);
            };
            Object.defineProperty(ControllerEcCultDetailCoverBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCoverBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerEcCultDetailCoverBase.prototype.EcCultDetailCover_itm__orderingNumber_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailCoverBase.prototype.EcCultDetailCover_itm__agreesDeclar_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailCoverBase.prototype.EcCultDetailCover_itm__comparisonOper_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailCoverBase.prototype.EcCultDetailCover_itm__probabThres_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailCoverBase.prototype.EcCultDetailCover_itm__agreesDeclar2_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailCoverBase.prototype.EcCultDetailCover_itm__comparisonOper2_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailCoverBase.prototype.EcCultDetailCover_itm__probabThres2_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailCoverBase.prototype.EcCultDetailCover_itm__comparisonOper3_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailCoverBase.prototype.EcCultDetailCover_itm__probabThresSum_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailCoverBase.prototype.EcCultDetailCover_itm__decisionLight_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailCoverBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultDetailCoverBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultDetailCoverBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerEcCultDetailCoverBase.prototype._deleteIsDisabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === null || ecCultDetail === undefined || ecCultDetail.getEntityName() !== "EcCultDetail")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            return ControllerEcCultDetailCoverBase;
        })(Controllers.AbstractGroupTableController);
        EcGroup.ControllerEcCultDetailCoverBase = ControllerEcCultDetailCoverBase;
        // GROUP EcCultSuper
        var ModelEcCultSuperBase = (function (_super) {
            __extends(ModelEcCultSuperBase, _super);
            function ModelEcCultSuperBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelEcCultSuperBase.prototype, "eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].eccuId;
                },
                set: function (eccuId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].eccuId = eccuId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._eccuId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].noneMatchDecision;
                },
                set: function (noneMatchDecision_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].noneMatchDecision = noneMatchDecision_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._noneMatchDecision;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecgrId;
                },
                set: function (ecgrId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecgrId = ecgrId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecgrId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].sucaId;
                },
                set: function (sucaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].sucaId = sucaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._sucaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultSuperBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelEcCultSuperBase;
        })(Controllers.AbstractGroupTableModel);
        EcGroup.ModelEcCultSuperBase = ModelEcCultSuperBase;
        var ControllerEcCultSuperBase = (function (_super) {
            __extends(ControllerEcCultSuperBase, _super);
            function ControllerEcCultSuperBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelEcCultSuper = self.model;
                $scope.EcCultSuper_itm__cultId_disabled =
                    function (ecCult) {
                        return self.EcCultSuper_itm__cultId_disabled(ecCult);
                    };
                $scope.showLov_EcCultSuper_itm__cultId =
                    function (ecCult) {
                        $timeout(function () {
                            self.showLov_EcCultSuper_itm__cultId(ecCult);
                        }, 0);
                    };
                $scope.child_group_EcCultDetailSuper_isInvisible =
                    function () {
                        return self.child_group_EcCultDetailSuper_isInvisible();
                    };
                $scope.pageModel.modelEcCultSuper = $scope.modelEcCultSuper;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpEcGroup.modelEcCultSuper = $scope.modelEcCultSuper;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpEcGroup.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCult[] , oldVisible:Entities.EcCult[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.EcCult.ecCultDetailCollection = function (ecCult, func) {
                    _this.model.modelEcCultDetailSuper.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecCult);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.EcCult.ecCultDetailCollection = null; //(ecCult, func) => { }
                });
            }
            ControllerEcCultSuperBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerEcCultSuper";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "HtmlDivId", {
                get: function () {
                    return "EcGroup_ControllerEcCultSuper";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.LovItem(function (ent) { return 'Crop'; }, false, function (ent) { return ent.cultId; }, function (ent) { return ent._cultId; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultSuperBase.prototype.gridTitle = function () {
                return "Crop to Land Cover Criteria";
            };
            ControllerEcCultSuperBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerEcCultSuperBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridClone\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);cloneEntity(row.entity)\" data-ng-disabled=\"newIsDisabled()\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'cultId.name', displayName: 'getALString("Crop", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '23%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!EcCultSuper_itm__cultId_disabled(row.entity) && showLov_EcCultSuper_itm__cultId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='EcCultSuper_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_EcCultSuper_itm__cultId(row.entity)'   data-ng-disabled=\"EcCultSuper_itm__cultId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultId.code', displayName: 'getALString("Crop Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultSuper_itm__cultId_code' data-ng-model='row.entity.cultId.code' data-np-ui-model='row.entity.cultId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultId.cotyId.name', displayName: 'getALString("Cover Type", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '14%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultSuper_itm__cultId_cotyId_name' data-ng-model='row.entity.cultId.cotyId.name' data-np-ui-model='row.entity.cultId.cotyId._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.cotyId.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultId.cotyId.code', displayName: 'getALString("Cover Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultSuper_itm__cultId_cotyId_code' data-ng-model='row.entity.cultId.cotyId.code' data-np-ui-model='row.entity.cultId.cotyId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.cotyId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "modelEcCultSuper", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultSuperBase.prototype.getEntityName = function () {
                return "EcCult";
            };
            ControllerEcCultSuperBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerEcCultSuperBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCult.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.ecgrId = _this.Parent;
                    }
                    else {
                        x.ecgrId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultSuper.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultSuperBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpEcGroup.controller.isEntityLocked(cur.ecgrId, lockKind);
            };
            ControllerEcCultSuperBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerEcCultSuperBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultSuper_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultSuperBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultSuper";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultSuperBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultSuper_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultSuperBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.ecgrId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultSuper";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['ecgrId_ecgrId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerEcCultSuperBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.ecgrId === undefined)
                    return false;
                return x.ecgrId.getKey() === this.Parent.getKey();
            };
            ControllerEcCultSuperBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.ecgrId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroup.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "ecgrId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroup.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultSuperBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCult(
                /*eccuId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*noneMatchDecision:number*/ null, 
                /*rowVersion:number*/ null, 
                /*cultId:Entities.Cultivation*/ null, 
                /*ecgrId:Entities.EcGroup*/ null, 
                /*cotyId:Entities.CoverType*/ null, 
                /*sucaId:Entities.SuperClas*/ null);
                return ret;
            };
            ControllerEcCultSuperBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.ecgrId = self.ecgrId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerEcCultSuperBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerEcCultSuperBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCult.Create();
                ret.updateInstance(src);
                ret.eccuId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelEcCultSuper.modelEcCultDetailSuper.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerEcCultSuperBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultList) {
                    ecCultList.forEach(function (ecCult) {
                        var ecCultCloned = _this.cloneEntity_internal(ecCult, true);
                        ecCultCloned.ecgrId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerEcCultSuperBase.prototype.deleteNewEntitiesUnderParent = function (ecGroup) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCult = change.a;
                    if (ecGroup.getKey() === ecCult.ecgrId.getKey()) {
                        toBeDeleted.push(ecCult);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelEcCultSuper.modelEcCultDetailSuper.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerEcCultSuperBase.prototype.deleteAllEntitiesUnderParent = function (ecGroup, afterDeleteAction) {
                var self = this;
                function deleteEcCultList(ecCultList) {
                    if (ecCultList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultList[0];
                        var tail = ecCultList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultList) {
                    deleteEcCultList(ecCultList);
                }, false, ecGroup);
            };
            Object.defineProperty(ControllerEcCultSuperBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelEcCultSuper.modelEcCultDetailSuper.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultSuperBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelEcCultSuper.modelEcCultDetailSuper.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelEcCultSuper.modelEcCultDetailSuper.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerEcCultSuperBase.prototype.EcCultSuper_itm__cultId_disabled = function (ecCult) {
                var self = this;
                if (ecCult === undefined || ecCult === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCult, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultSuperBase.prototype.showLov_EcCultSuper_itm__cultId = function (ecCult) {
                var self = this;
                if (ecCult === undefined)
                    return;
                var uimodel = ecCult._cultId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Cultivation';
                dialogOptions.previousModel = self.cachedLovModel_EcCultSuper_itm__cultId;
                dialogOptions.className = "ControllerLovCultivation";
                dialogOptions.onSelect = function (selectedEntity) {
                    var cultivation1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(cultivation1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(cultivation1) && cultivation1.isEqual(ecCult.cultId))
                        return;
                    ecCult.cultId = cultivation1;
                    self.markEntityAsUpdated(ecCult, 'EcCultSuper_itm__cultId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_EcCultSuper_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerEcCultSuperBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroup_2_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultSuperBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroup_2_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultSuperBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerEcCultSuperBase.prototype._deleteIsDisabled = function (ecCult) {
                var self = this;
                if (ecCult === null || ecCult === undefined || ecCult.getEntityName() !== "EcCult")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(ecCult, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerEcCultSuperBase.prototype.child_group_EcCultDetailSuper_isInvisible = function () {
                var self = this;
                if ((self.model.modelEcCultDetailSuper === undefined) || (self.model.modelEcCultDetailSuper.controller === undefined))
                    return false;
                return self.model.modelEcCultDetailSuper.controller._isInvisible();
            };
            return ControllerEcCultSuperBase;
        })(Controllers.AbstractGroupTableController);
        EcGroup.ControllerEcCultSuperBase = ControllerEcCultSuperBase;
        // GROUP EcCultDetailSuper
        var ModelEcCultDetailSuperBase = (function (_super) {
            __extends(ModelEcCultDetailSuperBase, _super);
            function ModelEcCultDetailSuperBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "eccdId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].eccdId;
                },
                set: function (eccdId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].eccdId = eccdId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_eccdId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._eccdId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].orderingNumber;
                },
                set: function (orderingNumber_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].orderingNumber = orderingNumber_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._orderingNumber;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar;
                },
                set: function (agreesDeclar_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar = agreesDeclar_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper;
                },
                set: function (comparisonOper_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper = comparisonOper_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres;
                },
                set: function (probabThres_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres = probabThres_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].decisionLight;
                },
                set: function (decisionLight_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].decisionLight = decisionLight_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._decisionLight;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar2;
                },
                set: function (agreesDeclar2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar2 = agreesDeclar2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper2;
                },
                set: function (comparisonOper2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper2 = comparisonOper2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres2;
                },
                set: function (probabThres2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres2 = probabThres2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThresSum;
                },
                set: function (probabThresSum_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThresSum = probabThresSum_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThresSum;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper3;
                },
                set: function (comparisonOper3_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper3 = comparisonOper3_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper3;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].eccuId;
                },
                set: function (eccuId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].eccuId = eccuId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_eccuId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._eccuId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailSuperBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelEcCultDetailSuperBase;
        })(Controllers.AbstractGroupTableModel);
        EcGroup.ModelEcCultDetailSuperBase = ModelEcCultDetailSuperBase;
        var ControllerEcCultDetailSuperBase = (function (_super) {
            __extends(ControllerEcCultDetailSuperBase, _super);
            function ControllerEcCultDetailSuperBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 3 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelEcCultDetailSuper = self.model;
                $scope.EcCultDetailSuper_itm__orderingNumber_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailSuper_itm__orderingNumber_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailSuper_itm__agreesDeclar_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailSuper_itm__agreesDeclar_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailSuper_itm__comparisonOper_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailSuper_itm__comparisonOper_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailSuper_itm__probabThres_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailSuper_itm__probabThres_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailSuper_itm__agreesDeclar2_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailSuper_itm__agreesDeclar2_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailSuper_itm__comparisonOper2_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailSuper_itm__comparisonOper2_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailSuper_itm__probabThres2_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailSuper_itm__probabThres2_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailSuper_itm__comparisonOper3_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailSuper_itm__comparisonOper3_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailSuper_itm__probabThresSum_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailSuper_itm__probabThresSum_disabled(ecCultDetail);
                    };
                $scope.EcCultDetailSuper_itm__decisionLight_disabled =
                    function (ecCultDetail) {
                        return self.EcCultDetailSuper_itm__decisionLight_disabled(ecCultDetail);
                    };
                $scope.pageModel.modelEcCultDetailSuper = $scope.modelEcCultDetailSuper;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelEcCultSuper.modelEcCultDetailSuper = $scope.modelEcCultDetailSuper;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelEcCultSuper.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetail[] , oldVisible:Entities.EcCultDetail[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerEcCultDetailSuperBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerEcCultDetailSuper";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "HtmlDivId", {
                get: function () {
                    return "EcGroup_ControllerEcCultDetailSuper";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.NumberItem(function (ent) { return 'Evaluation \n Order'; }, false, function (ent) { return ent.orderingNumber; }, function (ent) { return ent._orderingNumber; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0),
                            new Controllers.StaticListItem(function (ent) { return '1st \ Prediction '; }, false, function (ent) { return ent.agreesDeclar; }, function (ent) { return ent._agreesDeclar; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.StaticListItem(function (ent) { return 'Comparison \n Operator 1st'; }, false, function (ent) { return ent.comparisonOper; }, function (ent) { return ent._comparisonOper; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Confidence \n Pred. 1st (%)'; }, false, function (ent) { return ent.probabThres; }, function (ent) { return ent._probabThres; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.StaticListItem(function (ent) { return '2nd \ Prediction '; }, false, function (ent) { return ent.agreesDeclar2; }, function (ent) { return ent._agreesDeclar2; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.StaticListItem(function (ent) { return 'Comparison \n Operator 2nd'; }, false, function (ent) { return ent.comparisonOper2; }, function (ent) { return ent._comparisonOper2; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Confidence \n Pred. 2nd (%)'; }, false, function (ent) { return ent.probabThres2; }, function (ent) { return ent._probabThres2; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.StaticListItem(function (ent) { return 'Comparison \n Operator 1st + 2nd'; }, false, function (ent) { return ent.comparisonOper3; }, function (ent) { return ent._comparisonOper3; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Confidence \n Prediction \n 1st + 2nd (%)'; }, false, function (ent) { return ent.probabThresSum; }, function (ent) { return ent._probabThresSum; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.StaticListItem(function (ent) { return 'Traffic \n Light Code'; }, false, function (ent) { return ent.decisionLight; }, function (ent) { return ent._decisionLight; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailSuperBase.prototype.gridTitle = function () {
                return "Criteria";
            };
            ControllerEcCultDetailSuperBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerEcCultDetailSuperBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'orderingNumber', displayName: 'getALString("Evaluation \n Order", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '5%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailSuper_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='EcCultDetailSuper_itm__orderingNumber_disabled(row.entity)' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar', displayName: 'getALString("1st \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='EcCultDetailSuper_itm__agreesDeclar_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper', displayName: 'getALString("Comparison \n Operator 1st", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailSuper_itm__comparisonOper_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres', displayName: 'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailSuper_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='EcCultDetailSuper_itm__probabThres_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar2', displayName: 'getALString("2nd \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='EcCultDetailSuper_itm__agreesDeclar2_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper2', displayName: 'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailSuper_itm__comparisonOper2_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres2', displayName: 'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailSuper_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='EcCultDetailSuper_itm__probabThres2_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper3', displayName: 'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailSuper_itm__comparisonOper3_disabled(row.entity)' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThresSum', displayName: 'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailSuper_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='EcCultDetailSuper_itm__probabThresSum_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'decisionLight', displayName: 'getALString("Traffic \n Light Code", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_EcGroup_W"), resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='EcCultDetailSuper_itm__decisionLight_disabled(row.entity)' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "modelEcCultDetailSuper", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailSuperBase.prototype.getEntityName = function () {
                return "EcCultDetail";
            };
            ControllerEcCultDetailSuperBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerEcCultDetailSuperBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCultDetail.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.eccuId = _this.Parent;
                    }
                    else {
                        x.eccuId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultDetailSuper.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailSuperBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelEcCultSuper.controller.isEntityLocked(cur.eccuId, lockKind);
            };
            ControllerEcCultDetailSuperBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerEcCultDetailSuperBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailSuper_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailSuperBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailSuper";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailSuperBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailSuper_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailSuperBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.eccuId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailSuper";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['eccuId_eccuId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerEcCultDetailSuperBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.eccuId === undefined)
                    return false;
                return x.eccuId.getKey() === this.Parent.getKey();
            };
            ControllerEcCultDetailSuperBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.eccuId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultSuper.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "eccuId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultSuper.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailSuperBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCultDetail(
                /*eccdId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*orderingNumber:number*/ null, 
                /*agreesDeclar:number*/ null, 
                /*comparisonOper:number*/ null, 
                /*probabThres:number*/ null, 
                /*decisionLight:number*/ null, 
                /*rowVersion:number*/ null, 
                /*agreesDeclar2:number*/ null, 
                /*comparisonOper2:number*/ null, 
                /*probabThres2:number*/ null, 
                /*probabThresSum:number*/ null, 
                /*comparisonOper3:number*/ null, 
                /*eccuId:Entities.EcCult*/ null);
                this.getMergedItems(function (items) {
                    ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                }, false);
                return ret;
            };
            ControllerEcCultDetailSuperBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.eccuId = self.eccuId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerEcCultDetailSuperBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerEcCultDetailSuperBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCultDetail.Create();
                ret.updateInstance(src);
                ret.eccdId = tmpId;
                if (!calledByParent) {
                    this.getMergedItems(function (items) {
                        ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                    }, false);
                }
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerEcCultDetailSuperBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultDetailList) {
                    ecCultDetailList.forEach(function (ecCultDetail) {
                        var ecCultDetailCloned = _this.cloneEntity_internal(ecCultDetail, true);
                        ecCultDetailCloned.eccuId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerEcCultDetailSuperBase.prototype.deleteNewEntitiesUnderParent = function (ecCult) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCultDetail = change.a;
                    if (ecCult.getKey() === ecCultDetail.eccuId.getKey()) {
                        toBeDeleted.push(ecCultDetail);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerEcCultDetailSuperBase.prototype.deleteAllEntitiesUnderParent = function (ecCult, afterDeleteAction) {
                var self = this;
                function deleteEcCultDetailList(ecCultDetailList) {
                    if (ecCultDetailList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultDetailList[0];
                        var tail = ecCultDetailList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultDetailList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultDetailList) {
                    deleteEcCultDetailList(ecCultDetailList);
                }, false, ecCult);
            };
            Object.defineProperty(ControllerEcCultDetailSuperBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailSuperBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerEcCultDetailSuperBase.prototype.EcCultDetailSuper_itm__orderingNumber_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailSuperBase.prototype.EcCultDetailSuper_itm__agreesDeclar_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailSuperBase.prototype.EcCultDetailSuper_itm__comparisonOper_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailSuperBase.prototype.EcCultDetailSuper_itm__probabThres_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailSuperBase.prototype.EcCultDetailSuper_itm__agreesDeclar2_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailSuperBase.prototype.EcCultDetailSuper_itm__comparisonOper2_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailSuperBase.prototype.EcCultDetailSuper_itm__probabThres2_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailSuperBase.prototype.EcCultDetailSuper_itm__comparisonOper3_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailSuperBase.prototype.EcCultDetailSuper_itm__probabThresSum_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailSuperBase.prototype.EcCultDetailSuper_itm__decisionLight_disabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === undefined || ecCultDetail === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerEcCultDetailSuperBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultDetailSuperBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultDetailSuperBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerEcCultDetailSuperBase.prototype._deleteIsDisabled = function (ecCultDetail) {
                var self = this;
                if (ecCultDetail === null || ecCultDetail === undefined || ecCultDetail.getEntityName() !== "EcCultDetail")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(ecCultDetail, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            return ControllerEcCultDetailSuperBase;
        })(Controllers.AbstractGroupTableController);
        EcGroup.ControllerEcCultDetailSuperBase = ControllerEcCultDetailSuperBase;
    })(EcGroup = Controllers.EcGroup || (Controllers.EcGroup = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=EcGroupBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
