var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/AgrisnapUsersBase.ts" />
/// <reference path="../EntitiesBase/AgrisnapUsersProducersBase.ts" />
/// <reference path="../EntitiesBase/ProducersBase.ts" />
/// <reference path="LovProducersBase.ts" />
/// <reference path="../Controllers/AgrisnapUsers.ts" />
var Controllers;
(function (Controllers) {
    var AgrisnapUsers;
    (function (AgrisnapUsers) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        AgrisnapUsers.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $scope.onNewBtnAction = function () {
                    self.onNewBtnAction();
                };
                $scope.onDeleteBtnAction = function () {
                    self.onDeleteBtnAction();
                };
                $timeout(function () {
                    $('#AgrisnapUsers').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "AgrisnapUsers";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Agrisnap Users";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpAgrisnapUsers.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpAgrisnapUsers.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpAgrisnapUsersProducers.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpAgrisnapUsers.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpAgrisnapUsersProducers.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpAgrisnapUsers.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpAgrisnapUsers.controller,
                            self.model.modelGrpAgrisnapUsersProducers.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                    pageChanges = pageChanges.concat(self.model.modelGrpAgrisnapUsers.controller.getChangesToCommitForDeletion());
                    pageChanges = pageChanges.concat(self.model.modelGrpAgrisnapUsers.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpAgrisnapUsersProducers.controller.getDeleteChangesToCommit());
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpAgrisnapUsers.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpAgrisnapUsersProducers.controller.getChangesToCommit());
                }
                var hasAgrisnapUsers = pageChanges.some(function (change) { return change.entityName === "AgrisnapUsers"; });
                if (!hasAgrisnapUsers) {
                    var validateEntity = new Controllers.ChangeToCommit(Controllers.ChangeStatus.UPDATE, 0, "AgrisnapUsers", self.model.modelGrpAgrisnapUsers.controller.Current);
                    pageChanges = pageChanges.concat(validateEntity);
                }
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_AgrisnapUsers";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpAgrisnapUsers.controller.cleanUpAfterSave();
                self.model.modelGrpAgrisnapUsersProducers.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = self.$scope.getCurrentPageModel();
                        var newAgrisnapUsersId = response.newEntitiesIds.firstOrNull(function (x) { return x.entityName === 'AgrisnapUsers'; });
                        if (!isVoid(newAgrisnapUsersId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.AgrisnapUsers.CreateById(newAgrisnapUsersId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.getInitialEntity = function () {
                if (this.shownAsDialog()) {
                    return this.dialogSelectedEntity();
                }
                else {
                    var breadCrumbStepModel = this.$scope.getPageModelByURL('/AgrisnapUsers');
                    if (isVoid(breadCrumbStepModel)) {
                        return null;
                    }
                    else {
                        return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                    }
                }
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            PageControllerBase.prototype.onNewBtnAction = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                self.addNewRecord();
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () { self.addNewRecord(); }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    self.addNewRecord();
                }
            };
            PageControllerBase.prototype.addNewRecord = function () {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                self.model.modelGrpAgrisnapUsers.controller.cleanUpAfterSave();
                self.model.modelGrpAgrisnapUsersProducers.controller.cleanUpAfterSave();
                self.model.modelGrpAgrisnapUsers.controller.createNewEntityAndAddToUI();
            };
            PageControllerBase.prototype.onDeleteBtnAction = function () {
                var self = this;
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                    new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(); }),
                    new Tuple2("MessageBox_Button_No", function () { })
                ], 1, 1);
            };
            PageControllerBase.prototype.deleteRecord = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW) {
                    console.log("Delete pressed in a form while being in new mode!");
                    return;
                }
                NpTypes.AlertMessage.clearAlerts(self.model);
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData(true);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "Mode", {
                get: function () {
                    var self = this;
                    if (isVoid(self.model.modelGrpAgrisnapUsers) || isVoid(self.model.modelGrpAgrisnapUsers.controller))
                        return Controllers.EditMode.NEW;
                    return self.model.modelGrpAgrisnapUsers.controller.Mode;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._newIsDisabled = function () {
                var self = this;
                if (isVoid(self.model.modelGrpAgrisnapUsers) || isVoid(self.model.modelGrpAgrisnapUsers.controller))
                    return true;
                return self.model.modelGrpAgrisnapUsers.controller._newIsDisabled();
            };
            PageControllerBase.prototype._deleteIsDisabled = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW)
                    return true;
                if (isVoid(self.model.modelGrpAgrisnapUsers) || isVoid(self.model.modelGrpAgrisnapUsers.controller))
                    return true;
                return self.model.modelGrpAgrisnapUsers.controller._deleteIsDisabled(self.model.modelGrpAgrisnapUsers.controller.Current);
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        AgrisnapUsers.PageControllerBase = PageControllerBase;
        // GROUP GrpAgrisnapUsers
        var ModelGrpAgrisnapUsersBase = (function (_super) {
            __extends(ModelGrpAgrisnapUsersBase, _super);
            function ModelGrpAgrisnapUsersBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "agrisnapName", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agrisnapName;
                },
                set: function (agrisnapName_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agrisnapName = agrisnapName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_agrisnapName", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agrisnapName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "agrisnapUsersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agrisnapUsersId;
                },
                set: function (agrisnapUsersId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agrisnapUsersId = agrisnapUsersId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_agrisnapUsersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agrisnapUsersId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "agrisnapUid", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agrisnapUid;
                },
                set: function (agrisnapUid_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agrisnapUid = agrisnapUid_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_agrisnapUid", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agrisnapUid;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpAgrisnapUsersBase;
        })(Controllers.AbstractGroupFormModel);
        AgrisnapUsers.ModelGrpAgrisnapUsersBase = ModelGrpAgrisnapUsersBase;
        var ControllerGrpAgrisnapUsersBase = (function (_super) {
            __extends(ControllerGrpAgrisnapUsersBase, _super);
            function ControllerGrpAgrisnapUsersBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                $scope.modelGrpAgrisnapUsers = self.model;
                var selectedEntity = this.$scope.pageModel.controller.getInitialEntity();
                if (isVoid(selectedEntity)) {
                    self.createNewEntityAndAddToUI();
                }
                else {
                    var clonedEntity = Entities.AgrisnapUsers.Create();
                    clonedEntity.updateInstance(selectedEntity);
                    $scope.modelGrpAgrisnapUsers.visibleEntities[0] = clonedEntity;
                    $scope.modelGrpAgrisnapUsers.selectedEntities[0] = clonedEntity;
                }
                $scope.GrpAgrisnapUsers_itm__agrisnapName_disabled =
                    function (agrisnapUsers) {
                        return self.GrpAgrisnapUsers_itm__agrisnapName_disabled(agrisnapUsers);
                    };
                $scope.GrpAgrisnapUsers_itm__agrisnapUid_disabled =
                    function (agrisnapUsers) {
                        return self.GrpAgrisnapUsers_itm__agrisnapUid_disabled(agrisnapUsers);
                    };
                $scope.GrpAgrisnapUsers_0_disabled =
                    function () {
                        return self.GrpAgrisnapUsers_0_disabled();
                    };
                $scope.GrpAgrisnapUsers_0_invisible =
                    function () {
                        return self.GrpAgrisnapUsers_0_invisible();
                    };
                $scope.child_group_GrpAgrisnapUsersProducers_isInvisible =
                    function () {
                        return self.child_group_GrpAgrisnapUsersProducers_isInvisible();
                    };
                $scope.pageModel.modelGrpAgrisnapUsers = $scope.modelGrpAgrisnapUsers;
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.AgrisnapUsers[] , oldVisible:Entities.AgrisnapUsers[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                //bind entity child collection with child controller merged items
                Entities.AgrisnapUsers.agrisnapUsersProducersCollection = function (agrisnapUsers, func) {
                    _this.model.modelGrpAgrisnapUsersProducers.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, agrisnapUsers);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.AgrisnapUsers.agrisnapUsersProducersCollection = null; //(agrisnapUsers, func) => { }
                });
            }
            ControllerGrpAgrisnapUsersBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpAgrisnapUsersBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpAgrisnapUsers";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgrisnapUsersBase.prototype, "HtmlDivId", {
                get: function () {
                    return "AgrisnapUsers_ControllerGrpAgrisnapUsers";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgrisnapUsersBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Full Name'; }, false, function (ent) { return ent.agrisnapName; }, function (ent) { return ent._agrisnapName; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'AgriSnap UserId'; }, false, function (ent) { return ent.agrisnapUid; }, function (ent) { return ent._agrisnapUid; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgrisnapUsersBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgrisnapUsersBase.prototype, "modelGrpAgrisnapUsers", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgrisnapUsersBase.prototype.getEntityName = function () {
                return "AgrisnapUsers";
            };
            ControllerGrpAgrisnapUsersBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
            };
            ControllerGrpAgrisnapUsersBase.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
                var self = this;
                if (self.$scope.pageModel.controller.shownAsDialog())
                    return;
                var breadCrumbStepModel = { selectedEntity: null };
                if (!isVoid(self.Current)) {
                    var selectedEntity = Entities.AgrisnapUsers.Create();
                    selectedEntity.updateInstance(self.Current);
                    breadCrumbStepModel.selectedEntity = selectedEntity;
                }
                else {
                    breadCrumbStepModel.selectedEntity = null;
                }
                self.$scope.setCurrentPageModel(breadCrumbStepModel);
            };
            ControllerGrpAgrisnapUsersBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.AgrisnapUsers.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpAgrisnapUsersBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpAgrisnapUsers.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgrisnapUsersBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpAgrisnapUsersBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.AgrisnapUsers(
                /*agrisnapName:string*/ null, 
                /*rowVersion:number*/ null, 
                /*agrisnapUsersId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*agrisnapUid:string*/ null);
                return ret;
            };
            ControllerGrpAgrisnapUsersBase.prototype.createNewEntityAndAddToUI = function () {
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.$scope.modelGrpAgrisnapUsers.visibleEntities[0] = newEnt;
                self.$scope.modelGrpAgrisnapUsers.selectedEntities[0] = newEnt;
                return newEnt;
            };
            ControllerGrpAgrisnapUsersBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpAgrisnapUsersBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.AgrisnapUsers.Create();
                ret.updateInstance(src);
                ret.agrisnapUsersId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.visibleEntities[0] = ret;
                this.model.selectedEntities[0] = ret;
                this.$timeout(function () {
                    _this.modelGrpAgrisnapUsers.modelGrpAgrisnapUsersProducers.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpAgrisnapUsersBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpAgrisnapUsers.modelGrpAgrisnapUsersProducers.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgrisnapUsersBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpAgrisnapUsers.modelGrpAgrisnapUsersProducers.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpAgrisnapUsers.modelGrpAgrisnapUsersProducers.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpAgrisnapUsersBase.prototype.GrpAgrisnapUsers_itm__agrisnapName_disabled = function (agrisnapUsers) {
                var self = this;
                if (agrisnapUsers === undefined || agrisnapUsers === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(agrisnapUsers, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpAgrisnapUsersBase.prototype.GrpAgrisnapUsers_itm__agrisnapUid_disabled = function (agrisnapUsers) {
                var self = this;
                if (agrisnapUsers === undefined || agrisnapUsers === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(agrisnapUsers, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpAgrisnapUsersBase.prototype.GrpAgrisnapUsers_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgrisnapUsersBase.prototype.GrpAgrisnapUsers_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgrisnapUsersBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgrisnapUsersBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgrisnapUsersBase.prototype._newIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                    return true; // no write privilege
                var parEntityIsLocked = false;
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpAgrisnapUsersBase.prototype._deleteIsDisabled = function (agrisnapUsers) {
                var self = this;
                if (agrisnapUsers === null || agrisnapUsers === undefined || agrisnapUsers.getEntityName() !== "AgrisnapUsers")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(agrisnapUsers, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpAgrisnapUsersBase.prototype.child_group_GrpAgrisnapUsersProducers_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpAgrisnapUsersProducers === undefined) || (self.model.modelGrpAgrisnapUsersProducers.controller === undefined))
                    return false;
                return self.model.modelGrpAgrisnapUsersProducers.controller._isInvisible();
            };
            return ControllerGrpAgrisnapUsersBase;
        })(Controllers.AbstractGroupFormController);
        AgrisnapUsers.ControllerGrpAgrisnapUsersBase = ControllerGrpAgrisnapUsersBase;
        // GROUP GrpAgrisnapUsersProducers
        var ModelGrpAgrisnapUsersProducersBase = (function (_super) {
            __extends(ModelGrpAgrisnapUsersProducersBase, _super);
            function ModelGrpAgrisnapUsersProducersBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_producersId = new NpTypes.UIManyToOneModel(undefined);
                this._fsch_dteRelCreated = new NpTypes.UIDateModel(undefined);
                this._fsch_dteRelCanceled = new NpTypes.UIDateModel(undefined);
            }
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "dteRelCreated", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteRelCreated;
                },
                set: function (dteRelCreated_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteRelCreated = dteRelCreated_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_dteRelCreated", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteRelCreated;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "dteRelCanceled", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteRelCanceled;
                },
                set: function (dteRelCanceled_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteRelCanceled = dteRelCanceled_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_dteRelCanceled", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteRelCanceled;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "agrisnapUsersProducersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agrisnapUsersProducersId;
                },
                set: function (agrisnapUsersProducersId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agrisnapUsersProducersId = agrisnapUsersProducersId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_agrisnapUsersProducersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agrisnapUsersProducersId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "agrisnapUsersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agrisnapUsersId;
                },
                set: function (agrisnapUsersId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agrisnapUsersId = agrisnapUsersId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_agrisnapUsersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agrisnapUsersId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "producersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].producersId;
                },
                set: function (producersId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].producersId = producersId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_producersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._producersId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "fsch_producersId", {
                get: function () {
                    return this._fsch_producersId.value;
                },
                set: function (vl) {
                    this._fsch_producersId.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "fsch_dteRelCreated", {
                get: function () {
                    return this._fsch_dteRelCreated.value;
                },
                set: function (vl) {
                    this._fsch_dteRelCreated.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "fsch_dteRelCanceled", {
                get: function () {
                    return this._fsch_dteRelCanceled.value;
                },
                set: function (vl) {
                    this._fsch_dteRelCanceled.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgrisnapUsersProducersBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpAgrisnapUsersProducersBase;
        })(Controllers.AbstractGroupTableModel);
        AgrisnapUsers.ModelGrpAgrisnapUsersProducersBase = ModelGrpAgrisnapUsersProducersBase;
        var ControllerGrpAgrisnapUsersProducersBase = (function (_super) {
            __extends(ControllerGrpAgrisnapUsersProducersBase, _super);
            function ControllerGrpAgrisnapUsersProducersBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpAgrisnapUsersProducers = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpAgrisnapUsersProducers_srchr__fsch_producersId_disabled =
                    function (agrisnapUsersProducers) {
                        return self.GrpAgrisnapUsersProducers_srchr__fsch_producersId_disabled(agrisnapUsersProducers);
                    };
                $scope.showLov_GrpAgrisnapUsersProducers_srchr__fsch_producersId =
                    function () {
                        $timeout(function () {
                            self.showLov_GrpAgrisnapUsersProducers_srchr__fsch_producersId();
                        }, 0);
                    };
                $scope.GrpAgrisnapUsersProducers_srchr__fsch_dteRelCreated_disabled =
                    function (agrisnapUsersProducers) {
                        return self.GrpAgrisnapUsersProducers_srchr__fsch_dteRelCreated_disabled(agrisnapUsersProducers);
                    };
                $scope.GrpAgrisnapUsersProducers_srchr__fsch_dteRelCanceled_disabled =
                    function (agrisnapUsersProducers) {
                        return self.GrpAgrisnapUsersProducers_srchr__fsch_dteRelCanceled_disabled(agrisnapUsersProducers);
                    };
                $scope.GrpAgrisnapUsersProducers_itm__producersId_disabled =
                    function (agrisnapUsersProducers) {
                        return self.GrpAgrisnapUsersProducers_itm__producersId_disabled(agrisnapUsersProducers);
                    };
                $scope.showLov_GrpAgrisnapUsersProducers_itm__producersId =
                    function (agrisnapUsersProducers) {
                        $timeout(function () {
                            self.showLov_GrpAgrisnapUsersProducers_itm__producersId(agrisnapUsersProducers);
                        }, 0);
                    };
                $scope.GrpAgrisnapUsersProducers_itm__dteRelCreated_disabled =
                    function (agrisnapUsersProducers) {
                        return self.GrpAgrisnapUsersProducers_itm__dteRelCreated_disabled(agrisnapUsersProducers);
                    };
                $scope.GrpAgrisnapUsersProducers_itm__dteRelCanceled_disabled =
                    function (agrisnapUsersProducers) {
                        return self.GrpAgrisnapUsersProducers_itm__dteRelCanceled_disabled(agrisnapUsersProducers);
                    };
                $scope.pageModel.modelGrpAgrisnapUsersProducers = $scope.modelGrpAgrisnapUsersProducers;
                $scope.clearBtnAction = function () {
                    self.modelGrpAgrisnapUsersProducers.fsch_producersId = undefined;
                    self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated = undefined;
                    self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpAgrisnapUsers.modelGrpAgrisnapUsersProducers = $scope.modelGrpAgrisnapUsersProducers;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpAgrisnapUsers.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.AgrisnapUsersProducers[] , oldVisible:Entities.AgrisnapUsersProducers[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpAgrisnapUsersProducersBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpAgrisnapUsersProducers";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "HtmlDivId", {
                get: function () {
                    return "AgrisnapUsers_ControllerGrpAgrisnapUsersProducers";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.LovItem(function (ent) { return 'Producer'; }, true, function (ent) { return self.model.fsch_producersId; }, function (ent) { return self.model._fsch_producersId; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.DateItem(function (ent) { return 'Relation Created'; }, true, function (ent) { return self.model.fsch_dteRelCreated; }, function (ent) { return self.model._fsch_dteRelCreated; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.DateItem(function (ent) { return 'Relation Ended'; }, true, function (ent) { return self.model.fsch_dteRelCanceled; }, function (ent) { return self.model._fsch_dteRelCanceled; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.LovItem(function (ent) { return 'Producer Code'; }, false, function (ent) { return ent.producersId; }, function (ent) { return ent._producersId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.DateItem(function (ent) { return 'Relation Created'; }, false, function (ent) { return ent.dteRelCreated; }, function (ent) { return ent._dteRelCreated; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.DateItem(function (ent) { return 'Relation Ended'; }, false, function (ent) { return ent.dteRelCanceled; }, function (ent) { return ent._dteRelCanceled; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgrisnapUsersProducersBase.prototype.gridTitle = function () {
                return "Agrisnap Users and Producers";
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'producersId.prodName', displayName: 'getALString("Producer Code", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpAgrisnapUsersProducers_itm__producersId_disabled(row.entity) && showLov_GrpAgrisnapUsersProducers_itm__producersId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.producersId.prodName' data-np-ui-model='row.entity._producersId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpAgrisnapUsersProducers_itm__producersId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpAgrisnapUsersProducers_itm__producersId(row.entity)'   data-ng-disabled=\"GrpAgrisnapUsersProducers_itm__producersId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteRelCreated', displayName: 'getALString("Relation Created", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpAgrisnapUsersProducers_itm__dteRelCreated' data-ng-model='row.entity.dteRelCreated' data-np-ui-model='row.entity._dteRelCreated' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteRelCreated&quot;)' data-ng-readonly='GrpAgrisnapUsersProducers_itm__dteRelCreated_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteRelCanceled', displayName: 'getALString("Relation Ended", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpAgrisnapUsersProducers_itm__dteRelCanceled' data-ng-model='row.entity.dteRelCanceled' data-np-ui-model='row.entity._dteRelCanceled' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteRelCanceled&quot;)' data-ng-readonly='GrpAgrisnapUsersProducers_itm__dteRelCanceled_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "modelGrpAgrisnapUsersProducers", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgrisnapUsersProducersBase.prototype.getEntityName = function () {
                return "AgrisnapUsersProducers";
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.AgrisnapUsersProducers.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.agrisnapUsersId = _this.Parent;
                    }
                    else {
                        x.agrisnapUsersId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpAgrisnapUsersProducers.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgrisnapUsersProducersBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpAgrisnapUsers.controller.isEntityLocked(cur.agrisnapUsersId, lockKind);
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.agrisnapUsersId)) {
                    paramData['agrisnapUsersId_agrisnapUsersId'] = self.Parent.agrisnapUsersId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated)) {
                    paramData['fsch_dteRelCreated'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled)) {
                    paramData['fsch_dteRelCanceled'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId)) {
                    paramData['fsch_producersId_producersId'] = self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "AgrisnapUsersProducers/findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.agrisnapUsersId)) {
                    paramData['agrisnapUsersId_agrisnapUsersId'] = self.Parent.agrisnapUsersId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated)) {
                    paramData['fsch_dteRelCreated'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled)) {
                    paramData['fsch_dteRelCanceled'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId)) {
                    paramData['fsch_producersId_producersId'] = self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "AgrisnapUsersProducers/findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.agrisnapUsersId)) {
                    paramData['agrisnapUsersId_agrisnapUsersId'] = self.Parent.agrisnapUsersId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated)) {
                    paramData['fsch_dteRelCreated'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled)) {
                    paramData['fsch_dteRelCanceled'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId)) {
                    paramData['fsch_producersId_producersId'] = self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "AgrisnapUsersProducers/findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.agrisnapUsersId)) {
                    paramData['agrisnapUsersId_agrisnapUsersId'] = self.Parent.agrisnapUsersId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated)) {
                    paramData['fsch_dteRelCreated'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCreated;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled)) {
                    paramData['fsch_dteRelCanceled'] = self.modelGrpAgrisnapUsersProducers.fsch_dteRelCanceled;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgrisnapUsersProducers) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId) && !isVoid(self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId)) {
                    paramData['fsch_producersId_producersId'] = self.modelGrpAgrisnapUsersProducers.fsch_producersId.producersId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.agrisnapUsersId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "AgrisnapUsersProducers/findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['agrisnapUsersId_agrisnapUsersId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.agrisnapUsersId === undefined)
                    return false;
                return x.agrisnapUsersId.getKey() === this.Parent.getKey();
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.agrisnapUsersId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpAgrisnapUsers.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "agrisnapUsersId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpAgrisnapUsers.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgrisnapUsersProducersBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.AgrisnapUsersProducers(
                /*dteRelCreated:Date*/ null, 
                /*dteRelCanceled:Date*/ null, 
                /*agrisnapUsersProducersId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*rowVersion:number*/ null, 
                /*agrisnapUsersId:Entities.AgrisnapUsers*/ null, 
                /*producersId:Entities.Producers*/ null);
                return ret;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.agrisnapUsersId = self.agrisnapUsersId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.AgrisnapUsersProducers.Create();
                ret.updateInstance(src);
                ret.agrisnapUsersProducersId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (agrisnapUsersProducersList) {
                    agrisnapUsersProducersList.forEach(function (agrisnapUsersProducers) {
                        var agrisnapUsersProducersCloned = _this.cloneEntity_internal(agrisnapUsersProducers, true);
                        agrisnapUsersProducersCloned.agrisnapUsersId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.deleteNewEntitiesUnderParent = function (agrisnapUsers) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var agrisnapUsersProducers = change.a;
                    if (agrisnapUsers.getKey() === agrisnapUsersProducers.agrisnapUsersId.getKey()) {
                        toBeDeleted.push(agrisnapUsersProducers);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.deleteAllEntitiesUnderParent = function (agrisnapUsers, afterDeleteAction) {
                var self = this;
                function deleteAgrisnapUsersProducersList(agrisnapUsersProducersList) {
                    if (agrisnapUsersProducersList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = agrisnapUsersProducersList[0];
                        var tail = agrisnapUsersProducersList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteAgrisnapUsersProducersList(tail);
                        });
                    }
                }
                this.getMergedItems(function (agrisnapUsersProducersList) {
                    deleteAgrisnapUsersProducersList(agrisnapUsersProducersList);
                }, false, agrisnapUsers);
            };
            Object.defineProperty(ControllerGrpAgrisnapUsersProducersBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgrisnapUsersProducersBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.GrpAgrisnapUsersProducers_srchr__fsch_producersId_disabled = function (agrisnapUsersProducers) {
                var self = this;
                return false;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.showLov_GrpAgrisnapUsersProducers_srchr__fsch_producersId = function () {
                var self = this;
                var uimodel = self.modelGrpAgrisnapUsersProducers._fsch_producersId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Producers';
                dialogOptions.previousModel = self.cachedLovModel_GrpAgrisnapUsersProducers_srchr__fsch_producersId;
                dialogOptions.className = "ControllerLovProducers";
                dialogOptions.onSelect = function (selectedEntity) {
                    var producers1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(producers1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(producers1) && producers1.isEqual(self.modelGrpAgrisnapUsersProducers.fsch_producersId))
                        return;
                    self.modelGrpAgrisnapUsersProducers.fsch_producersId = producers1;
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpAgrisnapUsersProducers_srchr__fsch_producersId = lovModel;
                    var wsPath = "Producers/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Producers/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['prodName'] = true;
                dialogOptions.shownCols['prodCode'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Producers.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.GrpAgrisnapUsersProducers_srchr__fsch_dteRelCreated_disabled = function (agrisnapUsersProducers) {
                var self = this;
                return false;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.GrpAgrisnapUsersProducers_srchr__fsch_dteRelCanceled_disabled = function (agrisnapUsersProducers) {
                var self = this;
                return false;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.GrpAgrisnapUsersProducers_itm__producersId_disabled = function (agrisnapUsersProducers) {
                var self = this;
                if (agrisnapUsersProducers === undefined || agrisnapUsersProducers === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(agrisnapUsersProducers, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.showLov_GrpAgrisnapUsersProducers_itm__producersId = function (agrisnapUsersProducers) {
                var self = this;
                if (agrisnapUsersProducers === undefined)
                    return;
                var uimodel = agrisnapUsersProducers._producersId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Producers';
                dialogOptions.previousModel = self.cachedLovModel_GrpAgrisnapUsersProducers_itm__producersId;
                dialogOptions.className = "ControllerLovProducers";
                dialogOptions.onSelect = function (selectedEntity) {
                    var producers1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(producers1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(producers1) && producers1.isEqual(agrisnapUsersProducers.producersId))
                        return;
                    agrisnapUsersProducers.producersId = producers1;
                    self.markEntityAsUpdated(agrisnapUsersProducers, 'GrpAgrisnapUsersProducers_itm__producersId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpAgrisnapUsersProducers_itm__producersId = lovModel;
                    var wsPath = "Producers/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Producers/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodName)) {
                        paramData['fsch_ProducersLov_prodName'] = lovModel.fsch_ProducersLov_prodName;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ProducersLov_prodCode)) {
                        paramData['fsch_ProducersLov_prodCode'] = lovModel.fsch_ProducersLov_prodCode;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['prodName'] = true;
                dialogOptions.shownCols['prodCode'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Producers.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.GrpAgrisnapUsersProducers_itm__dteRelCreated_disabled = function (agrisnapUsersProducers) {
                var self = this;
                if (agrisnapUsersProducers === undefined || agrisnapUsersProducers === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(agrisnapUsersProducers, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype.GrpAgrisnapUsersProducers_itm__dteRelCanceled_disabled = function (agrisnapUsersProducers) {
                var self = this;
                if (agrisnapUsersProducers === undefined || agrisnapUsersProducers === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(agrisnapUsersProducers, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpAgrisnapUsers_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpAgrisnapUsers_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpAgrisnapUsersProducersBase.prototype._deleteIsDisabled = function (agrisnapUsersProducers) {
                var self = this;
                if (agrisnapUsersProducers === null || agrisnapUsersProducers === undefined || agrisnapUsersProducers.getEntityName() !== "AgrisnapUsersProducers")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_AgrisnapUsers_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(agrisnapUsersProducers, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            return ControllerGrpAgrisnapUsersProducersBase;
        })(Controllers.AbstractGroupTableController);
        AgrisnapUsers.ControllerGrpAgrisnapUsersProducersBase = ControllerGrpAgrisnapUsersProducersBase;
    })(AgrisnapUsers = Controllers.AgrisnapUsers || (Controllers.AgrisnapUsers = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=AgrisnapUsersBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
