var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="../EntitiesBase/ClassifierBase.ts" />
/// <reference path="LovClassifierBase.ts" />
/// <reference path="../EntitiesBase/FileTemplateBase.ts" />
/// <reference path="LovFileTemplateBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="LovCoverTypeBase.ts" />
/// <reference path="../Controllers/Classification.ts" />
var Controllers;
(function (Controllers) {
    var Classification;
    (function (Classification) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        Classification.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $scope.onNewBtnAction = function () {
                    self.onNewBtnAction();
                };
                $scope.onDeleteBtnAction = function () {
                    self.onDeleteBtnAction();
                };
                $timeout(function () {
                    $('#Classification').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Classification";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Classification";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpClas.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpClas.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelClas.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpClas.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelClas.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpClas.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpClas.controller,
                            self.model.modelGrpParcelClas.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                    pageChanges = pageChanges.concat(self.model.modelGrpClas.controller.getChangesToCommitForDeletion());
                    pageChanges = pageChanges.concat(self.model.modelGrpClas.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getDeleteChangesToCommit());
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpClas.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelClas.controller.getChangesToCommit());
                }
                var hasClassification = pageChanges.some(function (change) { return change.entityName === "Classification"; });
                if (!hasClassification) {
                    var validateEntity = new Controllers.ChangeToCommit(Controllers.ChangeStatus.UPDATE, 0, "Classification", self.model.modelGrpClas.controller.Current);
                    pageChanges = pageChanges.concat(validateEntity);
                }
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_Classification";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpClas.controller.cleanUpAfterSave();
                self.model.modelGrpParcelClas.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = self.$scope.getCurrentPageModel();
                        var newClassificationId = response.newEntitiesIds.firstOrNull(function (x) { return x.entityName === 'Classification'; });
                        if (!isVoid(newClassificationId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.Classification.CreateById(newClassificationId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.getInitialEntity = function () {
                if (this.shownAsDialog()) {
                    return this.dialogSelectedEntity();
                }
                else {
                    var breadCrumbStepModel = this.$scope.getPageModelByURL('/Classification');
                    if (isVoid(breadCrumbStepModel)) {
                        return null;
                    }
                    else {
                        return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                    }
                }
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            PageControllerBase.prototype.onNewBtnAction = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                self.addNewRecord();
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () { self.addNewRecord(); }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    self.addNewRecord();
                }
            };
            PageControllerBase.prototype.addNewRecord = function () {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                self.model.modelGrpClas.controller.cleanUpAfterSave();
                self.model.modelGrpParcelClas.controller.cleanUpAfterSave();
                self.model.modelGrpClas.controller.createNewEntityAndAddToUI();
            };
            PageControllerBase.prototype.onDeleteBtnAction = function () {
                var self = this;
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                    new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(); }),
                    new Tuple2("MessageBox_Button_No", function () { })
                ], 1, 1);
            };
            PageControllerBase.prototype.deleteRecord = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW) {
                    console.log("Delete pressed in a form while being in new mode!");
                    return;
                }
                NpTypes.AlertMessage.clearAlerts(self.model);
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData(true);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "Mode", {
                get: function () {
                    var self = this;
                    if (isVoid(self.model.modelGrpClas) || isVoid(self.model.modelGrpClas.controller))
                        return Controllers.EditMode.NEW;
                    return self.model.modelGrpClas.controller.Mode;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._newIsDisabled = function () {
                var self = this;
                if (isVoid(self.model.modelGrpClas) || isVoid(self.model.modelGrpClas.controller))
                    return true;
                return self.model.modelGrpClas.controller._newIsDisabled();
            };
            PageControllerBase.prototype._deleteIsDisabled = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW)
                    return true;
                if (isVoid(self.model.modelGrpClas) || isVoid(self.model.modelGrpClas.controller))
                    return true;
                return self.model.modelGrpClas.controller._deleteIsDisabled(self.model.modelGrpClas.controller.Current);
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        Classification.PageControllerBase = PageControllerBase;
        // GROUP GrpClas
        var ModelGrpClasBase = (function (_super) {
            __extends(ModelGrpClasBase, _super);
            function ModelGrpClasBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpClasBase.prototype, "clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].clasId;
                },
                set: function (clasId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].clasId = clasId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._clasId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].name;
                },
                set: function (name_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].name = name_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "description", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].description;
                },
                set: function (description_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].description = description_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_description", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._description;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "dateTime", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dateTime;
                },
                set: function (dateTime_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dateTime = dateTime_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_dateTime", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dateTime;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "recordtype", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].recordtype;
                },
                set: function (recordtype_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].recordtype = recordtype_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_recordtype", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._recordtype;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "filePath", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].filePath;
                },
                set: function (filePath_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].filePath = filePath_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_filePath", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._filePath;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "attachedFile", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].attachedFile;
                },
                set: function (attachedFile_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].attachedFile = attachedFile_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_attachedFile", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._attachedFile;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "year", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].year;
                },
                set: function (year_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].year = year_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_year", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._year;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "isImported", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].isImported;
                },
                set: function (isImported_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].isImported = isImported_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_isImported", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._isImported;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "clfrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].clfrId;
                },
                set: function (clfrId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].clfrId = clfrId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_clfrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._clfrId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "fiteId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].fiteId;
                },
                set: function (fiteId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].fiteId = fiteId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_fiteId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._fiteId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpClasBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpClasBase;
        })(Controllers.AbstractGroupFormModel);
        Classification.ModelGrpClasBase = ModelGrpClasBase;
        var ControllerGrpClasBase = (function (_super) {
            __extends(ControllerGrpClasBase, _super);
            function ControllerGrpClasBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                $scope.modelGrpClas = self.model;
                var selectedEntity = this.$scope.pageModel.controller.getInitialEntity();
                if (isVoid(selectedEntity)) {
                    self.createNewEntityAndAddToUI();
                }
                else {
                    var clonedEntity = Entities.Classification.Create();
                    clonedEntity.updateInstance(selectedEntity);
                    $scope.modelGrpClas.visibleEntities[0] = clonedEntity;
                    $scope.modelGrpClas.selectedEntities[0] = clonedEntity;
                }
                $scope.GrpClas_itm__name_disabled =
                    function (classification) {
                        return self.GrpClas_itm__name_disabled(classification);
                    };
                $scope.GrpClas_itm__clfrId_disabled =
                    function (classification) {
                        return self.GrpClas_itm__clfrId_disabled(classification);
                    };
                $scope.showLov_GrpClas_itm__clfrId =
                    function (classification) {
                        $timeout(function () {
                            self.showLov_GrpClas_itm__clfrId(classification);
                        }, 0);
                    };
                $scope.GrpClas_itm__year_disabled =
                    function (classification) {
                        return self.GrpClas_itm__year_disabled(classification);
                    };
                $scope.GrpClas_itm__fiteId_disabled =
                    function (classification) {
                        return self.GrpClas_itm__fiteId_disabled(classification);
                    };
                $scope.showLov_GrpClas_itm__fiteId =
                    function (classification) {
                        $timeout(function () {
                            self.showLov_GrpClas_itm__fiteId(classification);
                        }, 0);
                    };
                $scope.GrpClas_itm__description_disabled =
                    function (classification) {
                        return self.GrpClas_itm__description_disabled(classification);
                    };
                $scope.GrpClas_itm__attachedFile_disabled =
                    function (classification) {
                        return self.GrpClas_itm__attachedFile_disabled(classification);
                    };
                $scope.createBlobModel_GrpClas_itm__attachedFile =
                    function () {
                        var tmp = self.createBlobModel_GrpClas_itm__attachedFile();
                        return tmp;
                    };
                $scope.GrpClas_itm__filePath_disabled =
                    function (classification) {
                        return self.GrpClas_itm__filePath_disabled(classification);
                    };
                $scope.importButtonId_disabled =
                    function (classification) {
                        return self.importButtonId_disabled(classification);
                    };
                $scope.importAction =
                    function (classification) {
                        self.importAction(classification);
                    };
                $scope.GrpClas_0_disabled =
                    function () {
                        return self.GrpClas_0_disabled();
                    };
                $scope.GrpClas_0_invisible =
                    function () {
                        return self.GrpClas_0_invisible();
                    };
                $scope.child_group_GrpParcelClas_isInvisible =
                    function () {
                        return self.child_group_GrpParcelClas_isInvisible();
                    };
                $scope.pageModel.modelGrpClas = $scope.modelGrpClas;
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Classification[] , oldVisible:Entities.Classification[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                //bind entity child collection with child controller merged items
                Entities.Classification.parcelClasCollection = function (classification, func) {
                    _this.model.modelGrpParcelClas.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, classification);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.Classification.parcelClasCollection = null; //(classification, func) => { }
                });
            }
            ControllerGrpClasBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpClasBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpClas";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpClasBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Classification_ControllerGrpClas";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpClasBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Name'; }, false, function (ent) { return ent.name; }, function (ent) { return ent._name; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.DateItem(function (ent) { return 'Classification Date'; }, false, function (ent) { return ent.dateTime; }, function (ent) { return ent._dateTime; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.LovItem(function (ent) { return 'Classification Engine'; }, false, function (ent) { return ent.clfrId; }, function (ent) { return ent._clfrId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.StaticListItem(function (ent) { return 'Year of Declaration'; }, false, function (ent) { return ent.year; }, function (ent) { return ent._year; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.LovItem(function (ent) { return 'Data File Columns Template'; }, false, function (ent) { return ent.fiteId; }, function (ent) { return ent._fiteId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.TextItem(function (ent) { return 'Description'; }, false, function (ent) { return ent.description; }, function (ent) { return ent._description; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.BlobItem(function (ent) { return 'File Name'; }, false, function (ent) { return ent.filePath; }, function (ent) { return ent._attachedFile; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.TextItem(function (ent) { return ' '; }, false, function (ent) { return ent.filePath; }, function (ent) { return ent._filePath; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpClasBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpClasBase.prototype, "modelGrpClas", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpClasBase.prototype.getEntityName = function () {
                return "Classification";
            };
            ControllerGrpClasBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
            };
            ControllerGrpClasBase.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
                var self = this;
                if (self.$scope.pageModel.controller.shownAsDialog())
                    return;
                var breadCrumbStepModel = { selectedEntity: null };
                if (!isVoid(self.Current)) {
                    var selectedEntity = Entities.Classification.Create();
                    selectedEntity.updateInstance(self.Current);
                    breadCrumbStepModel.selectedEntity = selectedEntity;
                }
                else {
                    breadCrumbStepModel.selectedEntity = null;
                }
                self.$scope.setCurrentPageModel(breadCrumbStepModel);
            };
            ControllerGrpClasBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.Classification.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpClasBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpClas.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpClasBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpClasBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.Classification(
                /*clasId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*name:string*/ null, 
                /*description:string*/ null, 
                /*dateTime:Date*/ new Date, 
                /*rowVersion:number*/ null, 
                /*recordtype:number*/ 0, 
                /*filePath:string*/ null, 
                /*attachedFile:string*/ null, 
                /*year:number*/ null, 
                /*isImported:boolean*/ null, 
                /*clfrId:Entities.Classifier*/ null, 
                /*fiteId:Entities.FileTemplate*/ null);
                return ret;
            };
            ControllerGrpClasBase.prototype.createNewEntityAndAddToUI = function () {
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.$scope.modelGrpClas.visibleEntities[0] = newEnt;
                self.$scope.modelGrpClas.selectedEntities[0] = newEnt;
                return newEnt;
            };
            ControllerGrpClasBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpClasBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.Classification.Create();
                ret.updateInstance(src);
                ret.clasId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.visibleEntities[0] = ret;
                this.model.selectedEntities[0] = ret;
                this.$timeout(function () {
                    _this.modelGrpClas.modelGrpParcelClas.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpClasBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpClas.modelGrpParcelClas.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpClasBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpClas.modelGrpParcelClas.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpClas.modelGrpParcelClas.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpClasBase.prototype.GrpClas_itm__name_disabled = function (classification) {
                var self = this;
                if (classification === undefined || classification === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(classification, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpClasBase.prototype.isEditClassificationDisabled = function (classification) {
                console.warn("Unimplemented function isEditClassificationDisabled()");
                return false;
            };
            ControllerGrpClasBase.prototype.GrpClas_itm__clfrId_disabled = function (classification) {
                var self = this;
                if (classification === undefined || classification === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(classification, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpClasBase.prototype.showLov_GrpClas_itm__clfrId = function (classification) {
                var self = this;
                if (classification === undefined)
                    return;
                var uimodel = classification._clfrId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Classification Engine';
                dialogOptions.previousModel = self.cachedLovModel_GrpClas_itm__clfrId;
                dialogOptions.className = "ControllerLovClassifier";
                dialogOptions.onSelect = function (selectedEntity) {
                    var classifier1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(classifier1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(classifier1) && classifier1.isEqual(classification.clfrId))
                        return;
                    classification.clfrId = classifier1;
                    self.markEntityAsUpdated(classification, 'GrpClas_itm__clfrId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpClas_itm__clfrId = lovModel;
                    var wsPath = "Classifier/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_name)) {
                        paramData['fsch_ClassifierLov_name'] = lovModel.fsch_ClassifierLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_description)) {
                        paramData['fsch_ClassifierLov_description'] = lovModel.fsch_ClassifierLov_description;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Classifier/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_name)) {
                        paramData['fsch_ClassifierLov_name'] = lovModel.fsch_ClassifierLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_description)) {
                        paramData['fsch_ClassifierLov_description'] = lovModel.fsch_ClassifierLov_description;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_name)) {
                        paramData['fsch_ClassifierLov_name'] = lovModel.fsch_ClassifierLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_description)) {
                        paramData['fsch_ClassifierLov_description'] = lovModel.fsch_ClassifierLov_description;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['description'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Classifier.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpClasBase.prototype.GrpClas_itm__year_disabled = function (classification) {
                var self = this;
                if (classification === undefined || classification === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(classification, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpClasBase.prototype.GrpClas_itm__fiteId_disabled = function (classification) {
                var self = this;
                if (classification === undefined || classification === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(classification, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpClasBase.prototype.showLov_GrpClas_itm__fiteId = function (classification) {
                var self = this;
                if (classification === undefined)
                    return;
                var uimodel = classification._fiteId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Data Import Template';
                dialogOptions.previousModel = self.cachedLovModel_GrpClas_itm__fiteId;
                dialogOptions.className = "ControllerLovFileTemplate";
                dialogOptions.onSelect = function (selectedEntity) {
                    var fileTemplate1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(fileTemplate1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(fileTemplate1) && fileTemplate1.isEqual(classification.fiteId))
                        return;
                    classification.fiteId = fileTemplate1;
                    self.markEntityAsUpdated(classification, 'GrpClas_itm__fiteId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpClas_itm__fiteId = lovModel;
                    var wsPath = "FileTemplate/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_FileTemplateLov_name)) {
                        paramData['fsch_FileTemplateLov_name'] = lovModel.fsch_FileTemplateLov_name;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "FileTemplate/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_FileTemplateLov_name)) {
                        paramData['fsch_FileTemplateLov_name'] = lovModel.fsch_FileTemplateLov_name;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_FileTemplateLov_name)) {
                        paramData['fsch_FileTemplateLov_name'] = lovModel.fsch_FileTemplateLov_name;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/FileTemplate.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpClasBase.prototype.GrpClas_itm__description_disabled = function (classification) {
                var self = this;
                if (classification === undefined || classification === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(classification, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpClasBase.prototype.GrpClas_itm__attachedFile_disabled = function (classification) {
                var self = this;
                if (classification === undefined || classification === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(classification, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpClasBase.prototype.createBlobModel_GrpClas_itm__attachedFile = function () {
                var self = this;
                var ret = new NpTypes.NpBlob(function (e) { self.markEntityAsUpdated(e, "attachedFile"); }, // on value change
                function (e) { return self.getDownloadUrl_createBlobModel_GrpClas_itm__attachedFile(e); }, // download url
                function (e) { return self.GrpClas_itm__attachedFile_disabled(e); }, // is disabled
                function (e) { return false; }, // is invisible
                "/Niva/rest/Classification/setAttachedFile", // post url
                true, // add is enabled
                true, // del is enabled
                "", // valid extensions
                function (e) { return 976562; } //size in KB
                 //size in KB
                );
                return ret;
            };
            ControllerGrpClasBase.prototype.getDownloadUrl_createBlobModel_GrpClas_itm__attachedFile = function (classification) {
                var self = this;
                if (isVoid(classification))
                    return 'javascript:void(0)';
                if (classification.isNew() && isVoid(classification.attachedFile))
                    return 'javascript:void(0)';
                var url = "/Niva/rest/Classification/getAttachedFile?";
                if (!classification.isNew()) {
                    url += "&id=" + encodeURIComponent(classification.clasId);
                }
                if (!isVoid(classification.attachedFile)) {
                    url += "&temp_id=" + encodeURIComponent(classification.attachedFile);
                }
                return url;
            };
            ControllerGrpClasBase.prototype.GrpClas_itm__filePath_disabled = function (classification) {
                var self = this;
                if (classification === undefined || classification === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(classification, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpClasBase.prototype.importButtonId_disabled = function (classification) {
                var self = this;
                if (classification === undefined || classification === null)
                    return true;
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isEditClassificationDisabled(classification);
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            ControllerGrpClasBase.prototype.importAction = function (classification) {
                var self = this;
                console.log("Method: importAction() called");
                console.log(classification);
            };
            ControllerGrpClasBase.prototype.GrpClas_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpClasBase.prototype.GrpClas_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = this.isEditClassificationNotImported();
                return programmerVal;
            };
            ControllerGrpClasBase.prototype.isEditClassificationNotImported = function () {
                console.warn("Unimplemented function isEditClassificationNotImported()");
                return false;
            };
            ControllerGrpClasBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpClasBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpClasBase.prototype._newIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // no write privilege
                var parEntityIsLocked = false;
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpClasBase.prototype._deleteIsDisabled = function (classification) {
                var self = this;
                if (classification === null || classification === undefined || classification.getEntityName() !== "Classification")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(classification, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpClasBase.prototype.child_group_GrpParcelClas_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpParcelClas === undefined) || (self.model.modelGrpParcelClas.controller === undefined))
                    return false;
                return self.model.modelGrpParcelClas.controller._isInvisible();
            };
            return ControllerGrpClasBase;
        })(Controllers.AbstractGroupFormController);
        Classification.ControllerGrpClasBase = ControllerGrpClasBase;
        // GROUP GrpParcelClas
        var ModelGrpParcelClasBase = (function (_super) {
            __extends(ModelGrpParcelClasBase, _super);
            function ModelGrpParcelClasBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_parcIdentifier = new NpTypes.UIStringModel(undefined);
                this._fsch_parcCode = new NpTypes.UIStringModel(undefined);
                this._fsch_prodCode = new NpTypes.UINumberModel(undefined);
                this._fsch_cultIdDecl = new NpTypes.UIManyToOneModel(undefined);
                this._fsch_cotyIdDecl = new NpTypes.UIManyToOneModel(undefined);
            }
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].pclaId;
                },
                set: function (pclaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].pclaId = pclaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "probPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probPred;
                },
                set: function (probPred_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probPred = probPred_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_probPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probPred;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "probPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probPred2;
                },
                set: function (probPred2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probPred2 = probPred2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_probPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probPred2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "prodCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].prodCode;
                },
                set: function (prodCode_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].prodCode = prodCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_prodCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._prodCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "parcIdentifier", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcIdentifier;
                },
                set: function (parcIdentifier_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcIdentifier = parcIdentifier_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_parcIdentifier", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcIdentifier;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "parcCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcCode;
                },
                set: function (parcCode_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcCode = parcCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_parcCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "geom4326", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].geom4326;
                },
                set: function (geom4326_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].geom4326 = geom4326_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_geom4326", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._geom4326;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].clasId;
                },
                set: function (clasId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].clasId = clasId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._clasId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cultIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultIdDecl;
                },
                set: function (cultIdDecl_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultIdDecl = cultIdDecl_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cultIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultIdDecl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cultIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultIdPred;
                },
                set: function (cultIdPred_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultIdPred = cultIdPred_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cultIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultIdPred;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cotyIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyIdDecl;
                },
                set: function (cotyIdDecl_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyIdDecl = cotyIdDecl_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cotyIdDecl", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyIdDecl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cotyIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyIdPred;
                },
                set: function (cotyIdPred_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyIdPred = cotyIdPred_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cotyIdPred", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyIdPred;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "cultIdPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultIdPred2;
                },
                set: function (cultIdPred2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultIdPred2 = cultIdPred2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_cultIdPred2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultIdPred2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "fsch_parcIdentifier", {
                get: function () {
                    return this._fsch_parcIdentifier.value;
                },
                set: function (vl) {
                    this._fsch_parcIdentifier.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "fsch_parcCode", {
                get: function () {
                    return this._fsch_parcCode.value;
                },
                set: function (vl) {
                    this._fsch_parcCode.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "fsch_prodCode", {
                get: function () {
                    return this._fsch_prodCode.value;
                },
                set: function (vl) {
                    this._fsch_prodCode.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "fsch_cultIdDecl", {
                get: function () {
                    return this._fsch_cultIdDecl.value;
                },
                set: function (vl) {
                    this._fsch_cultIdDecl.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "fsch_cotyIdDecl", {
                get: function () {
                    return this._fsch_cotyIdDecl.value;
                },
                set: function (vl) {
                    this._fsch_cotyIdDecl.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelClasBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpParcelClasBase;
        })(Controllers.AbstractGroupTableModel);
        Classification.ModelGrpParcelClasBase = ModelGrpParcelClasBase;
        var ControllerGrpParcelClasBase = (function (_super) {
            __extends(ControllerGrpParcelClasBase, _super);
            function ControllerGrpParcelClasBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 15, maxLinesInHeader: 3 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpParcelClas = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpParcelClas_srchr__fsch_parcIdentifier_disabled =
                    function (parcelClas) {
                        return self.GrpParcelClas_srchr__fsch_parcIdentifier_disabled(parcelClas);
                    };
                $scope.GrpParcelClas_srchr__fsch_parcCode_disabled =
                    function (parcelClas) {
                        return self.GrpParcelClas_srchr__fsch_parcCode_disabled(parcelClas);
                    };
                $scope.GrpParcelClas_srchr__fsch_prodCode_disabled =
                    function (parcelClas) {
                        return self.GrpParcelClas_srchr__fsch_prodCode_disabled(parcelClas);
                    };
                $scope.GrpParcelClas_srchr__fsch_cultIdDecl_disabled =
                    function (parcelClas) {
                        return self.GrpParcelClas_srchr__fsch_cultIdDecl_disabled(parcelClas);
                    };
                $scope.showLov_GrpParcelClas_srchr__fsch_cultIdDecl =
                    function () {
                        $timeout(function () {
                            self.showLov_GrpParcelClas_srchr__fsch_cultIdDecl();
                        }, 0);
                    };
                $scope.GrpParcelClas_srchr__fsch_cotyIdDecl_disabled =
                    function (parcelClas) {
                        return self.GrpParcelClas_srchr__fsch_cotyIdDecl_disabled(parcelClas);
                    };
                $scope.showLov_GrpParcelClas_srchr__fsch_cotyIdDecl =
                    function () {
                        $timeout(function () {
                            self.showLov_GrpParcelClas_srchr__fsch_cotyIdDecl();
                        }, 0);
                    };
                $scope.GrpParcelClas_itm__probPred_disabled =
                    function (parcelClas) {
                        return self.GrpParcelClas_itm__probPred_disabled(parcelClas);
                    };
                $scope.GrpParcelClas_itm__probPred2_disabled =
                    function (parcelClas) {
                        return self.GrpParcelClas_itm__probPred2_disabled(parcelClas);
                    };
                $scope.GrpParcelClas_0_disabled =
                    function () {
                        return self.GrpParcelClas_0_disabled();
                    };
                $scope.GrpParcelClas_0_invisible =
                    function () {
                        return self.GrpParcelClas_0_invisible();
                    };
                $scope.XartoReg_disabled =
                    function () {
                        return self.XartoReg_disabled();
                    };
                $scope.XartoReg_invisible =
                    function () {
                        return self.XartoReg_invisible();
                    };
                $scope.pageModel.modelGrpParcelClas = $scope.modelGrpParcelClas;
                $scope.clearBtnAction = function () {
                    self.modelGrpParcelClas.fsch_parcIdentifier = undefined;
                    self.modelGrpParcelClas.fsch_parcCode = undefined;
                    self.modelGrpParcelClas.fsch_prodCode = undefined;
                    self.modelGrpParcelClas.fsch_cultIdDecl = undefined;
                    self.modelGrpParcelClas.fsch_cotyIdDecl = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpClas.modelGrpParcelClas = $scope.modelGrpParcelClas;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpClas.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelClas[] , oldVisible:Entities.ParcelClas[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
                this.createGeom4326Map({
                    grp: this,
                    Plato: Plato,
                    divId: "INPMP",
                    getDataFromEntity: function (x) { return isVoid(x) ? undefined : x.geom4326; },
                    saveDataToEntity: function (x, f) {
                        if (!isVoid(x)) {
                            x.geom4326 = f;
                            _this.markEntityAsUpdated(x, "geom4326");
                        }
                    },
                    isDisabled: function (x) { return true; },
                    layers: [
                        new NpGeoLayers.GoogleTileLayer(true),
                        new NpGeoLayers.DigitalGlobeTileLayer(false),
                        new NpGeoLayers.BingTileLayer(false),
                        new NpGeoLayers.GeoserverTileLayer('ΟΠΕΚΕΠΕ', 'http://tiles.dik.loc/geoserver/gwc/service/wms', 'orthos_17', 'image/jpeg', false, null, null),
                        new NpGeoLayers.OpenStreetTileLayer(false),
                        new NpGeoLayers.QuestTileLayer(false),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMP1',
                            label: 'Search1',
                            isVisible: true,
                            isSearch: true
                        }),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMP2',
                            label: 'Search2',
                            isVisible: true,
                            isSearch: true
                        }),
                        new NpGeoLayers.SqlVectorLayer({
                            layerId: 'INPMP3',
                            label: 'Search3',
                            isVisible: true,
                            isSearch: true
                        })
                    ],
                    coordinateSystem: "EPSG:2100"
                });
            }
            ControllerGrpParcelClasBase.prototype.createGeom4326Map = function (defaultOptions) {
                this.geom4326Map = new NpGeo.NpMap(defaultOptions);
            };
            ControllerGrpParcelClasBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpParcelClas";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Classification_ControllerGrpParcelClas";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Identifier'; }, true, function (ent) { return self.model.fsch_parcIdentifier; }, function (ent) { return self.model._fsch_parcIdentifier; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'Parcel\'s Code'; }, true, function (ent) { return self.model.fsch_parcCode; }, function (ent) { return self.model._fsch_parcCode; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.NumberItem(function (ent) { return 'Farmer\'s Code'; }, true, function (ent) { return self.model.fsch_prodCode; }, function (ent) { return self.model._fsch_prodCode; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0),
                            new Controllers.LovItem(function (ent) { return 'cultIdDecl'; }, true, function (ent) { return self.model.fsch_cultIdDecl; }, function (ent) { return self.model._fsch_cultIdDecl; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.LovItem(function (ent) { return 'cotyIdDecl'; }, true, function (ent) { return self.model.fsch_cotyIdDecl; }, function (ent) { return self.model._fsch_cotyIdDecl; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.NumberItem(function (ent) { return 'Cultivation \n 1st Prediction \n Probability'; }, false, function (ent) { return ent.probPred; }, function (ent) { return ent._probPred; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.NumberItem(function (ent) { return 'Cultivation \n 2nd Prediction \n Probability'; }, false, function (ent) { return ent.probPred2; }, function (ent) { return ent._probPred2; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 2),
                            new Controllers.MapItem(function (ent) { return ''; }, false, function (ent) { return ent.geom4326; }, function (ent) { return ent._geom4326; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.gridTitle = function () {
                return "Parcels";
            };
            ControllerGrpParcelClasBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpParcelClasBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'parcIdentifier', displayName: 'getALString("Identifier", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__parcIdentifier' data-ng-model='row.entity.parcIdentifier' data-np-ui-model='row.entity._parcIdentifier' data-ng-change='markEntityAsUpdated(row.entity,&quot;parcIdentifier&quot;)' data-np-required='true' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'parcCode', displayName: 'getALString("Parcel\'s \n Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__parcCode' data-ng-model='row.entity.parcCode' data-np-ui-model='row.entity._parcCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;parcCode&quot;)' data-np-required='true' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'prodCode', displayName: 'getALString("Farmer\'s \n Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__prodCode' data-ng-model='row.entity.prodCode' data-np-ui-model='row.entity._prodCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;prodCode&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultIdDecl.code', displayName: 'getALString("Cultivation \n Declared \n Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '6%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdDecl_code' data-ng-model='row.entity.cultIdDecl.code' data-np-ui-model='row.entity.cultIdDecl._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdDecl.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultIdDecl.name', displayName: 'getALString("Cultivation \n Declared \n Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '10%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdDecl_name' data-ng-model='row.entity.cultIdDecl.name' data-np-ui-model='row.entity.cultIdDecl._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdDecl.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultIdPred.code', displayName: 'getALString("Cultivation \n 1st Prediction \n Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '6%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdPred_code' data-ng-model='row.entity.cultIdPred.code' data-np-ui-model='row.entity.cultIdPred._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdPred.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultIdPred.name', displayName: 'getALString("Cultivation \n 1st Prediction \n Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '10%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdPred_name' data-ng-model='row.entity.cultIdPred.name' data-np-ui-model='row.entity.cultIdPred._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdPred.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probPred', displayName: 'getALString("Cultivation \n 1st Prediction \n Probability", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__probPred' data-ng-model='row.entity.probPred' data-np-ui-model='row.entity._probPred' data-ng-change='markEntityAsUpdated(row.entity,&quot;probPred&quot;)' data-ng-readonly='GrpParcelClas_itm__probPred_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultIdPred2.code', displayName: 'getALString("Cultivation \n 2nd Prediction  \nCode", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '6%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdPred2_code' data-ng-model='row.entity.cultIdPred2.code' data-np-ui-model='row.entity.cultIdPred2._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdPred2.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultIdPred2.name', displayName: 'getALString("Cultivation \n 2nd Prediction \n Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '10%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__cultIdPred2_name' data-ng-model='row.entity.cultIdPred2.name' data-np-ui-model='row.entity.cultIdPred2._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultIdPred2.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probPred2', displayName: 'getALString("Cultivation \n 2nd Prediction \n Probability", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelClas_itm__probPred2' data-ng-model='row.entity.probPred2' data-np-ui-model='row.entity._probPred2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probPred2&quot;)' data-ng-readonly='GrpParcelClas_itm__probPred2_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "modelGrpParcelClas", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.getEntityName = function () {
                return "ParcelClas";
            };
            ControllerGrpParcelClasBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
                this.geom4326Map.cleanUndoBuffers();
            };
            ControllerGrpParcelClasBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.ParcelClas.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.clasId = _this.Parent;
                    }
                    else {
                        x.clasId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelClas.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpClas.controller.isEntityLocked(cur.clasId, lockKind);
            };
            ControllerGrpParcelClasBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.clasId)) {
                    paramData['clasId_clasId'] = self.Parent.clasId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcIdentifier)) {
                    paramData['fsch_parcIdentifier'] = self.modelGrpParcelClas.fsch_parcIdentifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcCode)) {
                    paramData['fsch_parcCode'] = self.modelGrpParcelClas.fsch_parcCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelClas.fsch_prodCode;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpParcelClasBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelClas/findAllByCriteriaRange_ClassificationGrpParcelClas_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.clasId)) {
                    paramData['clasId_clasId'] = self.Parent.clasId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcIdentifier)) {
                    paramData['fsch_parcIdentifier'] = self.modelGrpParcelClas.fsch_parcIdentifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcCode)) {
                    paramData['fsch_parcCode'] = self.modelGrpParcelClas.fsch_parcCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelClas.fsch_prodCode;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelClasBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelClas/findAllByCriteriaRange_ClassificationGrpParcelClas";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.clasId)) {
                    paramData['clasId_clasId'] = self.Parent.clasId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcIdentifier)) {
                    paramData['fsch_parcIdentifier'] = self.modelGrpParcelClas.fsch_parcIdentifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcCode)) {
                    paramData['fsch_parcCode'] = self.modelGrpParcelClas.fsch_parcCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelClas.fsch_prodCode;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelClasBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelClas/findAllByCriteriaRange_ClassificationGrpParcelClas_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.clasId)) {
                    paramData['clasId_clasId'] = self.Parent.clasId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcIdentifier)) {
                    paramData['fsch_parcIdentifier'] = self.modelGrpParcelClas.fsch_parcIdentifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_parcCode)) {
                    paramData['fsch_parcCode'] = self.modelGrpParcelClas.fsch_parcCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelClas) && !isVoid(self.modelGrpParcelClas.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelClas.fsch_prodCode;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelClasBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.clasId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "ParcelClas/findAllByCriteriaRange_ClassificationGrpParcelClas";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['clasId_clasId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpParcelClasBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.clasId === undefined)
                    return false;
                return x.clasId.getKey() === this.Parent.getKey();
            };
            ControllerGrpParcelClasBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.clasId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpClas.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "clasId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpClas.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.ParcelClas(
                /*pclaId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*probPred:number*/ null, 
                /*probPred2:number*/ null, 
                /*prodCode:number*/ null, 
                /*parcIdentifier:string*/ null, 
                /*parcCode:string*/ null, 
                /*geom4326:ol.Feature*/ null, 
                /*clasId:Entities.Classification*/ null, 
                /*cultIdDecl:Entities.Cultivation*/ null, 
                /*cultIdPred:Entities.Cultivation*/ null, 
                /*cotyIdDecl:Entities.CoverType*/ null, 
                /*cotyIdPred:Entities.CoverType*/ null, 
                /*cultIdPred2:Entities.Cultivation*/ null);
                return ret;
            };
            ControllerGrpParcelClasBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.clasId = self.clasId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpParcelClasBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpParcelClasBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.ParcelClas.Create();
                ret.updateInstance(src);
                ret.pclaId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpParcelClasBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (parcelClasList) {
                    parcelClasList.forEach(function (parcelClas) {
                        var parcelClasCloned = _this.cloneEntity_internal(parcelClas, true);
                        parcelClasCloned.clasId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpParcelClasBase.prototype.deleteNewEntitiesUnderParent = function (classification) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var parcelClas = change.a;
                    if (classification.getKey() === parcelClas.clasId.getKey()) {
                        toBeDeleted.push(parcelClas);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpParcelClasBase.prototype.deleteAllEntitiesUnderParent = function (classification, afterDeleteAction) {
                var self = this;
                function deleteParcelClasList(parcelClasList) {
                    if (parcelClasList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = parcelClasList[0];
                        var tail = parcelClasList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteParcelClasList(tail);
                        });
                    }
                }
                this.getMergedItems(function (parcelClasList) {
                    deleteParcelClasList(parcelClasList);
                }, false, classification);
            };
            Object.defineProperty(ControllerGrpParcelClasBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelClasBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpParcelClasBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_srchr__fsch_parcIdentifier_disabled = function (parcelClas) {
                var self = this;
                return false;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_srchr__fsch_parcCode_disabled = function (parcelClas) {
                var self = this;
                return false;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_srchr__fsch_prodCode_disabled = function (parcelClas) {
                var self = this;
                return false;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_srchr__fsch_cultIdDecl_disabled = function (parcelClas) {
                var self = this;
                return false;
            };
            ControllerGrpParcelClasBase.prototype.showLov_GrpParcelClas_srchr__fsch_cultIdDecl = function () {
                var self = this;
                var uimodel = self.modelGrpParcelClas._fsch_cultIdDecl;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Cultivation';
                dialogOptions.previousModel = self.cachedLovModel_GrpParcelClas_srchr__fsch_cultIdDecl;
                dialogOptions.className = "ControllerLovCultivation";
                dialogOptions.onSelect = function (selectedEntity) {
                    var cultivation1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(cultivation1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(cultivation1) && cultivation1.isEqual(self.modelGrpParcelClas.fsch_cultIdDecl))
                        return;
                    self.modelGrpParcelClas.fsch_cultIdDecl = cultivation1;
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpParcelClas_srchr__fsch_cultIdDecl = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_srchr__fsch_cotyIdDecl_disabled = function (parcelClas) {
                var self = this;
                return false;
            };
            ControllerGrpParcelClasBase.prototype.showLov_GrpParcelClas_srchr__fsch_cotyIdDecl = function () {
                var self = this;
                var uimodel = self.modelGrpParcelClas._fsch_cotyIdDecl;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Land Cover';
                dialogOptions.previousModel = self.cachedLovModel_GrpParcelClas_srchr__fsch_cotyIdDecl;
                dialogOptions.className = "ControllerLovCoverType";
                dialogOptions.onSelect = function (selectedEntity) {
                    var coverType1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(coverType1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(coverType1) && coverType1.isEqual(self.modelGrpParcelClas.fsch_cotyIdDecl))
                        return;
                    self.modelGrpParcelClas.fsch_cotyIdDecl = coverType1;
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpParcelClas_srchr__fsch_cotyIdDecl = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_itm__probPred_disabled = function (parcelClas) {
                var self = this;
                if (parcelClas === undefined || parcelClas === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelClas, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_itm__probPred2_disabled = function (parcelClas) {
                var self = this;
                if (parcelClas === undefined || parcelClas === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelClas, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.GrpParcelClas_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.XartoReg_disabled = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype.XartoReg_invisible = function () {
                if (false)
                    return true;
                var parControl = this.GrpParcelClas_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype._isDisabled = function () {
                if (true)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpClas_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelClasBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpParcelClasBase.prototype._deleteIsDisabled = function (parcelClas) {
                return true;
            };
            return ControllerGrpParcelClasBase;
        })(Controllers.AbstractGroupTableController);
        Classification.ControllerGrpParcelClasBase = ControllerGrpParcelClasBase;
    })(Classification = Controllers.Classification || (Controllers.Classification = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ClassificationBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
