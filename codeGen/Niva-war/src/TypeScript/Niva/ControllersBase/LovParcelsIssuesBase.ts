/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />
/// <reference path="../Controllers/LovParcelsIssues.ts" />

module Controllers {
    export class ModelLovParcelsIssuesBase extends AbstractLovModel {
        _fsch_ParcelsIssuesLov_dteCreated:NpTypes.UIDateModel = new NpTypes.UIDateModel(undefined);
        public get fsch_ParcelsIssuesLov_dteCreated():Date {
            return this._fsch_ParcelsIssuesLov_dteCreated.value;
        }
        public set fsch_ParcelsIssuesLov_dteCreated(vl:Date) {
            this._fsch_ParcelsIssuesLov_dteCreated.value = vl;
        }
        _fsch_ParcelsIssuesLov_status:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_ParcelsIssuesLov_status():number {
            return this._fsch_ParcelsIssuesLov_status.value;
        }
        public set fsch_ParcelsIssuesLov_status(vl:number) {
            this._fsch_ParcelsIssuesLov_status.value = vl;
        }
        _fsch_ParcelsIssuesLov_dteStatusUpdate:NpTypes.UIDateModel = new NpTypes.UIDateModel(undefined);
        public get fsch_ParcelsIssuesLov_dteStatusUpdate():Date {
            return this._fsch_ParcelsIssuesLov_dteStatusUpdate.value;
        }
        public set fsch_ParcelsIssuesLov_dteStatusUpdate(vl:Date) {
            this._fsch_ParcelsIssuesLov_dteStatusUpdate.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        controller: ControllerLovParcelsIssues;
        constructor(public $scope: IScopeLovParcelsIssues) { super($scope); }
    }


    export interface IScopeLovParcelsIssuesBase extends IAbstractLovScope {
        globals: Globals;
        modelLovParcelsIssues : ModelLovParcelsIssues;
        _disabled():boolean; 
        _invisible():boolean; 
        LovParcelsIssues_fsch_ParcelsIssuesLov_dteCreated_disabled():boolean; 
        LovParcelsIssues_fsch_ParcelsIssuesLov_status_disabled():boolean; 
        LovParcelsIssues_fsch_ParcelsIssuesLov_dteStatusUpdate_disabled():boolean; 
    }

    export class ControllerLovParcelsIssuesBase extends LovController {
        constructor(
            public $scope: IScopeLovParcelsIssues,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:ModelLovParcelsIssues)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 5, maxLinesInHeader: 1})
            var self: ControllerLovParcelsIssuesBase = this;
            model.controller = <ControllerLovParcelsIssues>self;
            $scope.clearBtnAction = () => { 
                self.model.fsch_ParcelsIssuesLov_dteCreated = undefined;
                self.model.fsch_ParcelsIssuesLov_status = undefined;
                self.model.fsch_ParcelsIssuesLov_dteStatusUpdate = undefined;
                self.updateGrid();
            };
            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.LovParcelsIssues_fsch_ParcelsIssuesLov_dteCreated_disabled = 
                () => {
                    return self.LovParcelsIssues_fsch_ParcelsIssuesLov_dteCreated_disabled();
                };
            $scope.LovParcelsIssues_fsch_ParcelsIssuesLov_status_disabled = 
                () => {
                    return self.LovParcelsIssues_fsch_ParcelsIssuesLov_status_disabled();
                };
            $scope.LovParcelsIssues_fsch_ParcelsIssuesLov_dteStatusUpdate_disabled = 
                () => {
                    return self.LovParcelsIssues_fsch_ParcelsIssuesLov_dteStatusUpdate_disabled();
                };
            $scope.modelLovParcelsIssues = model;
            self.updateUI();
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerLovParcelsIssues";
        }
        public get HtmlDivId(): string {
            return "ParcelsIssues_Id";
        }
        
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions =   <NpTypes.LovDialogOptions>self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName); 
            }
            var ret = [
                { cellClass:'cellToolTip', field:'dteCreated', displayName:'getALString("dteCreated", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'40%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <input data-ng-model='row.entity.dteCreated' data-np-date='dummy' data-ng-readonly='true' data-np-format='dd-MM-yyyy' data-np-ui-model='row.entity._dteCreated' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'status', displayName:'getALString("Κατάσταση", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'14%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.status' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteStatusUpdate', displayName:'getALString("Ημερομηνία", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'40%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <input data-ng-model='row.entity.dteStatusUpdate' data-np-date='dummy' data-ng-readonly='true' data-np-format='dd-MM-yyyy' data-np-ui-model='row.entity._dteStatusUpdate' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter((cl => cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true));
            if (self.isMultiSelect()) {
                ret.splice(0,0,{ width:'22', cellTemplate:"<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined});
            }
            return ret;
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            return Entities.ParcelsIssues.fromJSONComplete(webResponse.data);
        }

        public get modelLovParcelsIssues():ModelLovParcelsIssues {
            var self = this;
            return self.model;
        }

        public initializeModelWithPreviousValues() {
            var self = this;
            var prevModel:ModelLovParcelsIssues = <ModelLovParcelsIssues>self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovParcelsIssues.fsch_ParcelsIssuesLov_dteCreated = prevModel.fsch_ParcelsIssuesLov_dteCreated
                self.modelLovParcelsIssues.fsch_ParcelsIssuesLov_status = prevModel.fsch_ParcelsIssuesLov_status
                self.modelLovParcelsIssues.fsch_ParcelsIssuesLov_dteStatusUpdate = prevModel.fsch_ParcelsIssuesLov_dteStatusUpdate
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public LovParcelsIssues_fsch_ParcelsIssuesLov_dteCreated_disabled():boolean {
            var self = this;


            return false;
        }
        public LovParcelsIssues_fsch_ParcelsIssuesLov_status_disabled():boolean {
            var self = this;


            return false;
        }
        public LovParcelsIssues_fsch_ParcelsIssuesLov_dteStatusUpdate_disabled():boolean {
            var self = this;


            return false;
        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
