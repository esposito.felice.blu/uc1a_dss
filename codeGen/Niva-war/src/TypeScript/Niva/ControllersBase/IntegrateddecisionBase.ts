/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/IntegrateddecisionBase.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="LovParcelClasBase.ts" />
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
/// <reference path="LovDecisionMakingBase.ts" />
/// <reference path="../Controllers/Integrateddecision.ts" />
module Controllers.Integrateddecision {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpIntegrateddecision:ModelGrpIntegrateddecision;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
        onNewBtnAction(): void;
        onDeleteBtnAction():void;

    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            

            $scope.onNewBtnAction = () => { 
                self.onNewBtnAction();
            };
            
            $scope.onDeleteBtnAction = () => { 
                self.onDeleteBtnAction();
            };


            $timeout(function() {
                $('#Integrateddecision').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "Integrateddecision";
        }
    
        public _getPageTitle(): string {
            return "Integrateddecision";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpIntegrateddecision.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpIntegrateddecision.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpIntegrateddecision.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpIntegrateddecision.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpIntegrateddecision.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
            if (bForDelete) {
                pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getChangesToCommitForDeletion());
                pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getDeleteChangesToCommit());
            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getChangesToCommit());
            }
            
            var hasIntegrateddecision = pageChanges.some(change => change.entityName === "Integrateddecision")
            if (!hasIntegrateddecision) {
                var validateEntity = new ChangeToCommit(ChangeStatus.UPDATE, 0, "Integrateddecision", self.model.modelGrpIntegrateddecision.controller.Current);
                pageChanges = pageChanges.concat(validateEntity);
            }
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_Integrateddecision"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpIntegrateddecision.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;

                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.Integrateddecision>>self.$scope.getCurrentPageModel();
                        var newIntegrateddecisionId = response.newEntitiesIds.firstOrNull(x => x.entityName === 'Integrateddecision');
                        if (!isVoid(newIntegrateddecisionId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.Integrateddecision.CreateById(newIntegrateddecisionId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        } else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }

                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }

        public getInitialEntity(): Entities.Integrateddecision {
            if (this.shownAsDialog()) {
                return <Entities.Integrateddecision>this.dialogSelectedEntity();
            } else {
                var breadCrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.Integrateddecision>>this.$scope.getPageModelByURL('/Integrateddecision');
                if (isVoid(breadCrumbStepModel)) {
                    return null;
                } else {
                    return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                }
            }
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


        public onNewBtnAction(): void {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => { 
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                self.addNewRecord();
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {self.addNewRecord();}),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                self.addNewRecord();
            }
        }

        public addNewRecord(): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            self.model.modelGrpIntegrateddecision.controller.cleanUpAfterSave();
            self.model.modelGrpIntegrateddecision.controller.createNewEntityAndAddToUI();
        }

        public onDeleteBtnAction():void {
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, 
                [
                    new Tuple2("MessageBox_Button_Yes", () => {self.deleteRecord();}),
                    new Tuple2("MessageBox_Button_No", () => {})
                ], 1,1);
        }

        public deleteRecord(): void {
            var self = this;
            if (self.Mode === EditMode.NEW) {
                console.log("Delete pressed in a form while being in new mode!");
                return;
            }
            NpTypes.AlertMessage.clearAlerts(self.model);
            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData(true);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
            });
        }
        
        public get Mode(): EditMode {
            var self = this;
            if (isVoid(self.model.modelGrpIntegrateddecision) || isVoid(self.model.modelGrpIntegrateddecision.controller))
                return EditMode.NEW;
            return self.model.modelGrpIntegrateddecision.controller.Mode;
        }

        public _newIsDisabled(): boolean {
            var self = this;
            if (isVoid(self.model.modelGrpIntegrateddecision) || isVoid(self.model.modelGrpIntegrateddecision.controller))
                return true;
            return self.model.modelGrpIntegrateddecision.controller._newIsDisabled();
        }
        public _deleteIsDisabled(): boolean {
            var self = this;
            if (self.Mode === EditMode.NEW)
                return true;
            if (isVoid(self.model.modelGrpIntegrateddecision) || isVoid(self.model.modelGrpIntegrateddecision.controller))
                return true;
            return self.model.modelGrpIntegrateddecision.controller._deleteIsDisabled(self.model.modelGrpIntegrateddecision.controller.Current);
        }


    }


    

    // GROUP GrpIntegrateddecision

    export class ModelGrpIntegrateddecisionBase extends Controllers.AbstractGroupFormModel {
        controller: ControllerGrpIntegrateddecision;
        public get integrateddecisionsId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).integrateddecisionsId;
        }

        public set integrateddecisionsId(integrateddecisionsId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).integrateddecisionsId = integrateddecisionsId_newVal;
        }

        public get _integrateddecisionsId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._integrateddecisionsId;
        }

        public get decisionCode():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).decisionCode;
        }

        public set decisionCode(decisionCode_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).decisionCode = decisionCode_newVal;
        }

        public get _decisionCode():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionCode;
        }

        public get dteUpdate():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).dteUpdate;
        }

        public set dteUpdate(dteUpdate_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).dteUpdate = dteUpdate_newVal;
        }

        public get _dteUpdate():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteUpdate;
        }

        public get usrUpdate():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).usrUpdate;
        }

        public set usrUpdate(usrUpdate_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).usrUpdate = usrUpdate_newVal;
        }

        public get _usrUpdate():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._usrUpdate;
        }

        public get pclaId():Entities.ParcelClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).pclaId;
        }

        public set pclaId(pclaId_newVal:Entities.ParcelClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).pclaId = pclaId_newVal;
        }

        public get _pclaId():NpTypes.UIManyToOneModel<Entities.ParcelClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._pclaId;
        }

        public get demaId():Entities.DecisionMaking {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).demaId;
        }

        public set demaId(demaId_newVal:Entities.DecisionMaking) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).demaId = demaId_newVal;
        }

        public get _demaId():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._demaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpIntegrateddecision) { super($scope); }
    }



    export interface IScopeGrpIntegrateddecisionBase extends Controllers.IAbstractFormGroupScope, IPageScope{
        globals: Globals;
        modelGrpIntegrateddecision : ModelGrpIntegrateddecision;
        GrpIntegrateddecision_itm__decisionCode_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        GrpIntegrateddecision_itm__dteUpdate_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        GrpIntegrateddecision_itm__usrUpdate_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        GrpIntegrateddecision_itm__pclaId_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        showLov_GrpIntegrateddecision_itm__pclaId(integrateddecision:Entities.Integrateddecision):void; 
        GrpIntegrateddecision_itm__demaId_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        showLov_GrpIntegrateddecision_itm__demaId(integrateddecision:Entities.Integrateddecision):void; 

    }



    export class ControllerGrpIntegrateddecisionBase extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpIntegrateddecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpIntegrateddecision)
        {
            super($scope, $http, $timeout, Plato, model);
            var self:ControllerGrpIntegrateddecisionBase = this;
            model.controller = <ControllerGrpIntegrateddecision>self;
            $scope.modelGrpIntegrateddecision = self.model;

            var selectedEntity: Entities.Integrateddecision = this.$scope.pageModel.controller.getInitialEntity();
            if (isVoid(selectedEntity)) {
                self.createNewEntityAndAddToUI();
            } else {
                var clonedEntity = Entities.Integrateddecision.Create();
                clonedEntity.updateInstance(selectedEntity);
                $scope.modelGrpIntegrateddecision.visibleEntities[0] = clonedEntity;
                $scope.modelGrpIntegrateddecision.selectedEntities[0] = clonedEntity;
            } 
            $scope.GrpIntegrateddecision_itm__decisionCode_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_itm__decisionCode_disabled(integrateddecision);
                };
            $scope.GrpIntegrateddecision_itm__dteUpdate_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_itm__dteUpdate_disabled(integrateddecision);
                };
            $scope.GrpIntegrateddecision_itm__usrUpdate_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_itm__usrUpdate_disabled(integrateddecision);
                };
            $scope.GrpIntegrateddecision_itm__pclaId_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_itm__pclaId_disabled(integrateddecision);
                };
            $scope.showLov_GrpIntegrateddecision_itm__pclaId = 
                (integrateddecision:Entities.Integrateddecision) => {
                    $timeout( () => {        
                        self.showLov_GrpIntegrateddecision_itm__pclaId(integrateddecision);
                    }, 0);
                };
            $scope.GrpIntegrateddecision_itm__demaId_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_itm__demaId_disabled(integrateddecision);
                };
            $scope.showLov_GrpIntegrateddecision_itm__demaId = 
                (integrateddecision:Entities.Integrateddecision) => {
                    $timeout( () => {        
                        self.showLov_GrpIntegrateddecision_itm__demaId(integrateddecision);
                    }, 0);
                };


            $scope.pageModel.modelGrpIntegrateddecision = $scope.modelGrpIntegrateddecision;


    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Integrateddecision[] , oldVisible:Entities.Integrateddecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpIntegrateddecision";
        }
        public get HtmlDivId(): string {
            return "Integrateddecision_ControllerGrpIntegrateddecision";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new NumberItem (
                        (ent?: Entities.Integrateddecision) => 'Κωδικός',
                        false ,
                        (ent?: Entities.Integrateddecision) => ent.decisionCode,  
                        (ent?: Entities.Integrateddecision) => ent._decisionCode,  
                        (ent?: Entities.Integrateddecision) => true, //isRequired
                        (vl: number, ent?: Entities.Integrateddecision) => new NpTypes.ValidationResult(true, ""), 
                        () => -32768,
                        () => 32767,
                        0),
                    new DateItem (
                        (ent?: Entities.Integrateddecision) => 'Ημερομηνία',
                        false ,
                        (ent?: Entities.Integrateddecision) => ent.dteUpdate,  
                        (ent?: Entities.Integrateddecision) => ent._dteUpdate,  
                        (ent?: Entities.Integrateddecision) => true, //isRequired
                        (vl: Date, ent?: Entities.Integrateddecision) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined),
                    new TextItem (
                        (ent?: Entities.Integrateddecision) => 'Ημερομηνία',
                        false ,
                        (ent?: Entities.Integrateddecision) => ent.usrUpdate,  
                        (ent?: Entities.Integrateddecision) => ent._usrUpdate,  
                        (ent?: Entities.Integrateddecision) => false, 
                        (vl: string, ent?: Entities.Integrateddecision) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new LovItem (
                        (ent?: Entities.Integrateddecision) => 'pclaId',
                        false ,
                        (ent?: Entities.Integrateddecision) => ent.pclaId,  
                        (ent?: Entities.Integrateddecision) => ent._pclaId,  
                        (ent?: Entities.Integrateddecision) => true, //isRequired
                        (vl: Entities.ParcelClas, ent?: Entities.Integrateddecision) => new NpTypes.ValidationResult(true, "")),
                    new LovItem (
                        (ent?: Entities.Integrateddecision) => 'demaId',
                        false ,
                        (ent?: Entities.Integrateddecision) => ent.demaId,  
                        (ent?: Entities.Integrateddecision) => ent._demaId,  
                        (ent?: Entities.Integrateddecision) => true, //isRequired
                        (vl: Entities.DecisionMaking, ent?: Entities.Integrateddecision) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpIntegrateddecision():ModelGrpIntegrateddecision {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "Integrateddecision";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
        }

        public setCurrentFormPageBreadcrumpStepModel() {
            var self = this;
            if (self.$scope.pageModel.controller.shownAsDialog())
                return;

            var breadCrumbStepModel: IFormPageBreadcrumbStepModel<Entities.Integrateddecision> = { selectedEntity: null };
            if (!isVoid(self.Current)) {
                var selectedEntity: Entities.Integrateddecision = Entities.Integrateddecision.Create();
                selectedEntity.updateInstance(self.Current);
                breadCrumbStepModel.selectedEntity = selectedEntity;
            } else {
                breadCrumbStepModel.selectedEntity = null;
            }
            self.$scope.setCurrentPageModel(breadCrumbStepModel);
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.Integrateddecision.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.Integrateddecision {
            var self = this;
            return <Entities.Integrateddecision>self.$scope.modelGrpIntegrateddecision.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.Integrateddecision, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }







        public constructEntity(): Entities.Integrateddecision {
            var self = this;
            var ret = new Entities.Integrateddecision(
                /*integrateddecisionsId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*decisionCode:number*/ null,
                /*dteUpdate:Date*/ null,
                /*usrUpdate:string*/ null,
                /*pclaId:Entities.ParcelClas*/ null,
                /*demaId:Entities.DecisionMaking*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.Integrateddecision = <Entities.Integrateddecision>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.$scope.modelGrpIntegrateddecision.visibleEntities[0] = newEnt;
            self.$scope.modelGrpIntegrateddecision.selectedEntities[0] = newEnt;
            return newEnt;

        }

        public cloneEntity(src: Entities.Integrateddecision): Entities.Integrateddecision {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.Integrateddecision, calledByParent: boolean= false): Entities.Integrateddecision {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.Integrateddecision.Create();
            ret.updateInstance(src);
            ret.integrateddecisionsId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.visibleEntities[0] = ret;
            this.model.selectedEntities[0] = ret;

            this.$timeout( () => {
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.Integrateddecision, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public GrpIntegrateddecision_itm__decisionCode_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(integrateddecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpIntegrateddecision_itm__dteUpdate_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(integrateddecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpIntegrateddecision_itm__usrUpdate_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(integrateddecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpIntegrateddecision_itm__pclaId_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(integrateddecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpIntegrateddecision_itm__pclaId:ModelLovParcelClasBase;
        public showLov_GrpIntegrateddecision_itm__pclaId(integrateddecision:Entities.Integrateddecision) {
            var self = this;
            if (integrateddecision === undefined)
                return;
            var uimodel:NpTypes.IUIModel = integrateddecision._pclaId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'ParcelClas';
            dialogOptions.previousModel= self.cachedLovModel_GrpIntegrateddecision_itm__pclaId;
            dialogOptions.className = "ControllerLovParcelClas";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var parcelClas1:Entities.ParcelClas =   <Entities.ParcelClas>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(parcelClas1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(parcelClas1) && parcelClas1.isEqual(integrateddecision.pclaId))
                    return;
                integrateddecision.pclaId = parcelClas1;
                self.markEntityAsUpdated(integrateddecision, 'GrpIntegrateddecision_itm__pclaId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovParcelClasBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpIntegrateddecision_itm__pclaId = lovModel;
                    var wsPath = "ParcelClas/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovParcelClasBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "ParcelClas/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['probPred'] = true;
            dialogOptions.shownCols['probPred2'] = true;
            dialogOptions.shownCols['prodCode'] = true;
            dialogOptions.shownCols['parcIdentifier'] = true;
            dialogOptions.shownCols['parcCode'] = true;
            dialogOptions.shownCols['geom4326'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/ParcelClas.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpIntegrateddecision_itm__demaId_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(integrateddecision, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpIntegrateddecision_itm__demaId:ModelLovDecisionMakingBase;
        public showLov_GrpIntegrateddecision_itm__demaId(integrateddecision:Entities.Integrateddecision) {
            var self = this;
            if (integrateddecision === undefined)
                return;
            var uimodel:NpTypes.IUIModel = integrateddecision._demaId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'DecisionMaking';
            dialogOptions.previousModel= self.cachedLovModel_GrpIntegrateddecision_itm__demaId;
            dialogOptions.className = "ControllerLovDecisionMaking";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var decisionMaking1:Entities.DecisionMaking =   <Entities.DecisionMaking>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(decisionMaking1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(decisionMaking1) && decisionMaking1.isEqual(integrateddecision.demaId))
                    return;
                integrateddecision.demaId = decisionMaking1;
                self.markEntityAsUpdated(integrateddecision, 'GrpIntegrateddecision_itm__demaId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovDecisionMakingBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpIntegrateddecision_itm__demaId = lovModel;
                    var wsPath = "DecisionMaking/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_description)) {
                        paramData['fsch_DecisionMakingLov_description'] = lovModel.fsch_DecisionMakingLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_dateTime)) {
                        paramData['fsch_DecisionMakingLov_dateTime'] = lovModel.fsch_DecisionMakingLov_dateTime;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovDecisionMakingBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "DecisionMaking/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_description)) {
                        paramData['fsch_DecisionMakingLov_description'] = lovModel.fsch_DecisionMakingLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_dateTime)) {
                        paramData['fsch_DecisionMakingLov_dateTime'] = lovModel.fsch_DecisionMakingLov_dateTime;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_description)) {
                        paramData['fsch_DecisionMakingLov_description'] = lovModel.fsch_DecisionMakingLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_dateTime)) {
                        paramData['fsch_DecisionMakingLov_dateTime'] = lovModel.fsch_DecisionMakingLov_dateTime;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['description'] = true;
            dialogOptions.shownCols['dateTime'] = true;
            dialogOptions.shownCols['recordtype'] = true;
            dialogOptions.shownCols['hasBeenRun'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/DecisionMaking.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = false;
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(integrateddecision:Entities.Integrateddecision):boolean  {
            var self = this;
            if (integrateddecision === null || integrateddecision === undefined || integrateddecision.getEntityName() !== "Integrateddecision")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(integrateddecision, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
