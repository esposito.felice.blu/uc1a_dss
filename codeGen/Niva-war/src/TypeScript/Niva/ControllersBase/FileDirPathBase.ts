/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/FileDirPathBase.ts" />
/// <reference path="../Controllers/FileDirPath.ts" />
module Controllers.FileDirPath {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpFileDirPath:ModelGrpFileDirPath;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            


            $timeout(function() {
                $('#FileDirPath').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "FileDirPath";
        }
    
        public _getPageTitle(): string {
            return "Files and Directories Preferences";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_FileDirPath_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpFileDirPath.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpFileDirPath.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpFileDirPath.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpFileDirPath.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpFileDirPath.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
                pageChanges = pageChanges.concat(self.model.modelGrpFileDirPath.controller.getChangesToCommit());
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_FileDirPath"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpFileDirPath.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


    }


    

    // GROUP GrpFileDirPath

    export class ModelGrpFileDirPathBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpFileDirPath;
        public get fidrId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FileDirPath>this.selectedEntities[0]).fidrId;
        }

        public set fidrId(fidrId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FileDirPath>this.selectedEntities[0]).fidrId = fidrId_newVal;
        }

        public get _fidrId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._fidrId;
        }

        public get directory():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FileDirPath>this.selectedEntities[0]).directory;
        }

        public set directory(directory_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FileDirPath>this.selectedEntities[0]).directory = directory_newVal;
        }

        public get _directory():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._directory;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FileDirPath>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FileDirPath>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get fileType():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FileDirPath>this.selectedEntities[0]).fileType;
        }

        public set fileType(fileType_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FileDirPath>this.selectedEntities[0]).fileType = fileType_newVal;
        }

        public get _fileType():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._fileType;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpFileDirPath) { super($scope); }
    }



    export interface IScopeGrpFileDirPathBase extends Controllers.IAbstractTableGroupScope, IPageScope{
        globals: Globals;
        modelGrpFileDirPath : ModelGrpFileDirPath;
        GrpFileDirPath_itm__fileType_disabled(fileDirPath:Entities.FileDirPath):boolean; 
        GrpFileDirPath_itm__directory_disabled(fileDirPath:Entities.FileDirPath):boolean; 

    }



    export class ControllerGrpFileDirPathBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpFileDirPath,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpFileDirPath)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpFileDirPathBase = this;
            model.controller = <ControllerGrpFileDirPath>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpFileDirPath = self.model;

            $scope.GrpFileDirPath_itm__fileType_disabled = 
                (fileDirPath:Entities.FileDirPath) => {
                    return self.GrpFileDirPath_itm__fileType_disabled(fileDirPath);
                };
            $scope.GrpFileDirPath_itm__directory_disabled = 
                (fileDirPath:Entities.FileDirPath) => {
                    return self.GrpFileDirPath_itm__directory_disabled(fileDirPath);
                };


            $scope.pageModel.modelGrpFileDirPath = $scope.modelGrpFileDirPath;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };


            self.$timeout( () => {
                self.updateUI();

                },0);

    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.FileDirPath[] , oldVisible:Entities.FileDirPath[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpFileDirPath";
        }
        public get HtmlDivId(): string {
            return "FileDirPath_ControllerGrpFileDirPath";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new StaticListItem<number> (
                        (ent?: Entities.FileDirPath) => 'Subject',
                        false ,
                        (ent?: Entities.FileDirPath) => ent.fileType,  
                        (ent?: Entities.FileDirPath) => ent._fileType,  
                        (ent?: Entities.FileDirPath) => true, //isRequired
                        (vl: number, ent?: Entities.FileDirPath) => new NpTypes.ValidationResult(true, "")),
                    new TextItem (
                        (ent?: Entities.FileDirPath) => 'File | Directory',
                        false ,
                        (ent?: Entities.FileDirPath) => ent.directory,  
                        (ent?: Entities.FileDirPath) => ent._directory,  
                        (ent?: Entities.FileDirPath) => true, 
                        (vl: string, ent?: Entities.FileDirPath) => new NpTypes.ValidationResult(true, ""), 
                        undefined)
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Files and Directories Preferences";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'fileType', displayName:'getALString("Subject", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_FileDirPath_W"), resizable:true, sortable:true, enableCellEdit:false, width:'30%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpFileDirPath_itm__fileType' data-ng-model='row.entity.fileType' data-np-ui-model='row.entity._fileType' data-ng-change='markEntityAsUpdated(row.entity,&quot;fileType&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Temporary Files Directory for Importing Shape Files\"), value:1}]' data-ng-disabled='GrpFileDirPath_itm__fileType_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'directory', displayName:'getALString("File | Directory", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_FileDirPath_W"), resizable:true, sortable:true, enableCellEdit:false, width:'23%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpFileDirPath_itm__directory' data-ng-model='row.entity.directory' data-np-ui-model='row.entity._directory' data-ng-change='markEntityAsUpdated(row.entity,&quot;directory&quot;)' data-np-required='true' data-ng-readonly='GrpFileDirPath_itm__directory_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpFileDirPath():ModelGrpFileDirPath {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "FileDirPath";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = undefined;

        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.FileDirPath.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.FileDirPath {
            var self = this;
            return <Entities.FileDirPath>self.$scope.modelGrpFileDirPath.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.FileDirPath, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "FileDirPath/findAllByCriteriaRange_FileDirPathGrpFileDirPath_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;


                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "FileDirPath/findAllByCriteriaRange_FileDirPathGrpFileDirPath";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }


                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "FileDirPath/findAllByCriteriaRange_FileDirPathGrpFileDirPath_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;


                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: Tuple2<boolean, Entities.FileDirPath[] > = undefined;
        private bNonContinuousCallersFuncs: Array<(items: Entities.FileDirPath[]) => void> = [];


        public getMergedItems(func: (items: Entities.FileDirPath[]) => void, bContinuousCaller:boolean=true, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.FileDirPath[]):void {
                var mergedEntities = <Entities.FileDirPath[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        map(e => <Entities.FileDirPath>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }



                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache !== undefined &&
                        self.getMergedItems_cache.a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache = new Tuple2(true, undefined);

                    var wsPath = "FileDirPath/findAllByCriteriaRange_FileDirPathGrpFileDirPath";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);


                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.FileDirPath[]>self.getEntitiesFromJSON(response);
                            self.getMergedItems_cache.a = false;
                            self.getMergedItems_cache.b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache.a = false;
                            self.getMergedItems_cache.b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache.a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache.b;
                        processDBItems(dbEntities);
                    }
                }
        }









        public constructEntity(): Entities.FileDirPath {
            var self = this;
            var ret = new Entities.FileDirPath(
                /*fidrId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*directory:string*/ null,
                /*rowVersion:number*/ null,
                /*fileType:number*/ 1);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.FileDirPath = <Entities.FileDirPath>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.FileDirPath): Entities.FileDirPath {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.FileDirPath, calledByParent: boolean= false): Entities.FileDirPath {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.FileDirPath.Create();
            ret.updateInstance(src);
            ret.fidrId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.FileDirPath, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public GrpFileDirPath_itm__fileType_disabled(fileDirPath:Entities.FileDirPath):boolean {
            var self = this;
            if (fileDirPath === undefined || fileDirPath === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_FileDirPath_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(fileDirPath, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpFileDirPath_itm__directory_disabled(fileDirPath:Entities.FileDirPath):boolean {
            var self = this;
            if (fileDirPath === undefined || fileDirPath === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_FileDirPath_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(fileDirPath, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_FileDirPath_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = false;
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(fileDirPath:Entities.FileDirPath):boolean  {
            var self = this;
            if (fileDirPath === null || fileDirPath === undefined || fileDirPath.getEntityName() !== "FileDirPath")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_FileDirPath_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(fileDirPath, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
