var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/FileTemplateBase.ts" />
/// <reference path="../EntitiesBase/TemplateColumnBase.ts" />
/// <reference path="../EntitiesBase/PredefColBase.ts" />
/// <reference path="LovPredefColBase.ts" />
/// <reference path="../Controllers/FileTemplate.ts" />
var Controllers;
(function (Controllers) {
    var FileTemplate;
    (function (FileTemplate) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        FileTemplate.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $scope.onNewBtnAction = function () {
                    self.onNewBtnAction();
                };
                $scope.onDeleteBtnAction = function () {
                    self.onDeleteBtnAction();
                };
                $timeout(function () {
                    $('#FileTemplate').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "FileTemplate";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Data Import Template";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpFileTemplate.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpFileTemplate.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpTemplateColumn.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpFileTemplate.controller.refreshVisibleEntities(newEntitiesIds, function () {
                    self.model.modelGrpTemplateColumn.controller.updateUI();
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpFileTemplate.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpFileTemplate.controller,
                            self.model.modelGrpTemplateColumn.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                    pageChanges = pageChanges.concat(self.model.modelGrpFileTemplate.controller.getChangesToCommitForDeletion());
                    pageChanges = pageChanges.concat(self.model.modelGrpFileTemplate.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpTemplateColumn.controller.getDeleteChangesToCommit());
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpFileTemplate.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpTemplateColumn.controller.getChangesToCommit());
                }
                var hasFileTemplate = pageChanges.some(function (change) { return change.entityName === "FileTemplate"; });
                if (!hasFileTemplate) {
                    var validateEntity = new Controllers.ChangeToCommit(Controllers.ChangeStatus.UPDATE, 0, "FileTemplate", self.model.modelGrpFileTemplate.controller.Current);
                    pageChanges = pageChanges.concat(validateEntity);
                }
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_FileTemplate";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpFileTemplate.controller.cleanUpAfterSave();
                self.model.modelGrpTemplateColumn.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = self.$scope.getCurrentPageModel();
                        var newFileTemplateId = response.newEntitiesIds.firstOrNull(function (x) { return x.entityName === 'FileTemplate'; });
                        if (!isVoid(newFileTemplateId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.FileTemplate.CreateById(newFileTemplateId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.getInitialEntity = function () {
                if (this.shownAsDialog()) {
                    return this.dialogSelectedEntity();
                }
                else {
                    var breadCrumbStepModel = this.$scope.getPageModelByURL('/FileTemplate');
                    if (isVoid(breadCrumbStepModel)) {
                        return null;
                    }
                    else {
                        return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                    }
                }
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            PageControllerBase.prototype.onNewBtnAction = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                self.addNewRecord();
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () { self.addNewRecord(); }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    self.addNewRecord();
                }
            };
            PageControllerBase.prototype.addNewRecord = function () {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                self.model.modelGrpFileTemplate.controller.cleanUpAfterSave();
                self.model.modelGrpTemplateColumn.controller.cleanUpAfterSave();
                self.model.modelGrpFileTemplate.controller.createNewEntityAndAddToUI();
            };
            PageControllerBase.prototype.onDeleteBtnAction = function () {
                var self = this;
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                    new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(); }),
                    new Tuple2("MessageBox_Button_No", function () { })
                ], 1, 1);
            };
            PageControllerBase.prototype.deleteRecord = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW) {
                    console.log("Delete pressed in a form while being in new mode!");
                    return;
                }
                NpTypes.AlertMessage.clearAlerts(self.model);
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData(true);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "Mode", {
                get: function () {
                    var self = this;
                    if (isVoid(self.model.modelGrpFileTemplate) || isVoid(self.model.modelGrpFileTemplate.controller))
                        return Controllers.EditMode.NEW;
                    return self.model.modelGrpFileTemplate.controller.Mode;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._newIsDisabled = function () {
                var self = this;
                if (isVoid(self.model.modelGrpFileTemplate) || isVoid(self.model.modelGrpFileTemplate.controller))
                    return true;
                return self.model.modelGrpFileTemplate.controller._newIsDisabled();
            };
            PageControllerBase.prototype._deleteIsDisabled = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW)
                    return true;
                if (isVoid(self.model.modelGrpFileTemplate) || isVoid(self.model.modelGrpFileTemplate.controller))
                    return true;
                return self.model.modelGrpFileTemplate.controller._deleteIsDisabled(self.model.modelGrpFileTemplate.controller.Current);
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        FileTemplate.PageControllerBase = PageControllerBase;
        // GROUP GrpFileTemplate
        var ModelGrpFileTemplateBase = (function (_super) {
            __extends(ModelGrpFileTemplateBase, _super);
            function ModelGrpFileTemplateBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "fiteId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].fiteId;
                },
                set: function (fiteId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].fiteId = fiteId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_fiteId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._fiteId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].name;
                },
                set: function (name_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].name = name_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpFileTemplateBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpFileTemplateBase;
        })(Controllers.AbstractGroupFormModel);
        FileTemplate.ModelGrpFileTemplateBase = ModelGrpFileTemplateBase;
        var ControllerGrpFileTemplateBase = (function (_super) {
            __extends(ControllerGrpFileTemplateBase, _super);
            function ControllerGrpFileTemplateBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                $scope.modelGrpFileTemplate = self.model;
                var selectedEntity = this.$scope.pageModel.controller.getInitialEntity();
                if (isVoid(selectedEntity)) {
                    self.createNewEntityAndAddToUI();
                }
                else {
                    var clonedEntity = Entities.FileTemplate.Create();
                    clonedEntity.updateInstance(selectedEntity);
                    $scope.modelGrpFileTemplate.visibleEntities[0] = clonedEntity;
                    $scope.modelGrpFileTemplate.selectedEntities[0] = clonedEntity;
                }
                $scope.GrpFileTemplate_itm__name_disabled =
                    function (fileTemplate) {
                        return self.GrpFileTemplate_itm__name_disabled(fileTemplate);
                    };
                $scope.GrpFileTemplate_0_disabled =
                    function () {
                        return self.GrpFileTemplate_0_disabled();
                    };
                $scope.GrpFileTemplate_0_invisible =
                    function () {
                        return self.GrpFileTemplate_0_invisible();
                    };
                $scope.child_group_GrpTemplateColumn_isInvisible =
                    function () {
                        return self.child_group_GrpTemplateColumn_isInvisible();
                    };
                $scope.pageModel.modelGrpFileTemplate = $scope.modelGrpFileTemplate;
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.FileTemplate[] , oldVisible:Entities.FileTemplate[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                //bind entity child collection with child controller merged items
                Entities.FileTemplate.templateColumnCollection = function (fileTemplate, func) {
                    _this.model.modelGrpTemplateColumn.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, fileTemplate);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.FileTemplate.templateColumnCollection = null; //(fileTemplate, func) => { }
                });
            }
            ControllerGrpFileTemplateBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpFileTemplateBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpFileTemplate";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpFileTemplateBase.prototype, "HtmlDivId", {
                get: function () {
                    return "FileTemplate_ControllerGrpFileTemplate";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpFileTemplateBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Name'; }, false, function (ent) { return ent.name; }, function (ent) { return ent._name; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpFileTemplateBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpFileTemplateBase.prototype, "modelGrpFileTemplate", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpFileTemplateBase.prototype.getEntityName = function () {
                return "FileTemplate";
            };
            ControllerGrpFileTemplateBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
            };
            ControllerGrpFileTemplateBase.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
                var self = this;
                if (self.$scope.pageModel.controller.shownAsDialog())
                    return;
                var breadCrumbStepModel = { selectedEntity: null };
                if (!isVoid(self.Current)) {
                    var selectedEntity = Entities.FileTemplate.Create();
                    selectedEntity.updateInstance(self.Current);
                    breadCrumbStepModel.selectedEntity = selectedEntity;
                }
                else {
                    breadCrumbStepModel.selectedEntity = null;
                }
                self.$scope.setCurrentPageModel(breadCrumbStepModel);
            };
            ControllerGrpFileTemplateBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.FileTemplate.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpFileTemplateBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpFileTemplate.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpFileTemplateBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpFileTemplateBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.FileTemplate(
                /*fiteId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*name:string*/ null, 
                /*rowVersion:number*/ null);
                return ret;
            };
            ControllerGrpFileTemplateBase.prototype.createNewEntityAndAddToUI = function () {
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.$scope.modelGrpFileTemplate.visibleEntities[0] = newEnt;
                self.$scope.modelGrpFileTemplate.selectedEntities[0] = newEnt;
                return newEnt;
            };
            ControllerGrpFileTemplateBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpFileTemplateBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.FileTemplate.Create();
                ret.updateInstance(src);
                ret.fiteId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.visibleEntities[0] = ret;
                this.model.selectedEntities[0] = ret;
                this.$timeout(function () {
                    _this.modelGrpFileTemplate.modelGrpTemplateColumn.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpFileTemplateBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpFileTemplate.modelGrpTemplateColumn.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpFileTemplateBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpFileTemplate.modelGrpTemplateColumn.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpFileTemplate.modelGrpTemplateColumn.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpFileTemplateBase.prototype.GrpFileTemplate_itm__name_disabled = function (fileTemplate) {
                var self = this;
                if (fileTemplate === undefined || fileTemplate === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(fileTemplate, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpFileTemplateBase.prototype.GrpFileTemplate_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpFileTemplateBase.prototype.GrpFileTemplate_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpFileTemplateBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpFileTemplateBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpFileTemplateBase.prototype._newIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"))
                    return true; // no write privilege
                var parEntityIsLocked = false;
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpFileTemplateBase.prototype._deleteIsDisabled = function (fileTemplate) {
                var self = this;
                if (fileTemplate === null || fileTemplate === undefined || fileTemplate.getEntityName() !== "FileTemplate")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(fileTemplate, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpFileTemplateBase.prototype.child_group_GrpTemplateColumn_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpTemplateColumn === undefined) || (self.model.modelGrpTemplateColumn.controller === undefined))
                    return false;
                return self.model.modelGrpTemplateColumn.controller._isInvisible();
            };
            return ControllerGrpFileTemplateBase;
        })(Controllers.AbstractGroupFormController);
        FileTemplate.ControllerGrpFileTemplateBase = ControllerGrpFileTemplateBase;
        // GROUP GrpTemplateColumn
        var ModelGrpTemplateColumnBase = (function (_super) {
            __extends(ModelGrpTemplateColumnBase, _super);
            function ModelGrpTemplateColumnBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "tecoId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].tecoId;
                },
                set: function (tecoId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].tecoId = tecoId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_tecoId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._tecoId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "clfierName", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].clfierName;
                },
                set: function (clfierName_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].clfierName = clfierName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_clfierName", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._clfierName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "fiteId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].fiteId;
                },
                set: function (fiteId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].fiteId = fiteId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_fiteId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._fiteId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "prcoId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].prcoId;
                },
                set: function (prcoId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].prcoId = prcoId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_prcoId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._prcoId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpTemplateColumnBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpTemplateColumnBase;
        })(Controllers.AbstractGroupTableModel);
        FileTemplate.ModelGrpTemplateColumnBase = ModelGrpTemplateColumnBase;
        var ControllerGrpTemplateColumnBase = (function (_super) {
            __extends(ControllerGrpTemplateColumnBase, _super);
            function ControllerGrpTemplateColumnBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpTemplateColumn = self.model;
                $scope.showLov_GrpTemplateColumn_itm__prcoId =
                    function (templateColumn) {
                        $timeout(function () {
                            self.showLov_GrpTemplateColumn_itm__prcoId(templateColumn);
                        }, 0);
                    };
                $scope.GrpTemplateColumn_itm__clfierName_disabled =
                    function (templateColumn) {
                        return self.GrpTemplateColumn_itm__clfierName_disabled(templateColumn);
                    };
                $scope.GrpTemplateColumn_0_disabled =
                    function () {
                        return self.GrpTemplateColumn_0_disabled();
                    };
                $scope.GrpTemplateColumn_0_invisible =
                    function () {
                        return self.GrpTemplateColumn_0_invisible();
                    };
                $scope.pageModel.modelGrpTemplateColumn = $scope.modelGrpTemplateColumn;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpFileTemplate.modelGrpTemplateColumn = $scope.modelGrpTemplateColumn;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpFileTemplate.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.TemplateColumn[] , oldVisible:Entities.TemplateColumn[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpTemplateColumnBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpTemplateColumn";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "HtmlDivId", {
                get: function () {
                    return "FileTemplate_ControllerGrpTemplateColumn";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.LovItem(function (ent) { return 'System Column'; }, false, function (ent) { return ent.prcoId; }, function (ent) { return ent._prcoId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.TextItem(function (ent) { return 'Import Column'; }, false, function (ent) { return ent.clfierName; }, function (ent) { return ent._clfierName; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpTemplateColumnBase.prototype.gridTitle = function () {
                return "Columns Assignments";
            };
            ControllerGrpTemplateColumnBase.prototype.getColumnTooltip = function (fieldName) {
                if (fieldName === 'clfierName')
                    return "You Can Update the Import Columns To Your File Schema";
                return "";
            };
            ControllerGrpTemplateColumnBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpTemplateColumnBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'prcoId.columnName', displayName: 'getALString("System Column", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div  > \
                    <input data-np-source='row.entity.prcoId.columnName' data-np-ui-model='row.entity._prcoId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpTemplateColumn_itm__prcoId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpTemplateColumn_itm__prcoId(row.entity)'   data-ng-disabled=\"true\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'clfierName', displayName: 'getALString("Import Column", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', headerCellTemplate: this.getGridHeaderCellTemplate({ tooltip: "{{getColumnTooltip('clfierName')}}" }), cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpTemplateColumn_itm__clfierName' data-ng-model='row.entity.clfierName' data-np-ui-model='row.entity._clfierName' data-ng-change='markEntityAsUpdated(row.entity,&quot;clfierName&quot;)' data-np-required='true' data-ng-readonly='GrpTemplateColumn_itm__clfierName_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "modelGrpTemplateColumn", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpTemplateColumnBase.prototype.getEntityName = function () {
                return "TemplateColumn";
            };
            ControllerGrpTemplateColumnBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpTemplateColumnBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.TemplateColumn.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.fiteId = _this.Parent;
                    }
                    else {
                        x.fiteId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpTemplateColumn.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpTemplateColumnBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpFileTemplate.controller.isEntityLocked(cur.fiteId, lockKind);
            };
            ControllerGrpTemplateColumnBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.fiteId)) {
                    paramData['fiteId_fiteId'] = self.Parent.fiteId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpTemplateColumnBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "TemplateColumn/findAllByCriteriaRange_FileTemplateGrpTemplateColumn_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.fiteId)) {
                    paramData['fiteId_fiteId'] = self.Parent.fiteId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpTemplateColumnBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "TemplateColumn/findAllByCriteriaRange_FileTemplateGrpTemplateColumn";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.fiteId)) {
                    paramData['fiteId_fiteId'] = self.Parent.fiteId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpTemplateColumnBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "TemplateColumn/findAllByCriteriaRange_FileTemplateGrpTemplateColumn_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.fiteId)) {
                    paramData['fiteId_fiteId'] = self.Parent.fiteId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpTemplateColumnBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.fiteId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "TemplateColumn/findAllByCriteriaRange_FileTemplateGrpTemplateColumn";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['fiteId_fiteId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpTemplateColumnBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.fiteId === undefined)
                    return false;
                return x.fiteId.getKey() === this.Parent.getKey();
            };
            ControllerGrpTemplateColumnBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.fiteId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpFileTemplate.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "fiteId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpFileTemplate.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpTemplateColumnBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.TemplateColumn(
                /*tecoId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*clfierName:string*/ null, 
                /*rowVersion:number*/ null, 
                /*fiteId:Entities.FileTemplate*/ null, 
                /*prcoId:Entities.PredefCol*/ null);
                return ret;
            };
            ControllerGrpTemplateColumnBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.fiteId = self.fiteId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpTemplateColumnBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpTemplateColumnBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.TemplateColumn.Create();
                ret.updateInstance(src);
                ret.tecoId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpTemplateColumnBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (templateColumnList) {
                    templateColumnList.forEach(function (templateColumn) {
                        var templateColumnCloned = _this.cloneEntity_internal(templateColumn, true);
                        templateColumnCloned.fiteId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpTemplateColumnBase.prototype.deleteNewEntitiesUnderParent = function (fileTemplate) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var templateColumn = change.a;
                    if (fileTemplate.getKey() === templateColumn.fiteId.getKey()) {
                        toBeDeleted.push(templateColumn);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpTemplateColumnBase.prototype.deleteAllEntitiesUnderParent = function (fileTemplate, afterDeleteAction) {
                var self = this;
                function deleteTemplateColumnList(templateColumnList) {
                    if (templateColumnList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = templateColumnList[0];
                        var tail = templateColumnList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteTemplateColumnList(tail);
                        });
                    }
                }
                this.getMergedItems(function (templateColumnList) {
                    deleteTemplateColumnList(templateColumnList);
                }, false, fileTemplate);
            };
            Object.defineProperty(ControllerGrpTemplateColumnBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpTemplateColumnBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpTemplateColumnBase.prototype.showLov_GrpTemplateColumn_itm__prcoId = function (templateColumn) {
                var self = this;
                if (templateColumn === undefined)
                    return;
                var uimodel = templateColumn._prcoId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Predefined Columns';
                dialogOptions.previousModel = self.cachedLovModel_GrpTemplateColumn_itm__prcoId;
                dialogOptions.className = "ControllerLovPredefCol";
                dialogOptions.onSelect = function (selectedEntity) {
                    var predefCol1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(predefCol1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(predefCol1) && predefCol1.isEqual(templateColumn.prcoId))
                        return;
                    templateColumn.prcoId = predefCol1;
                    self.markEntityAsUpdated(templateColumn, 'GrpTemplateColumn_itm__prcoId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpTemplateColumn_itm__prcoId = lovModel;
                    var wsPath = "PredefCol/findAllByCriteriaRange_FileTemplate_GrpTemplateColumn_itm__prcoId";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_PredefColLov_columnName)) {
                        paramData['fsch_PredefColLov_columnName'] = lovModel.fsch_PredefColLov_columnName;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "PredefCol/findAllByCriteriaRange_FileTemplate_GrpTemplateColumn_itm__prcoId_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_PredefColLov_columnName)) {
                        paramData['fsch_PredefColLov_columnName'] = lovModel.fsch_PredefColLov_columnName;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_PredefColLov_columnName)) {
                        paramData['fsch_PredefColLov_columnName'] = lovModel.fsch_PredefColLov_columnName;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['columnName'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/PredefCol.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpTemplateColumnBase.prototype.GrpTemplateColumn_itm__clfierName_disabled = function (templateColumn) {
                var self = this;
                if (templateColumn === undefined || templateColumn === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(templateColumn, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpTemplateColumnBase.prototype.GrpTemplateColumn_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpTemplateColumnBase.prototype.GrpTemplateColumn_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpTemplateColumnBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpFileTemplate_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpTemplateColumnBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpFileTemplate_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpTemplateColumnBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpTemplateColumnBase.prototype._deleteIsDisabled = function (templateColumn) {
                return true;
            };
            return ControllerGrpTemplateColumnBase;
        })(Controllers.AbstractGroupTableController);
        FileTemplate.ControllerGrpTemplateColumnBase = ControllerGrpTemplateColumnBase;
    })(FileTemplate = Controllers.FileTemplate || (Controllers.FileTemplate = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=FileTemplateBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
