/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../Controllers/ParcelGPSearch.ts" />
var Controllers;
(function (Controllers) {
    var ModelParcelGPSearchBase = (function (_super) {
        __extends(ModelParcelGPSearchBase, _super);
        function ModelParcelGPSearchBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._parcIdentifier = new NpTypes.UIStringModel(undefined);
            this._parcCode = new NpTypes.UIStringModel(undefined);
            this._prodCode = new NpTypes.UINumberModel(undefined);
            this._clasId = new NpTypes.UIManyToOneModel(undefined);
        }
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "parcIdentifier", {
            get: function () {
                return this._parcIdentifier.value;
            },
            set: function (vl) {
                this._parcIdentifier.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "parcCode", {
            get: function () {
                return this._parcCode.value;
            },
            set: function (vl) {
                this._parcCode.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "prodCode", {
            get: function () {
                return this._prodCode.value;
            },
            set: function (vl) {
                this._prodCode.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "clasId", {
            get: function () {
                return this._clasId.value;
            },
            set: function (vl) {
                this._clasId.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelParcelGPSearchBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelParcelGPSearchBase;
    })(Controllers.AbstractSearchPageModel);
    Controllers.ModelParcelGPSearchBase = ModelParcelGPSearchBase;
    var ModelParcelGPSearchPersistedModel = (function () {
        function ModelParcelGPSearchPersistedModel() {
        }
        return ModelParcelGPSearchPersistedModel;
    })();
    Controllers.ModelParcelGPSearchPersistedModel = ModelParcelGPSearchPersistedModel;
    var ControllerParcelGPSearchBase = (function (_super) {
        __extends(ControllerParcelGPSearchBase, _super);
        function ControllerParcelGPSearchBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'Geotagged Photos Decisions';
            var lastSearchPageModel = $scope.getPageModelByURL('/ParcelGPSearch');
            var rowIndexToSelect = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.parcIdentifier = lastSearchPageModel.parcIdentifier;
                self.pageModel.parcCode = lastSearchPageModel.parcCode;
                self.pageModel.prodCode = lastSearchPageModel.prodCode;
                self.pageModel.clasId = lastSearchPageModel.clasId;
            }
            $scope._newIsDisabled =
                function () {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled =
                function () {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled =
                function (parcelClas) {
                    return self._deleteIsDisabled(parcelClas);
                };
            $scope._editBtnIsDisabled =
                function (parcelClas) {
                    return self._editBtnIsDisabled(parcelClas);
                };
            $scope._cancelIsDisabled =
                function () {
                    return self._cancelIsDisabled();
                };
            $scope.d__parcIdentifier_disabled =
                function () {
                    return self.d__parcIdentifier_disabled();
                };
            $scope.d__parcCode_disabled =
                function () {
                    return self.d__parcCode_disabled();
                };
            $scope.d__prodCode_disabled =
                function () {
                    return self.d__prodCode_disabled();
                };
            $scope.d__clasId_disabled =
                function () {
                    return self.d__clasId_disabled();
                };
            $scope.showLov_d__clasId =
                function () {
                    $timeout(function () {
                        self.showLov_d__clasId();
                    }, 0);
                };
            $scope.clearBtnAction = function () {
                self.pageModel.parcIdentifier = undefined;
                self.pageModel.parcCode = undefined;
                self.pageModel.prodCode = undefined;
                self.pageModel.clasId = undefined;
                self.updateGrid(0, false, true);
            };
            $scope.newBtnAction = function () {
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };
            $scope.onEdit = function (x) {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };
            $timeout(function () {
                $('#Search_ParcelGP').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);
            self.updateUI(rowIndexToSelect);
        }
        ControllerParcelGPSearchBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerParcelGPSearchBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerParcelGPSearch";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerParcelGPSearchBase.prototype, "HtmlDivId", {
            get: function () {
                return "Search_ParcelGP";
            },
            enumerable: true,
            configurable: true
        });
        ControllerParcelGPSearchBase.prototype._getPageTitle = function () {
            return "Parcels Geotagged Photos Decisions";
        };
        ControllerParcelGPSearchBase.prototype.gridColumnFilter = function (field) {
            return true;
        };
        ControllerParcelGPSearchBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            return [
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { cellClass: 'cellToolTip', field: 'parcIdentifier', displayName: 'getALString("Parcel Identifier", true)', requiredAsterisk: false, resizable: true, sortable: false, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.parcIdentifier' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'parcCode', displayName: 'getALString("Parcel Code", true)', requiredAsterisk: false, resizable: true, sortable: false, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.parcCode' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'prodCode', displayName: 'getALString("Producer Code", true)', requiredAsterisk: false, resizable: true, sortable: false, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.prodCode.toStringFormatted(\",\", \"\", 0)' style='width: 100%;text-align: right;padding-right: 6px;' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'clasId.name', displayName: 'getALString("Classification", true)', requiredAsterisk: false, resizable: true, sortable: false, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.clasId.name' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
        };
        ControllerParcelGPSearchBase.prototype.getPersistedModel = function () {
            var self = this;
            var ret = new ModelParcelGPSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.parcIdentifier = self.pageModel.parcIdentifier;
            ret.parcCode = self.pageModel.parcCode;
            ret.prodCode = self.pageModel.prodCode;
            ret.clasId = self.pageModel.clasId;
            return ret;
        };
        Object.defineProperty(ControllerParcelGPSearchBase.prototype, "pageModel", {
            get: function () {
                return this.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerParcelGPSearchBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.ParcelClas.fromJSONComplete(webResponse.data);
        };
        ControllerParcelGPSearchBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "ParcelClas/findLazyParcelGP";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodCode)) {
                paramData['prodCode'] = self.pageModel.prodCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcIdentifier)) {
                paramData['parcIdentifier'] = self.pageModel.parcIdentifier;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcCode)) {
                paramData['parcCode'] = self.pageModel.parcCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerParcelGPSearchBase.prototype.makeWebRequest_count = function (excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "ParcelClas/findLazyParcelGP_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodCode)) {
                paramData['prodCode'] = self.pageModel.prodCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcIdentifier)) {
                paramData['parcIdentifier'] = self.pageModel.parcIdentifier;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcCode)) {
                paramData['parcCode'] = self.pageModel.parcCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerParcelGPSearchBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var paramData = {};
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.prodCode)) {
                paramData['prodCode'] = self.pageModel.prodCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcIdentifier)) {
                paramData['parcIdentifier'] = self.pageModel.parcIdentifier;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.parcCode)) {
                paramData['parcCode'] = self.pageModel.parcCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
            return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        };
        Object.defineProperty(ControllerParcelGPSearchBase.prototype, "Current", {
            get: function () {
                var self = this;
                return self.$scope.pageModel.selectedEntities[0];
            },
            enumerable: true,
            configurable: true
        });
        ControllerParcelGPSearchBase.prototype.onNew = function () {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/ParcelGP', self._getPageTitle(), {});
        };
        ControllerParcelGPSearchBase.prototype.onEdit = function (x) {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel = { "selectedEntity": x };
            self.$scope.navigateForward('/ParcelGP', self._getPageTitle(), visitedPageModel);
        };
        ControllerParcelGPSearchBase.prototype.getSynchronizeChangesWithDbUrl = function () {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_ParcelGP";
        };
        ControllerParcelGPSearchBase.prototype.getSynchronizeChangesWithDbData = function (x) {
            var self = this;
            var paramData = {
                data: []
            };
            var changeToCommit = new Controllers.ChangeToCommit(Controllers.ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);
            paramData.data.push(changeToCommit);
            return paramData;
        };
        ControllerParcelGPSearchBase.prototype.deleteRecord = function (x) {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
            console.log("deleting ParcelClas with PK field:" + x.pclaId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                self.updateGrid();
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        };
        ControllerParcelGPSearchBase.prototype._newIsDisabled = function () {
            return true;
        };
        ControllerParcelGPSearchBase.prototype._searchIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerParcelGPSearchBase.prototype._deleteIsDisabled = function (parcelClas) {
            return true;
        };
        ControllerParcelGPSearchBase.prototype._editBtnIsDisabled = function (parcelClas) {
            var self = this;
            if (parcelClas === undefined || parcelClas === null)
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_ParcelGP_R"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerParcelGPSearchBase.prototype._cancelIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerParcelGPSearchBase.prototype.d__parcIdentifier_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerParcelGPSearchBase.prototype.d__parcCode_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerParcelGPSearchBase.prototype.d__prodCode_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerParcelGPSearchBase.prototype.d__clasId_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerParcelGPSearchBase.prototype.showLov_d__clasId = function () {
            var self = this;
            var uimodel = self.pageModel._clasId;
            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Data Import Name';
            dialogOptions.previousModel = self.cachedLovModel_d__clasId;
            dialogOptions.className = "ControllerLovClassification";
            dialogOptions.onSelect = function (selectedEntity) {
                var classification1 = selectedEntity;
                uimodel.clearAllErrors();
                if (false && isVoid(classification1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                if (!isVoid(classification1) && classification1.isEqual(self.pageModel.clasId))
                    return;
                self.pageModel.clasId = classification1;
            };
            dialogOptions.openNewEntityDialog = function () {
            };
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                self.cachedLovModel_d__clasId = lovModel;
                var wsPath = "Classification/findAllByCriteriaRange_forLov";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                    paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                    paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                    paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var wsPath = "Classification/findAllByCriteriaRange_forLov_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                    paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                    paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                    paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                var paramData = {};
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                    paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                    paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                    paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;
            dialogOptions.shownCols['dateTime'] = true;
            dialogOptions.shownCols['recordtype'] = true;
            dialogOptions.shownCols['filePath'] = true;
            dialogOptions.shownCols['attachedFile'] = true;
            dialogOptions.shownCols['year'] = true;
            self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Classification.html?rev=' + self.$scope.globals.version, dialogOptions);
        };
        return ControllerParcelGPSearchBase;
    })(Controllers.AbstractSearchPageController);
    Controllers.ControllerParcelGPSearchBase = ControllerParcelGPSearchBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelGPSearchBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
