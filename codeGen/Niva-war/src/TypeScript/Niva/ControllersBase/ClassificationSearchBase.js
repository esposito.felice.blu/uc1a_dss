/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="../EntitiesBase/ClassifierBase.ts" />
/// <reference path="LovClassifierBase.ts" />
/// <reference path="../Controllers/ClassificationSearch.ts" />
var Controllers;
(function (Controllers) {
    var ModelClassificationSearchBase = (function (_super) {
        __extends(ModelClassificationSearchBase, _super);
        function ModelClassificationSearchBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._name = new NpTypes.UIStringModel(undefined);
            this._year = new NpTypes.UINumberModel(undefined);
            this._dateTime = new NpTypes.UIDateModel(undefined);
            this._clfrId = new NpTypes.UIManyToOneModel(undefined);
        }
        Object.defineProperty(ModelClassificationSearchBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (vl) {
                this._name.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "year", {
            get: function () {
                return this._year.value;
            },
            set: function (vl) {
                this._year.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "dateTime", {
            get: function () {
                return this._dateTime.value;
            },
            set: function (vl) {
                this._dateTime.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "clfrId", {
            get: function () {
                return this._clfrId.value;
            },
            set: function (vl) {
                this._clfrId.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelClassificationSearchBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelClassificationSearchBase;
    })(Controllers.AbstractSearchPageModel);
    Controllers.ModelClassificationSearchBase = ModelClassificationSearchBase;
    var ModelClassificationSearchPersistedModel = (function () {
        function ModelClassificationSearchPersistedModel() {
        }
        return ModelClassificationSearchPersistedModel;
    })();
    Controllers.ModelClassificationSearchPersistedModel = ModelClassificationSearchPersistedModel;
    var ControllerClassificationSearchBase = (function (_super) {
        __extends(ControllerClassificationSearchBase, _super);
        function ControllerClassificationSearchBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'Classification Data Import';
            var lastSearchPageModel = $scope.getPageModelByURL('/ClassificationSearch');
            var rowIndexToSelect = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.name = lastSearchPageModel.name;
                self.pageModel.year = lastSearchPageModel.year;
                self.pageModel.dateTime = lastSearchPageModel.dateTime;
                self.pageModel.clfrId = lastSearchPageModel.clfrId;
            }
            $scope._newIsDisabled =
                function () {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled =
                function () {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled =
                function (classification) {
                    return self._deleteIsDisabled(classification);
                };
            $scope._editBtnIsDisabled =
                function (classification) {
                    return self._editBtnIsDisabled(classification);
                };
            $scope._cancelIsDisabled =
                function () {
                    return self._cancelIsDisabled();
                };
            $scope.d__name_disabled =
                function () {
                    return self.d__name_disabled();
                };
            $scope.d__year_disabled =
                function () {
                    return self.d__year_disabled();
                };
            $scope.d__dateTime_disabled =
                function () {
                    return self.d__dateTime_disabled();
                };
            $scope.d__clfrId_disabled =
                function () {
                    return self.d__clfrId_disabled();
                };
            $scope.showLov_d__clfrId =
                function () {
                    $timeout(function () {
                        self.showLov_d__clfrId();
                    }, 0);
                };
            $scope.clearBtnAction = function () {
                self.pageModel.name = undefined;
                self.pageModel.year = undefined;
                self.pageModel.dateTime = undefined;
                self.pageModel.clfrId = undefined;
                self.updateGrid(0, false, true);
            };
            $scope.newBtnAction = function () {
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };
            $scope.onEdit = function (x) {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };
            $timeout(function () {
                $('#Search_Classification').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);
            self.updateUI(rowIndexToSelect);
        }
        ControllerClassificationSearchBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerClassificationSearchBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerClassificationSearch";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerClassificationSearchBase.prototype, "HtmlDivId", {
            get: function () {
                return "Search_Classification";
            },
            enumerable: true,
            configurable: true
        });
        ControllerClassificationSearchBase.prototype._getPageTitle = function () {
            return "Classification";
        };
        ControllerClassificationSearchBase.prototype.gridColumnFilter = function (field) {
            return true;
        };
        ControllerClassificationSearchBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            return [
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { cellClass: 'cellToolTip', field: 'name', displayName: 'getALString("Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '14%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.name' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'description', displayName: 'getALString("Description", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '28%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.description' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'year', displayName: 'getALString("Year of Declaration", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.year.toStringFormatted(\",\", \"\", 0)' style='width: 100%;text-align: right;padding-right: 6px;' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'dateTime', displayName: 'getALString("Classification Date", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_dateTime' data-ng-model='row.entity.dateTime' data-np-ui-model='row.entity._dateTime' data-ng-change='markEntityAsUpdated(row.entity,&quot;dateTime&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'clfrId.name', displayName: 'getALString("Classification Engine", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '14%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.clfrId.name' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
        };
        ControllerClassificationSearchBase.prototype.getPersistedModel = function () {
            var self = this;
            var ret = new ModelClassificationSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.name = self.pageModel.name;
            ret.year = self.pageModel.year;
            ret.dateTime = self.pageModel.dateTime;
            ret.clfrId = self.pageModel.clfrId;
            return ret;
        };
        Object.defineProperty(ControllerClassificationSearchBase.prototype, "pageModel", {
            get: function () {
                return this.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerClassificationSearchBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.Classification.fromJSONComplete(webResponse.data);
        };
        ControllerClassificationSearchBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "Classification/findLazyClassification";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.name)) {
                paramData['name'] = self.pageModel.name;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clfrId) && !isVoid(self.pageModel.clfrId.clfrId)) {
                paramData['clfrId_clfrId'] = self.pageModel.clfrId.clfrId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.year)) {
                paramData['year'] = self.pageModel.year;
            }
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerClassificationSearchBase.prototype.makeWebRequest_count = function (excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "Classification/findLazyClassification_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.name)) {
                paramData['name'] = self.pageModel.name;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clfrId) && !isVoid(self.pageModel.clfrId.clfrId)) {
                paramData['clfrId_clfrId'] = self.pageModel.clfrId.clfrId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.year)) {
                paramData['year'] = self.pageModel.year;
            }
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerClassificationSearchBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var paramData = {};
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.name)) {
                paramData['name'] = self.pageModel.name;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clfrId) && !isVoid(self.pageModel.clfrId.clfrId)) {
                paramData['clfrId_clfrId'] = self.pageModel.clfrId.clfrId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.year)) {
                paramData['year'] = self.pageModel.year;
            }
            var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
            return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        };
        Object.defineProperty(ControllerClassificationSearchBase.prototype, "Current", {
            get: function () {
                var self = this;
                return self.$scope.pageModel.selectedEntities[0];
            },
            enumerable: true,
            configurable: true
        });
        ControllerClassificationSearchBase.prototype.onNew = function () {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/Classification', self._getPageTitle(), {});
        };
        ControllerClassificationSearchBase.prototype.onEdit = function (x) {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel = { "selectedEntity": x };
            self.$scope.navigateForward('/Classification', self._getPageTitle(), visitedPageModel);
        };
        ControllerClassificationSearchBase.prototype.getSynchronizeChangesWithDbUrl = function () {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_Classification";
        };
        ControllerClassificationSearchBase.prototype.getSynchronizeChangesWithDbData = function (x) {
            var self = this;
            var paramData = {
                data: []
            };
            var changeToCommit = new Controllers.ChangeToCommit(Controllers.ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);
            paramData.data.push(changeToCommit);
            return paramData;
        };
        ControllerClassificationSearchBase.prototype.deleteRecord = function (x) {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
            console.log("deleting Classification with PK field:" + x.clasId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                self.updateGrid();
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        };
        ControllerClassificationSearchBase.prototype._newIsDisabled = function () {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_Classification_W"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerClassificationSearchBase.prototype._searchIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerClassificationSearchBase.prototype._deleteIsDisabled = function (classification) {
            var self = this;
            if (classification === undefined || classification === null)
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_Classification_D"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerClassificationSearchBase.prototype._editBtnIsDisabled = function (classification) {
            var self = this;
            if (classification === undefined || classification === null)
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_Classification_R"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerClassificationSearchBase.prototype._cancelIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerClassificationSearchBase.prototype.d__name_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerClassificationSearchBase.prototype.d__year_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerClassificationSearchBase.prototype.d__dateTime_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerClassificationSearchBase.prototype.d__clfrId_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerClassificationSearchBase.prototype.showLov_d__clfrId = function () {
            var self = this;
            var uimodel = self.pageModel._clfrId;
            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Classification Engine';
            dialogOptions.previousModel = self.cachedLovModel_d__clfrId;
            dialogOptions.className = "ControllerLovClassifier";
            dialogOptions.onSelect = function (selectedEntity) {
                var classifier1 = selectedEntity;
                uimodel.clearAllErrors();
                if (false && isVoid(classifier1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                if (!isVoid(classifier1) && classifier1.isEqual(self.pageModel.clfrId))
                    return;
                self.pageModel.clfrId = classifier1;
            };
            dialogOptions.openNewEntityDialog = function () {
            };
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                self.cachedLovModel_d__clfrId = lovModel;
                var wsPath = "Classifier/findAllByCriteriaRange_forLov";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_name)) {
                    paramData['fsch_ClassifierLov_name'] = lovModel.fsch_ClassifierLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_description)) {
                    paramData['fsch_ClassifierLov_description'] = lovModel.fsch_ClassifierLov_description;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var wsPath = "Classifier/findAllByCriteriaRange_forLov_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_name)) {
                    paramData['fsch_ClassifierLov_name'] = lovModel.fsch_ClassifierLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_description)) {
                    paramData['fsch_ClassifierLov_description'] = lovModel.fsch_ClassifierLov_description;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                var paramData = {};
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_name)) {
                    paramData['fsch_ClassifierLov_name'] = lovModel.fsch_ClassifierLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassifierLov_description)) {
                    paramData['fsch_ClassifierLov_description'] = lovModel.fsch_ClassifierLov_description;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;
            self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Classifier.html?rev=' + self.$scope.globals.version, dialogOptions);
        };
        return ControllerClassificationSearchBase;
    })(Controllers.AbstractSearchPageController);
    Controllers.ControllerClassificationSearchBase = ControllerClassificationSearchBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ClassificationSearchBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
