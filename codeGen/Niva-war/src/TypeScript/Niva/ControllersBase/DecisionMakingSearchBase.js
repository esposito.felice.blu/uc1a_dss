/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
/// <reference path="LovEcGroupBase.ts" />
/// <reference path="../Controllers/DecisionMakingSearch.ts" />
var Controllers;
(function (Controllers) {
    var ModelDecisionMakingSearchBase = (function (_super) {
        __extends(ModelDecisionMakingSearchBase, _super);
        function ModelDecisionMakingSearchBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._clasId = new NpTypes.UIManyToOneModel(undefined);
            this._ecgrId = new NpTypes.UIManyToOneModel(undefined);
            this._description = new NpTypes.UIStringModel(undefined);
            this._dateTime = new NpTypes.UIDateModel(undefined);
            this._hasBeenRun = new NpTypes.UIBoolModel(undefined);
            this._hasPopulatedIntegratedDecisionsAndIssues = new NpTypes.UIBoolModel(undefined);
        }
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "clasId", {
            get: function () {
                return this._clasId.value;
            },
            set: function (vl) {
                this._clasId.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "ecgrId", {
            get: function () {
                return this._ecgrId.value;
            },
            set: function (vl) {
                this._ecgrId.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "description", {
            get: function () {
                return this._description.value;
            },
            set: function (vl) {
                this._description.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "dateTime", {
            get: function () {
                return this._dateTime.value;
            },
            set: function (vl) {
                this._dateTime.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "hasBeenRun", {
            get: function () {
                return this._hasBeenRun.value;
            },
            set: function (vl) {
                this._hasBeenRun.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "hasPopulatedIntegratedDecisionsAndIssues", {
            get: function () {
                return this._hasPopulatedIntegratedDecisionsAndIssues.value;
            },
            set: function (vl) {
                this._hasPopulatedIntegratedDecisionsAndIssues.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelDecisionMakingSearchBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelDecisionMakingSearchBase;
    })(Controllers.AbstractSearchPageModel);
    Controllers.ModelDecisionMakingSearchBase = ModelDecisionMakingSearchBase;
    var ModelDecisionMakingSearchPersistedModel = (function () {
        function ModelDecisionMakingSearchPersistedModel() {
        }
        return ModelDecisionMakingSearchPersistedModel;
    })();
    Controllers.ModelDecisionMakingSearchPersistedModel = ModelDecisionMakingSearchPersistedModel;
    var ControllerDecisionMakingSearchBase = (function (_super) {
        __extends(ControllerDecisionMakingSearchBase, _super);
        function ControllerDecisionMakingSearchBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'Decision Making';
            var lastSearchPageModel = $scope.getPageModelByURL('/DecisionMakingSearch');
            var rowIndexToSelect = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.clasId = lastSearchPageModel.clasId;
                self.pageModel.ecgrId = lastSearchPageModel.ecgrId;
                self.pageModel.description = lastSearchPageModel.description;
                self.pageModel.dateTime = lastSearchPageModel.dateTime;
                self.pageModel.hasBeenRun = lastSearchPageModel.hasBeenRun;
                self.pageModel.hasPopulatedIntegratedDecisionsAndIssues = lastSearchPageModel.hasPopulatedIntegratedDecisionsAndIssues;
            }
            $scope._newIsDisabled =
                function () {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled =
                function () {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled =
                function (decisionMaking) {
                    return self._deleteIsDisabled(decisionMaking);
                };
            $scope._editBtnIsDisabled =
                function (decisionMaking) {
                    return self._editBtnIsDisabled(decisionMaking);
                };
            $scope._cancelIsDisabled =
                function () {
                    return self._cancelIsDisabled();
                };
            $scope.d__clasId_disabled =
                function () {
                    return self.d__clasId_disabled();
                };
            $scope.showLov_d__clasId =
                function () {
                    $timeout(function () {
                        self.showLov_d__clasId();
                    }, 0);
                };
            $scope.d__ecgrId_disabled =
                function () {
                    return self.d__ecgrId_disabled();
                };
            $scope.showLov_d__ecgrId =
                function () {
                    $timeout(function () {
                        self.showLov_d__ecgrId();
                    }, 0);
                };
            $scope.d__description_disabled =
                function () {
                    return self.d__description_disabled();
                };
            $scope.d__dateTime_disabled =
                function () {
                    return self.d__dateTime_disabled();
                };
            $scope.d__hasBeenRun_disabled =
                function () {
                    return self.d__hasBeenRun_disabled();
                };
            $scope.d__hasPopulatedIntegratedDecisionsAndIssues_disabled =
                function () {
                    return self.d__hasPopulatedIntegratedDecisionsAndIssues_disabled();
                };
            $scope.clearBtnAction = function () {
                self.pageModel.clasId = undefined;
                self.pageModel.ecgrId = undefined;
                self.pageModel.description = undefined;
                self.pageModel.dateTime = undefined;
                self.pageModel.hasBeenRun = undefined;
                self.pageModel.hasPopulatedIntegratedDecisionsAndIssues = undefined;
                self.updateGrid(0, false, true);
            };
            $scope.newBtnAction = function () {
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };
            $scope.onEdit = function (x) {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };
            $timeout(function () {
                $('#Search_DecisionMaking').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);
            self.updateUI(rowIndexToSelect);
        }
        ControllerDecisionMakingSearchBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerDecisionMakingSearchBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerDecisionMakingSearch";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerDecisionMakingSearchBase.prototype, "HtmlDivId", {
            get: function () {
                return "Search_DecisionMaking";
            },
            enumerable: true,
            configurable: true
        });
        ControllerDecisionMakingSearchBase.prototype._getPageTitle = function () {
            return "Decision Making";
        };
        ControllerDecisionMakingSearchBase.prototype.getColumnTooltip = function (fieldName) {
            if (fieldName === 'hasBeenRun')
                return "If YES, the Decision Making process has been executed";
            if (fieldName === 'hasPopulatedIntegratedDecisionsAndIssues')
                return "If YES, the Decision Making has populated the Integrated Decisions and Isues";
            return "";
        };
        ControllerDecisionMakingSearchBase.prototype.gridColumnFilter = function (field) {
            return true;
        };
        ControllerDecisionMakingSearchBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            return [
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                { cellClass: 'cellToolTip', field: 'description', displayName: 'getALString("Description", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '28%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.description' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'dateTime', displayName: 'getALString("Date", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_dateTime' data-ng-model='row.entity.dateTime' data-np-ui-model='row.entity._dateTime' data-ng-change='markEntityAsUpdated(row.entity,&quot;dateTime&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'clasId.name', displayName: 'getALString("Data Import Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.clasId.name' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'ecgrId.name', displayName: 'getALString("BRE Rules Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.ecgrId.name' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'hasBeenRun', displayName: 'getALString("Has Been Run", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '6%', headerCellTemplate: this.getGridHeaderCellTemplate({ tooltip: "{{getColumnTooltip('hasBeenRun')}}" }), cellTemplate: "<div data-ng-class='col.colIndex()' class='ngCellCheckBox' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_hasBeenRun' data-ng-model='row.entity.hasBeenRun' data-np-ui-model='row.entity._hasBeenRun' data-ng-change='markEntityAsUpdated(row.entity,&quot;hasBeenRun&quot;)' type='checkbox' data-np-checkbox='dummy' data-ng-click='setSelectedRow(row.rowIndex)' data-ng-disabled='true' class='ngCellCheckBox' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'hasPopulatedIntegratedDecisionsAndIssues', displayName: 'getALString("is Used", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '6%', headerCellTemplate: this.getGridHeaderCellTemplate({ tooltip: "{{getColumnTooltip('hasPopulatedIntegratedDecisionsAndIssues')}}" }), cellTemplate: "<div data-ng-class='col.colIndex()' class='ngCellCheckBox' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_hasPopulatedIntegratedDecisionsAndIssues' data-ng-model='row.entity.hasPopulatedIntegratedDecisionsAndIssues' data-np-ui-model='row.entity._hasPopulatedIntegratedDecisionsAndIssues' data-ng-change='markEntityAsUpdated(row.entity,&quot;hasPopulatedIntegratedDecisionsAndIssues&quot;)' type='checkbox' data-np-checkbox='dummy' data-ng-click='setSelectedRow(row.rowIndex)' data-ng-disabled='true' class='ngCellCheckBox' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
        };
        ControllerDecisionMakingSearchBase.prototype.getPersistedModel = function () {
            var self = this;
            var ret = new ModelDecisionMakingSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.clasId = self.pageModel.clasId;
            ret.ecgrId = self.pageModel.ecgrId;
            ret.description = self.pageModel.description;
            ret.dateTime = self.pageModel.dateTime;
            ret.hasBeenRun = self.pageModel.hasBeenRun;
            ret.hasPopulatedIntegratedDecisionsAndIssues = self.pageModel.hasPopulatedIntegratedDecisionsAndIssues;
            return ret;
        };
        Object.defineProperty(ControllerDecisionMakingSearchBase.prototype, "pageModel", {
            get: function () {
                return this.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerDecisionMakingSearchBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.DecisionMaking.fromJSONComplete(webResponse.data);
        };
        ControllerDecisionMakingSearchBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "DecisionMaking/findLazyDecisionMaking";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.description)) {
                paramData['description'] = self.pageModel.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.ecgrId) && !isVoid(self.pageModel.ecgrId.ecgrId)) {
                paramData['ecgrId_ecgrId'] = self.pageModel.ecgrId.ecgrId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasBeenRun)) {
                paramData['hasBeenRun'] = self.pageModel.hasBeenRun;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasPopulatedIntegratedDecisionsAndIssues)) {
                paramData['hasPopulatedIntegratedDecisionsAndIssues'] = self.pageModel.hasPopulatedIntegratedDecisionsAndIssues;
            }
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerDecisionMakingSearchBase.prototype.makeWebRequest_count = function (excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var wsPath = "DecisionMaking/findLazyDecisionMaking_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.description)) {
                paramData['description'] = self.pageModel.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.ecgrId) && !isVoid(self.pageModel.ecgrId.ecgrId)) {
                paramData['ecgrId_ecgrId'] = self.pageModel.ecgrId.ecgrId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasBeenRun)) {
                paramData['hasBeenRun'] = self.pageModel.hasBeenRun;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasPopulatedIntegratedDecisionsAndIssues)) {
                paramData['hasPopulatedIntegratedDecisionsAndIssues'] = self.pageModel.hasPopulatedIntegratedDecisionsAndIssues;
            }
            var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(function () {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        };
        ControllerDecisionMakingSearchBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
            if (excludedIds === void 0) { excludedIds = []; }
            var self = this;
            var paramData = {};
            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.description)) {
                paramData['description'] = self.pageModel.description;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dateTime)) {
                paramData['dateTime'] = self.pageModel.dateTime;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.clasId) && !isVoid(self.pageModel.clasId.clasId)) {
                paramData['clasId_clasId'] = self.pageModel.clasId.clasId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.ecgrId) && !isVoid(self.pageModel.ecgrId.ecgrId)) {
                paramData['ecgrId_ecgrId'] = self.pageModel.ecgrId.ecgrId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasBeenRun)) {
                paramData['hasBeenRun'] = self.pageModel.hasBeenRun;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.hasPopulatedIntegratedDecisionsAndIssues)) {
                paramData['hasPopulatedIntegratedDecisionsAndIssues'] = self.pageModel.hasPopulatedIntegratedDecisionsAndIssues;
            }
            var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
            return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        };
        Object.defineProperty(ControllerDecisionMakingSearchBase.prototype, "Current", {
            get: function () {
                var self = this;
                return self.$scope.pageModel.selectedEntities[0];
            },
            enumerable: true,
            configurable: true
        });
        ControllerDecisionMakingSearchBase.prototype.onNew = function () {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/DecisionMaking', self._getPageTitle(), {});
        };
        ControllerDecisionMakingSearchBase.prototype.onEdit = function (x) {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel = { "selectedEntity": x };
            self.$scope.navigateForward('/DecisionMaking', self._getPageTitle(), visitedPageModel);
        };
        ControllerDecisionMakingSearchBase.prototype.getSynchronizeChangesWithDbUrl = function () {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_DecisionMaking";
        };
        ControllerDecisionMakingSearchBase.prototype.getSynchronizeChangesWithDbData = function (x) {
            var self = this;
            var paramData = {
                data: []
            };
            var changeToCommit = new Controllers.ChangeToCommit(Controllers.ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);
            paramData.data.push(changeToCommit);
            return paramData;
        };
        ControllerDecisionMakingSearchBase.prototype.deleteRecord = function (x) {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
            console.log("deleting DecisionMaking with PK field:" + x.demaId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                self.updateGrid();
            }).error(function (data, status, header, config) {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        };
        ControllerDecisionMakingSearchBase.prototype._newIsDisabled = function () {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerDecisionMakingSearchBase.prototype._searchIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerDecisionMakingSearchBase.prototype._deleteIsDisabled = function (decisionMaking) {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_D"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = self.isFieldsDisabled(decisionMaking);
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerDecisionMakingSearchBase.prototype.isFieldsDisabled = function (decisionMaking) {
            console.warn("Unimplemented function isFieldsDisabled()");
            return false;
        };
        ControllerDecisionMakingSearchBase.prototype._editBtnIsDisabled = function (decisionMaking) {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_R"))
                return true; // 
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerDecisionMakingSearchBase.prototype._cancelIsDisabled = function () {
            var self = this;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;
        };
        ControllerDecisionMakingSearchBase.prototype.d__clasId_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerDecisionMakingSearchBase.prototype.showLov_d__clasId = function () {
            var self = this;
            var uimodel = self.pageModel._clasId;
            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Data Import Name';
            dialogOptions.previousModel = self.cachedLovModel_d__clasId;
            dialogOptions.className = "ControllerLovClassification";
            dialogOptions.onSelect = function (selectedEntity) {
                var classification1 = selectedEntity;
                uimodel.clearAllErrors();
                if (false && isVoid(classification1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                if (!isVoid(classification1) && classification1.isEqual(self.pageModel.clasId))
                    return;
                self.pageModel.clasId = classification1;
            };
            dialogOptions.openNewEntityDialog = function () {
            };
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                self.cachedLovModel_d__clasId = lovModel;
                var wsPath = "Classification/findAllByCriteriaRange_forLov";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                    paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                    paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                    paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var wsPath = "Classification/findAllByCriteriaRange_forLov_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                    paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                    paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                    paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                var paramData = {};
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                    paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                    paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                    paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;
            dialogOptions.shownCols['dateTime'] = true;
            self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Classification.html?rev=' + self.$scope.globals.version, dialogOptions);
        };
        ControllerDecisionMakingSearchBase.prototype.d__ecgrId_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerDecisionMakingSearchBase.prototype.showLov_d__ecgrId = function () {
            var self = this;
            var uimodel = self.pageModel._ecgrId;
            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'BRE Rules';
            dialogOptions.previousModel = self.cachedLovModel_d__ecgrId;
            dialogOptions.className = "ControllerLovEcGroup";
            dialogOptions.onSelect = function (selectedEntity) {
                var ecGroup1 = selectedEntity;
                uimodel.clearAllErrors();
                if (false && isVoid(ecGroup1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout(function () {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                if (!isVoid(ecGroup1) && ecGroup1.isEqual(self.pageModel.ecgrId))
                    return;
                self.pageModel.ecgrId = ecGroup1;
            };
            dialogOptions.openNewEntityDialog = function () {
            };
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                self.cachedLovModel_d__ecgrId = lovModel;
                var wsPath = "EcGroup/findAllByCriteriaRange_forLov";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                    paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                    paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var wsPath = "EcGroup/findAllByCriteriaRange_forLov_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = lovModel;
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                    paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                    paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                var paramData = {};
                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                    paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                }
                if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                    paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;
            self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/EcGroup.html?rev=' + self.$scope.globals.version, dialogOptions);
        };
        ControllerDecisionMakingSearchBase.prototype.d__description_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerDecisionMakingSearchBase.prototype.d__dateTime_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerDecisionMakingSearchBase.prototype.d__hasBeenRun_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        ControllerDecisionMakingSearchBase.prototype.d__hasPopulatedIntegratedDecisionsAndIssues_disabled = function () {
            var self = this;
            var entityIsDisabled = false;
            var isContainerControlDisabled = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
        };
        return ControllerDecisionMakingSearchBase;
    })(Controllers.AbstractSearchPageController);
    Controllers.ControllerDecisionMakingSearchBase = ControllerDecisionMakingSearchBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DecisionMakingSearchBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
