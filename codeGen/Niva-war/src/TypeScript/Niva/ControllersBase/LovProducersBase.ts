/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ProducersBase.ts" />
/// <reference path="../Controllers/LovProducers.ts" />

module Controllers {
    export class ModelLovProducersBase extends AbstractLovModel {
        _fsch_ProducersLov_prodName:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_ProducersLov_prodName():string {
            return this._fsch_ProducersLov_prodName.value;
        }
        public set fsch_ProducersLov_prodName(vl:string) {
            this._fsch_ProducersLov_prodName.value = vl;
        }
        _fsch_ProducersLov_prodCode:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_ProducersLov_prodCode():number {
            return this._fsch_ProducersLov_prodCode.value;
        }
        public set fsch_ProducersLov_prodCode(vl:number) {
            this._fsch_ProducersLov_prodCode.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        controller: ControllerLovProducers;
        constructor(public $scope: IScopeLovProducers) { super($scope); }
    }


    export interface IScopeLovProducersBase extends IAbstractLovScope {
        globals: Globals;
        modelLovProducers : ModelLovProducers;
        _disabled():boolean; 
        _invisible():boolean; 
        LovProducers_fsch_ProducersLov_prodName_disabled():boolean; 
        LovProducers_fsch_ProducersLov_prodCode_disabled():boolean; 
    }

    export class ControllerLovProducersBase extends LovController {
        constructor(
            public $scope: IScopeLovProducers,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:ModelLovProducers)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 5, maxLinesInHeader: 1})
            var self: ControllerLovProducersBase = this;
            model.controller = <ControllerLovProducers>self;
            $scope.clearBtnAction = () => { 
                self.model.fsch_ProducersLov_prodName = undefined;
                self.model.fsch_ProducersLov_prodCode = undefined;
                self.updateGrid();
            };
            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.LovProducers_fsch_ProducersLov_prodName_disabled = 
                () => {
                    return self.LovProducers_fsch_ProducersLov_prodName_disabled();
                };
            $scope.LovProducers_fsch_ProducersLov_prodCode_disabled = 
                () => {
                    return self.LovProducers_fsch_ProducersLov_prodCode_disabled();
                };
            $scope.modelLovProducers = model;
            self.updateUI();
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerLovProducers";
        }
        public get HtmlDivId(): string {
            return "Producers_Id";
        }
        
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions =   <NpTypes.LovDialogOptions>self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName); 
            }
            var ret = [
                { cellClass:'cellToolTip', field:'prodName', displayName:'getALString("Full Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'76%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.prodName' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'prodCode', displayName:'getALString("Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.prodCode' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter((cl => cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true));
            if (self.isMultiSelect()) {
                ret.splice(0,0,{ width:'22', cellTemplate:"<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined});
            }
            return ret;
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            return Entities.Producers.fromJSONComplete(webResponse.data);
        }

        public get modelLovProducers():ModelLovProducers {
            var self = this;
            return self.model;
        }

        public initializeModelWithPreviousValues() {
            var self = this;
            var prevModel:ModelLovProducers = <ModelLovProducers>self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovProducers.fsch_ProducersLov_prodName = prevModel.fsch_ProducersLov_prodName
                self.modelLovProducers.fsch_ProducersLov_prodCode = prevModel.fsch_ProducersLov_prodCode
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public LovProducers_fsch_ProducersLov_prodName_disabled():boolean {
            var self = this;


            return false;
        }
        public LovProducers_fsch_ProducersLov_prodCode_disabled():boolean {
            var self = this;


            return false;
        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
