/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/FmisDecision.ts" />
/// <reference path="../Entities/GpDecision.ts" />
/// <reference path="../Entities/GpRequestsContexts.ts" />
/// <reference path="../Entities/FmisUpload.ts" />
/// <reference path="../Entities/ParcelsIssuesStatusPerDema.ts" />
/// <reference path="../Entities/ParcelsIssuesActivities.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />

module Entities {

    export class ParcelsIssuesBase extends NpTypes.BaseEntity {
        constructor(
            parcelsIssuesId: string,
            dteCreated: Date,
            status: number,
            dteStatusUpdate: Date,
            rowVersion: number,
            typeOfIssue: number,
            active: boolean,
            pclaId: ParcelClas) 
        {
            super();
            var self = this;
            this._parcelsIssuesId = new NpTypes.UIStringModel(parcelsIssuesId, this);
            this._dteCreated = new NpTypes.UIDateModel(dteCreated, this);
            this._status = new NpTypes.UINumberModel(status, this);
            this._dteStatusUpdate = new NpTypes.UIDateModel(dteStatusUpdate, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._typeOfIssue = new NpTypes.UINumberModel(typeOfIssue, this);
            this._active = new NpTypes.UIBoolModel(active, this);
            this._pclaId = new NpTypes.UIManyToOneModel<Entities.ParcelClas>(pclaId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.parcelsIssuesId; 
        }
        
        public getKeyName(): string { 
            return "parcelsIssuesId"; 
        }

        public getEntityName(): string { 
            return 'ParcelsIssues'; 
        }
        
        public fromJSON(data: any): ParcelsIssues[] { 
            return ParcelsIssues.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.parcelsIssuesId === null || this.parcelsIssuesId === undefined)
                return true;
            if (this.parcelsIssuesId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: ParcelsIssuesBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //parcelsIssuesId property
        _parcelsIssuesId: NpTypes.UIStringModel;
        public get parcelsIssuesId():string {
            return this._parcelsIssuesId.value;
        }
        public set parcelsIssuesId(parcelsIssuesId:string) {
            var self = this;
            self._parcelsIssuesId.value = parcelsIssuesId;
        //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
        }
        //public parcelsIssuesId_listeners: Array<(c:ParcelsIssues) => void> = [];
        //dteCreated property
        _dteCreated: NpTypes.UIDateModel;
        public get dteCreated():Date {
            return this._dteCreated.value;
        }
        public set dteCreated(dteCreated:Date) {
            var self = this;
            self._dteCreated.value = dteCreated;
        //    self.dteCreated_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
        }
        //public dteCreated_listeners: Array<(c:ParcelsIssues) => void> = [];
        //status property
        _status: NpTypes.UINumberModel;
        public get status():number {
            return this._status.value;
        }
        public set status(status:number) {
            var self = this;
            self._status.value = status;
        //    self.status_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
        }
        //public status_listeners: Array<(c:ParcelsIssues) => void> = [];
        //dteStatusUpdate property
        _dteStatusUpdate: NpTypes.UIDateModel;
        public get dteStatusUpdate():Date {
            return this._dteStatusUpdate.value;
        }
        public set dteStatusUpdate(dteStatusUpdate:Date) {
            var self = this;
            self._dteStatusUpdate.value = dteStatusUpdate;
        //    self.dteStatusUpdate_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
        }
        //public dteStatusUpdate_listeners: Array<(c:ParcelsIssues) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
        }
        //public rowVersion_listeners: Array<(c:ParcelsIssues) => void> = [];
        //typeOfIssue property
        _typeOfIssue: NpTypes.UINumberModel;
        public get typeOfIssue():number {
            return this._typeOfIssue.value;
        }
        public set typeOfIssue(typeOfIssue:number) {
            var self = this;
            self._typeOfIssue.value = typeOfIssue;
        //    self.typeOfIssue_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
        }
        //public typeOfIssue_listeners: Array<(c:ParcelsIssues) => void> = [];
        //active property
        _active: NpTypes.UIBoolModel;
        public get active():boolean {
            return this._active.value;
        }
        public set active(active:boolean) {
            var self = this;
            self._active.value = active;
        //    self.active_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
        }
        //public active_listeners: Array<(c:ParcelsIssues) => void> = [];
        //pclaId property
        _pclaId: NpTypes.UIManyToOneModel<Entities.ParcelClas>;
        public get pclaId():ParcelClas {
            return this._pclaId.value;
        }
        public set pclaId(pclaId:ParcelClas) {
            var self = this;
            self._pclaId.value = pclaId;
        //    self.pclaId_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
        }
        //public pclaId_listeners: Array<(c:ParcelsIssues) => void> = [];
        public static fmisDecisionCollection: (self:Entities.ParcelsIssues, func: (fmisDecisionList: Entities.FmisDecision[]) => void) => void = null; //(parcelsIssues, list) => { };
        public fmisDecisionCollection(func: (fmisDecisionList: Entities.FmisDecision[]) => void) {
            var self: Entities.ParcelsIssuesBase = this;
        	if (!isVoid(ParcelsIssues.fmisDecisionCollection)) {
        		if (self instanceof Entities.ParcelsIssues) {
        			ParcelsIssues.fmisDecisionCollection(<Entities.ParcelsIssues>self, func);
                }
            }
        }
        public static gpDecisionCollection: (self:Entities.ParcelsIssues, func: (gpDecisionList: Entities.GpDecision[]) => void) => void = null; //(parcelsIssues, list) => { };
        public gpDecisionCollection(func: (gpDecisionList: Entities.GpDecision[]) => void) {
            var self: Entities.ParcelsIssuesBase = this;
        	if (!isVoid(ParcelsIssues.gpDecisionCollection)) {
        		if (self instanceof Entities.ParcelsIssues) {
        			ParcelsIssues.gpDecisionCollection(<Entities.ParcelsIssues>self, func);
                }
            }
        }
        public static gpRequestsContextsCollection: (self:Entities.ParcelsIssues, func: (gpRequestsContextsList: Entities.GpRequestsContexts[]) => void) => void = null; //(parcelsIssues, list) => { };
        public gpRequestsContextsCollection(func: (gpRequestsContextsList: Entities.GpRequestsContexts[]) => void) {
            var self: Entities.ParcelsIssuesBase = this;
        	if (!isVoid(ParcelsIssues.gpRequestsContextsCollection)) {
        		if (self instanceof Entities.ParcelsIssues) {
        			ParcelsIssues.gpRequestsContextsCollection(<Entities.ParcelsIssues>self, func);
                }
            }
        }
        public static fmisUploadCollection: (self:Entities.ParcelsIssues, func: (fmisUploadList: Entities.FmisUpload[]) => void) => void = null; //(parcelsIssues, list) => { };
        public fmisUploadCollection(func: (fmisUploadList: Entities.FmisUpload[]) => void) {
            var self: Entities.ParcelsIssuesBase = this;
        	if (!isVoid(ParcelsIssues.fmisUploadCollection)) {
        		if (self instanceof Entities.ParcelsIssues) {
        			ParcelsIssues.fmisUploadCollection(<Entities.ParcelsIssues>self, func);
                }
            }
        }
        public static parcelsIssuesStatusPerDemaCollection: (self:Entities.ParcelsIssues, func: (parcelsIssuesStatusPerDemaList: Entities.ParcelsIssuesStatusPerDema[]) => void) => void = null; //(parcelsIssues, list) => { };
        public parcelsIssuesStatusPerDemaCollection(func: (parcelsIssuesStatusPerDemaList: Entities.ParcelsIssuesStatusPerDema[]) => void) {
            var self: Entities.ParcelsIssuesBase = this;
        	if (!isVoid(ParcelsIssues.parcelsIssuesStatusPerDemaCollection)) {
        		if (self instanceof Entities.ParcelsIssues) {
        			ParcelsIssues.parcelsIssuesStatusPerDemaCollection(<Entities.ParcelsIssues>self, func);
                }
            }
        }
        public static parcelsIssuesActivitiesCollection: (self:Entities.ParcelsIssues, func: (parcelsIssuesActivitiesList: Entities.ParcelsIssuesActivities[]) => void) => void = null; //(parcelsIssues, list) => { };
        public parcelsIssuesActivitiesCollection(func: (parcelsIssuesActivitiesList: Entities.ParcelsIssuesActivities[]) => void) {
            var self: Entities.ParcelsIssuesBase = this;
        	if (!isVoid(ParcelsIssues.parcelsIssuesActivitiesCollection)) {
        		if (self instanceof Entities.ParcelsIssues) {
        			ParcelsIssues.parcelsIssuesActivitiesCollection(<Entities.ParcelsIssues>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.parcelsIssuesId_listeners.splice(0);
            self.dteCreated_listeners.splice(0);
            self.status_listeners.splice(0);
            self.dteStatusUpdate_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.typeOfIssue_listeners.splice(0);
            self.active_listeners.splice(0);
            self.pclaId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : ParcelsIssues {
            var key = "ParcelsIssues:"+x.parcelsIssuesId;
            var ret =  new ParcelsIssues(
                x.parcelsIssuesId,
                isVoid(x.dteCreated) ? null : new Date(x.dteCreated),
                x.status,
                isVoid(x.dteStatusUpdate) ? null : new Date(x.dteStatusUpdate),
                x.rowVersion,
                x.typeOfIssue,
                x.active,
                x.pclaId
            );
            deserializedEntities[key] = ret;
            ret.pclaId = (x.pclaId !== undefined && x.pclaId !== null) ? <ParcelClas>NpTypes.BaseEntity.entitiesFactory[x.pclaId.$entityName].fromJSONPartial(x.pclaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): ParcelsIssues {
            
            return <ParcelsIssues>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : ParcelsIssues {
            var self = this;
            var key="";
            var ret:ParcelsIssues = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcelsIssues:"+x.$refId;
                ret = <ParcelsIssues>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcelsIssuesBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcelsIssues:"+x.parcelsIssuesId;
                    var cachedCopy = <ParcelsIssues>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcelsIssues.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcelsIssues.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = ParcelsIssues.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelsIssues> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<ParcelsIssues> = _.map(data, (x:any):ParcelsIssues => {
                return ParcelsIssues.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:ParcelsIssues[])=>void)  {
            var url = "/Niva/rest/ParcelsIssues/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = ParcelsIssues.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.parcelsIssuesId = this.parcelsIssuesId;
                ret.dteCreated = this.dteCreated;
                ret.status = this.status;
                ret.dteStatusUpdate = this.dteStatusUpdate;
                ret.rowVersion = this.rowVersion;
                ret.typeOfIssue = this.typeOfIssue;
                ret.active = this.active;
                ret.pclaId = 
                    (this.pclaId !== undefined && this.pclaId !== null) ? 
                        { pclaId :  this.pclaId.pclaId} :
                        (this.pclaId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:ParcelsIssues):void {
            var self = this;
            self.parcelsIssuesId = other.parcelsIssuesId
            self.dteCreated = other.dteCreated
            self.status = other.status
            self.dteStatusUpdate = other.dteStatusUpdate
            self.rowVersion = other.rowVersion
            self.typeOfIssue = other.typeOfIssue
            self.active = other.active
            self.pclaId = other.pclaId
        }

        public static Create() : ParcelsIssues {
            return new ParcelsIssues(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(parcelsIssuesId:string) : ParcelsIssues {
            var ret = ParcelsIssues.Create();
            ret.parcelsIssuesId = parcelsIssuesId;
            return ret;
        }

        public static cashedEntities: { [id: string]: ParcelsIssues; } = {};
        public static findById_unecrypted(parcelsIssuesId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : ParcelsIssues {
            if (Entities.ParcelsIssues.cashedEntities[parcelsIssuesId.toString()] !== undefined)
                return Entities.ParcelsIssues.cashedEntities[parcelsIssuesId.toString()];

            var wsPath = "ParcelsIssues/findByParcelsIssuesId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['parcelsIssuesId'] = parcelsIssuesId;
            var ret = ParcelsIssues.Create();
            Entities.ParcelsIssues.cashedEntities[parcelsIssuesId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.ParcelsIssues.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + parcelsIssuesId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['ParcelsIssues'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.ParcelsIssues.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.ParcelsIssues.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
