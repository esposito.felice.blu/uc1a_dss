/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/TemplateColumn.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/FileTemplate.ts" />

module Entities {

    export class FileTemplateBase extends NpTypes.BaseEntity {
        constructor(
            fiteId: string,
            name: string,
            rowVersion: number) 
        {
            super();
            var self = this;
            this._fiteId = new NpTypes.UIStringModel(fiteId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.fiteId; 
        }
        
        public getKeyName(): string { 
            return "fiteId"; 
        }

        public getEntityName(): string { 
            return 'FileTemplate'; 
        }
        
        public fromJSON(data: any): FileTemplate[] { 
            return FileTemplate.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.fiteId === null || this.fiteId === undefined)
                return true;
            if (this.fiteId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: FileTemplateBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //fiteId property
        _fiteId: NpTypes.UIStringModel;
        public get fiteId():string {
            return this._fiteId.value;
        }
        public set fiteId(fiteId:string) {
            var self = this;
            self._fiteId.value = fiteId;
        //    self.fiteId_listeners.forEach(cb => { cb(<FileTemplate>self); });
        }
        //public fiteId_listeners: Array<(c:FileTemplate) => void> = [];
        //name property
        _name: NpTypes.UIStringModel;
        public get name():string {
            return this._name.value;
        }
        public set name(name:string) {
            var self = this;
            self._name.value = name;
        //    self.name_listeners.forEach(cb => { cb(<FileTemplate>self); });
        }
        //public name_listeners: Array<(c:FileTemplate) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<FileTemplate>self); });
        }
        //public rowVersion_listeners: Array<(c:FileTemplate) => void> = [];
        public static templateColumnCollection: (self:Entities.FileTemplate, func: (templateColumnList: Entities.TemplateColumn[]) => void) => void = null; //(fileTemplate, list) => { };
        public templateColumnCollection(func: (templateColumnList: Entities.TemplateColumn[]) => void) {
            var self: Entities.FileTemplateBase = this;
        	if (!isVoid(FileTemplate.templateColumnCollection)) {
        		if (self instanceof Entities.FileTemplate) {
        			FileTemplate.templateColumnCollection(<Entities.FileTemplate>self, func);
                }
            }
        }
        public static classificationCollection: (self:Entities.FileTemplate, func: (classificationList: Entities.Classification[]) => void) => void = null; //(fileTemplate, list) => { };
        public classificationCollection(func: (classificationList: Entities.Classification[]) => void) {
            var self: Entities.FileTemplateBase = this;
        	if (!isVoid(FileTemplate.classificationCollection)) {
        		if (self instanceof Entities.FileTemplate) {
        			FileTemplate.classificationCollection(<Entities.FileTemplate>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.fiteId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : FileTemplate {
            var key = "FileTemplate:"+x.fiteId;
            var ret =  new FileTemplate(
                x.fiteId,
                x.name,
                x.rowVersion
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): FileTemplate {
            
            return <FileTemplate>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : FileTemplate {
            var self = this;
            var key="";
            var ret:FileTemplate = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "FileTemplate:"+x.$refId;
                ret = <FileTemplate>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "FileTemplateBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "FileTemplate:"+x.fiteId;
                    var cachedCopy = <FileTemplate>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = FileTemplate.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = FileTemplate.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = FileTemplate.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FileTemplate> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<FileTemplate> = _.map(data, (x:any):FileTemplate => {
                return FileTemplate.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:FileTemplate[])=>void)  {
            var url = "/Niva/rest/FileTemplate/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = FileTemplate.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.fiteId = this.fiteId;
                ret.name = this.name;
                ret.rowVersion = this.rowVersion;
            return ret;
        }

        public updateInstance(other:FileTemplate):void {
            var self = this;
            self.fiteId = other.fiteId
            self.name = other.name
            self.rowVersion = other.rowVersion
        }

        public static Create() : FileTemplate {
            return new FileTemplate(
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(fiteId:string) : FileTemplate {
            var ret = FileTemplate.Create();
            ret.fiteId = fiteId;
            return ret;
        }

        public static cashedEntities: { [id: string]: FileTemplate; } = {};
        public static findById_unecrypted(fiteId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : FileTemplate {
            if (Entities.FileTemplate.cashedEntities[fiteId.toString()] !== undefined)
                return Entities.FileTemplate.cashedEntities[fiteId.toString()];

            var wsPath = "FileTemplate/findByFiteId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fiteId'] = fiteId;
            var ret = FileTemplate.Create();
            Entities.FileTemplate.cashedEntities[fiteId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.FileTemplate.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + fiteId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['FileTemplate'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.FileTemplate.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.FileTemplate.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
