/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/ExcelError.ts" />
/// <reference path="../Entities/ExcelFile.ts" />

module Entities {

    export class ExcelFileBase extends NpTypes.BaseEntity {
        constructor(
            id: string,
            filename: string,
            data: string,
            totalRows: number,
            totalErrorRows: number,
            rowVersion: number) 
        {
            super();
            var self = this;
            this._id = new NpTypes.UIStringModel(id, this);
            this._filename = new NpTypes.UIStringModel(filename, this);
            this._data = new NpTypes.UIBlobModel(
                data, 
                ()  => '...',
                (fileName:string)   => {},
                this);
            this._totalRows = new NpTypes.UINumberModel(totalRows, this);
            this._totalErrorRows = new NpTypes.UINumberModel(totalErrorRows, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.id; 
        }
        
        public getKeyName(): string { 
            return "id"; 
        }

        public getEntityName(): string { 
            return 'ExcelFile'; 
        }
        
        public fromJSON(data: any): ExcelFile[] { 
            return ExcelFile.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.id === null || this.id === undefined)
                return true;
            if (this.id.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: ExcelFileBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //id property
        _id: NpTypes.UIStringModel;
        public get id():string {
            return this._id.value;
        }
        public set id(id:string) {
            var self = this;
            self._id.value = id;
        //    self.id_listeners.forEach(cb => { cb(<ExcelFile>self); });
        }
        //public id_listeners: Array<(c:ExcelFile) => void> = [];
        //filename property
        _filename: NpTypes.UIStringModel;
        public get filename():string {
            return this._filename.value;
        }
        public set filename(filename:string) {
            var self = this;
            self._filename.value = filename;
        //    self.filename_listeners.forEach(cb => { cb(<ExcelFile>self); });
        }
        //public filename_listeners: Array<(c:ExcelFile) => void> = [];
        //data property
        _data: NpTypes.UIBlobModel;
        public get data():string {
            return this._data.value;
        }
        public set data(data:string) {
            var self = this;
            self._data.value = data;
        //    self.data_listeners.forEach(cb => { cb(<ExcelFile>self); });
        }
        //public data_listeners: Array<(c:ExcelFile) => void> = [];
        //totalRows property
        _totalRows: NpTypes.UINumberModel;
        public get totalRows():number {
            return this._totalRows.value;
        }
        public set totalRows(totalRows:number) {
            var self = this;
            self._totalRows.value = totalRows;
        //    self.totalRows_listeners.forEach(cb => { cb(<ExcelFile>self); });
        }
        //public totalRows_listeners: Array<(c:ExcelFile) => void> = [];
        //totalErrorRows property
        _totalErrorRows: NpTypes.UINumberModel;
        public get totalErrorRows():number {
            return this._totalErrorRows.value;
        }
        public set totalErrorRows(totalErrorRows:number) {
            var self = this;
            self._totalErrorRows.value = totalErrorRows;
        //    self.totalErrorRows_listeners.forEach(cb => { cb(<ExcelFile>self); });
        }
        //public totalErrorRows_listeners: Array<(c:ExcelFile) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<ExcelFile>self); });
        }
        //public rowVersion_listeners: Array<(c:ExcelFile) => void> = [];
        public static coverTypeCollection: (self:Entities.ExcelFile, func: (coverTypeList: Entities.CoverType[]) => void) => void = null; //(excelFile, list) => { };
        public coverTypeCollection(func: (coverTypeList: Entities.CoverType[]) => void) {
            var self: Entities.ExcelFileBase = this;
        	if (!isVoid(ExcelFile.coverTypeCollection)) {
        		if (self instanceof Entities.ExcelFile) {
        			ExcelFile.coverTypeCollection(<Entities.ExcelFile>self, func);
                }
            }
        }
        public static cultivationCollection: (self:Entities.ExcelFile, func: (cultivationList: Entities.Cultivation[]) => void) => void = null; //(excelFile, list) => { };
        public cultivationCollection(func: (cultivationList: Entities.Cultivation[]) => void) {
            var self: Entities.ExcelFileBase = this;
        	if (!isVoid(ExcelFile.cultivationCollection)) {
        		if (self instanceof Entities.ExcelFile) {
        			ExcelFile.cultivationCollection(<Entities.ExcelFile>self, func);
                }
            }
        }
        public static excelErrorCollection: (self:Entities.ExcelFile, func: (excelErrorList: Entities.ExcelError[]) => void) => void = null; //(excelFile, list) => { };
        public excelErrorCollection(func: (excelErrorList: Entities.ExcelError[]) => void) {
            var self: Entities.ExcelFileBase = this;
        	if (!isVoid(ExcelFile.excelErrorCollection)) {
        		if (self instanceof Entities.ExcelFile) {
        			ExcelFile.excelErrorCollection(<Entities.ExcelFile>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.id_listeners.splice(0);
            self.filename_listeners.splice(0);
            self.data_listeners.splice(0);
            self.totalRows_listeners.splice(0);
            self.totalErrorRows_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : ExcelFile {
            var key = "ExcelFile:"+x.id;
            var ret =  new ExcelFile(
                x.id,
                x.filename,
                x.data,
                x.totalRows,
                x.totalErrorRows,
                x.rowVersion
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): ExcelFile {
            
            return <ExcelFile>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : ExcelFile {
            var self = this;
            var key="";
            var ret:ExcelFile = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ExcelFile:"+x.$refId;
                ret = <ExcelFile>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ExcelFileBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ExcelFile:"+x.id;
                    var cachedCopy = <ExcelFile>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ExcelFile.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ExcelFile.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = ExcelFile.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ExcelFile> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<ExcelFile> = _.map(data, (x:any):ExcelFile => {
                return ExcelFile.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:ExcelFile[])=>void)  {
            var url = "/Niva/rest/ExcelFile/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = ExcelFile.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.id = this.id;
                ret.filename = this.filename;
                ret.data = this.data;
                ret.totalRows = this.totalRows;
                ret.totalErrorRows = this.totalErrorRows;
                ret.rowVersion = this.rowVersion;
            return ret;
        }

        public updateInstance(other:ExcelFile):void {
            var self = this;
            self.id = other.id
            self.filename = other.filename
            self.data = other.data
            self.totalRows = other.totalRows
            self.totalErrorRows = other.totalErrorRows
            self.rowVersion = other.rowVersion
        }

        public static Create() : ExcelFile {
            return new ExcelFile(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(id:string) : ExcelFile {
            var ret = ExcelFile.Create();
            ret.id = id;
            return ret;
        }

        public static cashedEntities: { [id: string]: ExcelFile; } = {};
        public static findById_unecrypted(id:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : ExcelFile {
            if (Entities.ExcelFile.cashedEntities[id.toString()] !== undefined)
                return Entities.ExcelFile.cashedEntities[id.toString()];

            var wsPath = "ExcelFile/findById_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['id'] = id;
            var ret = ExcelFile.Create();
            Entities.ExcelFile.cashedEntities[id.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.ExcelFile.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + id + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['ExcelFile'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.ExcelFile.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.ExcelFile.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
