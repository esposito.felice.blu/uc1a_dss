/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/GpRequestsProducers.ts" />
/// <reference path="../Entities/AgrisnapUsersProducers.ts" />
/// <reference path="../Entities/Producers.ts" />

module Entities {

    export class ProducersBase extends NpTypes.BaseEntity {
        constructor(
            prodName: string,
            rowVersion: number,
            prodCode: number,
            producersId: string) 
        {
            super();
            var self = this;
            this._prodName = new NpTypes.UIStringModel(prodName, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._prodCode = new NpTypes.UINumberModel(prodCode, this);
            this._producersId = new NpTypes.UIStringModel(producersId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.producersId; 
        }
        
        public getKeyName(): string { 
            return "producersId"; 
        }

        public getEntityName(): string { 
            return 'Producers'; 
        }
        
        public fromJSON(data: any): Producers[] { 
            return Producers.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.producersId === null || this.producersId === undefined)
                return true;
            if (this.producersId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: ProducersBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //prodName property
        _prodName: NpTypes.UIStringModel;
        public get prodName():string {
            return this._prodName.value;
        }
        public set prodName(prodName:string) {
            var self = this;
            self._prodName.value = prodName;
        //    self.prodName_listeners.forEach(cb => { cb(<Producers>self); });
        }
        //public prodName_listeners: Array<(c:Producers) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<Producers>self); });
        }
        //public rowVersion_listeners: Array<(c:Producers) => void> = [];
        //prodCode property
        _prodCode: NpTypes.UINumberModel;
        public get prodCode():number {
            return this._prodCode.value;
        }
        public set prodCode(prodCode:number) {
            var self = this;
            self._prodCode.value = prodCode;
        //    self.prodCode_listeners.forEach(cb => { cb(<Producers>self); });
        }
        //public prodCode_listeners: Array<(c:Producers) => void> = [];
        //producersId property
        _producersId: NpTypes.UIStringModel;
        public get producersId():string {
            return this._producersId.value;
        }
        public set producersId(producersId:string) {
            var self = this;
            self._producersId.value = producersId;
        //    self.producersId_listeners.forEach(cb => { cb(<Producers>self); });
        }
        //public producersId_listeners: Array<(c:Producers) => void> = [];
        public static gpRequestsProducersCollection: (self:Entities.Producers, func: (gpRequestsProducersList: Entities.GpRequestsProducers[]) => void) => void = null; //(producers, list) => { };
        public gpRequestsProducersCollection(func: (gpRequestsProducersList: Entities.GpRequestsProducers[]) => void) {
            var self: Entities.ProducersBase = this;
        	if (!isVoid(Producers.gpRequestsProducersCollection)) {
        		if (self instanceof Entities.Producers) {
        			Producers.gpRequestsProducersCollection(<Entities.Producers>self, func);
                }
            }
        }
        public static agrisnapUsersProducersCollection: (self:Entities.Producers, func: (agrisnapUsersProducersList: Entities.AgrisnapUsersProducers[]) => void) => void = null; //(producers, list) => { };
        public agrisnapUsersProducersCollection(func: (agrisnapUsersProducersList: Entities.AgrisnapUsersProducers[]) => void) {
            var self: Entities.ProducersBase = this;
        	if (!isVoid(Producers.agrisnapUsersProducersCollection)) {
        		if (self instanceof Entities.Producers) {
        			Producers.agrisnapUsersProducersCollection(<Entities.Producers>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.prodName_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.prodCode_listeners.splice(0);
            self.producersId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : Producers {
            var key = "Producers:"+x.producersId;
            var ret =  new Producers(
                x.prodName,
                x.rowVersion,
                x.prodCode,
                x.producersId
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): Producers {
            
            return <Producers>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : Producers {
            var self = this;
            var key="";
            var ret:Producers = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Producers:"+x.$refId;
                ret = <Producers>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ProducersBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Producers:"+x.producersId;
                    var cachedCopy = <Producers>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Producers.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Producers.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Producers.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Producers> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<Producers> = _.map(data, (x:any):Producers => {
                return Producers.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:Producers[])=>void)  {
            var url = "/Niva/rest/Producers/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = Producers.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.prodName = this.prodName;
                ret.rowVersion = this.rowVersion;
                ret.prodCode = this.prodCode;
                ret.producersId = this.producersId;
            return ret;
        }

        public updateInstance(other:Producers):void {
            var self = this;
            self.prodName = other.prodName
            self.rowVersion = other.rowVersion
            self.prodCode = other.prodCode
            self.producersId = other.producersId
        }

        public static Create() : Producers {
            return new Producers(
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(producersId:string) : Producers {
            var ret = Producers.Create();
            ret.producersId = producersId;
            return ret;
        }

        public static cashedEntities: { [id: string]: Producers; } = {};
        public static findById_unecrypted(producersId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : Producers {
            if (Entities.Producers.cashedEntities[producersId.toString()] !== undefined)
                return Entities.Producers.cashedEntities[producersId.toString()];

            var wsPath = "Producers/findByProducersId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['producersId'] = producersId;
            var ret = Producers.Create();
            Entities.Producers.cashedEntities[producersId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.Producers.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + producersId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['Producers'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.Producers.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.Producers.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
