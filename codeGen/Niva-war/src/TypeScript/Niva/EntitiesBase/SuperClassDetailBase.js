/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/SuperClas.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/SuperClassDetail.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var SuperClassDetailBase = (function (_super) {
        __extends(SuperClassDetailBase, _super);
        function SuperClassDetailBase(sucdId, rowVersion, sucaId, cultId) {
            _super.call(this);
            var self = this;
            this._sucdId = new NpTypes.UIStringModel(sucdId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._sucaId = new NpTypes.UIManyToOneModel(sucaId, this);
            this._cultId = new NpTypes.UIManyToOneModel(cultId, this);
            self.postConstruct();
        }
        SuperClassDetailBase.prototype.getKey = function () {
            return this.sucdId;
        };
        SuperClassDetailBase.prototype.getKeyName = function () {
            return "sucdId";
        };
        SuperClassDetailBase.prototype.getEntityName = function () {
            return 'SuperClassDetail';
        };
        SuperClassDetailBase.prototype.fromJSON = function (data) {
            return Entities.SuperClassDetail.fromJSONComplete(data);
        };
        Object.defineProperty(SuperClassDetailBase.prototype, "sucdId", {
            get: function () {
                return this._sucdId.value;
            },
            set: function (sucdId) {
                var self = this;
                self._sucdId.value = sucdId;
                //    self.sucdId_listeners.forEach(cb => { cb(<SuperClassDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SuperClassDetailBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<SuperClassDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SuperClassDetailBase.prototype, "sucaId", {
            get: function () {
                return this._sucaId.value;
            },
            set: function (sucaId) {
                var self = this;
                self._sucaId.value = sucaId;
                //    self.sucaId_listeners.forEach(cb => { cb(<SuperClassDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SuperClassDetailBase.prototype, "cultId", {
            get: function () {
                return this._cultId.value;
            },
            set: function (cultId) {
                var self = this;
                self._cultId.value = cultId;
                //    self.cultId_listeners.forEach(cb => { cb(<SuperClassDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public cultId_listeners: Array<(c:SuperClassDetail) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.sucdId_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.sucaId_listeners.splice(0);
            self.cultId_listeners.splice(0);
        }
        */
        SuperClassDetailBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "SuperClassDetail:" + x.sucdId;
            var ret = new Entities.SuperClassDetail(x.sucdId, x.rowVersion, x.sucaId, x.cultId);
            deserializedEntities[key] = ret;
            ret.sucaId = (x.sucaId !== undefined && x.sucaId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.sucaId.$entityName].fromJSONPartial(x.sucaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.cultId = (x.cultId !== undefined && x.cultId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cultId.$entityName].fromJSONPartial(x.cultId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        SuperClassDetailBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        SuperClassDetailBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "SuperClassDetail:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "SuperClassDetailBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "SuperClassDetail:"+x.sucdId;
                    var cachedCopy = <SuperClassDetail>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = SuperClassDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = SuperClassDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.SuperClassDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        SuperClassDetailBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.SuperClassDetail.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        SuperClassDetailBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/SuperClassDetail/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.SuperClassDetail.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        SuperClassDetailBase.prototype.toJSON = function () {
            var ret = {};
            ret.sucdId = this.sucdId;
            ret.rowVersion = this.rowVersion;
            ret.sucaId =
                (this.sucaId !== undefined && this.sucaId !== null) ?
                    { sucaId: this.sucaId.sucaId } :
                    (this.sucaId === undefined ? undefined : null);
            ret.cultId =
                (this.cultId !== undefined && this.cultId !== null) ?
                    { cultId: this.cultId.cultId } :
                    (this.cultId === undefined ? undefined : null);
            return ret;
        };
        SuperClassDetailBase.prototype.updateInstance = function (other) {
            var self = this;
            self.sucdId = other.sucdId;
            self.rowVersion = other.rowVersion;
            self.sucaId = other.sucaId;
            self.cultId = other.cultId;
        };
        SuperClassDetailBase.Create = function () {
            return new Entities.SuperClassDetail(undefined, undefined, undefined, undefined);
        };
        SuperClassDetailBase.CreateById = function (sucdId) {
            var ret = Entities.SuperClassDetail.Create();
            ret.sucdId = sucdId;
            return ret;
        };
        SuperClassDetailBase.findById_unecrypted = function (sucdId, $scope, $http, errFunc) {
            if (Entities.SuperClassDetail.cashedEntities[sucdId.toString()] !== undefined)
                return Entities.SuperClassDetail.cashedEntities[sucdId.toString()];
            var wsPath = "SuperClassDetail/findBySucdId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['sucdId'] = sucdId;
            var ret = Entities.SuperClassDetail.Create();
            Entities.SuperClassDetail.cashedEntities[sucdId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.SuperClassDetail.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + sucdId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        SuperClassDetailBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        SuperClassDetailBase.cashedEntities = {};
        return SuperClassDetailBase;
    })(NpTypes.BaseEntity);
    Entities.SuperClassDetailBase = SuperClassDetailBase;
    NpTypes.BaseEntity.entitiesFactory['SuperClassDetail'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.SuperClassDetail.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.SuperClassDetail.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=SuperClassDetailBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
