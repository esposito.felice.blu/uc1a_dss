/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/TemplateColumn.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/FileTemplate.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FileTemplateBase = (function (_super) {
        __extends(FileTemplateBase, _super);
        function FileTemplateBase(fiteId, name, rowVersion) {
            _super.call(this);
            var self = this;
            this._fiteId = new NpTypes.UIStringModel(fiteId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }
        FileTemplateBase.prototype.getKey = function () {
            return this.fiteId;
        };
        FileTemplateBase.prototype.getKeyName = function () {
            return "fiteId";
        };
        FileTemplateBase.prototype.getEntityName = function () {
            return 'FileTemplate';
        };
        FileTemplateBase.prototype.fromJSON = function (data) {
            return Entities.FileTemplate.fromJSONComplete(data);
        };
        Object.defineProperty(FileTemplateBase.prototype, "fiteId", {
            get: function () {
                return this._fiteId.value;
            },
            set: function (fiteId) {
                var self = this;
                self._fiteId.value = fiteId;
                //    self.fiteId_listeners.forEach(cb => { cb(<FileTemplate>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FileTemplateBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (name) {
                var self = this;
                self._name.value = name;
                //    self.name_listeners.forEach(cb => { cb(<FileTemplate>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FileTemplateBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<FileTemplate>self); });
            },
            enumerable: true,
            configurable: true
        });
        FileTemplateBase.prototype.templateColumnCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.FileTemplate.templateColumnCollection)) {
                if (self instanceof Entities.FileTemplate) {
                    Entities.FileTemplate.templateColumnCollection(self, func);
                }
            }
        };
        FileTemplateBase.prototype.classificationCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.FileTemplate.classificationCollection)) {
                if (self instanceof Entities.FileTemplate) {
                    Entities.FileTemplate.classificationCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.fiteId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        FileTemplateBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "FileTemplate:" + x.fiteId;
            var ret = new Entities.FileTemplate(x.fiteId, x.name, x.rowVersion);
            deserializedEntities[key] = ret;
            return ret;
        };
        FileTemplateBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        FileTemplateBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "FileTemplate:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "FileTemplateBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "FileTemplate:"+x.fiteId;
                    var cachedCopy = <FileTemplate>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = FileTemplate.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = FileTemplate.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.FileTemplate.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        FileTemplateBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.FileTemplate.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        FileTemplateBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/FileTemplate/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.FileTemplate.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        FileTemplateBase.prototype.toJSON = function () {
            var ret = {};
            ret.fiteId = this.fiteId;
            ret.name = this.name;
            ret.rowVersion = this.rowVersion;
            return ret;
        };
        FileTemplateBase.prototype.updateInstance = function (other) {
            var self = this;
            self.fiteId = other.fiteId;
            self.name = other.name;
            self.rowVersion = other.rowVersion;
        };
        FileTemplateBase.Create = function () {
            return new Entities.FileTemplate(undefined, undefined, undefined);
        };
        FileTemplateBase.CreateById = function (fiteId) {
            var ret = Entities.FileTemplate.Create();
            ret.fiteId = fiteId;
            return ret;
        };
        FileTemplateBase.findById_unecrypted = function (fiteId, $scope, $http, errFunc) {
            if (Entities.FileTemplate.cashedEntities[fiteId.toString()] !== undefined)
                return Entities.FileTemplate.cashedEntities[fiteId.toString()];
            var wsPath = "FileTemplate/findByFiteId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fiteId'] = fiteId;
            var ret = Entities.FileTemplate.Create();
            Entities.FileTemplate.cashedEntities[fiteId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.FileTemplate.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + fiteId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        FileTemplateBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public rowVersion_listeners: Array<(c:FileTemplate) => void> = [];
        FileTemplateBase.templateColumnCollection = null; //(fileTemplate, list) => { };
        FileTemplateBase.classificationCollection = null; //(fileTemplate, list) => { };
        FileTemplateBase.cashedEntities = {};
        return FileTemplateBase;
    })(NpTypes.BaseEntity);
    Entities.FileTemplateBase = FileTemplateBase;
    NpTypes.BaseEntity.entitiesFactory['FileTemplate'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.FileTemplate.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.FileTemplate.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=FileTemplateBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
