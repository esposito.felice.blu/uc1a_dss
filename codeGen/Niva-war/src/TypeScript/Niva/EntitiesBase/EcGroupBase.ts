/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCult.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
/// <reference path="../Entities/EcGroup.ts" />

module Entities {

    export class EcGroupBase extends NpTypes.BaseEntity {
        constructor(
            ecgrId: string,
            description: string,
            name: string,
            rowVersion: number,
            recordtype: number,
            cropLevel: number) 
        {
            super();
            var self = this;
            this._ecgrId = new NpTypes.UIStringModel(ecgrId, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._recordtype = new NpTypes.UINumberModel(recordtype, this);
            this._cropLevel = new NpTypes.UINumberModel(cropLevel, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.ecgrId; 
        }
        
        public getKeyName(): string { 
            return "ecgrId"; 
        }

        public getEntityName(): string { 
            return 'EcGroup'; 
        }
        
        public fromJSON(data: any): EcGroup[] { 
            return EcGroup.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.ecgrId === null || this.ecgrId === undefined)
                return true;
            if (this.ecgrId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: EcGroupBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //ecgrId property
        _ecgrId: NpTypes.UIStringModel;
        public get ecgrId():string {
            return this._ecgrId.value;
        }
        public set ecgrId(ecgrId:string) {
            var self = this;
            self._ecgrId.value = ecgrId;
        //    self.ecgrId_listeners.forEach(cb => { cb(<EcGroup>self); });
        }
        //public ecgrId_listeners: Array<(c:EcGroup) => void> = [];
        //description property
        _description: NpTypes.UIStringModel;
        public get description():string {
            return this._description.value;
        }
        public set description(description:string) {
            var self = this;
            self._description.value = description;
        //    self.description_listeners.forEach(cb => { cb(<EcGroup>self); });
        }
        //public description_listeners: Array<(c:EcGroup) => void> = [];
        //name property
        _name: NpTypes.UIStringModel;
        public get name():string {
            return this._name.value;
        }
        public set name(name:string) {
            var self = this;
            self._name.value = name;
        //    self.name_listeners.forEach(cb => { cb(<EcGroup>self); });
        }
        //public name_listeners: Array<(c:EcGroup) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<EcGroup>self); });
        }
        //public rowVersion_listeners: Array<(c:EcGroup) => void> = [];
        //recordtype property
        _recordtype: NpTypes.UINumberModel;
        public get recordtype():number {
            return this._recordtype.value;
        }
        public set recordtype(recordtype:number) {
            var self = this;
            self._recordtype.value = recordtype;
        //    self.recordtype_listeners.forEach(cb => { cb(<EcGroup>self); });
        }
        //public recordtype_listeners: Array<(c:EcGroup) => void> = [];
        //cropLevel property
        _cropLevel: NpTypes.UINumberModel;
        public get cropLevel():number {
            return this._cropLevel.value;
        }
        public set cropLevel(cropLevel:number) {
            var self = this;
            self._cropLevel.value = cropLevel;
        //    self.cropLevel_listeners.forEach(cb => { cb(<EcGroup>self); });
        }
        //public cropLevel_listeners: Array<(c:EcGroup) => void> = [];
        public static ecCultCollection: (self:Entities.EcGroup, func: (ecCultList: Entities.EcCult[]) => void) => void = null; //(ecGroup, list) => { };
        public ecCultCollection(func: (ecCultList: Entities.EcCult[]) => void) {
            var self: Entities.EcGroupBase = this;
        	if (!isVoid(EcGroup.ecCultCollection)) {
        		if (self instanceof Entities.EcGroup) {
        			EcGroup.ecCultCollection(<Entities.EcGroup>self, func);
                }
            }
        }
        public static decisionMakingCollection: (self:Entities.EcGroup, func: (decisionMakingList: Entities.DecisionMaking[]) => void) => void = null; //(ecGroup, list) => { };
        public decisionMakingCollection(func: (decisionMakingList: Entities.DecisionMaking[]) => void) {
            var self: Entities.EcGroupBase = this;
        	if (!isVoid(EcGroup.decisionMakingCollection)) {
        		if (self instanceof Entities.EcGroup) {
        			EcGroup.decisionMakingCollection(<Entities.EcGroup>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.ecgrId_listeners.splice(0);
            self.description_listeners.splice(0);
            self.name_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.recordtype_listeners.splice(0);
            self.cropLevel_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : EcGroup {
            var key = "EcGroup:"+x.ecgrId;
            var ret =  new EcGroup(
                x.ecgrId,
                x.description,
                x.name,
                x.rowVersion,
                x.recordtype,
                x.cropLevel
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): EcGroup {
            
            return <EcGroup>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : EcGroup {
            var self = this;
            var key="";
            var ret:EcGroup = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcGroup:"+x.$refId;
                ret = <EcGroup>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcGroupBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcGroup:"+x.ecgrId;
                    var cachedCopy = <EcGroup>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcGroup.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcGroup.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = EcGroup.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcGroup> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<EcGroup> = _.map(data, (x:any):EcGroup => {
                return EcGroup.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:EcGroup[])=>void)  {
            var url = "/Niva/rest/EcGroup/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = EcGroup.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.ecgrId = this.ecgrId;
                ret.description = this.description;
                ret.name = this.name;
                ret.rowVersion = this.rowVersion;
                ret.recordtype = this.recordtype;
                ret.cropLevel = this.cropLevel;
            return ret;
        }

        public updateInstance(other:EcGroup):void {
            var self = this;
            self.ecgrId = other.ecgrId
            self.description = other.description
            self.name = other.name
            self.rowVersion = other.rowVersion
            self.recordtype = other.recordtype
            self.cropLevel = other.cropLevel
        }

        public static Create() : EcGroup {
            return new EcGroup(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(ecgrId:string) : EcGroup {
            var ret = EcGroup.Create();
            ret.ecgrId = ecgrId;
            return ret;
        }

        public static cashedEntities: { [id: string]: EcGroup; } = {};
        public static findById_unecrypted(ecgrId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : EcGroup {
            if (Entities.EcGroup.cashedEntities[ecgrId.toString()] !== undefined)
                return Entities.EcGroup.cashedEntities[ecgrId.toString()];

            var wsPath = "EcGroup/findByEcgrId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['ecgrId'] = ecgrId;
            var ret = EcGroup.Create();
            Entities.EcGroup.cashedEntities[ecgrId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.EcGroup.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + ecgrId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['EcGroup'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.EcGroup.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.EcGroup.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
