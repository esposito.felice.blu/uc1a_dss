/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/ParcelsIssuesStatusPerDema.ts" />

module Entities {

    export class ParcelsIssuesStatusPerDemaBase extends NpTypes.BaseEntity {
        constructor(
            status: number,
            dteStatusUpdate: Date,
            rowVersion: number,
            parcelsIssuesStatusPerDemaId: string,
            parcelsIssuesId: ParcelsIssues) 
        {
            super();
            var self = this;
            this._status = new NpTypes.UINumberModel(status, this);
            this._dteStatusUpdate = new NpTypes.UIDateModel(dteStatusUpdate, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._parcelsIssuesStatusPerDemaId = new NpTypes.UIStringModel(parcelsIssuesStatusPerDemaId, this);
            this._parcelsIssuesId = new NpTypes.UIManyToOneModel<Entities.ParcelsIssues>(parcelsIssuesId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.parcelsIssuesStatusPerDemaId; 
        }
        
        public getKeyName(): string { 
            return "parcelsIssuesStatusPerDemaId"; 
        }

        public getEntityName(): string { 
            return 'ParcelsIssuesStatusPerDema'; 
        }
        
        public fromJSON(data: any): ParcelsIssuesStatusPerDema[] { 
            return ParcelsIssuesStatusPerDema.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.parcelsIssuesStatusPerDemaId === null || this.parcelsIssuesStatusPerDemaId === undefined)
                return true;
            if (this.parcelsIssuesStatusPerDemaId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: ParcelsIssuesStatusPerDemaBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //status property
        _status: NpTypes.UINumberModel;
        public get status():number {
            return this._status.value;
        }
        public set status(status:number) {
            var self = this;
            self._status.value = status;
        //    self.status_listeners.forEach(cb => { cb(<ParcelsIssuesStatusPerDema>self); });
        }
        //public status_listeners: Array<(c:ParcelsIssuesStatusPerDema) => void> = [];
        //dteStatusUpdate property
        _dteStatusUpdate: NpTypes.UIDateModel;
        public get dteStatusUpdate():Date {
            return this._dteStatusUpdate.value;
        }
        public set dteStatusUpdate(dteStatusUpdate:Date) {
            var self = this;
            self._dteStatusUpdate.value = dteStatusUpdate;
        //    self.dteStatusUpdate_listeners.forEach(cb => { cb(<ParcelsIssuesStatusPerDema>self); });
        }
        //public dteStatusUpdate_listeners: Array<(c:ParcelsIssuesStatusPerDema) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<ParcelsIssuesStatusPerDema>self); });
        }
        //public rowVersion_listeners: Array<(c:ParcelsIssuesStatusPerDema) => void> = [];
        //parcelsIssuesStatusPerDemaId property
        _parcelsIssuesStatusPerDemaId: NpTypes.UIStringModel;
        public get parcelsIssuesStatusPerDemaId():string {
            return this._parcelsIssuesStatusPerDemaId.value;
        }
        public set parcelsIssuesStatusPerDemaId(parcelsIssuesStatusPerDemaId:string) {
            var self = this;
            self._parcelsIssuesStatusPerDemaId.value = parcelsIssuesStatusPerDemaId;
        //    self.parcelsIssuesStatusPerDemaId_listeners.forEach(cb => { cb(<ParcelsIssuesStatusPerDema>self); });
        }
        //public parcelsIssuesStatusPerDemaId_listeners: Array<(c:ParcelsIssuesStatusPerDema) => void> = [];
        //parcelsIssuesId property
        _parcelsIssuesId: NpTypes.UIManyToOneModel<Entities.ParcelsIssues>;
        public get parcelsIssuesId():ParcelsIssues {
            return this._parcelsIssuesId.value;
        }
        public set parcelsIssuesId(parcelsIssuesId:ParcelsIssues) {
            var self = this;
            self._parcelsIssuesId.value = parcelsIssuesId;
        //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<ParcelsIssuesStatusPerDema>self); });
        }
        //public parcelsIssuesId_listeners: Array<(c:ParcelsIssuesStatusPerDema) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.status_listeners.splice(0);
            self.dteStatusUpdate_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.parcelsIssuesStatusPerDemaId_listeners.splice(0);
            self.parcelsIssuesId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : ParcelsIssuesStatusPerDema {
            var key = "ParcelsIssuesStatusPerDema:"+x.parcelsIssuesStatusPerDemaId;
            var ret =  new ParcelsIssuesStatusPerDema(
                x.status,
                isVoid(x.dteStatusUpdate) ? null : new Date(x.dteStatusUpdate),
                x.rowVersion,
                x.parcelsIssuesStatusPerDemaId,
                x.parcelsIssuesId
            );
            deserializedEntities[key] = ret;
            ret.parcelsIssuesId = (x.parcelsIssuesId !== undefined && x.parcelsIssuesId !== null) ? <ParcelsIssues>NpTypes.BaseEntity.entitiesFactory[x.parcelsIssuesId.$entityName].fromJSONPartial(x.parcelsIssuesId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): ParcelsIssuesStatusPerDema {
            
            return <ParcelsIssuesStatusPerDema>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : ParcelsIssuesStatusPerDema {
            var self = this;
            var key="";
            var ret:ParcelsIssuesStatusPerDema = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcelsIssuesStatusPerDema:"+x.$refId;
                ret = <ParcelsIssuesStatusPerDema>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcelsIssuesStatusPerDemaBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcelsIssuesStatusPerDema:"+x.parcelsIssuesStatusPerDemaId;
                    var cachedCopy = <ParcelsIssuesStatusPerDema>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcelsIssuesStatusPerDema.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcelsIssuesStatusPerDema.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = ParcelsIssuesStatusPerDema.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelsIssuesStatusPerDema> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<ParcelsIssuesStatusPerDema> = _.map(data, (x:any):ParcelsIssuesStatusPerDema => {
                return ParcelsIssuesStatusPerDema.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:ParcelsIssuesStatusPerDema[])=>void)  {
            var url = "/Niva/rest/ParcelsIssuesStatusPerDema/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = ParcelsIssuesStatusPerDema.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.status = this.status;
                ret.dteStatusUpdate = this.dteStatusUpdate;
                ret.rowVersion = this.rowVersion;
                ret.parcelsIssuesStatusPerDemaId = this.parcelsIssuesStatusPerDemaId;
                ret.parcelsIssuesId = 
                    (this.parcelsIssuesId !== undefined && this.parcelsIssuesId !== null) ? 
                        { parcelsIssuesId :  this.parcelsIssuesId.parcelsIssuesId} :
                        (this.parcelsIssuesId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:ParcelsIssuesStatusPerDema):void {
            var self = this;
            self.status = other.status
            self.dteStatusUpdate = other.dteStatusUpdate
            self.rowVersion = other.rowVersion
            self.parcelsIssuesStatusPerDemaId = other.parcelsIssuesStatusPerDemaId
            self.parcelsIssuesId = other.parcelsIssuesId
        }

        public static Create() : ParcelsIssuesStatusPerDema {
            return new ParcelsIssuesStatusPerDema(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(parcelsIssuesStatusPerDemaId:string) : ParcelsIssuesStatusPerDema {
            var ret = ParcelsIssuesStatusPerDema.Create();
            ret.parcelsIssuesStatusPerDemaId = parcelsIssuesStatusPerDemaId;
            return ret;
        }

        public static cashedEntities: { [id: string]: ParcelsIssuesStatusPerDema; } = {};
        public static findById_unecrypted(parcelsIssuesStatusPerDemaId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : ParcelsIssuesStatusPerDema {
            if (Entities.ParcelsIssuesStatusPerDema.cashedEntities[parcelsIssuesStatusPerDemaId.toString()] !== undefined)
                return Entities.ParcelsIssuesStatusPerDema.cashedEntities[parcelsIssuesStatusPerDemaId.toString()];

            var wsPath = "ParcelsIssuesStatusPerDema/findByParcelsIssuesStatusPerDemaId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['parcelsIssuesStatusPerDemaId'] = parcelsIssuesStatusPerDemaId;
            var ret = ParcelsIssuesStatusPerDema.Create();
            Entities.ParcelsIssuesStatusPerDema.cashedEntities[parcelsIssuesStatusPerDemaId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.ParcelsIssuesStatusPerDema.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + parcelsIssuesStatusPerDemaId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['ParcelsIssuesStatusPerDema'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.ParcelsIssuesStatusPerDema.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.ParcelsIssuesStatusPerDema.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
