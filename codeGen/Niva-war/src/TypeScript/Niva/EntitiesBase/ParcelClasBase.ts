/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Integrateddecision.ts" />
/// <reference path="../Entities/ParcelDecision.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/ParcelClas.ts" />

module Entities {

    export class ParcelClasBase extends NpTypes.BaseEntity {
        constructor(
            pclaId: string,
            probPred: number,
            probPred2: number,
            prodCode: number,
            parcIdentifier: string,
            parcCode: string,
            geom4326: ol.Feature,
            clasId: Classification,
            cultIdDecl: Cultivation,
            cultIdPred: Cultivation,
            cotyIdDecl: CoverType,
            cotyIdPred: CoverType,
            cultIdPred2: Cultivation) 
        {
            super();
            var self = this;
            this._pclaId = new NpTypes.UIStringModel(pclaId, this);
            this._probPred = new NpTypes.UINumberModel(probPred, this);
            this._probPred2 = new NpTypes.UINumberModel(probPred2, this);
            this._prodCode = new NpTypes.UINumberModel(prodCode, this);
            this._parcIdentifier = new NpTypes.UIStringModel(parcIdentifier, this);
            this._parcCode = new NpTypes.UIStringModel(parcCode, this);
            this._geom4326 = new NpTypes.UIGeoModel(geom4326, this);
            this._clasId = new NpTypes.UIManyToOneModel<Entities.Classification>(clasId, this);
            this._cultIdDecl = new NpTypes.UIManyToOneModel<Entities.Cultivation>(cultIdDecl, this);
            this._cultIdPred = new NpTypes.UIManyToOneModel<Entities.Cultivation>(cultIdPred, this);
            this._cotyIdDecl = new NpTypes.UIManyToOneModel<Entities.CoverType>(cotyIdDecl, this);
            this._cotyIdPred = new NpTypes.UIManyToOneModel<Entities.CoverType>(cotyIdPred, this);
            this._cultIdPred2 = new NpTypes.UIManyToOneModel<Entities.Cultivation>(cultIdPred2, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.pclaId; 
        }
        
        public getKeyName(): string { 
            return "pclaId"; 
        }

        public getEntityName(): string { 
            return 'ParcelClas'; 
        }
        
        public fromJSON(data: any): ParcelClas[] { 
            return ParcelClas.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.pclaId === null || this.pclaId === undefined)
                return true;
            if (this.pclaId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: ParcelClasBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //pclaId property
        _pclaId: NpTypes.UIStringModel;
        public get pclaId():string {
            return this._pclaId.value;
        }
        public set pclaId(pclaId:string) {
            var self = this;
            self._pclaId.value = pclaId;
        //    self.pclaId_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public pclaId_listeners: Array<(c:ParcelClas) => void> = [];
        //probPred property
        _probPred: NpTypes.UINumberModel;
        public get probPred():number {
            return this._probPred.value;
        }
        public set probPred(probPred:number) {
            var self = this;
            self._probPred.value = probPred;
        //    self.probPred_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public probPred_listeners: Array<(c:ParcelClas) => void> = [];
        //probPred2 property
        _probPred2: NpTypes.UINumberModel;
        public get probPred2():number {
            return this._probPred2.value;
        }
        public set probPred2(probPred2:number) {
            var self = this;
            self._probPred2.value = probPred2;
        //    self.probPred2_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public probPred2_listeners: Array<(c:ParcelClas) => void> = [];
        //prodCode property
        _prodCode: NpTypes.UINumberModel;
        public get prodCode():number {
            return this._prodCode.value;
        }
        public set prodCode(prodCode:number) {
            var self = this;
            self._prodCode.value = prodCode;
        //    self.prodCode_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public prodCode_listeners: Array<(c:ParcelClas) => void> = [];
        //parcIdentifier property
        _parcIdentifier: NpTypes.UIStringModel;
        public get parcIdentifier():string {
            return this._parcIdentifier.value;
        }
        public set parcIdentifier(parcIdentifier:string) {
            var self = this;
            self._parcIdentifier.value = parcIdentifier;
        //    self.parcIdentifier_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public parcIdentifier_listeners: Array<(c:ParcelClas) => void> = [];
        //parcCode property
        _parcCode: NpTypes.UIStringModel;
        public get parcCode():string {
            return this._parcCode.value;
        }
        public set parcCode(parcCode:string) {
            var self = this;
            self._parcCode.value = parcCode;
        //    self.parcCode_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public parcCode_listeners: Array<(c:ParcelClas) => void> = [];
        //geom4326 property
        _geom4326: NpTypes.UIGeoModel;
        public get geom4326():ol.Feature {
            return this._geom4326.value;
        }
        public set geom4326(geom4326:ol.Feature) {
            var self = this;
            self._geom4326.value = geom4326;
        //    self.geom4326_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public geom4326_listeners: Array<(c:ParcelClas) => void> = [];
        //clasId property
        _clasId: NpTypes.UIManyToOneModel<Entities.Classification>;
        public get clasId():Classification {
            return this._clasId.value;
        }
        public set clasId(clasId:Classification) {
            var self = this;
            self._clasId.value = clasId;
        //    self.clasId_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public clasId_listeners: Array<(c:ParcelClas) => void> = [];
        //cultIdDecl property
        _cultIdDecl: NpTypes.UIManyToOneModel<Entities.Cultivation>;
        public get cultIdDecl():Cultivation {
            return this._cultIdDecl.value;
        }
        public set cultIdDecl(cultIdDecl:Cultivation) {
            var self = this;
            self._cultIdDecl.value = cultIdDecl;
        //    self.cultIdDecl_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public cultIdDecl_listeners: Array<(c:ParcelClas) => void> = [];
        //cultIdPred property
        _cultIdPred: NpTypes.UIManyToOneModel<Entities.Cultivation>;
        public get cultIdPred():Cultivation {
            return this._cultIdPred.value;
        }
        public set cultIdPred(cultIdPred:Cultivation) {
            var self = this;
            self._cultIdPred.value = cultIdPred;
        //    self.cultIdPred_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public cultIdPred_listeners: Array<(c:ParcelClas) => void> = [];
        //cotyIdDecl property
        _cotyIdDecl: NpTypes.UIManyToOneModel<Entities.CoverType>;
        public get cotyIdDecl():CoverType {
            return this._cotyIdDecl.value;
        }
        public set cotyIdDecl(cotyIdDecl:CoverType) {
            var self = this;
            self._cotyIdDecl.value = cotyIdDecl;
        //    self.cotyIdDecl_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public cotyIdDecl_listeners: Array<(c:ParcelClas) => void> = [];
        //cotyIdPred property
        _cotyIdPred: NpTypes.UIManyToOneModel<Entities.CoverType>;
        public get cotyIdPred():CoverType {
            return this._cotyIdPred.value;
        }
        public set cotyIdPred(cotyIdPred:CoverType) {
            var self = this;
            self._cotyIdPred.value = cotyIdPred;
        //    self.cotyIdPred_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public cotyIdPred_listeners: Array<(c:ParcelClas) => void> = [];
        //cultIdPred2 property
        _cultIdPred2: NpTypes.UIManyToOneModel<Entities.Cultivation>;
        public get cultIdPred2():Cultivation {
            return this._cultIdPred2.value;
        }
        public set cultIdPred2(cultIdPred2:Cultivation) {
            var self = this;
            self._cultIdPred2.value = cultIdPred2;
        //    self.cultIdPred2_listeners.forEach(cb => { cb(<ParcelClas>self); });
        }
        //public cultIdPred2_listeners: Array<(c:ParcelClas) => void> = [];
        public static integrateddecisionCollection: (self:Entities.ParcelClas, func: (integrateddecisionList: Entities.Integrateddecision[]) => void) => void = null; //(parcelClas, list) => { };
        public integrateddecisionCollection(func: (integrateddecisionList: Entities.Integrateddecision[]) => void) {
            var self: Entities.ParcelClasBase = this;
        	if (!isVoid(ParcelClas.integrateddecisionCollection)) {
        		if (self instanceof Entities.ParcelClas) {
        			ParcelClas.integrateddecisionCollection(<Entities.ParcelClas>self, func);
                }
            }
        }
        public static parcelDecisionCollection: (self:Entities.ParcelClas, func: (parcelDecisionList: Entities.ParcelDecision[]) => void) => void = null; //(parcelClas, list) => { };
        public parcelDecisionCollection(func: (parcelDecisionList: Entities.ParcelDecision[]) => void) {
            var self: Entities.ParcelClasBase = this;
        	if (!isVoid(ParcelClas.parcelDecisionCollection)) {
        		if (self instanceof Entities.ParcelClas) {
        			ParcelClas.parcelDecisionCollection(<Entities.ParcelClas>self, func);
                }
            }
        }
        public static parcelsIssuesCollection: (self:Entities.ParcelClas, func: (parcelsIssuesList: Entities.ParcelsIssues[]) => void) => void = null; //(parcelClas, list) => { };
        public parcelsIssuesCollection(func: (parcelsIssuesList: Entities.ParcelsIssues[]) => void) {
            var self: Entities.ParcelClasBase = this;
        	if (!isVoid(ParcelClas.parcelsIssuesCollection)) {
        		if (self instanceof Entities.ParcelClas) {
        			ParcelClas.parcelsIssuesCollection(<Entities.ParcelClas>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.pclaId_listeners.splice(0);
            self.probPred_listeners.splice(0);
            self.probPred2_listeners.splice(0);
            self.prodCode_listeners.splice(0);
            self.parcIdentifier_listeners.splice(0);
            self.parcCode_listeners.splice(0);
            self.geom4326_listeners.splice(0);
            self.clasId_listeners.splice(0);
            self.cultIdDecl_listeners.splice(0);
            self.cultIdPred_listeners.splice(0);
            self.cotyIdDecl_listeners.splice(0);
            self.cotyIdPred_listeners.splice(0);
            self.cultIdPred2_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : ParcelClas {
            var key = "ParcelClas:"+x.pclaId;
            var ret =  new ParcelClas(
                x.pclaId,
                x.probPred,
                x.probPred2,
                x.prodCode,
                x.parcIdentifier,
                x.parcCode,
                x.geom4326,
                x.clasId,
                x.cultIdDecl,
                x.cultIdPred,
                x.cotyIdDecl,
                x.cotyIdPred,
                x.cultIdPred2
            );
            deserializedEntities[key] = ret;
            ret.clasId = (x.clasId !== undefined && x.clasId !== null) ? <Classification>NpTypes.BaseEntity.entitiesFactory[x.clasId.$entityName].fromJSONPartial(x.clasId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.cultIdDecl = (x.cultIdDecl !== undefined && x.cultIdDecl !== null) ? <Cultivation>NpTypes.BaseEntity.entitiesFactory[x.cultIdDecl.$entityName].fromJSONPartial(x.cultIdDecl, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.cultIdPred = (x.cultIdPred !== undefined && x.cultIdPred !== null) ? <Cultivation>NpTypes.BaseEntity.entitiesFactory[x.cultIdPred.$entityName].fromJSONPartial(x.cultIdPred, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.cotyIdDecl = (x.cotyIdDecl !== undefined && x.cotyIdDecl !== null) ? <CoverType>NpTypes.BaseEntity.entitiesFactory[x.cotyIdDecl.$entityName].fromJSONPartial(x.cotyIdDecl, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.cotyIdPred = (x.cotyIdPred !== undefined && x.cotyIdPred !== null) ? <CoverType>NpTypes.BaseEntity.entitiesFactory[x.cotyIdPred.$entityName].fromJSONPartial(x.cotyIdPred, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.cultIdPred2 = (x.cultIdPred2 !== undefined && x.cultIdPred2 !== null) ? <Cultivation>NpTypes.BaseEntity.entitiesFactory[x.cultIdPred2.$entityName].fromJSONPartial(x.cultIdPred2, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): ParcelClas {
            
            return <ParcelClas>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : ParcelClas {
            var self = this;
            var key="";
            var ret:ParcelClas = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcelClas:"+x.$refId;
                ret = <ParcelClas>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcelClasBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcelClas:"+x.pclaId;
                    var cachedCopy = <ParcelClas>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcelClas.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcelClas.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = ParcelClas.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelClas> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<ParcelClas> = _.map(data, (x:any):ParcelClas => {
                return ParcelClas.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:ParcelClas[])=>void)  {
            var url = "/Niva/rest/ParcelClas/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = ParcelClas.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.pclaId = this.pclaId;
                ret.probPred = this.probPred;
                ret.probPred2 = this.probPred2;
                ret.prodCode = this.prodCode;
                ret.parcIdentifier = this.parcIdentifier;
                ret.parcCode = this.parcCode;
                ret.geom4326 = this.geom4326;
                ret.clasId = 
                    (this.clasId !== undefined && this.clasId !== null) ? 
                        { clasId :  this.clasId.clasId} :
                        (this.clasId === undefined ? undefined : null);
                ret.cultIdDecl = 
                    (this.cultIdDecl !== undefined && this.cultIdDecl !== null) ? 
                        { cultId :  this.cultIdDecl.cultId} :
                        (this.cultIdDecl === undefined ? undefined : null);
                ret.cultIdPred = 
                    (this.cultIdPred !== undefined && this.cultIdPred !== null) ? 
                        { cultId :  this.cultIdPred.cultId} :
                        (this.cultIdPred === undefined ? undefined : null);
                ret.cotyIdDecl = 
                    (this.cotyIdDecl !== undefined && this.cotyIdDecl !== null) ? 
                        { cotyId :  this.cotyIdDecl.cotyId} :
                        (this.cotyIdDecl === undefined ? undefined : null);
                ret.cotyIdPred = 
                    (this.cotyIdPred !== undefined && this.cotyIdPred !== null) ? 
                        { cotyId :  this.cotyIdPred.cotyId} :
                        (this.cotyIdPred === undefined ? undefined : null);
                ret.cultIdPred2 = 
                    (this.cultIdPred2 !== undefined && this.cultIdPred2 !== null) ? 
                        { cultId :  this.cultIdPred2.cultId} :
                        (this.cultIdPred2 === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:ParcelClas):void {
            var self = this;
            self.pclaId = other.pclaId
            self.probPred = other.probPred
            self.probPred2 = other.probPred2
            self.prodCode = other.prodCode
            self.parcIdentifier = other.parcIdentifier
            self.parcCode = other.parcCode
            self.geom4326 = other.geom4326
            self.clasId = other.clasId
            self.cultIdDecl = other.cultIdDecl
            self.cultIdPred = other.cultIdPred
            self.cotyIdDecl = other.cotyIdDecl
            self.cotyIdPred = other.cotyIdPred
            self.cultIdPred2 = other.cultIdPred2
        }

        public static Create() : ParcelClas {
            return new ParcelClas(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(pclaId:string) : ParcelClas {
            var ret = ParcelClas.Create();
            ret.pclaId = pclaId;
            return ret;
        }

        public static cashedEntities: { [id: string]: ParcelClas; } = {};
        public static findById_unecrypted(pclaId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : ParcelClas {
            if (Entities.ParcelClas.cashedEntities[pclaId.toString()] !== undefined)
                return Entities.ParcelClas.cashedEntities[pclaId.toString()];

            var wsPath = "ParcelClas/findByPclaId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['pclaId'] = pclaId;
            var ret = ParcelClas.Create();
            Entities.ParcelClas.cashedEntities[pclaId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.ParcelClas.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + pclaId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return 0;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['ParcelClas'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.ParcelClas.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.ParcelClas.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
