/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/GpRequestsContexts.ts" />
/// <reference path="../Entities/GpUpload.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var GpUploadBase = (function (_super) {
        __extends(GpUploadBase, _super);
        function GpUploadBase(gpUploadsId, data, environment, dteUpload, hash, image, gpRequestsContextsId) {
            _super.call(this);
            var self = this;
            this._gpUploadsId = new NpTypes.UIStringModel(gpUploadsId, this);
            this._data = new NpTypes.UIStringModel(data, this);
            this._environment = new NpTypes.UIStringModel(environment, this);
            this._dteUpload = new NpTypes.UIDateModel(dteUpload, this);
            this._hash = new NpTypes.UIStringModel(hash, this);
            this._image = new NpTypes.UIBlobModel(image, function () { return 'Download'; }, function (fileName) { }, this);
            this._gpRequestsContextsId = new NpTypes.UIManyToOneModel(gpRequestsContextsId, this);
            self.postConstruct();
        }
        GpUploadBase.prototype.getKey = function () {
            return this.gpUploadsId;
        };
        GpUploadBase.prototype.getKeyName = function () {
            return "gpUploadsId";
        };
        GpUploadBase.prototype.getEntityName = function () {
            return 'GpUpload';
        };
        GpUploadBase.prototype.fromJSON = function (data) {
            return Entities.GpUpload.fromJSONComplete(data);
        };
        Object.defineProperty(GpUploadBase.prototype, "gpUploadsId", {
            get: function () {
                return this._gpUploadsId.value;
            },
            set: function (gpUploadsId) {
                var self = this;
                self._gpUploadsId.value = gpUploadsId;
                //    self.gpUploadsId_listeners.forEach(cb => { cb(<GpUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpUploadBase.prototype, "data", {
            get: function () {
                return this._data.value;
            },
            set: function (data) {
                var self = this;
                self._data.value = data;
                //    self.data_listeners.forEach(cb => { cb(<GpUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpUploadBase.prototype, "environment", {
            get: function () {
                return this._environment.value;
            },
            set: function (environment) {
                var self = this;
                self._environment.value = environment;
                //    self.environment_listeners.forEach(cb => { cb(<GpUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpUploadBase.prototype, "dteUpload", {
            get: function () {
                return this._dteUpload.value;
            },
            set: function (dteUpload) {
                var self = this;
                self._dteUpload.value = dteUpload;
                //    self.dteUpload_listeners.forEach(cb => { cb(<GpUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpUploadBase.prototype, "hash", {
            get: function () {
                return this._hash.value;
            },
            set: function (hash) {
                var self = this;
                self._hash.value = hash;
                //    self.hash_listeners.forEach(cb => { cb(<GpUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpUploadBase.prototype, "image", {
            get: function () {
                return this._image.value;
            },
            set: function (image) {
                var self = this;
                self._image.value = image;
                //    self.image_listeners.forEach(cb => { cb(<GpUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpUploadBase.prototype, "gpRequestsContextsId", {
            get: function () {
                return this._gpRequestsContextsId.value;
            },
            set: function (gpRequestsContextsId) {
                var self = this;
                self._gpRequestsContextsId.value = gpRequestsContextsId;
                //    self.gpRequestsContextsId_listeners.forEach(cb => { cb(<GpUpload>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public gpRequestsContextsId_listeners: Array<(c:GpUpload) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.gpUploadsId_listeners.splice(0);
            self.data_listeners.splice(0);
            self.environment_listeners.splice(0);
            self.dteUpload_listeners.splice(0);
            self.hash_listeners.splice(0);
            self.image_listeners.splice(0);
            self.gpRequestsContextsId_listeners.splice(0);
        }
        */
        GpUploadBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "GpUpload:" + x.gpUploadsId;
            var ret = new Entities.GpUpload(x.gpUploadsId, x.data, x.environment, isVoid(x.dteUpload) ? null : new Date(x.dteUpload), x.hash, x.image, x.gpRequestsContextsId);
            deserializedEntities[key] = ret;
            ret.gpRequestsContextsId = (x.gpRequestsContextsId !== undefined && x.gpRequestsContextsId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.gpRequestsContextsId.$entityName].fromJSONPartial(x.gpRequestsContextsId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        GpUploadBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        GpUploadBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "GpUpload:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "GpUploadBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "GpUpload:"+x.gpUploadsId;
                    var cachedCopy = <GpUpload>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = GpUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = GpUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.GpUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        GpUploadBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.GpUpload.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        GpUploadBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/GpUpload/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.GpUpload.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        GpUploadBase.prototype.toJSON = function () {
            var ret = {};
            ret.gpUploadsId = this.gpUploadsId;
            ret.data = this.data;
            ret.environment = this.environment;
            ret.dteUpload = this.dteUpload;
            ret.hash = this.hash;
            ret.image = this.image;
            ret.gpRequestsContextsId =
                (this.gpRequestsContextsId !== undefined && this.gpRequestsContextsId !== null) ?
                    { gpRequestsContextsId: this.gpRequestsContextsId.gpRequestsContextsId } :
                    (this.gpRequestsContextsId === undefined ? undefined : null);
            return ret;
        };
        GpUploadBase.prototype.updateInstance = function (other) {
            var self = this;
            self.gpUploadsId = other.gpUploadsId;
            self.data = other.data;
            self.environment = other.environment;
            self.dteUpload = other.dteUpload;
            self.hash = other.hash;
            self.image = other.image;
            self.gpRequestsContextsId = other.gpRequestsContextsId;
        };
        GpUploadBase.Create = function () {
            return new Entities.GpUpload(undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        GpUploadBase.CreateById = function (gpUploadsId) {
            var ret = Entities.GpUpload.Create();
            ret.gpUploadsId = gpUploadsId;
            return ret;
        };
        GpUploadBase.findById_unecrypted = function (gpUploadsId, $scope, $http, errFunc) {
            if (Entities.GpUpload.cashedEntities[gpUploadsId.toString()] !== undefined)
                return Entities.GpUpload.cashedEntities[gpUploadsId.toString()];
            var wsPath = "GpUpload/findByGpUploadsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['gpUploadsId'] = gpUploadsId;
            var ret = Entities.GpUpload.Create();
            Entities.GpUpload.cashedEntities[gpUploadsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.GpUpload.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + gpUploadsId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        GpUploadBase.prototype.getRowVersion = function () {
            return 0;
        };
        GpUploadBase.cashedEntities = {};
        return GpUploadBase;
    })(NpTypes.BaseEntity);
    Entities.GpUploadBase = GpUploadBase;
    NpTypes.BaseEntity.entitiesFactory['GpUpload'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.GpUpload.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.GpUpload.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=GpUploadBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
