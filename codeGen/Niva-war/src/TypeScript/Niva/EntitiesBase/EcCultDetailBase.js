/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCult.ts" />
/// <reference path="../Entities/EcCultDetail.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcCultDetailBase = (function (_super) {
        __extends(EcCultDetailBase, _super);
        function EcCultDetailBase(eccdId, orderingNumber, agreesDeclar, comparisonOper, probabThres, decisionLight, rowVersion, agreesDeclar2, comparisonOper2, probabThres2, probabThresSum, comparisonOper3, eccuId) {
            _super.call(this);
            var self = this;
            this._eccdId = new NpTypes.UIStringModel(eccdId, this);
            this._orderingNumber = new NpTypes.UINumberModel(orderingNumber, this);
            this._agreesDeclar = new NpTypes.UINumberModel(agreesDeclar, this);
            this._comparisonOper = new NpTypes.UINumberModel(comparisonOper, this);
            this._probabThres = new NpTypes.UINumberModel(probabThres, this);
            this._decisionLight = new NpTypes.UINumberModel(decisionLight, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._agreesDeclar2 = new NpTypes.UINumberModel(agreesDeclar2, this);
            this._comparisonOper2 = new NpTypes.UINumberModel(comparisonOper2, this);
            this._probabThres2 = new NpTypes.UINumberModel(probabThres2, this);
            this._probabThresSum = new NpTypes.UINumberModel(probabThresSum, this);
            this._comparisonOper3 = new NpTypes.UINumberModel(comparisonOper3, this);
            this._eccuId = new NpTypes.UIManyToOneModel(eccuId, this);
            self.postConstruct();
        }
        EcCultDetailBase.prototype.getKey = function () {
            return this.eccdId;
        };
        EcCultDetailBase.prototype.getKeyName = function () {
            return "eccdId";
        };
        EcCultDetailBase.prototype.getEntityName = function () {
            return 'EcCultDetail';
        };
        EcCultDetailBase.prototype.fromJSON = function (data) {
            return Entities.EcCultDetail.fromJSONComplete(data);
        };
        Object.defineProperty(EcCultDetailBase.prototype, "eccdId", {
            get: function () {
                return this._eccdId.value;
            },
            set: function (eccdId) {
                var self = this;
                self._eccdId.value = eccdId;
                //    self.eccdId_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "orderingNumber", {
            get: function () {
                return this._orderingNumber.value;
            },
            set: function (orderingNumber) {
                var self = this;
                self._orderingNumber.value = orderingNumber;
                //    self.orderingNumber_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "agreesDeclar", {
            get: function () {
                return this._agreesDeclar.value;
            },
            set: function (agreesDeclar) {
                var self = this;
                self._agreesDeclar.value = agreesDeclar;
                //    self.agreesDeclar_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "comparisonOper", {
            get: function () {
                return this._comparisonOper.value;
            },
            set: function (comparisonOper) {
                var self = this;
                self._comparisonOper.value = comparisonOper;
                //    self.comparisonOper_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "probabThres", {
            get: function () {
                return this._probabThres.value;
            },
            set: function (probabThres) {
                var self = this;
                self._probabThres.value = probabThres;
                //    self.probabThres_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "decisionLight", {
            get: function () {
                return this._decisionLight.value;
            },
            set: function (decisionLight) {
                var self = this;
                self._decisionLight.value = decisionLight;
                //    self.decisionLight_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "agreesDeclar2", {
            get: function () {
                return this._agreesDeclar2.value;
            },
            set: function (agreesDeclar2) {
                var self = this;
                self._agreesDeclar2.value = agreesDeclar2;
                //    self.agreesDeclar2_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "comparisonOper2", {
            get: function () {
                return this._comparisonOper2.value;
            },
            set: function (comparisonOper2) {
                var self = this;
                self._comparisonOper2.value = comparisonOper2;
                //    self.comparisonOper2_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "probabThres2", {
            get: function () {
                return this._probabThres2.value;
            },
            set: function (probabThres2) {
                var self = this;
                self._probabThres2.value = probabThres2;
                //    self.probabThres2_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "probabThresSum", {
            get: function () {
                return this._probabThresSum.value;
            },
            set: function (probabThresSum) {
                var self = this;
                self._probabThresSum.value = probabThresSum;
                //    self.probabThresSum_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "comparisonOper3", {
            get: function () {
                return this._comparisonOper3.value;
            },
            set: function (comparisonOper3) {
                var self = this;
                self._comparisonOper3.value = comparisonOper3;
                //    self.comparisonOper3_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailBase.prototype, "eccuId", {
            get: function () {
                return this._eccuId.value;
            },
            set: function (eccuId) {
                var self = this;
                self._eccuId.value = eccuId;
                //    self.eccuId_listeners.forEach(cb => { cb(<EcCultDetail>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public eccuId_listeners: Array<(c:EcCultDetail) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.eccdId_listeners.splice(0);
            self.orderingNumber_listeners.splice(0);
            self.agreesDeclar_listeners.splice(0);
            self.comparisonOper_listeners.splice(0);
            self.probabThres_listeners.splice(0);
            self.decisionLight_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.agreesDeclar2_listeners.splice(0);
            self.comparisonOper2_listeners.splice(0);
            self.probabThres2_listeners.splice(0);
            self.probabThresSum_listeners.splice(0);
            self.comparisonOper3_listeners.splice(0);
            self.eccuId_listeners.splice(0);
        }
        */
        EcCultDetailBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "EcCultDetail:" + x.eccdId;
            var ret = new Entities.EcCultDetail(x.eccdId, x.orderingNumber, x.agreesDeclar, x.comparisonOper, x.probabThres, x.decisionLight, x.rowVersion, x.agreesDeclar2, x.comparisonOper2, x.probabThres2, x.probabThresSum, x.comparisonOper3, x.eccuId);
            deserializedEntities[key] = ret;
            ret.eccuId = (x.eccuId !== undefined && x.eccuId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.eccuId.$entityName].fromJSONPartial(x.eccuId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        EcCultDetailBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        EcCultDetailBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcCultDetail:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcCultDetailBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcCultDetail:"+x.eccdId;
                    var cachedCopy = <EcCultDetail>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcCultDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcCultDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.EcCultDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        EcCultDetailBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.EcCultDetail.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        EcCultDetailBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/EcCultDetail/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.EcCultDetail.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        EcCultDetailBase.prototype.toJSON = function () {
            var ret = {};
            ret.eccdId = this.eccdId;
            ret.orderingNumber = this.orderingNumber;
            ret.agreesDeclar = this.agreesDeclar;
            ret.comparisonOper = this.comparisonOper;
            ret.probabThres = this.probabThres;
            ret.decisionLight = this.decisionLight;
            ret.rowVersion = this.rowVersion;
            ret.agreesDeclar2 = this.agreesDeclar2;
            ret.comparisonOper2 = this.comparisonOper2;
            ret.probabThres2 = this.probabThres2;
            ret.probabThresSum = this.probabThresSum;
            ret.comparisonOper3 = this.comparisonOper3;
            ret.eccuId =
                (this.eccuId !== undefined && this.eccuId !== null) ?
                    { eccuId: this.eccuId.eccuId } :
                    (this.eccuId === undefined ? undefined : null);
            return ret;
        };
        EcCultDetailBase.prototype.updateInstance = function (other) {
            var self = this;
            self.eccdId = other.eccdId;
            self.orderingNumber = other.orderingNumber;
            self.agreesDeclar = other.agreesDeclar;
            self.comparisonOper = other.comparisonOper;
            self.probabThres = other.probabThres;
            self.decisionLight = other.decisionLight;
            self.rowVersion = other.rowVersion;
            self.agreesDeclar2 = other.agreesDeclar2;
            self.comparisonOper2 = other.comparisonOper2;
            self.probabThres2 = other.probabThres2;
            self.probabThresSum = other.probabThresSum;
            self.comparisonOper3 = other.comparisonOper3;
            self.eccuId = other.eccuId;
        };
        EcCultDetailBase.Create = function () {
            return new Entities.EcCultDetail(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        EcCultDetailBase.CreateById = function (eccdId) {
            var ret = Entities.EcCultDetail.Create();
            ret.eccdId = eccdId;
            return ret;
        };
        EcCultDetailBase.findById_unecrypted = function (eccdId, $scope, $http, errFunc) {
            if (Entities.EcCultDetail.cashedEntities[eccdId.toString()] !== undefined)
                return Entities.EcCultDetail.cashedEntities[eccdId.toString()];
            var wsPath = "EcCultDetail/findByEccdId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['eccdId'] = eccdId;
            var ret = Entities.EcCultDetail.Create();
            Entities.EcCultDetail.cashedEntities[eccdId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.EcCultDetail.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + eccdId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        EcCultDetailBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        EcCultDetailBase.cashedEntities = {};
        return EcCultDetailBase;
    })(NpTypes.BaseEntity);
    Entities.EcCultDetailBase = EcCultDetailBase;
    NpTypes.BaseEntity.entitiesFactory['EcCultDetail'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.EcCultDetail.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.EcCultDetail.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=EcCultDetailBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
