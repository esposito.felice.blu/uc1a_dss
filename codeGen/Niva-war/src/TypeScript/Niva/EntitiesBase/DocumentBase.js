/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Document.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var DocumentBase = (function (_super) {
        __extends(DocumentBase, _super);
        function DocumentBase(docuId, attachedFile, filePath, description, rowVersion) {
            _super.call(this);
            var self = this;
            this._docuId = new NpTypes.UIStringModel(docuId, this);
            this._attachedFile = new NpTypes.UIBlobModel(attachedFile, function () { return self.filePath; }, function (fileName) { self.filePath = fileName; }, this);
            this._filePath = new NpTypes.UIStringModel(filePath, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }
        DocumentBase.prototype.getKey = function () {
            return this.docuId;
        };
        DocumentBase.prototype.getKeyName = function () {
            return "docuId";
        };
        DocumentBase.prototype.getEntityName = function () {
            return 'Document';
        };
        DocumentBase.prototype.fromJSON = function (data) {
            return Entities.Document.fromJSONComplete(data);
        };
        Object.defineProperty(DocumentBase.prototype, "docuId", {
            get: function () {
                return this._docuId.value;
            },
            set: function (docuId) {
                var self = this;
                self._docuId.value = docuId;
                //    self.docuId_listeners.forEach(cb => { cb(<Document>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DocumentBase.prototype, "attachedFile", {
            get: function () {
                return this._attachedFile.value;
            },
            set: function (attachedFile) {
                var self = this;
                self._attachedFile.value = attachedFile;
                //    self.attachedFile_listeners.forEach(cb => { cb(<Document>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DocumentBase.prototype, "filePath", {
            get: function () {
                return this._filePath.value;
            },
            set: function (filePath) {
                var self = this;
                self._filePath.value = filePath;
                //    self.filePath_listeners.forEach(cb => { cb(<Document>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DocumentBase.prototype, "description", {
            get: function () {
                return this._description.value;
            },
            set: function (description) {
                var self = this;
                self._description.value = description;
                //    self.description_listeners.forEach(cb => { cb(<Document>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DocumentBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<Document>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public rowVersion_listeners: Array<(c:Document) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.docuId_listeners.splice(0);
            self.attachedFile_listeners.splice(0);
            self.filePath_listeners.splice(0);
            self.description_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        DocumentBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "Document:" + x.docuId;
            var ret = new Entities.Document(x.docuId, x.attachedFile, x.filePath, x.description, x.rowVersion);
            deserializedEntities[key] = ret;
            return ret;
        };
        DocumentBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        DocumentBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Document:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "DocumentBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Document:"+x.docuId;
                    var cachedCopy = <Document>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Document.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Document.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.Document.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        DocumentBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.Document.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        DocumentBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/Document/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.Document.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        DocumentBase.prototype.toJSON = function () {
            var ret = {};
            ret.docuId = this.docuId;
            ret.attachedFile = this.attachedFile;
            ret.filePath = this.filePath;
            ret.description = this.description;
            ret.rowVersion = this.rowVersion;
            return ret;
        };
        DocumentBase.prototype.updateInstance = function (other) {
            var self = this;
            self.docuId = other.docuId;
            self.attachedFile = other.attachedFile;
            self.filePath = other.filePath;
            self.description = other.description;
            self.rowVersion = other.rowVersion;
        };
        DocumentBase.Create = function () {
            return new Entities.Document(undefined, undefined, undefined, undefined, undefined);
        };
        DocumentBase.CreateById = function (docuId) {
            var ret = Entities.Document.Create();
            ret.docuId = docuId;
            return ret;
        };
        DocumentBase.findById_unecrypted = function (docuId, $scope, $http, errFunc) {
            if (Entities.Document.cashedEntities[docuId.toString()] !== undefined)
                return Entities.Document.cashedEntities[docuId.toString()];
            var wsPath = "Document/findByDocuId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['docuId'] = docuId;
            var ret = Entities.Document.Create();
            Entities.Document.cashedEntities[docuId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.Document.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + docuId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        DocumentBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        DocumentBase.cashedEntities = {};
        return DocumentBase;
    })(NpTypes.BaseEntity);
    Entities.DocumentBase = DocumentBase;
    NpTypes.BaseEntity.entitiesFactory['Document'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.Document.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.Document.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=DocumentBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
