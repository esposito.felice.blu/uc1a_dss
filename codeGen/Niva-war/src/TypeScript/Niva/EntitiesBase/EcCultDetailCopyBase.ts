/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
/// <reference path="../Entities/EcCultDetailCopy.ts" />

module Entities {

    export class EcCultDetailCopyBase extends NpTypes.BaseEntity {
        constructor(
            ecdcId: string,
            orderingNumber: number,
            agreesDeclar: number,
            comparisonOper: number,
            probabThres: number,
            decisionLight: number,
            rowVersion: number,
            agreesDeclar2: number,
            comparisonOper2: number,
            probabThres2: number,
            probabThresSum: number,
            comparisonOper3: number,
            ecccId: EcCultCopy) 
        {
            super();
            var self = this;
            this._ecdcId = new NpTypes.UIStringModel(ecdcId, this);
            this._orderingNumber = new NpTypes.UINumberModel(orderingNumber, this);
            this._agreesDeclar = new NpTypes.UINumberModel(agreesDeclar, this);
            this._comparisonOper = new NpTypes.UINumberModel(comparisonOper, this);
            this._probabThres = new NpTypes.UINumberModel(probabThres, this);
            this._decisionLight = new NpTypes.UINumberModel(decisionLight, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._agreesDeclar2 = new NpTypes.UINumberModel(agreesDeclar2, this);
            this._comparisonOper2 = new NpTypes.UINumberModel(comparisonOper2, this);
            this._probabThres2 = new NpTypes.UINumberModel(probabThres2, this);
            this._probabThresSum = new NpTypes.UINumberModel(probabThresSum, this);
            this._comparisonOper3 = new NpTypes.UINumberModel(comparisonOper3, this);
            this._ecccId = new NpTypes.UIManyToOneModel<Entities.EcCultCopy>(ecccId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.ecdcId; 
        }
        
        public getKeyName(): string { 
            return "ecdcId"; 
        }

        public getEntityName(): string { 
            return 'EcCultDetailCopy'; 
        }
        
        public fromJSON(data: any): EcCultDetailCopy[] { 
            return EcCultDetailCopy.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.ecdcId === null || this.ecdcId === undefined)
                return true;
            if (this.ecdcId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: EcCultDetailCopyBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //ecdcId property
        _ecdcId: NpTypes.UIStringModel;
        public get ecdcId():string {
            return this._ecdcId.value;
        }
        public set ecdcId(ecdcId:string) {
            var self = this;
            self._ecdcId.value = ecdcId;
        //    self.ecdcId_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public ecdcId_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //orderingNumber property
        _orderingNumber: NpTypes.UINumberModel;
        public get orderingNumber():number {
            return this._orderingNumber.value;
        }
        public set orderingNumber(orderingNumber:number) {
            var self = this;
            self._orderingNumber.value = orderingNumber;
        //    self.orderingNumber_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public orderingNumber_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //agreesDeclar property
        _agreesDeclar: NpTypes.UINumberModel;
        public get agreesDeclar():number {
            return this._agreesDeclar.value;
        }
        public set agreesDeclar(agreesDeclar:number) {
            var self = this;
            self._agreesDeclar.value = agreesDeclar;
        //    self.agreesDeclar_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public agreesDeclar_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //comparisonOper property
        _comparisonOper: NpTypes.UINumberModel;
        public get comparisonOper():number {
            return this._comparisonOper.value;
        }
        public set comparisonOper(comparisonOper:number) {
            var self = this;
            self._comparisonOper.value = comparisonOper;
        //    self.comparisonOper_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public comparisonOper_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //probabThres property
        _probabThres: NpTypes.UINumberModel;
        public get probabThres():number {
            return this._probabThres.value;
        }
        public set probabThres(probabThres:number) {
            var self = this;
            self._probabThres.value = probabThres;
        //    self.probabThres_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public probabThres_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //decisionLight property
        _decisionLight: NpTypes.UINumberModel;
        public get decisionLight():number {
            return this._decisionLight.value;
        }
        public set decisionLight(decisionLight:number) {
            var self = this;
            self._decisionLight.value = decisionLight;
        //    self.decisionLight_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public decisionLight_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public rowVersion_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //agreesDeclar2 property
        _agreesDeclar2: NpTypes.UINumberModel;
        public get agreesDeclar2():number {
            return this._agreesDeclar2.value;
        }
        public set agreesDeclar2(agreesDeclar2:number) {
            var self = this;
            self._agreesDeclar2.value = agreesDeclar2;
        //    self.agreesDeclar2_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public agreesDeclar2_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //comparisonOper2 property
        _comparisonOper2: NpTypes.UINumberModel;
        public get comparisonOper2():number {
            return this._comparisonOper2.value;
        }
        public set comparisonOper2(comparisonOper2:number) {
            var self = this;
            self._comparisonOper2.value = comparisonOper2;
        //    self.comparisonOper2_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public comparisonOper2_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //probabThres2 property
        _probabThres2: NpTypes.UINumberModel;
        public get probabThres2():number {
            return this._probabThres2.value;
        }
        public set probabThres2(probabThres2:number) {
            var self = this;
            self._probabThres2.value = probabThres2;
        //    self.probabThres2_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public probabThres2_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //probabThresSum property
        _probabThresSum: NpTypes.UINumberModel;
        public get probabThresSum():number {
            return this._probabThresSum.value;
        }
        public set probabThresSum(probabThresSum:number) {
            var self = this;
            self._probabThresSum.value = probabThresSum;
        //    self.probabThresSum_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public probabThresSum_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //comparisonOper3 property
        _comparisonOper3: NpTypes.UINumberModel;
        public get comparisonOper3():number {
            return this._comparisonOper3.value;
        }
        public set comparisonOper3(comparisonOper3:number) {
            var self = this;
            self._comparisonOper3.value = comparisonOper3;
        //    self.comparisonOper3_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public comparisonOper3_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        //ecccId property
        _ecccId: NpTypes.UIManyToOneModel<Entities.EcCultCopy>;
        public get ecccId():EcCultCopy {
            return this._ecccId.value;
        }
        public set ecccId(ecccId:EcCultCopy) {
            var self = this;
            self._ecccId.value = ecccId;
        //    self.ecccId_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
        }
        //public ecccId_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.ecdcId_listeners.splice(0);
            self.orderingNumber_listeners.splice(0);
            self.agreesDeclar_listeners.splice(0);
            self.comparisonOper_listeners.splice(0);
            self.probabThres_listeners.splice(0);
            self.decisionLight_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.agreesDeclar2_listeners.splice(0);
            self.comparisonOper2_listeners.splice(0);
            self.probabThres2_listeners.splice(0);
            self.probabThresSum_listeners.splice(0);
            self.comparisonOper3_listeners.splice(0);
            self.ecccId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : EcCultDetailCopy {
            var key = "EcCultDetailCopy:"+x.ecdcId;
            var ret =  new EcCultDetailCopy(
                x.ecdcId,
                x.orderingNumber,
                x.agreesDeclar,
                x.comparisonOper,
                x.probabThres,
                x.decisionLight,
                x.rowVersion,
                x.agreesDeclar2,
                x.comparisonOper2,
                x.probabThres2,
                x.probabThresSum,
                x.comparisonOper3,
                x.ecccId
            );
            deserializedEntities[key] = ret;
            ret.ecccId = (x.ecccId !== undefined && x.ecccId !== null) ? <EcCultCopy>NpTypes.BaseEntity.entitiesFactory[x.ecccId.$entityName].fromJSONPartial(x.ecccId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): EcCultDetailCopy {
            
            return <EcCultDetailCopy>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : EcCultDetailCopy {
            var self = this;
            var key="";
            var ret:EcCultDetailCopy = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcCultDetailCopy:"+x.$refId;
                ret = <EcCultDetailCopy>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcCultDetailCopyBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcCultDetailCopy:"+x.ecdcId;
                    var cachedCopy = <EcCultDetailCopy>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcCultDetailCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcCultDetailCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = EcCultDetailCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcCultDetailCopy> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<EcCultDetailCopy> = _.map(data, (x:any):EcCultDetailCopy => {
                return EcCultDetailCopy.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:EcCultDetailCopy[])=>void)  {
            var url = "/Niva/rest/EcCultDetailCopy/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = EcCultDetailCopy.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.ecdcId = this.ecdcId;
                ret.orderingNumber = this.orderingNumber;
                ret.agreesDeclar = this.agreesDeclar;
                ret.comparisonOper = this.comparisonOper;
                ret.probabThres = this.probabThres;
                ret.decisionLight = this.decisionLight;
                ret.rowVersion = this.rowVersion;
                ret.agreesDeclar2 = this.agreesDeclar2;
                ret.comparisonOper2 = this.comparisonOper2;
                ret.probabThres2 = this.probabThres2;
                ret.probabThresSum = this.probabThresSum;
                ret.comparisonOper3 = this.comparisonOper3;
                ret.ecccId = 
                    (this.ecccId !== undefined && this.ecccId !== null) ? 
                        { ecccId :  this.ecccId.ecccId} :
                        (this.ecccId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:EcCultDetailCopy):void {
            var self = this;
            self.ecdcId = other.ecdcId
            self.orderingNumber = other.orderingNumber
            self.agreesDeclar = other.agreesDeclar
            self.comparisonOper = other.comparisonOper
            self.probabThres = other.probabThres
            self.decisionLight = other.decisionLight
            self.rowVersion = other.rowVersion
            self.agreesDeclar2 = other.agreesDeclar2
            self.comparisonOper2 = other.comparisonOper2
            self.probabThres2 = other.probabThres2
            self.probabThresSum = other.probabThresSum
            self.comparisonOper3 = other.comparisonOper3
            self.ecccId = other.ecccId
        }

        public static Create() : EcCultDetailCopy {
            return new EcCultDetailCopy(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(ecdcId:string) : EcCultDetailCopy {
            var ret = EcCultDetailCopy.Create();
            ret.ecdcId = ecdcId;
            return ret;
        }

        public static cashedEntities: { [id: string]: EcCultDetailCopy; } = {};
        public static findById_unecrypted(ecdcId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : EcCultDetailCopy {
            if (Entities.EcCultDetailCopy.cashedEntities[ecdcId.toString()] !== undefined)
                return Entities.EcCultDetailCopy.cashedEntities[ecdcId.toString()];

            var wsPath = "EcCultDetailCopy/findByEcdcId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['ecdcId'] = ecdcId;
            var ret = EcCultDetailCopy.Create();
            Entities.EcCultDetailCopy.cashedEntities[ecdcId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.EcCultDetailCopy.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + ecdcId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['EcCultDetailCopy'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.EcCultDetailCopy.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.EcCultDetailCopy.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
