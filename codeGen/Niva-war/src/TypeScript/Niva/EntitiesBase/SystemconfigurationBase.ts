/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Systemconfiguration.ts" />

module Entities {

    export class SystemconfigurationBase extends NpTypes.BaseEntity {
        constructor(
            agencyId: string,
            srid: number,
            language: string) 
        {
            super();
            var self = this;
            this._agencyId = new NpTypes.UIStringModel(agencyId, this);
            this._srid = new NpTypes.UINumberModel(srid, this);
            this._language = new NpTypes.UIStringModel(language, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.agencyId; 
        }
        
        public getKeyName(): string { 
            return "agencyId"; 
        }

        public getEntityName(): string { 
            return 'Systemconfiguration'; 
        }
        
        public fromJSON(data: any): Systemconfiguration[] { 
            return Systemconfiguration.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.agencyId === null || this.agencyId === undefined)
                return true;
            if (this.agencyId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: SystemconfigurationBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //agencyId property
        _agencyId: NpTypes.UIStringModel;
        public get agencyId():string {
            return this._agencyId.value;
        }
        public set agencyId(agencyId:string) {
            var self = this;
            self._agencyId.value = agencyId;
        //    self.agencyId_listeners.forEach(cb => { cb(<Systemconfiguration>self); });
        }
        //public agencyId_listeners: Array<(c:Systemconfiguration) => void> = [];
        //srid property
        _srid: NpTypes.UINumberModel;
        public get srid():number {
            return this._srid.value;
        }
        public set srid(srid:number) {
            var self = this;
            self._srid.value = srid;
        //    self.srid_listeners.forEach(cb => { cb(<Systemconfiguration>self); });
        }
        //public srid_listeners: Array<(c:Systemconfiguration) => void> = [];
        //language property
        _language: NpTypes.UIStringModel;
        public get language():string {
            return this._language.value;
        }
        public set language(language:string) {
            var self = this;
            self._language.value = language;
        //    self.language_listeners.forEach(cb => { cb(<Systemconfiguration>self); });
        }
        //public language_listeners: Array<(c:Systemconfiguration) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.agencyId_listeners.splice(0);
            self.srid_listeners.splice(0);
            self.language_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : Systemconfiguration {
            var key = "Systemconfiguration:"+x.agencyId;
            var ret =  new Systemconfiguration(
                x.agencyId,
                x.srid,
                x.language
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): Systemconfiguration {
            
            return <Systemconfiguration>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : Systemconfiguration {
            var self = this;
            var key="";
            var ret:Systemconfiguration = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Systemconfiguration:"+x.$refId;
                ret = <Systemconfiguration>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "SystemconfigurationBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Systemconfiguration:"+x.agencyId;
                    var cachedCopy = <Systemconfiguration>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Systemconfiguration.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Systemconfiguration.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Systemconfiguration.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Systemconfiguration> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<Systemconfiguration> = _.map(data, (x:any):Systemconfiguration => {
                return Systemconfiguration.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:Systemconfiguration[])=>void)  {
            var url = "/Niva/rest/Systemconfiguration/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = Systemconfiguration.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.agencyId = this.agencyId;
                ret.srid = this.srid;
                ret.language = this.language;
            return ret;
        }

        public updateInstance(other:Systemconfiguration):void {
            var self = this;
            self.agencyId = other.agencyId
            self.srid = other.srid
            self.language = other.language
        }

        public static Create() : Systemconfiguration {
            return new Systemconfiguration(
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(agencyId:string) : Systemconfiguration {
            var ret = Systemconfiguration.Create();
            ret.agencyId = agencyId;
            return ret;
        }

        public static cashedEntities: { [id: string]: Systemconfiguration; } = {};
        public static findById_unecrypted(agencyId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : Systemconfiguration {
            if (Entities.Systemconfiguration.cashedEntities[agencyId.toString()] !== undefined)
                return Entities.Systemconfiguration.cashedEntities[agencyId.toString()];

            var wsPath = "Systemconfiguration/findByAgencyId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['agencyId'] = agencyId;
            var ret = Systemconfiguration.Create();
            Entities.Systemconfiguration.cashedEntities[agencyId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.Systemconfiguration.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + agencyId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return 0;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['Systemconfiguration'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.Systemconfiguration.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.Systemconfiguration.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
