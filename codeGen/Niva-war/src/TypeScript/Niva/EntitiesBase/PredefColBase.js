/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/TemplateColumn.ts" />
/// <reference path="../Entities/PredefCol.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var PredefColBase = (function (_super) {
        __extends(PredefColBase, _super);
        function PredefColBase(prcoId, columnName, rowVersion, systemColumnName) {
            _super.call(this);
            var self = this;
            this._prcoId = new NpTypes.UIStringModel(prcoId, this);
            this._columnName = new NpTypes.UIStringModel(columnName, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._systemColumnName = new NpTypes.UIStringModel(systemColumnName, this);
            self.postConstruct();
        }
        PredefColBase.prototype.getKey = function () {
            return this.prcoId;
        };
        PredefColBase.prototype.getKeyName = function () {
            return "prcoId";
        };
        PredefColBase.prototype.getEntityName = function () {
            return 'PredefCol';
        };
        PredefColBase.prototype.fromJSON = function (data) {
            return Entities.PredefCol.fromJSONComplete(data);
        };
        Object.defineProperty(PredefColBase.prototype, "prcoId", {
            get: function () {
                return this._prcoId.value;
            },
            set: function (prcoId) {
                var self = this;
                self._prcoId.value = prcoId;
                //    self.prcoId_listeners.forEach(cb => { cb(<PredefCol>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PredefColBase.prototype, "columnName", {
            get: function () {
                return this._columnName.value;
            },
            set: function (columnName) {
                var self = this;
                self._columnName.value = columnName;
                //    self.columnName_listeners.forEach(cb => { cb(<PredefCol>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PredefColBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<PredefCol>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PredefColBase.prototype, "systemColumnName", {
            get: function () {
                return this._systemColumnName.value;
            },
            set: function (systemColumnName) {
                var self = this;
                self._systemColumnName.value = systemColumnName;
                //    self.systemColumnName_listeners.forEach(cb => { cb(<PredefCol>self); });
            },
            enumerable: true,
            configurable: true
        });
        PredefColBase.prototype.templateColumnCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.PredefCol.templateColumnCollection)) {
                if (self instanceof Entities.PredefCol) {
                    Entities.PredefCol.templateColumnCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.prcoId_listeners.splice(0);
            self.columnName_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.systemColumnName_listeners.splice(0);
        }
        */
        PredefColBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "PredefCol:" + x.prcoId;
            var ret = new Entities.PredefCol(x.prcoId, x.columnName, x.rowVersion, x.systemColumnName);
            deserializedEntities[key] = ret;
            return ret;
        };
        PredefColBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        PredefColBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "PredefCol:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "PredefColBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "PredefCol:"+x.prcoId;
                    var cachedCopy = <PredefCol>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = PredefCol.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = PredefCol.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.PredefCol.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        PredefColBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.PredefCol.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        PredefColBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/PredefCol/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.PredefCol.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        PredefColBase.prototype.toJSON = function () {
            var ret = {};
            ret.prcoId = this.prcoId;
            ret.columnName = this.columnName;
            ret.rowVersion = this.rowVersion;
            ret.systemColumnName = this.systemColumnName;
            return ret;
        };
        PredefColBase.prototype.updateInstance = function (other) {
            var self = this;
            self.prcoId = other.prcoId;
            self.columnName = other.columnName;
            self.rowVersion = other.rowVersion;
            self.systemColumnName = other.systemColumnName;
        };
        PredefColBase.Create = function () {
            return new Entities.PredefCol(undefined, undefined, undefined, undefined);
        };
        PredefColBase.CreateById = function (prcoId) {
            var ret = Entities.PredefCol.Create();
            ret.prcoId = prcoId;
            return ret;
        };
        PredefColBase.findById_unecrypted = function (prcoId, $scope, $http, errFunc) {
            if (Entities.PredefCol.cashedEntities[prcoId.toString()] !== undefined)
                return Entities.PredefCol.cashedEntities[prcoId.toString()];
            var wsPath = "PredefCol/findByPrcoId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['prcoId'] = prcoId;
            var ret = Entities.PredefCol.Create();
            Entities.PredefCol.cashedEntities[prcoId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.PredefCol.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + prcoId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        PredefColBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public systemColumnName_listeners: Array<(c:PredefCol) => void> = [];
        PredefColBase.templateColumnCollection = null; //(predefCol, list) => { };
        PredefColBase.cashedEntities = {};
        return PredefColBase;
    })(NpTypes.BaseEntity);
    Entities.PredefColBase = PredefColBase;
    NpTypes.BaseEntity.entitiesFactory['PredefCol'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.PredefCol.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.PredefCol.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=PredefColBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
