/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/FileDirPath.ts" />

module Entities {

    export class FileDirPathBase extends NpTypes.BaseEntity {
        constructor(
            fidrId: string,
            directory: string,
            rowVersion: number,
            fileType: number) 
        {
            super();
            var self = this;
            this._fidrId = new NpTypes.UIStringModel(fidrId, this);
            this._directory = new NpTypes.UIStringModel(directory, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._fileType = new NpTypes.UINumberModel(fileType, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.fidrId; 
        }
        
        public getKeyName(): string { 
            return "fidrId"; 
        }

        public getEntityName(): string { 
            return 'FileDirPath'; 
        }
        
        public fromJSON(data: any): FileDirPath[] { 
            return FileDirPath.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.fidrId === null || this.fidrId === undefined)
                return true;
            if (this.fidrId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: FileDirPathBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //fidrId property
        _fidrId: NpTypes.UIStringModel;
        public get fidrId():string {
            return this._fidrId.value;
        }
        public set fidrId(fidrId:string) {
            var self = this;
            self._fidrId.value = fidrId;
        //    self.fidrId_listeners.forEach(cb => { cb(<FileDirPath>self); });
        }
        //public fidrId_listeners: Array<(c:FileDirPath) => void> = [];
        //directory property
        _directory: NpTypes.UIStringModel;
        public get directory():string {
            return this._directory.value;
        }
        public set directory(directory:string) {
            var self = this;
            self._directory.value = directory;
        //    self.directory_listeners.forEach(cb => { cb(<FileDirPath>self); });
        }
        //public directory_listeners: Array<(c:FileDirPath) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<FileDirPath>self); });
        }
        //public rowVersion_listeners: Array<(c:FileDirPath) => void> = [];
        //fileType property
        _fileType: NpTypes.UINumberModel;
        public get fileType():number {
            return this._fileType.value;
        }
        public set fileType(fileType:number) {
            var self = this;
            self._fileType.value = fileType;
        //    self.fileType_listeners.forEach(cb => { cb(<FileDirPath>self); });
        }
        //public fileType_listeners: Array<(c:FileDirPath) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.fidrId_listeners.splice(0);
            self.directory_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.fileType_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : FileDirPath {
            var key = "FileDirPath:"+x.fidrId;
            var ret =  new FileDirPath(
                x.fidrId,
                x.directory,
                x.rowVersion,
                x.fileType
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): FileDirPath {
            
            return <FileDirPath>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : FileDirPath {
            var self = this;
            var key="";
            var ret:FileDirPath = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "FileDirPath:"+x.$refId;
                ret = <FileDirPath>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "FileDirPathBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "FileDirPath:"+x.fidrId;
                    var cachedCopy = <FileDirPath>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = FileDirPath.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = FileDirPath.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = FileDirPath.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FileDirPath> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<FileDirPath> = _.map(data, (x:any):FileDirPath => {
                return FileDirPath.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:FileDirPath[])=>void)  {
            var url = "/Niva/rest/FileDirPath/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = FileDirPath.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.fidrId = this.fidrId;
                ret.directory = this.directory;
                ret.rowVersion = this.rowVersion;
                ret.fileType = this.fileType;
            return ret;
        }

        public updateInstance(other:FileDirPath):void {
            var self = this;
            self.fidrId = other.fidrId
            self.directory = other.directory
            self.rowVersion = other.rowVersion
            self.fileType = other.fileType
        }

        public static Create() : FileDirPath {
            return new FileDirPath(
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(fidrId:string) : FileDirPath {
            var ret = FileDirPath.Create();
            ret.fidrId = fidrId;
            return ret;
        }

        public static cashedEntities: { [id: string]: FileDirPath; } = {};
        public static findById_unecrypted(fidrId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : FileDirPath {
            if (Entities.FileDirPath.cashedEntities[fidrId.toString()] !== undefined)
                return Entities.FileDirPath.cashedEntities[fidrId.toString()];

            var wsPath = "FileDirPath/findByFidrId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fidrId'] = fidrId;
            var ret = FileDirPath.Create();
            Entities.FileDirPath.cashedEntities[fidrId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.FileDirPath.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + fidrId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['FileDirPath'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.FileDirPath.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.FileDirPath.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
