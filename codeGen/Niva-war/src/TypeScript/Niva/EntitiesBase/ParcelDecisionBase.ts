/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ParcelDecision.ts" />

module Entities {

    export class ParcelDecisionBase extends NpTypes.BaseEntity {
        constructor(
            padeId: string,
            decisionLight: number,
            rowVersion: number,
            demaId: DecisionMaking,
            pclaId: ParcelClas) 
        {
            super();
            var self = this;
            this._padeId = new NpTypes.UIStringModel(padeId, this);
            this._decisionLight = new NpTypes.UINumberModel(decisionLight, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._demaId = new NpTypes.UIManyToOneModel<Entities.DecisionMaking>(demaId, this);
            this._pclaId = new NpTypes.UIManyToOneModel<Entities.ParcelClas>(pclaId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.padeId; 
        }
        
        public getKeyName(): string { 
            return "padeId"; 
        }

        public getEntityName(): string { 
            return 'ParcelDecision'; 
        }
        
        public fromJSON(data: any): ParcelDecision[] { 
            return ParcelDecision.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.padeId === null || this.padeId === undefined)
                return true;
            if (this.padeId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: ParcelDecisionBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //padeId property
        _padeId: NpTypes.UIStringModel;
        public get padeId():string {
            return this._padeId.value;
        }
        public set padeId(padeId:string) {
            var self = this;
            self._padeId.value = padeId;
        //    self.padeId_listeners.forEach(cb => { cb(<ParcelDecision>self); });
        }
        //public padeId_listeners: Array<(c:ParcelDecision) => void> = [];
        //decisionLight property
        _decisionLight: NpTypes.UINumberModel;
        public get decisionLight():number {
            return this._decisionLight.value;
        }
        public set decisionLight(decisionLight:number) {
            var self = this;
            self._decisionLight.value = decisionLight;
        //    self.decisionLight_listeners.forEach(cb => { cb(<ParcelDecision>self); });
        }
        //public decisionLight_listeners: Array<(c:ParcelDecision) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<ParcelDecision>self); });
        }
        //public rowVersion_listeners: Array<(c:ParcelDecision) => void> = [];
        //demaId property
        _demaId: NpTypes.UIManyToOneModel<Entities.DecisionMaking>;
        public get demaId():DecisionMaking {
            return this._demaId.value;
        }
        public set demaId(demaId:DecisionMaking) {
            var self = this;
            self._demaId.value = demaId;
        //    self.demaId_listeners.forEach(cb => { cb(<ParcelDecision>self); });
        }
        //public demaId_listeners: Array<(c:ParcelDecision) => void> = [];
        //pclaId property
        _pclaId: NpTypes.UIManyToOneModel<Entities.ParcelClas>;
        public get pclaId():ParcelClas {
            return this._pclaId.value;
        }
        public set pclaId(pclaId:ParcelClas) {
            var self = this;
            self._pclaId.value = pclaId;
        //    self.pclaId_listeners.forEach(cb => { cb(<ParcelDecision>self); });
        }
        //public pclaId_listeners: Array<(c:ParcelDecision) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.padeId_listeners.splice(0);
            self.decisionLight_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.demaId_listeners.splice(0);
            self.pclaId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : ParcelDecision {
            var key = "ParcelDecision:"+x.padeId;
            var ret =  new ParcelDecision(
                x.padeId,
                x.decisionLight,
                x.rowVersion,
                x.demaId,
                x.pclaId
            );
            deserializedEntities[key] = ret;
            ret.demaId = (x.demaId !== undefined && x.demaId !== null) ? <DecisionMaking>NpTypes.BaseEntity.entitiesFactory[x.demaId.$entityName].fromJSONPartial(x.demaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.pclaId = (x.pclaId !== undefined && x.pclaId !== null) ? <ParcelClas>NpTypes.BaseEntity.entitiesFactory[x.pclaId.$entityName].fromJSONPartial(x.pclaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): ParcelDecision {
            
            return <ParcelDecision>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : ParcelDecision {
            var self = this;
            var key="";
            var ret:ParcelDecision = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcelDecision:"+x.$refId;
                ret = <ParcelDecision>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcelDecisionBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcelDecision:"+x.padeId;
                    var cachedCopy = <ParcelDecision>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcelDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcelDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = ParcelDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelDecision> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<ParcelDecision> = _.map(data, (x:any):ParcelDecision => {
                return ParcelDecision.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:ParcelDecision[])=>void)  {
            var url = "/Niva/rest/ParcelDecision/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = ParcelDecision.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.padeId = this.padeId;
                ret.decisionLight = this.decisionLight;
                ret.rowVersion = this.rowVersion;
                ret.demaId = 
                    (this.demaId !== undefined && this.demaId !== null) ? 
                        { demaId :  this.demaId.demaId} :
                        (this.demaId === undefined ? undefined : null);
                ret.pclaId = 
                    (this.pclaId !== undefined && this.pclaId !== null) ? 
                        { pclaId :  this.pclaId.pclaId} :
                        (this.pclaId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:ParcelDecision):void {
            var self = this;
            self.padeId = other.padeId
            self.decisionLight = other.decisionLight
            self.rowVersion = other.rowVersion
            self.demaId = other.demaId
            self.pclaId = other.pclaId
        }

        public static Create() : ParcelDecision {
            return new ParcelDecision(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(padeId:string) : ParcelDecision {
            var ret = ParcelDecision.Create();
            ret.padeId = padeId;
            return ret;
        }

        public static cashedEntities: { [id: string]: ParcelDecision; } = {};
        public static findById_unecrypted(padeId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : ParcelDecision {
            if (Entities.ParcelDecision.cashedEntities[padeId.toString()] !== undefined)
                return Entities.ParcelDecision.cashedEntities[padeId.toString()];

            var wsPath = "ParcelDecision/findByPadeId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['padeId'] = padeId;
            var ret = ParcelDecision.Create();
            Entities.ParcelDecision.cashedEntities[padeId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.ParcelDecision.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + padeId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['ParcelDecision'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.ParcelDecision.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.ParcelDecision.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
