/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/TemplateColumn.ts" />
/// <reference path="../Entities/PredefCol.ts" />

module Entities {

    export class PredefColBase extends NpTypes.BaseEntity {
        constructor(
            prcoId: string,
            columnName: string,
            rowVersion: number,
            systemColumnName: string) 
        {
            super();
            var self = this;
            this._prcoId = new NpTypes.UIStringModel(prcoId, this);
            this._columnName = new NpTypes.UIStringModel(columnName, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._systemColumnName = new NpTypes.UIStringModel(systemColumnName, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.prcoId; 
        }
        
        public getKeyName(): string { 
            return "prcoId"; 
        }

        public getEntityName(): string { 
            return 'PredefCol'; 
        }
        
        public fromJSON(data: any): PredefCol[] { 
            return PredefCol.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.prcoId === null || this.prcoId === undefined)
                return true;
            if (this.prcoId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: PredefColBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //prcoId property
        _prcoId: NpTypes.UIStringModel;
        public get prcoId():string {
            return this._prcoId.value;
        }
        public set prcoId(prcoId:string) {
            var self = this;
            self._prcoId.value = prcoId;
        //    self.prcoId_listeners.forEach(cb => { cb(<PredefCol>self); });
        }
        //public prcoId_listeners: Array<(c:PredefCol) => void> = [];
        //columnName property
        _columnName: NpTypes.UIStringModel;
        public get columnName():string {
            return this._columnName.value;
        }
        public set columnName(columnName:string) {
            var self = this;
            self._columnName.value = columnName;
        //    self.columnName_listeners.forEach(cb => { cb(<PredefCol>self); });
        }
        //public columnName_listeners: Array<(c:PredefCol) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<PredefCol>self); });
        }
        //public rowVersion_listeners: Array<(c:PredefCol) => void> = [];
        //systemColumnName property
        _systemColumnName: NpTypes.UIStringModel;
        public get systemColumnName():string {
            return this._systemColumnName.value;
        }
        public set systemColumnName(systemColumnName:string) {
            var self = this;
            self._systemColumnName.value = systemColumnName;
        //    self.systemColumnName_listeners.forEach(cb => { cb(<PredefCol>self); });
        }
        //public systemColumnName_listeners: Array<(c:PredefCol) => void> = [];
        public static templateColumnCollection: (self:Entities.PredefCol, func: (templateColumnList: Entities.TemplateColumn[]) => void) => void = null; //(predefCol, list) => { };
        public templateColumnCollection(func: (templateColumnList: Entities.TemplateColumn[]) => void) {
            var self: Entities.PredefColBase = this;
        	if (!isVoid(PredefCol.templateColumnCollection)) {
        		if (self instanceof Entities.PredefCol) {
        			PredefCol.templateColumnCollection(<Entities.PredefCol>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.prcoId_listeners.splice(0);
            self.columnName_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.systemColumnName_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : PredefCol {
            var key = "PredefCol:"+x.prcoId;
            var ret =  new PredefCol(
                x.prcoId,
                x.columnName,
                x.rowVersion,
                x.systemColumnName
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): PredefCol {
            
            return <PredefCol>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : PredefCol {
            var self = this;
            var key="";
            var ret:PredefCol = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "PredefCol:"+x.$refId;
                ret = <PredefCol>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "PredefColBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "PredefCol:"+x.prcoId;
                    var cachedCopy = <PredefCol>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = PredefCol.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = PredefCol.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = PredefCol.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<PredefCol> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<PredefCol> = _.map(data, (x:any):PredefCol => {
                return PredefCol.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:PredefCol[])=>void)  {
            var url = "/Niva/rest/PredefCol/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = PredefCol.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.prcoId = this.prcoId;
                ret.columnName = this.columnName;
                ret.rowVersion = this.rowVersion;
                ret.systemColumnName = this.systemColumnName;
            return ret;
        }

        public updateInstance(other:PredefCol):void {
            var self = this;
            self.prcoId = other.prcoId
            self.columnName = other.columnName
            self.rowVersion = other.rowVersion
            self.systemColumnName = other.systemColumnName
        }

        public static Create() : PredefCol {
            return new PredefCol(
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(prcoId:string) : PredefCol {
            var ret = PredefCol.Create();
            ret.prcoId = prcoId;
            return ret;
        }

        public static cashedEntities: { [id: string]: PredefCol; } = {};
        public static findById_unecrypted(prcoId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : PredefCol {
            if (Entities.PredefCol.cashedEntities[prcoId.toString()] !== undefined)
                return Entities.PredefCol.cashedEntities[prcoId.toString()];

            var wsPath = "PredefCol/findByPrcoId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['prcoId'] = prcoId;
            var ret = PredefCol.Create();
            Entities.PredefCol.cashedEntities[prcoId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.PredefCol.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + prcoId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['PredefCol'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.PredefCol.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.PredefCol.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
