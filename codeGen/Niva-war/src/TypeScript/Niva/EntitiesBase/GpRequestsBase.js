/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/GpRequestsProducers.ts" />
/// <reference path="../Entities/AgrisnapUsers.ts" />
/// <reference path="../Entities/GpRequests.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var GpRequestsBase = (function (_super) {
        __extends(GpRequestsBase, _super);
        function GpRequestsBase(gpRequestsId, dteReceived, typeCode, typeDescription, scheme, correspondenceId, correspondenceDocNr, geotag, rowVersion, agrisnapUsersId) {
            _super.call(this);
            var self = this;
            this._gpRequestsId = new NpTypes.UIStringModel(gpRequestsId, this);
            this._dteReceived = new NpTypes.UIDateModel(dteReceived, this);
            this._typeCode = new NpTypes.UINumberModel(typeCode, this);
            this._typeDescription = new NpTypes.UIStringModel(typeDescription, this);
            this._scheme = new NpTypes.UIStringModel(scheme, this);
            this._correspondenceId = new NpTypes.UINumberModel(correspondenceId, this);
            this._correspondenceDocNr = new NpTypes.UINumberModel(correspondenceDocNr, this);
            this._geotag = new NpTypes.UIStringModel(geotag, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._agrisnapUsersId = new NpTypes.UIManyToOneModel(agrisnapUsersId, this);
            self.postConstruct();
        }
        GpRequestsBase.prototype.getKey = function () {
            return this.gpRequestsId;
        };
        GpRequestsBase.prototype.getKeyName = function () {
            return "gpRequestsId";
        };
        GpRequestsBase.prototype.getEntityName = function () {
            return 'GpRequests';
        };
        GpRequestsBase.prototype.fromJSON = function (data) {
            return Entities.GpRequests.fromJSONComplete(data);
        };
        Object.defineProperty(GpRequestsBase.prototype, "gpRequestsId", {
            get: function () {
                return this._gpRequestsId.value;
            },
            set: function (gpRequestsId) {
                var self = this;
                self._gpRequestsId.value = gpRequestsId;
                //    self.gpRequestsId_listeners.forEach(cb => { cb(<GpRequests>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsBase.prototype, "dteReceived", {
            get: function () {
                return this._dteReceived.value;
            },
            set: function (dteReceived) {
                var self = this;
                self._dteReceived.value = dteReceived;
                //    self.dteReceived_listeners.forEach(cb => { cb(<GpRequests>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsBase.prototype, "typeCode", {
            get: function () {
                return this._typeCode.value;
            },
            set: function (typeCode) {
                var self = this;
                self._typeCode.value = typeCode;
                //    self.typeCode_listeners.forEach(cb => { cb(<GpRequests>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsBase.prototype, "typeDescription", {
            get: function () {
                return this._typeDescription.value;
            },
            set: function (typeDescription) {
                var self = this;
                self._typeDescription.value = typeDescription;
                //    self.typeDescription_listeners.forEach(cb => { cb(<GpRequests>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsBase.prototype, "scheme", {
            get: function () {
                return this._scheme.value;
            },
            set: function (scheme) {
                var self = this;
                self._scheme.value = scheme;
                //    self.scheme_listeners.forEach(cb => { cb(<GpRequests>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsBase.prototype, "correspondenceId", {
            get: function () {
                return this._correspondenceId.value;
            },
            set: function (correspondenceId) {
                var self = this;
                self._correspondenceId.value = correspondenceId;
                //    self.correspondenceId_listeners.forEach(cb => { cb(<GpRequests>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsBase.prototype, "correspondenceDocNr", {
            get: function () {
                return this._correspondenceDocNr.value;
            },
            set: function (correspondenceDocNr) {
                var self = this;
                self._correspondenceDocNr.value = correspondenceDocNr;
                //    self.correspondenceDocNr_listeners.forEach(cb => { cb(<GpRequests>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsBase.prototype, "geotag", {
            get: function () {
                return this._geotag.value;
            },
            set: function (geotag) {
                var self = this;
                self._geotag.value = geotag;
                //    self.geotag_listeners.forEach(cb => { cb(<GpRequests>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<GpRequests>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsBase.prototype, "agrisnapUsersId", {
            get: function () {
                return this._agrisnapUsersId.value;
            },
            set: function (agrisnapUsersId) {
                var self = this;
                self._agrisnapUsersId.value = agrisnapUsersId;
                //    self.agrisnapUsersId_listeners.forEach(cb => { cb(<GpRequests>self); });
            },
            enumerable: true,
            configurable: true
        });
        GpRequestsBase.prototype.gpRequestsProducersCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.GpRequests.gpRequestsProducersCollection)) {
                if (self instanceof Entities.GpRequests) {
                    Entities.GpRequests.gpRequestsProducersCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.gpRequestsId_listeners.splice(0);
            self.dteReceived_listeners.splice(0);
            self.typeCode_listeners.splice(0);
            self.typeDescription_listeners.splice(0);
            self.scheme_listeners.splice(0);
            self.correspondenceId_listeners.splice(0);
            self.correspondenceDocNr_listeners.splice(0);
            self.geotag_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.agrisnapUsersId_listeners.splice(0);
        }
        */
        GpRequestsBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "GpRequests:" + x.gpRequestsId;
            var ret = new Entities.GpRequests(x.gpRequestsId, isVoid(x.dteReceived) ? null : new Date(x.dteReceived), x.typeCode, x.typeDescription, x.scheme, x.correspondenceId, x.correspondenceDocNr, x.geotag, x.rowVersion, x.agrisnapUsersId);
            deserializedEntities[key] = ret;
            ret.agrisnapUsersId = (x.agrisnapUsersId !== undefined && x.agrisnapUsersId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.agrisnapUsersId.$entityName].fromJSONPartial(x.agrisnapUsersId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        GpRequestsBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        GpRequestsBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "GpRequests:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "GpRequestsBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "GpRequests:"+x.gpRequestsId;
                    var cachedCopy = <GpRequests>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = GpRequests.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = GpRequests.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.GpRequests.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        GpRequestsBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.GpRequests.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        GpRequestsBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/GpRequests/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.GpRequests.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        GpRequestsBase.prototype.toJSON = function () {
            var ret = {};
            ret.gpRequestsId = this.gpRequestsId;
            ret.dteReceived = this.dteReceived;
            ret.typeCode = this.typeCode;
            ret.typeDescription = this.typeDescription;
            ret.scheme = this.scheme;
            ret.correspondenceId = this.correspondenceId;
            ret.correspondenceDocNr = this.correspondenceDocNr;
            ret.geotag = this.geotag;
            ret.rowVersion = this.rowVersion;
            ret.agrisnapUsersId =
                (this.agrisnapUsersId !== undefined && this.agrisnapUsersId !== null) ?
                    { agrisnapUsersId: this.agrisnapUsersId.agrisnapUsersId } :
                    (this.agrisnapUsersId === undefined ? undefined : null);
            return ret;
        };
        GpRequestsBase.prototype.updateInstance = function (other) {
            var self = this;
            self.gpRequestsId = other.gpRequestsId;
            self.dteReceived = other.dteReceived;
            self.typeCode = other.typeCode;
            self.typeDescription = other.typeDescription;
            self.scheme = other.scheme;
            self.correspondenceId = other.correspondenceId;
            self.correspondenceDocNr = other.correspondenceDocNr;
            self.geotag = other.geotag;
            self.rowVersion = other.rowVersion;
            self.agrisnapUsersId = other.agrisnapUsersId;
        };
        GpRequestsBase.Create = function () {
            return new Entities.GpRequests(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        GpRequestsBase.CreateById = function (gpRequestsId) {
            var ret = Entities.GpRequests.Create();
            ret.gpRequestsId = gpRequestsId;
            return ret;
        };
        GpRequestsBase.findById_unecrypted = function (gpRequestsId, $scope, $http, errFunc) {
            if (Entities.GpRequests.cashedEntities[gpRequestsId.toString()] !== undefined)
                return Entities.GpRequests.cashedEntities[gpRequestsId.toString()];
            var wsPath = "GpRequests/findByGpRequestsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['gpRequestsId'] = gpRequestsId;
            var ret = Entities.GpRequests.Create();
            Entities.GpRequests.cashedEntities[gpRequestsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.GpRequests.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + gpRequestsId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        GpRequestsBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public agrisnapUsersId_listeners: Array<(c:GpRequests) => void> = [];
        GpRequestsBase.gpRequestsProducersCollection = null; //(gpRequests, list) => { };
        GpRequestsBase.cashedEntities = {};
        return GpRequestsBase;
    })(NpTypes.BaseEntity);
    Entities.GpRequestsBase = GpRequestsBase;
    NpTypes.BaseEntity.entitiesFactory['GpRequests'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.GpRequests.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.GpRequests.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=GpRequestsBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
