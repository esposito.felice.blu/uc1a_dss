/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/Classifier.ts" />

module Entities {

    export class ClassifierBase extends NpTypes.BaseEntity {
        constructor(
            clfrId: string,
            name: string,
            description: string,
            rowVersion: number) 
        {
            super();
            var self = this;
            this._clfrId = new NpTypes.UIStringModel(clfrId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.clfrId; 
        }
        
        public getKeyName(): string { 
            return "clfrId"; 
        }

        public getEntityName(): string { 
            return 'Classifier'; 
        }
        
        public fromJSON(data: any): Classifier[] { 
            return Classifier.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.clfrId === null || this.clfrId === undefined)
                return true;
            if (this.clfrId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: ClassifierBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //clfrId property
        _clfrId: NpTypes.UIStringModel;
        public get clfrId():string {
            return this._clfrId.value;
        }
        public set clfrId(clfrId:string) {
            var self = this;
            self._clfrId.value = clfrId;
        //    self.clfrId_listeners.forEach(cb => { cb(<Classifier>self); });
        }
        //public clfrId_listeners: Array<(c:Classifier) => void> = [];
        //name property
        _name: NpTypes.UIStringModel;
        public get name():string {
            return this._name.value;
        }
        public set name(name:string) {
            var self = this;
            self._name.value = name;
        //    self.name_listeners.forEach(cb => { cb(<Classifier>self); });
        }
        //public name_listeners: Array<(c:Classifier) => void> = [];
        //description property
        _description: NpTypes.UIStringModel;
        public get description():string {
            return this._description.value;
        }
        public set description(description:string) {
            var self = this;
            self._description.value = description;
        //    self.description_listeners.forEach(cb => { cb(<Classifier>self); });
        }
        //public description_listeners: Array<(c:Classifier) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<Classifier>self); });
        }
        //public rowVersion_listeners: Array<(c:Classifier) => void> = [];
        public static classificationCollection: (self:Entities.Classifier, func: (classificationList: Entities.Classification[]) => void) => void = null; //(classifier, list) => { };
        public classificationCollection(func: (classificationList: Entities.Classification[]) => void) {
            var self: Entities.ClassifierBase = this;
        	if (!isVoid(Classifier.classificationCollection)) {
        		if (self instanceof Entities.Classifier) {
        			Classifier.classificationCollection(<Entities.Classifier>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.clfrId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.description_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : Classifier {
            var key = "Classifier:"+x.clfrId;
            var ret =  new Classifier(
                x.clfrId,
                x.name,
                x.description,
                x.rowVersion
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): Classifier {
            
            return <Classifier>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : Classifier {
            var self = this;
            var key="";
            var ret:Classifier = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Classifier:"+x.$refId;
                ret = <Classifier>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ClassifierBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Classifier:"+x.clfrId;
                    var cachedCopy = <Classifier>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Classifier.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Classifier.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Classifier.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Classifier> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<Classifier> = _.map(data, (x:any):Classifier => {
                return Classifier.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:Classifier[])=>void)  {
            var url = "/Niva/rest/Classifier/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = Classifier.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.clfrId = this.clfrId;
                ret.name = this.name;
                ret.description = this.description;
                ret.rowVersion = this.rowVersion;
            return ret;
        }

        public updateInstance(other:Classifier):void {
            var self = this;
            self.clfrId = other.clfrId
            self.name = other.name
            self.description = other.description
            self.rowVersion = other.rowVersion
        }

        public static Create() : Classifier {
            return new Classifier(
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(clfrId:string) : Classifier {
            var ret = Classifier.Create();
            ret.clfrId = clfrId;
            return ret;
        }

        public static cashedEntities: { [id: string]: Classifier; } = {};
        public static findById_unecrypted(clfrId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : Classifier {
            if (Entities.Classifier.cashedEntities[clfrId.toString()] !== undefined)
                return Entities.Classifier.cashedEntities[clfrId.toString()];

            var wsPath = "Classifier/findByClfrId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['clfrId'] = clfrId;
            var ret = Classifier.Create();
            Entities.Classifier.cashedEntities[clfrId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.Classifier.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + clfrId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['Classifier'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.Classifier.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.Classifier.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
