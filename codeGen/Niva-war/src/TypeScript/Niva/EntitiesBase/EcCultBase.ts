/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCultDetail.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/EcGroup.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/SuperClas.ts" />
/// <reference path="../Entities/EcCult.ts" />

module Entities {

    export class EcCultBase extends NpTypes.BaseEntity {
        constructor(
            eccuId: string,
            noneMatchDecision: number,
            rowVersion: number,
            cultId: Cultivation,
            ecgrId: EcGroup,
            cotyId: CoverType,
            sucaId: SuperClas) 
        {
            super();
            var self = this;
            this._eccuId = new NpTypes.UIStringModel(eccuId, this);
            this._noneMatchDecision = new NpTypes.UINumberModel(noneMatchDecision, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._cultId = new NpTypes.UIManyToOneModel<Entities.Cultivation>(cultId, this);
            this._ecgrId = new NpTypes.UIManyToOneModel<Entities.EcGroup>(ecgrId, this);
            this._cotyId = new NpTypes.UIManyToOneModel<Entities.CoverType>(cotyId, this);
            this._sucaId = new NpTypes.UIManyToOneModel<Entities.SuperClas>(sucaId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.eccuId; 
        }
        
        public getKeyName(): string { 
            return "eccuId"; 
        }

        public getEntityName(): string { 
            return 'EcCult'; 
        }
        
        public fromJSON(data: any): EcCult[] { 
            return EcCult.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.eccuId === null || this.eccuId === undefined)
                return true;
            if (this.eccuId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: EcCultBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //eccuId property
        _eccuId: NpTypes.UIStringModel;
        public get eccuId():string {
            return this._eccuId.value;
        }
        public set eccuId(eccuId:string) {
            var self = this;
            self._eccuId.value = eccuId;
        //    self.eccuId_listeners.forEach(cb => { cb(<EcCult>self); });
        }
        //public eccuId_listeners: Array<(c:EcCult) => void> = [];
        //noneMatchDecision property
        _noneMatchDecision: NpTypes.UINumberModel;
        public get noneMatchDecision():number {
            return this._noneMatchDecision.value;
        }
        public set noneMatchDecision(noneMatchDecision:number) {
            var self = this;
            self._noneMatchDecision.value = noneMatchDecision;
        //    self.noneMatchDecision_listeners.forEach(cb => { cb(<EcCult>self); });
        }
        //public noneMatchDecision_listeners: Array<(c:EcCult) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<EcCult>self); });
        }
        //public rowVersion_listeners: Array<(c:EcCult) => void> = [];
        //cultId property
        _cultId: NpTypes.UIManyToOneModel<Entities.Cultivation>;
        public get cultId():Cultivation {
            return this._cultId.value;
        }
        public set cultId(cultId:Cultivation) {
            var self = this;
            self._cultId.value = cultId;
        //    self.cultId_listeners.forEach(cb => { cb(<EcCult>self); });
        }
        //public cultId_listeners: Array<(c:EcCult) => void> = [];
        //ecgrId property
        _ecgrId: NpTypes.UIManyToOneModel<Entities.EcGroup>;
        public get ecgrId():EcGroup {
            return this._ecgrId.value;
        }
        public set ecgrId(ecgrId:EcGroup) {
            var self = this;
            self._ecgrId.value = ecgrId;
        //    self.ecgrId_listeners.forEach(cb => { cb(<EcCult>self); });
        }
        //public ecgrId_listeners: Array<(c:EcCult) => void> = [];
        //cotyId property
        _cotyId: NpTypes.UIManyToOneModel<Entities.CoverType>;
        public get cotyId():CoverType {
            return this._cotyId.value;
        }
        public set cotyId(cotyId:CoverType) {
            var self = this;
            self._cotyId.value = cotyId;
        //    self.cotyId_listeners.forEach(cb => { cb(<EcCult>self); });
        }
        //public cotyId_listeners: Array<(c:EcCult) => void> = [];
        //sucaId property
        _sucaId: NpTypes.UIManyToOneModel<Entities.SuperClas>;
        public get sucaId():SuperClas {
            return this._sucaId.value;
        }
        public set sucaId(sucaId:SuperClas) {
            var self = this;
            self._sucaId.value = sucaId;
        //    self.sucaId_listeners.forEach(cb => { cb(<EcCult>self); });
        }
        //public sucaId_listeners: Array<(c:EcCult) => void> = [];
        public static ecCultDetailCollection: (self:Entities.EcCult, func: (ecCultDetailList: Entities.EcCultDetail[]) => void) => void = null; //(ecCult, list) => { };
        public ecCultDetailCollection(func: (ecCultDetailList: Entities.EcCultDetail[]) => void) {
            var self: Entities.EcCultBase = this;
        	if (!isVoid(EcCult.ecCultDetailCollection)) {
        		if (self instanceof Entities.EcCult) {
        			EcCult.ecCultDetailCollection(<Entities.EcCult>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.eccuId_listeners.splice(0);
            self.noneMatchDecision_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.cultId_listeners.splice(0);
            self.ecgrId_listeners.splice(0);
            self.cotyId_listeners.splice(0);
            self.sucaId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : EcCult {
            var key = "EcCult:"+x.eccuId;
            var ret =  new EcCult(
                x.eccuId,
                x.noneMatchDecision,
                x.rowVersion,
                x.cultId,
                x.ecgrId,
                x.cotyId,
                x.sucaId
            );
            deserializedEntities[key] = ret;
            ret.cultId = (x.cultId !== undefined && x.cultId !== null) ? <Cultivation>NpTypes.BaseEntity.entitiesFactory[x.cultId.$entityName].fromJSONPartial(x.cultId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.ecgrId = (x.ecgrId !== undefined && x.ecgrId !== null) ? <EcGroup>NpTypes.BaseEntity.entitiesFactory[x.ecgrId.$entityName].fromJSONPartial(x.ecgrId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.cotyId = (x.cotyId !== undefined && x.cotyId !== null) ? <CoverType>NpTypes.BaseEntity.entitiesFactory[x.cotyId.$entityName].fromJSONPartial(x.cotyId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.sucaId = (x.sucaId !== undefined && x.sucaId !== null) ? <SuperClas>NpTypes.BaseEntity.entitiesFactory[x.sucaId.$entityName].fromJSONPartial(x.sucaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): EcCult {
            
            return <EcCult>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : EcCult {
            var self = this;
            var key="";
            var ret:EcCult = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcCult:"+x.$refId;
                ret = <EcCult>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcCultBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcCult:"+x.eccuId;
                    var cachedCopy = <EcCult>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcCult.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcCult.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = EcCult.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcCult> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<EcCult> = _.map(data, (x:any):EcCult => {
                return EcCult.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:EcCult[])=>void)  {
            var url = "/Niva/rest/EcCult/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = EcCult.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.eccuId = this.eccuId;
                ret.noneMatchDecision = this.noneMatchDecision;
                ret.rowVersion = this.rowVersion;
                ret.cultId = 
                    (this.cultId !== undefined && this.cultId !== null) ? 
                        { cultId :  this.cultId.cultId} :
                        (this.cultId === undefined ? undefined : null);
                ret.ecgrId = 
                    (this.ecgrId !== undefined && this.ecgrId !== null) ? 
                        { ecgrId :  this.ecgrId.ecgrId} :
                        (this.ecgrId === undefined ? undefined : null);
                ret.cotyId = 
                    (this.cotyId !== undefined && this.cotyId !== null) ? 
                        { cotyId :  this.cotyId.cotyId} :
                        (this.cotyId === undefined ? undefined : null);
                ret.sucaId = 
                    (this.sucaId !== undefined && this.sucaId !== null) ? 
                        { sucaId :  this.sucaId.sucaId} :
                        (this.sucaId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:EcCult):void {
            var self = this;
            self.eccuId = other.eccuId
            self.noneMatchDecision = other.noneMatchDecision
            self.rowVersion = other.rowVersion
            self.cultId = other.cultId
            self.ecgrId = other.ecgrId
            self.cotyId = other.cotyId
            self.sucaId = other.sucaId
        }

        public static Create() : EcCult {
            return new EcCult(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(eccuId:string) : EcCult {
            var ret = EcCult.Create();
            ret.eccuId = eccuId;
            return ret;
        }

        public static cashedEntities: { [id: string]: EcCult; } = {};
        public static findById_unecrypted(eccuId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : EcCult {
            if (Entities.EcCult.cashedEntities[eccuId.toString()] !== undefined)
                return Entities.EcCult.cashedEntities[eccuId.toString()];

            var wsPath = "EcCult/findByEccuId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['eccuId'] = eccuId;
            var ret = EcCult.Create();
            Entities.EcCult.cashedEntities[eccuId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.EcCult.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + eccuId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['EcCult'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.EcCult.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.EcCult.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
