/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/GpUpload.ts" />
/// <reference path="../Entities/GpRequestsProducers.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/GpRequestsContexts.ts" />

module Entities {

    export class GpRequestsContextsBase extends NpTypes.BaseEntity {
        constructor(
            gpRequestsContextsId: string,
            type: string,
            label: string,
            comment: string,
            geomHexewkb: string,
            referencepoint: string,
            rowVersion: number,
            hash: string,
            gpRequestsProducersId: GpRequestsProducers,
            parcelsIssuesId: ParcelsIssues) 
        {
            super();
            var self = this;
            this._gpRequestsContextsId = new NpTypes.UIStringModel(gpRequestsContextsId, this);
            this._type = new NpTypes.UIStringModel(type, this);
            this._label = new NpTypes.UIStringModel(label, this);
            this._comment = new NpTypes.UIStringModel(comment, this);
            this._geomHexewkb = new NpTypes.UIStringModel(geomHexewkb, this);
            this._referencepoint = new NpTypes.UIStringModel(referencepoint, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._hash = new NpTypes.UIStringModel(hash, this);
            this._gpRequestsProducersId = new NpTypes.UIManyToOneModel<Entities.GpRequestsProducers>(gpRequestsProducersId, this);
            this._parcelsIssuesId = new NpTypes.UIManyToOneModel<Entities.ParcelsIssues>(parcelsIssuesId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.gpRequestsContextsId; 
        }
        
        public getKeyName(): string { 
            return "gpRequestsContextsId"; 
        }

        public getEntityName(): string { 
            return 'GpRequestsContexts'; 
        }
        
        public fromJSON(data: any): GpRequestsContexts[] { 
            return GpRequestsContexts.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.gpRequestsContextsId === null || this.gpRequestsContextsId === undefined)
                return true;
            if (this.gpRequestsContextsId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: GpRequestsContextsBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //gpRequestsContextsId property
        _gpRequestsContextsId: NpTypes.UIStringModel;
        public get gpRequestsContextsId():string {
            return this._gpRequestsContextsId.value;
        }
        public set gpRequestsContextsId(gpRequestsContextsId:string) {
            var self = this;
            self._gpRequestsContextsId.value = gpRequestsContextsId;
        //    self.gpRequestsContextsId_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
        }
        //public gpRequestsContextsId_listeners: Array<(c:GpRequestsContexts) => void> = [];
        //type property
        _type: NpTypes.UIStringModel;
        public get type():string {
            return this._type.value;
        }
        public set type(type:string) {
            var self = this;
            self._type.value = type;
        //    self.type_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
        }
        //public type_listeners: Array<(c:GpRequestsContexts) => void> = [];
        //label property
        _label: NpTypes.UIStringModel;
        public get label():string {
            return this._label.value;
        }
        public set label(label:string) {
            var self = this;
            self._label.value = label;
        //    self.label_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
        }
        //public label_listeners: Array<(c:GpRequestsContexts) => void> = [];
        //comment property
        _comment: NpTypes.UIStringModel;
        public get comment():string {
            return this._comment.value;
        }
        public set comment(comment:string) {
            var self = this;
            self._comment.value = comment;
        //    self.comment_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
        }
        //public comment_listeners: Array<(c:GpRequestsContexts) => void> = [];
        //geomHexewkb property
        _geomHexewkb: NpTypes.UIStringModel;
        public get geomHexewkb():string {
            return this._geomHexewkb.value;
        }
        public set geomHexewkb(geomHexewkb:string) {
            var self = this;
            self._geomHexewkb.value = geomHexewkb;
        //    self.geomHexewkb_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
        }
        //public geomHexewkb_listeners: Array<(c:GpRequestsContexts) => void> = [];
        //referencepoint property
        _referencepoint: NpTypes.UIStringModel;
        public get referencepoint():string {
            return this._referencepoint.value;
        }
        public set referencepoint(referencepoint:string) {
            var self = this;
            self._referencepoint.value = referencepoint;
        //    self.referencepoint_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
        }
        //public referencepoint_listeners: Array<(c:GpRequestsContexts) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
        }
        //public rowVersion_listeners: Array<(c:GpRequestsContexts) => void> = [];
        //hash property
        _hash: NpTypes.UIStringModel;
        public get hash():string {
            return this._hash.value;
        }
        public set hash(hash:string) {
            var self = this;
            self._hash.value = hash;
        //    self.hash_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
        }
        //public hash_listeners: Array<(c:GpRequestsContexts) => void> = [];
        //gpRequestsProducersId property
        _gpRequestsProducersId: NpTypes.UIManyToOneModel<Entities.GpRequestsProducers>;
        public get gpRequestsProducersId():GpRequestsProducers {
            return this._gpRequestsProducersId.value;
        }
        public set gpRequestsProducersId(gpRequestsProducersId:GpRequestsProducers) {
            var self = this;
            self._gpRequestsProducersId.value = gpRequestsProducersId;
        //    self.gpRequestsProducersId_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
        }
        //public gpRequestsProducersId_listeners: Array<(c:GpRequestsContexts) => void> = [];
        //parcelsIssuesId property
        _parcelsIssuesId: NpTypes.UIManyToOneModel<Entities.ParcelsIssues>;
        public get parcelsIssuesId():ParcelsIssues {
            return this._parcelsIssuesId.value;
        }
        public set parcelsIssuesId(parcelsIssuesId:ParcelsIssues) {
            var self = this;
            self._parcelsIssuesId.value = parcelsIssuesId;
        //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
        }
        //public parcelsIssuesId_listeners: Array<(c:GpRequestsContexts) => void> = [];
        public static gpUploadCollection: (self:Entities.GpRequestsContexts, func: (gpUploadList: Entities.GpUpload[]) => void) => void = null; //(gpRequestsContexts, list) => { };
        public gpUploadCollection(func: (gpUploadList: Entities.GpUpload[]) => void) {
            var self: Entities.GpRequestsContextsBase = this;
        	if (!isVoid(GpRequestsContexts.gpUploadCollection)) {
        		if (self instanceof Entities.GpRequestsContexts) {
        			GpRequestsContexts.gpUploadCollection(<Entities.GpRequestsContexts>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.gpRequestsContextsId_listeners.splice(0);
            self.type_listeners.splice(0);
            self.label_listeners.splice(0);
            self.comment_listeners.splice(0);
            self.geomHexewkb_listeners.splice(0);
            self.referencepoint_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.hash_listeners.splice(0);
            self.gpRequestsProducersId_listeners.splice(0);
            self.parcelsIssuesId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : GpRequestsContexts {
            var key = "GpRequestsContexts:"+x.gpRequestsContextsId;
            var ret =  new GpRequestsContexts(
                x.gpRequestsContextsId,
                x.type,
                x.label,
                x.comment,
                x.geomHexewkb,
                x.referencepoint,
                x.rowVersion,
                x.hash,
                x.gpRequestsProducersId,
                x.parcelsIssuesId
            );
            deserializedEntities[key] = ret;
            ret.gpRequestsProducersId = (x.gpRequestsProducersId !== undefined && x.gpRequestsProducersId !== null) ? <GpRequestsProducers>NpTypes.BaseEntity.entitiesFactory[x.gpRequestsProducersId.$entityName].fromJSONPartial(x.gpRequestsProducersId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.parcelsIssuesId = (x.parcelsIssuesId !== undefined && x.parcelsIssuesId !== null) ? <ParcelsIssues>NpTypes.BaseEntity.entitiesFactory[x.parcelsIssuesId.$entityName].fromJSONPartial(x.parcelsIssuesId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): GpRequestsContexts {
            
            return <GpRequestsContexts>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : GpRequestsContexts {
            var self = this;
            var key="";
            var ret:GpRequestsContexts = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "GpRequestsContexts:"+x.$refId;
                ret = <GpRequestsContexts>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "GpRequestsContextsBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "GpRequestsContexts:"+x.gpRequestsContextsId;
                    var cachedCopy = <GpRequestsContexts>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = GpRequestsContexts.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = GpRequestsContexts.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = GpRequestsContexts.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpRequestsContexts> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<GpRequestsContexts> = _.map(data, (x:any):GpRequestsContexts => {
                return GpRequestsContexts.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:GpRequestsContexts[])=>void)  {
            var url = "/Niva/rest/GpRequestsContexts/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = GpRequestsContexts.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.gpRequestsContextsId = this.gpRequestsContextsId;
                ret.type = this.type;
                ret.label = this.label;
                ret.comment = this.comment;
                ret.geomHexewkb = this.geomHexewkb;
                ret.referencepoint = this.referencepoint;
                ret.rowVersion = this.rowVersion;
                ret.hash = this.hash;
                ret.gpRequestsProducersId = 
                    (this.gpRequestsProducersId !== undefined && this.gpRequestsProducersId !== null) ? 
                        { gpRequestsProducersId :  this.gpRequestsProducersId.gpRequestsProducersId} :
                        (this.gpRequestsProducersId === undefined ? undefined : null);
                ret.parcelsIssuesId = 
                    (this.parcelsIssuesId !== undefined && this.parcelsIssuesId !== null) ? 
                        { parcelsIssuesId :  this.parcelsIssuesId.parcelsIssuesId} :
                        (this.parcelsIssuesId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:GpRequestsContexts):void {
            var self = this;
            self.gpRequestsContextsId = other.gpRequestsContextsId
            self.type = other.type
            self.label = other.label
            self.comment = other.comment
            self.geomHexewkb = other.geomHexewkb
            self.referencepoint = other.referencepoint
            self.rowVersion = other.rowVersion
            self.hash = other.hash
            self.gpRequestsProducersId = other.gpRequestsProducersId
            self.parcelsIssuesId = other.parcelsIssuesId
        }

        public static Create() : GpRequestsContexts {
            return new GpRequestsContexts(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(gpRequestsContextsId:string) : GpRequestsContexts {
            var ret = GpRequestsContexts.Create();
            ret.gpRequestsContextsId = gpRequestsContextsId;
            return ret;
        }

        public static cashedEntities: { [id: string]: GpRequestsContexts; } = {};
        public static findById_unecrypted(gpRequestsContextsId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : GpRequestsContexts {
            if (Entities.GpRequestsContexts.cashedEntities[gpRequestsContextsId.toString()] !== undefined)
                return Entities.GpRequestsContexts.cashedEntities[gpRequestsContextsId.toString()];

            var wsPath = "GpRequestsContexts/findByGpRequestsContextsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['gpRequestsContextsId'] = gpRequestsContextsId;
            var ret = GpRequestsContexts.Create();
            Entities.GpRequestsContexts.cashedEntities[gpRequestsContextsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.GpRequestsContexts.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + gpRequestsContextsId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['GpRequestsContexts'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.GpRequestsContexts.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.GpRequestsContexts.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
