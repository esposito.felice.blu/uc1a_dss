/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/GpRequestsProducers.ts" />
/// <reference path="../Entities/AgrisnapUsers.ts" />
/// <reference path="../Entities/GpRequests.ts" />

module Entities {

    export class GpRequestsBase extends NpTypes.BaseEntity {
        constructor(
            gpRequestsId: string,
            dteReceived: Date,
            typeCode: number,
            typeDescription: string,
            scheme: string,
            correspondenceId: number,
            correspondenceDocNr: number,
            geotag: string,
            rowVersion: number,
            agrisnapUsersId: AgrisnapUsers) 
        {
            super();
            var self = this;
            this._gpRequestsId = new NpTypes.UIStringModel(gpRequestsId, this);
            this._dteReceived = new NpTypes.UIDateModel(dteReceived, this);
            this._typeCode = new NpTypes.UINumberModel(typeCode, this);
            this._typeDescription = new NpTypes.UIStringModel(typeDescription, this);
            this._scheme = new NpTypes.UIStringModel(scheme, this);
            this._correspondenceId = new NpTypes.UINumberModel(correspondenceId, this);
            this._correspondenceDocNr = new NpTypes.UINumberModel(correspondenceDocNr, this);
            this._geotag = new NpTypes.UIStringModel(geotag, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._agrisnapUsersId = new NpTypes.UIManyToOneModel<Entities.AgrisnapUsers>(agrisnapUsersId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.gpRequestsId; 
        }
        
        public getKeyName(): string { 
            return "gpRequestsId"; 
        }

        public getEntityName(): string { 
            return 'GpRequests'; 
        }
        
        public fromJSON(data: any): GpRequests[] { 
            return GpRequests.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.gpRequestsId === null || this.gpRequestsId === undefined)
                return true;
            if (this.gpRequestsId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: GpRequestsBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //gpRequestsId property
        _gpRequestsId: NpTypes.UIStringModel;
        public get gpRequestsId():string {
            return this._gpRequestsId.value;
        }
        public set gpRequestsId(gpRequestsId:string) {
            var self = this;
            self._gpRequestsId.value = gpRequestsId;
        //    self.gpRequestsId_listeners.forEach(cb => { cb(<GpRequests>self); });
        }
        //public gpRequestsId_listeners: Array<(c:GpRequests) => void> = [];
        //dteReceived property
        _dteReceived: NpTypes.UIDateModel;
        public get dteReceived():Date {
            return this._dteReceived.value;
        }
        public set dteReceived(dteReceived:Date) {
            var self = this;
            self._dteReceived.value = dteReceived;
        //    self.dteReceived_listeners.forEach(cb => { cb(<GpRequests>self); });
        }
        //public dteReceived_listeners: Array<(c:GpRequests) => void> = [];
        //typeCode property
        _typeCode: NpTypes.UINumberModel;
        public get typeCode():number {
            return this._typeCode.value;
        }
        public set typeCode(typeCode:number) {
            var self = this;
            self._typeCode.value = typeCode;
        //    self.typeCode_listeners.forEach(cb => { cb(<GpRequests>self); });
        }
        //public typeCode_listeners: Array<(c:GpRequests) => void> = [];
        //typeDescription property
        _typeDescription: NpTypes.UIStringModel;
        public get typeDescription():string {
            return this._typeDescription.value;
        }
        public set typeDescription(typeDescription:string) {
            var self = this;
            self._typeDescription.value = typeDescription;
        //    self.typeDescription_listeners.forEach(cb => { cb(<GpRequests>self); });
        }
        //public typeDescription_listeners: Array<(c:GpRequests) => void> = [];
        //scheme property
        _scheme: NpTypes.UIStringModel;
        public get scheme():string {
            return this._scheme.value;
        }
        public set scheme(scheme:string) {
            var self = this;
            self._scheme.value = scheme;
        //    self.scheme_listeners.forEach(cb => { cb(<GpRequests>self); });
        }
        //public scheme_listeners: Array<(c:GpRequests) => void> = [];
        //correspondenceId property
        _correspondenceId: NpTypes.UINumberModel;
        public get correspondenceId():number {
            return this._correspondenceId.value;
        }
        public set correspondenceId(correspondenceId:number) {
            var self = this;
            self._correspondenceId.value = correspondenceId;
        //    self.correspondenceId_listeners.forEach(cb => { cb(<GpRequests>self); });
        }
        //public correspondenceId_listeners: Array<(c:GpRequests) => void> = [];
        //correspondenceDocNr property
        _correspondenceDocNr: NpTypes.UINumberModel;
        public get correspondenceDocNr():number {
            return this._correspondenceDocNr.value;
        }
        public set correspondenceDocNr(correspondenceDocNr:number) {
            var self = this;
            self._correspondenceDocNr.value = correspondenceDocNr;
        //    self.correspondenceDocNr_listeners.forEach(cb => { cb(<GpRequests>self); });
        }
        //public correspondenceDocNr_listeners: Array<(c:GpRequests) => void> = [];
        //geotag property
        _geotag: NpTypes.UIStringModel;
        public get geotag():string {
            return this._geotag.value;
        }
        public set geotag(geotag:string) {
            var self = this;
            self._geotag.value = geotag;
        //    self.geotag_listeners.forEach(cb => { cb(<GpRequests>self); });
        }
        //public geotag_listeners: Array<(c:GpRequests) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<GpRequests>self); });
        }
        //public rowVersion_listeners: Array<(c:GpRequests) => void> = [];
        //agrisnapUsersId property
        _agrisnapUsersId: NpTypes.UIManyToOneModel<Entities.AgrisnapUsers>;
        public get agrisnapUsersId():AgrisnapUsers {
            return this._agrisnapUsersId.value;
        }
        public set agrisnapUsersId(agrisnapUsersId:AgrisnapUsers) {
            var self = this;
            self._agrisnapUsersId.value = agrisnapUsersId;
        //    self.agrisnapUsersId_listeners.forEach(cb => { cb(<GpRequests>self); });
        }
        //public agrisnapUsersId_listeners: Array<(c:GpRequests) => void> = [];
        public static gpRequestsProducersCollection: (self:Entities.GpRequests, func: (gpRequestsProducersList: Entities.GpRequestsProducers[]) => void) => void = null; //(gpRequests, list) => { };
        public gpRequestsProducersCollection(func: (gpRequestsProducersList: Entities.GpRequestsProducers[]) => void) {
            var self: Entities.GpRequestsBase = this;
        	if (!isVoid(GpRequests.gpRequestsProducersCollection)) {
        		if (self instanceof Entities.GpRequests) {
        			GpRequests.gpRequestsProducersCollection(<Entities.GpRequests>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.gpRequestsId_listeners.splice(0);
            self.dteReceived_listeners.splice(0);
            self.typeCode_listeners.splice(0);
            self.typeDescription_listeners.splice(0);
            self.scheme_listeners.splice(0);
            self.correspondenceId_listeners.splice(0);
            self.correspondenceDocNr_listeners.splice(0);
            self.geotag_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.agrisnapUsersId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : GpRequests {
            var key = "GpRequests:"+x.gpRequestsId;
            var ret =  new GpRequests(
                x.gpRequestsId,
                isVoid(x.dteReceived) ? null : new Date(x.dteReceived),
                x.typeCode,
                x.typeDescription,
                x.scheme,
                x.correspondenceId,
                x.correspondenceDocNr,
                x.geotag,
                x.rowVersion,
                x.agrisnapUsersId
            );
            deserializedEntities[key] = ret;
            ret.agrisnapUsersId = (x.agrisnapUsersId !== undefined && x.agrisnapUsersId !== null) ? <AgrisnapUsers>NpTypes.BaseEntity.entitiesFactory[x.agrisnapUsersId.$entityName].fromJSONPartial(x.agrisnapUsersId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): GpRequests {
            
            return <GpRequests>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : GpRequests {
            var self = this;
            var key="";
            var ret:GpRequests = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "GpRequests:"+x.$refId;
                ret = <GpRequests>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "GpRequestsBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "GpRequests:"+x.gpRequestsId;
                    var cachedCopy = <GpRequests>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = GpRequests.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = GpRequests.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = GpRequests.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpRequests> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<GpRequests> = _.map(data, (x:any):GpRequests => {
                return GpRequests.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:GpRequests[])=>void)  {
            var url = "/Niva/rest/GpRequests/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = GpRequests.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.gpRequestsId = this.gpRequestsId;
                ret.dteReceived = this.dteReceived;
                ret.typeCode = this.typeCode;
                ret.typeDescription = this.typeDescription;
                ret.scheme = this.scheme;
                ret.correspondenceId = this.correspondenceId;
                ret.correspondenceDocNr = this.correspondenceDocNr;
                ret.geotag = this.geotag;
                ret.rowVersion = this.rowVersion;
                ret.agrisnapUsersId = 
                    (this.agrisnapUsersId !== undefined && this.agrisnapUsersId !== null) ? 
                        { agrisnapUsersId :  this.agrisnapUsersId.agrisnapUsersId} :
                        (this.agrisnapUsersId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:GpRequests):void {
            var self = this;
            self.gpRequestsId = other.gpRequestsId
            self.dteReceived = other.dteReceived
            self.typeCode = other.typeCode
            self.typeDescription = other.typeDescription
            self.scheme = other.scheme
            self.correspondenceId = other.correspondenceId
            self.correspondenceDocNr = other.correspondenceDocNr
            self.geotag = other.geotag
            self.rowVersion = other.rowVersion
            self.agrisnapUsersId = other.agrisnapUsersId
        }

        public static Create() : GpRequests {
            return new GpRequests(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(gpRequestsId:string) : GpRequests {
            var ret = GpRequests.Create();
            ret.gpRequestsId = gpRequestsId;
            return ret;
        }

        public static cashedEntities: { [id: string]: GpRequests; } = {};
        public static findById_unecrypted(gpRequestsId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : GpRequests {
            if (Entities.GpRequests.cashedEntities[gpRequestsId.toString()] !== undefined)
                return Entities.GpRequests.cashedEntities[gpRequestsId.toString()];

            var wsPath = "GpRequests/findByGpRequestsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['gpRequestsId'] = gpRequestsId;
            var ret = GpRequests.Create();
            Entities.GpRequests.cashedEntities[gpRequestsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.GpRequests.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + gpRequestsId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['GpRequests'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.GpRequests.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.GpRequests.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
