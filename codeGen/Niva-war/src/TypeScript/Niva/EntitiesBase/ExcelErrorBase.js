/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ExcelFile.ts" />
/// <reference path="../Entities/ExcelError.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ExcelErrorBase = (function (_super) {
        __extends(ExcelErrorBase, _super);
        function ExcelErrorBase(id, excelRowNum, errMessage, rowVersion, exfiId) {
            _super.call(this);
            var self = this;
            this._id = new NpTypes.UIStringModel(id, this);
            this._excelRowNum = new NpTypes.UINumberModel(excelRowNum, this);
            this._errMessage = new NpTypes.UIStringModel(errMessage, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._exfiId = new NpTypes.UIManyToOneModel(exfiId, this);
            self.postConstruct();
        }
        ExcelErrorBase.prototype.getKey = function () {
            return this.id;
        };
        ExcelErrorBase.prototype.getKeyName = function () {
            return "id";
        };
        ExcelErrorBase.prototype.getEntityName = function () {
            return 'ExcelError';
        };
        ExcelErrorBase.prototype.fromJSON = function (data) {
            return Entities.ExcelError.fromJSONComplete(data);
        };
        Object.defineProperty(ExcelErrorBase.prototype, "id", {
            get: function () {
                return this._id.value;
            },
            set: function (id) {
                var self = this;
                self._id.value = id;
                //    self.id_listeners.forEach(cb => { cb(<ExcelError>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExcelErrorBase.prototype, "excelRowNum", {
            get: function () {
                return this._excelRowNum.value;
            },
            set: function (excelRowNum) {
                var self = this;
                self._excelRowNum.value = excelRowNum;
                //    self.excelRowNum_listeners.forEach(cb => { cb(<ExcelError>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExcelErrorBase.prototype, "errMessage", {
            get: function () {
                return this._errMessage.value;
            },
            set: function (errMessage) {
                var self = this;
                self._errMessage.value = errMessage;
                //    self.errMessage_listeners.forEach(cb => { cb(<ExcelError>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExcelErrorBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<ExcelError>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExcelErrorBase.prototype, "exfiId", {
            get: function () {
                return this._exfiId.value;
            },
            set: function (exfiId) {
                var self = this;
                self._exfiId.value = exfiId;
                //    self.exfiId_listeners.forEach(cb => { cb(<ExcelError>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public exfiId_listeners: Array<(c:ExcelError) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.id_listeners.splice(0);
            self.excelRowNum_listeners.splice(0);
            self.errMessage_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.exfiId_listeners.splice(0);
        }
        */
        ExcelErrorBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "ExcelError:" + x.id;
            var ret = new Entities.ExcelError(x.id, x.excelRowNum, x.errMessage, x.rowVersion, x.exfiId);
            deserializedEntities[key] = ret;
            ret.exfiId = (x.exfiId !== undefined && x.exfiId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.exfiId.$entityName].fromJSONPartial(x.exfiId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        ExcelErrorBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ExcelErrorBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ExcelError:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ExcelErrorBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ExcelError:"+x.id;
                    var cachedCopy = <ExcelError>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ExcelError.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ExcelError.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.ExcelError.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ExcelErrorBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.ExcelError.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ExcelErrorBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/ExcelError/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.ExcelError.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ExcelErrorBase.prototype.toJSON = function () {
            var ret = {};
            ret.id = this.id;
            ret.excelRowNum = this.excelRowNum;
            ret.errMessage = this.errMessage;
            ret.rowVersion = this.rowVersion;
            ret.exfiId =
                (this.exfiId !== undefined && this.exfiId !== null) ?
                    { id: this.exfiId.id } :
                    (this.exfiId === undefined ? undefined : null);
            return ret;
        };
        ExcelErrorBase.prototype.updateInstance = function (other) {
            var self = this;
            self.id = other.id;
            self.excelRowNum = other.excelRowNum;
            self.errMessage = other.errMessage;
            self.rowVersion = other.rowVersion;
            self.exfiId = other.exfiId;
        };
        ExcelErrorBase.Create = function () {
            return new Entities.ExcelError(undefined, undefined, undefined, undefined, undefined);
        };
        ExcelErrorBase.CreateById = function (id) {
            var ret = Entities.ExcelError.Create();
            ret.id = id;
            return ret;
        };
        ExcelErrorBase.findById_unecrypted = function (id, $scope, $http, errFunc) {
            if (Entities.ExcelError.cashedEntities[id.toString()] !== undefined)
                return Entities.ExcelError.cashedEntities[id.toString()];
            var wsPath = "ExcelError/findById_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['id'] = id;
            var ret = Entities.ExcelError.Create();
            Entities.ExcelError.cashedEntities[id.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.ExcelError.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + id + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ExcelErrorBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        ExcelErrorBase.cashedEntities = {};
        return ExcelErrorBase;
    })(NpTypes.BaseEntity);
    Entities.ExcelErrorBase = ExcelErrorBase;
    NpTypes.BaseEntity.entitiesFactory['ExcelError'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.ExcelError.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.ExcelError.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ExcelErrorBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
