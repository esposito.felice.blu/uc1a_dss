//AF5EED4B78BF11C7669E6645E027E22D
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/GpRequestsProducersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var GpRequestsProducers;
    (function (GpRequestsProducers) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(GpRequestsProducers.PageModelBase);
        GpRequestsProducers.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(GpRequestsProducers.PageControllerBase);
        GpRequestsProducers.PageController = PageController;
        g_controllers['GpRequestsProducers.PageController'] = Controllers.GpRequestsProducers.PageController;
        var ModelGrpGpRequestsProducers = (function (_super) {
            __extends(ModelGrpGpRequestsProducers, _super);
            function ModelGrpGpRequestsProducers() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpRequestsProducers;
        })(GpRequestsProducers.ModelGrpGpRequestsProducersBase);
        GpRequestsProducers.ModelGrpGpRequestsProducers = ModelGrpGpRequestsProducers;
        var ControllerGrpGpRequestsProducers = (function (_super) {
            __extends(ControllerGrpGpRequestsProducers, _super);
            function ControllerGrpGpRequestsProducers($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpRequestsProducers($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpRequestsProducers;
        })(GpRequestsProducers.ControllerGrpGpRequestsProducersBase);
        GpRequestsProducers.ControllerGrpGpRequestsProducers = ControllerGrpGpRequestsProducers;
        g_controllers['GpRequestsProducers.ControllerGrpGpRequestsProducers'] = Controllers.GpRequestsProducers.ControllerGrpGpRequestsProducers;
        var ModelGrpGpRequestsContexts = (function (_super) {
            __extends(ModelGrpGpRequestsContexts, _super);
            function ModelGrpGpRequestsContexts() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpRequestsContexts;
        })(GpRequestsProducers.ModelGrpGpRequestsContextsBase);
        GpRequestsProducers.ModelGrpGpRequestsContexts = ModelGrpGpRequestsContexts;
        var ControllerGrpGpRequestsContexts = (function (_super) {
            __extends(ControllerGrpGpRequestsContexts, _super);
            function ControllerGrpGpRequestsContexts($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpRequestsContexts($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpRequestsContexts;
        })(GpRequestsProducers.ControllerGrpGpRequestsContextsBase);
        GpRequestsProducers.ControllerGrpGpRequestsContexts = ControllerGrpGpRequestsContexts;
        g_controllers['GpRequestsProducers.ControllerGrpGpRequestsContexts'] = Controllers.GpRequestsProducers.ControllerGrpGpRequestsContexts;
    })(GpRequestsProducers = Controllers.GpRequestsProducers || (Controllers.GpRequestsProducers = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=GpRequestsProducers.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
