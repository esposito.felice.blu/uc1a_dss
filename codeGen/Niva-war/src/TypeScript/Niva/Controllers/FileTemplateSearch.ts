//BFB129886F07018E3990D6A5863683CA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/FileTemplateSearchBase.ts" />

module Controllers {

    export class ModelFileTemplateSearch extends ModelFileTemplateSearchBase {
    }

    export interface IScopeFileTemplateSearch extends IScopeFileTemplateSearchBase {
    }

    export class ControllerFileTemplateSearch extends ControllerFileTemplateSearchBase {
        
        constructor(
            public $scope: IScopeFileTemplateSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelFileTemplateSearch($scope));
        }

        public getLabelById(itmId: string): string {
            switch (itmId) {
                case 'saveBtn_id':
                    return 'Save'
                case 'searchBtn_id':
                    return 'Search';
                case 'clearBtn_id':
                    return 'Clear';
                case 'newBtn_id':
                    return 'Add New Data Import Template';
                case 'newPageBtn_id':
                    return 'Add New Data Import Template';
                case 'cancelBtn_id':
                    return 'Back';
                case 'deleteBtn_id':
                    return 'Delete';
                case 'auditBtn_id':
                    return '?';
            }
            return itmId;
        }

    }

    g_controllers['ControllerFileTemplateSearch'] = Controllers.ControllerFileTemplateSearch;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
