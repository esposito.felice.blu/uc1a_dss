//0838BBA5F263BD7980B091A8C1638A09
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovStatisticBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovStatistic = (function (_super) {
        __extends(ModelLovStatistic, _super);
        function ModelLovStatistic() {
            _super.apply(this, arguments);
        }
        return ModelLovStatistic;
    })(Controllers.ModelLovStatisticBase);
    Controllers.ModelLovStatistic = ModelLovStatistic;
    var ControllerLovStatistic = (function (_super) {
        __extends(ControllerLovStatistic, _super);
        function ControllerLovStatistic($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovStatistic($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovStatistic;
    })(Controllers.ControllerLovStatisticBase);
    Controllers.ControllerLovStatistic = ControllerLovStatistic;
    g_controllers['ControllerLovStatistic'] = Controllers.ControllerLovStatistic;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovStatistic.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
