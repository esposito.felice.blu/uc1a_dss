//CF9C8EC0C9B22DB3740A329D8C5D37E2
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/FmisUserSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelFmisUserSearch = (function (_super) {
        __extends(ModelFmisUserSearch, _super);
        function ModelFmisUserSearch() {
            _super.apply(this, arguments);
        }
        return ModelFmisUserSearch;
    })(Controllers.ModelFmisUserSearchBase);
    Controllers.ModelFmisUserSearch = ModelFmisUserSearch;
    var ControllerFmisUserSearch = (function (_super) {
        __extends(ControllerFmisUserSearch, _super);
        function ControllerFmisUserSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelFmisUserSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerFmisUserSearch;
    })(Controllers.ControllerFmisUserSearchBase);
    Controllers.ControllerFmisUserSearch = ControllerFmisUserSearch;
    g_controllers['ControllerFmisUserSearch'] = Controllers.ControllerFmisUserSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=FmisUserSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
