//3C60C77742B50F57EF2272DCCEC66767
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovGpRequestsContextsBase.ts" />

module Controllers {

    export class ModelLovGpRequestsContexts extends ModelLovGpRequestsContextsBase {
    }

    export interface IScopeLovGpRequestsContexts extends IScopeLovGpRequestsContextsBase {
    }

    export class ControllerLovGpRequestsContexts extends ControllerLovGpRequestsContextsBase {
        
        constructor(
            public $scope: IScopeLovGpRequestsContexts,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovGpRequestsContexts($scope));
        }
    }

    g_controllers['ControllerLovGpRequestsContexts'] = Controllers.ControllerLovGpRequestsContexts;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
