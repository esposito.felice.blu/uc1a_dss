//6959DBA8D89023075EA00CB568731DC4
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelFMISSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelParcelFMISSearch = (function (_super) {
        __extends(ModelParcelFMISSearch, _super);
        function ModelParcelFMISSearch() {
            _super.apply(this, arguments);
        }
        return ModelParcelFMISSearch;
    })(Controllers.ModelParcelFMISSearchBase);
    Controllers.ModelParcelFMISSearch = ModelParcelFMISSearch;
    var ControllerParcelFMISSearch = (function (_super) {
        __extends(ControllerParcelFMISSearch, _super);
        function ControllerParcelFMISSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelParcelFMISSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerParcelFMISSearch;
    })(Controllers.ControllerParcelFMISSearchBase);
    Controllers.ControllerParcelFMISSearch = ControllerParcelFMISSearch;
    g_controllers['ControllerParcelFMISSearch'] = Controllers.ControllerParcelFMISSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelFMISSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
