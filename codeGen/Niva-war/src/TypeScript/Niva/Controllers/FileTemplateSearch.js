//BFB129886F07018E3990D6A5863683CA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/FileTemplateSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelFileTemplateSearch = (function (_super) {
        __extends(ModelFileTemplateSearch, _super);
        function ModelFileTemplateSearch() {
            _super.apply(this, arguments);
        }
        return ModelFileTemplateSearch;
    })(Controllers.ModelFileTemplateSearchBase);
    Controllers.ModelFileTemplateSearch = ModelFileTemplateSearch;
    var ControllerFileTemplateSearch = (function (_super) {
        __extends(ControllerFileTemplateSearch, _super);
        function ControllerFileTemplateSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelFileTemplateSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        ControllerFileTemplateSearch.prototype.getLabelById = function (itmId) {
            switch (itmId) {
                case 'saveBtn_id':
                    return 'Save';
                case 'searchBtn_id':
                    return 'Search';
                case 'clearBtn_id':
                    return 'Clear';
                case 'newBtn_id':
                    return 'Add New Data Import Template';
                case 'newPageBtn_id':
                    return 'Add New Data Import Template';
                case 'cancelBtn_id':
                    return 'Back';
                case 'deleteBtn_id':
                    return 'Delete';
                case 'auditBtn_id':
                    return '?';
            }
            return itmId;
        };
        return ControllerFileTemplateSearch;
    })(Controllers.ControllerFileTemplateSearchBase);
    Controllers.ControllerFileTemplateSearch = ControllerFileTemplateSearch;
    g_controllers['ControllerFileTemplateSearch'] = Controllers.ControllerFileTemplateSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=FileTemplateSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
