//ADDFC979D044E77CB0EBD38E90265ACF
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/AgencyBase.ts" />

module Controllers.Agency {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['Agency.PageController'] = Controllers.Agency.PageController;



    export class ModelGrpAgency extends ModelGrpAgencyBase {
    }


    export interface IScopeGrpAgency extends IScopeGrpAgencyBase {
    }

    export class ControllerGrpAgency extends ControllerGrpAgencyBase {
        constructor(
            public $scope: IScopeGrpAgency,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpAgency($scope) );
        }
    }
    g_controllers['Agency.ControllerGrpAgency'] = Controllers.Agency.ControllerGrpAgency;
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
