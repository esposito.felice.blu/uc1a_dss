//5A1C49A3DFB80DBFD78C882A1D6F2224
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovEcCultDetailBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovEcCultDetail = (function (_super) {
        __extends(ModelLovEcCultDetail, _super);
        function ModelLovEcCultDetail() {
            _super.apply(this, arguments);
        }
        return ModelLovEcCultDetail;
    })(Controllers.ModelLovEcCultDetailBase);
    Controllers.ModelLovEcCultDetail = ModelLovEcCultDetail;
    var ControllerLovEcCultDetail = (function (_super) {
        __extends(ControllerLovEcCultDetail, _super);
        function ControllerLovEcCultDetail($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovEcCultDetail($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovEcCultDetail;
    })(Controllers.ControllerLovEcCultDetailBase);
    Controllers.ControllerLovEcCultDetail = ControllerLovEcCultDetail;
    g_controllers['ControllerLovEcCultDetail'] = Controllers.ControllerLovEcCultDetail;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovEcCultDetail.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
