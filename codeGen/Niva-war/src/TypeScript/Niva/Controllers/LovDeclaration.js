//2B33E1675A35FA382F09DA0FB5CAA2CC
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovDeclarationBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovDeclaration = (function (_super) {
        __extends(ModelLovDeclaration, _super);
        function ModelLovDeclaration() {
            _super.apply(this, arguments);
        }
        return ModelLovDeclaration;
    })(Controllers.ModelLovDeclarationBase);
    Controllers.ModelLovDeclaration = ModelLovDeclaration;
    var ControllerLovDeclaration = (function (_super) {
        __extends(ControllerLovDeclaration, _super);
        function ControllerLovDeclaration($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovDeclaration($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovDeclaration;
    })(Controllers.ControllerLovDeclarationBase);
    Controllers.ControllerLovDeclaration = ControllerLovDeclaration;
    g_controllers['ControllerLovDeclaration'] = Controllers.ControllerLovDeclaration;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovDeclaration.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
