//22D138BE5E0838ED30F6828758F4D1EA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/FmisUserBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var FmisUser;
    (function (FmisUser) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(FmisUser.PageModelBase);
        FmisUser.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(FmisUser.PageControllerBase);
        FmisUser.PageController = PageController;
        g_controllers['FmisUser.PageController'] = Controllers.FmisUser.PageController;
        var ModelGrpFmisUser = (function (_super) {
            __extends(ModelGrpFmisUser, _super);
            function ModelGrpFmisUser() {
                _super.apply(this, arguments);
            }
            return ModelGrpFmisUser;
        })(FmisUser.ModelGrpFmisUserBase);
        FmisUser.ModelGrpFmisUser = ModelGrpFmisUser;
        var ControllerGrpFmisUser = (function (_super) {
            __extends(ControllerGrpFmisUser, _super);
            function ControllerGrpFmisUser($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpFmisUser($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpFmisUser;
        })(FmisUser.ControllerGrpFmisUserBase);
        FmisUser.ControllerGrpFmisUser = ControllerGrpFmisUser;
        g_controllers['FmisUser.ControllerGrpFmisUser'] = Controllers.FmisUser.ControllerGrpFmisUser;
    })(FmisUser = Controllers.FmisUser || (Controllers.FmisUser = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=FmisUser.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
