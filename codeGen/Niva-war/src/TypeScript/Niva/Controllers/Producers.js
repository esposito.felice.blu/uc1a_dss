//9C72B560B20DE2FD236D2F0A270AD3B8
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ProducersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var Producers;
    (function (Producers) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(Producers.PageModelBase);
        Producers.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(Producers.PageControllerBase);
        Producers.PageController = PageController;
        g_controllers['Producers.PageController'] = Controllers.Producers.PageController;
        var ModelGrpProducers = (function (_super) {
            __extends(ModelGrpProducers, _super);
            function ModelGrpProducers() {
                _super.apply(this, arguments);
            }
            return ModelGrpProducers;
        })(Producers.ModelGrpProducersBase);
        Producers.ModelGrpProducers = ModelGrpProducers;
        var ControllerGrpProducers = (function (_super) {
            __extends(ControllerGrpProducers, _super);
            function ControllerGrpProducers($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpProducers($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpProducers;
        })(Producers.ControllerGrpProducersBase);
        Producers.ControllerGrpProducers = ControllerGrpProducers;
        g_controllers['Producers.ControllerGrpProducers'] = Controllers.Producers.ControllerGrpProducers;
    })(Producers = Controllers.Producers || (Controllers.Producers = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=Producers.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
