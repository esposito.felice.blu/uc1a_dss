//0C47F89FB860F67F8E9438EDA51C9546
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/GpRequestsBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var GpRequests;
    (function (GpRequests) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(GpRequests.PageModelBase);
        GpRequests.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(GpRequests.PageControllerBase);
        GpRequests.PageController = PageController;
        g_controllers['GpRequests.PageController'] = Controllers.GpRequests.PageController;
        var ModelGrpGpRequests = (function (_super) {
            __extends(ModelGrpGpRequests, _super);
            function ModelGrpGpRequests() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpRequests;
        })(GpRequests.ModelGrpGpRequestsBase);
        GpRequests.ModelGrpGpRequests = ModelGrpGpRequests;
        var ControllerGrpGpRequests = (function (_super) {
            __extends(ControllerGrpGpRequests, _super);
            function ControllerGrpGpRequests($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpRequests($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpRequests;
        })(GpRequests.ControllerGrpGpRequestsBase);
        GpRequests.ControllerGrpGpRequests = ControllerGrpGpRequests;
        g_controllers['GpRequests.ControllerGrpGpRequests'] = Controllers.GpRequests.ControllerGrpGpRequests;
        var ModelGrpGpRequestsProducers = (function (_super) {
            __extends(ModelGrpGpRequestsProducers, _super);
            function ModelGrpGpRequestsProducers() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpRequestsProducers;
        })(GpRequests.ModelGrpGpRequestsProducersBase);
        GpRequests.ModelGrpGpRequestsProducers = ModelGrpGpRequestsProducers;
        var ControllerGrpGpRequestsProducers = (function (_super) {
            __extends(ControllerGrpGpRequestsProducers, _super);
            function ControllerGrpGpRequestsProducers($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpRequestsProducers($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpRequestsProducers;
        })(GpRequests.ControllerGrpGpRequestsProducersBase);
        GpRequests.ControllerGrpGpRequestsProducers = ControllerGrpGpRequestsProducers;
        g_controllers['GpRequests.ControllerGrpGpRequestsProducers'] = Controllers.GpRequests.ControllerGrpGpRequestsProducers;
        var ModelGrpGpRequestsContexts = (function (_super) {
            __extends(ModelGrpGpRequestsContexts, _super);
            function ModelGrpGpRequestsContexts() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpRequestsContexts;
        })(GpRequests.ModelGrpGpRequestsContextsBase);
        GpRequests.ModelGrpGpRequestsContexts = ModelGrpGpRequestsContexts;
        var ControllerGrpGpRequestsContexts = (function (_super) {
            __extends(ControllerGrpGpRequestsContexts, _super);
            function ControllerGrpGpRequestsContexts($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpRequestsContexts($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpRequestsContexts;
        })(GpRequests.ControllerGrpGpRequestsContextsBase);
        GpRequests.ControllerGrpGpRequestsContexts = ControllerGrpGpRequestsContexts;
        g_controllers['GpRequests.ControllerGrpGpRequestsContexts'] = Controllers.GpRequests.ControllerGrpGpRequestsContexts;
    })(GpRequests = Controllers.GpRequests || (Controllers.GpRequests = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=GpRequests.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
