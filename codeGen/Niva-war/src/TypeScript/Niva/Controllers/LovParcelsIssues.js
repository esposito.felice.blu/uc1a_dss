//D671BB7F1359250D9349E9ACD5BA1455
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovParcelsIssuesBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovParcelsIssues = (function (_super) {
        __extends(ModelLovParcelsIssues, _super);
        function ModelLovParcelsIssues() {
            _super.apply(this, arguments);
        }
        return ModelLovParcelsIssues;
    })(Controllers.ModelLovParcelsIssuesBase);
    Controllers.ModelLovParcelsIssues = ModelLovParcelsIssues;
    var ControllerLovParcelsIssues = (function (_super) {
        __extends(ControllerLovParcelsIssues, _super);
        function ControllerLovParcelsIssues($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovParcelsIssues($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovParcelsIssues;
    })(Controllers.ControllerLovParcelsIssuesBase);
    Controllers.ControllerLovParcelsIssues = ControllerLovParcelsIssues;
    g_controllers['ControllerLovParcelsIssues'] = Controllers.ControllerLovParcelsIssues;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovParcelsIssues.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
