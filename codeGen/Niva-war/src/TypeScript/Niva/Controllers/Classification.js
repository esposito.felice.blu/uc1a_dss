//99D0F0E6499CC118B8B4141803CD43AA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ClassificationBase.ts" />
/// <reference path="../Services/MainService.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var Classification;
    (function (Classification) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(Classification.PageModelBase);
        Classification.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(Classification.PageControllerBase);
        Classification.PageController = PageController;
        g_controllers['Classification.PageController'] = Controllers.Classification.PageController;
        var ModelGrpClas = (function (_super) {
            __extends(ModelGrpClas, _super);
            function ModelGrpClas() {
                _super.apply(this, arguments);
            }
            return ModelGrpClas;
        })(Classification.ModelGrpClasBase);
        Classification.ModelGrpClas = ModelGrpClas;
        var ControllerGrpClas = (function (_super) {
            __extends(ControllerGrpClas, _super);
            function ControllerGrpClas($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpClas($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            ControllerGrpClas.prototype.isImportDisabled = function (classification) {
                if ((isVoid(classification)) || (classification.filePath === null)
                    || (classification.name === null) || (classification.dateTime === null)
                    || (classification.clfrId === null) || (classification.fiteId === null)
                    || (classification.isNew())) {
                    return true;
                }
                return false;
            };
            ControllerGrpClas.prototype.isFieldsDisabled = function (classification) {
                if ((!isVoid(classification)) && ((classification.recordtype === 1))) {
                    return true;
                }
                return false;
            };
            /**/
            ControllerGrpClas.prototype.importAction = function (classification) {
                var self = this;
                console.log("Method: importAction() called");
                // Checking Modifications
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    //Error was detected
                    NpTypes.AlertMessage.clearAlerts(this.$scope.pageModel);
                    NpTypes.AlertMessage.addDanger(this.$scope.pageModel, "Please save before Importing");
                    return;
                }
                //Ask Confirmatiom 
                messageBox(self.$scope, self.Plato, "Info", "Do you want to Import?", IconKind.QUESTION, [
                    new Tuple2("Yes", function () {
                        NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                        //call web service
                        var paramData = new Services.MainService_importingClassification_req();
                        paramData.classId = classification.clasId;
                        Services.MainService.importingClassification(self.$scope.pageModel.controller, paramData, function (respDataList) {
                            classification.isImported = true;
                            self.model.modelGrpParcelClas.controller.updateUI();
                            //self.updateUI();
                            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                            NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Import done succesfully.");
                            respDataList.forEach(function (item) { return NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item); });
                        });
                    }),
                    new Tuple2("No", function () { })
                ], 1, 1, '20em');
            };
            ControllerGrpClas.prototype.isEditClassificationDisabled = function (classification) {
                var self = this;
                if (classification.isImported === null) {
                    classification.isImported = false;
                }
                if (classification.isImported === false) {
                    return false;
                }
                else {
                    return true;
                }
            };
            ControllerGrpClas.prototype.isEditClassificationNotImported = function () {
                var self = this;
                return !self.modelGrpClas.isImported;
            };
            return ControllerGrpClas;
        })(Classification.ControllerGrpClasBase);
        Classification.ControllerGrpClas = ControllerGrpClas;
        g_controllers['Classification.ControllerGrpClas'] = Controllers.Classification.ControllerGrpClas;
        var ModelGrpParcelClas = (function (_super) {
            __extends(ModelGrpParcelClas, _super);
            function ModelGrpParcelClas() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelClas;
        })(Classification.ModelGrpParcelClasBase);
        Classification.ModelGrpParcelClas = ModelGrpParcelClas;
        var ControllerGrpParcelClas = (function (_super) {
            __extends(ControllerGrpParcelClas, _super);
            function ControllerGrpParcelClas($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelClas($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            ControllerGrpParcelClas.prototype.createGeom4326Map = function (defaultOptions) {
                defaultOptions.layers = [new NpGeoLayers.OpenStreetTileLayer(true)];
                defaultOptions.zoomOnEntityChange = true;
                defaultOptions.showCoordinates = true;
                defaultOptions.showRuler = true;
                defaultOptions.centerPoint = [49.843056, 9.901944];
                defaultOptions.zoomLevel = 10;
                defaultOptions.fillColor = "rgba(50, 0,255, 0.1)";
                defaultOptions.borderColor = "blue";
                //  defaultOptions.labelColor?: string;
                defaultOptions.penWidth = 4;
                defaultOptions.opacity = 0.5;
                defaultOptions.layerSelectorWidthInPx = 170;
                defaultOptions.coordinateSystem = "EPSG:4326";
                // mapId?: string;
                defaultOptions.showJumpButton = true;
                defaultOptions.showEditButtons = false;
                defaultOptions.showMeasureButton = false;
                defaultOptions.showLegendButton = false;
                // defaultOptions.getEntityLabel?: (currentEntity: any) => string;
                // defaultOptions.getFeatureLabel?: (feature: ol.Feature, lightweight: boolean) => string; // it should be property of vectory layer
                //  defaultOptions.topologyType?: number; //0|undfined = none, 1=client topology, 2:serverside topology
                //topologyFunc?: (feature: ol.geom.Geometry, resp : ) => void
                defaultOptions.fullScreenOnlyMap = false; //true or false. If true only mapdiv shall be used for full screen control. If false or ommited use map+datagrid
                this.geom4326Map = new NpGeo.NpMap(defaultOptions);
            };
            return ControllerGrpParcelClas;
        })(Classification.ControllerGrpParcelClasBase);
        Classification.ControllerGrpParcelClas = ControllerGrpParcelClas;
        g_controllers['Classification.ControllerGrpParcelClas'] = Controllers.Classification.ControllerGrpParcelClas;
    })(Classification = Controllers.Classification || (Controllers.Classification = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=Classification.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
