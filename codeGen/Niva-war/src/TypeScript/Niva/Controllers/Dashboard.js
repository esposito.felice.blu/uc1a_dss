//356FC6613BDE308955193C0094DFC5AE
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DashboardBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var Dashboard;
    (function (Dashboard) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(Dashboard.PageModelBase);
        Dashboard.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(Dashboard.PageControllerBase);
        Dashboard.PageController = PageController;
        g_controllers['Dashboard.PageController'] = Controllers.Dashboard.PageController;
        var ModelGrpParcelClas = (function (_super) {
            __extends(ModelGrpParcelClas, _super);
            function ModelGrpParcelClas() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelClas;
        })(Dashboard.ModelGrpParcelClasBase);
        Dashboard.ModelGrpParcelClas = ModelGrpParcelClas;
        var ControllerGrpParcelClas = (function (_super) {
            __extends(ControllerGrpParcelClas, _super);
            function ControllerGrpParcelClas($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelClas($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            ControllerGrpParcelClas.prototype.createGeom4326Map = function (defaultOptions) {
                var self = this;
                // self.$scope.modelGrpParcelDecision = self.model;
                defaultOptions.layers = [new NpGeoLayers.OpenStreetTileLayer(true)];
                defaultOptions.zoomOnEntityChange = true;
                defaultOptions.showCoordinates = true;
                defaultOptions.showRuler = true;
                defaultOptions.centerPoint = [49.843056, 9.901944];
                defaultOptions.zoomLevel = 10;
                defaultOptions.fillColor = "rgba(50, 0,255, 0.1)";
                defaultOptions.borderColor = "blue";
                //  defaultOptions.labelColor?: string;
                defaultOptions.penWidth = 4;
                defaultOptions.opacity = 0.5;
                defaultOptions.layerSelectorWidthInPx = 170;
                defaultOptions.coordinateSystem = "EPSG:4326";
                // mapId?: string;
                defaultOptions.showJumpButton = true;
                defaultOptions.showEditButtons = false;
                defaultOptions.showMeasureButton = false;
                defaultOptions.showLegendButton = false;
                // defaultOptions.getEntityLabel?: (currentEntity: any) => string;
                // defaultOptions.getFeatureLabel?: (feature: ol.Feature, lightweight: boolean) => string; // it should be property of vectory layer
                //  defaultOptions.topologyType?: number; //0|undfined = none, 1=client topology, 2:serverside topology
                //topologyFunc?: (feature: ol.geom.Geometry, resp : ) => void
                defaultOptions.fullScreenOnlyMap = false; //true or false. If true only mapdiv shall be used for full screen control. If false or ommited use map+datagrid
                this.geom4326Map = new NpGeo.NpMap(defaultOptions);
            };
            return ControllerGrpParcelClas;
        })(Dashboard.ControllerGrpParcelClasBase);
        Dashboard.ControllerGrpParcelClas = ControllerGrpParcelClas;
        g_controllers['Dashboard.ControllerGrpParcelClas'] = Controllers.Dashboard.ControllerGrpParcelClas;
        var ModelGrpParcelDecision = (function (_super) {
            __extends(ModelGrpParcelDecision, _super);
            function ModelGrpParcelDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelDecision;
        })(Dashboard.ModelGrpParcelDecisionBase);
        Dashboard.ModelGrpParcelDecision = ModelGrpParcelDecision;
        var ControllerGrpParcelDecision = (function (_super) {
            __extends(ControllerGrpParcelDecision, _super);
            function ControllerGrpParcelDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelDecision;
        })(Dashboard.ControllerGrpParcelDecisionBase);
        Dashboard.ControllerGrpParcelDecision = ControllerGrpParcelDecision;
        g_controllers['Dashboard.ControllerGrpParcelDecision'] = Controllers.Dashboard.ControllerGrpParcelDecision;
        var ModelGrpParcelsIssues = (function (_super) {
            __extends(ModelGrpParcelsIssues, _super);
            function ModelGrpParcelsIssues() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelsIssues;
        })(Dashboard.ModelGrpParcelsIssuesBase);
        Dashboard.ModelGrpParcelsIssues = ModelGrpParcelsIssues;
        var ControllerGrpParcelsIssues = (function (_super) {
            __extends(ControllerGrpParcelsIssues, _super);
            function ControllerGrpParcelsIssues($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelsIssues($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelsIssues;
        })(Dashboard.ControllerGrpParcelsIssuesBase);
        Dashboard.ControllerGrpParcelsIssues = ControllerGrpParcelsIssues;
        g_controllers['Dashboard.ControllerGrpParcelsIssues'] = Controllers.Dashboard.ControllerGrpParcelsIssues;
        var ModelGrpGpDecision = (function (_super) {
            __extends(ModelGrpGpDecision, _super);
            function ModelGrpGpDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpDecision;
        })(Dashboard.ModelGrpGpDecisionBase);
        Dashboard.ModelGrpGpDecision = ModelGrpGpDecision;
        var ControllerGrpGpDecision = (function (_super) {
            __extends(ControllerGrpGpDecision, _super);
            function ControllerGrpGpDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpDecision;
        })(Dashboard.ControllerGrpGpDecisionBase);
        Dashboard.ControllerGrpGpDecision = ControllerGrpGpDecision;
        g_controllers['Dashboard.ControllerGrpGpDecision'] = Controllers.Dashboard.ControllerGrpGpDecision;
        var ModelGrpFmisDecision = (function (_super) {
            __extends(ModelGrpFmisDecision, _super);
            function ModelGrpFmisDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpFmisDecision;
        })(Dashboard.ModelGrpFmisDecisionBase);
        Dashboard.ModelGrpFmisDecision = ModelGrpFmisDecision;
        var ControllerGrpFmisDecision = (function (_super) {
            __extends(ControllerGrpFmisDecision, _super);
            function ControllerGrpFmisDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpFmisDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpFmisDecision;
        })(Dashboard.ControllerGrpFmisDecisionBase);
        Dashboard.ControllerGrpFmisDecision = ControllerGrpFmisDecision;
        g_controllers['Dashboard.ControllerGrpFmisDecision'] = Controllers.Dashboard.ControllerGrpFmisDecision;
        var ModelGrpIntegrateddecision = (function (_super) {
            __extends(ModelGrpIntegrateddecision, _super);
            function ModelGrpIntegrateddecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpIntegrateddecision;
        })(Dashboard.ModelGrpIntegrateddecisionBase);
        Dashboard.ModelGrpIntegrateddecision = ModelGrpIntegrateddecision;
        var ControllerGrpIntegrateddecision = (function (_super) {
            __extends(ControllerGrpIntegrateddecision, _super);
            function ControllerGrpIntegrateddecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpIntegrateddecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpIntegrateddecision;
        })(Dashboard.ControllerGrpIntegrateddecisionBase);
        Dashboard.ControllerGrpIntegrateddecision = ControllerGrpIntegrateddecision;
        g_controllers['Dashboard.ControllerGrpIntegrateddecision'] = Controllers.Dashboard.ControllerGrpIntegrateddecision;
    })(Dashboard = Controllers.Dashboard || (Controllers.Dashboard = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=Dashboard.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
