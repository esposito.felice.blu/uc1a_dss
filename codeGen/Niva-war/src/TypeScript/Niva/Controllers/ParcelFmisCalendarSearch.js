//3ABBDF5F3FA7918ADA43E4D8382F0342
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelFmisCalendarSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelParcelFmisCalendarSearch = (function (_super) {
        __extends(ModelParcelFmisCalendarSearch, _super);
        function ModelParcelFmisCalendarSearch() {
            _super.apply(this, arguments);
        }
        return ModelParcelFmisCalendarSearch;
    })(Controllers.ModelParcelFmisCalendarSearchBase);
    Controllers.ModelParcelFmisCalendarSearch = ModelParcelFmisCalendarSearch;
    var ControllerParcelFmisCalendarSearch = (function (_super) {
        __extends(ControllerParcelFmisCalendarSearch, _super);
        function ControllerParcelFmisCalendarSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelParcelFmisCalendarSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerParcelFmisCalendarSearch;
    })(Controllers.ControllerParcelFmisCalendarSearchBase);
    Controllers.ControllerParcelFmisCalendarSearch = ControllerParcelFmisCalendarSearch;
    g_controllers['ControllerParcelFmisCalendarSearch'] = Controllers.ControllerParcelFmisCalendarSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelFmisCalendarSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
