//1CA554B1E3970220602949BC13DDB5A8
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovProducersBase.ts" />

module Controllers {

    export class ModelLovProducers extends ModelLovProducersBase {
    }

    export interface IScopeLovProducers extends IScopeLovProducersBase {
    }

    export class ControllerLovProducers extends ControllerLovProducersBase {
        
        constructor(
            public $scope: IScopeLovProducers,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovProducers($scope));
        }
    }

    g_controllers['ControllerLovProducers'] = Controllers.ControllerLovProducers;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
