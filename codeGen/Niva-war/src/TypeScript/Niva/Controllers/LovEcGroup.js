//659AC2115BD34666B98B854E5B01FEBE
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovEcGroupBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovEcGroup = (function (_super) {
        __extends(ModelLovEcGroup, _super);
        function ModelLovEcGroup() {
            _super.apply(this, arguments);
        }
        return ModelLovEcGroup;
    })(Controllers.ModelLovEcGroupBase);
    Controllers.ModelLovEcGroup = ModelLovEcGroup;
    var ControllerLovEcGroup = (function (_super) {
        __extends(ControllerLovEcGroup, _super);
        function ControllerLovEcGroup($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovEcGroup($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovEcGroup;
    })(Controllers.ControllerLovEcGroupBase);
    Controllers.ControllerLovEcGroup = ControllerLovEcGroup;
    g_controllers['ControllerLovEcGroup'] = Controllers.ControllerLovEcGroup;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovEcGroup.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
