//6CE481DC93040BC7B0A5AF89DB097DE6
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ProducersSearchBase.ts" />

module Controllers {

    export class ModelProducersSearch extends ModelProducersSearchBase {
    }

    export interface IScopeProducersSearch extends IScopeProducersSearchBase {
    }

    export class ControllerProducersSearch extends ControllerProducersSearchBase {
        
        constructor(
            public $scope: IScopeProducersSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelProducersSearch($scope));
        }
    }

    g_controllers['ControllerProducersSearch'] = Controllers.ControllerProducersSearch;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
