//D880EB4702867E19E0CBFC98D5FF39CA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ClassificationSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelClassificationSearch = (function (_super) {
        __extends(ModelClassificationSearch, _super);
        function ModelClassificationSearch() {
            _super.apply(this, arguments);
        }
        return ModelClassificationSearch;
    })(Controllers.ModelClassificationSearchBase);
    Controllers.ModelClassificationSearch = ModelClassificationSearch;
    var ControllerClassificationSearch = (function (_super) {
        __extends(ControllerClassificationSearch, _super);
        function ControllerClassificationSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelClassificationSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        ControllerClassificationSearch.prototype.getLabelById = function (itmId) {
            switch (itmId) {
                case 'saveBtn_id':
                    return 'Save';
                case 'searchBtn_id':
                    return 'Search';
                case 'clearBtn_id':
                    return 'Clear';
                case 'newBtn_id':
                    return 'Add New Data Import';
                case 'newPageBtn_id':
                    return 'Add New Data Import';
                case 'cancelBtn_id':
                    return 'Back';
                case 'deleteBtn_id':
                    return 'Delete';
                case 'auditBtn_id':
                    return '?';
            }
            return itmId;
        };
        return ControllerClassificationSearch;
    })(Controllers.ControllerClassificationSearchBase);
    Controllers.ControllerClassificationSearch = ControllerClassificationSearch;
    g_controllers['ControllerClassificationSearch'] = Controllers.ControllerClassificationSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ClassificationSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
