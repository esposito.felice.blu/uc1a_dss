//F316B70AE21AD3C836AEA43557A277AB
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/EcGroupBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var EcGroup;
    (function (EcGroup) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(EcGroup.PageModelBase);
        EcGroup.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(EcGroup.PageControllerBase);
        EcGroup.PageController = PageController;
        g_controllers['EcGroup.PageController'] = Controllers.EcGroup.PageController;
        var ModelGrpEcGroup = (function (_super) {
            __extends(ModelGrpEcGroup, _super);
            function ModelGrpEcGroup() {
                _super.apply(this, arguments);
            }
            return ModelGrpEcGroup;
        })(EcGroup.ModelGrpEcGroupBase);
        EcGroup.ModelGrpEcGroup = ModelGrpEcGroup;
        var ControllerGrpEcGroup = (function (_super) {
            __extends(ControllerGrpEcGroup, _super);
            function ControllerGrpEcGroup($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpEcGroup($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            ControllerGrpEcGroup.prototype.isEcGroupDisabled = function () {
                return this.Current.recordtype !== 0;
            };
            ControllerGrpEcGroup.prototype.isCropLevelDisabled = function (ecGroup) {
                return (isVoid(ecGroup)) || (!ecGroup.isNew());
            };
            // The three methods bellow controls which of the three groups (for Crop Type or Land Cover or Super Class) will be visible
            // show groups bellow  when crop level is Crop Type
            ControllerGrpEcGroup.prototype.isEcGroupChildInv = function () {
                if (this.Current.cropLevel === 0) {
                    return false;
                }
                return true;
            };
            // show groups bellow when crop level is Land Cover
            ControllerGrpEcGroup.prototype.isEcGroupCoverChildInv = function () {
                if (this.Current.cropLevel === 1) {
                    return false;
                }
                return true;
            };
            // show groups bellow when crop level is  Crop -> Land Cover
            ControllerGrpEcGroup.prototype.isEcGroupSuperChildInv = function () {
                if (this.Current.cropLevel === 2) {
                    return false;
                }
                return true;
            };
            return ControllerGrpEcGroup;
        })(EcGroup.ControllerGrpEcGroupBase);
        EcGroup.ControllerGrpEcGroup = ControllerGrpEcGroup;
        g_controllers['EcGroup.ControllerGrpEcGroup'] = Controllers.EcGroup.ControllerGrpEcGroup;
        var ModelGrpEcCult = (function (_super) {
            __extends(ModelGrpEcCult, _super);
            function ModelGrpEcCult() {
                _super.apply(this, arguments);
            }
            return ModelGrpEcCult;
        })(EcGroup.ModelGrpEcCultBase);
        EcGroup.ModelGrpEcCult = ModelGrpEcCult;
        var ControllerGrpEcCult = (function (_super) {
            __extends(ControllerGrpEcCult, _super);
            function ControllerGrpEcCult($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpEcCult($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpEcCult;
        })(EcGroup.ControllerGrpEcCultBase);
        EcGroup.ControllerGrpEcCult = ControllerGrpEcCult;
        g_controllers['EcGroup.ControllerGrpEcCult'] = Controllers.EcGroup.ControllerGrpEcCult;
        var ModelGrpEcCultDetail = (function (_super) {
            __extends(ModelGrpEcCultDetail, _super);
            function ModelGrpEcCultDetail() {
                _super.apply(this, arguments);
            }
            return ModelGrpEcCultDetail;
        })(EcGroup.ModelGrpEcCultDetailBase);
        EcGroup.ModelGrpEcCultDetail = ModelGrpEcCultDetail;
        var ControllerGrpEcCultDetail = (function (_super) {
            __extends(ControllerGrpEcCultDetail, _super);
            function ControllerGrpEcCultDetail($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpEcCultDetail($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpEcCultDetail;
        })(EcGroup.ControllerGrpEcCultDetailBase);
        EcGroup.ControllerGrpEcCultDetail = ControllerGrpEcCultDetail;
        g_controllers['EcGroup.ControllerGrpEcCultDetail'] = Controllers.EcGroup.ControllerGrpEcCultDetail;
        var ModelEcCultCover = (function (_super) {
            __extends(ModelEcCultCover, _super);
            function ModelEcCultCover() {
                _super.apply(this, arguments);
            }
            return ModelEcCultCover;
        })(EcGroup.ModelEcCultCoverBase);
        EcGroup.ModelEcCultCover = ModelEcCultCover;
        var ControllerEcCultCover = (function (_super) {
            __extends(ControllerEcCultCover, _super);
            function ControllerEcCultCover($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelEcCultCover($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerEcCultCover;
        })(EcGroup.ControllerEcCultCoverBase);
        EcGroup.ControllerEcCultCover = ControllerEcCultCover;
        g_controllers['EcGroup.ControllerEcCultCover'] = Controllers.EcGroup.ControllerEcCultCover;
        var ModelEcCultDetailCover = (function (_super) {
            __extends(ModelEcCultDetailCover, _super);
            function ModelEcCultDetailCover() {
                _super.apply(this, arguments);
            }
            return ModelEcCultDetailCover;
        })(EcGroup.ModelEcCultDetailCoverBase);
        EcGroup.ModelEcCultDetailCover = ModelEcCultDetailCover;
        var ControllerEcCultDetailCover = (function (_super) {
            __extends(ControllerEcCultDetailCover, _super);
            function ControllerEcCultDetailCover($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelEcCultDetailCover($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerEcCultDetailCover;
        })(EcGroup.ControllerEcCultDetailCoverBase);
        EcGroup.ControllerEcCultDetailCover = ControllerEcCultDetailCover;
        g_controllers['EcGroup.ControllerEcCultDetailCover'] = Controllers.EcGroup.ControllerEcCultDetailCover;
        var ModelEcCultSuper = (function (_super) {
            __extends(ModelEcCultSuper, _super);
            function ModelEcCultSuper() {
                _super.apply(this, arguments);
            }
            return ModelEcCultSuper;
        })(EcGroup.ModelEcCultSuperBase);
        EcGroup.ModelEcCultSuper = ModelEcCultSuper;
        var ControllerEcCultSuper = (function (_super) {
            __extends(ControllerEcCultSuper, _super);
            function ControllerEcCultSuper($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelEcCultSuper($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerEcCultSuper;
        })(EcGroup.ControllerEcCultSuperBase);
        EcGroup.ControllerEcCultSuper = ControllerEcCultSuper;
        g_controllers['EcGroup.ControllerEcCultSuper'] = Controllers.EcGroup.ControllerEcCultSuper;
        var ModelEcCultDetailSuper = (function (_super) {
            __extends(ModelEcCultDetailSuper, _super);
            function ModelEcCultDetailSuper() {
                _super.apply(this, arguments);
            }
            return ModelEcCultDetailSuper;
        })(EcGroup.ModelEcCultDetailSuperBase);
        EcGroup.ModelEcCultDetailSuper = ModelEcCultDetailSuper;
        var ControllerEcCultDetailSuper = (function (_super) {
            __extends(ControllerEcCultDetailSuper, _super);
            function ControllerEcCultDetailSuper($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelEcCultDetailSuper($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerEcCultDetailSuper;
        })(EcGroup.ControllerEcCultDetailSuperBase);
        EcGroup.ControllerEcCultDetailSuper = ControllerEcCultDetailSuper;
        g_controllers['EcGroup.ControllerEcCultDetailSuper'] = Controllers.EcGroup.ControllerEcCultDetailSuper;
    })(EcGroup = Controllers.EcGroup || (Controllers.EcGroup = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=EcGroup.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
