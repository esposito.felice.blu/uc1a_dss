//5585D323DF5C3F1CE617EEE20CAD0E0E
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelsIssuesSearchBase.ts" />

module Controllers {

    export class ModelParcelsIssuesSearch extends ModelParcelsIssuesSearchBase {
    }

    export interface IScopeParcelsIssuesSearch extends IScopeParcelsIssuesSearchBase {
    }

    export class ControllerParcelsIssuesSearch extends ControllerParcelsIssuesSearchBase {
        
        constructor(
            public $scope: IScopeParcelsIssuesSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelParcelsIssuesSearch($scope));
        }
    }

    g_controllers['ControllerParcelsIssuesSearch'] = Controllers.ControllerParcelsIssuesSearch;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
