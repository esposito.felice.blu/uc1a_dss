//3661D408F4F94B0B53A8109709AAC0F9
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovAgrisnapUsersProducersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovAgrisnapUsersProducers = (function (_super) {
        __extends(ModelLovAgrisnapUsersProducers, _super);
        function ModelLovAgrisnapUsersProducers() {
            _super.apply(this, arguments);
        }
        return ModelLovAgrisnapUsersProducers;
    })(Controllers.ModelLovAgrisnapUsersProducersBase);
    Controllers.ModelLovAgrisnapUsersProducers = ModelLovAgrisnapUsersProducers;
    var ControllerLovAgrisnapUsersProducers = (function (_super) {
        __extends(ControllerLovAgrisnapUsersProducers, _super);
        function ControllerLovAgrisnapUsersProducers($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovAgrisnapUsersProducers($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovAgrisnapUsersProducers;
    })(Controllers.ControllerLovAgrisnapUsersProducersBase);
    Controllers.ControllerLovAgrisnapUsersProducers = ControllerLovAgrisnapUsersProducers;
    g_controllers['ControllerLovAgrisnapUsersProducers'] = Controllers.ControllerLovAgrisnapUsersProducers;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovAgrisnapUsersProducers.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
