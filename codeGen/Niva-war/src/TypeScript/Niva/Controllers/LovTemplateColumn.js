//145E30A8CD9CCFC0EB027A3AF17041C0
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovTemplateColumnBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovTemplateColumn = (function (_super) {
        __extends(ModelLovTemplateColumn, _super);
        function ModelLovTemplateColumn() {
            _super.apply(this, arguments);
        }
        return ModelLovTemplateColumn;
    })(Controllers.ModelLovTemplateColumnBase);
    Controllers.ModelLovTemplateColumn = ModelLovTemplateColumn;
    var ControllerLovTemplateColumn = (function (_super) {
        __extends(ControllerLovTemplateColumn, _super);
        function ControllerLovTemplateColumn($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovTemplateColumn($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovTemplateColumn;
    })(Controllers.ControllerLovTemplateColumnBase);
    Controllers.ControllerLovTemplateColumn = ControllerLovTemplateColumn;
    g_controllers['ControllerLovTemplateColumn'] = Controllers.ControllerLovTemplateColumn;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovTemplateColumn.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
