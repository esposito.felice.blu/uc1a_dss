//FA8DE004B39630B719391B8B599AAC46
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DashboardSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelDashboardSearch = (function (_super) {
        __extends(ModelDashboardSearch, _super);
        function ModelDashboardSearch() {
            _super.apply(this, arguments);
        }
        return ModelDashboardSearch;
    })(Controllers.ModelDashboardSearchBase);
    Controllers.ModelDashboardSearch = ModelDashboardSearch;
    var ControllerDashboardSearch = (function (_super) {
        __extends(ControllerDashboardSearch, _super);
        function ControllerDashboardSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelDashboardSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerDashboardSearch;
    })(Controllers.ControllerDashboardSearchBase);
    Controllers.ControllerDashboardSearch = ControllerDashboardSearch;
    g_controllers['ControllerDashboardSearch'] = Controllers.ControllerDashboardSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DashboardSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
