//F8DE8A3D0D5EFF6543FE8A40E88E26CC
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovSuperClasBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovSuperClas = (function (_super) {
        __extends(ModelLovSuperClas, _super);
        function ModelLovSuperClas() {
            _super.apply(this, arguments);
        }
        return ModelLovSuperClas;
    })(Controllers.ModelLovSuperClasBase);
    Controllers.ModelLovSuperClas = ModelLovSuperClas;
    var ControllerLovSuperClas = (function (_super) {
        __extends(ControllerLovSuperClas, _super);
        function ControllerLovSuperClas($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovSuperClas($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovSuperClas;
    })(Controllers.ControllerLovSuperClasBase);
    Controllers.ControllerLovSuperClas = ControllerLovSuperClas;
    g_controllers['ControllerLovSuperClas'] = Controllers.ControllerLovSuperClas;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovSuperClas.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
