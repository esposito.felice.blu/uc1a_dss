//ADDFC979D044E77CB0EBD38E90265ACF
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/AgencyBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var Agency;
    (function (Agency) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(Agency.PageModelBase);
        Agency.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(Agency.PageControllerBase);
        Agency.PageController = PageController;
        g_controllers['Agency.PageController'] = Controllers.Agency.PageController;
        var ModelGrpAgency = (function (_super) {
            __extends(ModelGrpAgency, _super);
            function ModelGrpAgency() {
                _super.apply(this, arguments);
            }
            return ModelGrpAgency;
        })(Agency.ModelGrpAgencyBase);
        Agency.ModelGrpAgency = ModelGrpAgency;
        var ControllerGrpAgency = (function (_super) {
            __extends(ControllerGrpAgency, _super);
            function ControllerGrpAgency($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpAgency($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpAgency;
        })(Agency.ControllerGrpAgencyBase);
        Agency.ControllerGrpAgency = ControllerGrpAgency;
        g_controllers['Agency.ControllerGrpAgency'] = Controllers.Agency.ControllerGrpAgency;
    })(Agency = Controllers.Agency || (Controllers.Agency = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=Agency.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
