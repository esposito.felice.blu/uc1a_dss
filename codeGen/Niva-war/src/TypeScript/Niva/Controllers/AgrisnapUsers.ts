//44548F4D5A9CA35EFE398A1A2684FF58
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/AgrisnapUsersBase.ts" />

module Controllers.AgrisnapUsers {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['AgrisnapUsers.PageController'] = Controllers.AgrisnapUsers.PageController;



    export class ModelGrpAgrisnapUsers extends ModelGrpAgrisnapUsersBase {
    }


    export interface IScopeGrpAgrisnapUsers extends IScopeGrpAgrisnapUsersBase {
    }

    export class ControllerGrpAgrisnapUsers extends ControllerGrpAgrisnapUsersBase {
        constructor(
            public $scope: IScopeGrpAgrisnapUsers,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpAgrisnapUsers($scope) );
        }
    }
    g_controllers['AgrisnapUsers.ControllerGrpAgrisnapUsers'] = Controllers.AgrisnapUsers.ControllerGrpAgrisnapUsers;

    export class ModelGrpAgrisnapUsersProducers extends ModelGrpAgrisnapUsersProducersBase {
    }


    export interface IScopeGrpAgrisnapUsersProducers extends IScopeGrpAgrisnapUsersProducersBase {
    }

    export class ControllerGrpAgrisnapUsersProducers extends ControllerGrpAgrisnapUsersProducersBase {
        constructor(
            public $scope: IScopeGrpAgrisnapUsersProducers,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpAgrisnapUsersProducers($scope) );
        }
    }
    g_controllers['AgrisnapUsers.ControllerGrpAgrisnapUsersProducers'] = Controllers.AgrisnapUsers.ControllerGrpAgrisnapUsersProducers;
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
