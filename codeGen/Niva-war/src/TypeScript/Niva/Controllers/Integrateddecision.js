//8C6AFACF61D8EDC6C0A120CA3CFEFC5E
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/IntegrateddecisionBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var Integrateddecision;
    (function (Integrateddecision) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(Integrateddecision.PageModelBase);
        Integrateddecision.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(Integrateddecision.PageControllerBase);
        Integrateddecision.PageController = PageController;
        g_controllers['Integrateddecision.PageController'] = Controllers.Integrateddecision.PageController;
        var ModelGrpIntegrateddecision = (function (_super) {
            __extends(ModelGrpIntegrateddecision, _super);
            function ModelGrpIntegrateddecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpIntegrateddecision;
        })(Integrateddecision.ModelGrpIntegrateddecisionBase);
        Integrateddecision.ModelGrpIntegrateddecision = ModelGrpIntegrateddecision;
        var ControllerGrpIntegrateddecision = (function (_super) {
            __extends(ControllerGrpIntegrateddecision, _super);
            function ControllerGrpIntegrateddecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpIntegrateddecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpIntegrateddecision;
        })(Integrateddecision.ControllerGrpIntegrateddecisionBase);
        Integrateddecision.ControllerGrpIntegrateddecision = ControllerGrpIntegrateddecision;
        g_controllers['Integrateddecision.ControllerGrpIntegrateddecision'] = Controllers.Integrateddecision.ControllerGrpIntegrateddecision;
    })(Integrateddecision = Controllers.Integrateddecision || (Controllers.Integrateddecision = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=Integrateddecision.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
