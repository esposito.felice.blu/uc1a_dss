//B96CFA20DCFC54585D16D721D9D23A53
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelsIssuesActivitiesSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelParcelsIssuesActivitiesSearch = (function (_super) {
        __extends(ModelParcelsIssuesActivitiesSearch, _super);
        function ModelParcelsIssuesActivitiesSearch() {
            _super.apply(this, arguments);
        }
        return ModelParcelsIssuesActivitiesSearch;
    })(Controllers.ModelParcelsIssuesActivitiesSearchBase);
    Controllers.ModelParcelsIssuesActivitiesSearch = ModelParcelsIssuesActivitiesSearch;
    var ControllerParcelsIssuesActivitiesSearch = (function (_super) {
        __extends(ControllerParcelsIssuesActivitiesSearch, _super);
        function ControllerParcelsIssuesActivitiesSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelParcelsIssuesActivitiesSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerParcelsIssuesActivitiesSearch;
    })(Controllers.ControllerParcelsIssuesActivitiesSearchBase);
    Controllers.ControllerParcelsIssuesActivitiesSearch = ControllerParcelsIssuesActivitiesSearch;
    g_controllers['ControllerParcelsIssuesActivitiesSearch'] = Controllers.ControllerParcelsIssuesActivitiesSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelsIssuesActivitiesSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
