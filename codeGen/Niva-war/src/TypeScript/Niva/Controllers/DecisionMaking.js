//8921FD0AE168B829348FEE86B6049268
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DecisionMakingBase.ts" />
/// <reference path="../Services/MainService.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var DecisionMaking;
    (function (DecisionMaking) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(DecisionMaking.PageModelBase);
        DecisionMaking.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(DecisionMaking.PageControllerBase);
        DecisionMaking.PageController = PageController;
        g_controllers['DecisionMaking.PageController'] = Controllers.DecisionMaking.PageController;
        var ModelGrpDema = (function (_super) {
            __extends(ModelGrpDema, _super);
            function ModelGrpDema() {
                _super.apply(this, arguments);
            }
            return ModelGrpDema;
        })(DecisionMaking.ModelGrpDemaBase);
        DecisionMaking.ModelGrpDema = ModelGrpDema;
        var ControllerGrpDema = (function (_super) {
            __extends(ControllerGrpDema, _super);
            function ControllerGrpDema($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpDema($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            /**/
            ControllerGrpDema.prototype.actionUpdateIntegratedDecisionsNIssuesFromDema = function (decisionMaking) {
                var self = this;
                console.log("Method: updateIntegratedDecisionsNIssuesFromDema() called");
                //Check for modifications
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    //Add am Alert Message
                    NpTypes.AlertMessage.clearAlerts(this.$scope.pageModel);
                    NpTypes.AlertMessage.addDanger(this.$scope.pageModel, "Please Save before Running.");
                    return;
                }
                // Ask for Confirmation
                messageBox(self.$scope, self.Plato, "Please Confirm", "Do you really want to Update?\n This may take some time.", IconKind.QUESTION, [
                    new Tuple2("Yes", function () {
                        NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                        //call web service
                        var paramData = new Services.MainService_updateIntegratedDecisionsNIssuesFromDema_req();
                        paramData.demaId = decisionMaking.demaId;
                        console.log("Calling Integrated Decision & Issues Update Service....");
                        Services.MainService.updateIntegratedDecisionsNIssuesFromDema(self.$scope.pageModel.controller, paramData, function (respDataList) {
                            self.updateUI();
                            self.model.$scope.modelGrpDema.modelGrpParcelDecision.controller.updateGrid();
                            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                            NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Update was Completed Succesfully.");
                            respDataList.forEach(function (item) { return NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item); });
                        });
                    }),
                    new Tuple2("No", function () { })
                ], 1, 1, '20em');
            };
            ControllerGrpDema.prototype.finalizationAction = function (decisionMaking) {
                var self = this;
                console.log("Method: finalizationAction() called");
                //Check for modifications
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    //Add am Alert Message
                    NpTypes.AlertMessage.clearAlerts(this.$scope.pageModel);
                    NpTypes.AlertMessage.addDanger(this.$scope.pageModel, "Please Save before Running.");
                    return;
                }
                // Ask for Confirmation
                messageBox(self.$scope, self.Plato, "Please Confirm", "Do you really want to run?\n This may take some time.", IconKind.QUESTION, [
                    new Tuple2("Yes", function () {
                        NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                        //call web service
                        var paramData = new Services.MainService_runningDecisionMaking_req();
                        paramData.demaId = decisionMaking.demaId;
                        console.log("Calling Decision Making Service....");
                        Services.MainService.runningDecisionMaking(self.$scope.pageModel.controller, paramData, function (respDataList) {
                            self.updateUI();
                            self.model.$scope.modelGrpDema.modelGrpParcelDecision.controller.updateGrid();
                            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                            NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Run Completed Succesfully.");
                            respDataList.forEach(function (item) { return NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item); });
                        });
                    }),
                    new Tuple2("No", function () { })
                ], 1, 1, '18em');
            };
            ControllerGrpDema.prototype.decisionMakingHasBeenRun = function (decisionMaking) {
                var self = this;
                if (decisionMaking.hasBeenRun === null) {
                    decisionMaking.hasBeenRun = false;
                }
                return decisionMaking.hasBeenRun;
            };
            ControllerGrpDema.prototype.decisionMakingHasNotBeenRun = function () {
                var self = this;
                return !self.modelGrpDema.hasBeenRun;
            };
            ControllerGrpDema.prototype.parcelsIntegratedDecisionsAndIssuesHaveBeenPopulated = function (decisionMaking) {
                var self = this;
                if (decisionMaking.hasPopulatedIntegratedDecisionsAndIssues === null) {
                    decisionMaking.hasPopulatedIntegratedDecisionsAndIssues = false;
                }
                return decisionMaking.hasPopulatedIntegratedDecisionsAndIssues;
            };
            return ControllerGrpDema;
        })(DecisionMaking.ControllerGrpDemaBase);
        DecisionMaking.ControllerGrpDema = ControllerGrpDema;
        g_controllers['DecisionMaking.ControllerGrpDema'] = Controllers.DecisionMaking.ControllerGrpDema;
        var ModelGrpParcelDecision = (function (_super) {
            __extends(ModelGrpParcelDecision, _super);
            function ModelGrpParcelDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelDecision;
        })(DecisionMaking.ModelGrpParcelDecisionBase);
        DecisionMaking.ModelGrpParcelDecision = ModelGrpParcelDecision;
        var ControllerGrpParcelDecision = (function (_super) {
            __extends(ControllerGrpParcelDecision, _super);
            function ControllerGrpParcelDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            ControllerGrpParcelDecision.prototype.exportParcelDecisionsToCSVIsDisabled = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    return true;
                }
                return false;
            };
            ControllerGrpParcelDecision.prototype.exportParcelDecisionsToCSV_fileName = function () {
                var self = this;
                return "ParcelDecision" + "_" + Utils.convertDateToString(new Date(), "yyyy_MM_dd_HHmm") + ".csv";
            };
            ControllerGrpParcelDecision.prototype.exportParcelDecisionsToCSV = function () {
                var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DecisionMakingGrpParcelDecision_toCSV";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['__fields'] = self.getExcelFieldDefinitions();
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_decisionLight)) {
                    paramData['fsch_decisionLight'] = self.modelGrpParcelDecision.fsch_decisionLight;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelDecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpParcelDecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpParcelDecision.fsch_Parcel_Identifier;
                }
                NpTypes.AlertMessage.clearAlerts(self.PageModel);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    var data = response;
                    var blob = new Blob([data], { type: "text/csv;charset=utf-8" });
                    saveAs(blob, self.exportParcelDecisionsToCSV_fileName());
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                });
            };
            return ControllerGrpParcelDecision;
        })(DecisionMaking.ControllerGrpParcelDecisionBase);
        DecisionMaking.ControllerGrpParcelDecision = ControllerGrpParcelDecision;
        g_controllers['DecisionMaking.ControllerGrpParcelDecision'] = Controllers.DecisionMaking.ControllerGrpParcelDecision;
        var ModelGrpEcGroupCopy = (function (_super) {
            __extends(ModelGrpEcGroupCopy, _super);
            function ModelGrpEcGroupCopy() {
                _super.apply(this, arguments);
            }
            return ModelGrpEcGroupCopy;
        })(DecisionMaking.ModelGrpEcGroupCopyBase);
        DecisionMaking.ModelGrpEcGroupCopy = ModelGrpEcGroupCopy;
        var ControllerGrpEcGroupCopy = (function (_super) {
            __extends(ControllerGrpEcGroupCopy, _super);
            function ControllerGrpEcGroupCopy($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpEcGroupCopy($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            // show BRE tab
            ControllerGrpEcGroupCopy.prototype.isBREDisabled = function () {
                if ((!isVoid(DecisionMaking)) && ((this.Parent.recordtype === 1))) {
                    return false;
                }
                return true;
            };
            // show groups bellow  when crop level is Crop Type
            ControllerGrpEcGroupCopy.prototype.isEcGroupChildInv = function () {
                if (this.Current.cropLevel === 0) {
                    return false;
                }
                return true;
            };
            // show groups bellow when crop level is Land Cover
            ControllerGrpEcGroupCopy.prototype.isEcGroupCoverChildInv = function () {
                if (this.Current.cropLevel === 1) {
                    return false;
                }
                return true;
            };
            // show groups bellow when crop level is  Crop -> Land Cover
            ControllerGrpEcGroupCopy.prototype.isEcGroupSuperChildInv = function () {
                if (this.Current.cropLevel === 2) {
                    return false;
                }
                return true;
            };
            return ControllerGrpEcGroupCopy;
        })(DecisionMaking.ControllerGrpEcGroupCopyBase);
        DecisionMaking.ControllerGrpEcGroupCopy = ControllerGrpEcGroupCopy;
        g_controllers['DecisionMaking.ControllerGrpEcGroupCopy'] = Controllers.DecisionMaking.ControllerGrpEcGroupCopy;
        var ModelGrpEcCultCopy = (function (_super) {
            __extends(ModelGrpEcCultCopy, _super);
            function ModelGrpEcCultCopy() {
                _super.apply(this, arguments);
            }
            return ModelGrpEcCultCopy;
        })(DecisionMaking.ModelGrpEcCultCopyBase);
        DecisionMaking.ModelGrpEcCultCopy = ModelGrpEcCultCopy;
        var ControllerGrpEcCultCopy = (function (_super) {
            __extends(ControllerGrpEcCultCopy, _super);
            function ControllerGrpEcCultCopy($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpEcCultCopy($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpEcCultCopy;
        })(DecisionMaking.ControllerGrpEcCultCopyBase);
        DecisionMaking.ControllerGrpEcCultCopy = ControllerGrpEcCultCopy;
        g_controllers['DecisionMaking.ControllerGrpEcCultCopy'] = Controllers.DecisionMaking.ControllerGrpEcCultCopy;
        var ModelGrpEcCultDetailCopy = (function (_super) {
            __extends(ModelGrpEcCultDetailCopy, _super);
            function ModelGrpEcCultDetailCopy() {
                _super.apply(this, arguments);
            }
            return ModelGrpEcCultDetailCopy;
        })(DecisionMaking.ModelGrpEcCultDetailCopyBase);
        DecisionMaking.ModelGrpEcCultDetailCopy = ModelGrpEcCultDetailCopy;
        var ControllerGrpEcCultDetailCopy = (function (_super) {
            __extends(ControllerGrpEcCultDetailCopy, _super);
            function ControllerGrpEcCultDetailCopy($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpEcCultDetailCopy($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpEcCultDetailCopy;
        })(DecisionMaking.ControllerGrpEcCultDetailCopyBase);
        DecisionMaking.ControllerGrpEcCultDetailCopy = ControllerGrpEcCultDetailCopy;
        g_controllers['DecisionMaking.ControllerGrpEcCultDetailCopy'] = Controllers.DecisionMaking.ControllerGrpEcCultDetailCopy;
        var ModelEcCultCopyCover = (function (_super) {
            __extends(ModelEcCultCopyCover, _super);
            function ModelEcCultCopyCover() {
                _super.apply(this, arguments);
            }
            return ModelEcCultCopyCover;
        })(DecisionMaking.ModelEcCultCopyCoverBase);
        DecisionMaking.ModelEcCultCopyCover = ModelEcCultCopyCover;
        var ControllerEcCultCopyCover = (function (_super) {
            __extends(ControllerEcCultCopyCover, _super);
            function ControllerEcCultCopyCover($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelEcCultCopyCover($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerEcCultCopyCover;
        })(DecisionMaking.ControllerEcCultCopyCoverBase);
        DecisionMaking.ControllerEcCultCopyCover = ControllerEcCultCopyCover;
        g_controllers['DecisionMaking.ControllerEcCultCopyCover'] = Controllers.DecisionMaking.ControllerEcCultCopyCover;
        var ModelEcCultDetailCopyCover = (function (_super) {
            __extends(ModelEcCultDetailCopyCover, _super);
            function ModelEcCultDetailCopyCover() {
                _super.apply(this, arguments);
            }
            return ModelEcCultDetailCopyCover;
        })(DecisionMaking.ModelEcCultDetailCopyCoverBase);
        DecisionMaking.ModelEcCultDetailCopyCover = ModelEcCultDetailCopyCover;
        var ControllerEcCultDetailCopyCover = (function (_super) {
            __extends(ControllerEcCultDetailCopyCover, _super);
            function ControllerEcCultDetailCopyCover($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelEcCultDetailCopyCover($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerEcCultDetailCopyCover;
        })(DecisionMaking.ControllerEcCultDetailCopyCoverBase);
        DecisionMaking.ControllerEcCultDetailCopyCover = ControllerEcCultDetailCopyCover;
        g_controllers['DecisionMaking.ControllerEcCultDetailCopyCover'] = Controllers.DecisionMaking.ControllerEcCultDetailCopyCover;
        var ModelEcCultCopySuper = (function (_super) {
            __extends(ModelEcCultCopySuper, _super);
            function ModelEcCultCopySuper() {
                _super.apply(this, arguments);
            }
            return ModelEcCultCopySuper;
        })(DecisionMaking.ModelEcCultCopySuperBase);
        DecisionMaking.ModelEcCultCopySuper = ModelEcCultCopySuper;
        var ControllerEcCultCopySuper = (function (_super) {
            __extends(ControllerEcCultCopySuper, _super);
            function ControllerEcCultCopySuper($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelEcCultCopySuper($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerEcCultCopySuper;
        })(DecisionMaking.ControllerEcCultCopySuperBase);
        DecisionMaking.ControllerEcCultCopySuper = ControllerEcCultCopySuper;
        g_controllers['DecisionMaking.ControllerEcCultCopySuper'] = Controllers.DecisionMaking.ControllerEcCultCopySuper;
        var ModelEcCultDetailCopySuper = (function (_super) {
            __extends(ModelEcCultDetailCopySuper, _super);
            function ModelEcCultDetailCopySuper() {
                _super.apply(this, arguments);
            }
            return ModelEcCultDetailCopySuper;
        })(DecisionMaking.ModelEcCultDetailCopySuperBase);
        DecisionMaking.ModelEcCultDetailCopySuper = ModelEcCultDetailCopySuper;
        var ControllerEcCultDetailCopySuper = (function (_super) {
            __extends(ControllerEcCultDetailCopySuper, _super);
            function ControllerEcCultDetailCopySuper($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelEcCultDetailCopySuper($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerEcCultDetailCopySuper;
        })(DecisionMaking.ControllerEcCultDetailCopySuperBase);
        DecisionMaking.ControllerEcCultDetailCopySuper = ControllerEcCultDetailCopySuper;
        g_controllers['DecisionMaking.ControllerEcCultDetailCopySuper'] = Controllers.DecisionMaking.ControllerEcCultDetailCopySuper;
    })(DecisionMaking = Controllers.DecisionMaking || (Controllers.DecisionMaking = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DecisionMaking.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
