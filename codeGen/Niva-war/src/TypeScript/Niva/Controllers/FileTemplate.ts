//C717CA776607FCBAA78BAB0FC26317ED
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/FileTemplateBase.ts" />

module Controllers.FileTemplate {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['FileTemplate.PageController'] = Controllers.FileTemplate.PageController;



    export class ModelGrpFileTemplate extends ModelGrpFileTemplateBase {
    }


    export interface IScopeGrpFileTemplate extends IScopeGrpFileTemplateBase {
    }

    export class ControllerGrpFileTemplate extends ControllerGrpFileTemplateBase {
        constructor(
            public $scope: IScopeGrpFileTemplate,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpFileTemplate($scope) );
        }
    }
    g_controllers['FileTemplate.ControllerGrpFileTemplate'] = Controllers.FileTemplate.ControllerGrpFileTemplate;

    export class ModelGrpTemplateColumn extends ModelGrpTemplateColumnBase {
    }


    export interface IScopeGrpTemplateColumn extends IScopeGrpTemplateColumnBase {
    }

    export class ControllerGrpTemplateColumn extends ControllerGrpTemplateColumnBase {
        constructor(
            public $scope: IScopeGrpTemplateColumn,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpTemplateColumn($scope) );
        }
    }
    g_controllers['FileTemplate.ControllerGrpTemplateColumn'] = Controllers.FileTemplate.ControllerGrpTemplateColumn;
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
