//B6A1254FF0CD9C4954B122BBE52C3398
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovEccuActivityBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovEccuActivity = (function (_super) {
        __extends(ModelLovEccuActivity, _super);
        function ModelLovEccuActivity() {
            _super.apply(this, arguments);
        }
        return ModelLovEccuActivity;
    })(Controllers.ModelLovEccuActivityBase);
    Controllers.ModelLovEccuActivity = ModelLovEccuActivity;
    var ControllerLovEccuActivity = (function (_super) {
        __extends(ControllerLovEccuActivity, _super);
        function ControllerLovEccuActivity($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovEccuActivity($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovEccuActivity;
    })(Controllers.ControllerLovEccuActivityBase);
    Controllers.ControllerLovEccuActivity = ControllerLovEccuActivity;
    g_controllers['ControllerLovEccuActivity'] = Controllers.ControllerLovEccuActivity;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovEccuActivity.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
