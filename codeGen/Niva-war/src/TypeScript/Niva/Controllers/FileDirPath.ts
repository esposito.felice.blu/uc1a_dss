//61482D854C2B18B1E94FAD100EC5F115
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/FileDirPathBase.ts" />

module Controllers.FileDirPath {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['FileDirPath.PageController'] = Controllers.FileDirPath.PageController;



    export class ModelGrpFileDirPath extends ModelGrpFileDirPathBase {
    }


    export interface IScopeGrpFileDirPath extends IScopeGrpFileDirPathBase {
    }

    export class ControllerGrpFileDirPath extends ControllerGrpFileDirPathBase {
        constructor(
            public $scope: IScopeGrpFileDirPath,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpFileDirPath($scope) );
        }
    }
    g_controllers['FileDirPath.ControllerGrpFileDirPath'] = Controllers.FileDirPath.ControllerGrpFileDirPath;
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
