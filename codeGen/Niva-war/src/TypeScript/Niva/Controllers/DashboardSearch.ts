//FA8DE004B39630B719391B8B599AAC46
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DashboardSearchBase.ts" />

module Controllers {

    export class ModelDashboardSearch extends ModelDashboardSearchBase {
    }

    export interface IScopeDashboardSearch extends IScopeDashboardSearchBase {
    }

    export class ControllerDashboardSearch extends ControllerDashboardSearchBase {
        
        constructor(
            public $scope: IScopeDashboardSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelDashboardSearch($scope));
        }
    }

    g_controllers['ControllerDashboardSearch'] = Controllers.ControllerDashboardSearch;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
