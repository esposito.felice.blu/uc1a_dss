//8921FD0AE168B829348FEE86B6049268
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DecisionMakingBase.ts" />
/// <reference path="../Services/MainService.ts" />

module Controllers.DecisionMaking {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {

        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['DecisionMaking.PageController'] = Controllers.DecisionMaking.PageController;



    export class ModelGrpDema extends ModelGrpDemaBase {
    }


    export interface IScopeGrpDema extends IScopeGrpDemaBase {
    }

    export class ControllerGrpDema extends ControllerGrpDemaBase {
        constructor(
            public $scope: IScopeGrpDema,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpDema($scope));
        }

        /**/
         public actionUpdateIntegratedDecisionsNIssuesFromDema(decisionMaking: Entities.DecisionMaking) {

            var self = this;
            console.log("Method: updateIntegratedDecisionsNIssuesFromDema() called");

            //Check for modifications
            if (self.$scope.globals.isCurrentTransactionDirty) {

                //Add am Alert Message
                NpTypes.AlertMessage.clearAlerts(this.$scope.pageModel);
                NpTypes.AlertMessage.addDanger(this.$scope.pageModel, "Please Save before Running.");
                return;
            }

            // Ask for Confirmation

            messageBox(self.$scope, self.Plato, "Please Confirm", "Do you really want to Update?\n This may take some time.",
                IconKind.QUESTION,
                [
                    new Tuple2("Yes", () => {

                        NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);

                        //call web service
                        var paramData: Services.MainService_updateIntegratedDecisionsNIssuesFromDema_req = new Services.MainService_updateIntegratedDecisionsNIssuesFromDema_req();
                        paramData.demaId = decisionMaking.demaId;
                        console.log("Calling Integrated Decision & Issues Update Service....");
                        Services.MainService.updateIntegratedDecisionsNIssuesFromDema(self.$scope.pageModel.controller, paramData, respDataList => {

                            self.updateUI();

                            self.model.$scope.modelGrpDema.modelGrpParcelDecision.controller.updateGrid();

                            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                            NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Update was Completed Succesfully.");

                            respDataList.forEach(item => NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item));
                        });
                    }),
                    new Tuple2("No", () => { })
                ], 1, 1, '18em');

        }


        public finalizationAction(decisionMaking: Entities.DecisionMaking) {

            var self = this;
            console.log("Method: finalizationAction() called");

            //Check for modifications
            if (self.$scope.globals.isCurrentTransactionDirty) {

                //Add am Alert Message
                NpTypes.AlertMessage.clearAlerts(this.$scope.pageModel);
                NpTypes.AlertMessage.addDanger(this.$scope.pageModel, "Please Save before Running.");
                return;
            }

            // Ask for Confirmation

            messageBox(self.$scope, self.Plato, "Please Confirm", "Do you really want to run?\n This may take some time.",
                IconKind.QUESTION,
                [
                    new Tuple2("Yes", () => {

                        NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);

                        //call web service
                        var paramData: Services.MainService_runningDecisionMaking_req = new Services.MainService_runningDecisionMaking_req();
                        paramData.demaId = decisionMaking.demaId;
                        console.log("Calling Decision Making Service....");
                        Services.MainService.runningDecisionMaking(self.$scope.pageModel.controller, paramData, respDataList => {

                            self.updateUI();

                            self.model.$scope.modelGrpDema.modelGrpParcelDecision.controller.updateGrid();

                            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                            NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Run Completed Succesfully.");

                            respDataList.forEach(item => NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item));
                        });
                    }),
                    new Tuple2("No", () => { })
                ], 1, 1, '18em');

        }




        public decisionMakingHasBeenRun(decisionMaking: Entities.DecisionMaking): boolean {
            var self = this;
            if (decisionMaking.hasBeenRun === null) {
                decisionMaking.hasBeenRun = false;
            }
            return decisionMaking.hasBeenRun;
        }

        public decisionMakingHasNotBeenRun(): boolean {
            var self = this;
            return !self.modelGrpDema.hasBeenRun;
        }


        public parcelsIntegratedDecisionsAndIssuesHaveBeenPopulated (decisionMaking:Entities.DecisionMaking):boolean { 
            var self = this;
            if (decisionMaking.hasPopulatedIntegratedDecisionsAndIssues === null) {
                decisionMaking.hasPopulatedIntegratedDecisionsAndIssues = false;
            }
            return decisionMaking.hasPopulatedIntegratedDecisionsAndIssues;
        }



    }
    g_controllers['DecisionMaking.ControllerGrpDema'] = Controllers.DecisionMaking.ControllerGrpDema;

    export class ModelGrpParcelDecision extends ModelGrpParcelDecisionBase {
    }


    export interface IScopeGrpParcelDecision extends IScopeGrpParcelDecisionBase {
    }

    export class ControllerGrpParcelDecision extends ControllerGrpParcelDecisionBase {
        constructor(
            public $scope: IScopeGrpParcelDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelDecision($scope));
        }

        public exportParcelDecisionsToCSVIsDisabled(): boolean {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                return true;
            }
            return false;
        }
        public exportParcelDecisionsToCSV_fileName() {
            var self = this;
            return "ParcelDecision" + "_" + Utils.convertDateToString(new Date(), "yyyy_MM_dd_HHmm") + ".csv";
        }
        public exportParcelDecisionsToCSV() {
            var self = this;
            var wsPath = "ParcelDecision/findAllByCriteriaRange_DecisionMakingGrpParcelDecision_toCSV";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['__fields'] = self.getExcelFieldDefinitions();
            if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                paramData['demaId_demaId'] = self.Parent.demaId;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_decisionLight)) {
                paramData['fsch_decisionLight'] = self.modelGrpParcelDecision.fsch_decisionLight;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_prodCode)) {
                paramData['fsch_prodCode'] = self.modelGrpParcelDecision.fsch_prodCode;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Code)) {
                paramData['fsch_Code'] = self.modelGrpParcelDecision.fsch_Code;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Parcel_Identifier)) {
                paramData['fsch_Parcel_Identifier'] = self.modelGrpParcelDecision.fsch_Parcel_Identifier;
            }

            NpTypes.AlertMessage.clearAlerts(self.PageModel);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                    var data = response;
                    var blob = new Blob([data], { type: "text/csv;charset=utf-8" });
                    saveAs(blob, self.exportParcelDecisionsToCSV_fileName());
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                });

        }






    }
    g_controllers['DecisionMaking.ControllerGrpParcelDecision'] = Controllers.DecisionMaking.ControllerGrpParcelDecision;

    export class ModelGrpEcGroupCopy extends ModelGrpEcGroupCopyBase {
    }


    export interface IScopeGrpEcGroupCopy extends IScopeGrpEcGroupCopyBase {
    }

    export class ControllerGrpEcGroupCopy extends ControllerGrpEcGroupCopyBase {
        constructor(
            public $scope: IScopeGrpEcGroupCopy,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpEcGroupCopy($scope));
        }

        // show BRE tab
        public isBREDisabled(): boolean {
            if ((!isVoid(DecisionMaking)) && ((this.Parent.recordtype === 1))) {
                return false;
            }
            return true;
        }
        // show groups bellow  when crop level is Crop Type
        public isEcGroupChildInv(): boolean {
            if (this.Current.cropLevel === 0) {
                return false;
            }
            return true;
        }


        // show groups bellow when crop level is Land Cover
        public isEcGroupCoverChildInv(): boolean {
            if (this.Current.cropLevel === 1) {
                return false;
            }
            return true;
        }

        // show groups bellow when crop level is  Crop -> Land Cover
        public isEcGroupSuperChildInv(): boolean {
            if (this.Current.cropLevel === 2) {
                return false;
            }
            return true;
        }
    }
    g_controllers['DecisionMaking.ControllerGrpEcGroupCopy'] = Controllers.DecisionMaking.ControllerGrpEcGroupCopy;

    export class ModelGrpEcCultCopy extends ModelGrpEcCultCopyBase {
    }


    export interface IScopeGrpEcCultCopy extends IScopeGrpEcCultCopyBase {
    }

    export class ControllerGrpEcCultCopy extends ControllerGrpEcCultCopyBase {
        constructor(
            public $scope: IScopeGrpEcCultCopy,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpEcCultCopy($scope));
        }
    }
    g_controllers['DecisionMaking.ControllerGrpEcCultCopy'] = Controllers.DecisionMaking.ControllerGrpEcCultCopy;

    export class ModelGrpEcCultDetailCopy extends ModelGrpEcCultDetailCopyBase {
    }


    export interface IScopeGrpEcCultDetailCopy extends IScopeGrpEcCultDetailCopyBase {
    }

    export class ControllerGrpEcCultDetailCopy extends ControllerGrpEcCultDetailCopyBase {
        constructor(
            public $scope: IScopeGrpEcCultDetailCopy,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpEcCultDetailCopy($scope));
        }
    }
    g_controllers['DecisionMaking.ControllerGrpEcCultDetailCopy'] = Controllers.DecisionMaking.ControllerGrpEcCultDetailCopy;

    export class ModelEcCultCopyCover extends ModelEcCultCopyCoverBase {
    }


    export interface IScopeEcCultCopyCover extends IScopeEcCultCopyCoverBase {
    }

    export class ControllerEcCultCopyCover extends ControllerEcCultCopyCoverBase {
        constructor(
            public $scope: IScopeEcCultCopyCover,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelEcCultCopyCover($scope));
        }
    }
    g_controllers['DecisionMaking.ControllerEcCultCopyCover'] = Controllers.DecisionMaking.ControllerEcCultCopyCover;

    export class ModelEcCultDetailCopyCover extends ModelEcCultDetailCopyCoverBase {
    }


    export interface IScopeEcCultDetailCopyCover extends IScopeEcCultDetailCopyCoverBase {
    }

    export class ControllerEcCultDetailCopyCover extends ControllerEcCultDetailCopyCoverBase {
        constructor(
            public $scope: IScopeEcCultDetailCopyCover,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelEcCultDetailCopyCover($scope));
        }
    }
    g_controllers['DecisionMaking.ControllerEcCultDetailCopyCover'] = Controllers.DecisionMaking.ControllerEcCultDetailCopyCover;

    export class ModelEcCultCopySuper extends ModelEcCultCopySuperBase {
    }


    export interface IScopeEcCultCopySuper extends IScopeEcCultCopySuperBase {
    }

    export class ControllerEcCultCopySuper extends ControllerEcCultCopySuperBase {
        constructor(
            public $scope: IScopeEcCultCopySuper,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelEcCultCopySuper($scope));
        }
    }
    g_controllers['DecisionMaking.ControllerEcCultCopySuper'] = Controllers.DecisionMaking.ControllerEcCultCopySuper;

    export class ModelEcCultDetailCopySuper extends ModelEcCultDetailCopySuperBase {
    }


    export interface IScopeEcCultDetailCopySuper extends IScopeEcCultDetailCopySuperBase {
    }

    export class ControllerEcCultDetailCopySuper extends ControllerEcCultDetailCopySuperBase {
        constructor(
            public $scope: IScopeEcCultDetailCopySuper,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelEcCultDetailCopySuper($scope));
        }
    }
    g_controllers['DecisionMaking.ControllerEcCultDetailCopySuper'] = Controllers.DecisionMaking.ControllerEcCultDetailCopySuper;

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
