//89686FBEB0A899B6E187414A4E96473B
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/IntegrateddecisionSearchBase.ts" />

module Controllers {

    export class ModelIntegrateddecisionSearch extends ModelIntegrateddecisionSearchBase {
    }

    export interface IScopeIntegrateddecisionSearch extends IScopeIntegrateddecisionSearchBase {
    }

    export class ControllerIntegrateddecisionSearch extends ControllerIntegrateddecisionSearchBase {
        
        constructor(
            public $scope: IScopeIntegrateddecisionSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelIntegrateddecisionSearch($scope));
        }
    }

    g_controllers['ControllerIntegrateddecisionSearch'] = Controllers.ControllerIntegrateddecisionSearch;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
