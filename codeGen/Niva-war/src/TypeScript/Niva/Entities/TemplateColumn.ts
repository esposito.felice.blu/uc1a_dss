//8BC5DADB19580B5822BD906B89FC71B9
//Εxtended classes
/// <reference path="../EntitiesBase/TemplateColumnBase.ts" />

module Entities {

    export class TemplateColumn extends Entities.TemplateColumnBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<TemplateColumn> {
            return Entities.TemplateColumnBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
