//D208FE195FBE1BBF67D5B885AF9D0B6F
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Parcel = (function (_super) {
        __extends(Parcel, _super);
        function Parcel() {
            _super.apply(this, arguments);
        }
        Parcel.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ParcelBase.fromJSONComplete(data, deserializedEntities);
        };
        return Parcel;
    })(Entities.ParcelBase);
    Entities.Parcel = Parcel;
})(Entities || (Entities = {}));
//# sourceMappingURL=Parcel.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
