//789955616F10056F04D87443A31F5BB3
//Εxtended classes
/// <reference path="../EntitiesBase/FmisUploadBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FmisUpload = (function (_super) {
        __extends(FmisUpload, _super);
        function FmisUpload() {
            _super.apply(this, arguments);
        }
        FmisUpload.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.FmisUploadBase.fromJSONComplete(data, deserializedEntities);
        };
        return FmisUpload;
    })(Entities.FmisUploadBase);
    Entities.FmisUpload = FmisUpload;
})(Entities || (Entities = {}));
//# sourceMappingURL=FmisUpload.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
