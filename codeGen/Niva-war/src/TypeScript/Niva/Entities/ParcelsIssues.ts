//996CBA9563095D58D1256540E7DC5808
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />

module Entities {

    export class ParcelsIssues extends Entities.ParcelsIssuesBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelsIssues> {
            return Entities.ParcelsIssuesBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
