//52D2E6DC036609E5B2009F7E47994BAA
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelDecisionBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcelDecision = (function (_super) {
        __extends(ParcelDecision, _super);
        function ParcelDecision() {
            _super.apply(this, arguments);
        }
        ParcelDecision.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ParcelDecisionBase.fromJSONComplete(data, deserializedEntities);
        };
        return ParcelDecision;
    })(Entities.ParcelDecisionBase);
    Entities.ParcelDecision = ParcelDecision;
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcelDecision.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
