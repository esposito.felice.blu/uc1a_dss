//8AADE3B9B40495B6BFCD580F5058EFB4
//Εxtended classes
/// <reference path="../EntitiesBase/AgrisnapUsersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var AgrisnapUsers = (function (_super) {
        __extends(AgrisnapUsers, _super);
        function AgrisnapUsers() {
            _super.apply(this, arguments);
        }
        AgrisnapUsers.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.AgrisnapUsersBase.fromJSONComplete(data, deserializedEntities);
        };
        return AgrisnapUsers;
    })(Entities.AgrisnapUsersBase);
    Entities.AgrisnapUsers = AgrisnapUsers;
})(Entities || (Entities = {}));
//# sourceMappingURL=AgrisnapUsers.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
