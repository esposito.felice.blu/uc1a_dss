//AB55F02D962DE26DCC82D5E48C7D1CC4
//Εxtended classes
/// <reference path="../EntitiesBase/SuperClassDetailBase.ts" />

module Entities {

    export class SuperClassDetail extends Entities.SuperClassDetailBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<SuperClassDetail> {
            return Entities.SuperClassDetailBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
