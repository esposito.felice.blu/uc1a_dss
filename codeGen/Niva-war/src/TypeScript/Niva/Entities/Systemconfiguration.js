//4D8336E4660E913332B629C22B184042
//Εxtended classes
/// <reference path="../EntitiesBase/SystemconfigurationBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Systemconfiguration = (function (_super) {
        __extends(Systemconfiguration, _super);
        function Systemconfiguration() {
            _super.apply(this, arguments);
        }
        Systemconfiguration.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.SystemconfigurationBase.fromJSONComplete(data, deserializedEntities);
        };
        return Systemconfiguration;
    })(Entities.SystemconfigurationBase);
    Entities.Systemconfiguration = Systemconfiguration;
})(Entities || (Entities = {}));
//# sourceMappingURL=Systemconfiguration.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
