//D91DACA9547C6E73E350E892D310674F
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelsIssuesStatusPerDemaBase.ts" />

module Entities {

    export class ParcelsIssuesStatusPerDema extends Entities.ParcelsIssuesStatusPerDemaBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelsIssuesStatusPerDema> {
            return Entities.ParcelsIssuesStatusPerDemaBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
