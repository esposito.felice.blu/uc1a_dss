//AF7491E2ABA8012850E97561E72CBD3E
//Εxtended classes
/// <reference path="../EntitiesBase/ClassificationBase.ts" />

module Entities {

    export class Classification extends Entities.ClassificationBase {
        static fromJSONComplete(data: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}): Array<Classification> {
            return Entities.ClassificationBase.fromJSONComplete(data, deserializedEntities);
        }





    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
