//75C6CAA90BB656AFB63C8002C1120C7D
//Εxtended classes
/// <reference path="../EntitiesBase/ExcelFileBase.ts" />

module Entities {

    export class ExcelFile extends Entities.ExcelFileBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ExcelFile> {
            return Entities.ExcelFileBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
