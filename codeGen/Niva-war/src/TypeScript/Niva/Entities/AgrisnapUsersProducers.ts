//06448CD982E32B903599BB46B97B1281
//Εxtended classes
/// <reference path="../EntitiesBase/AgrisnapUsersProducersBase.ts" />

module Entities {

    export class AgrisnapUsersProducers extends Entities.AgrisnapUsersProducersBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<AgrisnapUsersProducers> {
            return Entities.AgrisnapUsersProducersBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
