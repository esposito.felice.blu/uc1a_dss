//67482CCA7D76EDC69BE72E1317DB9B50
//Εxtended classes
/// <reference path="../EntitiesBase/ProducersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Producers = (function (_super) {
        __extends(Producers, _super);
        function Producers() {
            _super.apply(this, arguments);
        }
        Producers.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ProducersBase.fromJSONComplete(data, deserializedEntities);
        };
        return Producers;
    })(Entities.ProducersBase);
    Entities.Producers = Producers;
})(Entities || (Entities = {}));
//# sourceMappingURL=Producers.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
