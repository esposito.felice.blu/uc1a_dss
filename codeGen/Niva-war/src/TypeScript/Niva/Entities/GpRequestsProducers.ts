//6815A9E961B863277EDDD671329E27EE
//Εxtended classes
/// <reference path="../EntitiesBase/GpRequestsProducersBase.ts" />

module Entities {

    export class GpRequestsProducers extends Entities.GpRequestsProducersBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpRequestsProducers> {
            return Entities.GpRequestsProducersBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
