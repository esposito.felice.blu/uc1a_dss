//1D330186A0A22CF942771EB1B15BC664
//Εxtended classes
/// <reference path="../EntitiesBase/CultivationBase.ts" />

module Entities {

    export class Cultivation extends Entities.CultivationBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Cultivation> {
            return Entities.CultivationBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
