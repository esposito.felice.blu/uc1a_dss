//6874ABE2275E13EC443F4559D1C76B58
//Εxtended classes
/// <reference path="../EntitiesBase/EcCultDetailBase.ts" />

module Entities {

    export class EcCultDetail extends Entities.EcCultDetailBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcCultDetail> {
            return Entities.EcCultDetailBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
