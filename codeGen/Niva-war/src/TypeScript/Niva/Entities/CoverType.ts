//DE0927D8627BCB8144CD9F45A87C9685
//Εxtended classes
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />

module Entities {

    export class CoverType extends Entities.CoverTypeBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<CoverType> {
            return Entities.CoverTypeBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
