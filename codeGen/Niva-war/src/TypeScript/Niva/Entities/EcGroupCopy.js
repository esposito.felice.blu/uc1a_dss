//B22C91017AA9CB00834401391B017D44
//Εxtended classes
/// <reference path="../EntitiesBase/EcGroupCopyBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcGroupCopy = (function (_super) {
        __extends(EcGroupCopy, _super);
        function EcGroupCopy() {
            _super.apply(this, arguments);
        }
        EcGroupCopy.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.EcGroupCopyBase.fromJSONComplete(data, deserializedEntities);
        };
        return EcGroupCopy;
    })(Entities.EcGroupCopyBase);
    Entities.EcGroupCopy = EcGroupCopy;
})(Entities || (Entities = {}));
//# sourceMappingURL=EcGroupCopy.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
