//B3935997F0AF3CEBA7F831687B4CE271
//Εxtended classes
/// <reference path="../EntitiesBase/FileDirPathBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FileDirPath = (function (_super) {
        __extends(FileDirPath, _super);
        function FileDirPath() {
            _super.apply(this, arguments);
        }
        FileDirPath.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.FileDirPathBase.fromJSONComplete(data, deserializedEntities);
        };
        return FileDirPath;
    })(Entities.FileDirPathBase);
    Entities.FileDirPath = FileDirPath;
})(Entities || (Entities = {}));
//# sourceMappingURL=FileDirPath.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
