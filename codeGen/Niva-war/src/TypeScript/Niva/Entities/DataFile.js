//9BE7F1FADC236E878A0AA9F4E8268C0D
//Εxtended classes
/// <reference path="../EntitiesBase/DataFileBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var DataFile = (function (_super) {
        __extends(DataFile, _super);
        function DataFile() {
            _super.apply(this, arguments);
        }
        DataFile.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.DataFileBase.fromJSONComplete(data, deserializedEntities);
        };
        return DataFile;
    })(Entities.DataFileBase);
    Entities.DataFile = DataFile;
})(Entities || (Entities = {}));
//# sourceMappingURL=DataFile.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
