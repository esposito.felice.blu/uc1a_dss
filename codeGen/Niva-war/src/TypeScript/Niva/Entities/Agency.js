//E9BCA2B557C99BC1BD6A2BA0E968DAA6
//Εxtended classes
/// <reference path="../EntitiesBase/AgencyBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Agency = (function (_super) {
        __extends(Agency, _super);
        function Agency() {
            _super.apply(this, arguments);
        }
        Agency.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.AgencyBase.fromJSONComplete(data, deserializedEntities);
        };
        return Agency;
    })(Entities.AgencyBase);
    Entities.Agency = Agency;
})(Entities || (Entities = {}));
//# sourceMappingURL=Agency.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
