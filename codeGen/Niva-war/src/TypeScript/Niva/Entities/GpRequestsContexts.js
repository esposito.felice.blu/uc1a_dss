//588E0044C92B5C55BADB13B586228E44
//Εxtended classes
/// <reference path="../EntitiesBase/GpRequestsContextsBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var GpRequestsContexts = (function (_super) {
        __extends(GpRequestsContexts, _super);
        function GpRequestsContexts() {
            _super.apply(this, arguments);
        }
        GpRequestsContexts.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.GpRequestsContextsBase.fromJSONComplete(data, deserializedEntities);
        };
        return GpRequestsContexts;
    })(Entities.GpRequestsContextsBase);
    Entities.GpRequestsContexts = GpRequestsContexts;
})(Entities || (Entities = {}));
//# sourceMappingURL=GpRequestsContexts.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
