//AB9101FC56873AB5E2CF4A361D44FD73
//Εxtended classes
/// <reference path="../EntitiesBase/IntegrateddecisionBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Integrateddecision = (function (_super) {
        __extends(Integrateddecision, _super);
        function Integrateddecision() {
            _super.apply(this, arguments);
        }
        Integrateddecision.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.IntegrateddecisionBase.fromJSONComplete(data, deserializedEntities);
        };
        return Integrateddecision;
    })(Entities.IntegrateddecisionBase);
    Entities.Integrateddecision = Integrateddecision;
})(Entities || (Entities = {}));
//# sourceMappingURL=Integrateddecision.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
