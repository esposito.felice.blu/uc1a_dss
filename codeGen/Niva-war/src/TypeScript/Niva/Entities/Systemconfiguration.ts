//4D8336E4660E913332B629C22B184042
//Εxtended classes
/// <reference path="../EntitiesBase/SystemconfigurationBase.ts" />

module Entities {

    export class Systemconfiguration extends Entities.SystemconfigurationBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Systemconfiguration> {
            return Entities.SystemconfigurationBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
