//B3935997F0AF3CEBA7F831687B4CE271
//Εxtended classes
/// <reference path="../EntitiesBase/FileDirPathBase.ts" />

module Entities {

    export class FileDirPath extends Entities.FileDirPathBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FileDirPath> {
            return Entities.FileDirPathBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
