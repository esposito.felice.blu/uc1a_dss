//B5F8DD633E675037F1E2C1DF7BC3F96B
//Εxtended classes
/// <reference path="../EntitiesBase/GeotagphotoBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Geotagphoto = (function (_super) {
        __extends(Geotagphoto, _super);
        function Geotagphoto() {
            _super.apply(this, arguments);
        }
        Geotagphoto.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.GeotagphotoBase.fromJSONComplete(data, deserializedEntities);
        };
        return Geotagphoto;
    })(Entities.GeotagphotoBase);
    Entities.Geotagphoto = Geotagphoto;
})(Entities || (Entities = {}));
//# sourceMappingURL=Geotagphoto.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
