-- MT 20210301

-- Change the length of 'name' column to 60 chars.
ALTER TABLE niva.cover_type ALTER COLUMN name TYPE character varying(60);

-- This temporary drop is needed due to the connection to 'niva.cultivation'. The View will be re-created at a next step.
DROP VIEW IF EXISTS niva.v_parcel_decision_ws_export;

-- Change the length of name column to 200 chars.
ALTER TABLE niva.cultivation ALTER COLUMN name TYPE character varying(200);

-- Create this View again, as the table niva.cultivation has been changed.
	-- View: niva.v_parcel_decision_ws_export

	-- DROP VIEW niva.v_parcel_decision_ws_export;

CREATE OR REPLACE VIEW niva.v_parcel_decision_ws_export
 AS
 SELECT pade.dema_id AS view_key,
        CASE
            WHEN pade.decision_light = 1 THEN 'Green'::text
            WHEN pade.decision_light = 2 THEN 'Yellow'::text
            WHEN pade.decision_light = 3 THEN 'Red'::text
            WHEN pade.decision_light = 4 THEN 'Undefined'::text
            ELSE NULL::text
        END AS decision_light,
    parc.prod_code AS farmer_code,
    parc.code AS parcel_code,
    parc.identifier,
    cult_decl.code AS cultivation_declared_code,
    cult_decl.name AS cultivation_declared_name,
    cult_pred.code AS cultivation_1st_prediction_code,
    cult_pred.name AS cultivation_1st_prediction_name,
    pcla.prob_pred::numeric(3,2) AS probability_1st_prediction,
    cult_pred2.code AS cultivation_2nd_prediction_code,
    cult_pred2.name AS cultivation_2nd_prediction_name,
    pcla.prob_pred2::numeric(3,2) AS probability_2nd_prediction,
    ecgr.name AS ecgr_name,
    dema.date_time AS dema_date
   FROM niva.decision_making dema
     JOIN niva.parcel_decision pade ON pade.dema_id = dema.dema_id
     JOIN niva.parcel_class pcla ON pade.pcla_id = pcla.pcla_id
     JOIN niva.ec_group ecgr ON dema.ecgr_id = ecgr.ecgr_id
     JOIN niva.cultivation cult_decl ON pcla.cult_id_decl = cult_decl.cult_id
     LEFT JOIN niva.cultivation cult_pred ON pcla.cult_id_pred = cult_pred.cult_id
     LEFT JOIN niva.cultivation cult_pred2 ON pcla.cult_id_pred = cult_pred2.cult_id
     JOIN niva.parcel parc ON pcla.parc_id = parc.parc_id;


-- Replaces the import_crops function, to implement more sophisticated import.

	-- FUNCTION: niva.import_crops(character varying, integer, character varying, integer)


DROP FUNCTION IF EXISTS niva.import_crops;

CREATE OR REPLACE FUNCTION niva.import_crops(
	i_name character varying,
	i_code integer,
	i_coty_name character varying,
	i_excel_id integer)
    RETURNS numeric
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
-- MT 20210301
DECLARE
    n_name VARCHAR(200);   -- MT -Ready for longer names (>60)
    n_code INTEGER;
	n_coty_id INTEGER;

begin
	
	
	-- Check if a Crop with the same name already exists with another code
	-- or if a Crop with the same code already exists with another name.
	
	select name, code
     into n_name, n_code
     from niva.cultivation
    where (name = i_name and code !=i_code) or (name!=i_name and code = i_code);
     if found then
		case n_name=i_name
			when true then
				RAISE EXCEPTION 'Crop Name "%" With Code "%" Already Exists With Code "%"', i_name, i_code, n_code;
			else
				RAISE EXCEPTION 'Crop Code "%" With Name "%" Already Exists With Name "%"', i_code, i_name, n_name;
		end case;
     end if;
	
	
	-- Check if a Crop with the same code & name already exists (Do nothing).
	select name, code
     into n_name, n_code
     from niva.cultivation
    where (name = i_name and code =i_code);
     if not found then
  
		-- Check if a Land Cover with the name declared already exists.
		select cp.coty_id
		into n_coty_id
		from niva.cover_type cp
		where cp.name = i_coty_name;
		if not found then
			RAISE EXCEPTION 'Land Cover Named "%" Does Not Exist', i_coty_name;
		end if;
			
		
		INSERT INTO  niva.cultivation(
				coty_id, 
				name,
				code,
				exfi_id
			)
			VALUES (
				n_coty_id,
				i_name,
				i_code,
				i_excel_id                              
			);

  
     end if;

  return 0;

END;
$BODY$;


COMMENT ON FUNCTION niva.import_crops(character varying, integer, character varying, integer)
    IS 'Imports new crops into the list.';




-- Replaces the import_land_covers function, to implement more sophisticated import.

	-- FUNCTION: niva.import_land_covers(integer, character varying, integer)

DROP FUNCTION IF EXISTS niva.import_land_covers;

CREATE OR REPLACE FUNCTION niva.import_land_covers(
	i_code integer,
	i_name character varying,
	i_excel_id integer)
    RETURNS numeric
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
-- MT 20210225
DECLARE
    n_code INTEGER;   
    n_name VARCHAR(60); -- Changed to support longer names (30 -> 60)
begin
	
	
	-- Check if a Land Cover with the same name already exists with another code
	-- or if a Land Cover with the same code already exists with another name.
	
	select name, code
     into n_name, n_code
     from niva.cover_type
    where (name = i_name and code !=i_code) or (name!=i_name and code = i_code);
     if found then
		case n_name=i_name
			when true then
				RAISE EXCEPTION 'Land Cover Name "%" With Code "%" Already Exists With Code "%"', i_name, i_code, n_code;
			else
				RAISE EXCEPTION 'Land Cover Code "%" With Name "%" Already Exists With Name "%"', i_code, i_name, n_name;
		end case;
     end if;
	
	
	
	
	-- Check if a Land Cover with the same code & name already exists (Do nothing).
	select name, code
     into n_name, n_code
     from niva.cover_type
    where (name = i_name and code =i_code);
     if not found then
      
          INSERT INTO  niva.cover_type( code,
                                  name,
                                  exfi_id
                                )
                            VALUES (i_code,
                                    i_name,
                                    i_excel_id                              
                            );
		end if;
  return 0;

END;
$BODY$;

