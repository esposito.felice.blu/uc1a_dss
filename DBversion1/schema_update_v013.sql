CREATE TABLE niva.fmis_calendar (
	fmca_id serial NOT NULL,
	parc_id int4 NOT NULL,
	usrinsert varchar(150) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(150) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	attached_doc bytea NULL,
	attached_doc_filename varchar NULL,
	agree_flag int4 NULL,
	cult_id int4 NULL,
	CONSTRAINT fmca_pk PRIMARY KEY (fmca_id),
	CONSTRAINT fmca_cult_id_fk FOREIGN KEY (cult_id) REFERENCES niva.cultivation(cult_id),
	CONSTRAINT fmca_parc_id_fk FOREIGN KEY (parc_id) REFERENCES niva.parcel(parc_id)
)
TABLESPACE niva_data
;
CREATE INDEX fmca_parc_id_fi ON niva.fmis_calendar USING btree (parc_id);
