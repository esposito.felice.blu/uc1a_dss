CREATE TABLE niva.file_dir_path (
	fidr_id serial NOT NULL,
	"directory" varchar(150) NOT NULL,
	usrinsert varchar(150) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(150) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	CONSTRAINT fidr_pk PRIMARY KEY (fidr_id)
)
TABLESPACE niva_data
;

ALTER TABLE niva.ec_cult_detail ALTER COLUMN agrees_declar TYPE int4 USING agrees_declar::int4;

ALTER TABLE niva.file_dir_path ADD file_type int4 NOT NULL DEFAULT 1;
ALTER TABLE niva.file_dir_path ADD CONSTRAINT file_dir_path_un UNIQUE (file_type);

INSERT INTO niva.file_dir_path (directory, usrinsert, dteinsert, row_version, file_type) VALUES('nivashared', 'admin', current_timestamp, 0, 1);

ALTER TABLE niva.super_class ADD CONSTRAINT super_class_code_un UNIQUE (code);
ALTER TABLE niva.super_class ADD CONSTRAINT super_class_name_un UNIQUE ("name");

INSERT INTO niva.predef_col (prco_id,column_name,usrinsert,dteinsert,usrupdate,dteupdate,row_version,system_column_name) VALUES 
(199,'Parcel Code','admin',NULL,NULL,NULL,0,'parc_id')
,(202,'Cult Predicted Confidence 1','admin',NULL,NULL,NULL,0,'prob_pred')
,(204,'Cult Predicted Confidence 2','admin',NULL,NULL,NULL,0,'prob_pred2')
,(200,'Cultivation Declared','admin',NULL,NULL,NULL,0,'cult_id_decl')
,(201,'Cult Predicted 1','admin',NULL,NULL,NULL,0,'cult_id_pred')
,(203,'Cult Predicted 2','admin',NULL,NULL,NULL,0,'cult_id_pred2')
,(205,'Farmer Code','admin',NULL,'',NULL,1,'prod_code')
;

ALTER TABLE niva.file_template ADD CONSTRAINT file_template_name_un UNIQUE ("name");

ALTER TABLE niva.cover_type ADD exfi_id int4 NULL;
ALTER TABLE niva.cover_type ADD CONSTRAINT coty_exfi_id_fk FOREIGN KEY (exfi_id) REFERENCES niva.excel_files(id);


CREATE OR REPLACE FUNCTION niva.import_land_covers(i_code character varying, i_name character varying, i_excel_id integer)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
DECLARE
    n_code VARCHAR(30);   
    n_name VARCHAR(60);
begin
	
	select name
     into n_name
     from niva.cover_type 
    where name = i_name;
     if found then
       RAISE EXCEPTION 'Land Cover Name % Already Exist ', i_name;
     end if;
    
    select code
     into n_code
     from niva.cover_type 
    where code = i_code;
     if found then
       RAISE EXCEPTION 'Land Cover Code % Already Exist ', i_name;
     end if;
    
          INSERT INTO  niva.cover_type( code,
                                  name,
                                  exfi_id
                                )
                            VALUES (i_code,
                                    i_name,
                                    i_excel_id                              
                            );



  return 0;

END;
$function$
;

