CREATE OR REPLACE VIEW niva.v_parcel_decision_ws_export
AS SELECT
        CASE
            WHEN pade.decision_light = 1 THEN 'Green'::text
            WHEN pade.decision_light = 2 THEN 'Yellow'::text
            WHEN pade.decision_light = 3 THEN 'Red'::text
            WHEN pade.decision_light = 4 THEN 'Undefined'::text
            ELSE NULL::text
        END AS decision_light,
    parc.prod_code AS farmer_code,
    parc.code AS parcel_code,
    parc.identifier,
    cult_decl.code AS cultivation_declared_code,
    cult_decl.name AS cultivation_declared_name,
    cult_pred.code AS cultivation_1st_prediction_code,
    cult_pred.name AS cultivation_1st_prediction_name,
    pcla.prob_pred::numeric(3,2) AS probability_1st_prediction,
    cult_pred2.code AS cultivation_2nd_prediction_code,
    cult_pred2.name AS cultivation_2nd_prediction_name,
    pcla.prob_pred2::numeric(3,2) AS probability_2nd_prediction,
    ecgr.name AS ecgr_name,
    dema.date_time AS dema_date
   FROM niva.decision_making dema
     JOIN niva.parcel_decision pade ON pade.dema_id = dema.dema_id
     JOIN niva.parcel_class pcla ON pade.pcla_id = pcla.pcla_id
     JOIN niva.ec_group ecgr ON dema.ecgr_id = ecgr.ecgr_id
     JOIN niva.cultivation cult_decl ON pcla.cult_id_decl = cult_decl.cult_id
     LEFT JOIN niva.cultivation cult_pred ON pcla.cult_id_pred = cult_pred.cult_id
     LEFT JOIN niva.cultivation cult_pred2 ON pcla.cult_id_pred = cult_pred2.cult_id
     JOIN niva.parcel parc ON pcla.parc_id = parc.parc_id;