-- NIVA DATABASE SCHEMA UPDATE 16
-- v20210405
-- NEUROPUBLIC SA
-- MT


-- ADDED SUPPORT FOR GEOTAGGED PHOTOS




-- Table: niva.agrisnap_users

-- DROP TABLE niva.agrisnap_users;

CREATE TABLE niva.agrisnap_users
(
    agrisnap_name character varying(40) COLLATE pg_catalog."default" NOT NULL,
    row_version integer NOT NULL DEFAULT 0,
    agrisnap_users_id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    agrisnap_uid character(5) COLLATE pg_catalog."default" NOT NULL
)

TABLESPACE niva_data;

COMMENT ON TABLE niva.agrisnap_users
    IS 'AgrisnapUser';

COMMENT ON COLUMN niva.agrisnap_users.agrisnap_uid
    IS 'Contains a 5 chars human-readable User Id.';
	
	
	
-- Table: niva.agrisnap_users_producers

-- DROP TABLE niva.agrisnap_users_producers;

CREATE TABLE niva.agrisnap_users_producers
(
    agrisnap_users_id integer NOT NULL,
    dte_rel_created date DEFAULT now(),
    dte_rel_canceled date,
    agrisnap_users_producers_id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    row_version integer NOT NULL DEFAULT 0,
    producers_id integer NOT NULL
)

TABLESPACE niva_data;


COMMENT ON TABLE niva.agrisnap_users_producers
    IS 'Contains records with producers for each agrispan user.';
	
	
-- Table: niva.gp_requests

-- DROP TABLE niva.gp_requests;

CREATE TABLE niva.gp_requests
(
    gp_requests_id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    dte_received timestamp with time zone NOT NULL DEFAULT now(),
    type_code smallint,
    type_description character varying COLLATE pg_catalog."default",
    scheme character varying COLLATE pg_catalog."default",
    correspondence_id integer,
    correspondence_doc_nr integer,
    geotag character(1) COLLATE pg_catalog."default",
    row_version integer NOT NULL DEFAULT 0,
    agrisnap_users_id integer NOT NULL
)


TABLESPACE niva_data;

COMMENT ON TABLE niva.gp_requests
    IS 'Contains the requests (and their responses) of AgriSpan.';
	
	
	-- Table: niva.gp_requests_contexts

-- DROP TABLE niva.gp_requests_contexts;

CREATE TABLE niva.gp_requests_contexts
(
    gp_requests_contexts_id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    type character varying COLLATE pg_catalog."default",
    label character varying COLLATE pg_catalog."default",
    comment character varying COLLATE pg_catalog."default",
    geom_hexewkb character varying COLLATE pg_catalog."default",
    referencepoint character varying COLLATE pg_catalog."default",
    gp_requests_producers_id integer NOT NULL,
    row_version integer NOT NULL DEFAULT 0,
    hash character(32) COLLATE pg_catalog."default" NOT NULL,
    parcels_issues_id integer
)


TABLESPACE niva_data;

COMMENT ON TABLE niva.gp_requests_contexts
    IS 'Contains context details about each request & producer.';

COMMENT ON COLUMN niva.gp_requests_contexts.geom_hexewkb
    IS 'Contains the geometry (with irs SRID) in HEXEWKB format (as text).';

COMMENT ON COLUMN niva.gp_requests_contexts.hash
    IS 'Hash identification.';
	
	
-- Table: niva.gp_requests_producers

-- DROP TABLE niva.gp_requests_producers;

CREATE TABLE niva.gp_requests_producers
(
    gp_requests_id integer NOT NULL,
    producers_id integer NOT NULL,
    gp_requests_producers_id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    row_version integer NOT NULL DEFAULT 0,
    notes character(1) COLLATE pg_catalog."default"
)

TABLESPACE niva_data;

COMMENT ON TABLE niva.gp_requests_producers
    IS 'Contains info per Producer of a geotagged photos request. ';
	
	
	-- Table: niva.gp_uploads

-- DROP TABLE niva.gp_uploads;

CREATE TABLE niva.gp_uploads
(
    gp_uploads_id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    data character varying COLLATE pg_catalog."default",
    environment character varying COLLATE pg_catalog."default",
    gp_requests_contexts_id integer NOT NULL,
    dte_upload date NOT NULL DEFAULT now(),
    hash character(32) COLLATE pg_catalog."default" NOT NULL,
    image bytea
)



TABLESPACE niva_data;


COMMENT ON TABLE niva.gp_uploads
    IS 'Contains the uploaded Geotagged Photos and their Metadata.';
	

-- Table: niva.parcels_issues

-- DROP TABLE niva.parcels_issues;

CREATE TABLE niva.parcels_issues
(
    parcels_issues_id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    parc_id integer NOT NULL,
    dte_created timestamp with time zone DEFAULT now(),
    status smallint,
    dte_status_update timestamp with time zone DEFAULT now(),
    row_version integer NOT NULL DEFAULT 0
)



TABLESPACE niva_data;

COMMENT ON TABLE niva.parcels_issues
    IS 'Contains issues about the not indisputably classified parcels.
';

COMMENT ON COLUMN niva.parcels_issues.status
    IS 'value of 900 (or greater) means inactive.

value 10 means created.
value 20 means at least one photo request has occured.
value 30 means at least one photo has been uploaded. ';


-- Table: niva.parcels_issues_activities

-- DROP TABLE niva.parcels_issues_activities;

CREATE TABLE niva.parcels_issues_activities
(
    parcels_issues_activities_id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    row_version integer NOT NULL DEFAULT 0,
    parcels_issues_id integer NOT NULL,
    type smallint NOT NULL,
    type_extrainfo integer,
    dte_timestamp timestamp with time zone NOT NULL DEFAULT now()
)



TABLESPACE niva_data;

COMMENT ON TABLE niva.parcels_issues_activities
    IS 'Contains activities related to each Parcel Issue, e.g. AgriSnap interaction, etc.';


	
-- Table: niva.producers

-- DROP TABLE niva.producers;

CREATE TABLE niva.producers
(
    prod_name character varying(40) COLLATE pg_catalog."default" NOT NULL,
    row_version integer NOT NULL DEFAULT 0,
    prod_code integer NOT NULL,
    producers_id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass)
)



TABLESPACE niva_data;

COMMENT ON TABLE niva.producers
    IS 'Contains info about the farmers.';
	
	
--- CONSTRAINTS ETC.	
	
ALTER TABLE niva.agrisnap_users
    ADD CONSTRAINT agrisnap_users_pkey PRIMARY KEY (agrisnap_users_id)
        USING INDEX TABLESPACE niva_data;
	
	
ALTER TABLE niva.parcels_issues
    ADD CONSTRAINT parcels_issues_pkey PRIMARY KEY (parcels_issues_id)
        USING INDEX TABLESPACE niva_data;
	
ALTER TABLE niva.producers
    ADD CONSTRAINT producers_pkey PRIMARY KEY (producers_id);	


ALTER TABLE niva.gp_requests
   ADD CONSTRAINT gp_requests_id_pkey PRIMARY KEY (gp_requests_id),
   ADD CONSTRAINT gprequests_agrisnapusers_agrisnap_users_id FOREIGN KEY (agrisnap_users_id)
	REFERENCES niva.agrisnap_users (agrisnap_users_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID;


ALTER TABLE niva.gp_requests_producers
    ADD CONSTRAINT gp_requests_producers_pkey PRIMARY KEY (gp_requests_producers_id),
    ADD CONSTRAINT gprequestsproducers_uk UNIQUE (gp_requests_id, producers_id)
        USING INDEX TABLESPACE niva_data,
    ADD CONSTRAINT gprequestsproducers_gp_gprequests_requests_id_fk FOREIGN KEY (gp_requests_id)
        REFERENCES niva.gp_requests (gp_requests_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    ADD CONSTRAINT gprequestsproducers_producers_producers_id FOREIGN KEY (producers_id)
        REFERENCES niva.producers (producers_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID;

	
ALTER TABLE niva.agrisnap_users_producers
   ADD CONSTRAINT agrisnap_users_producers_id_pkey PRIMARY KEY (agrisnap_users_producers_id),
   ADD CONSTRAINT agrisnapproducers_agrisnap_users_id_producers_id_uk UNIQUE (agrisnap_users_id, agrisnap_users_producers_id)
        USING INDEX TABLESPACE niva_data,
   ADD CONSTRAINT agrisnapusersproducers_producers_producers_id_fk FOREIGN KEY (producers_id)
        REFERENCES niva.producers (producers_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
   ADD CONSTRAINT agrisnapusersroducers_agrisnapusers_agrisnap_users_id_fk FOREIGN KEY (agrisnap_users_id)
        REFERENCES niva.agrisnap_users (agrisnap_users_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID;
	
	
ALTER TABLE niva.gp_requests_contexts

    ADD CONSTRAINT gp_requests_contexts_pkey PRIMARY KEY (gp_requests_contexts_id)
        USING INDEX TABLESPACE niva_data,
    ADD CONSTRAINT "gpRequestsContexts_hash_uk" UNIQUE (hash),
    ADD CONSTRAINT gprequestscontexts_gprequestsproducers_gp_requests_producers_id FOREIGN KEY (gp_requests_producers_id)
        REFERENCES niva.gp_requests_producers (gp_requests_producers_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID;	


ALTER TABLE niva.gp_uploads
    ADD CONSTRAINT gp_uploads_pkey PRIMARY KEY (gp_uploads_id),
    ADD CONSTRAINT "gpUploads_gpRequestsContexts_gp_requests_contexts_id_fk" FOREIGN KEY (gp_requests_contexts_id)
        REFERENCES niva.gp_requests_contexts (gp_requests_contexts_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID;		
		

ALTER TABLE niva.parcels_issues_activities
    ADD CONSTRAINT parcels_issues_activities_pkey PRIMARY KEY (parcels_issues_activities_id),
    ADD CONSTRAINT parcelsissuesactivities_parcelsissues_parcels_issues_id_fk FOREIGN KEY (parcels_issues_id)
        REFERENCES niva.parcels_issues (parcels_issues_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID;

	