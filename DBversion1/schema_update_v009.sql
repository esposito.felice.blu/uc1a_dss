ALTER TABLE niva.geotagphotos ALTER COLUMN image_path DROP NOT NULL;
COMMENT ON COLUMN niva.geotagphotos.decision_light IS 'agree 1, disagree 0';

ALTER TABLE niva.geotagphotos ADD cult_id int4 NULL;
COMMENT ON COLUMN niva.geotagphotos.cult_id IS 'Fk to cultivation';
ALTER TABLE niva.geotagphotos ADD CONSTRAINT geot_cult_id_fk FOREIGN KEY (cult_id) REFERENCES niva.cultivation(cult_id);

ALTER TABLE niva.geotagphotos RENAME COLUMN decision_light TO agree_flag;


--probab_thres int4
ALTER TABLE niva.ec_cult_detail ALTER COLUMN probab_thres TYPE numeric(4,2);
ALTER TABLE niva.ec_cult_detail ALTER COLUMN probab_thres2 TYPE numeric(4,2);
ALTER TABLE niva.ec_cult_detail ALTER COLUMN probab_thres_sum  TYPE numeric(4,2);

ALTER TABLE niva.ec_cult_detail_copy ALTER COLUMN probab_thres TYPE numeric(4,2);
ALTER TABLE niva.ec_cult_detail_copy ALTER COLUMN probab_thres2 TYPE numeric(4,2);
ALTER TABLE niva.ec_cult_detail_copy ALTER COLUMN probab_thres_sum  TYPE numeric(4,2);