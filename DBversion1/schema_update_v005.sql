ALTER TABLE niva.ec_cult_detail ADD agrees_declar2 int4 ;
COMMENT ON COLUMN niva.ec_cult_detail.agrees_declar2 IS '1: agree, 0: disagree declaration';

ALTER TABLE niva.ec_cult_detail ADD comparison_oper2 int4 ;
COMMENT ON COLUMN niva.ec_cult_detail.comparison_oper2 IS '1: greater, 2: greater equal, 3:less, 4: less equal';

ALTER TABLE niva.ec_cult_detail ADD probab_thres2 int4 ;
COMMENT ON COLUMN niva.ec_cult_detail.probab_thres2 IS 'probability 2 threshold ';

ALTER TABLE niva.ec_cult_detail ADD probab_thres_sum int4 ;
COMMENT ON COLUMN niva.ec_cult_detail.probab_thres_sum IS 'sum of probability 1+2 threshold ';

ALTER TABLE niva.ec_cult_detail ALTER COLUMN activities_match DROP NOT NULL;
ALTER TABLE niva.ec_cult_detail ALTER COLUMN probab_thres DROP NOT NULL;
ALTER TABLE niva.ec_cult_detail ALTER COLUMN comparison_oper DROP NOT NULL;
ALTER TABLE niva.ec_cult_detail ALTER COLUMN agrees_declar DROP NOT NULL;

ALTER TABLE niva.ec_cult_detail ADD comparison_oper3 int4 ;
COMMENT ON COLUMN niva.ec_cult_detail.comparison_oper3 IS '1: greater, 2: greater equal, 3:less, 4: less equal';

ALTER TABLE niva.parcel_class ADD CONSTRAINT "pcla_clasId_parcId_UK" UNIQUE (clas_id,parc_id);
