ALTER TABLE niva.file_dir_path add if not exists file_type int4 NOT NULL DEFAULT 1;
alter TABLE niva.file_dir_path drop constraint IF EXISTS file_dir_path_un;
ALTER TABLE niva.file_dir_path ADD CONSTRAINT file_dir_path_un UNIQUE (file_type);

CREATE OR REPLACE FUNCTION niva.file_template()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
    BEGIN
        -- Check that empname and salary are given
        IF NEW.fite_id IS not NULL THEN
            INSERT INTO niva.template_columns (fite_id, prco_id, clfier_name)
                            SELECT NEW.fite_id, prco_id, column_name FROM niva.predef_col                           
                            ;
        END IF;



        -- Remember who changed the payroll when
--        NEW.dteupdate := current_timestamp;
--        NEW.usrupdate := current_user;
        RETURN NEW;
    END;
$function$
;


-- DROP TRIGGER file_template_tr ON niva.file_template;

create trigger file_template_tr after
insert
    on
    niva.file_template for each row execute function niva.file_template();
	
	
DROP FUNCTION if exists niva.import_crops("varchar","int4","int4");
	
CREATE OR REPLACE FUNCTION niva.import_crops(i_name character varying, i_code integer, i_coty_code character varying, i_excel_id integer)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
DECLARE
    n_name VARCHAR(60);   
    n_code INTEGER;
begin
	
	select name
     into n_name
     from niva.cultivation
    where name = i_name;
     if found then
       RAISE EXCEPTION 'Crop Name % Already Exist ', i_name;
     end if;
    
    select code
     into n_code
     from niva.cultivation
    where code = i_code;
     if found then
       RAISE EXCEPTION 'Crop Code % Already Exist ', i_name;
     end if;
    
          INSERT INTO  niva.cultivation( coty_id, 
          							name,
                                  	code,
                                  	exfi_id
                                )
                            VALUES ((select cp.coty_id from niva.cover_type cp where cp.code = i_coty_code::varchar),
                            		i_name,
                                    i_code,
                                    i_excel_id                              
                            );



  return 0;

END;
$function$
;

ALTER TABLE niva.activity ADD CONSTRAINT activity_code_un UNIQUE (code);
ALTER TABLE niva.activity ADD CONSTRAINT activity_name_un UNIQUE ("name");

ALTER TABLE niva.cover_type ADD CONSTRAINT cover_type_code_un UNIQUE (code);
ALTER TABLE niva.cover_type ADD CONSTRAINT cover_type_name_un UNIQUE ("name");
	
	