# N I V A  *Decision Support System* 

### Use Case 1a

## Introduction

This sub-project is part of the [New IACS Vision in Action --- NIVA](https://www.niva4cap.eu/) project that delivers a a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union&#39;s Horizon 2020 research and innovation programme under grant agreement No 842009.

Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/).




---

## WORK IN PROGRESS !

##### Branch [Version3_PreRelease](https://gitlab.com/nivaeu/uc1a_dss/-/tree/Version3_PreRelease) contains the new, Version 3 of NIVA DSS.

##### Some work is going on to set up the GitLab repository, develop the file structure, and provide helpful documentation.

###### You can checkout the branch, but you may face problems. Until the new Version repository becomes complete and well tested, it's better to try this older but stable Version (2) of the software.

---



## IMPORTANT NOTICE:
	
###### As of 25.05.2021, a highly modified main Database is needed (Abusively: "version 2").

###### Due to the large number of modifications, it is suggested that you create a new Database on your server, and not alter any existing one.

###### The new main Database can be created using the provided niva2.pg.sql file. This must be executed on the database & server of your choice. Don't forget to delete the previous one (if exists) and modify the configuration files as it is described below.
	
---



## Enviroment and app server

The following free software packages are required for a complete & functional setup of a Niva DSS server:

* PostgreSQL
    Recommended version: 12 (or newer).
 You can download it from https://www.postgresql.org/.

* PostGIS
  Recommended version: 3.0 (or newer).
   It is a spatial database extender for PostgreSQL, that adds support for geographic objects & allows location queries to be run in SQL.
   More info & downloads' page: https://postgis.net/ .

* Java JDK
Version SE 7  required (Currently newer versions are not compatible).
Downloads and info can be found at: https://www.oracle.com/java/technologies/javase/javase7-archive-downloads.html .

* JBoss AS
Version 7.1.1 required. (This is the final version, WildFly is currently not compatible)
Downloads and info can be found at: https://jbossas.jboss.org/downloads .

### JBoss settings

 
#### 1. Drivers

##### 
 - PostgreSQL

	For the cooperation of JBoss AS & PostgreSQL, the following files are needed:


```
    postgresql-42.2.1.jre7.jar
    postgis-1.5.3.jar


    -rw-r--r-- 1 jbossuser jbossuser    783 Jul  7 15:33 postgresql-42.2.1.jre7.jar.index
    -rw-r--r-- 1 jbossuser jbossuser 722411 Jul  7 15:33 postgresql-42.2.1.jre7.jar
    -rw-r--r-- 1 jbossuser jbossuser     88 Jul  7 15:33 postgis-1.5.3.jar.index
    -rw-r--r-- 1 jbossuser jbossuser  88108 Jul  7 15:33 postgis-1.5.3.jar
    -rw-r--r-- 1 jbossuser jbossuser    598 Jul  7 15:33 module.xml
```

File '_module.xml_' at _<JBoss_home dir>/modules/org/postgresql/main_ should be like:
```
<module xmlns="urn:jboss:module:1.1" name="org.postgresql">
	<!-- Load with jboss-cli command:
		    /subsystem=datasources/jdbc-driver=postgresql-driver:add(driver-name=postgresql-driver, driver-class-name=org.postgresql.Driver, driver-module-name=org.postgresql)
	  -->
	<resources>
		<resource-root path="postgresql-42.2.1.jre7.jar"/>
		<resource-root path="postgis-1.5.3.jar"/>
	</resources>
	<dependencies>
		<module name="javax.api"/>
		<module name="javax.transaction.api"/>
	</dependencies>
</module>
```
<br>

##### 
 -  Hibernate:

    Similarily, the cooperation with Hibernate needs the following files:
```
      hibernate-core-4.1.3.Final-redhat-1.jar
      hibernate-entitymanager-4.1.3.Final-redhat-1.jar
      hibernate-infinispan-4.1.3.Final-redhat-1.jar
      hibernate-spatial-4.0.1.jar, jts-1.13.jar
      jts-1.13.jar
```
and their corresponding dependencies:
```
      -rw-r--r-- 1 jbossuser jbossuser 4456682 Jul  7 15:34 hibernate-core-4.1.3.Final-redhat-1.jar
      -rw-r--r-- 1 jbossuser jbossuser    6126 Jul  7 15:34 hibernate-core-4.1.3.Final-redhat-1.jar.index
      -rw-r--r-- 1 jbossuser jbossuser  479501 Jul  7 15:34 hibernate-entitymanager-4.1.3.Final-redhat-1.jar
      -rw-r--r-- 1 jbossuser jbossuser     527 Jul  7 15:34 hibernate-entitymanager-4.1.3.Final-redhat-1.jar.index
      -rw-r--r-- 1 jbossuser jbossuser   70141 Jul  7 15:34 hibernate-infinispan-4.1.3.Final-redhat-1.jar
      -rw-r--r-- 1 jbossuser jbossuser     479 Jul  7 15:34 hibernate-infinispan-4.1.3.Final-redhat-1.jar.index
      -rw-r--r-- 1 jbossuser jbossuser  207496 Jul  7 15:34 hibernate-spatial-4.0.1.jar
      -rw-r--r-- 1 jbossuser jbossuser     565 Jul  7 15:34 hibernate-spatial-4.0.1.jar.index
      -rw-r--r-- 1 jbossuser jbossuser  794991 Jul  7 15:34 jts-1.13.jar
      -rw-r--r-- 1 jbossuser jbossuser    2469 Jul  7 15:34 jts-1.13.jar.index
      -rw-r--r-- 1 jbossuser jbossuser    2452 Jul  7 15:34 module.xml
```
<br>

File '_module.xml_' at _<JBoss_home dir>/modules/org/hibernate/main_ should be like:
<br>
```
<?xml version="1.0" encoding="UTF-8"?>
	<!--
		~ JBoss, Home of Professional Open Source.
		~ Copyright 2011, Red Hat, Inc., and individual contributors
		~ as indicated by the @author tags. See the copyright.txt file in the
		~ distribution for a full listing of individual contributors.
		~
		~ This is free software; you can redistribute it and/or modify it
		~ under the terms of the GNU Lesser General Public License as
		~ published by the Free Software Foundation; either version 2.1 of
		~ the License, or (at your option) any later version.
		~
		~ This software is distributed in the hope that it will be useful,
		~ but WITHOUT ANY WARRANTY; without even the implied warranty of
		~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
		~ Lesser General Public License for more details.
		~
		~ You should have received a copy of the GNU Lesser General Public
		~ License along with this software; if not, write to the Free
		~ Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
		~ 02110-1301 USA, or see the FSF site: http://www.fsf.org.
		-->
	<!-- Represents the Hibernate 4.0.x module-->
	<module xmlns="urn:jboss:module:1.1" name="org.hibernate">
		<resources>
			<resource-root path="hibernate-core-4.1.3.Final-redhat-1.jar"/>
			<resource-root path="hibernate-entitymanager-4.1.3.Final-redhat-1.jar"/>
			<resource-root path="hibernate-infinispan-4.1.3.Final-redhat-1.jar"/>
			<resource-root path="hibernate-spatial-4.0.1.jar"/>
			<resource-root path="jts-1.13.jar"/>
			<!-- Insert resources here -->
		</resources>
		<dependencies>
			<module name="asm.asm"/>
			<module name="javax.api"/>
			<module name="javax.persistence.api"/>
			<module name="javax.transaction.api"/>
			<module name="javax.validation.api"/>
			<module name="org.antlr"/>
			<module name="org.apache.commons.collections"/>
			<module name="org.dom4j"/>
			<module name="org.infinispan" optional="true"/>
			<module name="org.javassist"/>
			<module name="org.jboss.as.jpa.hibernate" slot="4" optional="true"/>
			<module name="org.jboss.logging"/>
			<module name="org.hibernate.envers" services="import" optional="true"/>
			<module name="org.hibernate.commons-annotations"/>
			<module name="org.postgresql"/>
			<!--<module name="com.edb"/>-->
			<module name="oracle.jdbc"/>
		</dependencies>
```
<br>
<br>
The drivers should be declared to JBoss AS, with the following command:
<br>

```
	jboss-cli.sh --connect controller=<ip>:9999 command="/profile=full-ha/subsystem=datasources/jdbc-driver=postgresql-driver:add(driver-name=postgresql-driver, driver-class-name=org.postgresql.Driver, driver-module-name=org.postgresql)".
```
<br>

Additionally, two needed datasources must be inserted into the 'standalone.xml' file, located in the 'JBossAS\jboss-as-7.1.1.Final\standalone\configuration' folder:
```
<datasource jta="true" jndi-name="java:/niva_usermgmnt " pool-name="niva_usermgmnt" enabled="true" use-ccm="false">
	<connection-url>jdbc:postgresql://127.0.0.1:port/database_name</connection-url>
	<driver-class>org.postgresql.Driver</driver-class>
	<driver>postgresql9.1</driver>
	<security>
		<user-name>username</user-name>
		<password>password</password>
	</security>
	<validation>
		<validate-on-match>false</validate-on-match>
		<background-validation>false</background-validation>
	</validation>
	<statement>
		<share-prepared-statements>false</share-prepared-statements>
	</statement>
</datasource>
<datasource jta="true" jndi-name="java:/Niva" pool-name="NivaPU" enabled="true" use-ccm="false">
	<connection-url>jdbc:postgresql://127.0.0.1:port/database_name</connection-url>
	<driver-class>org.postgresql.Driver</driver-class>
	<driver>postgresql9.1</driver>
	<security>
		<user-name>username</user-name>
		<password>password</password>
	</security>
	<validation>
		<validate-on-match>false</validate-on-match>
		<background-validation>false</background-validation>
	</validation>
	<statement>
		<share-prepared-statements>false</share-prepared-statements>
	</statement>
</datasource>
```

 <br>
 
#### 2.Memcached

Memcached is required, through which the management of sessions in order for the application to determine if the user has an active session or not.

Thus, the mMemcached package must be installed (on a server that will not be restarted, and sessions will not be lost). After the installation:

 1. In the central configuration directory of JBoss AS place the file
	'_memCache.properties_', with content:
```
	#
	# Memcached properties
	#

	IP = 127.0.0.1 (if it is the same PC - server)
	port = 11211
	timeout = 1800 (or larger)
```

 2. If JBoss AS is set up in domain mode, then in the configuration directory of the JBoss server instance (for example server-three: _\<JBoss-home dir>/domain/servers/server-three/configuration_) a Symbolic Link must be created, pointing to the file '_memCache.properties_' in the JBoss central configuration directory:
```
	lrwxrwxrwx 1 jbossuser jbossuser  42 Feb 25  2014 memCache.properties -> ../../../configuration/memCache.properties
```

<br>

###  User Management
	
Niva DSS uses an advanced User Management System. This System requires tables, views, and functions for its operation, that may exist in the same database with the main schema, or in an another one.

To create the needed elements in the database, execute the provided script (File: '*usermgmnt_template.sql*'). 
Thereafter, you may use the scripts in the files '*system.sql*' and '*test_users.sql*' (Directory: '*codeGen*') to create active test users with the required roles and permissions for Niva DSS. These test users will be created with login names *u1@Niva*, *u2@Niva*,... etc (up to *u100@Niva*), with password '*user1*'. 

<br>

###  Build
	
Install Ant (the Java 7 compatible version) and build the application using the provided '*build.xml*' file in the '*codeGen*' folder.
Additionally, the '*build.xml*' defines where the deployment folder is at the "publish". The goal for Ant is to deploy '*Niva.ear*' in the folder '*JBossAS\jboss-as-7.1.1.Final\standalone\deployments*'. 
This may require modification, changing '*build.properties*' to publish into your local Jboss folder accordingly. 

<br>

###  Log in
	
Start JBoss and will automatically deploy the application at url: http://localhost:8080/Niva
Log in using any user previously created by the script 

<br>

### File Directory For Importing Shapefiles
	
When importing .shp (classification) files, a folder for temporary files is used.
This folder can be defined from the Main Menu > System Parameters > Temporary Files Directory. 
Select 'Temporary Files Directory for Importing Shape Files' as the subject to be set , and input the path of your choice.

![View Snapshot](https://gitlab.com/nivaeu/uc1a_dss/-/blob/master/docs/DefineTemporaryFolderForImporting.jpg)
