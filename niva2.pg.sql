-- NIVA Main Database v2
-- NEUROPUBLIC / MT 20210604a

-- Table: niva.dbmanagementlog


--
-- Use the next line to delete the Schema if exists
--
-- W A R N I N G !!
-- A L L  D A T A  W I L L  B E  L O S T
--
--

-- DROP SCHEMA IF EXISTS niva CASCADE;


--
--
-- Name: niva; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA niva;


--
-- Name: niva_sq; Type: SEQUENCE; Schema: niva; Owner: -
--

CREATE SEQUENCE niva.niva_sq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999
    CACHE 1;


CREATE TABLE niva.dbmanagementlog
(
    dblid integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    dte_done timestamp(0) with time zone NOT NULL DEFAULT now(),
    version character varying(10) COLLATE pg_catalog."default" NOT NULL,
    notes character varying COLLATE pg_catalog."default",
    type integer,
    CONSTRAINT dbmanagementlog_pkey PRIMARY KEY (dblid)
);

COMMENT ON TABLE niva.dbmanagementlog
    IS 'Contains info about any updates of the db schema etc.';

COMMENT ON COLUMN niva.dbmanagementlog.type
    IS 'Contains the log type. 
1= Started.
2= Succeeded.';



-- LOGGING

INSERT INTO niva.dbmanagementlog(
	version, notes, type)
	VALUES ('20210604a', 'First Logged Script Execution Started', 1);



CREATE EXTENSION IF NOT EXISTS postgis;



--
-- Name: file_template(); Type: FUNCTION; Schema: niva; Owner: -
--

CREATE FUNCTION niva.file_template() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN

        IF NEW.fite_id IS not NULL THEN
            INSERT INTO niva.template_columns (fite_id, prco_id, clfier_name)
                            SELECT NEW.fite_id, prco_id, column_name FROM niva.predef_col                           
                            ;
        END IF;


--        NEW.dteupdate := current_timestamp;
--        NEW.usrupdate := current_user;
        RETURN NEW;
    END;
	$$;







--
-- Name: import_crops(character varying, integer, character varying, integer); Type: FUNCTION; Schema: niva; Owner: -
--

CREATE FUNCTION niva.import_crops(i_name character varying, i_code integer, i_coty_name character varying, i_excel_id integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
-- MT 20210301
DECLARE
    n_name VARCHAR(200);   -- MT -Ready for longer names (>60)
    n_code INTEGER;
	n_coty_id INTEGER;

BEGIN
	
	
	-- Check if a Crop with the same name already exists with another code
	-- or if a Crop with the same code already exists with another name.
	
	select name, code
     into n_name, n_code
     from niva.cultivation
    where (name = i_name and code !=i_code) or (name!=i_name and code = i_code);
     if found then
		case n_name=i_name
			when true then
				RAISE EXCEPTION 'Crop Name "%" With Code "%" Already Exists With Code "%"', i_name, i_code, n_code;
			else
				RAISE EXCEPTION 'Crop Code "%" With Name "%" Already Exists With Name "%"', i_code, i_name, n_name;
		end case;
     end if;
	
	
	-- Check if a Crop with the same code & name already exists (Do nothing).
	select name, code
     into n_name, n_code
     from niva.cultivation
    where (name = i_name and code =i_code);
     if not found then
  
		-- Check if a Land Cover with the name declared already exists.
		select cp.coty_id
		into n_coty_id
		from niva.cover_type cp
		where cp.name = i_coty_name;
		if not found then
			RAISE EXCEPTION 'Land Cover Named "%" Does Not Exist', i_coty_name;
		end if;
			
		
		INSERT INTO  niva.cultivation(
				coty_id, 
				name,
				code,
				exfi_id
			)
			VALUES (
				n_coty_id,
				i_name,
				i_code,
				i_excel_id                              
			);

  
     end if;

  return 0;

END;
$$;

--
-- Name: FUNCTION import_crops(i_name character varying, i_code integer, i_coty_name character varying, i_excel_id integer); Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON FUNCTION niva.import_crops(i_name character varying, i_code integer, i_coty_name character varying, i_excel_id integer) IS 'Imports new crops into the list.';


--
-- Name: import_land_covers(integer, character varying, integer); Type: FUNCTION; Schema: niva; Owner: -
--

CREATE FUNCTION niva.import_land_covers(i_code integer, i_name character varying, i_excel_id integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
-- MT 20210225
DECLARE
    n_code INTEGER;   
    n_name VARCHAR(60); -- Changed to support longer names (30 -> 60)
BEGIN
	
	
	-- Check if a Land Cover with the same name already exists with another code
	-- or if a Land Cover with the same code already exists with another name.
	
	select name, code
     into n_name, n_code
     from niva.cover_type
    where (name = i_name and code !=i_code) or (name!=i_name and code = i_code);
     if found then
		case n_name=i_name
			when true then
				RAISE EXCEPTION 'Land Cover Name "%" With Code "%" Already Exists With Code "%"', i_name, i_code, n_code;
			else
				RAISE EXCEPTION 'Land Cover Code "%" With Name "%" Already Exists With Name "%"', i_code, i_name, n_name;
		end case;
     end if;
	
	
	
	
	-- Check if a Land Cover with the same code & name already exists (Do nothing).
	select name, code
     into n_name, n_code
     from niva.cover_type
    where (name = i_name and code =i_code);
     if not found then
      
          INSERT INTO  niva.cover_type( code,
                                  name,
                                  exfi_id
                                )
                            VALUES (i_code,
                                    i_name,
                                    i_excel_id                              
                            );
		end if;
  return 0;

END;
$$;


--
-- Name: agency; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.agency (
    agen_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    name character varying(60) NOT NULL,
    country character varying(60),
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    srid smallint DEFAULT 4326
);


--
-- Name: TABLE agency; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.agency IS 'State paying Agency';


--
-- Name: COLUMN agency.agen_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.agency.agen_id IS 'primary key';


--
-- Name: COLUMN agency.name; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.agency.name IS 'name of agency';


--
-- Name: COLUMN agency.country; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.agency.country IS 'country of agency';


--
-- Name: COLUMN agency.srid; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.agency.srid IS 'The Spatial Reference Identifier used by the Agency.';


--
-- Name: agrisnap_users; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.agrisnap_users (
    agrisnap_name character varying(40) NOT NULL,
    row_version integer DEFAULT 0 NOT NULL,
    agrisnap_users_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    agrisnap_uid character(5) NOT NULL
);


--
-- Name: TABLE agrisnap_users; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.agrisnap_users IS 'AgrisnapUser';


--
-- Name: COLUMN agrisnap_users.agrisnap_uid; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.agrisnap_users.agrisnap_uid IS 'Contains a 5 chars human-readable User Id.';


--
-- Name: agrisnap_users_producers; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.agrisnap_users_producers (
    agrisnap_users_id integer NOT NULL,
    dte_rel_created date DEFAULT now(),
    dte_rel_canceled date,
    agrisnap_users_producers_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    row_version integer DEFAULT 0 NOT NULL,
    producers_id integer NOT NULL
);


--
-- Name: TABLE agrisnap_users_producers; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.agrisnap_users_producers IS 'Contains records with producers for each agrispan user.';


--
-- Name: classification; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.classification (
    clas_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    clfr_id integer NOT NULL,
    name character varying(60) NOT NULL,
    description character varying(60),
    date_time timestamp with time zone NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    recordtype integer DEFAULT 0 NOT NULL,
    file_path character varying(60),
    attached_file bytea,
    fite_id integer NOT NULL,
    year smallint DEFAULT 1970 NOT NULL,
    is_imported boolean DEFAULT false NOT NULL
);


--
-- Name: TABLE classification; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.classification IS 'classification';


--
-- Name: COLUMN classification.clas_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.classification.clas_id IS 'primary key';


--
-- Name: COLUMN classification.clfr_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.classification.clfr_id IS 'fk to classifier';


--
-- Name: COLUMN classification.fite_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.classification.fite_id IS 'fk to file template';


--
-- Name: COLUMN classification.year; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.classification.year IS 'The year that the classifications is about.';


--
-- Name: COLUMN classification.is_imported; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.classification.is_imported IS 'True if the file has been parsed.';


--
-- Name: classifier; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.classifier (
    clfr_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    name character varying(60) NOT NULL,
    description character varying(60),
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE classifier; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.classifier IS 'classifier';


--
-- Name: COLUMN classifier.clfr_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.classifier.clfr_id IS 'primary key';


--
-- Name: COLUMN classifier.name; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.classifier.name IS 'classifier name';


--
-- Name: COLUMN classifier.description; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.classifier.description IS 'classifier description';


--
-- Name: cover_type; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.cover_type (
    coty_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    code integer NOT NULL,
    name character varying(60) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    exfi_id integer
);


--
-- Name: TABLE cover_type; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.cover_type IS 'cover_type';


--
-- Name: COLUMN cover_type.coty_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.cover_type.coty_id IS 'primary key';


--
-- Name: COLUMN cover_type.code; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.cover_type.code IS 'code of crop';


--
-- Name: COLUMN cover_type.name; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.cover_type.name IS 'crop name';


--
-- Name: cultivation; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.cultivation (
    cult_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    coty_id integer,
    name character varying(200) NOT NULL,
    code integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    exfi_id integer
);


--
-- Name: TABLE cultivation; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.cultivation IS 'cultivation';


--
-- Name: COLUMN cultivation.cult_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.cultivation.cult_id IS 'primary key';


--
-- Name: COLUMN cultivation.coty_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.cultivation.coty_id IS 'fk to cover_type';


--
-- Name: COLUMN cultivation.name; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.cultivation.name IS 'cultivation name';


--
-- Name: COLUMN cultivation.code; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.cultivation.code IS 'code of cultivation';


--
-- Name: COLUMN cultivation.exfi_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.cultivation.exfi_id IS 'fk to excel_files';


--
-- Name: decision_making; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.decision_making (
    dema_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    clas_id integer NOT NULL,
    ecgr_id integer NOT NULL,
    description character varying(60),
    date_time timestamp with time zone NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    recordtype integer DEFAULT 0,
    has_been_run boolean DEFAULT false NOT NULL,
    has_populated_integrated_decisions_and_issues boolean DEFAULT false NOT NULL
);


--
-- Name: TABLE decision_making; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.decision_making IS 'decision making';


--
-- Name: COLUMN decision_making.dema_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.decision_making.dema_id IS 'primary key';


--
-- Name: COLUMN decision_making.clas_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.decision_making.clas_id IS 'fk to classification';


--
-- Name: COLUMN decision_making.ecgr_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.decision_making.ecgr_id IS 'fk to ec_group';


--
-- Name: COLUMN decision_making.has_been_run; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.decision_making.has_been_run IS 'Flag True if Decision Making has been run.';


--
-- Name: COLUMN decision_making.has_populated_integrated_decisions_and_issues; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.decision_making.has_populated_integrated_decisions_and_issues IS 'A flag about the creation of integrated decisions & issues for a parcel, according to the decision making results.';



--
-- Name: documents; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_blob
--

CREATE TABLE niva.documents (
    docu_id integer NOT NULL,
    attached_file bytea,
    file_path character varying(60),
    description character varying(60),
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL
);


--
-- Name: documents_docu_id_seq; Type: SEQUENCE; Schema: niva; Owner: -
--

CREATE SEQUENCE niva.documents_docu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: documents_docu_id_seq; Type: SEQUENCE OWNED BY; Schema: niva; Owner: -
--

ALTER SEQUENCE niva.documents_docu_id_seq OWNED BY niva.documents.docu_id;


--
-- Name: ec_cult; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.ec_cult (
    eccu_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    cult_id integer,
    ecgr_id integer NOT NULL,
    none_match_decision integer,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    coty_id integer,
    suca_id integer
);


--
-- Name: TABLE ec_cult; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.ec_cult IS 'Eligibility Criteria of cultivation';


--
-- Name: COLUMN ec_cult.eccu_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult.eccu_id IS 'primary key';


--
-- Name: COLUMN ec_cult.cult_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult.cult_id IS 'fk to cultivation';


--
-- Name: COLUMN ec_cult.ecgr_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult.ecgr_id IS 'fk to Ec_Group';


--
-- Name: COLUMN ec_cult.none_match_decision; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult.none_match_decision IS '1: green, 2: yellow, 3:red';


--
-- Name: COLUMN ec_cult.coty_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult.coty_id IS 'fk to cover_type';


--
-- Name: COLUMN ec_cult.suca_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult.suca_id IS 'fk to super_class';


--
-- Name: ec_cult_copy; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.ec_cult_copy (
    eccc_id integer NOT NULL,
    cult_id integer,
    ecgc_id integer NOT NULL,
    none_match_decision integer,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    coty_id integer,
    suca_id integer
);


--
-- Name: ec_cult_copy_eccc_id_seq; Type: SEQUENCE; Schema: niva; Owner: -
--

CREATE SEQUENCE niva.ec_cult_copy_eccc_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ec_cult_copy_eccc_id_seq; Type: SEQUENCE OWNED BY; Schema: niva; Owner: -
--

ALTER SEQUENCE niva.ec_cult_copy_eccc_id_seq OWNED BY niva.ec_cult_copy.eccc_id;


--
-- Name: ec_cult_detail; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.ec_cult_detail (
    eccd_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    eccu_id integer NOT NULL,
    ordering_number integer,
    agrees_declar integer,
    comparison_oper integer,
    probab_thres numeric(4,2),
    decision_light integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp(0) with time zone,
    usrupdate character varying(150),
    dteupdate timestamp(0) with time zone,
    row_version integer DEFAULT 0 NOT NULL,
    agrees_declar2 integer,
    comparison_oper2 integer,
    probab_thres2 numeric(4,2),
    probab_thres_sum numeric(4,2),
    comparison_oper3 integer,
    CONSTRAINT eccultdetail_ordering_number_greater_than_zero CHECK ((ordering_number > 0))
);


--
-- Name: TABLE ec_cult_detail; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.ec_cult_detail IS 'criteria of cultivation';


--
-- Name: COLUMN ec_cult_detail.eccd_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.eccd_id IS 'primary key';


--
-- Name: COLUMN ec_cult_detail.eccu_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.eccu_id IS 'fk to cultivation';


--
-- Name: COLUMN ec_cult_detail.ordering_number; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.ordering_number IS 'AA (No.) increasing ordering number';


--
-- Name: COLUMN ec_cult_detail.agrees_declar; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.agrees_declar IS '1: agree, 0: disagree declaration';


--
-- Name: COLUMN ec_cult_detail.comparison_oper; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.comparison_oper IS '1: greater, 2: greater equal, 3:less, 4: less equal';


--
-- Name: COLUMN ec_cult_detail.probab_thres; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.probab_thres IS 'probability threshold ';



--
-- Name: COLUMN ec_cult_detail.decision_light; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.decision_light IS '1: green, 2: yellow, 3:red';


--
-- Name: COLUMN ec_cult_detail.agrees_declar2; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.agrees_declar2 IS '1: agree, 0: disagree declaration';


--
-- Name: COLUMN ec_cult_detail.comparison_oper2; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.comparison_oper2 IS '1: greater, 2: greater equal, 3:less, 4: less equal';


--
-- Name: COLUMN ec_cult_detail.probab_thres2; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.probab_thres2 IS 'probability 2 threshold ';


--
-- Name: COLUMN ec_cult_detail.probab_thres_sum; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.probab_thres_sum IS 'sum of probability 1+2 threshold ';


--
-- Name: COLUMN ec_cult_detail.comparison_oper3; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_cult_detail.comparison_oper3 IS '1: greater, 2: greater equal, 3:less, 4: less equal';


--
-- Name: ec_cult_detail_copy; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.ec_cult_detail_copy (
    ecdc_id integer NOT NULL,
    eccc_id integer NOT NULL,
    ordering_number integer,
    agrees_declar integer,
    comparison_oper integer,
    probab_thres numeric(4,2),
    decision_light integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    agrees_declar2 integer,
    comparison_oper2 integer,
    probab_thres2 numeric(4,2),
    probab_thres_sum numeric(4,2),
    comparison_oper3 integer
);


--
-- Name: ec_cult_detail_copy_ecdc_id_seq; Type: SEQUENCE; Schema: niva; Owner: -
--

CREATE SEQUENCE niva.ec_cult_detail_copy_ecdc_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ec_cult_detail_copy_ecdc_id_seq; Type: SEQUENCE OWNED BY; Schema: niva; Owner: -
--

ALTER SEQUENCE niva.ec_cult_detail_copy_ecdc_id_seq OWNED BY niva.ec_cult_detail_copy.ecdc_id;


--
-- Name: ec_group; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.ec_group (
    ecgr_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    description character varying(60),
    name character varying(60) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    recordtype integer NOT NULL,
    crop_level smallint NOT NULL
);


--
-- Name: TABLE ec_group; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.ec_group IS 'eligibility Criteria group';


--
-- Name: COLUMN ec_group.ecgr_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_group.ecgr_id IS 'primary key';


--
-- Name: COLUMN ec_group.name; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_group.name IS 'ec_group name';


--
-- Name: COLUMN ec_group.crop_level; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.ec_group.crop_level IS '0: Cultivation, 1: Crop Cover(General type)';


--
-- Name: ec_group_copy; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.ec_group_copy (
    ecgc_id integer NOT NULL,
    description character varying(60),
    name character varying(60) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    recordtype integer NOT NULL,
    crop_level smallint NOT NULL,
    dema_id integer
);


--
-- Name: ec_group_copy_ecgc_id_seq; Type: SEQUENCE; Schema: niva; Owner: -
--

CREATE SEQUENCE niva.ec_group_copy_ecgc_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ec_group_copy_ecgc_id_seq; Type: SEQUENCE OWNED BY; Schema: niva; Owner: -
--

ALTER SEQUENCE niva.ec_group_copy_ecgc_id_seq OWNED BY niva.ec_group_copy.ecgc_id;


--
-- Name: excel_errors; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.excel_errors (
    id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    exfi_id integer NOT NULL,
    excel_row_num integer,
    err_message character varying(500),
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE excel_errors; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.excel_errors IS 'excel errors';


--
-- Name: COLUMN excel_errors.id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.excel_errors.id IS 'primary key';


--
-- Name: COLUMN excel_errors.exfi_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.excel_errors.exfi_id IS 'fk to excel_files';


--
-- Name: COLUMN excel_errors.excel_row_num; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.excel_errors.excel_row_num IS 'excel_row_num';


--
-- Name: COLUMN excel_errors.err_message; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.excel_errors.err_message IS 'err_message';



--
-- Name: excel_files; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_blob
--

CREATE TABLE niva.excel_files (
    id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    filename character varying(120) NOT NULL,
    data bytea,
    total_rows integer,
    total_error_rows integer,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE excel_files; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.excel_files IS 'excel files';


--
-- Name: COLUMN excel_files.id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.excel_files.id IS 'primary key';


--
-- Name: COLUMN excel_files.filename; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.excel_files.filename IS 'name of file';


--
-- Name: COLUMN excel_files.data; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.excel_files.data IS 'upload file';


--
-- Name: COLUMN excel_files.total_rows; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.excel_files.total_rows IS 'total rows';


--
-- Name: COLUMN excel_files.total_error_rows; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.excel_files.total_error_rows IS 'total rows';



--
-- Name: file_dir_path; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.file_dir_path (
    fidr_id integer NOT NULL,
    directory character varying(150) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    file_type integer DEFAULT 1 NOT NULL
);


--
-- Name: file_dir_path_fidr_id_seq; Type: SEQUENCE; Schema: niva; Owner: -
--

CREATE SEQUENCE niva.file_dir_path_fidr_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_dir_path_fidr_id_seq; Type: SEQUENCE OWNED BY; Schema: niva; Owner: -
--

ALTER SEQUENCE niva.file_dir_path_fidr_id_seq OWNED BY niva.file_dir_path.fidr_id;


--
-- Name: file_template; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.file_template (
    fite_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    name character varying(60) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE file_template; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.file_template IS 'the template  of the file of input data';


--
-- Name: COLUMN file_template.fite_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.file_template.fite_id IS 'primary key';


--
-- Name: COLUMN file_template.name; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.file_template.name IS 'template general name';


--
-- Name: fmis_decisions; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.fmis_decisions (
    fmis_decisions_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    parcels_issues_id integer NOT NULL,
    crop_ok smallint DEFAULT 0,
    landcover_ok smallint DEFAULT 0,
    dte_insert timestamp with time zone DEFAULT now(),
    usr_insert character varying(150),
    row_version integer DEFAULT 0 NOT NULL,
    cult_id integer NOT NULL,
    coty_id integer NOT NULL
);


--
-- Name: TABLE fmis_decisions; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.fmis_decisions IS 'Contains the decisions related to FMIS for each parcel issue (set of photos).';


--
-- Name: COLUMN fmis_decisions.landcover_ok; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.fmis_decisions.landcover_ok IS 'Compatibility with the declared landcover
0=Not Evaluated
1=Yes
2=No
3=Unclear';


--
-- Name: fmis_uploads; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.fmis_uploads (
    fmis_uploads_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    metadata character varying,
    dte_upload timestamp with time zone DEFAULT now() NOT NULL,
    docfile bytea,
    usr_upload character varying(150),
    parcels_issues_id integer NOT NULL
);


--
-- Name: fmis_users; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.fmis_users (
    fmis_name character varying(40) NOT NULL,
    row_version integer DEFAULT 0 NOT NULL,
    fmis_users_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    fmis_uid character(5) NOT NULL
);


--
-- Name: TABLE fmis_users; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.fmis_users IS 'FMIS User';


--
-- Name: COLUMN fmis_users.fmis_uid; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.fmis_users.fmis_uid IS 'Contains a 5 chars human-readable User Id.';


--
-- Name: gp_decisions; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.gp_decisions (
    gp_decisions_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    parcels_issues_id integer NOT NULL,
    crop_ok smallint DEFAULT 0,
    landcover_ok smallint DEFAULT 0,
    dte_insert timestamp(0) with time zone DEFAULT now() NOT NULL,
    usr_insert character varying(150),
    row_version integer DEFAULT 0 NOT NULL,
    cult_id integer NOT NULL,
    coty_id integer NOT NULL
);


--
-- Name: TABLE gp_decisions; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.gp_decisions IS 'Contains the decisions related to the geotagged photos for each parcel issue (set of photos).';


--
-- Name: COLUMN gp_decisions.landcover_ok; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.gp_decisions.landcover_ok IS 'Compatibility with the declared landcover
0=Not Evaluated
1=Yes
2=No
3=Unclear';


--
-- Name: COLUMN gp_decisions.cult_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.gp_decisions.cult_id IS 'The code of the presumed Crop.';


--
-- Name: COLUMN gp_decisions.coty_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.gp_decisions.coty_id IS 'Code of the presumed Land Cover.';


--
-- Name: gp_requests; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.gp_requests (
    gp_requests_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    dte_received timestamp(0) with time zone DEFAULT now() NOT NULL,
    type_code smallint,
    type_description character varying,
    scheme character varying,
    correspondence_id integer,
    correspondence_doc_nr integer,
    geotag character(1),
    row_version integer DEFAULT 0 NOT NULL,
    agrisnap_users_id integer NOT NULL
);


--
-- Name: TABLE gp_requests; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.gp_requests IS 'Contains the requests (and their responses) of AgriSpan.';


--
-- Name: gp_requests_contexts; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.gp_requests_contexts (
    gp_requests_contexts_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    type character varying,
    label character varying,
    comment character varying,
    geom_hexewkb character varying,
    referencepoint character varying,
    gp_requests_producers_id integer NOT NULL,
    row_version integer DEFAULT 0 NOT NULL,
    hash character(32) NOT NULL,
    parcels_issues_id integer
);


--
-- Name: TABLE gp_requests_contexts; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.gp_requests_contexts IS 'Contains context details about each request & producer.';


--
-- Name: COLUMN gp_requests_contexts.geom_hexewkb; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.gp_requests_contexts.geom_hexewkb IS 'Contains the geometry (with irs SRID) in HEXEWKB format (as text).';


--
-- Name: COLUMN gp_requests_contexts.hash; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.gp_requests_contexts.hash IS 'Hash identification.';


--
-- Name: gp_requests_producers; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.gp_requests_producers (
    gp_requests_id integer NOT NULL,
    producers_id integer NOT NULL,
    gp_requests_producers_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    row_version integer DEFAULT 0 NOT NULL,
    notes character varying
);


--
-- Name: TABLE gp_requests_producers; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.gp_requests_producers IS 'Contains info per Producer of a geotagged photos request. ';


--
-- Name: gp_uploads; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.gp_uploads (
    gp_uploads_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    data character varying,
    environment character varying,
    gp_requests_contexts_id integer NOT NULL,
    dte_upload timestamp with time zone DEFAULT now() NOT NULL,
    hash character(32) NOT NULL,
    image bytea
);


--
-- Name: TABLE gp_uploads; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.gp_uploads IS 'Contains the uploaded FMIS files.';


--
-- Name: integrateddecisions; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.integrateddecisions (
    integrateddecisions_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    pcla_id integer NOT NULL,
    decision_code smallint NOT NULL,
    dte_update timestamp(0) with time zone DEFAULT now() NOT NULL,
    usr_update character varying(150),
    dema_id integer NOT NULL
);


--
-- Name: TABLE integrateddecisions; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.integrateddecisions IS 'Contains the decisions per parcel, based on all modules.';


--
-- Name: COLUMN integrateddecisions.decision_code; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.integrateddecisions.decision_code IS '0=Unevaluated
1=Green
2=Yellow
3=Red
4=Undefined';


--
-- Name: parcel_class; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.parcel_class (
    pcla_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    clas_id integer NOT NULL,
    cult_id_decl integer,
    cult_id_pred integer,
    coty_id_decl integer,
    coty_id_pred integer,
    prob_pred double precision,
    cult_id_pred2 integer,
    prob_pred2 double precision,
    prod_code integer,
    parc_identifier character varying NOT NULL,
    parc_code character varying NOT NULL,
    geom4326 public.geometry(MultiPolygon,4326)
);


--
-- Name: TABLE parcel_class; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.parcel_class IS 'classification of parcel';


--
-- Name: COLUMN parcel_class.pcla_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_class.pcla_id IS 'primary key';


--
-- Name: COLUMN parcel_class.clas_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_class.clas_id IS 'fk to classification';


--
-- Name: COLUMN parcel_class.cult_id_decl; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_class.cult_id_decl IS 'cultivation declared';


--
-- Name: COLUMN parcel_class.cult_id_pred; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_class.cult_id_pred IS 'cultivation predicted';


--
-- Name: COLUMN parcel_class.coty_id_decl; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_class.coty_id_decl IS 'fk to cover_type declared';


--
-- Name: COLUMN parcel_class.coty_id_pred; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_class.coty_id_pred IS 'fk to cover_type predicted';


--
-- Name: COLUMN parcel_class.prob_pred; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_class.prob_pred IS 'probability predicted';


--
-- Name: COLUMN parcel_class.parc_code; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_class.parc_code IS 'Parcel''''s code as String.';


--
-- Name: parcel_decision; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.parcel_decision (
    pade_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    dema_id integer NOT NULL,
    pcla_id integer NOT NULL,
    decision_light integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp(0) with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp(0) with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE parcel_decision; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.parcel_decision IS 'parcel_class decision';


--
-- Name: COLUMN parcel_decision.pade_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_decision.pade_id IS 'primary key';


--
-- Name: COLUMN parcel_decision.dema_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_decision.dema_id IS 'fk to decision_making';


--
-- Name: COLUMN parcel_decision.pcla_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_decision.pcla_id IS 'fk to parcel_class';


--
-- Name: COLUMN parcel_decision.decision_light; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcel_decision.decision_light IS '1: green, 2: yellow, 3:red';


--
-- Name: parcels_issues; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.parcels_issues (
    parcels_issues_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    pcla_id integer NOT NULL,
    dte_created timestamp(0) with time zone DEFAULT now(),
    status smallint,
    dte_status_update timestamp(0) with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    type_of_issue smallint,
    active boolean
);


--
-- Name: TABLE parcels_issues; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.parcels_issues IS 'Contains issues about the not indisputably classified parcels.
';


--
-- Name: COLUMN parcels_issues.status; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcels_issues.status IS 'value of 900 (or greater) means inactive.

value 1 means was created.
value 2 means at least one photo request has occured.
value 4 means at least one photo has been uploaded. 
value 8 means at least a FMIS has been uploaded.
value 16 means both at least one photo and a FMIS have been uploaded.
value 32 means crop level photo check has been performed.
value 64 means crop level FMIS check has been performed.
value 128 means land cover level photo check has been performed.
value 256 means cover level FMIS check has been performed.
value 900 means issue has been solved.';


--
-- Name: COLUMN parcels_issues.type_of_issue; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcels_issues.type_of_issue IS 'Type of Issue
1=Declared Crop doesn''t match with Decision Making
2= Declated Land Cover doesn''t match with Decision Making
4=Declared Crop doesn''t match with Geotagged Photos
8=Declared Land Cover doesn''t match with Geotagged Photos
16=Declared Crop doesn''t match with FMIS
32=Declared Land Cover doesn''t match with FMIS';


--
-- Name: COLUMN parcels_issues.active; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.parcels_issues.active IS 'True if the issue is considered totally solved.';


--
-- Name: parcels_issues_activities; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.parcels_issues_activities (
    parcels_issues_activities_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    row_version integer DEFAULT 0 NOT NULL,
    parcels_issues_id integer NOT NULL,
    type smallint NOT NULL,
    type_extrainfo character varying,
    dte_timestamp timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE parcels_issues_activities; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.parcels_issues_activities IS 'Contains activities related to each Parcel Issue, e.g. AgriSpan interaction, etc.';


--
-- Name: parcels_issues_status_per_dema; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.parcels_issues_status_per_dema (
    parcels_issues_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    status smallint NOT NULL,
    dte_status_update timestamp(0) with time zone DEFAULT now() NOT NULL,
    row_version integer DEFAULT 0 NOT NULL,
    parcels_issues_status_per_dema_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL
);


--
-- Name: TABLE parcels_issues_status_per_dema; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.parcels_issues_status_per_dema IS 'Status of a parcels issue related to a decision making.';


--
-- Name: predef_col; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.predef_col (
    prco_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    column_name character varying(60) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL,
    system_column_name character varying
);


--
-- Name: TABLE predef_col; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.predef_col IS 'predefine columns';


--
-- Name: COLUMN predef_col.prco_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.predef_col.prco_id IS 'primary key';


--
-- Name: COLUMN predef_col.column_name; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.predef_col.column_name IS 'dss column name - internal system name';


--
-- Name: producers; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.producers (
    prod_name character varying(40) NOT NULL,
    row_version integer DEFAULT 0 NOT NULL,
    prod_code integer NOT NULL,
    producers_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL
);


--
-- Name: TABLE producers; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.producers IS 'Contains info about the farmers.';


--
-- Name: statistics; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.statistics (
    stat_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    clas_id integer NOT NULL,
    name character varying(60) NOT NULL,
    description character varying(60),
    value integer,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE statistics; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.statistics IS 'Statistics';


--
-- Name: COLUMN statistics.stat_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.statistics.stat_id IS 'primary key';


--
-- Name: COLUMN statistics.clas_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.statistics.clas_id IS 'fk to classification';


--
-- Name: COLUMN statistics.value; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.statistics.value IS 'value';


--
-- Name: super_class; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.super_class (
    suca_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    code character varying(30) NOT NULL,
    name character varying(60) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE super_class; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.super_class IS 'super_class';


--
-- Name: COLUMN super_class.suca_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.super_class.suca_id IS 'primary key';


--
-- Name: COLUMN super_class.code; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.super_class.code IS 'code super class';


--
-- Name: COLUMN super_class.name; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.super_class.name IS 'name of super class';


--
-- Name: super_class_detail; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.super_class_detail (
    sucd_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    suca_id integer NOT NULL,
    cult_id integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE super_class_detail; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.super_class_detail IS 'super_class_detail';


--
-- Name: COLUMN super_class_detail.sucd_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.super_class_detail.sucd_id IS 'primary key';


--
-- Name: COLUMN super_class_detail.suca_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.super_class_detail.suca_id IS 'fk to super_class';


--
-- Name: COLUMN super_class_detail.cult_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.super_class_detail.cult_id IS 'fk to cultivation';


--
-- Name: systemconfiguration; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.systemconfiguration (
    agency_id integer NOT NULL,
    srid smallint,
    language character(2)
);


--
-- Name: TABLE systemconfiguration; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.systemconfiguration IS 'Contains parameters about the configuration, mainly according to the selected Agency.';


--
-- Name: template_columns; Type: TABLE; Schema: niva; Owner: -; Tablespace: niva_data
--

CREATE TABLE niva.template_columns (
    teco_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    fite_id integer NOT NULL,
    prco_id integer NOT NULL,
    clfier_name character varying(60) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone,
    usrupdate character varying(150),
    dteupdate timestamp without time zone,
    row_version integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE template_columns; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON TABLE niva.template_columns IS 'the columns of the template file';


--
-- Name: COLUMN template_columns.teco_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.template_columns.teco_id IS 'primary key';


--
-- Name: COLUMN template_columns.fite_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.template_columns.fite_id IS 'fk to file template';


--
-- Name: COLUMN template_columns.prco_id; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.template_columns.prco_id IS 'fk to file predefine columns';


--
-- Name: COLUMN template_columns.clfier_name; Type: COMMENT; Schema: niva; Owner: -
--

COMMENT ON COLUMN niva.template_columns.clfier_name IS 'classifier name -inputs file name';


--
-- Name: tmp_blob; Type: TABLE; Schema: niva; Owner: -
--

CREATE TABLE niva.tmp_blob (
    id integer NOT NULL,
    data bytea,
    row_version integer DEFAULT 0 NOT NULL,
    dteinsert timestamp without time zone,
    usrinsert character varying(150),
    dteupdate timestamp without time zone,
    usrupdate character varying(150)
);





--
-- Name: tmp_blob_id_seq; Type: SEQUENCE; Schema: niva; Owner: -
--

CREATE SEQUENCE niva.tmp_blob_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;





--
-- Name: tmp_blob_id_seq; Type: SEQUENCE OWNED BY; Schema: niva; Owner: -
--

ALTER SEQUENCE niva.tmp_blob_id_seq OWNED BY niva.tmp_blob.id;


--
-- Name: documents docu_id; Type: DEFAULT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.documents ALTER COLUMN docu_id SET DEFAULT nextval('niva.documents_docu_id_seq'::regclass);


--
-- Name: ec_cult_copy eccc_id; Type: DEFAULT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_copy ALTER COLUMN eccc_id SET DEFAULT nextval('niva.ec_cult_copy_eccc_id_seq'::regclass);


--
-- Name: ec_cult_detail_copy ecdc_id; Type: DEFAULT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_detail_copy ALTER COLUMN ecdc_id SET DEFAULT nextval('niva.ec_cult_detail_copy_ecdc_id_seq'::regclass);


--
-- Name: ec_group_copy ecgc_id; Type: DEFAULT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_group_copy ALTER COLUMN ecgc_id SET DEFAULT nextval('niva.ec_group_copy_ecgc_id_seq'::regclass);


--
-- Name: file_dir_path fidr_id; Type: DEFAULT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.file_dir_path ALTER COLUMN fidr_id SET DEFAULT nextval('niva.file_dir_path_fidr_id_seq'::regclass);


--
-- Name: tmp_blob id; Type: DEFAULT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.tmp_blob ALTER COLUMN id SET DEFAULT nextval('niva.tmp_blob_id_seq'::regclass);


--
-- Name: agency agen_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.agency
    ADD CONSTRAINT agen_pk PRIMARY KEY (agen_id);


--
-- Name: agency agenname_uk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.agency
    ADD CONSTRAINT agenname_uk UNIQUE (name);


--
-- Name: agrisnap_users agrisnap_users_pkey; Type: CONSTRAINT; Schema: niva; Owner: -; Tablespace: niva_data
--

ALTER TABLE ONLY niva.agrisnap_users
    ADD CONSTRAINT agrisnap_users_pkey PRIMARY KEY (agrisnap_users_id);


--
-- Name: agrisnap_users_producers agrisnap_users_producers_id_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.agrisnap_users_producers
    ADD CONSTRAINT agrisnap_users_producers_id_pkey PRIMARY KEY (agrisnap_users_producers_id);


--
-- Name: agrisnap_users_producers agrisnapproducers_agrisnap_users_id_producers_id_uk; Type: CONSTRAINT; Schema: niva; Owner: -; Tablespace: niva_data
--

ALTER TABLE ONLY niva.agrisnap_users_producers
    ADD CONSTRAINT agrisnapproducers_agrisnap_users_id_producers_id_uk UNIQUE (agrisnap_users_id, agrisnap_users_producers_id);


--
-- Name: classification clas_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.classification
    ADD CONSTRAINT clas_pk PRIMARY KEY (clas_id);


--
-- Name: classifier clfr_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.classifier
    ADD CONSTRAINT clfr_pk PRIMARY KEY (clfr_id);


--
-- Name: cover_type coty_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.cover_type
    ADD CONSTRAINT coty_pk PRIMARY KEY (coty_id);


--
-- Name: cover_type cotycode_name_uk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.cover_type
    ADD CONSTRAINT cotycode_name_uk UNIQUE (code, name);


--
-- Name: cover_type cover_type_code_un; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.cover_type
    ADD CONSTRAINT cover_type_code_un UNIQUE (code);


--
-- Name: cover_type cover_type_name_un; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.cover_type
    ADD CONSTRAINT cover_type_name_un UNIQUE (name);


--
-- Name: cultivation cult_code_un; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.cultivation
    ADD CONSTRAINT cult_code_un UNIQUE (code);


--
-- Name: cultivation cult_name_un; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.cultivation
    ADD CONSTRAINT cult_name_un UNIQUE (name);


--
-- Name: cultivation cult_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.cultivation
    ADD CONSTRAINT cult_pk PRIMARY KEY (cult_id);


--
-- Name: decision_making dema_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.decision_making
    ADD CONSTRAINT dema_pk PRIMARY KEY (dema_id);


--
-- Name: documents docu_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.documents
    ADD CONSTRAINT docu_pk PRIMARY KEY (docu_id);


--
-- Name: ec_cult_copy eccc_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_copy
    ADD CONSTRAINT eccc_pk PRIMARY KEY (eccc_id);


--
-- Name: ec_cult_detail eccd_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_detail
    ADD CONSTRAINT eccd_pk PRIMARY KEY (eccd_id);


--
-- Name: ec_cult eccu_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult
    ADD CONSTRAINT eccu_pk PRIMARY KEY (eccu_id);


--
-- Name: ec_cult_detail eccultdetail_eccu_id_orderingnumber_un; Type: CONSTRAINT; Schema: niva; Owner: -; Tablespace: niva_data
--

ALTER TABLE ONLY niva.ec_cult_detail
    ADD CONSTRAINT eccultdetail_eccu_id_orderingnumber_un UNIQUE (eccu_id, ordering_number);


--
-- Name: ec_cult_detail_copy ecdc_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_detail_copy
    ADD CONSTRAINT ecdc_pk PRIMARY KEY (ecdc_id);


--
-- Name: ec_group_copy ecgc_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_group_copy
    ADD CONSTRAINT ecgc_pk PRIMARY KEY (ecgc_id);


--
-- Name: ec_group ecgr_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_group
    ADD CONSTRAINT ecgr_pk PRIMARY KEY (ecgr_id);


--
-- Name: excel_errors exer_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.excel_errors
    ADD CONSTRAINT exer_pk PRIMARY KEY (id);


--
-- Name: excel_files exfi_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.excel_files
    ADD CONSTRAINT exfi_pk PRIMARY KEY (id);


--
-- Name: file_dir_path fidr_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.file_dir_path
    ADD CONSTRAINT fidr_pk PRIMARY KEY (fidr_id);


--
-- Name: file_dir_path file_dir_path_un; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.file_dir_path
    ADD CONSTRAINT file_dir_path_un UNIQUE (file_type);


--
-- Name: file_template file_template_name_un; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.file_template
    ADD CONSTRAINT file_template_name_un UNIQUE (name);



--
-- Name: file_template fite_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.file_template
    ADD CONSTRAINT fite_pk PRIMARY KEY (fite_id);


--
-- Name: fmis_decisions fmis_decisions_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.fmis_decisions
    ADD CONSTRAINT fmis_decisions_pkey PRIMARY KEY (fmis_decisions_id);


--
-- Name: fmis_uploads fmis_uploads_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.fmis_uploads
    ADD CONSTRAINT fmis_uploads_pkey PRIMARY KEY (fmis_uploads_id);


--
-- Name: fmis_users fmis_users_pkey; Type: CONSTRAINT; Schema: niva; Owner: -; Tablespace: niva_data
--

ALTER TABLE ONLY niva.fmis_users
    ADD CONSTRAINT fmis_users_pkey PRIMARY KEY (fmis_users_id);


--
-- Name: gp_requests_contexts gpRequestsContexts_hash_uk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_requests_contexts
    ADD CONSTRAINT "gpRequestsContexts_hash_uk" UNIQUE (hash);


--
-- Name: gp_decisions gp_decisions_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_decisions
    ADD CONSTRAINT gp_decisions_pkey PRIMARY KEY (gp_decisions_id);


--
-- Name: gp_requests_contexts gp_requests_contexts_pkey; Type: CONSTRAINT; Schema: niva; Owner: -; Tablespace: niva_data
--

ALTER TABLE ONLY niva.gp_requests_contexts
    ADD CONSTRAINT gp_requests_contexts_pkey PRIMARY KEY (gp_requests_contexts_id);


--
-- Name: gp_requests gp_requests_id_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_requests
    ADD CONSTRAINT gp_requests_id_pkey PRIMARY KEY (gp_requests_id);


--
-- Name: gp_requests_producers gp_requests_producers_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_requests_producers
    ADD CONSTRAINT gp_requests_producers_pkey PRIMARY KEY (gp_requests_producers_id);


--
-- Name: gp_uploads gp_uploads_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_uploads
    ADD CONSTRAINT gp_uploads_pkey PRIMARY KEY (gp_uploads_id);


--
-- Name: gp_requests_producers gprequestsproducers_uk; Type: CONSTRAINT; Schema: niva; Owner: -; Tablespace: niva_data
--

ALTER TABLE ONLY niva.gp_requests_producers
    ADD CONSTRAINT gprequestsproducers_uk UNIQUE (gp_requests_id, producers_id);


--
-- Name: integrateddecisions integrateddecisions_unique_pcla_dema; Type: CONSTRAINT; Schema: niva; Owner: -; Tablespace: niva_data
--

ALTER TABLE ONLY niva.integrateddecisions
    ADD CONSTRAINT integrateddecisions_unique_pcla_dema UNIQUE (pcla_id, dema_id);


--
-- Name: tmp_blob nitb_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.tmp_blob
    ADD CONSTRAINT nitb_pk PRIMARY KEY (id);


--
-- Name: parcel_decision pade_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcel_decision
    ADD CONSTRAINT pade_pk PRIMARY KEY (pade_id);


--
-- Name: parcels_issues_activities parcels_issues_activities_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcels_issues_activities
    ADD CONSTRAINT parcels_issues_activities_pkey PRIMARY KEY (parcels_issues_activities_id);


--
-- Name: parcels_issues parcels_issues_pkey; Type: CONSTRAINT; Schema: niva; Owner: -; Tablespace: niva_data
--

ALTER TABLE ONLY niva.parcels_issues
    ADD CONSTRAINT parcels_issues_pkey PRIMARY KEY (parcels_issues_id);


--
-- Name: parcels_issues_status_per_dema parcels_issues_status_per_dema_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcels_issues_status_per_dema
    ADD CONSTRAINT parcels_issues_status_per_dema_pkey PRIMARY KEY (parcels_issues_status_per_dema_id);

--
-- Name: parcel_class pcla_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcel_class
    ADD CONSTRAINT pcla_pk PRIMARY KEY (pcla_id);


--
-- Name: template_columns prco_fite_un; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.template_columns
    ADD CONSTRAINT prco_fite_un UNIQUE (fite_id, prco_id);


--
-- Name: predef_col prco_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.predef_col
    ADD CONSTRAINT prco_pk PRIMARY KEY (prco_id);


--
-- Name: predef_col predef_col_un; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.predef_col
    ADD CONSTRAINT predef_col_un UNIQUE (column_name);


--
-- Name: producers producers_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.producers
    ADD CONSTRAINT producers_pkey PRIMARY KEY (producers_id);


--
-- Name: statistics stat_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.statistics
    ADD CONSTRAINT stat_pk PRIMARY KEY (stat_id);


--
-- Name: super_class suca_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.super_class
    ADD CONSTRAINT suca_pk PRIMARY KEY (suca_id);


--
-- Name: super_class_detail sucd_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.super_class_detail
    ADD CONSTRAINT sucd_pk PRIMARY KEY (sucd_id);


--
-- Name: super_class super_class_code_un; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.super_class
    ADD CONSTRAINT super_class_code_un UNIQUE (code);


--
-- Name: super_class super_class_name_un; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.super_class
    ADD CONSTRAINT super_class_name_un UNIQUE (name);


--
-- Name: systemconfiguration systemconfiguration_pkey; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.systemconfiguration
    ADD CONSTRAINT systemconfiguration_pkey PRIMARY KEY (agency_id);


--
-- Name: template_columns teco_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.template_columns
    ADD CONSTRAINT teco_pk PRIMARY KEY (teco_id);


--
-- Name: integrateddecisions tmpintegrateddecisions_pk; Type: CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.integrateddecisions
    ADD CONSTRAINT tmpintegrateddecisions_pk PRIMARY KEY (integrateddecisions_id);


--
-- Name: clas_clfr_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX clas_clfr_id_fi ON niva.classification USING btree (clfr_id);


--
-- Name: clas_fite_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX clas_fite_id_fi ON niva.classification USING btree (fite_id);



--
-- Name: cult_coty_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX cult_coty_id_fi ON niva.cultivation USING btree (coty_id);


--
-- Name: dema_clas_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX dema_clas_id_fi ON niva.decision_making USING btree (clas_id);


--
-- Name: dema_ecgr_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX dema_ecgr_id_fi ON niva.decision_making USING btree (ecgr_id);


--
-- Name: eccc_cult_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX eccc_cult_id_fi ON niva.ec_cult_copy USING btree (cult_id);


--
-- Name: eccc_ecgc_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX eccc_ecgc_id_fi ON niva.ec_cult_copy USING btree (ecgc_id);


--
-- Name: eccc_suca_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX eccc_suca_id_fi ON niva.ec_cult_copy USING btree (suca_id);


--
-- Name: eccd_eccu_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX eccd_eccu_id_fi ON niva.ec_cult_detail USING btree (eccu_id);


--
-- Name: eccu_cult_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX eccu_cult_id_fi ON niva.ec_cult USING btree (cult_id);


--
-- Name: eccu_ecgr_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX eccu_ecgr_id_fi ON niva.ec_cult USING btree (ecgr_id);


--
-- Name: eccu_suca_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX eccu_suca_id_fi ON niva.ec_cult USING btree (suca_id);


--
-- Name: ecdc_eccc_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX ecdc_eccc_id_fi ON niva.ec_cult_detail_copy USING btree (eccc_id);


--
-- Name: exer_exfi_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX exer_exfi_id_fi ON niva.excel_errors USING btree (exfi_id);


--
-- Name: pade_dema_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX pade_dema_id_fi ON niva.parcel_decision USING btree (dema_id);


--
-- Name: pade_pcla_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX pade_pcla_id_fi ON niva.parcel_decision USING btree (pcla_id);


--
-- Name: pcla_clas_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX pcla_clas_id_fi ON niva.parcel_class USING btree (clas_id);


--
-- Name: pcla_coty_id_decl_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX pcla_coty_id_decl_fi ON niva.parcel_class USING btree (coty_id_decl);


--
-- Name: pcla_coty_id_pred_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX pcla_coty_id_pred_fi ON niva.parcel_class USING btree (coty_id_pred);


--
-- Name: pcla_cult_id_decl_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX pcla_cult_id_decl_fi ON niva.parcel_class USING btree (cult_id_decl);


--
-- Name: pcla_cult_id_pred_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX pcla_cult_id_pred_fi ON niva.parcel_class USING btree (cult_id_pred);


--
-- Name: stat_clas_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX stat_clas_id_fi ON niva.statistics USING btree (clas_id);


--
-- Name: sucd_cult_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX sucd_cult_id_fi ON niva.super_class_detail USING btree (cult_id);


--
-- Name: sucd_suca_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX sucd_suca_id_fi ON niva.super_class_detail USING btree (suca_id);


--
-- Name: sucdsuca_id_cult_id__ui; Type: INDEX; Schema: niva; Owner: -
--

CREATE UNIQUE INDEX sucdsuca_id_cult_id__ui ON niva.super_class_detail USING btree (suca_id, cult_id);


--
-- Name: teco_fite_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX teco_fite_id_fi ON niva.template_columns USING btree (fite_id);


--
-- Name: teco_prco_id_fi; Type: INDEX; Schema: niva; Owner: -
--

CREATE INDEX teco_prco_id_fi ON niva.template_columns USING btree (prco_id);


--
-- Name: file_template file_template_tr; Type: TRIGGER; Schema: niva; Owner: -
--

CREATE TRIGGER file_template_tr AFTER INSERT ON niva.file_template FOR EACH ROW EXECUTE FUNCTION niva.file_template();


--
-- Name: agrisnap_users_producers agrisnapusersproducers_producers_producers_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.agrisnap_users_producers
    ADD CONSTRAINT agrisnapusersproducers_producers_producers_id_fk FOREIGN KEY (producers_id) REFERENCES niva.producers(producers_id) NOT VALID;


--
-- Name: agrisnap_users_producers agrisnapusersroducers_agrisnapusers_agrisnap_users_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.agrisnap_users_producers
    ADD CONSTRAINT agrisnapusersroducers_agrisnapusers_agrisnap_users_id_fk FOREIGN KEY (agrisnap_users_id) REFERENCES niva.agrisnap_users(agrisnap_users_id) NOT VALID;


--
-- Name: classification clas_clfr_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.classification
    ADD CONSTRAINT clas_clfr_id_fk FOREIGN KEY (clfr_id) REFERENCES niva.classifier(clfr_id);


--
-- Name: classification clas_fite_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.classification
    ADD CONSTRAINT clas_fite_id_fk FOREIGN KEY (fite_id) REFERENCES niva.file_template(fite_id);



--
-- Name: cover_type coty_exfi_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.cover_type
    ADD CONSTRAINT coty_exfi_id_fk FOREIGN KEY (exfi_id) REFERENCES niva.excel_files(id);


--
-- Name: cultivation cult_coty_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.cultivation
    ADD CONSTRAINT cult_coty_id_fk FOREIGN KEY (coty_id) REFERENCES niva.cover_type(coty_id);


--
-- Name: cultivation cult_exfi_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.cultivation
    ADD CONSTRAINT cult_exfi_id_fk FOREIGN KEY (exfi_id) REFERENCES niva.excel_files(id);


--
-- Name: decision_making dema_clas_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.decision_making
    ADD CONSTRAINT dema_clas_id_fk FOREIGN KEY (clas_id) REFERENCES niva.classification(clas_id);


--
-- Name: decision_making dema_ecgr_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.decision_making
    ADD CONSTRAINT dema_ecgr_id_fk FOREIGN KEY (ecgr_id) REFERENCES niva.ec_group(ecgr_id);


--
-- Name: ec_cult_copy ec_cult_copy_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_copy
    ADD CONSTRAINT ec_cult_copy_fk FOREIGN KEY (coty_id) REFERENCES niva.cover_type(coty_id);


--
-- Name: ec_cult ec_cult_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult
    ADD CONSTRAINT ec_cult_fk FOREIGN KEY (coty_id) REFERENCES niva.cover_type(coty_id);


--
-- Name: ec_cult_copy eccc_cult_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_copy
    ADD CONSTRAINT eccc_cult_id_fk FOREIGN KEY (cult_id) REFERENCES niva.cultivation(cult_id);


--
-- Name: ec_cult_copy eccc_ecgc_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_copy
    ADD CONSTRAINT eccc_ecgc_id_fk FOREIGN KEY (ecgc_id) REFERENCES niva.ec_group_copy(ecgc_id);


--
-- Name: ec_cult_copy eccc_suca_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_copy
    ADD CONSTRAINT eccc_suca_id_fk FOREIGN KEY (suca_id) REFERENCES niva.super_class(suca_id);


--
-- Name: ec_cult_detail eccd_eccu_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_detail
    ADD CONSTRAINT eccd_eccu_id_fk FOREIGN KEY (eccu_id) REFERENCES niva.ec_cult(eccu_id);


--
-- Name: ec_cult eccu_cult_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult
    ADD CONSTRAINT eccu_cult_id_fk FOREIGN KEY (cult_id) REFERENCES niva.cultivation(cult_id);


--
-- Name: ec_cult eccu_ecgr_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult
    ADD CONSTRAINT eccu_ecgr_id_fk FOREIGN KEY (ecgr_id) REFERENCES niva.ec_group(ecgr_id);


--
-- Name: ec_cult eccu_suca_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult
    ADD CONSTRAINT eccu_suca_id_fk FOREIGN KEY (suca_id) REFERENCES niva.super_class(suca_id);


--
-- Name: ec_cult_detail_copy ecdc_eccc_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_cult_detail_copy
    ADD CONSTRAINT ecdc_eccc_id_fk FOREIGN KEY (eccc_id) REFERENCES niva.ec_cult_copy(eccc_id);


--
-- Name: ec_group_copy ecgc_dema_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.ec_group_copy
    ADD CONSTRAINT ecgc_dema_id_fk FOREIGN KEY (dema_id) REFERENCES niva.decision_making(dema_id);


--
-- Name: excel_errors exer_exfi_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.excel_errors
    ADD CONSTRAINT exer_exfi_id_fk FOREIGN KEY (exfi_id) REFERENCES niva.excel_files(id);


--
-- Name: fmis_decisions fmisdecisions_covertype_coty_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.fmis_decisions
    ADD CONSTRAINT fmisdecisions_covertype_coty_id_fk FOREIGN KEY (coty_id) REFERENCES niva.cover_type(coty_id) NOT VALID;


--
-- Name: fmis_decisions fmisdecisions_cultivation_cult_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.fmis_decisions
    ADD CONSTRAINT fmisdecisions_cultivation_cult_id_fk FOREIGN KEY (cult_id) REFERENCES niva.cultivation(cult_id) NOT VALID;


--
-- Name: fmis_decisions fmisdecisions_parcelsissues_parcels_issues_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.fmis_decisions
    ADD CONSTRAINT fmisdecisions_parcelsissues_parcels_issues_id_fk FOREIGN KEY (parcels_issues_id) REFERENCES niva.parcels_issues(parcels_issues_id) NOT VALID;


--
-- Name: fmis_uploads fmisuploads_parcelsissues_parcelsissues_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.fmis_uploads
    ADD CONSTRAINT fmisuploads_parcelsissues_parcelsissues_id_fk FOREIGN KEY (parcels_issues_id) REFERENCES niva.parcels_issues(parcels_issues_id) NOT VALID;


--
-- Name: gp_uploads gpUploads_gpRequestsContexts_gp_requests_contexts_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_uploads
    ADD CONSTRAINT "gpUploads_gpRequestsContexts_gp_requests_contexts_id_fk" FOREIGN KEY (gp_requests_contexts_id) REFERENCES niva.gp_requests_contexts(gp_requests_contexts_id) NOT VALID;


--
-- Name: gp_decisions gpdecisions_covertype_coty_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_decisions
    ADD CONSTRAINT gpdecisions_covertype_coty_id_fk FOREIGN KEY (coty_id) REFERENCES niva.cover_type(coty_id) NOT VALID;


--
-- Name: gp_decisions gpdecisions_cultivation_cult_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_decisions
    ADD CONSTRAINT gpdecisions_cultivation_cult_id_fk FOREIGN KEY (cult_id) REFERENCES niva.cultivation(cult_id) NOT VALID;


--
-- Name: gp_decisions gpdecisions_parcelsissues_parcels_issues_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_decisions
    ADD CONSTRAINT gpdecisions_parcelsissues_parcels_issues_id_fk FOREIGN KEY (parcels_issues_id) REFERENCES niva.parcels_issues(parcels_issues_id) NOT VALID;


--
-- Name: gp_requests gprequests_agrisnapusers_agrisnap_users_id; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_requests
    ADD CONSTRAINT gprequests_agrisnapusers_agrisnap_users_id FOREIGN KEY (agrisnap_users_id) REFERENCES niva.agrisnap_users(agrisnap_users_id) NOT VALID;


--
-- Name: gp_requests_contexts gprequestscontexts_gprequestsproducers_gp_requests_producers_id; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_requests_contexts
    ADD CONSTRAINT gprequestscontexts_gprequestsproducers_gp_requests_producers_id FOREIGN KEY (gp_requests_producers_id) REFERENCES niva.gp_requests_producers(gp_requests_producers_id) NOT VALID;


--
-- Name: gp_requests_contexts gprequestscontexts_parcelsissues_parcrels_issues_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_requests_contexts
    ADD CONSTRAINT gprequestscontexts_parcelsissues_parcrels_issues_id_fk FOREIGN KEY (parcels_issues_id) REFERENCES niva.parcels_issues(parcels_issues_id) NOT VALID;


--
-- Name: gp_requests_producers gprequestsproducers_gp_gprequests_requests_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_requests_producers
    ADD CONSTRAINT gprequestsproducers_gp_gprequests_requests_id_fk FOREIGN KEY (gp_requests_id) REFERENCES niva.gp_requests(gp_requests_id) NOT VALID;


--
-- Name: gp_requests_producers gprequestsproducers_producers_producers_id; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.gp_requests_producers
    ADD CONSTRAINT gprequestsproducers_producers_producers_id FOREIGN KEY (producers_id) REFERENCES niva.producers(producers_id) NOT VALID;


--
-- Name: integrateddecisions integrateddecisions_decisionmaking_dema_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.integrateddecisions
    ADD CONSTRAINT integrateddecisions_decisionmaking_dema_id_fk FOREIGN KEY (dema_id) REFERENCES niva.decision_making(dema_id) NOT VALID;


--
-- Name: integrateddecisions integrateddecisions_parcelClass_pcla_id; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.integrateddecisions
    ADD CONSTRAINT "integrateddecisions_parcelClass_pcla_id" FOREIGN KEY (pcla_id) REFERENCES niva.parcel_class(pcla_id) NOT VALID;


--
-- Name: parcel_decision pade_dema_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcel_decision
    ADD CONSTRAINT pade_dema_id_fk FOREIGN KEY (dema_id) REFERENCES niva.decision_making(dema_id);


--
-- Name: parcel_decision pade_pcla_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcel_decision
    ADD CONSTRAINT pade_pcla_id_fk FOREIGN KEY (pcla_id) REFERENCES niva.parcel_class(pcla_id);


--
-- Name: parcel_class parcel_class_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcel_class
    ADD CONSTRAINT parcel_class_fk FOREIGN KEY (cult_id_decl) REFERENCES niva.cultivation(cult_id);


--
-- Name: parcel_class parcel_class_fk_1; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcel_class
    ADD CONSTRAINT parcel_class_fk_1 FOREIGN KEY (cult_id_pred) REFERENCES niva.cultivation(cult_id);


--
-- Name: parcel_class parcel_class_fk_2; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcel_class
    ADD CONSTRAINT parcel_class_fk_2 FOREIGN KEY (cult_id_pred2) REFERENCES niva.cultivation(cult_id);


--
-- Name: parcels_issues parcelsissues_parcelClass_pcla_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcels_issues
    ADD CONSTRAINT "parcelsissues_parcelClass_pcla_id_fk" FOREIGN KEY (pcla_id) REFERENCES niva.parcel_class(pcla_id) NOT VALID;


--
-- Name: parcels_issues_activities parcelsissuesactivities_parcelsissues_parcels_issues_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcels_issues_activities
    ADD CONSTRAINT parcelsissuesactivities_parcelsissues_parcels_issues_id_fk FOREIGN KEY (parcels_issues_id) REFERENCES niva.parcels_issues(parcels_issues_id) NOT VALID;


--
-- Name: parcels_issues_status_per_dema parcelsissuesstatusperdema_parcelsissues_parcels_issues_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcels_issues_status_per_dema
    ADD CONSTRAINT parcelsissuesstatusperdema_parcelsissues_parcels_issues_id_fk FOREIGN KEY (parcels_issues_id) REFERENCES niva.parcels_issues(parcels_issues_id) NOT VALID;


--
-- Name: parcel_class pcla_clas_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcel_class
    ADD CONSTRAINT pcla_clas_id_fk FOREIGN KEY (clas_id) REFERENCES niva.classification(clas_id);


--
-- Name: parcel_class pcla_coty_id_decl_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcel_class
    ADD CONSTRAINT pcla_coty_id_decl_fk FOREIGN KEY (coty_id_decl) REFERENCES niva.cover_type(coty_id);


--
-- Name: parcel_class pcla_coty_id_pred_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.parcel_class
    ADD CONSTRAINT pcla_coty_id_pred_fk FOREIGN KEY (coty_id_pred) REFERENCES niva.cover_type(coty_id);


--
-- Name: statistics stat_clas_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.statistics
    ADD CONSTRAINT stat_clas_id_fk FOREIGN KEY (clas_id) REFERENCES niva.classification(clas_id);


--
-- Name: super_class_detail sucd_cult_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.super_class_detail
    ADD CONSTRAINT sucd_cult_id_fk FOREIGN KEY (cult_id) REFERENCES niva.cultivation(cult_id);


--
-- Name: super_class_detail sucd_suca_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.super_class_detail
    ADD CONSTRAINT sucd_suca_id_fk FOREIGN KEY (suca_id) REFERENCES niva.super_class(suca_id);


--
-- Name: template_columns teco_fite_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.template_columns
    ADD CONSTRAINT teco_fite_id_fk FOREIGN KEY (fite_id) REFERENCES niva.file_template(fite_id);


--
-- Name: template_columns teco_prco_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: -
--

ALTER TABLE ONLY niva.template_columns
    ADD CONSTRAINT teco_prco_id_fk FOREIGN KEY (prco_id) REFERENCES niva.predef_col(prco_id);



-- Populate predef_col table


INSERT INTO niva.predef_col(prco_id, column_name, usrinsert, system_column_name) VALUES (301, 'Parcel Identifier', 'admin','identifier');
INSERT INTO niva.predef_col(prco_id, column_name, usrinsert, system_column_name) VALUES (302, 'Parcel Code', 'admin','parc_code');
INSERT INTO niva.predef_col(prco_id, column_name, usrinsert, system_column_name) VALUES (303, 'Farmer Code', 'admin','prod_code');
INSERT INTO niva.predef_col(prco_id, column_name, usrinsert, system_column_name) VALUES (304, 'Cultivation Declared', 'admin','cult_id_decl');
INSERT INTO niva.predef_col(prco_id, column_name, usrinsert, system_column_name) VALUES (305, 'Cult Predicted 1', 'admin','cult_id_pred');
INSERT INTO niva.predef_col(prco_id, column_name, usrinsert, system_column_name) VALUES (306, 'Cult Predicted Confidence 1', 'admin','prob_pred');
INSERT INTO niva.predef_col(prco_id, column_name, usrinsert, system_column_name) VALUES (307, 'Cult Predicted 2', 'admin','cult_id_pred2');
INSERT INTO niva.predef_col(prco_id, column_name, usrinsert, system_column_name) VALUES (308, 'Cult Predicted Confidence 2', 'admin','prob_pred2');


-- INITIAL VALUES

-- Agencies

INSERT INTO niva.agency VALUES (148, 'RVO', 'the Netherlands', 'u1@Niva', '2020-07-16 09:54:30.329', NULL, NULL, 0, 4326);
INSERT INTO niva.agency VALUES (149, 'LBST', 'Denmark', 'u1@Niva', '2020-07-16 09:54:30.66', NULL, NULL, 0, 4326);
INSERT INTO niva.agency VALUES (228, 'NPA', 'Lithuania', 'u1@Niva', '2020-08-27 15:35:11.842', NULL, NULL, 0, 4326);
INSERT INTO niva.agency VALUES (230, 'ARIB', 'Estonia', 'u1@Niva', '2020-08-27 15:39:59.427', NULL, NULL, 0, 4326);
INSERT INTO niva.agency VALUES (231, 'ASP', 'France', 'u1@Niva', '2020-08-27 15:39:59.433', NULL, NULL, 0, 4326);
INSERT INTO niva.agency VALUES (232, 'DAFM', 'Ireland', 'u1@Niva', '2020-08-27 15:39:59.436', NULL, NULL, 0, 4326);
INSERT INTO niva.agency VALUES (233, 'AGEA', 'Italy', 'u1@Niva', '2020-08-27 15:39:59.44', NULL, NULL, 0, 4326);
INSERT INTO niva.agency VALUES (234, 'FEGA', 'Spain', 'u1@Niva', '2020-08-27 15:40:00.41', NULL, NULL, 0, 4326);
INSERT INTO niva.agency VALUES (4, 'OPEKEPE', 'Greece', 'u1@Niva', '2020-06-12 14:46:11.834', 'u4@Niva', '2021-04-16 11:21:37.286', 1, 2100);


-- FilesAndDirs

INSERT INTO niva.file_dir_path VALUES (10, 'C:\cygwin64\tmp\', 'u10@Niva', '2020-09-17 11:46:16', 'u4@Niva', '2021-02-22 14:28:50.859', 8, 1);

-- Classification Engine

INSERT INTO niva.classifier VALUES (19, 'Sen4CAP', 'Sentinels for CAP monitoring approach', 'u1@Niva', '2020-06-15 13:26:18.693', 'u1@Niva', '2020-09-17 14:24:42.833', 2);



-- LOGGING

INSERT INTO niva.dbmanagementlog(
	version, notes, type)
	VALUES ('20210604a', 'First Logged Script Execution Succeeded', 2);




-- END OF FILE
